.class public final Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private correlationtoken_:Ljava/lang/Object;

.field private hintMsg_:Ljava/lang/Object;

.field private invitee_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Invitee;",
            ">;"
        }
    .end annotation
.end field

.field private inviterDisplayName_:Ljava/lang/Object;

.field private specifiedContent_:Ljava/lang/Object;

.field private specifiedSubject_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 18764
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 18975
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 19018
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->invitee_:Ljava/util/List;

    .line 19107
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->correlationtoken_:Ljava/lang/Object;

    .line 19143
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->inviterDisplayName_:Ljava/lang/Object;

    .line 19179
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->hintMsg_:Ljava/lang/Object;

    .line 19215
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->specifiedContent_:Ljava/lang/Object;

    .line 19251
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->specifiedSubject_:Ljava/lang/Object;

    .line 18765
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->maybeForceBuilderInitialization()V

    .line 18766
    return-void
.end method

.method static synthetic access$23400(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 18759
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$23500()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 1

    .prologue
    .line 18759
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 18811
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    move-result-object v0

    .line 18812
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 18813
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 18816
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 1

    .prologue
    .line 18771
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureInviteeIsMutable()V
    .locals 2

    .prologue
    .line 19021
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 19022
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->invitee_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->invitee_:Ljava/util/List;

    .line 19023
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 19025
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 18769
    return-void
.end method


# virtual methods
.method public addAllInvitee(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$Invitee;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;"
        }
    .end annotation

    .prologue
    .line 19088
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->ensureInviteeIsMutable()V

    .line 19089
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->invitee_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 19091
    return-object p0
.end method

.method public addInvitee(ILcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 19081
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->ensureInviteeIsMutable()V

    .line 19082
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->invitee_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 19084
    return-object p0
.end method

.method public addInvitee(ILcom/sgiggle/xmpp/SessionMessages$Invitee;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 19064
    if-nez p2, :cond_0

    .line 19065
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 19067
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->ensureInviteeIsMutable()V

    .line 19068
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->invitee_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 19070
    return-object p0
.end method

.method public addInvitee(Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 19074
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->ensureInviteeIsMutable()V

    .line 19075
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->invitee_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 19077
    return-object p0
.end method

.method public addInvitee(Lcom/sgiggle/xmpp/SessionMessages$Invitee;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 19054
    if-nez p1, :cond_0

    .line 19055
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 19057
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->ensureInviteeIsMutable()V

    .line 19058
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->invitee_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 19060
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 18759
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;
    .locals 2

    .prologue
    .line 18802
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    move-result-object v0

    .line 18803
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 18804
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 18806
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 18759
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;
    .locals 5

    .prologue
    .line 18820
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 18821
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 18822
    const/4 v2, 0x0

    .line 18823
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 18824
    or-int/lit8 v2, v2, 0x1

    .line 18826
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->access$23702(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 18827
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 18828
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->invitee_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->invitee_:Ljava/util/List;

    .line 18829
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x3

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 18831
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->invitee_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->invitee_:Ljava/util/List;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->access$23802(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;Ljava/util/List;)Ljava/util/List;

    .line 18832
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 18833
    or-int/lit8 v2, v2, 0x2

    .line 18835
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->correlationtoken_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->correlationtoken_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->access$23902(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18836
    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 18837
    or-int/lit8 v2, v2, 0x4

    .line 18839
    :cond_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->inviterDisplayName_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->inviterDisplayName_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->access$24002(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18840
    and-int/lit8 v3, v1, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 18841
    or-int/lit8 v2, v2, 0x8

    .line 18843
    :cond_4
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->hintMsg_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->hintMsg_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->access$24102(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18844
    and-int/lit8 v3, v1, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 18845
    or-int/lit8 v2, v2, 0x10

    .line 18847
    :cond_5
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->specifiedContent_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->specifiedContent_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->access$24202(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18848
    and-int/lit8 v1, v1, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_6

    .line 18849
    or-int/lit8 v1, v2, 0x20

    .line 18851
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->specifiedSubject_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->specifiedSubject_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->access$24302(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18852
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->access$24402(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;I)I

    .line 18853
    return-object v0

    :cond_6
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 18759
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 18759
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 1

    .prologue
    .line 18775
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 18776
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 18777
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 18778
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->invitee_:Ljava/util/List;

    .line 18779
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 18780
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->correlationtoken_:Ljava/lang/Object;

    .line 18781
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 18782
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->inviterDisplayName_:Ljava/lang/Object;

    .line 18783
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 18784
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->hintMsg_:Ljava/lang/Object;

    .line 18785
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 18786
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->specifiedContent_:Ljava/lang/Object;

    .line 18787
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 18788
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->specifiedSubject_:Ljava/lang/Object;

    .line 18789
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 18790
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 1

    .prologue
    .line 19011
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 19013
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 19014
    return-object p0
.end method

.method public clearCorrelationtoken()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 1

    .prologue
    .line 19131
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 19132
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getCorrelationtoken()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->correlationtoken_:Ljava/lang/Object;

    .line 19134
    return-object p0
.end method

.method public clearHintMsg()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 1

    .prologue
    .line 19203
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 19204
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getHintMsg()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->hintMsg_:Ljava/lang/Object;

    .line 19206
    return-object p0
.end method

.method public clearInvitee()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 1

    .prologue
    .line 19094
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->invitee_:Ljava/util/List;

    .line 19095
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 19097
    return-object p0
.end method

.method public clearInviterDisplayName()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 1

    .prologue
    .line 19167
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 19168
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getInviterDisplayName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->inviterDisplayName_:Ljava/lang/Object;

    .line 19170
    return-object p0
.end method

.method public clearSpecifiedContent()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 1

    .prologue
    .line 19239
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 19240
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getSpecifiedContent()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->specifiedContent_:Ljava/lang/Object;

    .line 19242
    return-object p0
.end method

.method public clearSpecifiedSubject()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 1

    .prologue
    .line 19275
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 19276
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getSpecifiedSubject()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->specifiedSubject_:Ljava/lang/Object;

    .line 19278
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 18759
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 18759
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 18759
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 2

    .prologue
    .line 18794
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 18759
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 18980
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCorrelationtoken()Ljava/lang/String;
    .locals 2

    .prologue
    .line 19112
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->correlationtoken_:Ljava/lang/Object;

    .line 19113
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 19114
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 19115
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->correlationtoken_:Ljava/lang/Object;

    .line 19118
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 18759
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 18759
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;
    .locals 1

    .prologue
    .line 18798
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    move-result-object v0

    return-object v0
.end method

.method public getHintMsg()Ljava/lang/String;
    .locals 2

    .prologue
    .line 19184
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->hintMsg_:Ljava/lang/Object;

    .line 19185
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 19186
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 19187
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->hintMsg_:Ljava/lang/Object;

    .line 19190
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getInvitee(I)Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    .locals 1
    .parameter

    .prologue
    .line 19034
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->invitee_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    return-object v0
.end method

.method public getInviteeCount()I
    .locals 1

    .prologue
    .line 19031
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->invitee_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getInviteeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Invitee;",
            ">;"
        }
    .end annotation

    .prologue
    .line 19028
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->invitee_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getInviterDisplayName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 19148
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->inviterDisplayName_:Ljava/lang/Object;

    .line 19149
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 19150
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 19151
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->inviterDisplayName_:Ljava/lang/Object;

    .line 19154
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getSpecifiedContent()Ljava/lang/String;
    .locals 2

    .prologue
    .line 19220
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->specifiedContent_:Ljava/lang/Object;

    .line 19221
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 19222
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 19223
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->specifiedContent_:Ljava/lang/Object;

    .line 19226
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getSpecifiedSubject()Ljava/lang/String;
    .locals 2

    .prologue
    .line 19256
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->specifiedSubject_:Ljava/lang/Object;

    .line 19257
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 19258
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 19259
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->specifiedSubject_:Ljava/lang/Object;

    .line 19262
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 18977
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCorrelationtoken()Z
    .locals 2

    .prologue
    .line 19109
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHintMsg()Z
    .locals 2

    .prologue
    .line 19181
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasInviterDisplayName()Z
    .locals 2

    .prologue
    .line 19145
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSpecifiedContent()Z
    .locals 2

    .prologue
    .line 19217
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSpecifiedSubject()Z
    .locals 2

    .prologue
    .line 19253
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 18890
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 18908
    :goto_0
    return v0

    .line 18894
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->hasCorrelationtoken()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 18896
    goto :goto_0

    .line 18898
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    .line 18900
    goto :goto_0

    :cond_2
    move v0, v2

    .line 18902
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->getInviteeCount()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 18903
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->getInvitee(I)Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_3

    move v0, v2

    .line 18905
    goto :goto_0

    .line 18902
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 18908
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 18999
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 19001
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 19007
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 19008
    return-object p0

    .line 19004
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18759
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 18759
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18759
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18916
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 18917
    sparse-switch v0, :sswitch_data_0

    .line 18922
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 18924
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 18920
    goto :goto_1

    .line 18929
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 18930
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 18931
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 18933
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 18934
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    goto :goto_0

    .line 18938
    :sswitch_2
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    .line 18939
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 18940
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->addInvitee(Lcom/sgiggle/xmpp/SessionMessages$Invitee;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    goto :goto_0

    .line 18944
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 18945
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->correlationtoken_:Ljava/lang/Object;

    goto :goto_0

    .line 18949
    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 18950
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->inviterDisplayName_:Ljava/lang/Object;

    goto :goto_0

    .line 18954
    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 18955
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->hintMsg_:Ljava/lang/Object;

    goto :goto_0

    .line 18959
    :sswitch_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 18960
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->specifiedContent_:Ljava/lang/Object;

    goto :goto_0

    .line 18964
    :sswitch_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 18965
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->specifiedSubject_:Ljava/lang/Object;

    goto :goto_0

    .line 18917
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 18857
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 18886
    :goto_0
    return-object v0

    .line 18858
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 18859
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    .line 18861
    :cond_1
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->invitee_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->access$23800(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 18862
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->invitee_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 18863
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->invitee_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->access$23800(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->invitee_:Ljava/util/List;

    .line 18864
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 18871
    :cond_2
    :goto_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->hasCorrelationtoken()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 18872
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getCorrelationtoken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->setCorrelationtoken(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    .line 18874
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->hasInviterDisplayName()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 18875
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getInviterDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->setInviterDisplayName(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    .line 18877
    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->hasHintMsg()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 18878
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getHintMsg()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->setHintMsg(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    .line 18880
    :cond_5
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->hasSpecifiedContent()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 18881
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getSpecifiedContent()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->setSpecifiedContent(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    .line 18883
    :cond_6
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->hasSpecifiedSubject()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 18884
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getSpecifiedSubject()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->setSpecifiedSubject(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    :cond_7
    move-object v0, p0

    .line 18886
    goto :goto_0

    .line 18866
    :cond_8
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->ensureInviteeIsMutable()V

    .line 18867
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->invitee_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->invitee_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->access$23800(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public removeInvitee(I)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 19100
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->ensureInviteeIsMutable()V

    .line 19101
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->invitee_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 19103
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 18993
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 18995
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 18996
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 18983
    if-nez p1, :cond_0

    .line 18984
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 18986
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 18988
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 18989
    return-object p0
.end method

.method public setCorrelationtoken(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 19122
    if-nez p1, :cond_0

    .line 19123
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 19125
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 19126
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->correlationtoken_:Ljava/lang/Object;

    .line 19128
    return-object p0
.end method

.method setCorrelationtoken(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 19137
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 19138
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->correlationtoken_:Ljava/lang/Object;

    .line 19140
    return-void
.end method

.method public setHintMsg(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 19194
    if-nez p1, :cond_0

    .line 19195
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 19197
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 19198
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->hintMsg_:Ljava/lang/Object;

    .line 19200
    return-object p0
.end method

.method setHintMsg(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 19209
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 19210
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->hintMsg_:Ljava/lang/Object;

    .line 19212
    return-void
.end method

.method public setInvitee(ILcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 19048
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->ensureInviteeIsMutable()V

    .line 19049
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->invitee_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 19051
    return-object p0
.end method

.method public setInvitee(ILcom/sgiggle/xmpp/SessionMessages$Invitee;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 19038
    if-nez p2, :cond_0

    .line 19039
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 19041
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->ensureInviteeIsMutable()V

    .line 19042
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->invitee_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 19044
    return-object p0
.end method

.method public setInviterDisplayName(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 19158
    if-nez p1, :cond_0

    .line 19159
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 19161
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 19162
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->inviterDisplayName_:Ljava/lang/Object;

    .line 19164
    return-object p0
.end method

.method setInviterDisplayName(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 19173
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 19174
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->inviterDisplayName_:Ljava/lang/Object;

    .line 19176
    return-void
.end method

.method public setSpecifiedContent(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 19230
    if-nez p1, :cond_0

    .line 19231
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 19233
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 19234
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->specifiedContent_:Ljava/lang/Object;

    .line 19236
    return-object p0
.end method

.method setSpecifiedContent(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 19245
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 19246
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->specifiedContent_:Ljava/lang/Object;

    .line 19248
    return-void
.end method

.method public setSpecifiedSubject(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 19266
    if-nez p1, :cond_0

    .line 19267
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 19269
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 19270
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->specifiedSubject_:Ljava/lang/Object;

    .line 19272
    return-object p0
.end method

.method setSpecifiedSubject(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 19281
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->bitField0_:I

    .line 19282
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->specifiedSubject_:Ljava/lang/Object;

    .line 19284
    return-void
.end method
