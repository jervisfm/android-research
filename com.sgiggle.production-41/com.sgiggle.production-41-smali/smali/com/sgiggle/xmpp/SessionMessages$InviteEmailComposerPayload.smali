.class public final Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InviteEmailComposerPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CORRELATIONTOKEN_FIELD_NUMBER:I = 0x3

.field public static final HINT_MSG_FIELD_NUMBER:I = 0x5

.field public static final INVITEE_FIELD_NUMBER:I = 0x2

.field public static final INVITER_DISPLAY_NAME_FIELD_NUMBER:I = 0x4

.field public static final SPECIFIED_CONTENT_FIELD_NUMBER:I = 0x6

.field public static final SPECIFIED_SUBJECT_FIELD_NUMBER:I = 0x7

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private correlationtoken_:Ljava/lang/Object;

.field private hintMsg_:Ljava/lang/Object;

.field private invitee_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Invitee;",
            ">;"
        }
    .end annotation
.end field

.field private inviterDisplayName_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private specifiedContent_:Ljava/lang/Object;

.field private specifiedSubject_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19290
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    .line 19291
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->initFields()V

    .line 19292
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 18375
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 18589
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->memoizedIsInitialized:B

    .line 18642
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->memoizedSerializedSize:I

    .line 18376
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 18370
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 18377
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 18589
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->memoizedIsInitialized:B

    .line 18642
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->memoizedSerializedSize:I

    .line 18377
    return-void
.end method

.method static synthetic access$23702(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 18370
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$23800(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 18370
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->invitee_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$23802(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 18370
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->invitee_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$23902(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 18370
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->correlationtoken_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$24002(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 18370
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->inviterDisplayName_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$24102(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 18370
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->hintMsg_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$24202(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 18370
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->specifiedContent_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$24302(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 18370
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->specifiedSubject_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$24402(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 18370
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->bitField0_:I

    return p1
.end method

.method private getCorrelationtokenBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 18441
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->correlationtoken_:Ljava/lang/Object;

    .line 18442
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 18443
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 18445
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->correlationtoken_:Ljava/lang/Object;

    .line 18448
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;
    .locals 1

    .prologue
    .line 18381
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    return-object v0
.end method

.method private getHintMsgBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 18505
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->hintMsg_:Ljava/lang/Object;

    .line 18506
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 18507
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 18509
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->hintMsg_:Ljava/lang/Object;

    .line 18512
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getInviterDisplayNameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 18473
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->inviterDisplayName_:Ljava/lang/Object;

    .line 18474
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 18475
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 18477
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->inviterDisplayName_:Ljava/lang/Object;

    .line 18480
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getSpecifiedContentBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 18537
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->specifiedContent_:Ljava/lang/Object;

    .line 18538
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 18539
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 18541
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->specifiedContent_:Ljava/lang/Object;

    .line 18544
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getSpecifiedSubjectBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 18569
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->specifiedSubject_:Ljava/lang/Object;

    .line 18570
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 18571
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 18573
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->specifiedSubject_:Ljava/lang/Object;

    .line 18576
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 18581
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 18582
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->invitee_:Ljava/util/List;

    .line 18583
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->correlationtoken_:Ljava/lang/Object;

    .line 18584
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->inviterDisplayName_:Ljava/lang/Object;

    .line 18585
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->hintMsg_:Ljava/lang/Object;

    .line 18586
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->specifiedContent_:Ljava/lang/Object;

    .line 18587
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->specifiedSubject_:Ljava/lang/Object;

    .line 18588
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 1

    .prologue
    .line 18752
    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->access$23500()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 18755
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18721
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    move-result-object v0

    .line 18722
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 18723
    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->access$23400(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    move-result-object v0

    .line 18725
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18732
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    move-result-object v0

    .line 18733
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 18734
    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->access$23400(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    move-result-object v0

    .line 18736
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 18688
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->access$23400(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 18694
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->access$23400(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18742
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->access$23400(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18748
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->access$23400(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18710
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->access$23400(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18716
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->access$23400(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 18699
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->access$23400(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 18705
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;->access$23400(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 18396
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCorrelationtoken()Ljava/lang/String;
    .locals 2

    .prologue
    .line 18427
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->correlationtoken_:Ljava/lang/Object;

    .line 18428
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 18429
    check-cast v0, Ljava/lang/String;

    .line 18437
    :goto_0
    return-object v0

    .line 18431
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 18433
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 18434
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 18435
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->correlationtoken_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 18437
    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 18370
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;
    .locals 1

    .prologue
    .line 18385
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    return-object v0
.end method

.method public getHintMsg()Ljava/lang/String;
    .locals 2

    .prologue
    .line 18491
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->hintMsg_:Ljava/lang/Object;

    .line 18492
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 18493
    check-cast v0, Ljava/lang/String;

    .line 18501
    :goto_0
    return-object v0

    .line 18495
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 18497
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 18498
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 18499
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->hintMsg_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 18501
    goto :goto_0
.end method

.method public getInvitee(I)Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    .locals 1
    .parameter

    .prologue
    .line 18413
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->invitee_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    return-object v0
.end method

.method public getInviteeCount()I
    .locals 1

    .prologue
    .line 18410
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->invitee_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getInviteeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Invitee;",
            ">;"
        }
    .end annotation

    .prologue
    .line 18403
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->invitee_:Ljava/util/List;

    return-object v0
.end method

.method public getInviteeOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$InviteeOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 18417
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->invitee_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteeOrBuilder;

    return-object v0
.end method

.method public getInviteeOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$InviteeOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 18407
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->invitee_:Ljava/util/List;

    return-object v0
.end method

.method public getInviterDisplayName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 18459
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->inviterDisplayName_:Ljava/lang/Object;

    .line 18460
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 18461
    check-cast v0, Ljava/lang/String;

    .line 18469
    :goto_0
    return-object v0

    .line 18463
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 18465
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 18466
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 18467
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->inviterDisplayName_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 18469
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 18644
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->memoizedSerializedSize:I

    .line 18645
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 18677
    :goto_0
    return v0

    .line 18648
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_7

    .line 18649
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    :goto_1
    move v1, v2

    move v2, v0

    .line 18652
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->invitee_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 18653
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->invitee_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 18652
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 18656
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_6

    .line 18657
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getCorrelationtokenBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/2addr v0, v2

    .line 18660
    :goto_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_2

    .line 18661
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getInviterDisplayNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 18664
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_3

    .line 18665
    const/4 v1, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getHintMsgBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 18668
    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_4

    .line 18669
    const/4 v1, 0x6

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getSpecifiedContentBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 18672
    :cond_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_5

    .line 18673
    const/4 v1, 0x7

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getSpecifiedSubjectBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 18676
    :cond_5
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->memoizedSerializedSize:I

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_3

    :cond_7
    move v0, v2

    goto :goto_1
.end method

.method public getSpecifiedContent()Ljava/lang/String;
    .locals 2

    .prologue
    .line 18523
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->specifiedContent_:Ljava/lang/Object;

    .line 18524
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 18525
    check-cast v0, Ljava/lang/String;

    .line 18533
    :goto_0
    return-object v0

    .line 18527
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 18529
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 18530
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 18531
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->specifiedContent_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 18533
    goto :goto_0
.end method

.method public getSpecifiedSubject()Ljava/lang/String;
    .locals 2

    .prologue
    .line 18555
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->specifiedSubject_:Ljava/lang/Object;

    .line 18556
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 18557
    check-cast v0, Ljava/lang/String;

    .line 18565
    :goto_0
    return-object v0

    .line 18559
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 18561
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 18562
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 18563
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->specifiedSubject_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 18565
    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 18393
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCorrelationtoken()Z
    .locals 2

    .prologue
    .line 18424
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHintMsg()Z
    .locals 2

    .prologue
    .line 18488
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasInviterDisplayName()Z
    .locals 2

    .prologue
    .line 18456
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSpecifiedContent()Z
    .locals 2

    .prologue
    .line 18520
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSpecifiedSubject()Z
    .locals 2

    .prologue
    .line 18552
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 18591
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->memoizedIsInitialized:B

    .line 18592
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 18613
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 18592
    goto :goto_0

    .line 18594
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 18595
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 18596
    goto :goto_0

    .line 18598
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->hasCorrelationtoken()Z

    move-result v0

    if-nez v0, :cond_3

    .line 18599
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 18600
    goto :goto_0

    .line 18602
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    .line 18603
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 18604
    goto :goto_0

    :cond_4
    move v0, v2

    .line 18606
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getInviteeCount()I

    move-result v1

    if-ge v0, v1, :cond_6

    .line 18607
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getInvitee(I)Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_5

    .line 18608
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 18609
    goto :goto_0

    .line 18606
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 18612
    :cond_6
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 18613
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 18370
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 1

    .prologue
    .line 18753
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 18370
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;
    .locals 1

    .prologue
    .line 18757
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 18682
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 18618
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getSerializedSize()I

    .line 18619
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 18620
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 18622
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->invitee_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 18623
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->invitee_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 18622
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 18625
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 18626
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getCorrelationtokenBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 18628
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_3

    .line 18629
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getInviterDisplayNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 18631
    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 18632
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getHintMsgBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 18634
    :cond_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 18635
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getSpecifiedContentBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 18637
    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 18638
    const/4 v0, 0x7

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getSpecifiedSubjectBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 18640
    :cond_6
    return-void
.end method
