.class public final Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private contact_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private selected_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private specifiedInvitePrompt_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 17262
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 17444
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 17487
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->contact_:Ljava/util/List;

    .line 17576
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->selected_:Ljava/util/List;

    .line 17621
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->specifiedInvitePrompt_:Ljava/lang/Object;

    .line 17263
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->maybeForceBuilderInitialization()V

    .line 17264
    return-void
.end method

.method static synthetic access$21800(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 17257
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$21900()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    .locals 1

    .prologue
    .line 17257
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 17303
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    move-result-object v0

    .line 17304
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 17305
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 17308
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    .locals 1

    .prologue
    .line 17269
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureContactIsMutable()V
    .locals 2

    .prologue
    .line 17490
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 17491
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->contact_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->contact_:Ljava/util/List;

    .line 17492
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    .line 17494
    :cond_0
    return-void
.end method

.method private ensureSelectedIsMutable()V
    .locals 2

    .prologue
    .line 17578
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 17579
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->selected_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->selected_:Ljava/util/List;

    .line 17580
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    .line 17582
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 17267
    return-void
.end method


# virtual methods
.method public addAllContact(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;"
        }
    .end annotation

    .prologue
    .line 17557
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->ensureContactIsMutable()V

    .line 17558
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->contact_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 17560
    return-object p0
.end method

.method public addAllSelected(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;"
        }
    .end annotation

    .prologue
    .line 17608
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->ensureSelectedIsMutable()V

    .line 17609
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->selected_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 17611
    return-object p0
.end method

.method public addContact(ILcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 17550
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->ensureContactIsMutable()V

    .line 17551
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->contact_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 17553
    return-object p0
.end method

.method public addContact(ILcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 17533
    if-nez p2, :cond_0

    .line 17534
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 17536
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->ensureContactIsMutable()V

    .line 17537
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->contact_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 17539
    return-object p0
.end method

.method public addContact(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 17543
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->ensureContactIsMutable()V

    .line 17544
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->contact_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 17546
    return-object p0
.end method

.method public addContact(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 17523
    if-nez p1, :cond_0

    .line 17524
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 17526
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->ensureContactIsMutable()V

    .line 17527
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->contact_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 17529
    return-object p0
.end method

.method public addSelected(Z)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 17601
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->ensureSelectedIsMutable()V

    .line 17602
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->selected_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 17604
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 17257
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;
    .locals 2

    .prologue
    .line 17294
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    move-result-object v0

    .line 17295
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 17296
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 17298
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 17257
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;
    .locals 5

    .prologue
    .line 17312
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 17313
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    .line 17314
    const/4 v2, 0x0

    .line 17315
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 17316
    or-int/lit8 v2, v2, 0x1

    .line 17318
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->access$22102(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 17319
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 17320
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->contact_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->contact_:Ljava/util/List;

    .line 17321
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x3

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    .line 17323
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->contact_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->contact_:Ljava/util/List;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->access$22202(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;Ljava/util/List;)Ljava/util/List;

    .line 17324
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 17325
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->selected_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->selected_:Ljava/util/List;

    .line 17326
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x5

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    .line 17328
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->selected_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->selected_:Ljava/util/List;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->access$22302(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;Ljava/util/List;)Ljava/util/List;

    .line 17329
    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_3

    .line 17330
    or-int/lit8 v1, v2, 0x2

    .line 17332
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->specifiedInvitePrompt_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->specifiedInvitePrompt_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->access$22402(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17333
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->access$22502(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;I)I

    .line 17334
    return-object v0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 17257
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 17257
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    .locals 1

    .prologue
    .line 17273
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 17274
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 17275
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    .line 17276
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->contact_:Ljava/util/List;

    .line 17277
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    .line 17278
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->selected_:Ljava/util/List;

    .line 17279
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    .line 17280
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->specifiedInvitePrompt_:Ljava/lang/Object;

    .line 17281
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    .line 17282
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    .locals 1

    .prologue
    .line 17480
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 17482
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    .line 17483
    return-object p0
.end method

.method public clearContact()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    .locals 1

    .prologue
    .line 17563
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->contact_:Ljava/util/List;

    .line 17564
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    .line 17566
    return-object p0
.end method

.method public clearSelected()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    .locals 1

    .prologue
    .line 17614
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->selected_:Ljava/util/List;

    .line 17615
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    .line 17617
    return-object p0
.end method

.method public clearSpecifiedInvitePrompt()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    .locals 1

    .prologue
    .line 17645
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    .line 17646
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->getSpecifiedInvitePrompt()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->specifiedInvitePrompt_:Ljava/lang/Object;

    .line 17648
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 17257
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 17257
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 17257
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    .locals 2

    .prologue
    .line 17286
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 17257
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 17449
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getContact(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter

    .prologue
    .line 17503
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->contact_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getContactCount()I
    .locals 1

    .prologue
    .line 17500
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->contact_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getContactList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17497
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->contact_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 17257
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 17257
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;
    .locals 1

    .prologue
    .line 17290
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    move-result-object v0

    return-object v0
.end method

.method public getSelected(I)Z
    .locals 1
    .parameter

    .prologue
    .line 17591
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->selected_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public getSelectedCount()I
    .locals 1

    .prologue
    .line 17588
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->selected_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getSelectedList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17585
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->selected_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSpecifiedInvitePrompt()Ljava/lang/String;
    .locals 2

    .prologue
    .line 17626
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->specifiedInvitePrompt_:Ljava/lang/Object;

    .line 17627
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 17628
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 17629
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->specifiedInvitePrompt_:Ljava/lang/Object;

    .line 17632
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 17446
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSpecifiedInvitePrompt()Z
    .locals 2

    .prologue
    .line 17623
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 17369
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 17383
    :goto_0
    return v0

    .line 17373
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 17375
    goto :goto_0

    :cond_1
    move v0, v2

    .line 17377
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->getContactCount()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 17378
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->getContact(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_2

    move v0, v2

    .line 17380
    goto :goto_0

    .line 17377
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 17383
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 17468
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 17470
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 17476
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    .line 17477
    return-object p0

    .line 17473
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17257
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 17257
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17257
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17391
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 17392
    sparse-switch v0, :sswitch_data_0

    .line 17397
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 17399
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 17395
    goto :goto_1

    .line 17404
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 17405
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 17406
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 17408
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 17409
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    goto :goto_0

    .line 17413
    :sswitch_2
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    .line 17414
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 17415
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->addContact(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    goto :goto_0

    .line 17419
    :sswitch_3
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->ensureSelectedIsMutable()V

    .line 17420
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->selected_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 17424
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readRawVarint32()I

    move-result v0

    .line 17425
    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->pushLimit(I)I

    move-result v0

    .line 17426
    :goto_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->getBytesUntilLimit()I

    move-result v1

    if-lez v1, :cond_2

    .line 17427
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->addSelected(Z)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    goto :goto_2

    .line 17429
    :cond_2
    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->popLimit(I)V

    goto :goto_0

    .line 17433
    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    .line 17434
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->specifiedInvitePrompt_:Ljava/lang/Object;

    goto :goto_0

    .line 17392
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
        0x22 -> :sswitch_5
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 17338
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 17365
    :goto_0
    return-object v0

    .line 17339
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 17340
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    .line 17342
    :cond_1
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->contact_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->access$22200(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 17343
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->contact_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 17344
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->contact_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->access$22200(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->contact_:Ljava/util/List;

    .line 17345
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    .line 17352
    :cond_2
    :goto_1
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->selected_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->access$22300(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 17353
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->selected_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 17354
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->selected_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->access$22300(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->selected_:Ljava/util/List;

    .line 17355
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    .line 17362
    :cond_3
    :goto_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->hasSpecifiedInvitePrompt()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 17363
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->getSpecifiedInvitePrompt()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->setSpecifiedInvitePrompt(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    :cond_4
    move-object v0, p0

    .line 17365
    goto :goto_0

    .line 17347
    :cond_5
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->ensureContactIsMutable()V

    .line 17348
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->contact_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->contact_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->access$22200(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 17357
    :cond_6
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->ensureSelectedIsMutable()V

    .line 17358
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->selected_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->selected_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->access$22300(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public removeContact(I)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 17569
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->ensureContactIsMutable()V

    .line 17570
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->contact_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 17572
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 17462
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 17464
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    .line 17465
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 17452
    if-nez p1, :cond_0

    .line 17453
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 17455
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 17457
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    .line 17458
    return-object p0
.end method

.method public setContact(ILcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 17517
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->ensureContactIsMutable()V

    .line 17518
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->contact_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 17520
    return-object p0
.end method

.method public setContact(ILcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 17507
    if-nez p2, :cond_0

    .line 17508
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 17510
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->ensureContactIsMutable()V

    .line 17511
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->contact_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 17513
    return-object p0
.end method

.method public setSelected(IZ)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 17595
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->ensureSelectedIsMutable()V

    .line 17596
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->selected_:Ljava/util/List;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 17598
    return-object p0
.end method

.method public setSpecifiedInvitePrompt(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 17636
    if-nez p1, :cond_0

    .line 17637
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 17639
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    .line 17640
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->specifiedInvitePrompt_:Ljava/lang/Object;

    .line 17642
    return-object p0
.end method

.method setSpecifiedInvitePrompt(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 17651
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->bitField0_:I

    .line 17652
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->specifiedInvitePrompt_:Ljava/lang/Object;

    .line 17654
    return-void
.end method
