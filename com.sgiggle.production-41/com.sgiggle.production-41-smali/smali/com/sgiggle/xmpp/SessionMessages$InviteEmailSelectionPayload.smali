.class public final Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InviteEmailSelectionPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CONTACT_FIELD_NUMBER:I = 0x2

.field public static final SELECTED_FIELD_NUMBER:I = 0x3

.field public static final SPECIFIED_INVITE_PROMPT_FIELD_NUMBER:I = 0x4

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private contact_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private selected_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private specifiedInvitePrompt_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17660
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    .line 17661
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->initFields()V

    .line 17662
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 17013
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 17110
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->memoizedIsInitialized:B

    .line 17150
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->memoizedSerializedSize:I

    .line 17014
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 17008
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 17015
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 17110
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->memoizedIsInitialized:B

    .line 17150
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->memoizedSerializedSize:I

    .line 17015
    return-void
.end method

.method static synthetic access$22102(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 17008
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$22200(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 17008
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->contact_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$22202(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 17008
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->contact_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$22300(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 17008
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->selected_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$22302(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 17008
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->selected_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$22402(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 17008
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->specifiedInvitePrompt_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$22502(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 17008
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;
    .locals 1

    .prologue
    .line 17019
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    return-object v0
.end method

.method private getSpecifiedInvitePromptBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 17093
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->specifiedInvitePrompt_:Ljava/lang/Object;

    .line 17094
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 17095
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 17097
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->specifiedInvitePrompt_:Ljava/lang/Object;

    .line 17100
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 17105
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 17106
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->contact_:Ljava/util/List;

    .line 17107
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->selected_:Ljava/util/List;

    .line 17108
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->specifiedInvitePrompt_:Ljava/lang/Object;

    .line 17109
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    .locals 1

    .prologue
    .line 17250
    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->access$21900()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 17253
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17219
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    move-result-object v0

    .line 17220
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 17221
    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->access$21800(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    move-result-object v0

    .line 17223
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17230
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    move-result-object v0

    .line 17231
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 17232
    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->access$21800(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    move-result-object v0

    .line 17234
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 17186
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->access$21800(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 17192
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->access$21800(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17240
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->access$21800(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17246
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->access$21800(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17208
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->access$21800(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17214
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->access$21800(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 17197
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->access$21800(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 17203
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;->access$21800(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 17034
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getContact(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter

    .prologue
    .line 17051
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->contact_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getContactCount()I
    .locals 1

    .prologue
    .line 17048
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->contact_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getContactList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17041
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->contact_:Ljava/util/List;

    return-object v0
.end method

.method public getContactOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 17055
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->contact_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;

    return-object v0
.end method

.method public getContactOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17045
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->contact_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 17008
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;
    .locals 1

    .prologue
    .line 17023
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    return-object v0
.end method

.method public getSelected(I)Z
    .locals 1
    .parameter

    .prologue
    .line 17069
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->selected_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public getSelectedCount()I
    .locals 1

    .prologue
    .line 17066
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->selected_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getSelectedList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17063
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->selected_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 17152
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->memoizedSerializedSize:I

    .line 17153
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 17175
    :goto_0
    return v0

    .line 17156
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 17157
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    :goto_1
    move v1, v2

    move v2, v0

    .line 17160
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->contact_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 17161
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->contact_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 17160
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 17166
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->getSelectedList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1

    .line 17167
    add-int/2addr v0, v2

    .line 17168
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->getSelectedList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 17170
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_2

    .line 17171
    const/4 v1, 0x4

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->getSpecifiedInvitePromptBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17174
    :cond_2
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->memoizedSerializedSize:I

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method public getSpecifiedInvitePrompt()Ljava/lang/String;
    .locals 2

    .prologue
    .line 17079
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->specifiedInvitePrompt_:Ljava/lang/Object;

    .line 17080
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 17081
    check-cast v0, Ljava/lang/String;

    .line 17089
    :goto_0
    return-object v0

    .line 17083
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 17085
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 17086
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 17087
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->specifiedInvitePrompt_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 17089
    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 17031
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSpecifiedInvitePrompt()Z
    .locals 2

    .prologue
    .line 17076
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 17112
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->memoizedIsInitialized:B

    .line 17113
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 17130
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 17113
    goto :goto_0

    .line 17115
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 17116
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 17117
    goto :goto_0

    .line 17119
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 17120
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 17121
    goto :goto_0

    :cond_3
    move v0, v2

    .line 17123
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->getContactCount()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 17124
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->getContact(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_4

    .line 17125
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 17126
    goto :goto_0

    .line 17123
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 17129
    :cond_5
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 17130
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 17008
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    .locals 1

    .prologue
    .line 17251
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 17008
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;
    .locals 1

    .prologue
    .line 17255
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 17180
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 17135
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->getSerializedSize()I

    .line 17136
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 17137
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    move v1, v2

    .line 17139
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->contact_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 17140
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->contact_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 17139
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v1, v2

    .line 17142
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->selected_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 17143
    const/4 v2, 0x3

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->selected_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 17142
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 17145
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_3

    .line 17146
    const/4 v0, 0x4

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->getSpecifiedInvitePromptBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 17148
    :cond_3
    return-void
.end method
