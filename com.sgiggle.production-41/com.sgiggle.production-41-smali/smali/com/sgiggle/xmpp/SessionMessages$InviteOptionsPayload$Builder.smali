.class public final Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private emailinvitetype_:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

.field private message_:Ljava/lang/Object;

.field private smsinvitetype_:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

.field private sns_:Ljava/lang/Object;

.field private specifiedInvitePrompt_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 16591
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 16777
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 16820
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;->CLIENT:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->smsinvitetype_:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    .line 16844
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;->CLIENT:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->emailinvitetype_:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    .line 16868
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->message_:Ljava/lang/Object;

    .line 16904
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->sns_:Ljava/lang/Object;

    .line 16940
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->specifiedInvitePrompt_:Ljava/lang/Object;

    .line 16592
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->maybeForceBuilderInitialization()V

    .line 16593
    return-void
.end method

.method static synthetic access$20800(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 16586
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$20900()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;
    .locals 1

    .prologue
    .line 16586
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 16636
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    move-result-object v0

    .line 16637
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 16638
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 16641
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;
    .locals 1

    .prologue
    .line 16598
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 16596
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 16586
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;
    .locals 2

    .prologue
    .line 16627
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    move-result-object v0

    .line 16628
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 16629
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 16631
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 16586
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;
    .locals 5

    .prologue
    .line 16645
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 16646
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    .line 16647
    const/4 v2, 0x0

    .line 16648
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 16649
    or-int/lit8 v2, v2, 0x1

    .line 16651
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->access$21102(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 16652
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 16653
    or-int/lit8 v2, v2, 0x2

    .line 16655
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->smsinvitetype_:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->smsinvitetype_:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->access$21202(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    .line 16656
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 16657
    or-int/lit8 v2, v2, 0x4

    .line 16659
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->emailinvitetype_:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->emailinvitetype_:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->access$21302(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    .line 16660
    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 16661
    or-int/lit8 v2, v2, 0x8

    .line 16663
    :cond_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->message_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->message_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->access$21402(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16664
    and-int/lit8 v3, v1, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 16665
    or-int/lit8 v2, v2, 0x10

    .line 16667
    :cond_4
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->sns_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->sns_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->access$21502(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16668
    and-int/lit8 v1, v1, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_5

    .line 16669
    or-int/lit8 v1, v2, 0x20

    .line 16671
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->specifiedInvitePrompt_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->specifiedInvitePrompt_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->access$21602(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16672
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->access$21702(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;I)I

    .line 16673
    return-object v0

    :cond_5
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 16586
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 16586
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;
    .locals 1

    .prologue
    .line 16602
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 16603
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 16604
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    .line 16605
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;->CLIENT:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->smsinvitetype_:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    .line 16606
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    .line 16607
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;->CLIENT:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->emailinvitetype_:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    .line 16608
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    .line 16609
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->message_:Ljava/lang/Object;

    .line 16610
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    .line 16611
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->sns_:Ljava/lang/Object;

    .line 16612
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    .line 16613
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->specifiedInvitePrompt_:Ljava/lang/Object;

    .line 16614
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    .line 16615
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;
    .locals 1

    .prologue
    .line 16813
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 16815
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    .line 16816
    return-object p0
.end method

.method public clearEmailinvitetype()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;
    .locals 1

    .prologue
    .line 16861
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    .line 16862
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;->CLIENT:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->emailinvitetype_:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    .line 16864
    return-object p0
.end method

.method public clearMessage()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;
    .locals 1

    .prologue
    .line 16892
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    .line 16893
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->message_:Ljava/lang/Object;

    .line 16895
    return-object p0
.end method

.method public clearSmsinvitetype()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;
    .locals 1

    .prologue
    .line 16837
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    .line 16838
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;->CLIENT:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->smsinvitetype_:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    .line 16840
    return-object p0
.end method

.method public clearSns()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;
    .locals 1

    .prologue
    .line 16928
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    .line 16929
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->getSns()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->sns_:Ljava/lang/Object;

    .line 16931
    return-object p0
.end method

.method public clearSpecifiedInvitePrompt()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;
    .locals 1

    .prologue
    .line 16964
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    .line 16965
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->getSpecifiedInvitePrompt()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->specifiedInvitePrompt_:Ljava/lang/Object;

    .line 16967
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 16586
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 16586
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 16586
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;
    .locals 2

    .prologue
    .line 16619
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 16586
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 16782
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 16586
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 16586
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;
    .locals 1

    .prologue
    .line 16623
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    move-result-object v0

    return-object v0
.end method

.method public getEmailinvitetype()Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;
    .locals 1

    .prologue
    .line 16849
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->emailinvitetype_:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 16873
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->message_:Ljava/lang/Object;

    .line 16874
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 16875
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 16876
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->message_:Ljava/lang/Object;

    .line 16879
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getSmsinvitetype()Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;
    .locals 1

    .prologue
    .line 16825
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->smsinvitetype_:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    return-object v0
.end method

.method public getSns()Ljava/lang/String;
    .locals 2

    .prologue
    .line 16909
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->sns_:Ljava/lang/Object;

    .line 16910
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 16911
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 16912
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->sns_:Ljava/lang/Object;

    .line 16915
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getSpecifiedInvitePrompt()Ljava/lang/String;
    .locals 2

    .prologue
    .line 16945
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->specifiedInvitePrompt_:Ljava/lang/Object;

    .line 16946
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 16947
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 16948
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->specifiedInvitePrompt_:Ljava/lang/Object;

    .line 16951
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 16779
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasEmailinvitetype()Z
    .locals 2

    .prologue
    .line 16846
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMessage()Z
    .locals 2

    .prologue
    .line 16870
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSmsinvitetype()Z
    .locals 2

    .prologue
    .line 16822
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSns()Z
    .locals 2

    .prologue
    .line 16906
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSpecifiedInvitePrompt()Z
    .locals 2

    .prologue
    .line 16942
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 16700
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 16708
    :goto_0
    return v0

    .line 16704
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 16706
    goto :goto_0

    .line 16708
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 16801
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 16803
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 16809
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    .line 16810
    return-object p0

    .line 16806
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 16586
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 16586
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 16586
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 16716
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 16717
    sparse-switch v0, :sswitch_data_0

    .line 16722
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 16724
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 16720
    goto :goto_1

    .line 16729
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 16730
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 16731
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 16733
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 16734
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    goto :goto_0

    .line 16738
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 16739
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    move-result-object v0

    .line 16740
    if-eqz v0, :cond_0

    .line 16741
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    .line 16742
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->smsinvitetype_:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    goto :goto_0

    .line 16747
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 16748
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    move-result-object v0

    .line 16749
    if-eqz v0, :cond_0

    .line 16750
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    .line 16751
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->emailinvitetype_:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    goto :goto_0

    .line 16756
    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    .line 16757
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->message_:Ljava/lang/Object;

    goto :goto_0

    .line 16761
    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    .line 16762
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->sns_:Ljava/lang/Object;

    goto :goto_0

    .line 16766
    :sswitch_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    .line 16767
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->specifiedInvitePrompt_:Ljava/lang/Object;

    goto :goto_0

    .line 16717
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 16677
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 16696
    :goto_0
    return-object v0

    .line 16678
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 16679
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    .line 16681
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->hasSmsinvitetype()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 16682
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->getSmsinvitetype()Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->setSmsinvitetype(Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    .line 16684
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->hasEmailinvitetype()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 16685
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->getEmailinvitetype()Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->setEmailinvitetype(Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    .line 16687
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->hasMessage()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 16688
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->setMessage(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    .line 16690
    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->hasSns()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 16691
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->getSns()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->setSns(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    .line 16693
    :cond_5
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->hasSpecifiedInvitePrompt()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 16694
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->getSpecifiedInvitePrompt()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->setSpecifiedInvitePrompt(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    :cond_6
    move-object v0, p0

    .line 16696
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 16795
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 16797
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    .line 16798
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 16785
    if-nez p1, :cond_0

    .line 16786
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 16788
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 16790
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    .line 16791
    return-object p0
.end method

.method public setEmailinvitetype(Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 16852
    if-nez p1, :cond_0

    .line 16853
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 16855
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    .line 16856
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->emailinvitetype_:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    .line 16858
    return-object p0
.end method

.method public setMessage(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 16883
    if-nez p1, :cond_0

    .line 16884
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 16886
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    .line 16887
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->message_:Ljava/lang/Object;

    .line 16889
    return-object p0
.end method

.method setMessage(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 16898
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    .line 16899
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->message_:Ljava/lang/Object;

    .line 16901
    return-void
.end method

.method public setSmsinvitetype(Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 16828
    if-nez p1, :cond_0

    .line 16829
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 16831
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    .line 16832
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->smsinvitetype_:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    .line 16834
    return-object p0
.end method

.method public setSns(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 16919
    if-nez p1, :cond_0

    .line 16920
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 16922
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    .line 16923
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->sns_:Ljava/lang/Object;

    .line 16925
    return-object p0
.end method

.method setSns(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 16934
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    .line 16935
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->sns_:Ljava/lang/Object;

    .line 16937
    return-void
.end method

.method public setSpecifiedInvitePrompt(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 16955
    if-nez p1, :cond_0

    .line 16956
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 16958
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    .line 16959
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->specifiedInvitePrompt_:Ljava/lang/Object;

    .line 16961
    return-object p0
.end method

.method setSpecifiedInvitePrompt(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 16970
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->bitField0_:I

    .line 16971
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->specifiedInvitePrompt_:Ljava/lang/Object;

    .line 16973
    return-void
.end method
