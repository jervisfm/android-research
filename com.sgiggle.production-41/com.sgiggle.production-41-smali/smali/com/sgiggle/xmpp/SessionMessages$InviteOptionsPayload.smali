.class public final Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InviteOptionsPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final EMAILINVITETYPE_FIELD_NUMBER:I = 0x3

.field public static final MESSAGE_FIELD_NUMBER:I = 0x4

.field public static final SMSINVITETYPE_FIELD_NUMBER:I = 0x2

.field public static final SNS_FIELD_NUMBER:I = 0x5

.field public static final SPECIFIED_INVITE_PROMPT_FIELD_NUMBER:I = 0x6

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private emailinvitetype_:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private message_:Ljava/lang/Object;

.field private smsinvitetype_:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

.field private sns_:Ljava/lang/Object;

.field private specifiedInvitePrompt_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 16979
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    .line 16980
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->initFields()V

    .line 16981
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 16285
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 16433
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->memoizedIsInitialized:B

    .line 16473
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->memoizedSerializedSize:I

    .line 16286
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 16280
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 16287
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 16433
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->memoizedIsInitialized:B

    .line 16473
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->memoizedSerializedSize:I

    .line 16287
    return-void
.end method

.method static synthetic access$21102(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 16280
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$21202(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 16280
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->smsinvitetype_:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    return-object p1
.end method

.method static synthetic access$21302(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 16280
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->emailinvitetype_:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    return-object p1
.end method

.method static synthetic access$21402(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 16280
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->message_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$21502(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 16280
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->sns_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$21602(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 16280
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->specifiedInvitePrompt_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$21702(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 16280
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;
    .locals 1

    .prologue
    .line 16291
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    return-object v0
.end method

.method private getMessageBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 16350
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->message_:Ljava/lang/Object;

    .line 16351
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 16352
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 16354
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->message_:Ljava/lang/Object;

    .line 16357
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getSnsBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 16382
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->sns_:Ljava/lang/Object;

    .line 16383
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 16384
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 16386
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->sns_:Ljava/lang/Object;

    .line 16389
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getSpecifiedInvitePromptBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 16414
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->specifiedInvitePrompt_:Ljava/lang/Object;

    .line 16415
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 16416
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 16418
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->specifiedInvitePrompt_:Ljava/lang/Object;

    .line 16421
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 16426
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 16427
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;->CLIENT:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->smsinvitetype_:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    .line 16428
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;->CLIENT:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->emailinvitetype_:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    .line 16429
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->message_:Ljava/lang/Object;

    .line 16430
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->sns_:Ljava/lang/Object;

    .line 16431
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->specifiedInvitePrompt_:Ljava/lang/Object;

    .line 16432
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;
    .locals 1

    .prologue
    .line 16579
    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->access$20900()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 16582
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 16548
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    move-result-object v0

    .line 16549
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 16550
    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->access$20800(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    move-result-object v0

    .line 16552
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 16559
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    move-result-object v0

    .line 16560
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 16561
    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->access$20800(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    move-result-object v0

    .line 16563
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 16515
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->access$20800(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 16521
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->access$20800(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 16569
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->access$20800(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 16575
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->access$20800(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 16537
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->access$20800(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 16543
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->access$20800(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 16526
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->access$20800(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 16532
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;->access$20800(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 16306
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 16280
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;
    .locals 1

    .prologue
    .line 16295
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    return-object v0
.end method

.method public getEmailinvitetype()Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;
    .locals 1

    .prologue
    .line 16326
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->emailinvitetype_:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 16336
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->message_:Ljava/lang/Object;

    .line 16337
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 16338
    check-cast v0, Ljava/lang/String;

    .line 16346
    :goto_0
    return-object v0

    .line 16340
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 16342
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 16343
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 16344
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->message_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 16346
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 16475
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->memoizedSerializedSize:I

    .line 16476
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 16504
    :goto_0
    return v0

    .line 16478
    :cond_0
    const/4 v0, 0x0

    .line 16479
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 16480
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16483
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 16484
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->smsinvitetype_:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;->getNumber()I

    move-result v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 16487
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 16488
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->emailinvitetype_:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 16491
    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 16492
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->getMessageBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16495
    :cond_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 16496
    const/4 v1, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->getSnsBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16499
    :cond_5
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    .line 16500
    const/4 v1, 0x6

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->getSpecifiedInvitePromptBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16503
    :cond_6
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getSmsinvitetype()Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;
    .locals 1

    .prologue
    .line 16316
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->smsinvitetype_:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    return-object v0
.end method

.method public getSns()Ljava/lang/String;
    .locals 2

    .prologue
    .line 16368
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->sns_:Ljava/lang/Object;

    .line 16369
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 16370
    check-cast v0, Ljava/lang/String;

    .line 16378
    :goto_0
    return-object v0

    .line 16372
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 16374
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 16375
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 16376
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->sns_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 16378
    goto :goto_0
.end method

.method public getSpecifiedInvitePrompt()Ljava/lang/String;
    .locals 2

    .prologue
    .line 16400
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->specifiedInvitePrompt_:Ljava/lang/Object;

    .line 16401
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 16402
    check-cast v0, Ljava/lang/String;

    .line 16410
    :goto_0
    return-object v0

    .line 16404
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 16406
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 16407
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 16408
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->specifiedInvitePrompt_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 16410
    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 16303
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasEmailinvitetype()Z
    .locals 2

    .prologue
    .line 16323
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMessage()Z
    .locals 2

    .prologue
    .line 16333
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSmsinvitetype()Z
    .locals 2

    .prologue
    .line 16313
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSns()Z
    .locals 2

    .prologue
    .line 16365
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSpecifiedInvitePrompt()Z
    .locals 2

    .prologue
    .line 16397
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 16435
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->memoizedIsInitialized:B

    .line 16436
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 16447
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 16436
    goto :goto_0

    .line 16438
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 16439
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 16440
    goto :goto_0

    .line 16442
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 16443
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 16444
    goto :goto_0

    .line 16446
    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 16447
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 16280
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;
    .locals 1

    .prologue
    .line 16580
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 16280
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;
    .locals 1

    .prologue
    .line 16584
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 16509
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 16452
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->getSerializedSize()I

    .line 16453
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 16454
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 16456
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 16457
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->smsinvitetype_:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;->getNumber()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 16459
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 16460
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->emailinvitetype_:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 16462
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 16463
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->getMessageBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 16465
    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 16466
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->getSnsBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 16468
    :cond_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 16469
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->getSpecifiedInvitePromptBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 16471
    :cond_5
    return-void
.end method
