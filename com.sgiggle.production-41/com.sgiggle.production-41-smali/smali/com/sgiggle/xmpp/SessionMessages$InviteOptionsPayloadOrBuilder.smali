.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "InviteOptionsPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getEmailinvitetype()Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;
.end method

.method public abstract getMessage()Ljava/lang/String;
.end method

.method public abstract getSmsinvitetype()Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;
.end method

.method public abstract getSns()Ljava/lang/String;
.end method

.method public abstract getSpecifiedInvitePrompt()Ljava/lang/String;
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasEmailinvitetype()Z
.end method

.method public abstract hasMessage()Z
.end method

.method public abstract hasSmsinvitetype()Z
.end method

.method public abstract hasSns()Z
.end method

.method public abstract hasSpecifiedInvitePrompt()Z
.end method
