.class public final Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$InvitePayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$InvitePayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private correlationtoken_:Ljava/lang/Object;

.field private invitee_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Invitee;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 15920
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 16075
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 16118
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->correlationtoken_:Ljava/lang/Object;

    .line 16154
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->invitee_:Ljava/util/List;

    .line 15921
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->maybeForceBuilderInitialization()V

    .line 15922
    return-void
.end method

.method static synthetic access$20100(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 15915
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$20200()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;
    .locals 1

    .prologue
    .line 15915
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 15959
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;

    move-result-object v0

    .line 15960
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 15961
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 15964
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;
    .locals 1

    .prologue
    .line 15927
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureInviteeIsMutable()V
    .locals 2

    .prologue
    .line 16157
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 16158
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->invitee_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->invitee_:Ljava/util/List;

    .line 16159
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    .line 16161
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 15925
    return-void
.end method


# virtual methods
.method public addAllInvitee(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$Invitee;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;"
        }
    .end annotation

    .prologue
    .line 16224
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->ensureInviteeIsMutable()V

    .line 16225
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->invitee_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 16227
    return-object p0
.end method

.method public addInvitee(ILcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 16217
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->ensureInviteeIsMutable()V

    .line 16218
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->invitee_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 16220
    return-object p0
.end method

.method public addInvitee(ILcom/sgiggle/xmpp/SessionMessages$Invitee;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 16200
    if-nez p2, :cond_0

    .line 16201
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 16203
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->ensureInviteeIsMutable()V

    .line 16204
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->invitee_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 16206
    return-object p0
.end method

.method public addInvitee(Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 16210
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->ensureInviteeIsMutable()V

    .line 16211
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->invitee_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 16213
    return-object p0
.end method

.method public addInvitee(Lcom/sgiggle/xmpp/SessionMessages$Invitee;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 16190
    if-nez p1, :cond_0

    .line 16191
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 16193
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->ensureInviteeIsMutable()V

    .line 16194
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->invitee_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 16196
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 15915
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;
    .locals 2

    .prologue
    .line 15950
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;

    move-result-object v0

    .line 15951
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 15952
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 15954
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 15915
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;
    .locals 5

    .prologue
    .line 15968
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 15969
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    .line 15970
    const/4 v2, 0x0

    .line 15971
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 15972
    or-int/lit8 v2, v2, 0x1

    .line 15974
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->access$20402(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 15975
    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_2

    .line 15976
    or-int/lit8 v1, v2, 0x2

    .line 15978
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->correlationtoken_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->correlationtoken_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->access$20502(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 15979
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    .line 15980
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->invitee_:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->invitee_:Ljava/util/List;

    .line 15981
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    .line 15983
    :cond_1
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->invitee_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->invitee_:Ljava/util/List;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->access$20602(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;Ljava/util/List;)Ljava/util/List;

    .line 15984
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->access$20702(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;I)I

    .line 15985
    return-object v0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 15915
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 15915
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;
    .locals 1

    .prologue
    .line 15931
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 15932
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 15933
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    .line 15934
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->correlationtoken_:Ljava/lang/Object;

    .line 15935
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    .line 15936
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->invitee_:Ljava/util/List;

    .line 15937
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    .line 15938
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;
    .locals 1

    .prologue
    .line 16111
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 16113
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    .line 16114
    return-object p0
.end method

.method public clearCorrelationtoken()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;
    .locals 1

    .prologue
    .line 16142
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    .line 16143
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->getCorrelationtoken()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->correlationtoken_:Ljava/lang/Object;

    .line 16145
    return-object p0
.end method

.method public clearInvitee()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;
    .locals 1

    .prologue
    .line 16230
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->invitee_:Ljava/util/List;

    .line 16231
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    .line 16233
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 15915
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 15915
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 15915
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;
    .locals 2

    .prologue
    .line 15942
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 15915
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 16080
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCorrelationtoken()Ljava/lang/String;
    .locals 2

    .prologue
    .line 16123
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->correlationtoken_:Ljava/lang/Object;

    .line 16124
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 16125
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 16126
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->correlationtoken_:Ljava/lang/Object;

    .line 16129
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 15915
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 15915
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;
    .locals 1

    .prologue
    .line 15946
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;

    move-result-object v0

    return-object v0
.end method

.method public getInvitee(I)Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    .locals 1
    .parameter

    .prologue
    .line 16170
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->invitee_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    return-object v0
.end method

.method public getInviteeCount()I
    .locals 1

    .prologue
    .line 16167
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->invitee_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getInviteeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Invitee;",
            ">;"
        }
    .end annotation

    .prologue
    .line 16164
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->invitee_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 16077
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCorrelationtoken()Z
    .locals 2

    .prologue
    .line 16120
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 16010
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 16028
    :goto_0
    return v0

    .line 16014
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->hasCorrelationtoken()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 16016
    goto :goto_0

    .line 16018
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    .line 16020
    goto :goto_0

    :cond_2
    move v0, v2

    .line 16022
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->getInviteeCount()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 16023
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->getInvitee(I)Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_3

    move v0, v2

    .line 16025
    goto :goto_0

    .line 16022
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 16028
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 16099
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 16101
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 16107
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    .line 16108
    return-object p0

    .line 16104
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15915
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 15915
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15915
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 16036
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 16037
    sparse-switch v0, :sswitch_data_0

    .line 16042
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 16044
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 16040
    goto :goto_1

    .line 16049
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 16050
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 16051
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 16053
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 16054
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    goto :goto_0

    .line 16058
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    .line 16059
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->correlationtoken_:Ljava/lang/Object;

    goto :goto_0

    .line 16063
    :sswitch_3
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    .line 16064
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 16065
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->addInvitee(Lcom/sgiggle/xmpp/SessionMessages$Invitee;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    goto :goto_0

    .line 16037
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 15989
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 16006
    :goto_0
    return-object v0

    .line 15990
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 15991
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    .line 15993
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->hasCorrelationtoken()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 15994
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->getCorrelationtoken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->setCorrelationtoken(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    .line 15996
    :cond_2
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->invitee_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->access$20600(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 15997
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->invitee_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 15998
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->invitee_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->access$20600(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->invitee_:Ljava/util/List;

    .line 15999
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    :cond_3
    :goto_1
    move-object v0, p0

    .line 16006
    goto :goto_0

    .line 16001
    :cond_4
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->ensureInviteeIsMutable()V

    .line 16002
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->invitee_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->invitee_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->access$20600(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public removeInvitee(I)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 16236
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->ensureInviteeIsMutable()V

    .line 16237
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->invitee_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 16239
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 16093
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 16095
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    .line 16096
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 16083
    if-nez p1, :cond_0

    .line 16084
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 16086
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 16088
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    .line 16089
    return-object p0
.end method

.method public setCorrelationtoken(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 16133
    if-nez p1, :cond_0

    .line 16134
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 16136
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    .line 16137
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->correlationtoken_:Ljava/lang/Object;

    .line 16139
    return-object p0
.end method

.method setCorrelationtoken(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 16148
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->bitField0_:I

    .line 16149
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->correlationtoken_:Ljava/lang/Object;

    .line 16151
    return-void
.end method

.method public setInvitee(ILcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 16184
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->ensureInviteeIsMutable()V

    .line 16185
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->invitee_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 16187
    return-object p0
.end method

.method public setInvitee(ILcom/sgiggle/xmpp/SessionMessages$Invitee;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 16174
    if-nez p2, :cond_0

    .line 16175
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 16177
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->ensureInviteeIsMutable()V

    .line 16178
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->invitee_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 16180
    return-object p0
.end method
