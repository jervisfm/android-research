.class public final Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$InvitePayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InvitePayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CORRELATIONTOKEN_FIELD_NUMBER:I = 0x2

.field public static final INVITEE_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private correlationtoken_:Ljava/lang/Object;

.field private invitee_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Invitee;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 16246
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;

    .line 16247
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->initFields()V

    .line 16248
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 15691
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 15773
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->memoizedIsInitialized:B

    .line 15814
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->memoizedSerializedSize:I

    .line 15692
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 15686
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 15693
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 15773
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->memoizedIsInitialized:B

    .line 15814
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->memoizedSerializedSize:I

    .line 15693
    return-void
.end method

.method static synthetic access$20402(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 15686
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$20502(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 15686
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->correlationtoken_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$20600(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 15686
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->invitee_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$20602(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 15686
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->invitee_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$20702(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 15686
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->bitField0_:I

    return p1
.end method

.method private getCorrelationtokenBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 15736
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->correlationtoken_:Ljava/lang/Object;

    .line 15737
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 15738
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 15740
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->correlationtoken_:Ljava/lang/Object;

    .line 15743
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;
    .locals 1

    .prologue
    .line 15697
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 15769
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 15770
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->correlationtoken_:Ljava/lang/Object;

    .line 15771
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->invitee_:Ljava/util/List;

    .line 15772
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;
    .locals 1

    .prologue
    .line 15908
    #calls: Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->access$20200()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 15911
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15877
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    move-result-object v0

    .line 15878
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15879
    #calls: Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->access$20100(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;

    move-result-object v0

    .line 15881
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15888
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    move-result-object v0

    .line 15889
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15890
    #calls: Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->access$20100(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;

    move-result-object v0

    .line 15892
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 15844
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->access$20100(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 15850
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->access$20100(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15898
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->access$20100(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15904
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->access$20100(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15866
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->access$20100(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15872
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->access$20100(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 15855
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->access$20100(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 15861
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;->access$20100(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 15712
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCorrelationtoken()Ljava/lang/String;
    .locals 2

    .prologue
    .line 15722
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->correlationtoken_:Ljava/lang/Object;

    .line 15723
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 15724
    check-cast v0, Ljava/lang/String;

    .line 15732
    :goto_0
    return-object v0

    .line 15726
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 15728
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 15729
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 15730
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->correlationtoken_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 15732
    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 15686
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;
    .locals 1

    .prologue
    .line 15701
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;

    return-object v0
.end method

.method public getInvitee(I)Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    .locals 1
    .parameter

    .prologue
    .line 15761
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->invitee_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    return-object v0
.end method

.method public getInviteeCount()I
    .locals 1

    .prologue
    .line 15758
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->invitee_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getInviteeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Invitee;",
            ">;"
        }
    .end annotation

    .prologue
    .line 15751
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->invitee_:Ljava/util/List;

    return-object v0
.end method

.method public getInviteeOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$InviteeOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 15765
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->invitee_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteeOrBuilder;

    return-object v0
.end method

.method public getInviteeOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$InviteeOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 15755
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->invitee_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 15816
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->memoizedSerializedSize:I

    .line 15817
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 15833
    :goto_0
    return v0

    .line 15820
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 15821
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 15824
    :goto_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_1

    .line 15825
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->getCorrelationtokenBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    move v1, v2

    move v2, v0

    .line 15828
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->invitee_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 15829
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->invitee_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 15828
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 15832
    :cond_2
    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->memoizedSerializedSize:I

    move v0, v2

    .line 15833
    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 15709
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCorrelationtoken()Z
    .locals 2

    .prologue
    .line 15719
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 15775
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->memoizedIsInitialized:B

    .line 15776
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 15797
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 15776
    goto :goto_0

    .line 15778
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 15779
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 15780
    goto :goto_0

    .line 15782
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->hasCorrelationtoken()Z

    move-result v0

    if-nez v0, :cond_3

    .line 15783
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 15784
    goto :goto_0

    .line 15786
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    .line 15787
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 15788
    goto :goto_0

    :cond_4
    move v0, v2

    .line 15790
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->getInviteeCount()I

    move-result v1

    if-ge v0, v1, :cond_6

    .line 15791
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->getInvitee(I)Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_5

    .line 15792
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 15793
    goto :goto_0

    .line 15790
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 15796
    :cond_6
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->memoizedIsInitialized:B

    move v0, v3

    .line 15797
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 15686
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;
    .locals 1

    .prologue
    .line 15909
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 15686
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;
    .locals 1

    .prologue
    .line 15913
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;)Lcom/sgiggle/xmpp/SessionMessages$InvitePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 15838
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 15802
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->getSerializedSize()I

    .line 15803
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 15804
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 15806
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 15807
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->getCorrelationtokenBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 15809
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->invitee_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 15810
    const/4 v2, 0x3

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InvitePayload;->invitee_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 15809
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 15812
    :cond_2
    return-void
.end method
