.class public final Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private contact_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private hintMsg_:Ljava/lang/Object;

.field private inviterDisplayName_:Ljava/lang/Object;

.field private recommendationAlgorithm_:Ljava/lang/Object;

.field private specifiedContent_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 32892
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 33085
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 33128
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->contact_:Ljava/util/List;

    .line 33217
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->inviterDisplayName_:Ljava/lang/Object;

    .line 33253
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->hintMsg_:Ljava/lang/Object;

    .line 33289
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->specifiedContent_:Ljava/lang/Object;

    .line 33325
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 32893
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->maybeForceBuilderInitialization()V

    .line 32894
    return-void
.end method

.method static synthetic access$41300(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 32887
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$41400()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 1

    .prologue
    .line 32887
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 32937
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    move-result-object v0

    .line 32938
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 32939
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 32942
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 1

    .prologue
    .line 32899
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureContactIsMutable()V
    .locals 2

    .prologue
    .line 33131
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 33132
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->contact_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->contact_:Ljava/util/List;

    .line 33133
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 33135
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 32897
    return-void
.end method


# virtual methods
.method public addAllContact(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;"
        }
    .end annotation

    .prologue
    .line 33198
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->ensureContactIsMutable()V

    .line 33199
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->contact_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 33201
    return-object p0
.end method

.method public addContact(ILcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 33191
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->ensureContactIsMutable()V

    .line 33192
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->contact_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 33194
    return-object p0
.end method

.method public addContact(ILcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 33174
    if-nez p2, :cond_0

    .line 33175
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 33177
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->ensureContactIsMutable()V

    .line 33178
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->contact_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 33180
    return-object p0
.end method

.method public addContact(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 33184
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->ensureContactIsMutable()V

    .line 33185
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->contact_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 33187
    return-object p0
.end method

.method public addContact(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 33164
    if-nez p1, :cond_0

    .line 33165
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 33167
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->ensureContactIsMutable()V

    .line 33168
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->contact_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 33170
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 32887
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;
    .locals 2

    .prologue
    .line 32928
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    move-result-object v0

    .line 32929
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 32930
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 32932
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 32887
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;
    .locals 5

    .prologue
    .line 32946
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 32947
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 32948
    const/4 v2, 0x0

    .line 32949
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 32950
    or-int/lit8 v2, v2, 0x1

    .line 32952
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->access$41602(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 32953
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 32954
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->contact_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->contact_:Ljava/util/List;

    .line 32955
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x3

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 32957
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->contact_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->contact_:Ljava/util/List;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->access$41702(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;Ljava/util/List;)Ljava/util/List;

    .line 32958
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 32959
    or-int/lit8 v2, v2, 0x2

    .line 32961
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->inviterDisplayName_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->inviterDisplayName_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->access$41802(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32962
    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 32963
    or-int/lit8 v2, v2, 0x4

    .line 32965
    :cond_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->hintMsg_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->hintMsg_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->access$41902(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32966
    and-int/lit8 v3, v1, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 32967
    or-int/lit8 v2, v2, 0x8

    .line 32969
    :cond_4
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->specifiedContent_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->specifiedContent_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->access$42002(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32970
    and-int/lit8 v1, v1, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_5

    .line 32971
    or-int/lit8 v1, v2, 0x10

    .line 32973
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->recommendationAlgorithm_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->recommendationAlgorithm_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->access$42102(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32974
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->access$42202(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;I)I

    .line 32975
    return-object v0

    :cond_5
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 32887
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 32887
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 1

    .prologue
    .line 32903
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 32904
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 32905
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 32906
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->contact_:Ljava/util/List;

    .line 32907
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 32908
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->inviterDisplayName_:Ljava/lang/Object;

    .line 32909
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 32910
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->hintMsg_:Ljava/lang/Object;

    .line 32911
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 32912
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->specifiedContent_:Ljava/lang/Object;

    .line 32913
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 32914
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 32915
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 32916
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 1

    .prologue
    .line 33121
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 33123
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 33124
    return-object p0
.end method

.method public clearContact()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 1

    .prologue
    .line 33204
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->contact_:Ljava/util/List;

    .line 33205
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 33207
    return-object p0
.end method

.method public clearHintMsg()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 1

    .prologue
    .line 33277
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 33278
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getHintMsg()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->hintMsg_:Ljava/lang/Object;

    .line 33280
    return-object p0
.end method

.method public clearInviterDisplayName()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 1

    .prologue
    .line 33241
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 33242
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getInviterDisplayName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->inviterDisplayName_:Ljava/lang/Object;

    .line 33244
    return-object p0
.end method

.method public clearRecommendationAlgorithm()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 1

    .prologue
    .line 33349
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 33350
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getRecommendationAlgorithm()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 33352
    return-object p0
.end method

.method public clearSpecifiedContent()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 1

    .prologue
    .line 33313
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 33314
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getSpecifiedContent()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->specifiedContent_:Ljava/lang/Object;

    .line 33316
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 32887
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 32887
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 32887
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 2

    .prologue
    .line 32920
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 32887
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 33090
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getContact(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter

    .prologue
    .line 33144
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->contact_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getContactCount()I
    .locals 1

    .prologue
    .line 33141
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->contact_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getContactList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33138
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->contact_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 32887
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 32887
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;
    .locals 1

    .prologue
    .line 32924
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    move-result-object v0

    return-object v0
.end method

.method public getHintMsg()Ljava/lang/String;
    .locals 2

    .prologue
    .line 33258
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->hintMsg_:Ljava/lang/Object;

    .line 33259
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 33260
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 33261
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->hintMsg_:Ljava/lang/Object;

    .line 33264
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getInviterDisplayName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 33222
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->inviterDisplayName_:Ljava/lang/Object;

    .line 33223
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 33224
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 33225
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->inviterDisplayName_:Ljava/lang/Object;

    .line 33228
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getRecommendationAlgorithm()Ljava/lang/String;
    .locals 2

    .prologue
    .line 33330
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 33331
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 33332
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 33333
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 33336
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getSpecifiedContent()Ljava/lang/String;
    .locals 2

    .prologue
    .line 33294
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->specifiedContent_:Ljava/lang/Object;

    .line 33295
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 33296
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 33297
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->specifiedContent_:Ljava/lang/Object;

    .line 33300
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 33087
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHintMsg()Z
    .locals 2

    .prologue
    .line 33255
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasInviterDisplayName()Z
    .locals 2

    .prologue
    .line 33219
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRecommendationAlgorithm()Z
    .locals 2

    .prologue
    .line 33327
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSpecifiedContent()Z
    .locals 2

    .prologue
    .line 33291
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 33009
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 33023
    :goto_0
    return v0

    .line 33013
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 33015
    goto :goto_0

    :cond_1
    move v0, v2

    .line 33017
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->getContactCount()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 33018
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->getContact(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_2

    move v0, v2

    .line 33020
    goto :goto_0

    .line 33017
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 33023
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 33109
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 33111
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 33117
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 33118
    return-object p0

    .line 33114
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32887
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 32887
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32887
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 33031
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 33032
    sparse-switch v0, :sswitch_data_0

    .line 33037
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 33039
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 33035
    goto :goto_1

    .line 33044
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 33045
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 33046
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 33048
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 33049
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    goto :goto_0

    .line 33053
    :sswitch_2
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    .line 33054
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 33055
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->addContact(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    goto :goto_0

    .line 33059
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 33060
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->inviterDisplayName_:Ljava/lang/Object;

    goto :goto_0

    .line 33064
    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 33065
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->hintMsg_:Ljava/lang/Object;

    goto :goto_0

    .line 33069
    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 33070
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->specifiedContent_:Ljava/lang/Object;

    goto :goto_0

    .line 33074
    :sswitch_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 33075
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->recommendationAlgorithm_:Ljava/lang/Object;

    goto :goto_0

    .line 33032
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 32979
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 33005
    :goto_0
    return-object v0

    .line 32980
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 32981
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    .line 32983
    :cond_1
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->contact_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->access$41700(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 32984
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->contact_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 32985
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->contact_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->access$41700(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->contact_:Ljava/util/List;

    .line 32986
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 32993
    :cond_2
    :goto_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->hasInviterDisplayName()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 32994
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getInviterDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->setInviterDisplayName(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    .line 32996
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->hasHintMsg()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 32997
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getHintMsg()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->setHintMsg(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    .line 32999
    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->hasSpecifiedContent()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 33000
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getSpecifiedContent()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->setSpecifiedContent(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    .line 33002
    :cond_5
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->hasRecommendationAlgorithm()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 33003
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getRecommendationAlgorithm()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->setRecommendationAlgorithm(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    :cond_6
    move-object v0, p0

    .line 33005
    goto :goto_0

    .line 32988
    :cond_7
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->ensureContactIsMutable()V

    .line 32989
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->contact_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->contact_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->access$41700(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public removeContact(I)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 33210
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->ensureContactIsMutable()V

    .line 33211
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->contact_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 33213
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 33103
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 33105
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 33106
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 33093
    if-nez p1, :cond_0

    .line 33094
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 33096
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 33098
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 33099
    return-object p0
.end method

.method public setContact(ILcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 33158
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->ensureContactIsMutable()V

    .line 33159
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->contact_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 33161
    return-object p0
.end method

.method public setContact(ILcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 33148
    if-nez p2, :cond_0

    .line 33149
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 33151
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->ensureContactIsMutable()V

    .line 33152
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->contact_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 33154
    return-object p0
.end method

.method public setHintMsg(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 33268
    if-nez p1, :cond_0

    .line 33269
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 33271
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 33272
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->hintMsg_:Ljava/lang/Object;

    .line 33274
    return-object p0
.end method

.method setHintMsg(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 33283
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 33284
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->hintMsg_:Ljava/lang/Object;

    .line 33286
    return-void
.end method

.method public setInviterDisplayName(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 33232
    if-nez p1, :cond_0

    .line 33233
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 33235
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 33236
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->inviterDisplayName_:Ljava/lang/Object;

    .line 33238
    return-object p0
.end method

.method setInviterDisplayName(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 33247
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 33248
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->inviterDisplayName_:Ljava/lang/Object;

    .line 33250
    return-void
.end method

.method public setRecommendationAlgorithm(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 33340
    if-nez p1, :cond_0

    .line 33341
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 33343
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 33344
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 33346
    return-object p0
.end method

.method setRecommendationAlgorithm(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 33355
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 33356
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 33358
    return-void
.end method

.method public setSpecifiedContent(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 33304
    if-nez p1, :cond_0

    .line 33305
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 33307
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 33308
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->specifiedContent_:Ljava/lang/Object;

    .line 33310
    return-object p0
.end method

.method setSpecifiedContent(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 33319
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->bitField0_:I

    .line 33320
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->specifiedContent_:Ljava/lang/Object;

    .line 33322
    return-void
.end method
