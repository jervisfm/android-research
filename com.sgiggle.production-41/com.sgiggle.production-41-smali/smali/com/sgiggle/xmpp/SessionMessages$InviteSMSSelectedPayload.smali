.class public final Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InviteSMSSelectedPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CONTACT_FIELD_NUMBER:I = 0x2

.field public static final HINT_MSG_FIELD_NUMBER:I = 0x4

.field public static final INVITERDISPLAYNAME_FIELD_NUMBER:I = 0x3

.field public static final RECOMMENDATION_ALGORITHM_FIELD_NUMBER:I = 0x6

.field public static final SPECIFIED_CONTENT_FIELD_NUMBER:I = 0x5

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private contact_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private hintMsg_:Ljava/lang/Object;

.field private inviterDisplayName_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private recommendationAlgorithm_:Ljava/lang/Object;

.field private specifiedContent_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33364
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    .line 33365
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->initFields()V

    .line 33366
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 32547
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 32728
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->memoizedIsInitialized:B

    .line 32774
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->memoizedSerializedSize:I

    .line 32548
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 32542
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 32549
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 32728
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->memoizedIsInitialized:B

    .line 32774
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->memoizedSerializedSize:I

    .line 32549
    return-void
.end method

.method static synthetic access$41602(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 32542
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$41700(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 32542
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->contact_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$41702(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 32542
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->contact_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$41802(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 32542
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->inviterDisplayName_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$41902(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 32542
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->hintMsg_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$42002(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 32542
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->specifiedContent_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$42102(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 32542
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->recommendationAlgorithm_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$42202(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 32542
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;
    .locals 1

    .prologue
    .line 32553
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    return-object v0
.end method

.method private getHintMsgBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 32645
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->hintMsg_:Ljava/lang/Object;

    .line 32646
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 32647
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 32649
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->hintMsg_:Ljava/lang/Object;

    .line 32652
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getInviterDisplayNameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 32613
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->inviterDisplayName_:Ljava/lang/Object;

    .line 32614
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 32615
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 32617
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->inviterDisplayName_:Ljava/lang/Object;

    .line 32620
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getRecommendationAlgorithmBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 32709
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 32710
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 32711
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 32713
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 32716
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getSpecifiedContentBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 32677
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->specifiedContent_:Ljava/lang/Object;

    .line 32678
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 32679
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 32681
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->specifiedContent_:Ljava/lang/Object;

    .line 32684
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 32721
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 32722
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->contact_:Ljava/util/List;

    .line 32723
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->inviterDisplayName_:Ljava/lang/Object;

    .line 32724
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->hintMsg_:Ljava/lang/Object;

    .line 32725
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->specifiedContent_:Ljava/lang/Object;

    .line 32726
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 32727
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 1

    .prologue
    .line 32880
    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->access$41400()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 32883
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32849
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    .line 32850
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 32851
    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->access$41300(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    move-result-object v0

    .line 32853
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32860
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    .line 32861
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 32862
    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->access$41300(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    move-result-object v0

    .line 32864
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 32816
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->access$41300(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 32822
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->access$41300(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32870
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->access$41300(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32876
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->access$41300(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32838
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->access$41300(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32844
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->access$41300(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 32827
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->access$41300(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 32833
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;->access$41300(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 32568
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getContact(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter

    .prologue
    .line 32585
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->contact_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getContactCount()I
    .locals 1

    .prologue
    .line 32582
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->contact_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getContactList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32575
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->contact_:Ljava/util/List;

    return-object v0
.end method

.method public getContactOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 32589
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->contact_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;

    return-object v0
.end method

.method public getContactOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32579
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->contact_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 32542
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;
    .locals 1

    .prologue
    .line 32557
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    return-object v0
.end method

.method public getHintMsg()Ljava/lang/String;
    .locals 2

    .prologue
    .line 32631
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->hintMsg_:Ljava/lang/Object;

    .line 32632
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 32633
    check-cast v0, Ljava/lang/String;

    .line 32641
    :goto_0
    return-object v0

    .line 32635
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 32637
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 32638
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 32639
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->hintMsg_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 32641
    goto :goto_0
.end method

.method public getInviterDisplayName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 32599
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->inviterDisplayName_:Ljava/lang/Object;

    .line 32600
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 32601
    check-cast v0, Ljava/lang/String;

    .line 32609
    :goto_0
    return-object v0

    .line 32603
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 32605
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 32606
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 32607
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->inviterDisplayName_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 32609
    goto :goto_0
.end method

.method public getRecommendationAlgorithm()Ljava/lang/String;
    .locals 2

    .prologue
    .line 32695
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 32696
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 32697
    check-cast v0, Ljava/lang/String;

    .line 32705
    :goto_0
    return-object v0

    .line 32699
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 32701
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 32702
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 32703
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->recommendationAlgorithm_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 32705
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 32776
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->memoizedSerializedSize:I

    .line 32777
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 32805
    :goto_0
    return v0

    .line 32780
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 32781
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    :goto_1
    move v1, v2

    move v2, v0

    .line 32784
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->contact_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 32785
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->contact_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 32784
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 32788
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_5

    .line 32789
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getInviterDisplayNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/2addr v0, v2

    .line 32792
    :goto_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_2

    .line 32793
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getHintMsgBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 32796
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_3

    .line 32797
    const/4 v1, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getSpecifiedContentBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 32800
    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_4

    .line 32801
    const/4 v1, 0x6

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getRecommendationAlgorithmBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 32804
    :cond_4
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->memoizedSerializedSize:I

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_3

    :cond_6
    move v0, v2

    goto :goto_1
.end method

.method public getSpecifiedContent()Ljava/lang/String;
    .locals 2

    .prologue
    .line 32663
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->specifiedContent_:Ljava/lang/Object;

    .line 32664
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 32665
    check-cast v0, Ljava/lang/String;

    .line 32673
    :goto_0
    return-object v0

    .line 32667
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 32669
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 32670
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 32671
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->specifiedContent_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 32673
    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 32565
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHintMsg()Z
    .locals 2

    .prologue
    .line 32628
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasInviterDisplayName()Z
    .locals 2

    .prologue
    .line 32596
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRecommendationAlgorithm()Z
    .locals 2

    .prologue
    .line 32692
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSpecifiedContent()Z
    .locals 2

    .prologue
    .line 32660
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 32730
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->memoizedIsInitialized:B

    .line 32731
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 32748
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 32731
    goto :goto_0

    .line 32733
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 32734
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 32735
    goto :goto_0

    .line 32737
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 32738
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 32739
    goto :goto_0

    :cond_3
    move v0, v2

    .line 32741
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getContactCount()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 32742
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getContact(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_4

    .line 32743
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 32744
    goto :goto_0

    .line 32741
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 32747
    :cond_5
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 32748
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 32542
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 1

    .prologue
    .line 32881
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 32542
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;
    .locals 1

    .prologue
    .line 32885
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 32810
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 32753
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getSerializedSize()I

    .line 32754
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 32755
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 32757
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->contact_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 32758
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->contact_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 32757
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 32760
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 32761
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getInviterDisplayNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 32763
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_3

    .line 32764
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getHintMsgBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 32766
    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 32767
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getSpecifiedContentBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 32769
    :cond_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 32770
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getRecommendationAlgorithmBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 32772
    :cond_5
    return-void
.end method
