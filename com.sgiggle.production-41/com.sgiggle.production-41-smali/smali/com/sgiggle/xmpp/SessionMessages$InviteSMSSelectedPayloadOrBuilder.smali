.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "InviteSMSSelectedPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getContact(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
.end method

.method public abstract getContactCount()I
.end method

.method public abstract getContactList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getHintMsg()Ljava/lang/String;
.end method

.method public abstract getInviterDisplayName()Ljava/lang/String;
.end method

.method public abstract getRecommendationAlgorithm()Ljava/lang/String;
.end method

.method public abstract getSpecifiedContent()Ljava/lang/String;
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasHintMsg()Z
.end method

.method public abstract hasInviterDisplayName()Z
.end method

.method public abstract hasRecommendationAlgorithm()Z
.end method

.method public abstract hasSpecifiedContent()Z
.end method
