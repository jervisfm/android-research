.class public final Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private contacts_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private recommendationAlgorithm_:Ljava/lang/Object;

.field private recommendationDiffCount_:I

.field private selected_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 28260
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 28456
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 28499
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->contacts_:Ljava/util/List;

    .line 28588
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->selected_:Ljava/util/List;

    .line 28633
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 28261
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->maybeForceBuilderInitialization()V

    .line 28262
    return-void
.end method

.method static synthetic access$35800(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 28255
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$35900()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 1

    .prologue
    .line 28255
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 28303
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    move-result-object v0

    .line 28304
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 28305
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 28308
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 1

    .prologue
    .line 28267
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureContactsIsMutable()V
    .locals 2

    .prologue
    .line 28502
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 28503
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->contacts_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->contacts_:Ljava/util/List;

    .line 28504
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    .line 28506
    :cond_0
    return-void
.end method

.method private ensureSelectedIsMutable()V
    .locals 2

    .prologue
    .line 28590
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 28591
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->selected_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->selected_:Ljava/util/List;

    .line 28592
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    .line 28594
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 28265
    return-void
.end method


# virtual methods
.method public addAllContacts(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;"
        }
    .end annotation

    .prologue
    .line 28569
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->ensureContactsIsMutable()V

    .line 28570
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->contacts_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 28572
    return-object p0
.end method

.method public addAllSelected(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;"
        }
    .end annotation

    .prologue
    .line 28620
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->ensureSelectedIsMutable()V

    .line 28621
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->selected_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 28623
    return-object p0
.end method

.method public addContacts(ILcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 28562
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->ensureContactsIsMutable()V

    .line 28563
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->contacts_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 28565
    return-object p0
.end method

.method public addContacts(ILcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 28545
    if-nez p2, :cond_0

    .line 28546
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 28548
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->ensureContactsIsMutable()V

    .line 28549
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 28551
    return-object p0
.end method

.method public addContacts(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 28555
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->ensureContactsIsMutable()V

    .line 28556
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->contacts_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 28558
    return-object p0
.end method

.method public addContacts(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 28535
    if-nez p1, :cond_0

    .line 28536
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 28538
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->ensureContactsIsMutable()V

    .line 28539
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 28541
    return-object p0
.end method

.method public addSelected(Z)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 28613
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->ensureSelectedIsMutable()V

    .line 28614
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->selected_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 28616
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 28255
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;
    .locals 2

    .prologue
    .line 28294
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    move-result-object v0

    .line 28295
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 28296
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 28298
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 28255
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;
    .locals 5

    .prologue
    .line 28312
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 28313
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    .line 28314
    const/4 v2, 0x0

    .line 28315
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 28316
    or-int/lit8 v2, v2, 0x1

    .line 28318
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->access$36102(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 28319
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 28320
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->contacts_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->contacts_:Ljava/util/List;

    .line 28321
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x3

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    .line 28323
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->contacts_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->contacts_:Ljava/util/List;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->access$36202(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;Ljava/util/List;)Ljava/util/List;

    .line 28324
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 28325
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->selected_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->selected_:Ljava/util/List;

    .line 28326
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x5

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    .line 28328
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->selected_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->selected_:Ljava/util/List;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->access$36302(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;Ljava/util/List;)Ljava/util/List;

    .line 28329
    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 28330
    or-int/lit8 v2, v2, 0x2

    .line 28332
    :cond_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->recommendationAlgorithm_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->recommendationAlgorithm_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->access$36402(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28333
    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_4

    .line 28334
    or-int/lit8 v1, v2, 0x4

    .line 28336
    :goto_0
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->recommendationDiffCount_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->recommendationDiffCount_:I
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->access$36502(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;I)I

    .line 28337
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->access$36602(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;I)I

    .line 28338
    return-object v0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 28255
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 28255
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 1

    .prologue
    .line 28271
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 28272
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 28273
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    .line 28274
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->contacts_:Ljava/util/List;

    .line 28275
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    .line 28276
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->selected_:Ljava/util/List;

    .line 28277
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    .line 28278
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 28279
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    .line 28280
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->recommendationDiffCount_:I

    .line 28281
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    .line 28282
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 1

    .prologue
    .line 28492
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 28494
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    .line 28495
    return-object p0
.end method

.method public clearContacts()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 1

    .prologue
    .line 28575
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->contacts_:Ljava/util/List;

    .line 28576
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    .line 28578
    return-object p0
.end method

.method public clearRecommendationAlgorithm()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 1

    .prologue
    .line 28657
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    .line 28658
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->getRecommendationAlgorithm()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 28660
    return-object p0
.end method

.method public clearRecommendationDiffCount()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 1

    .prologue
    .line 28683
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    .line 28684
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->recommendationDiffCount_:I

    .line 28686
    return-object p0
.end method

.method public clearSelected()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 1

    .prologue
    .line 28626
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->selected_:Ljava/util/List;

    .line 28627
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    .line 28629
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 28255
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 28255
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 28255
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 2

    .prologue
    .line 28286
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 28255
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 28461
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getContacts(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter

    .prologue
    .line 28515
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getContactsCount()I
    .locals 1

    .prologue
    .line 28512
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getContactsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28509
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->contacts_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 28255
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 28255
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;
    .locals 1

    .prologue
    .line 28290
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    move-result-object v0

    return-object v0
.end method

.method public getRecommendationAlgorithm()Ljava/lang/String;
    .locals 2

    .prologue
    .line 28638
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 28639
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 28640
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 28641
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 28644
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getRecommendationDiffCount()I
    .locals 1

    .prologue
    .line 28674
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->recommendationDiffCount_:I

    return v0
.end method

.method public getSelected(I)Z
    .locals 1
    .parameter

    .prologue
    .line 28603
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->selected_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public getSelectedCount()I
    .locals 1

    .prologue
    .line 28600
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->selected_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getSelectedList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28597
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->selected_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 28458
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRecommendationAlgorithm()Z
    .locals 2

    .prologue
    .line 28635
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRecommendationDiffCount()Z
    .locals 2

    .prologue
    .line 28671
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 28376
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 28390
    :goto_0
    return v0

    .line 28380
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 28382
    goto :goto_0

    :cond_1
    move v0, v2

    .line 28384
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->getContactsCount()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 28385
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->getContacts(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_2

    move v0, v2

    .line 28387
    goto :goto_0

    .line 28384
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 28390
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 28480
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 28482
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 28488
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    .line 28489
    return-object p0

    .line 28485
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28255
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 28255
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28255
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28398
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 28399
    sparse-switch v0, :sswitch_data_0

    .line 28404
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 28406
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 28402
    goto :goto_1

    .line 28411
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 28412
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 28413
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 28415
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 28416
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    goto :goto_0

    .line 28420
    :sswitch_2
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    .line 28421
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 28422
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->addContacts(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    goto :goto_0

    .line 28426
    :sswitch_3
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->ensureSelectedIsMutable()V

    .line 28427
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->selected_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 28431
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readRawVarint32()I

    move-result v0

    .line 28432
    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->pushLimit(I)I

    move-result v0

    .line 28433
    :goto_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->getBytesUntilLimit()I

    move-result v1

    if-lez v1, :cond_2

    .line 28434
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->addSelected(Z)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    goto :goto_2

    .line 28436
    :cond_2
    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->popLimit(I)V

    goto :goto_0

    .line 28440
    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    .line 28441
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->recommendationAlgorithm_:Ljava/lang/Object;

    goto :goto_0

    .line 28445
    :sswitch_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    .line 28446
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->recommendationDiffCount_:I

    goto :goto_0

    .line 28399
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
        0x22 -> :sswitch_5
        0x28 -> :sswitch_6
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 28342
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 28372
    :goto_0
    return-object v0

    .line 28343
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 28344
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    .line 28346
    :cond_1
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->contacts_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->access$36200(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 28347
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 28348
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->contacts_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->access$36200(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->contacts_:Ljava/util/List;

    .line 28349
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    .line 28356
    :cond_2
    :goto_1
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->selected_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->access$36300(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 28357
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->selected_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 28358
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->selected_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->access$36300(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->selected_:Ljava/util/List;

    .line 28359
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    .line 28366
    :cond_3
    :goto_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->hasRecommendationAlgorithm()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 28367
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->getRecommendationAlgorithm()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->setRecommendationAlgorithm(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    .line 28369
    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->hasRecommendationDiffCount()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 28370
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->getRecommendationDiffCount()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->setRecommendationDiffCount(I)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    :cond_5
    move-object v0, p0

    .line 28372
    goto :goto_0

    .line 28351
    :cond_6
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->ensureContactsIsMutable()V

    .line 28352
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->contacts_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->contacts_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->access$36200(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 28361
    :cond_7
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->ensureSelectedIsMutable()V

    .line 28362
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->selected_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->selected_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->access$36300(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public removeContacts(I)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 28581
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->ensureContactsIsMutable()V

    .line 28582
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 28584
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 28474
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 28476
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    .line 28477
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 28464
    if-nez p1, :cond_0

    .line 28465
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 28467
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 28469
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    .line 28470
    return-object p0
.end method

.method public setContacts(ILcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 28529
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->ensureContactsIsMutable()V

    .line 28530
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->contacts_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 28532
    return-object p0
.end method

.method public setContacts(ILcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 28519
    if-nez p2, :cond_0

    .line 28520
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 28522
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->ensureContactsIsMutable()V

    .line 28523
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 28525
    return-object p0
.end method

.method public setRecommendationAlgorithm(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 28648
    if-nez p1, :cond_0

    .line 28649
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 28651
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    .line 28652
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 28654
    return-object p0
.end method

.method setRecommendationAlgorithm(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 28663
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    .line 28664
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 28666
    return-void
.end method

.method public setRecommendationDiffCount(I)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 28677
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->bitField0_:I

    .line 28678
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->recommendationDiffCount_:I

    .line 28680
    return-object p0
.end method

.method public setSelected(IZ)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 28607
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->ensureSelectedIsMutable()V

    .line 28608
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->selected_:Ljava/util/List;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 28610
    return-object p0
.end method
