.class public final Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InviteSMSSelectionPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CONTACTS_FIELD_NUMBER:I = 0x2

.field public static final RECOMMENDATION_ALGORITHM_FIELD_NUMBER:I = 0x4

.field public static final RECOMMENDATION_DIFF_COUNT_FIELD_NUMBER:I = 0x5

.field public static final SELECTED_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private contacts_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private recommendationAlgorithm_:Ljava/lang/Object;

.field private recommendationDiffCount_:I

.field private selected_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28693
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    .line 28694
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->initFields()V

    .line 28695
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 27993
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 28101
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->memoizedIsInitialized:B

    .line 28144
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->memoizedSerializedSize:I

    .line 27994
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 27988
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 27995
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 28101
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->memoizedIsInitialized:B

    .line 28144
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->memoizedSerializedSize:I

    .line 27995
    return-void
.end method

.method static synthetic access$36102(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 27988
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$36200(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 27988
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->contacts_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$36202(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 27988
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->contacts_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$36300(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 27988
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->selected_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$36302(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 27988
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->selected_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$36402(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 27988
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->recommendationAlgorithm_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$36502(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 27988
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->recommendationDiffCount_:I

    return p1
.end method

.method static synthetic access$36602(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 27988
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;
    .locals 1

    .prologue
    .line 27999
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    return-object v0
.end method

.method private getRecommendationAlgorithmBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 28073
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 28074
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 28075
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 28077
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 28080
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 28095
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 28096
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->contacts_:Ljava/util/List;

    .line 28097
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->selected_:Ljava/util/List;

    .line 28098
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 28099
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->recommendationDiffCount_:I

    .line 28100
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 1

    .prologue
    .line 28248
    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->access$35900()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 28251
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28217
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    move-result-object v0

    .line 28218
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 28219
    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->access$35800(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    move-result-object v0

    .line 28221
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28228
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    move-result-object v0

    .line 28229
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 28230
    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->access$35800(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    move-result-object v0

    .line 28232
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 28184
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->access$35800(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 28190
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->access$35800(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28238
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->access$35800(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28244
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->access$35800(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28206
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->access$35800(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28212
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->access$35800(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 28195
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->access$35800(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 28201
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;->access$35800(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 28014
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getContacts(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter

    .prologue
    .line 28031
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getContactsCount()I
    .locals 1

    .prologue
    .line 28028
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->contacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getContactsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28021
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->contacts_:Ljava/util/List;

    return-object v0
.end method

.method public getContactsOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 28035
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;

    return-object v0
.end method

.method public getContactsOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28025
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->contacts_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 27988
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;
    .locals 1

    .prologue
    .line 28003
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    return-object v0
.end method

.method public getRecommendationAlgorithm()Ljava/lang/String;
    .locals 2

    .prologue
    .line 28059
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 28060
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 28061
    check-cast v0, Ljava/lang/String;

    .line 28069
    :goto_0
    return-object v0

    .line 28063
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 28065
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 28066
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 28067
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->recommendationAlgorithm_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 28069
    goto :goto_0
.end method

.method public getRecommendationDiffCount()I
    .locals 1

    .prologue
    .line 28091
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->recommendationDiffCount_:I

    return v0
.end method

.method public getSelected(I)Z
    .locals 1
    .parameter

    .prologue
    .line 28049
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->selected_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public getSelectedCount()I
    .locals 1

    .prologue
    .line 28046
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->selected_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getSelectedList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28043
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->selected_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 28146
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->memoizedSerializedSize:I

    .line 28147
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 28173
    :goto_0
    return v0

    .line 28150
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 28151
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    :goto_1
    move v1, v2

    move v2, v0

    .line 28154
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->contacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 28155
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->contacts_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 28154
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 28160
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->getSelectedList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1

    .line 28161
    add-int/2addr v0, v2

    .line 28162
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->getSelectedList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 28164
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_2

    .line 28165
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->getRecommendationAlgorithmBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28168
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_3

    .line 28169
    const/4 v1, 0x5

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->recommendationDiffCount_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 28172
    :cond_3
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->memoizedSerializedSize:I

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 28011
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRecommendationAlgorithm()Z
    .locals 2

    .prologue
    .line 28056
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRecommendationDiffCount()Z
    .locals 2

    .prologue
    .line 28088
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 28103
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->memoizedIsInitialized:B

    .line 28104
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 28121
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 28104
    goto :goto_0

    .line 28106
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 28107
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 28108
    goto :goto_0

    .line 28110
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 28111
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 28112
    goto :goto_0

    :cond_3
    move v0, v2

    .line 28114
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->getContactsCount()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 28115
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->getContacts(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_4

    .line 28116
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 28117
    goto :goto_0

    .line 28114
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 28120
    :cond_5
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 28121
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 27988
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 1

    .prologue
    .line 28249
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 27988
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;
    .locals 1

    .prologue
    .line 28253
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 28178
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 28126
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->getSerializedSize()I

    .line 28127
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 28128
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    move v1, v2

    .line 28130
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->contacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 28131
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->contacts_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 28130
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v1, v2

    .line 28133
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->selected_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 28134
    const/4 v2, 0x3

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->selected_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 28133
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 28136
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_3

    .line 28137
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->getRecommendationAlgorithmBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 28139
    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_4

    .line 28140
    const/4 v0, 0x5

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->recommendationDiffCount_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 28142
    :cond_4
    return-void
.end method
