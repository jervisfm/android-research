.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "InviteSMSSelectionPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getContacts(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
.end method

.method public abstract getContactsCount()I
.end method

.method public abstract getContactsList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getRecommendationAlgorithm()Ljava/lang/String;
.end method

.method public abstract getRecommendationDiffCount()I
.end method

.method public abstract getSelected(I)Z
.end method

.method public abstract getSelectedCount()I
.end method

.method public abstract getSelectedList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasRecommendationAlgorithm()Z
.end method

.method public abstract hasRecommendationDiffCount()Z
.end method
