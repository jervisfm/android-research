.class public final Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private correlationtoken_:Ljava/lang/Object;

.field private invitee_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Invitee;",
            ">;"
        }
    .end annotation
.end field

.field private lang_:Ljava/lang/Object;

.field private messagebody_:Ljava/lang/Object;

.field private messagesubject_:Ljava/lang/Object;

.field private recommendationAlgorithm_:Ljava/lang/Object;

.field private success_:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 19742
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 19963
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 20006
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->lang_:Ljava/lang/Object;

    .line 20042
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->invitee_:Ljava/util/List;

    .line 20131
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->correlationtoken_:Ljava/lang/Object;

    .line 20167
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->messagesubject_:Ljava/lang/Object;

    .line 20203
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->messagebody_:Ljava/lang/Object;

    .line 20260
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 19743
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->maybeForceBuilderInitialization()V

    .line 19744
    return-void
.end method

.method static synthetic access$24500(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 19737
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$24600()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 1

    .prologue
    .line 19737
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 19791
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v0

    .line 19792
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 19793
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 19796
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 1

    .prologue
    .line 19749
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureInviteeIsMutable()V
    .locals 2

    .prologue
    .line 20045
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 20046
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->invitee_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->invitee_:Ljava/util/List;

    .line 20047
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 20049
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 19747
    return-void
.end method


# virtual methods
.method public addAllInvitee(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$Invitee;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;"
        }
    .end annotation

    .prologue
    .line 20112
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->ensureInviteeIsMutable()V

    .line 20113
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->invitee_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 20115
    return-object p0
.end method

.method public addInvitee(ILcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 20105
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->ensureInviteeIsMutable()V

    .line 20106
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->invitee_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 20108
    return-object p0
.end method

.method public addInvitee(ILcom/sgiggle/xmpp/SessionMessages$Invitee;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 20088
    if-nez p2, :cond_0

    .line 20089
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 20091
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->ensureInviteeIsMutable()V

    .line 20092
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->invitee_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 20094
    return-object p0
.end method

.method public addInvitee(Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 20098
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->ensureInviteeIsMutable()V

    .line 20099
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->invitee_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 20101
    return-object p0
.end method

.method public addInvitee(Lcom/sgiggle/xmpp/SessionMessages$Invitee;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 20078
    if-nez p1, :cond_0

    .line 20079
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 20081
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->ensureInviteeIsMutable()V

    .line 20082
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->invitee_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 20084
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 19737
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;
    .locals 2

    .prologue
    .line 19782
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v0

    .line 19783
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 19784
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 19786
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 19737
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;
    .locals 5

    .prologue
    .line 19800
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 19801
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 19802
    const/4 v2, 0x0

    .line 19803
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 19804
    or-int/lit8 v2, v2, 0x1

    .line 19806
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->access$24802(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 19807
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 19808
    or-int/lit8 v2, v2, 0x2

    .line 19810
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->lang_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->lang_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->access$24902(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19811
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 19812
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->invitee_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->invitee_:Ljava/util/List;

    .line 19813
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x5

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 19815
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->invitee_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->invitee_:Ljava/util/List;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->access$25002(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;Ljava/util/List;)Ljava/util/List;

    .line 19816
    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 19817
    or-int/lit8 v2, v2, 0x4

    .line 19819
    :cond_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->correlationtoken_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->correlationtoken_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->access$25102(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19820
    and-int/lit8 v3, v1, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 19821
    or-int/lit8 v2, v2, 0x8

    .line 19823
    :cond_4
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->messagesubject_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->messagesubject_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->access$25202(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19824
    and-int/lit8 v3, v1, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 19825
    or-int/lit8 v2, v2, 0x10

    .line 19827
    :cond_5
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->messagebody_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->messagebody_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->access$25302(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19828
    and-int/lit8 v3, v1, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    .line 19829
    or-int/lit8 v2, v2, 0x20

    .line 19831
    :cond_6
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->success_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->success_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->access$25402(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;Z)Z

    .line 19832
    and-int/lit16 v1, v1, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_7

    .line 19833
    or-int/lit8 v1, v2, 0x40

    .line 19835
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->recommendationAlgorithm_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->recommendationAlgorithm_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->access$25502(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19836
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->access$25602(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;I)I

    .line 19837
    return-object v0

    :cond_7
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 19737
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 19737
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 1

    .prologue
    .line 19753
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 19754
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 19755
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 19756
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->lang_:Ljava/lang/Object;

    .line 19757
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 19758
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->invitee_:Ljava/util/List;

    .line 19759
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 19760
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->correlationtoken_:Ljava/lang/Object;

    .line 19761
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 19762
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->messagesubject_:Ljava/lang/Object;

    .line 19763
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 19764
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->messagebody_:Ljava/lang/Object;

    .line 19765
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 19766
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->success_:Z

    .line 19767
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 19768
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 19769
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 19770
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 1

    .prologue
    .line 19999
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 20001
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 20002
    return-object p0
.end method

.method public clearCorrelationtoken()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 1

    .prologue
    .line 20155
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 20156
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getCorrelationtoken()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->correlationtoken_:Ljava/lang/Object;

    .line 20158
    return-object p0
.end method

.method public clearInvitee()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 1

    .prologue
    .line 20118
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->invitee_:Ljava/util/List;

    .line 20119
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 20121
    return-object p0
.end method

.method public clearLang()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 1

    .prologue
    .line 20030
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 20031
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getLang()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->lang_:Ljava/lang/Object;

    .line 20033
    return-object p0
.end method

.method public clearMessagebody()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 1

    .prologue
    .line 20227
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 20228
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getMessagebody()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->messagebody_:Ljava/lang/Object;

    .line 20230
    return-object p0
.end method

.method public clearMessagesubject()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 1

    .prologue
    .line 20191
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 20192
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getMessagesubject()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->messagesubject_:Ljava/lang/Object;

    .line 20194
    return-object p0
.end method

.method public clearRecommendationAlgorithm()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 1

    .prologue
    .line 20284
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 20285
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getRecommendationAlgorithm()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 20287
    return-object p0
.end method

.method public clearSuccess()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 1

    .prologue
    .line 20253
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 20254
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->success_:Z

    .line 20256
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 19737
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 19737
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 19737
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 2

    .prologue
    .line 19774
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 19737
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 19968
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCorrelationtoken()Ljava/lang/String;
    .locals 2

    .prologue
    .line 20136
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->correlationtoken_:Ljava/lang/Object;

    .line 20137
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 20138
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 20139
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->correlationtoken_:Ljava/lang/Object;

    .line 20142
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 19737
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 19737
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;
    .locals 1

    .prologue
    .line 19778
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v0

    return-object v0
.end method

.method public getInvitee(I)Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    .locals 1
    .parameter

    .prologue
    .line 20058
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->invitee_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    return-object v0
.end method

.method public getInviteeCount()I
    .locals 1

    .prologue
    .line 20055
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->invitee_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getInviteeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Invitee;",
            ">;"
        }
    .end annotation

    .prologue
    .line 20052
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->invitee_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getLang()Ljava/lang/String;
    .locals 2

    .prologue
    .line 20011
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->lang_:Ljava/lang/Object;

    .line 20012
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 20013
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 20014
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->lang_:Ljava/lang/Object;

    .line 20017
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getMessagebody()Ljava/lang/String;
    .locals 2

    .prologue
    .line 20208
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->messagebody_:Ljava/lang/Object;

    .line 20209
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 20210
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 20211
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->messagebody_:Ljava/lang/Object;

    .line 20214
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getMessagesubject()Ljava/lang/String;
    .locals 2

    .prologue
    .line 20172
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->messagesubject_:Ljava/lang/Object;

    .line 20173
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 20174
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 20175
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->messagesubject_:Ljava/lang/Object;

    .line 20178
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getRecommendationAlgorithm()Ljava/lang/String;
    .locals 2

    .prologue
    .line 20265
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 20266
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 20267
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 20268
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 20271
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getSuccess()Z
    .locals 1

    .prologue
    .line 20244
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->success_:Z

    return v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 19965
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCorrelationtoken()Z
    .locals 2

    .prologue
    .line 20133
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLang()Z
    .locals 2

    .prologue
    .line 20008
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMessagebody()Z
    .locals 2

    .prologue
    .line 20205
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMessagesubject()Z
    .locals 2

    .prologue
    .line 20169
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRecommendationAlgorithm()Z
    .locals 2

    .prologue
    .line 20262
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSuccess()Z
    .locals 2

    .prologue
    .line 20241
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 19877
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 19891
    :goto_0
    return v0

    .line 19881
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 19883
    goto :goto_0

    :cond_1
    move v0, v2

    .line 19885
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->getInviteeCount()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 19886
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->getInvitee(I)Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_2

    move v0, v2

    .line 19888
    goto :goto_0

    .line 19885
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 19891
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 19987
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 19989
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 19995
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 19996
    return-object p0

    .line 19992
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19737
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 19737
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19737
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19899
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 19900
    sparse-switch v0, :sswitch_data_0

    .line 19905
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 19907
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 19903
    goto :goto_1

    .line 19912
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 19913
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 19914
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 19916
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 19917
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    goto :goto_0

    .line 19921
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 19922
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->lang_:Ljava/lang/Object;

    goto :goto_0

    .line 19926
    :sswitch_3
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    .line 19927
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 19928
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->addInvitee(Lcom/sgiggle/xmpp/SessionMessages$Invitee;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    goto :goto_0

    .line 19932
    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 19933
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->correlationtoken_:Ljava/lang/Object;

    goto :goto_0

    .line 19937
    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 19938
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->messagesubject_:Ljava/lang/Object;

    goto :goto_0

    .line 19942
    :sswitch_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 19943
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->messagebody_:Ljava/lang/Object;

    goto :goto_0

    .line 19947
    :sswitch_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 19948
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->success_:Z

    goto :goto_0

    .line 19952
    :sswitch_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 19953
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->recommendationAlgorithm_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 19900
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 19841
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 19873
    :goto_0
    return-object v0

    .line 19842
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 19843
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    .line 19845
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->hasLang()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 19846
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getLang()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->setLang(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    .line 19848
    :cond_2
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->invitee_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->access$25000(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 19849
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->invitee_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 19850
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->invitee_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->access$25000(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->invitee_:Ljava/util/List;

    .line 19851
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 19858
    :cond_3
    :goto_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->hasCorrelationtoken()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 19859
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getCorrelationtoken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->setCorrelationtoken(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    .line 19861
    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->hasMessagesubject()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 19862
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getMessagesubject()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->setMessagesubject(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    .line 19864
    :cond_5
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->hasMessagebody()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 19865
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getMessagebody()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->setMessagebody(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    .line 19867
    :cond_6
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->hasSuccess()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 19868
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getSuccess()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->setSuccess(Z)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    .line 19870
    :cond_7
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->hasRecommendationAlgorithm()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 19871
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getRecommendationAlgorithm()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->setRecommendationAlgorithm(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    :cond_8
    move-object v0, p0

    .line 19873
    goto :goto_0

    .line 19853
    :cond_9
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->ensureInviteeIsMutable()V

    .line 19854
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->invitee_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->invitee_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->access$25000(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public removeInvitee(I)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 20124
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->ensureInviteeIsMutable()V

    .line 20125
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->invitee_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 20127
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 19981
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 19983
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 19984
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 19971
    if-nez p1, :cond_0

    .line 19972
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 19974
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 19976
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 19977
    return-object p0
.end method

.method public setCorrelationtoken(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 20146
    if-nez p1, :cond_0

    .line 20147
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 20149
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 20150
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->correlationtoken_:Ljava/lang/Object;

    .line 20152
    return-object p0
.end method

.method setCorrelationtoken(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 20161
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 20162
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->correlationtoken_:Ljava/lang/Object;

    .line 20164
    return-void
.end method

.method public setInvitee(ILcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 20072
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->ensureInviteeIsMutable()V

    .line 20073
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->invitee_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 20075
    return-object p0
.end method

.method public setInvitee(ILcom/sgiggle/xmpp/SessionMessages$Invitee;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 20062
    if-nez p2, :cond_0

    .line 20063
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 20065
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->ensureInviteeIsMutable()V

    .line 20066
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->invitee_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 20068
    return-object p0
.end method

.method public setLang(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 20021
    if-nez p1, :cond_0

    .line 20022
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 20024
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 20025
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->lang_:Ljava/lang/Object;

    .line 20027
    return-object p0
.end method

.method setLang(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 20036
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 20037
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->lang_:Ljava/lang/Object;

    .line 20039
    return-void
.end method

.method public setMessagebody(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 20218
    if-nez p1, :cond_0

    .line 20219
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 20221
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 20222
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->messagebody_:Ljava/lang/Object;

    .line 20224
    return-object p0
.end method

.method setMessagebody(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 20233
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 20234
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->messagebody_:Ljava/lang/Object;

    .line 20236
    return-void
.end method

.method public setMessagesubject(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 20182
    if-nez p1, :cond_0

    .line 20183
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 20185
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 20186
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->messagesubject_:Ljava/lang/Object;

    .line 20188
    return-object p0
.end method

.method setMessagesubject(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 20197
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 20198
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->messagesubject_:Ljava/lang/Object;

    .line 20200
    return-void
.end method

.method public setRecommendationAlgorithm(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 20275
    if-nez p1, :cond_0

    .line 20276
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 20278
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 20279
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 20281
    return-object p0
.end method

.method setRecommendationAlgorithm(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 20290
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 20291
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 20293
    return-void
.end method

.method public setSuccess(Z)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 20247
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->bitField0_:I

    .line 20248
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->success_:Z

    .line 20250
    return-object p0
.end method
