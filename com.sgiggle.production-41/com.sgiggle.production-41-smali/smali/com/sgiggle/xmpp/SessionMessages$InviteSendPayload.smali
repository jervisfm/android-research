.class public final Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InviteSendPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CORRELATIONTOKEN_FIELD_NUMBER:I = 0x4

.field public static final INVITEE_FIELD_NUMBER:I = 0x3

.field public static final LANG_FIELD_NUMBER:I = 0x2

.field public static final MESSAGEBODY_FIELD_NUMBER:I = 0x6

.field public static final MESSAGESUBJECT_FIELD_NUMBER:I = 0x5

.field public static final RECOMMENDATION_ALGORITHM_FIELD_NUMBER:I = 0x8

.field public static final SUCCESS_FIELD_NUMBER:I = 0x7

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private correlationtoken_:Ljava/lang/Object;

.field private invitee_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Invitee;",
            ">;"
        }
    .end annotation
.end field

.field private lang_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private messagebody_:Ljava/lang/Object;

.field private messagesubject_:Ljava/lang/Object;

.field private recommendationAlgorithm_:Ljava/lang/Object;

.field private success_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20299
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    .line 20300
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->initFields()V

    .line 20301
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 19339
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 19564
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->memoizedIsInitialized:B

    .line 19616
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->memoizedSerializedSize:I

    .line 19340
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 19334
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 19341
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 19564
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->memoizedIsInitialized:B

    .line 19616
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->memoizedSerializedSize:I

    .line 19341
    return-void
.end method

.method static synthetic access$24802(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 19334
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$24902(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 19334
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->lang_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$25000(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 19334
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->invitee_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$25002(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 19334
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->invitee_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$25102(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 19334
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->correlationtoken_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$25202(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 19334
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->messagesubject_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$25302(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 19334
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->messagebody_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$25402(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 19334
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->success_:Z

    return p1
.end method

.method static synthetic access$25502(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 19334
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->recommendationAlgorithm_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$25602(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 19334
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->bitField0_:I

    return p1
.end method

.method private getCorrelationtokenBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 19437
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->correlationtoken_:Ljava/lang/Object;

    .line 19438
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 19439
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 19441
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->correlationtoken_:Ljava/lang/Object;

    .line 19444
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;
    .locals 1

    .prologue
    .line 19345
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    return-object v0
.end method

.method private getLangBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 19384
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->lang_:Ljava/lang/Object;

    .line 19385
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 19386
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 19388
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->lang_:Ljava/lang/Object;

    .line 19391
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getMessagebodyBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 19501
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->messagebody_:Ljava/lang/Object;

    .line 19502
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 19503
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 19505
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->messagebody_:Ljava/lang/Object;

    .line 19508
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getMessagesubjectBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 19469
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->messagesubject_:Ljava/lang/Object;

    .line 19470
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 19471
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 19473
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->messagesubject_:Ljava/lang/Object;

    .line 19476
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getRecommendationAlgorithmBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 19543
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 19544
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 19545
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 19547
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 19550
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 19555
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 19556
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->lang_:Ljava/lang/Object;

    .line 19557
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->invitee_:Ljava/util/List;

    .line 19558
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->correlationtoken_:Ljava/lang/Object;

    .line 19559
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->messagesubject_:Ljava/lang/Object;

    .line 19560
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->messagebody_:Ljava/lang/Object;

    .line 19561
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->success_:Z

    .line 19562
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 19563
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 1

    .prologue
    .line 19730
    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->access$24600()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 19733
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19699
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    .line 19700
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 19701
    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->access$24500(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v0

    .line 19703
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19710
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    .line 19711
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 19712
    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->access$24500(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v0

    .line 19714
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 19666
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->access$24500(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 19672
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->access$24500(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19720
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->access$24500(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19726
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->access$24500(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19688
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->access$24500(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19694
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->access$24500(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 19677
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->access$24500(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 19683
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;->access$24500(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 19360
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCorrelationtoken()Ljava/lang/String;
    .locals 2

    .prologue
    .line 19423
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->correlationtoken_:Ljava/lang/Object;

    .line 19424
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 19425
    check-cast v0, Ljava/lang/String;

    .line 19433
    :goto_0
    return-object v0

    .line 19427
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 19429
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 19430
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 19431
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->correlationtoken_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 19433
    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 19334
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;
    .locals 1

    .prologue
    .line 19349
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;

    return-object v0
.end method

.method public getInvitee(I)Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    .locals 1
    .parameter

    .prologue
    .line 19409
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->invitee_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    return-object v0
.end method

.method public getInviteeCount()I
    .locals 1

    .prologue
    .line 19406
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->invitee_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getInviteeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Invitee;",
            ">;"
        }
    .end annotation

    .prologue
    .line 19399
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->invitee_:Ljava/util/List;

    return-object v0
.end method

.method public getInviteeOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$InviteeOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 19413
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->invitee_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteeOrBuilder;

    return-object v0
.end method

.method public getInviteeOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$InviteeOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 19403
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->invitee_:Ljava/util/List;

    return-object v0
.end method

.method public getLang()Ljava/lang/String;
    .locals 2

    .prologue
    .line 19370
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->lang_:Ljava/lang/Object;

    .line 19371
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 19372
    check-cast v0, Ljava/lang/String;

    .line 19380
    :goto_0
    return-object v0

    .line 19374
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 19376
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 19377
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 19378
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->lang_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 19380
    goto :goto_0
.end method

.method public getMessagebody()Ljava/lang/String;
    .locals 2

    .prologue
    .line 19487
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->messagebody_:Ljava/lang/Object;

    .line 19488
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 19489
    check-cast v0, Ljava/lang/String;

    .line 19497
    :goto_0
    return-object v0

    .line 19491
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 19493
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 19494
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 19495
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->messagebody_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 19497
    goto :goto_0
.end method

.method public getMessagesubject()Ljava/lang/String;
    .locals 2

    .prologue
    .line 19455
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->messagesubject_:Ljava/lang/Object;

    .line 19456
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 19457
    check-cast v0, Ljava/lang/String;

    .line 19465
    :goto_0
    return-object v0

    .line 19459
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 19461
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 19462
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 19463
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->messagesubject_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 19465
    goto :goto_0
.end method

.method public getRecommendationAlgorithm()Ljava/lang/String;
    .locals 2

    .prologue
    .line 19529
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->recommendationAlgorithm_:Ljava/lang/Object;

    .line 19530
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 19531
    check-cast v0, Ljava/lang/String;

    .line 19539
    :goto_0
    return-object v0

    .line 19533
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 19535
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 19536
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 19537
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->recommendationAlgorithm_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 19539
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19618
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->memoizedSerializedSize:I

    .line 19619
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 19655
    :goto_0
    return v0

    .line 19622
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_8

    .line 19623
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 19626
    :goto_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_1

    .line 19627
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getLangBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    move v1, v2

    move v2, v0

    .line 19630
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->invitee_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 19631
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->invitee_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 19630
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 19634
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_7

    .line 19635
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getCorrelationtokenBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/2addr v0, v2

    .line 19638
    :goto_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v6, :cond_3

    .line 19639
    const/4 v1, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getMessagesubjectBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19642
    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_4

    .line 19643
    const/4 v1, 0x6

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getMessagebodyBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19646
    :cond_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_5

    .line 19647
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->success_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 19650
    :cond_5
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_6

    .line 19651
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getRecommendationAlgorithmBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v6, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19654
    :cond_6
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->memoizedSerializedSize:I

    goto/16 :goto_0

    :cond_7
    move v0, v2

    goto :goto_3

    :cond_8
    move v0, v2

    goto/16 :goto_1
.end method

.method public getSuccess()Z
    .locals 1

    .prologue
    .line 19519
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->success_:Z

    return v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 19357
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCorrelationtoken()Z
    .locals 2

    .prologue
    .line 19420
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLang()Z
    .locals 2

    .prologue
    .line 19367
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMessagebody()Z
    .locals 2

    .prologue
    .line 19484
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMessagesubject()Z
    .locals 2

    .prologue
    .line 19452
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRecommendationAlgorithm()Z
    .locals 2

    .prologue
    .line 19526
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSuccess()Z
    .locals 2

    .prologue
    .line 19516
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19566
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->memoizedIsInitialized:B

    .line 19567
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 19584
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 19567
    goto :goto_0

    .line 19569
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 19570
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 19571
    goto :goto_0

    .line 19573
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 19574
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 19575
    goto :goto_0

    :cond_3
    move v0, v2

    .line 19577
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getInviteeCount()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 19578
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getInvitee(I)Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_4

    .line 19579
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 19580
    goto :goto_0

    .line 19577
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 19583
    :cond_5
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 19584
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 19334
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 1

    .prologue
    .line 19731
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 19334
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;
    .locals 1

    .prologue
    .line 19735
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 19660
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 19589
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getSerializedSize()I

    .line 19590
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 19591
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 19593
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 19594
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getLangBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 19596
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->invitee_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 19597
    const/4 v2, 0x3

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->invitee_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 19596
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 19599
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_3

    .line 19600
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getCorrelationtokenBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 19602
    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_4

    .line 19603
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getMessagesubjectBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 19605
    :cond_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 19606
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getMessagebodyBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 19608
    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 19609
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->success_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 19611
    :cond_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_7

    .line 19612
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayload;->getRecommendationAlgorithmBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 19614
    :cond_7
    return-void
.end method
