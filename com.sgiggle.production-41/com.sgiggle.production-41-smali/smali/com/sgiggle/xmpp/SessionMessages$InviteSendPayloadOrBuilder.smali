.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$InviteSendPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "InviteSendPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getCorrelationtoken()Ljava/lang/String;
.end method

.method public abstract getInvitee(I)Lcom/sgiggle/xmpp/SessionMessages$Invitee;
.end method

.method public abstract getInviteeCount()I
.end method

.method public abstract getInviteeList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Invitee;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getLang()Ljava/lang/String;
.end method

.method public abstract getMessagebody()Ljava/lang/String;
.end method

.method public abstract getMessagesubject()Ljava/lang/String;
.end method

.method public abstract getRecommendationAlgorithm()Ljava/lang/String;
.end method

.method public abstract getSuccess()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasCorrelationtoken()Z
.end method

.method public abstract hasLang()Z
.end method

.method public abstract hasMessagebody()Z
.end method

.method public abstract hasMessagesubject()Z
.end method

.method public abstract hasRecommendationAlgorithm()Z
.end method

.method public abstract hasSuccess()Z
.end method
