.class public final Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$InviteeOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$Invitee;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$Invitee;",
        "Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$InviteeOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private displayname_:Ljava/lang/Object;

.field private email_:Ljava/lang/Object;

.field private firstname_:Ljava/lang/Object;

.field private lastname_:Ljava/lang/Object;

.field private middlename_:Ljava/lang/Object;

.field private nameprefix_:Ljava/lang/Object;

.field private namesuffix_:Ljava/lang/Object;

.field private phonenumber_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 15169
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 15371
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->firstname_:Ljava/lang/Object;

    .line 15407
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->lastname_:Ljava/lang/Object;

    .line 15443
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->email_:Ljava/lang/Object;

    .line 15479
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->phonenumber_:Ljava/lang/Object;

    .line 15515
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->nameprefix_:Ljava/lang/Object;

    .line 15551
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->middlename_:Ljava/lang/Object;

    .line 15587
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->namesuffix_:Ljava/lang/Object;

    .line 15623
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->displayname_:Ljava/lang/Object;

    .line 15170
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->maybeForceBuilderInitialization()V

    .line 15171
    return-void
.end method

.method static synthetic access$18900(Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 15164
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$19000()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;
    .locals 1

    .prologue
    .line 15164
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 15218
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    .line 15219
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 15220
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 15223
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;
    .locals 1

    .prologue
    .line 15176
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 15174
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 15164
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    .locals 2

    .prologue
    .line 15209
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    .line 15210
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 15211
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 15213
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 15164
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    .locals 5

    .prologue
    .line 15227
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;-><init>(Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 15228
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15229
    const/4 v2, 0x0

    .line 15230
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 15231
    or-int/lit8 v2, v2, 0x1

    .line 15233
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->firstname_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Invitee;->firstname_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->access$19202(Lcom/sgiggle/xmpp/SessionMessages$Invitee;Ljava/lang/Object;)Ljava/lang/Object;

    .line 15234
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 15235
    or-int/lit8 v2, v2, 0x2

    .line 15237
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->lastname_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Invitee;->lastname_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->access$19302(Lcom/sgiggle/xmpp/SessionMessages$Invitee;Ljava/lang/Object;)Ljava/lang/Object;

    .line 15238
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 15239
    or-int/lit8 v2, v2, 0x4

    .line 15241
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->email_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Invitee;->email_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->access$19402(Lcom/sgiggle/xmpp/SessionMessages$Invitee;Ljava/lang/Object;)Ljava/lang/Object;

    .line 15242
    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 15243
    or-int/lit8 v2, v2, 0x8

    .line 15245
    :cond_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->phonenumber_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Invitee;->phonenumber_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->access$19502(Lcom/sgiggle/xmpp/SessionMessages$Invitee;Ljava/lang/Object;)Ljava/lang/Object;

    .line 15246
    and-int/lit8 v3, v1, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 15247
    or-int/lit8 v2, v2, 0x10

    .line 15249
    :cond_4
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->nameprefix_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Invitee;->nameprefix_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->access$19602(Lcom/sgiggle/xmpp/SessionMessages$Invitee;Ljava/lang/Object;)Ljava/lang/Object;

    .line 15250
    and-int/lit8 v3, v1, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 15251
    or-int/lit8 v2, v2, 0x20

    .line 15253
    :cond_5
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->middlename_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Invitee;->middlename_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->access$19702(Lcom/sgiggle/xmpp/SessionMessages$Invitee;Ljava/lang/Object;)Ljava/lang/Object;

    .line 15254
    and-int/lit8 v3, v1, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    .line 15255
    or-int/lit8 v2, v2, 0x40

    .line 15257
    :cond_6
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->namesuffix_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Invitee;->namesuffix_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->access$19802(Lcom/sgiggle/xmpp/SessionMessages$Invitee;Ljava/lang/Object;)Ljava/lang/Object;

    .line 15258
    and-int/lit16 v1, v1, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_7

    .line 15259
    or-int/lit16 v1, v2, 0x80

    .line 15261
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->displayname_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Invitee;->displayname_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->access$19902(Lcom/sgiggle/xmpp/SessionMessages$Invitee;Ljava/lang/Object;)Ljava/lang/Object;

    .line 15262
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Invitee;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->access$20002(Lcom/sgiggle/xmpp/SessionMessages$Invitee;I)I

    .line 15263
    return-object v0

    :cond_7
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 15164
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 15164
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;
    .locals 1

    .prologue
    .line 15180
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 15181
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->firstname_:Ljava/lang/Object;

    .line 15182
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15183
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->lastname_:Ljava/lang/Object;

    .line 15184
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15185
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->email_:Ljava/lang/Object;

    .line 15186
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15187
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->phonenumber_:Ljava/lang/Object;

    .line 15188
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15189
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->nameprefix_:Ljava/lang/Object;

    .line 15190
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15191
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->middlename_:Ljava/lang/Object;

    .line 15192
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15193
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->namesuffix_:Ljava/lang/Object;

    .line 15194
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15195
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->displayname_:Ljava/lang/Object;

    .line 15196
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15197
    return-object p0
.end method

.method public clearDisplayname()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;
    .locals 1

    .prologue
    .line 15647
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15648
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getDisplayname()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->displayname_:Ljava/lang/Object;

    .line 15650
    return-object p0
.end method

.method public clearEmail()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;
    .locals 1

    .prologue
    .line 15467
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15468
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getEmail()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->email_:Ljava/lang/Object;

    .line 15470
    return-object p0
.end method

.method public clearFirstname()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;
    .locals 1

    .prologue
    .line 15395
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15396
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getFirstname()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->firstname_:Ljava/lang/Object;

    .line 15398
    return-object p0
.end method

.method public clearLastname()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;
    .locals 1

    .prologue
    .line 15431
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15432
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getLastname()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->lastname_:Ljava/lang/Object;

    .line 15434
    return-object p0
.end method

.method public clearMiddlename()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;
    .locals 1

    .prologue
    .line 15575
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15576
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getMiddlename()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->middlename_:Ljava/lang/Object;

    .line 15578
    return-object p0
.end method

.method public clearNameprefix()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;
    .locals 1

    .prologue
    .line 15539
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15540
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getNameprefix()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->nameprefix_:Ljava/lang/Object;

    .line 15542
    return-object p0
.end method

.method public clearNamesuffix()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;
    .locals 1

    .prologue
    .line 15611
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15612
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getNamesuffix()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->namesuffix_:Ljava/lang/Object;

    .line 15614
    return-object p0
.end method

.method public clearPhonenumber()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;
    .locals 1

    .prologue
    .line 15503
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15504
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getPhonenumber()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->phonenumber_:Ljava/lang/Object;

    .line 15506
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 15164
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 15164
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 15164
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;
    .locals 2

    .prologue
    .line 15201
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Invitee;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 15164
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 15164
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 15164
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    .locals 1

    .prologue
    .line 15205
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 15628
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->displayname_:Ljava/lang/Object;

    .line 15629
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 15630
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 15631
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->displayname_:Ljava/lang/Object;

    .line 15634
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 2

    .prologue
    .line 15448
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->email_:Ljava/lang/Object;

    .line 15449
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 15450
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 15451
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->email_:Ljava/lang/Object;

    .line 15454
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getFirstname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 15376
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->firstname_:Ljava/lang/Object;

    .line 15377
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 15378
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 15379
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->firstname_:Ljava/lang/Object;

    .line 15382
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getLastname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 15412
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->lastname_:Ljava/lang/Object;

    .line 15413
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 15414
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 15415
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->lastname_:Ljava/lang/Object;

    .line 15418
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getMiddlename()Ljava/lang/String;
    .locals 2

    .prologue
    .line 15556
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->middlename_:Ljava/lang/Object;

    .line 15557
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 15558
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 15559
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->middlename_:Ljava/lang/Object;

    .line 15562
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getNameprefix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 15520
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->nameprefix_:Ljava/lang/Object;

    .line 15521
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 15522
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 15523
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->nameprefix_:Ljava/lang/Object;

    .line 15526
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getNamesuffix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 15592
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->namesuffix_:Ljava/lang/Object;

    .line 15593
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 15594
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 15595
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->namesuffix_:Ljava/lang/Object;

    .line 15598
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getPhonenumber()Ljava/lang/String;
    .locals 2

    .prologue
    .line 15484
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->phonenumber_:Ljava/lang/Object;

    .line 15485
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 15486
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 15487
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->phonenumber_:Ljava/lang/Object;

    .line 15490
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasDisplayname()Z
    .locals 2

    .prologue
    .line 15625
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasEmail()Z
    .locals 2

    .prologue
    .line 15445
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFirstname()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 15373
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLastname()Z
    .locals 2

    .prologue
    .line 15409
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMiddlename()Z
    .locals 2

    .prologue
    .line 15553
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNameprefix()Z
    .locals 2

    .prologue
    .line 15517
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNamesuffix()Z
    .locals 2

    .prologue
    .line 15589
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPhonenumber()Z
    .locals 2

    .prologue
    .line 15481
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 15296
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->hasEmail()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 15304
    :goto_0
    return v0

    .line 15300
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->hasPhonenumber()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 15302
    goto :goto_0

    .line 15304
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15164
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 15164
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Invitee;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15164
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15312
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 15313
    sparse-switch v0, :sswitch_data_0

    .line 15318
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 15320
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 15316
    goto :goto_1

    .line 15325
    :sswitch_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15326
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->firstname_:Ljava/lang/Object;

    goto :goto_0

    .line 15330
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15331
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->lastname_:Ljava/lang/Object;

    goto :goto_0

    .line 15335
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15336
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->email_:Ljava/lang/Object;

    goto :goto_0

    .line 15340
    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15341
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->phonenumber_:Ljava/lang/Object;

    goto :goto_0

    .line 15345
    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15346
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->nameprefix_:Ljava/lang/Object;

    goto :goto_0

    .line 15350
    :sswitch_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15351
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->middlename_:Ljava/lang/Object;

    goto :goto_0

    .line 15355
    :sswitch_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15356
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->namesuffix_:Ljava/lang/Object;

    goto :goto_0

    .line 15360
    :sswitch_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15361
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->displayname_:Ljava/lang/Object;

    goto :goto_0

    .line 15313
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Invitee;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;
    .locals 1
    .parameter

    .prologue
    .line 15267
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 15292
    :goto_0
    return-object v0

    .line 15268
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->hasFirstname()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 15269
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getFirstname()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setFirstname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    .line 15271
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->hasLastname()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 15272
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getLastname()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setLastname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    .line 15274
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->hasEmail()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 15275
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getEmail()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setEmail(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    .line 15277
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->hasPhonenumber()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 15278
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getPhonenumber()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setPhonenumber(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    .line 15280
    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->hasNameprefix()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 15281
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getNameprefix()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setNameprefix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    .line 15283
    :cond_5
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->hasMiddlename()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 15284
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getMiddlename()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setMiddlename(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    .line 15286
    :cond_6
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->hasNamesuffix()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 15287
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getNamesuffix()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setNamesuffix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    .line 15289
    :cond_7
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->hasDisplayname()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 15290
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getDisplayname()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setDisplayname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    :cond_8
    move-object v0, p0

    .line 15292
    goto :goto_0
.end method

.method public setDisplayname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;
    .locals 1
    .parameter

    .prologue
    .line 15638
    if-nez p1, :cond_0

    .line 15639
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 15641
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15642
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->displayname_:Ljava/lang/Object;

    .line 15644
    return-object p0
.end method

.method setDisplayname(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 15653
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15654
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->displayname_:Ljava/lang/Object;

    .line 15656
    return-void
.end method

.method public setEmail(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;
    .locals 1
    .parameter

    .prologue
    .line 15458
    if-nez p1, :cond_0

    .line 15459
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 15461
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15462
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->email_:Ljava/lang/Object;

    .line 15464
    return-object p0
.end method

.method setEmail(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 15473
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15474
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->email_:Ljava/lang/Object;

    .line 15476
    return-void
.end method

.method public setFirstname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;
    .locals 1
    .parameter

    .prologue
    .line 15386
    if-nez p1, :cond_0

    .line 15387
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 15389
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15390
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->firstname_:Ljava/lang/Object;

    .line 15392
    return-object p0
.end method

.method setFirstname(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 15401
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15402
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->firstname_:Ljava/lang/Object;

    .line 15404
    return-void
.end method

.method public setLastname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;
    .locals 1
    .parameter

    .prologue
    .line 15422
    if-nez p1, :cond_0

    .line 15423
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 15425
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15426
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->lastname_:Ljava/lang/Object;

    .line 15428
    return-object p0
.end method

.method setLastname(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 15437
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15438
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->lastname_:Ljava/lang/Object;

    .line 15440
    return-void
.end method

.method public setMiddlename(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;
    .locals 1
    .parameter

    .prologue
    .line 15566
    if-nez p1, :cond_0

    .line 15567
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 15569
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15570
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->middlename_:Ljava/lang/Object;

    .line 15572
    return-object p0
.end method

.method setMiddlename(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 15581
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15582
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->middlename_:Ljava/lang/Object;

    .line 15584
    return-void
.end method

.method public setNameprefix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;
    .locals 1
    .parameter

    .prologue
    .line 15530
    if-nez p1, :cond_0

    .line 15531
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 15533
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15534
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->nameprefix_:Ljava/lang/Object;

    .line 15536
    return-object p0
.end method

.method setNameprefix(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 15545
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15546
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->nameprefix_:Ljava/lang/Object;

    .line 15548
    return-void
.end method

.method public setNamesuffix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;
    .locals 1
    .parameter

    .prologue
    .line 15602
    if-nez p1, :cond_0

    .line 15603
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 15605
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15606
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->namesuffix_:Ljava/lang/Object;

    .line 15608
    return-object p0
.end method

.method setNamesuffix(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 15617
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15618
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->namesuffix_:Ljava/lang/Object;

    .line 15620
    return-void
.end method

.method public setPhonenumber(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;
    .locals 1
    .parameter

    .prologue
    .line 15494
    if-nez p1, :cond_0

    .line 15495
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 15497
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15498
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->phonenumber_:Ljava/lang/Object;

    .line 15500
    return-object p0
.end method

.method setPhonenumber(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 15509
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->bitField0_:I

    .line 15510
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->phonenumber_:Ljava/lang/Object;

    .line 15512
    return-void
.end method
