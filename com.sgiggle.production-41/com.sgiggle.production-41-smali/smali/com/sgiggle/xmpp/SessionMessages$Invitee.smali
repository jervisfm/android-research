.class public final Lcom/sgiggle/xmpp/SessionMessages$Invitee;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$InviteeOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Invitee"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;
    }
.end annotation


# static fields
.field public static final DISPLAYNAME_FIELD_NUMBER:I = 0x8

.field public static final EMAIL_FIELD_NUMBER:I = 0x3

.field public static final FIRSTNAME_FIELD_NUMBER:I = 0x1

.field public static final LASTNAME_FIELD_NUMBER:I = 0x2

.field public static final MIDDLENAME_FIELD_NUMBER:I = 0x6

.field public static final NAMEPREFIX_FIELD_NUMBER:I = 0x5

.field public static final NAMESUFFIX_FIELD_NUMBER:I = 0x7

.field public static final PHONENUMBER_FIELD_NUMBER:I = 0x4

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$Invitee;


# instance fields
.field private bitField0_:I

.field private displayname_:Ljava/lang/Object;

.field private email_:Ljava/lang/Object;

.field private firstname_:Ljava/lang/Object;

.field private lastname_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private middlename_:Ljava/lang/Object;

.field private nameprefix_:Ljava/lang/Object;

.field private namesuffix_:Ljava/lang/Object;

.field private phonenumber_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 15662
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    .line 15663
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->initFields()V

    .line 15664
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 14717
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 14997
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->memoizedIsInitialized:B

    .line 15043
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->memoizedSerializedSize:I

    .line 14718
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 14712
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;-><init>(Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 14719
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 14997
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->memoizedIsInitialized:B

    .line 15043
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->memoizedSerializedSize:I

    .line 14719
    return-void
.end method

.method static synthetic access$19202(Lcom/sgiggle/xmpp/SessionMessages$Invitee;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 14712
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->firstname_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$19302(Lcom/sgiggle/xmpp/SessionMessages$Invitee;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 14712
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->lastname_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$19402(Lcom/sgiggle/xmpp/SessionMessages$Invitee;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 14712
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->email_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$19502(Lcom/sgiggle/xmpp/SessionMessages$Invitee;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 14712
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->phonenumber_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$19602(Lcom/sgiggle/xmpp/SessionMessages$Invitee;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 14712
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->nameprefix_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$19702(Lcom/sgiggle/xmpp/SessionMessages$Invitee;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 14712
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->middlename_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$19802(Lcom/sgiggle/xmpp/SessionMessages$Invitee;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 14712
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->namesuffix_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$19902(Lcom/sgiggle/xmpp/SessionMessages$Invitee;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 14712
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->displayname_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$20002(Lcom/sgiggle/xmpp/SessionMessages$Invitee;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 14712
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    .locals 1

    .prologue
    .line 14723
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    return-object v0
.end method

.method private getDisplaynameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 14976
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->displayname_:Ljava/lang/Object;

    .line 14977
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 14978
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 14980
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->displayname_:Ljava/lang/Object;

    .line 14983
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getEmailBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 14816
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->email_:Ljava/lang/Object;

    .line 14817
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 14818
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 14820
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->email_:Ljava/lang/Object;

    .line 14823
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getFirstnameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 14752
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->firstname_:Ljava/lang/Object;

    .line 14753
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 14754
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 14756
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->firstname_:Ljava/lang/Object;

    .line 14759
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getLastnameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 14784
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->lastname_:Ljava/lang/Object;

    .line 14785
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 14786
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 14788
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->lastname_:Ljava/lang/Object;

    .line 14791
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getMiddlenameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 14912
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->middlename_:Ljava/lang/Object;

    .line 14913
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 14914
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 14916
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->middlename_:Ljava/lang/Object;

    .line 14919
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getNameprefixBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 14880
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->nameprefix_:Ljava/lang/Object;

    .line 14881
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 14882
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 14884
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->nameprefix_:Ljava/lang/Object;

    .line 14887
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getNamesuffixBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 14944
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->namesuffix_:Ljava/lang/Object;

    .line 14945
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 14946
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 14948
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->namesuffix_:Ljava/lang/Object;

    .line 14951
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getPhonenumberBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 14848
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->phonenumber_:Ljava/lang/Object;

    .line 14849
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 14850
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 14852
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->phonenumber_:Ljava/lang/Object;

    .line 14855
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 14988
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->firstname_:Ljava/lang/Object;

    .line 14989
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->lastname_:Ljava/lang/Object;

    .line 14990
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->email_:Ljava/lang/Object;

    .line 14991
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->phonenumber_:Ljava/lang/Object;

    .line 14992
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->nameprefix_:Ljava/lang/Object;

    .line 14993
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->middlename_:Ljava/lang/Object;

    .line 14994
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->namesuffix_:Ljava/lang/Object;

    .line 14995
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->displayname_:Ljava/lang/Object;

    .line 14996
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;
    .locals 1

    .prologue
    .line 15157
    #calls: Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->access$19000()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Invitee;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;
    .locals 1
    .parameter

    .prologue
    .line 15160
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Invitee;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15126
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    .line 15127
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15128
    #calls: Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->access$18900(Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    .line 15130
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15137
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    .line 15138
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15139
    #calls: Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->access$18900(Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    .line 15141
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 15093
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->access$18900(Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 15099
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->access$18900(Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15147
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->access$18900(Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15153
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->access$18900(Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15115
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->access$18900(Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15121
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->access$18900(Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 15104
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->access$18900(Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 15110
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->access$18900(Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 14712
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    .locals 1

    .prologue
    .line 14727
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    return-object v0
.end method

.method public getDisplayname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 14962
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->displayname_:Ljava/lang/Object;

    .line 14963
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 14964
    check-cast v0, Ljava/lang/String;

    .line 14972
    :goto_0
    return-object v0

    .line 14966
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 14968
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 14969
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 14970
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->displayname_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 14972
    goto :goto_0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 2

    .prologue
    .line 14802
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->email_:Ljava/lang/Object;

    .line 14803
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 14804
    check-cast v0, Ljava/lang/String;

    .line 14812
    :goto_0
    return-object v0

    .line 14806
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 14808
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 14809
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 14810
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->email_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 14812
    goto :goto_0
.end method

.method public getFirstname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 14738
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->firstname_:Ljava/lang/Object;

    .line 14739
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 14740
    check-cast v0, Ljava/lang/String;

    .line 14748
    :goto_0
    return-object v0

    .line 14742
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 14744
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 14745
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 14746
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->firstname_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 14748
    goto :goto_0
.end method

.method public getLastname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 14770
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->lastname_:Ljava/lang/Object;

    .line 14771
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 14772
    check-cast v0, Ljava/lang/String;

    .line 14780
    :goto_0
    return-object v0

    .line 14774
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 14776
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 14777
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 14778
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->lastname_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 14780
    goto :goto_0
.end method

.method public getMiddlename()Ljava/lang/String;
    .locals 2

    .prologue
    .line 14898
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->middlename_:Ljava/lang/Object;

    .line 14899
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 14900
    check-cast v0, Ljava/lang/String;

    .line 14908
    :goto_0
    return-object v0

    .line 14902
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 14904
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 14905
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 14906
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->middlename_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 14908
    goto :goto_0
.end method

.method public getNameprefix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 14866
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->nameprefix_:Ljava/lang/Object;

    .line 14867
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 14868
    check-cast v0, Ljava/lang/String;

    .line 14876
    :goto_0
    return-object v0

    .line 14870
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 14872
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 14873
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 14874
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->nameprefix_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 14876
    goto :goto_0
.end method

.method public getNamesuffix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 14930
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->namesuffix_:Ljava/lang/Object;

    .line 14931
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 14932
    check-cast v0, Ljava/lang/String;

    .line 14940
    :goto_0
    return-object v0

    .line 14934
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 14936
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 14937
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 14938
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->namesuffix_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 14940
    goto :goto_0
.end method

.method public getPhonenumber()Ljava/lang/String;
    .locals 2

    .prologue
    .line 14834
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->phonenumber_:Ljava/lang/Object;

    .line 14835
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 14836
    check-cast v0, Ljava/lang/String;

    .line 14844
    :goto_0
    return-object v0

    .line 14838
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 14840
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 14841
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 14842
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->phonenumber_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 14844
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 15045
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->memoizedSerializedSize:I

    .line 15046
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 15082
    :goto_0
    return v0

    .line 15048
    :cond_0
    const/4 v0, 0x0

    .line 15049
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 15050
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getFirstnameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15053
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 15054
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getLastnameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15057
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 15058
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getEmailBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15061
    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_4

    .line 15062
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getPhonenumberBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15065
    :cond_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 15066
    const/4 v1, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getNameprefixBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15069
    :cond_5
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    .line 15070
    const/4 v1, 0x6

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getMiddlenameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15073
    :cond_6
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    .line 15074
    const/4 v1, 0x7

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getNamesuffixBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15077
    :cond_7
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_8

    .line 15078
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getDisplaynameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15081
    :cond_8
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->memoizedSerializedSize:I

    goto/16 :goto_0
.end method

.method public hasDisplayname()Z
    .locals 2

    .prologue
    .line 14959
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasEmail()Z
    .locals 2

    .prologue
    .line 14799
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFirstname()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 14735
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLastname()Z
    .locals 2

    .prologue
    .line 14767
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMiddlename()Z
    .locals 2

    .prologue
    .line 14895
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNameprefix()Z
    .locals 2

    .prologue
    .line 14863
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNamesuffix()Z
    .locals 2

    .prologue
    .line 14927
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPhonenumber()Z
    .locals 2

    .prologue
    .line 14831
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 14999
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->memoizedIsInitialized:B

    .line 15000
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 15011
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 15000
    goto :goto_0

    .line 15002
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->hasEmail()Z

    move-result v0

    if-nez v0, :cond_2

    .line 15003
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->memoizedIsInitialized:B

    move v0, v2

    .line 15004
    goto :goto_0

    .line 15006
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->hasPhonenumber()Z

    move-result v0

    if-nez v0, :cond_3

    .line 15007
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->memoizedIsInitialized:B

    move v0, v2

    .line 15008
    goto :goto_0

    .line 15010
    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->memoizedIsInitialized:B

    move v0, v3

    .line 15011
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 14712
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;
    .locals 1

    .prologue
    .line 15158
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 14712
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;
    .locals 1

    .prologue
    .line 15162
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Invitee;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 15087
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 15016
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getSerializedSize()I

    .line 15017
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 15018
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getFirstnameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 15020
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 15021
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getLastnameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 15023
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 15024
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getEmailBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 15026
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 15027
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getPhonenumberBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 15029
    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 15030
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getNameprefixBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 15032
    :cond_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 15033
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getMiddlenameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 15035
    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 15036
    const/4 v0, 0x7

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getNamesuffixBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 15038
    :cond_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 15039
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getDisplaynameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 15041
    :cond_7
    return-void
.end method
