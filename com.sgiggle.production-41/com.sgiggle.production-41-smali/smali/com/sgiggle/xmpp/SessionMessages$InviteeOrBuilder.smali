.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$InviteeOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "InviteeOrBuilder"
.end annotation


# virtual methods
.method public abstract getDisplayname()Ljava/lang/String;
.end method

.method public abstract getEmail()Ljava/lang/String;
.end method

.method public abstract getFirstname()Ljava/lang/String;
.end method

.method public abstract getLastname()Ljava/lang/String;
.end method

.method public abstract getMiddlename()Ljava/lang/String;
.end method

.method public abstract getNameprefix()Ljava/lang/String;
.end method

.method public abstract getNamesuffix()Ljava/lang/String;
.end method

.method public abstract getPhonenumber()Ljava/lang/String;
.end method

.method public abstract hasDisplayname()Z
.end method

.method public abstract hasEmail()Z
.end method

.method public abstract hasFirstname()Z
.end method

.method public abstract hasLastname()Z
.end method

.method public abstract hasMiddlename()Z
.end method

.method public abstract hasNameprefix()Z
.end method

.method public abstract hasNamesuffix()Z
.end method

.method public abstract hasPhonenumber()Z
.end method
