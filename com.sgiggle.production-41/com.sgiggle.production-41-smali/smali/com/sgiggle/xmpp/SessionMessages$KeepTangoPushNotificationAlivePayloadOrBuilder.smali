.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$KeepTangoPushNotificationAlivePayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "KeepTangoPushNotificationAlivePayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getAutoKeepAlive()Z
.end method

.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getMaxKeepAliveInterval()I
.end method

.method public abstract getMinKeepAliveInterval()I
.end method

.method public abstract hasAutoKeepAlive()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasMaxKeepAliveInterval()Z
.end method

.method public abstract hasMinKeepAliveInterval()Z
.end method
