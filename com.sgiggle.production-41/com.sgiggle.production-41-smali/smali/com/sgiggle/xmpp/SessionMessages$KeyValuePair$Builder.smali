.class public final Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$KeyValuePairOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;",
        "Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$KeyValuePairOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private key_:Ljava/lang/Object;

.field private value_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 60171
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 60289
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->key_:Ljava/lang/Object;

    .line 60325
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->value_:Ljava/lang/Object;

    .line 60172
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->maybeForceBuilderInitialization()V

    .line 60173
    return-void
.end method

.method static synthetic access$77500(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 60166
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$77600()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;
    .locals 1

    .prologue
    .line 60166
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 60208
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v0

    .line 60209
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 60210
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 60213
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;
    .locals 1

    .prologue
    .line 60178
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 60176
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 60166
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    .locals 2

    .prologue
    .line 60199
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v0

    .line 60200
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 60201
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 60203
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 60166
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    .locals 5

    .prologue
    .line 60217
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;-><init>(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 60218
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->bitField0_:I

    .line 60219
    const/4 v2, 0x0

    .line 60220
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 60221
    or-int/lit8 v2, v2, 0x1

    .line 60223
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->key_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->key_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->access$77802(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60224
    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    .line 60225
    or-int/lit8 v1, v2, 0x2

    .line 60227
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->value_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->value_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->access$77902(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60228
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->access$78002(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;I)I

    .line 60229
    return-object v0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 60166
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 60166
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;
    .locals 1

    .prologue
    .line 60182
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 60183
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->key_:Ljava/lang/Object;

    .line 60184
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->bitField0_:I

    .line 60185
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->value_:Ljava/lang/Object;

    .line 60186
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->bitField0_:I

    .line 60187
    return-object p0
.end method

.method public clearKey()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;
    .locals 1

    .prologue
    .line 60313
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->bitField0_:I

    .line 60314
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->getKey()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->key_:Ljava/lang/Object;

    .line 60316
    return-object p0
.end method

.method public clearValue()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;
    .locals 1

    .prologue
    .line 60349
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->bitField0_:I

    .line 60350
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->getValue()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->value_:Ljava/lang/Object;

    .line 60352
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 60166
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 60166
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 60166
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;
    .locals 2

    .prologue
    .line 60191
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 60166
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 60166
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 60166
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    .locals 1

    .prologue
    .line 60195
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v0

    return-object v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 2

    .prologue
    .line 60294
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->key_:Ljava/lang/Object;

    .line 60295
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 60296
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 60297
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->key_:Ljava/lang/Object;

    .line 60300
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getValue()Ljava/lang/String;
    .locals 2

    .prologue
    .line 60330
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->value_:Ljava/lang/Object;

    .line 60331
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 60332
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 60333
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->value_:Ljava/lang/Object;

    .line 60336
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasKey()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 60291
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasValue()Z
    .locals 2

    .prologue
    .line 60327
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 60244
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->hasKey()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 60252
    :goto_0
    return v0

    .line 60248
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->hasValue()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 60250
    goto :goto_0

    .line 60252
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60166
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 60166
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60166
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60260
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 60261
    sparse-switch v0, :sswitch_data_0

    .line 60266
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 60268
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 60264
    goto :goto_1

    .line 60273
    :sswitch_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->bitField0_:I

    .line 60274
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->key_:Ljava/lang/Object;

    goto :goto_0

    .line 60278
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->bitField0_:I

    .line 60279
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->value_:Ljava/lang/Object;

    goto :goto_0

    .line 60261
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;
    .locals 1
    .parameter

    .prologue
    .line 60233
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 60240
    :goto_0
    return-object v0

    .line 60234
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->hasKey()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60235
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->setKey(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    .line 60237
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 60238
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->setValue(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    :cond_2
    move-object v0, p0

    .line 60240
    goto :goto_0
.end method

.method public setKey(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;
    .locals 1
    .parameter

    .prologue
    .line 60304
    if-nez p1, :cond_0

    .line 60305
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 60307
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->bitField0_:I

    .line 60308
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->key_:Ljava/lang/Object;

    .line 60310
    return-object p0
.end method

.method setKey(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 60319
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->bitField0_:I

    .line 60320
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->key_:Ljava/lang/Object;

    .line 60322
    return-void
.end method

.method public setValue(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;
    .locals 1
    .parameter

    .prologue
    .line 60340
    if-nez p1, :cond_0

    .line 60341
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 60343
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->bitField0_:I

    .line 60344
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->value_:Ljava/lang/Object;

    .line 60346
    return-object p0
.end method

.method setValue(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 60355
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->bitField0_:I

    .line 60356
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->value_:Ljava/lang/Object;

    .line 60358
    return-void
.end method
