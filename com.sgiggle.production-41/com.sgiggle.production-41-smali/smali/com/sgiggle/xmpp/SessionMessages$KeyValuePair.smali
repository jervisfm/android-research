.class public final Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$KeyValuePairOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "KeyValuePair"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;
    }
.end annotation


# static fields
.field public static final KEY_FIELD_NUMBER:I = 0x1

.field public static final VALUE_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;


# instance fields
.field private bitField0_:I

.field private key_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private value_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 60364
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    .line 60365
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->initFields()V

    .line 60366
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 59959
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 60041
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->memoizedIsInitialized:B

    .line 60069
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->memoizedSerializedSize:I

    .line 59960
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 59954
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;-><init>(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 59961
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 60041
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->memoizedIsInitialized:B

    .line 60069
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->memoizedSerializedSize:I

    .line 59961
    return-void
.end method

.method static synthetic access$77802(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 59954
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->key_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$77902(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 59954
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->value_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$78002(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 59954
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    .locals 1

    .prologue
    .line 59965
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    return-object v0
.end method

.method private getKeyBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 59994
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->key_:Ljava/lang/Object;

    .line 59995
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 59996
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 59998
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->key_:Ljava/lang/Object;

    .line 60001
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getValueBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 60026
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->value_:Ljava/lang/Object;

    .line 60027
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 60028
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 60030
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->value_:Ljava/lang/Object;

    .line 60033
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 60038
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->key_:Ljava/lang/Object;

    .line 60039
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->value_:Ljava/lang/Object;

    .line 60040
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;
    .locals 1

    .prologue
    .line 60159
    #calls: Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->access$77600()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;
    .locals 1
    .parameter

    .prologue
    .line 60162
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60128
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    .line 60129
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 60130
    #calls: Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->access$77500(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v0

    .line 60132
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60139
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    .line 60140
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 60141
    #calls: Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->access$77500(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v0

    .line 60143
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 60095
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->access$77500(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 60101
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->access$77500(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60149
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->access$77500(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60155
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->access$77500(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60117
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->access$77500(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60123
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->access$77500(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 60106
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->access$77500(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 60112
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->access$77500(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 59954
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    .locals 1

    .prologue
    .line 59969
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    return-object v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 2

    .prologue
    .line 59980
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->key_:Ljava/lang/Object;

    .line 59981
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 59982
    check-cast v0, Ljava/lang/String;

    .line 59990
    :goto_0
    return-object v0

    .line 59984
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 59986
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 59987
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 59988
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->key_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 59990
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 60071
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->memoizedSerializedSize:I

    .line 60072
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 60084
    :goto_0
    return v0

    .line 60074
    :cond_0
    const/4 v0, 0x0

    .line 60075
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 60076
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->getKeyBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 60079
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 60080
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->getValueBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 60083
    :cond_2
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getValue()Ljava/lang/String;
    .locals 2

    .prologue
    .line 60012
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->value_:Ljava/lang/Object;

    .line 60013
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 60014
    check-cast v0, Ljava/lang/String;

    .line 60022
    :goto_0
    return-object v0

    .line 60016
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 60018
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 60019
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60020
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->value_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 60022
    goto :goto_0
.end method

.method public hasKey()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 59977
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasValue()Z
    .locals 2

    .prologue
    .line 60009
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 60043
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->memoizedIsInitialized:B

    .line 60044
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 60055
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 60044
    goto :goto_0

    .line 60046
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->hasKey()Z

    move-result v0

    if-nez v0, :cond_2

    .line 60047
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->memoizedIsInitialized:B

    move v0, v2

    .line 60048
    goto :goto_0

    .line 60050
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->hasValue()Z

    move-result v0

    if-nez v0, :cond_3

    .line 60051
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->memoizedIsInitialized:B

    move v0, v2

    .line 60052
    goto :goto_0

    .line 60054
    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->memoizedIsInitialized:B

    move v0, v3

    .line 60055
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 59954
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;
    .locals 1

    .prologue
    .line 60160
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 59954
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;
    .locals 1

    .prologue
    .line 60164
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 60089
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 60060
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->getSerializedSize()I

    .line 60061
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 60062
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->getKeyBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 60064
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 60065
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->getValueBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 60067
    :cond_1
    return-void
.end method
