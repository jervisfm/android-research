.class public final Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private callees_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private dialogType_:I

.field private leave_:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 58616
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 58781
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 58824
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->callees_:Ljava/util/List;

    .line 58913
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->leave_:Z

    .line 58617
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->maybeForceBuilderInitialization()V

    .line 58618
    return-void
.end method

.method static synthetic access$75400(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 58611
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$75500()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 58611
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 58657
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    move-result-object v0

    .line 58658
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 58659
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 58662
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 58623
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureCalleesIsMutable()V
    .locals 2

    .prologue
    .line 58827
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 58828
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->callees_:Ljava/util/List;

    .line 58829
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    .line 58831
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 58621
    return-void
.end method


# virtual methods
.method public addAllCallees(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;"
        }
    .end annotation

    .prologue
    .line 58894
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->ensureCalleesIsMutable()V

    .line 58895
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 58897
    return-object p0
.end method

.method public addCallees(ILcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 58887
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->ensureCalleesIsMutable()V

    .line 58888
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 58890
    return-object p0
.end method

.method public addCallees(ILcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 58870
    if-nez p2, :cond_0

    .line 58871
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 58873
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->ensureCalleesIsMutable()V

    .line 58874
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 58876
    return-object p0
.end method

.method public addCallees(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 58880
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->ensureCalleesIsMutable()V

    .line 58881
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58883
    return-object p0
.end method

.method public addCallees(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 58860
    if-nez p1, :cond_0

    .line 58861
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 58863
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->ensureCalleesIsMutable()V

    .line 58864
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58866
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 58611
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;
    .locals 2

    .prologue
    .line 58648
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    move-result-object v0

    .line 58649
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 58650
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 58652
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 58611
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;
    .locals 5

    .prologue
    .line 58666
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 58667
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    .line 58668
    const/4 v2, 0x0

    .line 58669
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 58670
    or-int/lit8 v2, v2, 0x1

    .line 58672
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->access$75702(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 58673
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 58674
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->callees_:Ljava/util/List;

    .line 58675
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x3

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    .line 58677
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->callees_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->callees_:Ljava/util/List;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->access$75802(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;Ljava/util/List;)Ljava/util/List;

    .line 58678
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 58679
    or-int/lit8 v2, v2, 0x2

    .line 58681
    :cond_2
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->leave_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->leave_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->access$75902(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;Z)Z

    .line 58682
    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_3

    .line 58683
    or-int/lit8 v1, v2, 0x4

    .line 58685
    :goto_0
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->dialogType_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->dialogType_:I
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->access$76002(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;I)I

    .line 58686
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->access$76102(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;I)I

    .line 58687
    return-object v0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 58611
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 58611
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 58627
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 58628
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 58629
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    .line 58630
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->callees_:Ljava/util/List;

    .line 58631
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    .line 58632
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->leave_:Z

    .line 58633
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    .line 58634
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->dialogType_:I

    .line 58635
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    .line 58636
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 58817
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 58819
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    .line 58820
    return-object p0
.end method

.method public clearCallees()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 58900
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->callees_:Ljava/util/List;

    .line 58901
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    .line 58903
    return-object p0
.end method

.method public clearDialogType()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 58948
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    .line 58949
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->dialogType_:I

    .line 58951
    return-object p0
.end method

.method public clearLeave()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 58927
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    .line 58928
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->leave_:Z

    .line 58930
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 58611
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 58611
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 58611
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;
    .locals 2

    .prologue
    .line 58640
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 58611
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 58786
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCallees(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter

    .prologue
    .line 58840
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getCalleesCount()I
    .locals 1

    .prologue
    .line 58837
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCalleesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58834
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 58611
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 58611
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;
    .locals 1

    .prologue
    .line 58644
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDialogType()I
    .locals 1

    .prologue
    .line 58939
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->dialogType_:I

    return v0
.end method

.method public getLeave()Z
    .locals 1

    .prologue
    .line 58918
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->leave_:Z

    return v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 58783
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDialogType()Z
    .locals 2

    .prologue
    .line 58936
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLeave()Z
    .locals 2

    .prologue
    .line 58915
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 58715
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 58729
    :goto_0
    return v0

    .line 58719
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 58721
    goto :goto_0

    :cond_1
    move v0, v2

    .line 58723
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->getCalleesCount()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 58724
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->getCallees(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_2

    move v0, v2

    .line 58726
    goto :goto_0

    .line 58723
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 58729
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 58805
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 58807
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 58813
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    .line 58814
    return-object p0

    .line 58810
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58611
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 58611
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58611
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58737
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 58738
    sparse-switch v0, :sswitch_data_0

    .line 58743
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 58745
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 58741
    goto :goto_1

    .line 58750
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 58751
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 58752
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 58754
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 58755
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    goto :goto_0

    .line 58759
    :sswitch_2
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    .line 58760
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 58761
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->addCallees(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    goto :goto_0

    .line 58765
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    .line 58766
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->leave_:Z

    goto :goto_0

    .line 58770
    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    .line 58771
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->dialogType_:I

    goto :goto_0

    .line 58738
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 58691
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 58711
    :goto_0
    return-object v0

    .line 58692
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 58693
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    .line 58695
    :cond_1
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->callees_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->access$75800(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 58696
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 58697
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->callees_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->access$75800(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->callees_:Ljava/util/List;

    .line 58698
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    .line 58705
    :cond_2
    :goto_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->hasLeave()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 58706
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->getLeave()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->setLeave(Z)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    .line 58708
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->hasDialogType()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 58709
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->getDialogType()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->setDialogType(I)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    :cond_4
    move-object v0, p0

    .line 58711
    goto :goto_0

    .line 58700
    :cond_5
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->ensureCalleesIsMutable()V

    .line 58701
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->callees_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->callees_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->access$75800(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public removeCallees(I)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 58906
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->ensureCalleesIsMutable()V

    .line 58907
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 58909
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 58799
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 58801
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    .line 58802
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 58789
    if-nez p1, :cond_0

    .line 58790
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 58792
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 58794
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    .line 58795
    return-object p0
.end method

.method public setCallees(ILcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 58854
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->ensureCalleesIsMutable()V

    .line 58855
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 58857
    return-object p0
.end method

.method public setCallees(ILcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 58844
    if-nez p2, :cond_0

    .line 58845
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 58847
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->ensureCalleesIsMutable()V

    .line 58848
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 58850
    return-object p0
.end method

.method public setDialogType(I)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 58942
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    .line 58943
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->dialogType_:I

    .line 58945
    return-object p0
.end method

.method public setLeave(Z)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 58921
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->bitField0_:I

    .line 58922
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->leave_:Z

    .line 58924
    return-object p0
.end method
