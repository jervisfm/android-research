.class public final Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LeaveVideoMailPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CALLEES_FIELD_NUMBER:I = 0x2

.field public static final DIALOG_TYPE_FIELD_NUMBER:I = 0x4

.field public static final LEAVE_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private callees_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private dialogType_:I

.field private leave_:Z

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 58958
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    .line 58959
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->initFields()V

    .line 58960
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 58395
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 58466
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->memoizedIsInitialized:B

    .line 58506
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->memoizedSerializedSize:I

    .line 58396
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 58390
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 58397
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 58466
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->memoizedIsInitialized:B

    .line 58506
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->memoizedSerializedSize:I

    .line 58397
    return-void
.end method

.method static synthetic access$75702(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 58390
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$75800(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 58390
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->callees_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$75802(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 58390
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->callees_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$75902(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 58390
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->leave_:Z

    return p1
.end method

.method static synthetic access$76002(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 58390
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->dialogType_:I

    return p1
.end method

.method static synthetic access$76102(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 58390
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;
    .locals 1

    .prologue
    .line 58401
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 58461
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 58462
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->callees_:Ljava/util/List;

    .line 58463
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->leave_:Z

    .line 58464
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->dialogType_:I

    .line 58465
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 58604
    #calls: Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->access$75500()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 58607
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58573
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    .line 58574
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 58575
    #calls: Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->access$75400(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    move-result-object v0

    .line 58577
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58584
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    .line 58585
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 58586
    #calls: Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->access$75400(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    move-result-object v0

    .line 58588
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 58540
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->access$75400(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 58546
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->access$75400(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58594
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->access$75400(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58600
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->access$75400(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58562
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->access$75400(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58568
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->access$75400(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 58551
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->access$75400(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 58557
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;->access$75400(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 58416
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCallees(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter

    .prologue
    .line 58433
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->callees_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getCalleesCount()I
    .locals 1

    .prologue
    .line 58430
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->callees_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCalleesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58423
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->callees_:Ljava/util/List;

    return-object v0
.end method

.method public getCalleesOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 58437
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->callees_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;

    return-object v0
.end method

.method public getCalleesOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58427
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->callees_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 58390
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;
    .locals 1

    .prologue
    .line 58405
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;

    return-object v0
.end method

.method public getDialogType()I
    .locals 1

    .prologue
    .line 58457
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->dialogType_:I

    return v0
.end method

.method public getLeave()Z
    .locals 1

    .prologue
    .line 58447
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->leave_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 58508
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->memoizedSerializedSize:I

    .line 58509
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 58529
    :goto_0
    return v0

    .line 58512
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 58513
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    :goto_1
    move v1, v2

    move v2, v0

    .line 58516
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->callees_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 58517
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->callees_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 58516
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 58520
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_3

    .line 58521
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->leave_:Z

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v0

    add-int/2addr v0, v2

    .line 58524
    :goto_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_2

    .line 58525
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->dialogType_:I

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 58528
    :cond_2
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->memoizedSerializedSize:I

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 58413
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDialogType()Z
    .locals 2

    .prologue
    .line 58454
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLeave()Z
    .locals 2

    .prologue
    .line 58444
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 58468
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->memoizedIsInitialized:B

    .line 58469
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 58486
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 58469
    goto :goto_0

    .line 58471
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 58472
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 58473
    goto :goto_0

    .line 58475
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 58476
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 58477
    goto :goto_0

    :cond_3
    move v0, v2

    .line 58479
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->getCalleesCount()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 58480
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->getCallees(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_4

    .line 58481
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 58482
    goto :goto_0

    .line 58479
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 58485
    :cond_5
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 58486
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 58390
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 58605
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 58390
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 58609
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 58534
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 58491
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->getSerializedSize()I

    .line 58492
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 58493
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 58495
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->callees_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 58496
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->callees_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 58495
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 58498
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 58499
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->leave_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 58501
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_3

    .line 58502
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayload;->dialogType_:I

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 58504
    :cond_3
    return-void
.end method
