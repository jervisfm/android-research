.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$LeaveVideoMailPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "LeaveVideoMailPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getCallees(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
.end method

.method public abstract getCalleesCount()I
.end method

.method public abstract getCalleesList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getDialogType()I
.end method

.method public abstract getLeave()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasDialogType()Z
.end method

.method public abstract hasLeave()Z
.end method
