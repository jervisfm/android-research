.class public final Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private callid_:Ljava/lang/Object;

.field private fromUI_:Z

.field private notificationtimestamp_:I

.field private peername_:Ljava/lang/Object;

.field private present_:Z

.field private pushType_:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

.field private sessionid_:Ljava/lang/Object;

.field private swiftServerIp_:Ljava/lang/Object;

.field private swiftTcpPort_:I

.field private swiftUdpPort_:I

.field private type_:I

.field private username_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 5438
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 5722
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 5765
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->username_:Ljava/lang/Object;

    .line 5801
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->peername_:Ljava/lang/Object;

    .line 5921
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->callid_:Ljava/lang/Object;

    .line 5957
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->sessionid_:Ljava/lang/Object;

    .line 5993
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->swiftServerIp_:Ljava/lang/Object;

    .line 6071
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->UNKNOWN_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->pushType_:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    .line 5439
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->maybeForceBuilderInitialization()V

    .line 5440
    return-void
.end method

.method static synthetic access$5300(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 5433
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$5400()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1

    .prologue
    .line 5433
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 5497
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    move-result-object v0

    .line 5498
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 5499
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 5502
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1

    .prologue
    .line 5445
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 5443
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5433
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;
    .locals 2

    .prologue
    .line 5488
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    move-result-object v0

    .line 5489
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 5490
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 5492
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5433
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;
    .locals 5

    .prologue
    .line 5506
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 5507
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5508
    const/4 v2, 0x0

    .line 5509
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 5510
    or-int/lit8 v2, v2, 0x1

    .line 5512
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->access$5602(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 5513
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 5514
    or-int/lit8 v2, v2, 0x2

    .line 5516
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->username_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->username_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->access$5702(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5517
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 5518
    or-int/lit8 v2, v2, 0x4

    .line 5520
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->peername_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->peername_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->access$5802(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5521
    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 5522
    or-int/lit8 v2, v2, 0x8

    .line 5524
    :cond_3
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->type_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->type_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->access$5902(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;I)I

    .line 5525
    and-int/lit8 v3, v1, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 5526
    or-int/lit8 v2, v2, 0x10

    .line 5528
    :cond_4
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->present_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->present_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->access$6002(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;Z)Z

    .line 5529
    and-int/lit8 v3, v1, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 5530
    or-int/lit8 v2, v2, 0x20

    .line 5532
    :cond_5
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->notificationtimestamp_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->notificationtimestamp_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->access$6102(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;I)I

    .line 5533
    and-int/lit8 v3, v1, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    .line 5534
    or-int/lit8 v2, v2, 0x40

    .line 5536
    :cond_6
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->fromUI_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->fromUI_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->access$6202(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;Z)Z

    .line 5537
    and-int/lit16 v3, v1, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    .line 5538
    or-int/lit16 v2, v2, 0x80

    .line 5540
    :cond_7
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->callid_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->callid_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->access$6302(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5541
    and-int/lit16 v3, v1, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_8

    .line 5542
    or-int/lit16 v2, v2, 0x100

    .line 5544
    :cond_8
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->sessionid_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->sessionid_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->access$6402(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5545
    and-int/lit16 v3, v1, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_9

    .line 5546
    or-int/lit16 v2, v2, 0x200

    .line 5548
    :cond_9
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->swiftServerIp_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->swiftServerIp_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->access$6502(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5549
    and-int/lit16 v3, v1, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_a

    .line 5550
    or-int/lit16 v2, v2, 0x400

    .line 5552
    :cond_a
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->swiftTcpPort_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->swiftTcpPort_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->access$6602(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;I)I

    .line 5553
    and-int/lit16 v3, v1, 0x800

    const/16 v4, 0x800

    if-ne v3, v4, :cond_b

    .line 5554
    or-int/lit16 v2, v2, 0x800

    .line 5556
    :cond_b
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->swiftUdpPort_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->swiftUdpPort_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->access$6702(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;I)I

    .line 5557
    and-int/lit16 v1, v1, 0x1000

    const/16 v3, 0x1000

    if-ne v1, v3, :cond_c

    .line 5558
    or-int/lit16 v1, v2, 0x1000

    .line 5560
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->pushType_:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->pushType_:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->access$6802(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    .line 5561
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->access$6902(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;I)I

    .line 5562
    return-object v0

    :cond_c
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 5433
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5433
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5449
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 5450
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 5451
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5452
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->username_:Ljava/lang/Object;

    .line 5453
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5454
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->peername_:Ljava/lang/Object;

    .line 5455
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5456
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->type_:I

    .line 5457
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5458
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->present_:Z

    .line 5459
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5460
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->notificationtimestamp_:I

    .line 5461
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5462
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->fromUI_:Z

    .line 5463
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5464
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->callid_:Ljava/lang/Object;

    .line 5465
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5466
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->sessionid_:Ljava/lang/Object;

    .line 5467
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5468
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->swiftServerIp_:Ljava/lang/Object;

    .line 5469
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5470
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->swiftTcpPort_:I

    .line 5471
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5472
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->swiftUdpPort_:I

    .line 5473
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5474
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->UNKNOWN_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->pushType_:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    .line 5475
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5476
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1

    .prologue
    .line 5758
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 5760
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5761
    return-object p0
.end method

.method public clearCallid()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1

    .prologue
    .line 5945
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5946
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getCallid()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->callid_:Ljava/lang/Object;

    .line 5948
    return-object p0
.end method

.method public clearFromUI()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1

    .prologue
    .line 5914
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5915
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->fromUI_:Z

    .line 5917
    return-object p0
.end method

.method public clearNotificationtimestamp()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1

    .prologue
    .line 5893
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5894
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->notificationtimestamp_:I

    .line 5896
    return-object p0
.end method

.method public clearPeername()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1

    .prologue
    .line 5825
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5826
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getPeername()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->peername_:Ljava/lang/Object;

    .line 5828
    return-object p0
.end method

.method public clearPresent()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1

    .prologue
    .line 5872
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5873
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->present_:Z

    .line 5875
    return-object p0
.end method

.method public clearPushType()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1

    .prologue
    .line 6088
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 6089
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->UNKNOWN_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->pushType_:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    .line 6091
    return-object p0
.end method

.method public clearSessionid()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1

    .prologue
    .line 5981
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5982
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getSessionid()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->sessionid_:Ljava/lang/Object;

    .line 5984
    return-object p0
.end method

.method public clearSwiftServerIp()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1

    .prologue
    .line 6017
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 6018
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getSwiftServerIp()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->swiftServerIp_:Ljava/lang/Object;

    .line 6020
    return-object p0
.end method

.method public clearSwiftTcpPort()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1

    .prologue
    .line 6043
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 6044
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->swiftTcpPort_:I

    .line 6046
    return-object p0
.end method

.method public clearSwiftUdpPort()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1

    .prologue
    .line 6064
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 6065
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->swiftUdpPort_:I

    .line 6067
    return-object p0
.end method

.method public clearType()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1

    .prologue
    .line 5851
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5852
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->type_:I

    .line 5854
    return-object p0
.end method

.method public clearUsername()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1

    .prologue
    .line 5789
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5790
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getUsername()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->username_:Ljava/lang/Object;

    .line 5792
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 5433
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 5433
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 5433
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 2

    .prologue
    .line 5480
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 5433
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 5727
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCallid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 5926
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->callid_:Ljava/lang/Object;

    .line 5927
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 5928
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 5929
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->callid_:Ljava/lang/Object;

    .line 5932
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 5433
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 5433
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;
    .locals 1

    .prologue
    .line 5484
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    move-result-object v0

    return-object v0
.end method

.method public getFromUI()Z
    .locals 1

    .prologue
    .line 5905
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->fromUI_:Z

    return v0
.end method

.method public getNotificationtimestamp()I
    .locals 1

    .prologue
    .line 5884
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->notificationtimestamp_:I

    return v0
.end method

.method public getPeername()Ljava/lang/String;
    .locals 2

    .prologue
    .line 5806
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->peername_:Ljava/lang/Object;

    .line 5807
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 5808
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 5809
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->peername_:Ljava/lang/Object;

    .line 5812
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getPresent()Z
    .locals 1

    .prologue
    .line 5863
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->present_:Z

    return v0
.end method

.method public getPushType()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;
    .locals 1

    .prologue
    .line 6076
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->pushType_:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    return-object v0
.end method

.method public getSessionid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 5962
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->sessionid_:Ljava/lang/Object;

    .line 5963
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 5964
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 5965
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->sessionid_:Ljava/lang/Object;

    .line 5968
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getSwiftServerIp()Ljava/lang/String;
    .locals 2

    .prologue
    .line 5998
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->swiftServerIp_:Ljava/lang/Object;

    .line 5999
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 6000
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 6001
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->swiftServerIp_:Ljava/lang/Object;

    .line 6004
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getSwiftTcpPort()I
    .locals 1

    .prologue
    .line 6034
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->swiftTcpPort_:I

    return v0
.end method

.method public getSwiftUdpPort()I
    .locals 1

    .prologue
    .line 6055
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->swiftUdpPort_:I

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 5842
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->type_:I

    return v0
.end method

.method public getUsername()Ljava/lang/String;
    .locals 2

    .prologue
    .line 5770
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->username_:Ljava/lang/Object;

    .line 5771
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 5772
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 5773
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->username_:Ljava/lang/Object;

    .line 5776
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 5724
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCallid()Z
    .locals 2

    .prologue
    .line 5923
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFromUI()Z
    .locals 2

    .prologue
    .line 5902
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNotificationtimestamp()Z
    .locals 2

    .prologue
    .line 5881
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPeername()Z
    .locals 2

    .prologue
    .line 5803
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPresent()Z
    .locals 2

    .prologue
    .line 5860
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPushType()Z
    .locals 2

    .prologue
    .line 6073
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSessionid()Z
    .locals 2

    .prologue
    .line 5959
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSwiftServerIp()Z
    .locals 2

    .prologue
    .line 5995
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSwiftTcpPort()Z
    .locals 2

    .prologue
    .line 6031
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSwiftUdpPort()Z
    .locals 2

    .prologue
    .line 6052
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    .prologue
    .line 5839
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUsername()Z
    .locals 2

    .prologue
    .line 5767
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5610
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 5622
    :goto_0
    return v0

    .line 5614
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->hasPeername()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 5616
    goto :goto_0

    .line 5618
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 5620
    goto :goto_0

    .line 5622
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 5746
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 5748
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 5754
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5755
    return-object p0

    .line 5751
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5433
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 5433
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5433
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5630
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 5631
    sparse-switch v0, :sswitch_data_0

    .line 5636
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 5638
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 5634
    goto :goto_1

    .line 5643
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 5644
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 5645
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 5647
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 5648
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    goto :goto_0

    .line 5652
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5653
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->username_:Ljava/lang/Object;

    goto :goto_0

    .line 5657
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5658
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->peername_:Ljava/lang/Object;

    goto :goto_0

    .line 5662
    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5663
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->type_:I

    goto :goto_0

    .line 5667
    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5668
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->present_:Z

    goto :goto_0

    .line 5672
    :sswitch_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5673
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->notificationtimestamp_:I

    goto :goto_0

    .line 5677
    :sswitch_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5678
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->fromUI_:Z

    goto :goto_0

    .line 5682
    :sswitch_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5683
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->callid_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 5687
    :sswitch_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5688
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->sessionid_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 5692
    :sswitch_a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5693
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->swiftServerIp_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 5697
    :sswitch_b
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5698
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->swiftTcpPort_:I

    goto/16 :goto_0

    .line 5702
    :sswitch_c
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5703
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->swiftUdpPort_:I

    goto/16 :goto_0

    .line 5707
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 5708
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    move-result-object v0

    .line 5709
    if-eqz v0, :cond_0

    .line 5710
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit16 v1, v1, 0x1000

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5711
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->pushType_:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    goto/16 :goto_0

    .line 5631
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 5566
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 5606
    :goto_0
    return-object v0

    .line 5567
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5568
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    .line 5570
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->hasUsername()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5571
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getUsername()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->setUsername(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    .line 5573
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->hasPeername()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 5574
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getPeername()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->setPeername(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    .line 5576
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->hasType()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 5577
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getType()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->setType(I)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    .line 5579
    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->hasPresent()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 5580
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getPresent()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->setPresent(Z)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    .line 5582
    :cond_5
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->hasNotificationtimestamp()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 5583
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getNotificationtimestamp()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->setNotificationtimestamp(I)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    .line 5585
    :cond_6
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->hasFromUI()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 5586
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getFromUI()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->setFromUI(Z)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    .line 5588
    :cond_7
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->hasCallid()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 5589
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getCallid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->setCallid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    .line 5591
    :cond_8
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->hasSessionid()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 5592
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getSessionid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->setSessionid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    .line 5594
    :cond_9
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->hasSwiftServerIp()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 5595
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getSwiftServerIp()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->setSwiftServerIp(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    .line 5597
    :cond_a
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->hasSwiftTcpPort()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 5598
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getSwiftTcpPort()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->setSwiftTcpPort(I)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    .line 5600
    :cond_b
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->hasSwiftUdpPort()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 5601
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getSwiftUdpPort()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->setSwiftUdpPort(I)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    .line 5603
    :cond_c
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->hasPushType()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 5604
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getPushType()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->setPushType(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    :cond_d
    move-object v0, p0

    .line 5606
    goto/16 :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 5740
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 5742
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5743
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 5730
    if-nez p1, :cond_0

    .line 5731
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5733
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 5735
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5736
    return-object p0
.end method

.method public setCallid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 5936
    if-nez p1, :cond_0

    .line 5937
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5939
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5940
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->callid_:Ljava/lang/Object;

    .line 5942
    return-object p0
.end method

.method setCallid(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 5951
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5952
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->callid_:Ljava/lang/Object;

    .line 5954
    return-void
.end method

.method public setFromUI(Z)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 5908
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5909
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->fromUI_:Z

    .line 5911
    return-object p0
.end method

.method public setNotificationtimestamp(I)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 5887
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5888
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->notificationtimestamp_:I

    .line 5890
    return-object p0
.end method

.method public setPeername(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 5816
    if-nez p1, :cond_0

    .line 5817
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5819
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5820
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->peername_:Ljava/lang/Object;

    .line 5822
    return-object p0
.end method

.method setPeername(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 5831
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5832
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->peername_:Ljava/lang/Object;

    .line 5834
    return-void
.end method

.method public setPresent(Z)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 5866
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5867
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->present_:Z

    .line 5869
    return-object p0
.end method

.method public setPushType(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 6079
    if-nez p1, :cond_0

    .line 6080
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6082
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 6083
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->pushType_:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    .line 6085
    return-object p0
.end method

.method public setSessionid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 5972
    if-nez p1, :cond_0

    .line 5973
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5975
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5976
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->sessionid_:Ljava/lang/Object;

    .line 5978
    return-object p0
.end method

.method setSessionid(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 5987
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5988
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->sessionid_:Ljava/lang/Object;

    .line 5990
    return-void
.end method

.method public setSwiftServerIp(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 6008
    if-nez p1, :cond_0

    .line 6009
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6011
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 6012
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->swiftServerIp_:Ljava/lang/Object;

    .line 6014
    return-object p0
.end method

.method setSwiftServerIp(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 6023
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 6024
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->swiftServerIp_:Ljava/lang/Object;

    .line 6026
    return-void
.end method

.method public setSwiftTcpPort(I)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 6037
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 6038
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->swiftTcpPort_:I

    .line 6040
    return-object p0
.end method

.method public setSwiftUdpPort(I)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 6058
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 6059
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->swiftUdpPort_:I

    .line 6061
    return-object p0
.end method

.method public setType(I)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 5845
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5846
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->type_:I

    .line 5848
    return-object p0
.end method

.method public setUsername(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 5780
    if-nez p1, :cond_0

    .line 5781
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5783
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5784
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->username_:Ljava/lang/Object;

    .line 5786
    return-object p0
.end method

.method setUsername(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 5795
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->bitField0_:I

    .line 5796
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->username_:Ljava/lang/Object;

    .line 5798
    return-void
.end method
