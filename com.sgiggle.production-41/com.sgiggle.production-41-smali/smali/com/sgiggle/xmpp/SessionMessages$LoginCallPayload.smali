.class public final Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LoginCallPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CALLID_FIELD_NUMBER:I = 0x8

.field public static final FROMUI_FIELD_NUMBER:I = 0x7

.field public static final NOTIFICATIONTIMESTAMP_FIELD_NUMBER:I = 0x6

.field public static final PEERNAME_FIELD_NUMBER:I = 0x3

.field public static final PRESENT_FIELD_NUMBER:I = 0x5

.field public static final PUSH_TYPE_FIELD_NUMBER:I = 0xd

.field public static final SESSIONID_FIELD_NUMBER:I = 0x9

.field public static final SWIFTSERVERIP_FIELD_NUMBER:I = 0xa

.field public static final SWIFTTCPPORT_FIELD_NUMBER:I = 0xb

.field public static final SWIFTUDPPORT_FIELD_NUMBER:I = 0xc

.field public static final TYPE_FIELD_NUMBER:I = 0x4

.field public static final USERNAME_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private callid_:Ljava/lang/Object;

.field private fromUI_:Z

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private notificationtimestamp_:I

.field private peername_:Ljava/lang/Object;

.field private present_:Z

.field private pushType_:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

.field private sessionid_:Ljava/lang/Object;

.field private swiftServerIp_:Ljava/lang/Object;

.field private swiftTcpPort_:I

.field private swiftUdpPort_:I

.field private type_:I

.field private username_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 6098
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    .line 6099
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->initFields()V

    .line 6100
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 4958
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 5227
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->memoizedIsInitialized:B

    .line 5292
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->memoizedSerializedSize:I

    .line 4959
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 4953
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 4960
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 5227
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->memoizedIsInitialized:B

    .line 5292
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->memoizedSerializedSize:I

    .line 4960
    return-void
.end method

.method static synthetic access$5602(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 4953
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$5702(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 4953
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->username_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$5802(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 4953
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->peername_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$5902(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 4953
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->type_:I

    return p1
.end method

.method static synthetic access$6002(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 4953
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->present_:Z

    return p1
.end method

.method static synthetic access$6102(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 4953
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->notificationtimestamp_:I

    return p1
.end method

.method static synthetic access$6202(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 4953
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->fromUI_:Z

    return p1
.end method

.method static synthetic access$6302(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 4953
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->callid_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$6402(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 4953
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->sessionid_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$6502(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 4953
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->swiftServerIp_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$6602(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 4953
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->swiftTcpPort_:I

    return p1
.end method

.method static synthetic access$6702(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 4953
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->swiftUdpPort_:I

    return p1
.end method

.method static synthetic access$6802(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 4953
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->pushType_:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    return-object p1
.end method

.method static synthetic access$6902(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 4953
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    return p1
.end method

.method private getCallidBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 5107
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->callid_:Ljava/lang/Object;

    .line 5108
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 5109
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 5111
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->callid_:Ljava/lang/Object;

    .line 5114
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;
    .locals 1

    .prologue
    .line 4964
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    return-object v0
.end method

.method private getPeernameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 5035
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->peername_:Ljava/lang/Object;

    .line 5036
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 5037
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 5039
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->peername_:Ljava/lang/Object;

    .line 5042
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getSessionidBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 5139
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->sessionid_:Ljava/lang/Object;

    .line 5140
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 5141
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 5143
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->sessionid_:Ljava/lang/Object;

    .line 5146
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getSwiftServerIpBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 5171
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->swiftServerIp_:Ljava/lang/Object;

    .line 5172
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 5173
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 5175
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->swiftServerIp_:Ljava/lang/Object;

    .line 5178
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getUsernameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 5003
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->username_:Ljava/lang/Object;

    .line 5004
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 5005
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 5007
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->username_:Ljava/lang/Object;

    .line 5010
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5213
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 5214
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->username_:Ljava/lang/Object;

    .line 5215
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->peername_:Ljava/lang/Object;

    .line 5216
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->type_:I

    .line 5217
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->present_:Z

    .line 5218
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->notificationtimestamp_:I

    .line 5219
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->fromUI_:Z

    .line 5220
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->callid_:Ljava/lang/Object;

    .line 5221
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->sessionid_:Ljava/lang/Object;

    .line 5222
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->swiftServerIp_:Ljava/lang/Object;

    .line 5223
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->swiftTcpPort_:I

    .line 5224
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->swiftUdpPort_:I

    .line 5225
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->UNKNOWN_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->pushType_:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    .line 5226
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1

    .prologue
    .line 5426
    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->access$5400()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 5429
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5395
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    move-result-object v0

    .line 5396
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5397
    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->access$5300(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    move-result-object v0

    .line 5399
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5406
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    move-result-object v0

    .line 5407
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5408
    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->access$5300(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    move-result-object v0

    .line 5410
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 5362
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->access$5300(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 5368
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->access$5300(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5416
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->access$5300(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5422
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->access$5300(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5384
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->access$5300(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5390
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->access$5300(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 5373
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->access$5300(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 5379
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;->access$5300(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 4979
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCallid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 5093
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->callid_:Ljava/lang/Object;

    .line 5094
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 5095
    check-cast v0, Ljava/lang/String;

    .line 5103
    :goto_0
    return-object v0

    .line 5097
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 5099
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 5100
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5101
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->callid_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 5103
    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 4953
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;
    .locals 1

    .prologue
    .line 4968
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;

    return-object v0
.end method

.method public getFromUI()Z
    .locals 1

    .prologue
    .line 5083
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->fromUI_:Z

    return v0
.end method

.method public getNotificationtimestamp()I
    .locals 1

    .prologue
    .line 5073
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->notificationtimestamp_:I

    return v0
.end method

.method public getPeername()Ljava/lang/String;
    .locals 2

    .prologue
    .line 5021
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->peername_:Ljava/lang/Object;

    .line 5022
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 5023
    check-cast v0, Ljava/lang/String;

    .line 5031
    :goto_0
    return-object v0

    .line 5025
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 5027
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 5028
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5029
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->peername_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 5031
    goto :goto_0
.end method

.method public getPresent()Z
    .locals 1

    .prologue
    .line 5063
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->present_:Z

    return v0
.end method

.method public getPushType()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;
    .locals 1

    .prologue
    .line 5209
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->pushType_:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 5294
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->memoizedSerializedSize:I

    .line 5295
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 5351
    :goto_0
    return v0

    .line 5297
    :cond_0
    const/4 v0, 0x0

    .line 5298
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 5299
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5302
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 5303
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getUsernameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5306
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 5307
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getPeernameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5310
    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_4

    .line 5311
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->type_:I

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5314
    :cond_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 5315
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->present_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 5318
    :cond_5
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    .line 5319
    const/4 v1, 0x6

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->notificationtimestamp_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5322
    :cond_6
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    .line 5323
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->fromUI_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 5326
    :cond_7
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_8

    .line 5327
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getCallidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5330
    :cond_8
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_9

    .line 5331
    const/16 v1, 0x9

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getSessionidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5334
    :cond_9
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_a

    .line 5335
    const/16 v1, 0xa

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getSwiftServerIpBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5338
    :cond_a
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x400

    const/16 v2, 0x400

    if-ne v1, v2, :cond_b

    .line 5339
    const/16 v1, 0xb

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->swiftTcpPort_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5342
    :cond_b
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x800

    const/16 v2, 0x800

    if-ne v1, v2, :cond_c

    .line 5343
    const/16 v1, 0xc

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->swiftUdpPort_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5346
    :cond_c
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x1000

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_d

    .line 5347
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->pushType_:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5350
    :cond_d
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->memoizedSerializedSize:I

    goto/16 :goto_0
.end method

.method public getSessionid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 5125
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->sessionid_:Ljava/lang/Object;

    .line 5126
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 5127
    check-cast v0, Ljava/lang/String;

    .line 5135
    :goto_0
    return-object v0

    .line 5129
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 5131
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 5132
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5133
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->sessionid_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 5135
    goto :goto_0
.end method

.method public getSwiftServerIp()Ljava/lang/String;
    .locals 2

    .prologue
    .line 5157
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->swiftServerIp_:Ljava/lang/Object;

    .line 5158
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 5159
    check-cast v0, Ljava/lang/String;

    .line 5167
    :goto_0
    return-object v0

    .line 5161
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 5163
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 5164
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5165
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->swiftServerIp_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 5167
    goto :goto_0
.end method

.method public getSwiftTcpPort()I
    .locals 1

    .prologue
    .line 5189
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->swiftTcpPort_:I

    return v0
.end method

.method public getSwiftUdpPort()I
    .locals 1

    .prologue
    .line 5199
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->swiftUdpPort_:I

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 5053
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->type_:I

    return v0
.end method

.method public getUsername()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4989
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->username_:Ljava/lang/Object;

    .line 4990
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 4991
    check-cast v0, Ljava/lang/String;

    .line 4999
    :goto_0
    return-object v0

    .line 4993
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 4995
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 4996
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4997
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->username_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 4999
    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 4976
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCallid()Z
    .locals 2

    .prologue
    .line 5090
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFromUI()Z
    .locals 2

    .prologue
    .line 5080
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNotificationtimestamp()Z
    .locals 2

    .prologue
    .line 5070
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPeername()Z
    .locals 2

    .prologue
    .line 5018
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPresent()Z
    .locals 2

    .prologue
    .line 5060
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPushType()Z
    .locals 2

    .prologue
    .line 5206
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSessionid()Z
    .locals 2

    .prologue
    .line 5122
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSwiftServerIp()Z
    .locals 2

    .prologue
    .line 5154
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSwiftTcpPort()Z
    .locals 2

    .prologue
    .line 5186
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSwiftUdpPort()Z
    .locals 2

    .prologue
    .line 5196
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    .prologue
    .line 5050
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUsername()Z
    .locals 2

    .prologue
    .line 4986
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5229
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->memoizedIsInitialized:B

    .line 5230
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 5245
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 5230
    goto :goto_0

    .line 5232
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 5233
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 5234
    goto :goto_0

    .line 5236
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->hasPeername()Z

    move-result v0

    if-nez v0, :cond_3

    .line 5237
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 5238
    goto :goto_0

    .line 5240
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    .line 5241
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 5242
    goto :goto_0

    .line 5244
    :cond_4
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 5245
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 4953
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1

    .prologue
    .line 5427
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 4953
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;
    .locals 1

    .prologue
    .line 5431
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;)Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 5356
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 5250
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getSerializedSize()I

    .line 5251
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 5252
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 5254
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 5255
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getUsernameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 5257
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 5258
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getPeernameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 5260
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 5261
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->type_:I

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 5263
    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 5264
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->present_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 5266
    :cond_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 5267
    const/4 v0, 0x6

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->notificationtimestamp_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 5269
    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 5270
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->fromUI_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 5272
    :cond_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 5273
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getCallidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 5275
    :cond_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    .line 5276
    const/16 v0, 0x9

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getSessionidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 5278
    :cond_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_9

    .line 5279
    const/16 v0, 0xa

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->getSwiftServerIpBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 5281
    :cond_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_a

    .line 5282
    const/16 v0, 0xb

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->swiftTcpPort_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 5284
    :cond_a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_b

    .line 5285
    const/16 v0, 0xc

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->swiftUdpPort_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 5287
    :cond_b
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_c

    .line 5288
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayload;->pushType_:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 5290
    :cond_c
    return-void
.end method
