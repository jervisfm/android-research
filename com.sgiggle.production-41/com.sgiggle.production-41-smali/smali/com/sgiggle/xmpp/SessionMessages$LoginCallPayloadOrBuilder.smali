.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$LoginCallPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "LoginCallPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getCallid()Ljava/lang/String;
.end method

.method public abstract getFromUI()Z
.end method

.method public abstract getNotificationtimestamp()I
.end method

.method public abstract getPeername()Ljava/lang/String;
.end method

.method public abstract getPresent()Z
.end method

.method public abstract getPushType()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;
.end method

.method public abstract getSessionid()Ljava/lang/String;
.end method

.method public abstract getSwiftServerIp()Ljava/lang/String;
.end method

.method public abstract getSwiftTcpPort()I
.end method

.method public abstract getSwiftUdpPort()I
.end method

.method public abstract getType()I
.end method

.method public abstract getUsername()Ljava/lang/String;
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasCallid()Z
.end method

.method public abstract hasFromUI()Z
.end method

.method public abstract hasNotificationtimestamp()Z
.end method

.method public abstract hasPeername()Z
.end method

.method public abstract hasPresent()Z
.end method

.method public abstract hasPushType()Z
.end method

.method public abstract hasSessionid()Z
.end method

.method public abstract hasSwiftServerIp()Z
.end method

.method public abstract hasSwiftTcpPort()Z
.end method

.method public abstract hasSwiftUdpPort()Z
.end method

.method public abstract hasType()Z
.end method

.method public abstract hasUsername()Z
.end method
