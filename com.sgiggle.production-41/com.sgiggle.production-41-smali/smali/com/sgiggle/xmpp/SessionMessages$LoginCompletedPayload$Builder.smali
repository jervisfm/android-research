.class public final Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private accessAddressBook_:Z

.field private alerts_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;"
        }
    .end annotation
.end field

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private contacts_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private message_:Ljava/lang/Object;

.field private registrationSubmitted_:Z

.field private specifiedEmptyListPrompt_:Ljava/lang/Object;

.field private version_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 29118
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 29352
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 29416
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->message_:Ljava/lang/Object;

    .line 29452
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->version_:Ljava/lang/Object;

    .line 29488
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->contacts_:Ljava/util/List;

    .line 29577
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->alerts_:Ljava/util/List;

    .line 29687
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->specifiedEmptyListPrompt_:Ljava/lang/Object;

    .line 29119
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->maybeForceBuilderInitialization()V

    .line 29120
    return-void
.end method

.method static synthetic access$36700(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 29113
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$36800()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1

    .prologue
    .line 29113
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 29167
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    move-result-object v0

    .line 29168
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 29169
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 29172
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1

    .prologue
    .line 29125
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureAlertsIsMutable()V
    .locals 2

    .prologue
    .line 29580
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    .line 29581
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->alerts_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->alerts_:Ljava/util/List;

    .line 29582
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29584
    :cond_0
    return-void
.end method

.method private ensureContactsIsMutable()V
    .locals 2

    .prologue
    .line 29491
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    .line 29492
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->contacts_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->contacts_:Ljava/util/List;

    .line 29493
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29495
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 29123
    return-void
.end method


# virtual methods
.method public addAlerts(ILcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 29640
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->ensureAlertsIsMutable()V

    .line 29641
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->alerts_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 29643
    return-object p0
.end method

.method public addAlerts(ILcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 29623
    if-nez p2, :cond_0

    .line 29624
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 29626
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->ensureAlertsIsMutable()V

    .line 29627
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->alerts_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 29629
    return-object p0
.end method

.method public addAlerts(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 29633
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->ensureAlertsIsMutable()V

    .line 29634
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->alerts_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 29636
    return-object p0
.end method

.method public addAlerts(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 29613
    if-nez p1, :cond_0

    .line 29614
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 29616
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->ensureAlertsIsMutable()V

    .line 29617
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->alerts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 29619
    return-object p0
.end method

.method public addAllAlerts(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;"
        }
    .end annotation

    .prologue
    .line 29647
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->ensureAlertsIsMutable()V

    .line 29648
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->alerts_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 29650
    return-object p0
.end method

.method public addAllContacts(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;"
        }
    .end annotation

    .prologue
    .line 29558
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->ensureContactsIsMutable()V

    .line 29559
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->contacts_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 29561
    return-object p0
.end method

.method public addContacts(ILcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 29551
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->ensureContactsIsMutable()V

    .line 29552
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->contacts_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 29554
    return-object p0
.end method

.method public addContacts(ILcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 29534
    if-nez p2, :cond_0

    .line 29535
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 29537
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->ensureContactsIsMutable()V

    .line 29538
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 29540
    return-object p0
.end method

.method public addContacts(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 29544
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->ensureContactsIsMutable()V

    .line 29545
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->contacts_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 29547
    return-object p0
.end method

.method public addContacts(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 29524
    if-nez p1, :cond_0

    .line 29525
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 29527
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->ensureContactsIsMutable()V

    .line 29528
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 29530
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 29113
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;
    .locals 2

    .prologue
    .line 29158
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    move-result-object v0

    .line 29159
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 29160
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 29162
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 29113
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;
    .locals 5

    .prologue
    .line 29176
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 29177
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29178
    const/4 v2, 0x0

    .line 29179
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 29180
    or-int/lit8 v2, v2, 0x1

    .line 29182
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->access$37002(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 29183
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 29184
    or-int/lit8 v2, v2, 0x2

    .line 29186
    :cond_1
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->accessAddressBook_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->accessAddressBook_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->access$37102(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;Z)Z

    .line 29187
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 29188
    or-int/lit8 v2, v2, 0x4

    .line 29190
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->message_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->message_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->access$37202(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29191
    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 29192
    or-int/lit8 v2, v2, 0x8

    .line 29194
    :cond_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->version_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->version_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->access$37302(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29195
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 29196
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->contacts_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->contacts_:Ljava/util/List;

    .line 29197
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x11

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29199
    :cond_4
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->contacts_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->contacts_:Ljava/util/List;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->access$37402(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;Ljava/util/List;)Ljava/util/List;

    .line 29200
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 29201
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->alerts_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->alerts_:Ljava/util/List;

    .line 29202
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x21

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29204
    :cond_5
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->alerts_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->alerts_:Ljava/util/List;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->access$37502(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;Ljava/util/List;)Ljava/util/List;

    .line 29205
    and-int/lit8 v3, v1, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    .line 29206
    or-int/lit8 v2, v2, 0x10

    .line 29208
    :cond_6
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->registrationSubmitted_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->registrationSubmitted_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->access$37602(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;Z)Z

    .line 29209
    and-int/lit16 v1, v1, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_7

    .line 29210
    or-int/lit8 v1, v2, 0x20

    .line 29212
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->specifiedEmptyListPrompt_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->specifiedEmptyListPrompt_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->access$37702(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29213
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->access$37802(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;I)I

    .line 29214
    return-object v0

    :cond_7
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 29113
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 29113
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 29129
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 29130
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 29131
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29132
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->accessAddressBook_:Z

    .line 29133
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29134
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->message_:Ljava/lang/Object;

    .line 29135
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29136
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->version_:Ljava/lang/Object;

    .line 29137
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29138
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->contacts_:Ljava/util/List;

    .line 29139
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29140
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->alerts_:Ljava/util/List;

    .line 29141
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29142
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->registrationSubmitted_:Z

    .line 29143
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29144
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->specifiedEmptyListPrompt_:Ljava/lang/Object;

    .line 29145
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29146
    return-object p0
.end method

.method public clearAccessAddressBook()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1

    .prologue
    .line 29409
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29410
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->accessAddressBook_:Z

    .line 29412
    return-object p0
.end method

.method public clearAlerts()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1

    .prologue
    .line 29653
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->alerts_:Ljava/util/List;

    .line 29654
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29656
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1

    .prologue
    .line 29388
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 29390
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29391
    return-object p0
.end method

.method public clearContacts()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1

    .prologue
    .line 29564
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->contacts_:Ljava/util/List;

    .line 29565
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29567
    return-object p0
.end method

.method public clearMessage()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1

    .prologue
    .line 29440
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29441
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->message_:Ljava/lang/Object;

    .line 29443
    return-object p0
.end method

.method public clearRegistrationSubmitted()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1

    .prologue
    .line 29680
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29681
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->registrationSubmitted_:Z

    .line 29683
    return-object p0
.end method

.method public clearSpecifiedEmptyListPrompt()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1

    .prologue
    .line 29711
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29712
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->getSpecifiedEmptyListPrompt()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->specifiedEmptyListPrompt_:Ljava/lang/Object;

    .line 29714
    return-object p0
.end method

.method public clearVersion()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1

    .prologue
    .line 29476
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29477
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->getVersion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->version_:Ljava/lang/Object;

    .line 29479
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 29113
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 29113
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 29113
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 2

    .prologue
    .line 29150
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 29113
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAccessAddressBook()Z
    .locals 1

    .prologue
    .line 29400
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->accessAddressBook_:Z

    return v0
.end method

.method public getAlerts(I)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 1
    .parameter

    .prologue
    .line 29593
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->alerts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    return-object v0
.end method

.method public getAlertsCount()I
    .locals 1

    .prologue
    .line 29590
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->alerts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getAlertsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29587
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->alerts_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 29357
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getContacts(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter

    .prologue
    .line 29504
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getContactsCount()I
    .locals 1

    .prologue
    .line 29501
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getContactsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29498
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->contacts_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 29113
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 29113
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;
    .locals 1

    .prologue
    .line 29154
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    move-result-object v0

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 29421
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->message_:Ljava/lang/Object;

    .line 29422
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 29423
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 29424
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->message_:Ljava/lang/Object;

    .line 29427
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getRegistrationSubmitted()Z
    .locals 1

    .prologue
    .line 29671
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->registrationSubmitted_:Z

    return v0
.end method

.method public getSpecifiedEmptyListPrompt()Ljava/lang/String;
    .locals 2

    .prologue
    .line 29692
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->specifiedEmptyListPrompt_:Ljava/lang/Object;

    .line 29693
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 29694
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 29695
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->specifiedEmptyListPrompt_:Ljava/lang/Object;

    .line 29698
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 29457
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->version_:Ljava/lang/Object;

    .line 29458
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 29459
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 29460
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->version_:Ljava/lang/Object;

    .line 29463
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasAccessAddressBook()Z
    .locals 2

    .prologue
    .line 29397
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 29354
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMessage()Z
    .locals 2

    .prologue
    .line 29418
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRegistrationSubmitted()Z
    .locals 2

    .prologue
    .line 29668
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSpecifiedEmptyListPrompt()Z
    .locals 2

    .prologue
    .line 29689
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVersion()Z
    .locals 2

    .prologue
    .line 29454
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 29261
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 29279
    :goto_0
    return v0

    .line 29265
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->hasAccessAddressBook()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 29267
    goto :goto_0

    .line 29269
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    .line 29271
    goto :goto_0

    :cond_2
    move v0, v2

    .line 29273
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->getContactsCount()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 29274
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->getContacts(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_3

    move v0, v2

    .line 29276
    goto :goto_0

    .line 29273
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 29279
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 29376
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 29378
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 29384
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29385
    return-object p0

    .line 29381
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29113
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 29113
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29113
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29287
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 29288
    sparse-switch v0, :sswitch_data_0

    .line 29293
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 29295
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 29291
    goto :goto_1

    .line 29300
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 29301
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 29302
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 29304
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 29305
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    goto :goto_0

    .line 29309
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29310
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->accessAddressBook_:Z

    goto :goto_0

    .line 29314
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29315
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->message_:Ljava/lang/Object;

    goto :goto_0

    .line 29319
    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29320
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->version_:Ljava/lang/Object;

    goto :goto_0

    .line 29324
    :sswitch_5
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    .line 29325
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 29326
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->addContacts(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    goto :goto_0

    .line 29330
    :sswitch_6
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    .line 29331
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 29332
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->addAlerts(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    goto :goto_0

    .line 29336
    :sswitch_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29337
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->registrationSubmitted_:Z

    goto :goto_0

    .line 29341
    :sswitch_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29342
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->specifiedEmptyListPrompt_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 29288
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 29218
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 29257
    :goto_0
    return-object v0

    .line 29219
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 29220
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    .line 29222
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->hasAccessAddressBook()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 29223
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->getAccessAddressBook()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->setAccessAddressBook(Z)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    .line 29225
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->hasMessage()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 29226
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->setMessage(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    .line 29228
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->hasVersion()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 29229
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->getVersion()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->setVersion(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    .line 29231
    :cond_4
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->contacts_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->access$37400(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 29232
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 29233
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->contacts_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->access$37400(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->contacts_:Ljava/util/List;

    .line 29234
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29241
    :cond_5
    :goto_1
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->alerts_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->access$37500(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 29242
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->alerts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 29243
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->alerts_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->access$37500(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->alerts_:Ljava/util/List;

    .line 29244
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29251
    :cond_6
    :goto_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->hasRegistrationSubmitted()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 29252
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->getRegistrationSubmitted()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->setRegistrationSubmitted(Z)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    .line 29254
    :cond_7
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->hasSpecifiedEmptyListPrompt()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 29255
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->getSpecifiedEmptyListPrompt()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->setSpecifiedEmptyListPrompt(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    :cond_8
    move-object v0, p0

    .line 29257
    goto/16 :goto_0

    .line 29236
    :cond_9
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->ensureContactsIsMutable()V

    .line 29237
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->contacts_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->contacts_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->access$37400(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 29246
    :cond_a
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->ensureAlertsIsMutable()V

    .line 29247
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->alerts_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->alerts_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->access$37500(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public removeAlerts(I)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 29659
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->ensureAlertsIsMutable()V

    .line 29660
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->alerts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 29662
    return-object p0
.end method

.method public removeContacts(I)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 29570
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->ensureContactsIsMutable()V

    .line 29571
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 29573
    return-object p0
.end method

.method public setAccessAddressBook(Z)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 29403
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29404
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->accessAddressBook_:Z

    .line 29406
    return-object p0
.end method

.method public setAlerts(ILcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 29607
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->ensureAlertsIsMutable()V

    .line 29608
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->alerts_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 29610
    return-object p0
.end method

.method public setAlerts(ILcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 29597
    if-nez p2, :cond_0

    .line 29598
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 29600
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->ensureAlertsIsMutable()V

    .line 29601
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->alerts_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 29603
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 29370
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 29372
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29373
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 29360
    if-nez p1, :cond_0

    .line 29361
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 29363
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 29365
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29366
    return-object p0
.end method

.method public setContacts(ILcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 29518
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->ensureContactsIsMutable()V

    .line 29519
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->contacts_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 29521
    return-object p0
.end method

.method public setContacts(ILcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 29508
    if-nez p2, :cond_0

    .line 29509
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 29511
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->ensureContactsIsMutable()V

    .line 29512
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 29514
    return-object p0
.end method

.method public setMessage(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 29431
    if-nez p1, :cond_0

    .line 29432
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 29434
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29435
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->message_:Ljava/lang/Object;

    .line 29437
    return-object p0
.end method

.method setMessage(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 29446
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29447
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->message_:Ljava/lang/Object;

    .line 29449
    return-void
.end method

.method public setRegistrationSubmitted(Z)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 29674
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29675
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->registrationSubmitted_:Z

    .line 29677
    return-object p0
.end method

.method public setSpecifiedEmptyListPrompt(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 29702
    if-nez p1, :cond_0

    .line 29703
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 29705
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29706
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->specifiedEmptyListPrompt_:Ljava/lang/Object;

    .line 29708
    return-object p0
.end method

.method setSpecifiedEmptyListPrompt(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 29717
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29718
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->specifiedEmptyListPrompt_:Ljava/lang/Object;

    .line 29720
    return-void
.end method

.method public setVersion(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 29467
    if-nez p1, :cond_0

    .line 29468
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 29470
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29471
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->version_:Ljava/lang/Object;

    .line 29473
    return-object p0
.end method

.method setVersion(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 29482
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->bitField0_:I

    .line 29483
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->version_:Ljava/lang/Object;

    .line 29485
    return-void
.end method
