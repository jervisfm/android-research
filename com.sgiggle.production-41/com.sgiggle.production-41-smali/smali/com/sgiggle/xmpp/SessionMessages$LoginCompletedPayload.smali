.class public final Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LoginCompletedPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    }
.end annotation


# static fields
.field public static final ACCESSADDRESSBOOK_FIELD_NUMBER:I = 0x2

.field public static final ALERTS_FIELD_NUMBER:I = 0x6

.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CONTACTS_FIELD_NUMBER:I = 0x5

.field public static final MESSAGE_FIELD_NUMBER:I = 0x3

.field public static final REGISTRATIONSUBMITTED_FIELD_NUMBER:I = 0x7

.field public static final SPECIFIED_EMPTY_LIST_PROMPT_FIELD_NUMBER:I = 0x8

.field public static final VERSION_FIELD_NUMBER:I = 0x4

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;


# instance fields
.field private accessAddressBook_:Z

.field private alerts_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;"
        }
    .end annotation
.end field

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private contacts_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private message_:Ljava/lang/Object;

.field private registrationSubmitted_:Z

.field private specifiedEmptyListPrompt_:Ljava/lang/Object;

.field private version_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 29726
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    .line 29727
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->initFields()V

    .line 29728
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 28744
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 28936
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->memoizedIsInitialized:B

    .line 28992
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->memoizedSerializedSize:I

    .line 28745
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 28739
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 28746
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 28936
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->memoizedIsInitialized:B

    .line 28992
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->memoizedSerializedSize:I

    .line 28746
    return-void
.end method

.method static synthetic access$37002(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 28739
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$37102(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 28739
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->accessAddressBook_:Z

    return p1
.end method

.method static synthetic access$37202(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 28739
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->message_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$37302(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 28739
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->version_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$37400(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 28739
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->contacts_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$37402(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 28739
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->contacts_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$37500(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 28739
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->alerts_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$37502(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 28739
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->alerts_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$37602(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 28739
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->registrationSubmitted_:Z

    return p1
.end method

.method static synthetic access$37702(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 28739
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->specifiedEmptyListPrompt_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$37802(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 28739
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;
    .locals 1

    .prologue
    .line 28750
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    return-object v0
.end method

.method private getMessageBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 28799
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->message_:Ljava/lang/Object;

    .line 28800
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 28801
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 28803
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->message_:Ljava/lang/Object;

    .line 28806
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getSpecifiedEmptyListPromptBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 28915
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->specifiedEmptyListPrompt_:Ljava/lang/Object;

    .line 28916
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 28917
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 28919
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->specifiedEmptyListPrompt_:Ljava/lang/Object;

    .line 28922
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getVersionBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 28831
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->version_:Ljava/lang/Object;

    .line 28832
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 28833
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 28835
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->version_:Ljava/lang/Object;

    .line 28838
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 28927
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 28928
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->accessAddressBook_:Z

    .line 28929
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->message_:Ljava/lang/Object;

    .line 28930
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->version_:Ljava/lang/Object;

    .line 28931
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->contacts_:Ljava/util/List;

    .line 28932
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->alerts_:Ljava/util/List;

    .line 28933
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->registrationSubmitted_:Z

    .line 28934
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->specifiedEmptyListPrompt_:Ljava/lang/Object;

    .line 28935
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1

    .prologue
    .line 29106
    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->access$36800()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 29109
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29075
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    move-result-object v0

    .line 29076
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 29077
    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->access$36700(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    move-result-object v0

    .line 29079
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29086
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    move-result-object v0

    .line 29087
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 29088
    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->access$36700(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    move-result-object v0

    .line 29090
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 29042
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->access$36700(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 29048
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->access$36700(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29096
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->access$36700(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29102
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->access$36700(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29064
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->access$36700(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29070
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->access$36700(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 29053
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->access$36700(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 29059
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;->access$36700(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAccessAddressBook()Z
    .locals 1

    .prologue
    .line 28775
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->accessAddressBook_:Z

    return v0
.end method

.method public getAlerts(I)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 1
    .parameter

    .prologue
    .line 28877
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->alerts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    return-object v0
.end method

.method public getAlertsCount()I
    .locals 1

    .prologue
    .line 28874
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->alerts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getAlertsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28867
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->alerts_:Ljava/util/List;

    return-object v0
.end method

.method public getAlertsOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlertOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 28881
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->alerts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlertOrBuilder;

    return-object v0
.end method

.method public getAlertsOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlertOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28871
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->alerts_:Ljava/util/List;

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 28765
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getContacts(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter

    .prologue
    .line 28856
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getContactsCount()I
    .locals 1

    .prologue
    .line 28853
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->contacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getContactsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28846
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->contacts_:Ljava/util/List;

    return-object v0
.end method

.method public getContactsOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 28860
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;

    return-object v0
.end method

.method public getContactsOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28850
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->contacts_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 28739
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;
    .locals 1

    .prologue
    .line 28754
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 28785
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->message_:Ljava/lang/Object;

    .line 28786
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 28787
    check-cast v0, Ljava/lang/String;

    .line 28795
    :goto_0
    return-object v0

    .line 28789
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 28791
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 28792
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 28793
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->message_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 28795
    goto :goto_0
.end method

.method public getRegistrationSubmitted()Z
    .locals 1

    .prologue
    .line 28891
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->registrationSubmitted_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 28994
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->memoizedSerializedSize:I

    .line 28995
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 29031
    :goto_0
    return v0

    .line 28998
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_8

    .line 28999
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v4

    .line 29002
    :goto_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_1

    .line 29003
    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->accessAddressBook_:Z

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 29006
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_2

    .line 29007
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->getMessageBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29010
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v6, :cond_3

    .line 29011
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->getVersionBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    move v1, v4

    move v2, v0

    .line 29014
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->contacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 29015
    const/4 v3, 0x5

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->contacts_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 29014
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    :cond_4
    move v1, v4

    .line 29018
    :goto_3
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->alerts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 29019
    const/4 v3, 0x6

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->alerts_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 29018
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_3

    .line 29022
    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_7

    .line 29023
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->registrationSubmitted_:Z

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v0

    add-int/2addr v0, v2

    .line 29026
    :goto_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    .line 29027
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->getSpecifiedEmptyListPromptBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v6, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29030
    :cond_6
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->memoizedSerializedSize:I

    goto/16 :goto_0

    :cond_7
    move v0, v2

    goto :goto_4

    :cond_8
    move v0, v4

    goto/16 :goto_1
.end method

.method public getSpecifiedEmptyListPrompt()Ljava/lang/String;
    .locals 2

    .prologue
    .line 28901
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->specifiedEmptyListPrompt_:Ljava/lang/Object;

    .line 28902
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 28903
    check-cast v0, Ljava/lang/String;

    .line 28911
    :goto_0
    return-object v0

    .line 28905
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 28907
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 28908
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 28909
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->specifiedEmptyListPrompt_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 28911
    goto :goto_0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 28817
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->version_:Ljava/lang/Object;

    .line 28818
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 28819
    check-cast v0, Ljava/lang/String;

    .line 28827
    :goto_0
    return-object v0

    .line 28821
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 28823
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 28824
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 28825
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->version_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 28827
    goto :goto_0
.end method

.method public hasAccessAddressBook()Z
    .locals 2

    .prologue
    .line 28772
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 28762
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMessage()Z
    .locals 2

    .prologue
    .line 28782
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRegistrationSubmitted()Z
    .locals 2

    .prologue
    .line 28888
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSpecifiedEmptyListPrompt()Z
    .locals 2

    .prologue
    .line 28898
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVersion()Z
    .locals 2

    .prologue
    .line 28814
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 28938
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->memoizedIsInitialized:B

    .line 28939
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 28960
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 28939
    goto :goto_0

    .line 28941
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 28942
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 28943
    goto :goto_0

    .line 28945
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->hasAccessAddressBook()Z

    move-result v0

    if-nez v0, :cond_3

    .line 28946
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 28947
    goto :goto_0

    .line 28949
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    .line 28950
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 28951
    goto :goto_0

    :cond_4
    move v0, v2

    .line 28953
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->getContactsCount()I

    move-result v1

    if-ge v0, v1, :cond_6

    .line 28954
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->getContacts(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_5

    .line 28955
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 28956
    goto :goto_0

    .line 28953
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 28959
    :cond_6
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 28960
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 28739
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1

    .prologue
    .line 29107
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 28739
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;
    .locals 1

    .prologue
    .line 29111
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;)Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 29036
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 6
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 28965
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->getSerializedSize()I

    .line 28966
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 28967
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 28969
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 28970
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->accessAddressBook_:Z

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 28972
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 28973
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->getMessageBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 28975
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_3

    .line 28976
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->getVersionBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_3
    move v1, v3

    .line 28978
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->contacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 28979
    const/4 v2, 0x5

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->contacts_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 28978
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_4
    move v1, v3

    .line 28981
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->alerts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 28982
    const/4 v2, 0x6

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->alerts_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 28981
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 28984
    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_6

    .line 28985
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->registrationSubmitted_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 28987
    :cond_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_7

    .line 28988
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->getSpecifiedEmptyListPromptBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 28990
    :cond_7
    return-void
.end method
