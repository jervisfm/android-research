.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "LoginCompletedPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getAccessAddressBook()Z
.end method

.method public abstract getAlerts(I)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
.end method

.method public abstract getAlertsCount()I
.end method

.method public abstract getAlertsList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getContacts(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
.end method

.method public abstract getContactsCount()I
.end method

.method public abstract getContactsList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getMessage()Ljava/lang/String;
.end method

.method public abstract getRegistrationSubmitted()Z
.end method

.method public abstract getSpecifiedEmptyListPrompt()Ljava/lang/String;
.end method

.method public abstract getVersion()Ljava/lang/String;
.end method

.method public abstract hasAccessAddressBook()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasMessage()Z
.end method

.method public abstract hasRegistrationSubmitted()Z
.end method

.method public abstract hasSpecifiedEmptyListPrompt()Z
.end method

.method public abstract hasVersion()Z
.end method
