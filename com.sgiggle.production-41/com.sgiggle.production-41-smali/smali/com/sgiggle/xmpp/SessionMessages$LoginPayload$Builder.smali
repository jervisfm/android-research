.class public final Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$LoginPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$LoginPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private canBindUdp_:Z

.field private display_:I

.field private domain_:Ljava/lang/Object;

.field private fromUI_:Z

.field private hostname_:Ljava/lang/Object;

.field private password_:Ljava/lang/Object;

.field private port_:I

.field private receivedPush_:Z

.field private resource_:Ljava/lang/Object;

.field private username_:Ljava/lang/Object;

.field private validationcode_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 3882
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 4144
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 4187
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->hostname_:Ljava/lang/Object;

    .line 4244
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->resource_:Ljava/lang/Object;

    .line 4280
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->username_:Ljava/lang/Object;

    .line 4316
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->password_:Ljava/lang/Object;

    .line 4352
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->domain_:Ljava/lang/Object;

    .line 4388
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->validationcode_:Ljava/lang/Object;

    .line 3883
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->maybeForceBuilderInitialization()V

    .line 3884
    return-void
.end method

.method static synthetic access$3100(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 3877
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3200()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1

    .prologue
    .line 3877
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 3939
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    move-result-object v0

    .line 3940
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3941
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 3944
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1

    .prologue
    .line 3889
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 3887
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 3877
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;
    .locals 2

    .prologue
    .line 3930
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    move-result-object v0

    .line 3931
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3932
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 3934
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 3877
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;
    .locals 5

    .prologue
    .line 3948
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 3949
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 3950
    const/4 v2, 0x0

    .line 3951
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 3952
    or-int/lit8 v2, v2, 0x1

    .line 3954
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->access$3402(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 3955
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 3956
    or-int/lit8 v2, v2, 0x2

    .line 3958
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->hostname_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->hostname_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->access$3502(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3959
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 3960
    or-int/lit8 v2, v2, 0x4

    .line 3962
    :cond_2
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->port_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->port_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->access$3602(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;I)I

    .line 3963
    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 3964
    or-int/lit8 v2, v2, 0x8

    .line 3966
    :cond_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->resource_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->resource_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->access$3702(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3967
    and-int/lit8 v3, v1, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 3968
    or-int/lit8 v2, v2, 0x10

    .line 3970
    :cond_4
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->username_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->username_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->access$3802(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3971
    and-int/lit8 v3, v1, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 3972
    or-int/lit8 v2, v2, 0x20

    .line 3974
    :cond_5
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->password_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->password_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->access$3902(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3975
    and-int/lit8 v3, v1, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    .line 3976
    or-int/lit8 v2, v2, 0x40

    .line 3978
    :cond_6
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->domain_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->domain_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->access$4002(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3979
    and-int/lit16 v3, v1, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    .line 3980
    or-int/lit16 v2, v2, 0x80

    .line 3982
    :cond_7
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->validationcode_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->validationcode_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->access$4102(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3983
    and-int/lit16 v3, v1, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_8

    .line 3984
    or-int/lit16 v2, v2, 0x100

    .line 3986
    :cond_8
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->fromUI_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->fromUI_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->access$4202(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;Z)Z

    .line 3987
    and-int/lit16 v3, v1, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_9

    .line 3988
    or-int/lit16 v2, v2, 0x200

    .line 3990
    :cond_9
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->receivedPush_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->receivedPush_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->access$4302(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;Z)Z

    .line 3991
    and-int/lit16 v3, v1, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_a

    .line 3992
    or-int/lit16 v2, v2, 0x400

    .line 3994
    :cond_a
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->canBindUdp_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->canBindUdp_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->access$4402(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;Z)Z

    .line 3995
    and-int/lit16 v1, v1, 0x800

    const/16 v3, 0x800

    if-ne v1, v3, :cond_b

    .line 3996
    or-int/lit16 v1, v2, 0x800

    .line 3998
    :goto_0
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->display_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->display_:I
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->access$4502(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;I)I

    .line 3999
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->access$4602(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;I)I

    .line 4000
    return-object v0

    :cond_b
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 3877
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 3877
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3893
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 3894
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 3895
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 3896
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->hostname_:Ljava/lang/Object;

    .line 3897
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 3898
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->port_:I

    .line 3899
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 3900
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->resource_:Ljava/lang/Object;

    .line 3901
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 3902
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->username_:Ljava/lang/Object;

    .line 3903
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 3904
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->password_:Ljava/lang/Object;

    .line 3905
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 3906
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->domain_:Ljava/lang/Object;

    .line 3907
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 3908
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->validationcode_:Ljava/lang/Object;

    .line 3909
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 3910
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->fromUI_:Z

    .line 3911
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 3912
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->receivedPush_:Z

    .line 3913
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 3914
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->canBindUdp_:Z

    .line 3915
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 3916
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->display_:I

    .line 3917
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 3918
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1

    .prologue
    .line 4180
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 4182
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4183
    return-object p0
.end method

.method public clearCanBindUdp()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1

    .prologue
    .line 4480
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4481
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->canBindUdp_:Z

    .line 4483
    return-object p0
.end method

.method public clearDisplay()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1

    .prologue
    .line 4501
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4502
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->display_:I

    .line 4504
    return-object p0
.end method

.method public clearDomain()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1

    .prologue
    .line 4376
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4377
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getDomain()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->domain_:Ljava/lang/Object;

    .line 4379
    return-object p0
.end method

.method public clearFromUI()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1

    .prologue
    .line 4438
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4439
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->fromUI_:Z

    .line 4441
    return-object p0
.end method

.method public clearHostname()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1

    .prologue
    .line 4211
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4212
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getHostname()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->hostname_:Ljava/lang/Object;

    .line 4214
    return-object p0
.end method

.method public clearPassword()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1

    .prologue
    .line 4340
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4341
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getPassword()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->password_:Ljava/lang/Object;

    .line 4343
    return-object p0
.end method

.method public clearPort()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1

    .prologue
    .line 4237
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4238
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->port_:I

    .line 4240
    return-object p0
.end method

.method public clearReceivedPush()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1

    .prologue
    .line 4459
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4460
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->receivedPush_:Z

    .line 4462
    return-object p0
.end method

.method public clearResource()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1

    .prologue
    .line 4268
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4269
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getResource()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->resource_:Ljava/lang/Object;

    .line 4271
    return-object p0
.end method

.method public clearUsername()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1

    .prologue
    .line 4304
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4305
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getUsername()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->username_:Ljava/lang/Object;

    .line 4307
    return-object p0
.end method

.method public clearValidationcode()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1

    .prologue
    .line 4412
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4413
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getValidationcode()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->validationcode_:Ljava/lang/Object;

    .line 4415
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 3877
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 3877
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 3877
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 2

    .prologue
    .line 3922
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 3877
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 4149
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCanBindUdp()Z
    .locals 1

    .prologue
    .line 4471
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->canBindUdp_:Z

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 3877
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 3877
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;
    .locals 1

    .prologue
    .line 3926
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDisplay()I
    .locals 1

    .prologue
    .line 4492
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->display_:I

    return v0
.end method

.method public getDomain()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4357
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->domain_:Ljava/lang/Object;

    .line 4358
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 4359
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 4360
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->domain_:Ljava/lang/Object;

    .line 4363
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getFromUI()Z
    .locals 1

    .prologue
    .line 4429
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->fromUI_:Z

    return v0
.end method

.method public getHostname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4192
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->hostname_:Ljava/lang/Object;

    .line 4193
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 4194
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 4195
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->hostname_:Ljava/lang/Object;

    .line 4198
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4321
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->password_:Ljava/lang/Object;

    .line 4322
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 4323
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 4324
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->password_:Ljava/lang/Object;

    .line 4327
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getPort()I
    .locals 1

    .prologue
    .line 4228
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->port_:I

    return v0
.end method

.method public getReceivedPush()Z
    .locals 1

    .prologue
    .line 4450
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->receivedPush_:Z

    return v0
.end method

.method public getResource()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4249
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->resource_:Ljava/lang/Object;

    .line 4250
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 4251
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 4252
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->resource_:Ljava/lang/Object;

    .line 4255
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getUsername()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4285
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->username_:Ljava/lang/Object;

    .line 4286
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 4287
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 4288
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->username_:Ljava/lang/Object;

    .line 4291
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getValidationcode()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4393
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->validationcode_:Ljava/lang/Object;

    .line 4394
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 4395
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 4396
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->validationcode_:Ljava/lang/Object;

    .line 4399
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 4146
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCanBindUdp()Z
    .locals 2

    .prologue
    .line 4468
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDisplay()Z
    .locals 2

    .prologue
    .line 4489
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDomain()Z
    .locals 2

    .prologue
    .line 4354
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFromUI()Z
    .locals 2

    .prologue
    .line 4426
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHostname()Z
    .locals 2

    .prologue
    .line 4189
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPassword()Z
    .locals 2

    .prologue
    .line 4318
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPort()Z
    .locals 2

    .prologue
    .line 4225
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasReceivedPush()Z
    .locals 2

    .prologue
    .line 4447
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasResource()Z
    .locals 2

    .prologue
    .line 4246
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUsername()Z
    .locals 2

    .prologue
    .line 4282
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasValidationcode()Z
    .locals 2

    .prologue
    .line 4390
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4045
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 4053
    :goto_0
    return v0

    .line 4049
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 4051
    goto :goto_0

    .line 4053
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 4168
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 4170
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 4176
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4177
    return-object p0

    .line 4173
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3877
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 3877
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3877
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4061
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 4062
    sparse-switch v0, :sswitch_data_0

    .line 4067
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 4069
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 4065
    goto :goto_1

    .line 4074
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 4075
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4076
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 4078
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 4079
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    goto :goto_0

    .line 4083
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4084
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->hostname_:Ljava/lang/Object;

    goto :goto_0

    .line 4088
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4089
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->port_:I

    goto :goto_0

    .line 4093
    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4094
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->resource_:Ljava/lang/Object;

    goto :goto_0

    .line 4098
    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4099
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->username_:Ljava/lang/Object;

    goto :goto_0

    .line 4103
    :sswitch_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4104
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->password_:Ljava/lang/Object;

    goto :goto_0

    .line 4108
    :sswitch_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4109
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->domain_:Ljava/lang/Object;

    goto :goto_0

    .line 4113
    :sswitch_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4114
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->validationcode_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 4118
    :sswitch_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4119
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->fromUI_:Z

    goto/16 :goto_0

    .line 4123
    :sswitch_a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4124
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->receivedPush_:Z

    goto/16 :goto_0

    .line 4128
    :sswitch_b
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4129
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->canBindUdp_:Z

    goto/16 :goto_0

    .line 4133
    :sswitch_c
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4134
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->display_:I

    goto/16 :goto_0

    .line 4062
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4004
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 4041
    :goto_0
    return-object v0

    .line 4005
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4006
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    .line 4008
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->hasHostname()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4009
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getHostname()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->setHostname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    .line 4011
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->hasPort()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4012
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getPort()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->setPort(I)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    .line 4014
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->hasResource()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 4015
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getResource()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->setResource(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    .line 4017
    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->hasUsername()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 4018
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getUsername()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->setUsername(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    .line 4020
    :cond_5
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->hasPassword()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 4021
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getPassword()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->setPassword(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    .line 4023
    :cond_6
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->hasDomain()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 4024
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getDomain()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->setDomain(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    .line 4026
    :cond_7
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->hasValidationcode()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 4027
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getValidationcode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->setValidationcode(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    .line 4029
    :cond_8
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->hasFromUI()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 4030
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getFromUI()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->setFromUI(Z)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    .line 4032
    :cond_9
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->hasReceivedPush()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 4033
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getReceivedPush()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->setReceivedPush(Z)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    .line 4035
    :cond_a
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->hasCanBindUdp()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 4036
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getCanBindUdp()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->setCanBindUdp(Z)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    .line 4038
    :cond_b
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->hasDisplay()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 4039
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getDisplay()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->setDisplay(I)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    :cond_c
    move-object v0, p0

    .line 4041
    goto/16 :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4162
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 4164
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4165
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4152
    if-nez p1, :cond_0

    .line 4153
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4155
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 4157
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4158
    return-object p0
.end method

.method public setCanBindUdp(Z)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4474
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4475
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->canBindUdp_:Z

    .line 4477
    return-object p0
.end method

.method public setDisplay(I)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4495
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4496
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->display_:I

    .line 4498
    return-object p0
.end method

.method public setDomain(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4367
    if-nez p1, :cond_0

    .line 4368
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4370
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4371
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->domain_:Ljava/lang/Object;

    .line 4373
    return-object p0
.end method

.method setDomain(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 4382
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4383
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->domain_:Ljava/lang/Object;

    .line 4385
    return-void
.end method

.method public setFromUI(Z)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4432
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4433
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->fromUI_:Z

    .line 4435
    return-object p0
.end method

.method public setHostname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4202
    if-nez p1, :cond_0

    .line 4203
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4205
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4206
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->hostname_:Ljava/lang/Object;

    .line 4208
    return-object p0
.end method

.method setHostname(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 4217
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4218
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->hostname_:Ljava/lang/Object;

    .line 4220
    return-void
.end method

.method public setPassword(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4331
    if-nez p1, :cond_0

    .line 4332
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4334
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4335
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->password_:Ljava/lang/Object;

    .line 4337
    return-object p0
.end method

.method setPassword(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 4346
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4347
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->password_:Ljava/lang/Object;

    .line 4349
    return-void
.end method

.method public setPort(I)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4231
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4232
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->port_:I

    .line 4234
    return-object p0
.end method

.method public setReceivedPush(Z)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4453
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4454
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->receivedPush_:Z

    .line 4456
    return-object p0
.end method

.method public setResource(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4259
    if-nez p1, :cond_0

    .line 4260
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4262
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4263
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->resource_:Ljava/lang/Object;

    .line 4265
    return-object p0
.end method

.method setResource(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 4274
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4275
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->resource_:Ljava/lang/Object;

    .line 4277
    return-void
.end method

.method public setUsername(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4295
    if-nez p1, :cond_0

    .line 4296
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4298
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4299
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->username_:Ljava/lang/Object;

    .line 4301
    return-object p0
.end method

.method setUsername(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 4310
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4311
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->username_:Ljava/lang/Object;

    .line 4313
    return-void
.end method

.method public setValidationcode(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4403
    if-nez p1, :cond_0

    .line 4404
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4406
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4407
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->validationcode_:Ljava/lang/Object;

    .line 4409
    return-object p0
.end method

.method setValidationcode(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 4418
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->bitField0_:I

    .line 4419
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->validationcode_:Ljava/lang/Object;

    .line 4421
    return-void
.end method
