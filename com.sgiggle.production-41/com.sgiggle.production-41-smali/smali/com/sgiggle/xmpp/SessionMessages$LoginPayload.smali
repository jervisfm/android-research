.class public final Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$LoginPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LoginPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CANBINDUDP_FIELD_NUMBER:I = 0xb

.field public static final DISPLAY_FIELD_NUMBER:I = 0xc

.field public static final DOMAIN_FIELD_NUMBER:I = 0x7

.field public static final FROMUI_FIELD_NUMBER:I = 0x9

.field public static final HOSTNAME_FIELD_NUMBER:I = 0x2

.field public static final PASSWORD_FIELD_NUMBER:I = 0x6

.field public static final PORT_FIELD_NUMBER:I = 0x3

.field public static final RECEIVEDPUSH_FIELD_NUMBER:I = 0xa

.field public static final RESOURCE_FIELD_NUMBER:I = 0x4

.field public static final USERNAME_FIELD_NUMBER:I = 0x5

.field public static final VALIDATIONCODE_FIELD_NUMBER:I = 0x8

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private canBindUdp_:Z

.field private display_:I

.field private domain_:Ljava/lang/Object;

.field private fromUI_:Z

.field private hostname_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private password_:Ljava/lang/Object;

.field private port_:I

.field private receivedPush_:Z

.field private resource_:Ljava/lang/Object;

.field private username_:Ljava/lang/Object;

.field private validationcode_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 4511
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    .line 4512
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->initFields()V

    .line 4513
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 3402
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 3682
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->memoizedIsInitialized:B

    .line 3740
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->memoizedSerializedSize:I

    .line 3403
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 3397
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 3404
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 3682
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->memoizedIsInitialized:B

    .line 3740
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->memoizedSerializedSize:I

    .line 3404
    return-void
.end method

.method static synthetic access$3402(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 3397
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$3502(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 3397
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->hostname_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$3602(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 3397
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->port_:I

    return p1
.end method

.method static synthetic access$3702(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 3397
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->resource_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$3802(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 3397
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->username_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$3902(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 3397
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->password_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$4002(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 3397
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->domain_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$4102(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 3397
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->validationcode_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$4202(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 3397
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->fromUI_:Z

    return p1
.end method

.method static synthetic access$4302(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 3397
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->receivedPush_:Z

    return p1
.end method

.method static synthetic access$4402(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 3397
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->canBindUdp_:Z

    return p1
.end method

.method static synthetic access$4502(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 3397
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->display_:I

    return p1
.end method

.method static synthetic access$4602(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 3397
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;
    .locals 1

    .prologue
    .line 3408
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    return-object v0
.end method

.method private getDomainBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 3585
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->domain_:Ljava/lang/Object;

    .line 3586
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3587
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 3589
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->domain_:Ljava/lang/Object;

    .line 3592
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getHostnameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 3447
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->hostname_:Ljava/lang/Object;

    .line 3448
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3449
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 3451
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->hostname_:Ljava/lang/Object;

    .line 3454
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getPasswordBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 3553
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->password_:Ljava/lang/Object;

    .line 3554
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3555
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 3557
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->password_:Ljava/lang/Object;

    .line 3560
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getResourceBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 3489
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->resource_:Ljava/lang/Object;

    .line 3490
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3491
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 3493
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->resource_:Ljava/lang/Object;

    .line 3496
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getUsernameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 3521
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->username_:Ljava/lang/Object;

    .line 3522
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3523
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 3525
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->username_:Ljava/lang/Object;

    .line 3528
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getValidationcodeBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 3617
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->validationcode_:Ljava/lang/Object;

    .line 3618
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3619
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 3621
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->validationcode_:Ljava/lang/Object;

    .line 3624
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3669
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 3670
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->hostname_:Ljava/lang/Object;

    .line 3671
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->port_:I

    .line 3672
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->resource_:Ljava/lang/Object;

    .line 3673
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->username_:Ljava/lang/Object;

    .line 3674
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->password_:Ljava/lang/Object;

    .line 3675
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->domain_:Ljava/lang/Object;

    .line 3676
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->validationcode_:Ljava/lang/Object;

    .line 3677
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->fromUI_:Z

    .line 3678
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->receivedPush_:Z

    .line 3679
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->canBindUdp_:Z

    .line 3680
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->display_:I

    .line 3681
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1

    .prologue
    .line 3870
    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->access$3200()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 3873
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3839
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    .line 3840
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3841
    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->access$3100(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    move-result-object v0

    .line 3843
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3850
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    .line 3851
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3852
    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->access$3100(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    move-result-object v0

    .line 3854
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 3806
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->access$3100(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 3812
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->access$3100(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3860
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->access$3100(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3866
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->access$3100(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3828
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->access$3100(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3834
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->access$3100(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 3817
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->access$3100(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 3823
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;->access$3100(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 3423
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCanBindUdp()Z
    .locals 1

    .prologue
    .line 3655
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->canBindUdp_:Z

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 3397
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;
    .locals 1

    .prologue
    .line 3412
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;

    return-object v0
.end method

.method public getDisplay()I
    .locals 1

    .prologue
    .line 3665
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->display_:I

    return v0
.end method

.method public getDomain()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3571
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->domain_:Ljava/lang/Object;

    .line 3572
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3573
    check-cast v0, Ljava/lang/String;

    .line 3581
    :goto_0
    return-object v0

    .line 3575
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 3577
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 3578
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3579
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->domain_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 3581
    goto :goto_0
.end method

.method public getFromUI()Z
    .locals 1

    .prologue
    .line 3635
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->fromUI_:Z

    return v0
.end method

.method public getHostname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3433
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->hostname_:Ljava/lang/Object;

    .line 3434
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3435
    check-cast v0, Ljava/lang/String;

    .line 3443
    :goto_0
    return-object v0

    .line 3437
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 3439
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 3440
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3441
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->hostname_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 3443
    goto :goto_0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3539
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->password_:Ljava/lang/Object;

    .line 3540
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3541
    check-cast v0, Ljava/lang/String;

    .line 3549
    :goto_0
    return-object v0

    .line 3543
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 3545
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 3546
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3547
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->password_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 3549
    goto :goto_0
.end method

.method public getPort()I
    .locals 1

    .prologue
    .line 3465
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->port_:I

    return v0
.end method

.method public getReceivedPush()Z
    .locals 1

    .prologue
    .line 3645
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->receivedPush_:Z

    return v0
.end method

.method public getResource()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3475
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->resource_:Ljava/lang/Object;

    .line 3476
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3477
    check-cast v0, Ljava/lang/String;

    .line 3485
    :goto_0
    return-object v0

    .line 3479
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 3481
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 3482
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3483
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->resource_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 3485
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 3742
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->memoizedSerializedSize:I

    .line 3743
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 3795
    :goto_0
    return v0

    .line 3745
    :cond_0
    const/4 v0, 0x0

    .line 3746
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 3747
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3750
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 3751
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getHostnameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3754
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 3755
    const/4 v1, 0x3

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->port_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3758
    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_4

    .line 3759
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getResourceBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3762
    :cond_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 3763
    const/4 v1, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getUsernameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3766
    :cond_5
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    .line 3767
    const/4 v1, 0x6

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getPasswordBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3770
    :cond_6
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    .line 3771
    const/4 v1, 0x7

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getDomainBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3774
    :cond_7
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_8

    .line 3775
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getValidationcodeBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3778
    :cond_8
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_9

    .line 3779
    const/16 v1, 0x9

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->fromUI_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3782
    :cond_9
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_a

    .line 3783
    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->receivedPush_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3786
    :cond_a
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x400

    const/16 v2, 0x400

    if-ne v1, v2, :cond_b

    .line 3787
    const/16 v1, 0xb

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->canBindUdp_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3790
    :cond_b
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x800

    const/16 v2, 0x800

    if-ne v1, v2, :cond_c

    .line 3791
    const/16 v1, 0xc

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->display_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3794
    :cond_c
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->memoizedSerializedSize:I

    goto/16 :goto_0
.end method

.method public getUsername()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3507
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->username_:Ljava/lang/Object;

    .line 3508
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3509
    check-cast v0, Ljava/lang/String;

    .line 3517
    :goto_0
    return-object v0

    .line 3511
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 3513
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 3514
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3515
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->username_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 3517
    goto :goto_0
.end method

.method public getValidationcode()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3603
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->validationcode_:Ljava/lang/Object;

    .line 3604
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3605
    check-cast v0, Ljava/lang/String;

    .line 3613
    :goto_0
    return-object v0

    .line 3607
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 3609
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 3610
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3611
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->validationcode_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 3613
    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 3420
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCanBindUdp()Z
    .locals 2

    .prologue
    .line 3652
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDisplay()Z
    .locals 2

    .prologue
    .line 3662
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDomain()Z
    .locals 2

    .prologue
    .line 3568
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFromUI()Z
    .locals 2

    .prologue
    .line 3632
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHostname()Z
    .locals 2

    .prologue
    .line 3430
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPassword()Z
    .locals 2

    .prologue
    .line 3536
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPort()Z
    .locals 2

    .prologue
    .line 3462
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasReceivedPush()Z
    .locals 2

    .prologue
    .line 3642
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasResource()Z
    .locals 2

    .prologue
    .line 3472
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUsername()Z
    .locals 2

    .prologue
    .line 3504
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasValidationcode()Z
    .locals 2

    .prologue
    .line 3600
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3684
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->memoizedIsInitialized:B

    .line 3685
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 3696
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 3685
    goto :goto_0

    .line 3687
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 3688
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 3689
    goto :goto_0

    .line 3691
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 3692
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 3693
    goto :goto_0

    .line 3695
    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 3696
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 3397
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1

    .prologue
    .line 3871
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 3397
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;
    .locals 1

    .prologue
    .line 3875
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;)Lcom/sgiggle/xmpp/SessionMessages$LoginPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 3800
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 3701
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getSerializedSize()I

    .line 3702
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 3703
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 3705
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 3706
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getHostnameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3708
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 3709
    const/4 v0, 0x3

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->port_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 3711
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 3712
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getResourceBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3714
    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 3715
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getUsernameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3717
    :cond_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 3718
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getPasswordBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3720
    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 3721
    const/4 v0, 0x7

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getDomainBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3723
    :cond_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 3724
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->getValidationcodeBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 3726
    :cond_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    .line 3727
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->fromUI_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 3729
    :cond_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_9

    .line 3730
    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->receivedPush_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 3732
    :cond_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_a

    .line 3733
    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->canBindUdp_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 3735
    :cond_a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_b

    .line 3736
    const/16 v0, 0xc

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LoginPayload;->display_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 3738
    :cond_b
    return-void
.end method
