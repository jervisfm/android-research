.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$LoginPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "LoginPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getCanBindUdp()Z
.end method

.method public abstract getDisplay()I
.end method

.method public abstract getDomain()Ljava/lang/String;
.end method

.method public abstract getFromUI()Z
.end method

.method public abstract getHostname()Ljava/lang/String;
.end method

.method public abstract getPassword()Ljava/lang/String;
.end method

.method public abstract getPort()I
.end method

.method public abstract getReceivedPush()Z
.end method

.method public abstract getResource()Ljava/lang/String;
.end method

.method public abstract getUsername()Ljava/lang/String;
.end method

.method public abstract getValidationcode()Ljava/lang/String;
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasCanBindUdp()Z
.end method

.method public abstract hasDisplay()Z
.end method

.method public abstract hasDomain()Z
.end method

.method public abstract hasFromUI()Z
.end method

.method public abstract hasHostname()Z
.end method

.method public abstract hasPassword()Z
.end method

.method public abstract hasPort()Z
.end method

.method public abstract hasReceivedPush()Z
.end method

.method public abstract hasResource()Z
.end method

.method public abstract hasUsername()Z
.end method

.method public abstract hasValidationcode()Z
.end method
