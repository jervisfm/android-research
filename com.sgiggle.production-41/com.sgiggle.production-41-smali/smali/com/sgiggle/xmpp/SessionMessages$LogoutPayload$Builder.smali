.class public final Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$LogoutPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$LogoutPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private fromUI_:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 4702
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 4824
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 4703
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->maybeForceBuilderInitialization()V

    .line 4704
    return-void
.end method

.method static synthetic access$4700(Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 4697
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4800()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;
    .locals 1

    .prologue
    .line 4697
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 4739
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;

    move-result-object v0

    .line 4740
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 4741
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 4744
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;
    .locals 1

    .prologue
    .line 4709
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 4707
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 4697
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;
    .locals 2

    .prologue
    .line 4730
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;

    move-result-object v0

    .line 4731
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 4732
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 4734
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 4697
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;
    .locals 5

    .prologue
    .line 4748
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 4749
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->bitField0_:I

    .line 4750
    const/4 v2, 0x0

    .line 4751
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 4752
    or-int/lit8 v2, v2, 0x1

    .line 4754
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->access$5002(Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 4755
    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    .line 4756
    or-int/lit8 v1, v2, 0x2

    .line 4758
    :goto_0
    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->fromUI_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->fromUI_:Z
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->access$5102(Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;Z)Z

    .line 4759
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->access$5202(Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;I)I

    .line 4760
    return-object v0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 4697
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 4697
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;
    .locals 1

    .prologue
    .line 4713
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 4714
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 4715
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->bitField0_:I

    .line 4716
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->fromUI_:Z

    .line 4717
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->bitField0_:I

    .line 4718
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;
    .locals 1

    .prologue
    .line 4860
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 4862
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->bitField0_:I

    .line 4863
    return-object p0
.end method

.method public clearFromUI()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;
    .locals 1

    .prologue
    .line 4881
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->bitField0_:I

    .line 4882
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->fromUI_:Z

    .line 4884
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 4697
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 4697
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 4697
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;
    .locals 2

    .prologue
    .line 4722
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 4697
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 4829
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 4697
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 4697
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;
    .locals 1

    .prologue
    .line 4726
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public getFromUI()Z
    .locals 1

    .prologue
    .line 4872
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->fromUI_:Z

    return v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 4826
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFromUI()Z
    .locals 2

    .prologue
    .line 4869
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4775
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 4783
    :goto_0
    return v0

    .line 4779
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 4781
    goto :goto_0

    .line 4783
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 4848
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 4850
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 4856
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->bitField0_:I

    .line 4857
    return-object p0

    .line 4853
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4697
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4697
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4697
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4791
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 4792
    sparse-switch v0, :sswitch_data_0

    .line 4797
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 4799
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 4795
    goto :goto_1

    .line 4804
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 4805
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4806
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 4808
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 4809
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    goto :goto_0

    .line 4813
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->bitField0_:I

    .line 4814
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->fromUI_:Z

    goto :goto_0

    .line 4792
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4764
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 4771
    :goto_0
    return-object v0

    .line 4765
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4766
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    .line 4768
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->hasFromUI()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4769
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->getFromUI()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->setFromUI(Z)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    :cond_2
    move-object v0, p0

    .line 4771
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4842
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 4844
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->bitField0_:I

    .line 4845
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4832
    if-nez p1, :cond_0

    .line 4833
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4835
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 4837
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->bitField0_:I

    .line 4838
    return-object p0
.end method

.method public setFromUI(Z)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4875
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->bitField0_:I

    .line 4876
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->fromUI_:Z

    .line 4878
    return-object p0
.end method
