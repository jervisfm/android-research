.class public final Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$LogoutPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LogoutPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final FROMUI_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private fromUI_:Z

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 4891
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;

    .line 4892
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->initFields()V

    .line 4893
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 4534
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 4572
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->memoizedIsInitialized:B

    .line 4600
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->memoizedSerializedSize:I

    .line 4535
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 4529
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 4536
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 4572
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->memoizedIsInitialized:B

    .line 4600
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->memoizedSerializedSize:I

    .line 4536
    return-void
.end method

.method static synthetic access$5002(Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 4529
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$5102(Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 4529
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->fromUI_:Z

    return p1
.end method

.method static synthetic access$5202(Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 4529
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;
    .locals 1

    .prologue
    .line 4540
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 4569
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 4570
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->fromUI_:Z

    .line 4571
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;
    .locals 1

    .prologue
    .line 4690
    #calls: Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->access$4800()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 4693
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4659
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    move-result-object v0

    .line 4660
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4661
    #calls: Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->access$4700(Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;

    move-result-object v0

    .line 4663
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4670
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    move-result-object v0

    .line 4671
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4672
    #calls: Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->access$4700(Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;

    move-result-object v0

    .line 4674
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 4626
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->access$4700(Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 4632
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->access$4700(Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4680
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->access$4700(Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4686
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->access$4700(Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4648
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->access$4700(Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4654
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->access$4700(Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 4637
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->access$4700(Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 4643
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;->access$4700(Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 4555
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 4529
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;
    .locals 1

    .prologue
    .line 4544
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;

    return-object v0
.end method

.method public getFromUI()Z
    .locals 1

    .prologue
    .line 4565
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->fromUI_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 4602
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->memoizedSerializedSize:I

    .line 4603
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 4615
    :goto_0
    return v0

    .line 4605
    :cond_0
    const/4 v0, 0x0

    .line 4606
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 4607
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4610
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 4611
    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->fromUI_:Z

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4614
    :cond_2
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 4552
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFromUI()Z
    .locals 2

    .prologue
    .line 4562
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4574
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->memoizedIsInitialized:B

    .line 4575
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 4586
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 4575
    goto :goto_0

    .line 4577
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 4578
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 4579
    goto :goto_0

    .line 4581
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 4582
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 4583
    goto :goto_0

    .line 4585
    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 4586
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 4529
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;
    .locals 1

    .prologue
    .line 4691
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 4529
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;
    .locals 1

    .prologue
    .line 4695
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;)Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 4620
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 4591
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->getSerializedSize()I

    .line 4592
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 4593
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 4595
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 4596
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$LogoutPayload;->fromUI_:Z

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 4598
    :cond_1
    return-void
.end method
