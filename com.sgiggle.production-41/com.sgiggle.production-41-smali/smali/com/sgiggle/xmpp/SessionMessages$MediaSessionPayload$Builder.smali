.class public final Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private accountId_:Ljava/lang/Object;

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private callid_:Ljava/lang/Object;

.field private cameraPosition_:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

.field private deviceContactId_:J

.field private direction_:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

.field private displaymessage_:Ljava/lang/Object;

.field private displayname_:Ljava/lang/Object;

.field private emptySlotCount_:I

.field private fromUi_:Z

.field private localDisplayname_:Ljava/lang/Object;

.field private muted_:Z

.field private networkmessage_:Ljava/lang/Object;

.field private present_:Z

.field private sessionId_:Ljava/lang/Object;

.field private showWand_:Z

.field private speakerOn_:Z

.field private timestamp_:I

.field private type_:Ljava/lang/Object;

.field private unawsered_:Z

.field private vgoodBundle_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;",
            ">;"
        }
    .end annotation
.end field

.field private vgoodSupport_:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

.field private vgoodsPurchased_:Z

.field private videoMode_:Z

.field private videoRingbackPrologue_:Ljava/lang/Object;

.field private videoRingback_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 9961
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 10444
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 10487
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->accountId_:Ljava/lang/Object;

    .line 10523
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->type_:Ljava/lang/Object;

    .line 10559
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->sessionId_:Ljava/lang/Object;

    .line 10616
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->direction_:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    .line 10661
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->displayname_:Ljava/lang/Object;

    .line 10718
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->displaymessage_:Ljava/lang/Object;

    .line 10754
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->networkmessage_:Ljava/lang/Object;

    .line 10832
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->callid_:Ljava/lang/Object;

    .line 10868
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->CAM_NONE:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->cameraPosition_:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    .line 10955
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->VGOOD_YES:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodSupport_:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    .line 11000
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->videoRingback_:Ljava/lang/Object;

    .line 11036
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->videoRingbackPrologue_:Ljava/lang/Object;

    .line 11072
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodBundle_:Ljava/util/List;

    .line 11203
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->localDisplayname_:Ljava/lang/Object;

    .line 9962
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->maybeForceBuilderInitialization()V

    .line 9963
    return-void
.end method

.method static synthetic access$10800(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 9956
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$10900()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1

    .prologue
    .line 9956
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 10046
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    .line 10047
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 10048
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 10051
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1

    .prologue
    .line 9968
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureVgoodBundleIsMutable()V
    .locals 3

    .prologue
    const/high16 v2, 0x40

    .line 11075
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/2addr v0, v2

    if-eq v0, v2, :cond_0

    .line 11076
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodBundle_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodBundle_:Ljava/util/List;

    .line 11077
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/2addr v0, v2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 11079
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 9966
    return-void
.end method


# virtual methods
.method public addAllVgoodBundle(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;"
        }
    .end annotation

    .prologue
    .line 11142
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->ensureVgoodBundleIsMutable()V

    .line 11143
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodBundle_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 11145
    return-object p0
.end method

.method public addVgoodBundle(ILcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 11135
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->ensureVgoodBundleIsMutable()V

    .line 11136
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodBundle_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 11138
    return-object p0
.end method

.method public addVgoodBundle(ILcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 11118
    if-nez p2, :cond_0

    .line 11119
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 11121
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->ensureVgoodBundleIsMutable()V

    .line 11122
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 11124
    return-object p0
.end method

.method public addVgoodBundle(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 11128
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->ensureVgoodBundleIsMutable()V

    .line 11129
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodBundle_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 11131
    return-object p0
.end method

.method public addVgoodBundle(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 11108
    if-nez p1, :cond_0

    .line 11109
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 11111
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->ensureVgoodBundleIsMutable()V

    .line 11112
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 11114
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 9956
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;
    .locals 2

    .prologue
    .line 10037
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    .line 10038
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 10039
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 10041
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 9956
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;
    .locals 10

    .prologue
    const/high16 v9, 0x8

    const/high16 v8, 0x4

    const/high16 v7, 0x2

    const/high16 v6, 0x1

    const v5, 0x8000

    .line 10055
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 10056
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10057
    const/4 v2, 0x0

    .line 10058
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 10059
    or-int/lit8 v2, v2, 0x1

    .line 10061
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->access$11102(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 10062
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 10063
    or-int/lit8 v2, v2, 0x2

    .line 10065
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->accountId_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->accountId_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->access$11202(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 10066
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 10067
    or-int/lit8 v2, v2, 0x4

    .line 10069
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->type_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->type_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->access$11302(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 10070
    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 10071
    or-int/lit8 v2, v2, 0x8

    .line 10073
    :cond_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->sessionId_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->sessionId_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->access$11402(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 10074
    and-int/lit8 v3, v1, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 10075
    or-int/lit8 v2, v2, 0x10

    .line 10077
    :cond_4
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->present_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->present_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->access$11502(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Z)Z

    .line 10078
    and-int/lit8 v3, v1, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 10079
    or-int/lit8 v2, v2, 0x20

    .line 10081
    :cond_5
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->direction_:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->direction_:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->access$11602(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    .line 10082
    and-int/lit8 v3, v1, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    .line 10083
    or-int/lit8 v2, v2, 0x40

    .line 10085
    :cond_6
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->speakerOn_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->speakerOn_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->access$11702(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Z)Z

    .line 10086
    and-int/lit16 v3, v1, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    .line 10087
    or-int/lit16 v2, v2, 0x80

    .line 10089
    :cond_7
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->displayname_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->displayname_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->access$11802(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 10090
    and-int/lit16 v3, v1, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_8

    .line 10091
    or-int/lit16 v2, v2, 0x100

    .line 10093
    :cond_8
    iget-wide v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->deviceContactId_:J

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->deviceContactId_:J
    invoke-static {v0, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->access$11902(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;J)J

    .line 10094
    and-int/lit16 v3, v1, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_9

    .line 10095
    or-int/lit16 v2, v2, 0x200

    .line 10097
    :cond_9
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->displaymessage_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->displaymessage_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->access$12002(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 10098
    and-int/lit16 v3, v1, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_a

    .line 10099
    or-int/lit16 v2, v2, 0x400

    .line 10101
    :cond_a
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->networkmessage_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->networkmessage_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->access$12102(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 10102
    and-int/lit16 v3, v1, 0x800

    const/16 v4, 0x800

    if-ne v3, v4, :cond_b

    .line 10103
    or-int/lit16 v2, v2, 0x800

    .line 10105
    :cond_b
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->muted_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->muted_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->access$12202(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Z)Z

    .line 10106
    and-int/lit16 v3, v1, 0x1000

    const/16 v4, 0x1000

    if-ne v3, v4, :cond_c

    .line 10107
    or-int/lit16 v2, v2, 0x1000

    .line 10109
    :cond_c
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->timestamp_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->timestamp_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->access$12302(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;I)I

    .line 10110
    and-int/lit16 v3, v1, 0x2000

    const/16 v4, 0x2000

    if-ne v3, v4, :cond_d

    .line 10111
    or-int/lit16 v2, v2, 0x2000

    .line 10113
    :cond_d
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->callid_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->callid_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->access$12402(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 10114
    and-int/lit16 v3, v1, 0x4000

    const/16 v4, 0x4000

    if-ne v3, v4, :cond_e

    .line 10115
    or-int/lit16 v2, v2, 0x4000

    .line 10117
    :cond_e
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->cameraPosition_:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->cameraPosition_:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->access$12502(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;)Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    .line 10118
    and-int v3, v1, v5

    if-ne v3, v5, :cond_f

    .line 10119
    or-int/2addr v2, v5

    .line 10121
    :cond_f
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->fromUi_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->fromUi_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->access$12602(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Z)Z

    .line 10122
    and-int v3, v1, v6

    if-ne v3, v6, :cond_10

    .line 10123
    or-int/2addr v2, v6

    .line 10125
    :cond_10
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->unawsered_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->unawsered_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->access$12702(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Z)Z

    .line 10126
    and-int v3, v1, v7

    if-ne v3, v7, :cond_11

    .line 10127
    or-int/2addr v2, v7

    .line 10129
    :cond_11
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodsPurchased_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->vgoodsPurchased_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->access$12802(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Z)Z

    .line 10130
    and-int v3, v1, v8

    if-ne v3, v8, :cond_12

    .line 10131
    or-int/2addr v2, v8

    .line 10133
    :cond_12
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodSupport_:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->vgoodSupport_:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->access$12902(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    .line 10134
    and-int v3, v1, v9

    if-ne v3, v9, :cond_13

    .line 10135
    or-int/2addr v2, v9

    .line 10137
    :cond_13
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->videoMode_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->videoMode_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->access$13002(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Z)Z

    .line 10138
    const/high16 v3, 0x10

    and-int/2addr v3, v1

    const/high16 v4, 0x10

    if-ne v3, v4, :cond_14

    .line 10139
    const/high16 v3, 0x10

    or-int/2addr v2, v3

    .line 10141
    :cond_14
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->videoRingback_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->videoRingback_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->access$13102(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 10142
    const/high16 v3, 0x20

    and-int/2addr v3, v1

    const/high16 v4, 0x20

    if-ne v3, v4, :cond_15

    .line 10143
    const/high16 v3, 0x20

    or-int/2addr v2, v3

    .line 10145
    :cond_15
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->videoRingbackPrologue_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->videoRingbackPrologue_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->access$13202(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 10146
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const/high16 v4, 0x40

    and-int/2addr v3, v4

    const/high16 v4, 0x40

    if-ne v3, v4, :cond_16

    .line 10147
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodBundle_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodBundle_:Ljava/util/List;

    .line 10148
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const v4, -0x400001

    and-int/2addr v3, v4

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10150
    :cond_16
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodBundle_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->vgoodBundle_:Ljava/util/List;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->access$13302(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Ljava/util/List;)Ljava/util/List;

    .line 10151
    const/high16 v3, 0x80

    and-int/2addr v3, v1

    const/high16 v4, 0x80

    if-ne v3, v4, :cond_17

    .line 10152
    const/high16 v3, 0x40

    or-int/2addr v2, v3

    .line 10154
    :cond_17
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->emptySlotCount_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->emptySlotCount_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->access$13402(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;I)I

    .line 10155
    const/high16 v3, 0x100

    and-int/2addr v3, v1

    const/high16 v4, 0x100

    if-ne v3, v4, :cond_18

    .line 10156
    const/high16 v3, 0x80

    or-int/2addr v2, v3

    .line 10158
    :cond_18
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->showWand_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->showWand_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->access$13502(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Z)Z

    .line 10159
    const/high16 v3, 0x200

    and-int/2addr v1, v3

    const/high16 v3, 0x200

    if-ne v1, v3, :cond_19

    .line 10160
    const/high16 v1, 0x100

    or-int/2addr v1, v2

    .line 10162
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->localDisplayname_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->localDisplayname_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->access$13602(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 10163
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->access$13702(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;I)I

    .line 10164
    return-object v0

    :cond_19
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 9956
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 9956
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 9972
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 9973
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 9974
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 9975
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->accountId_:Ljava/lang/Object;

    .line 9976
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 9977
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->type_:Ljava/lang/Object;

    .line 9978
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 9979
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->sessionId_:Ljava/lang/Object;

    .line 9980
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 9981
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->present_:Z

    .line 9982
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 9983
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->direction_:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    .line 9984
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 9985
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->speakerOn_:Z

    .line 9986
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 9987
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->displayname_:Ljava/lang/Object;

    .line 9988
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 9989
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->deviceContactId_:J

    .line 9990
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 9991
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->displaymessage_:Ljava/lang/Object;

    .line 9992
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 9993
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->networkmessage_:Ljava/lang/Object;

    .line 9994
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 9995
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->muted_:Z

    .line 9996
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 9997
    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->timestamp_:I

    .line 9998
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 9999
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->callid_:Ljava/lang/Object;

    .line 10000
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x2001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10001
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->CAM_NONE:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->cameraPosition_:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    .line 10002
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x4001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10003
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->fromUi_:Z

    .line 10004
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const v1, -0x8001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10005
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->unawsered_:Z

    .line 10006
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const v1, -0x10001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10007
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodsPurchased_:Z

    .line 10008
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const v1, -0x20001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10009
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->VGOOD_YES:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodSupport_:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    .line 10010
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const v1, -0x40001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10011
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->videoMode_:Z

    .line 10012
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const v1, -0x80001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10013
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->videoRingback_:Ljava/lang/Object;

    .line 10014
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const v1, -0x100001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10015
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->videoRingbackPrologue_:Ljava/lang/Object;

    .line 10016
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const v1, -0x200001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10017
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodBundle_:Ljava/util/List;

    .line 10018
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const v1, -0x400001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10019
    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->emptySlotCount_:I

    .line 10020
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const v1, -0x800001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10021
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->showWand_:Z

    .line 10022
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const v1, -0x1000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10023
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->localDisplayname_:Ljava/lang/Object;

    .line 10024
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const v1, -0x2000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10025
    return-object p0
.end method

.method public clearAccountId()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1

    .prologue
    .line 10511
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10512
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getAccountId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->accountId_:Ljava/lang/Object;

    .line 10514
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1

    .prologue
    .line 10480
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 10482
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10483
    return-object p0
.end method

.method public clearCallid()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1

    .prologue
    .line 10856
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x2001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10857
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getCallid()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->callid_:Ljava/lang/Object;

    .line 10859
    return-object p0
.end method

.method public clearCameraPosition()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1

    .prologue
    .line 10885
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x4001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10886
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->CAM_NONE:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->cameraPosition_:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    .line 10888
    return-object p0
.end method

.method public clearDeviceContactId()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 2

    .prologue
    .line 10711
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10712
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->deviceContactId_:J

    .line 10714
    return-object p0
.end method

.method public clearDirection()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1

    .prologue
    .line 10633
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10634
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->direction_:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    .line 10636
    return-object p0
.end method

.method public clearDisplaymessage()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1

    .prologue
    .line 10742
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10743
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDisplaymessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->displaymessage_:Ljava/lang/Object;

    .line 10745
    return-object p0
.end method

.method public clearDisplayname()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1

    .prologue
    .line 10685
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10686
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDisplayname()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->displayname_:Ljava/lang/Object;

    .line 10688
    return-object p0
.end method

.method public clearEmptySlotCount()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 2

    .prologue
    .line 11175
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const v1, -0x800001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 11176
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->emptySlotCount_:I

    .line 11178
    return-object p0
.end method

.method public clearFromUi()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 2

    .prologue
    .line 10906
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const v1, -0x8001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10907
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->fromUi_:Z

    .line 10909
    return-object p0
.end method

.method public clearLocalDisplayname()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 2

    .prologue
    .line 11227
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const v1, -0x2000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 11228
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getLocalDisplayname()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->localDisplayname_:Ljava/lang/Object;

    .line 11230
    return-object p0
.end method

.method public clearMuted()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1

    .prologue
    .line 10804
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10805
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->muted_:Z

    .line 10807
    return-object p0
.end method

.method public clearNetworkmessage()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1

    .prologue
    .line 10778
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10779
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getNetworkmessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->networkmessage_:Ljava/lang/Object;

    .line 10781
    return-object p0
.end method

.method public clearPresent()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1

    .prologue
    .line 10609
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10610
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->present_:Z

    .line 10612
    return-object p0
.end method

.method public clearSessionId()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1

    .prologue
    .line 10583
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10584
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getSessionId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->sessionId_:Ljava/lang/Object;

    .line 10586
    return-object p0
.end method

.method public clearShowWand()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 2

    .prologue
    .line 11196
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const v1, -0x1000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 11197
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->showWand_:Z

    .line 11199
    return-object p0
.end method

.method public clearSpeakerOn()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1

    .prologue
    .line 10654
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10655
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->speakerOn_:Z

    .line 10657
    return-object p0
.end method

.method public clearTimestamp()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1

    .prologue
    .line 10825
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10826
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->timestamp_:I

    .line 10828
    return-object p0
.end method

.method public clearType()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1

    .prologue
    .line 10547
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10548
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getType()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->type_:Ljava/lang/Object;

    .line 10550
    return-object p0
.end method

.method public clearUnawsered()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 2

    .prologue
    .line 10927
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const v1, -0x10001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10928
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->unawsered_:Z

    .line 10930
    return-object p0
.end method

.method public clearVgoodBundle()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 2

    .prologue
    .line 11148
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodBundle_:Ljava/util/List;

    .line 11149
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const v1, -0x400001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 11151
    return-object p0
.end method

.method public clearVgoodSupport()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 2

    .prologue
    .line 10972
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const v1, -0x40001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10973
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->VGOOD_YES:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodSupport_:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    .line 10975
    return-object p0
.end method

.method public clearVgoodsPurchased()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 2

    .prologue
    .line 10948
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const v1, -0x20001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10949
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodsPurchased_:Z

    .line 10951
    return-object p0
.end method

.method public clearVideoMode()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 2

    .prologue
    .line 10993
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const v1, -0x80001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10994
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->videoMode_:Z

    .line 10996
    return-object p0
.end method

.method public clearVideoRingback()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 2

    .prologue
    .line 11024
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const v1, -0x100001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 11025
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getVideoRingback()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->videoRingback_:Ljava/lang/Object;

    .line 11027
    return-object p0
.end method

.method public clearVideoRingbackPrologue()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 2

    .prologue
    .line 11060
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const v1, -0x200001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 11061
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getVideoRingbackPrologue()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->videoRingbackPrologue_:Ljava/lang/Object;

    .line 11063
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 9956
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 9956
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 9956
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 2

    .prologue
    .line 10029
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 9956
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAccountId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 10492
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->accountId_:Ljava/lang/Object;

    .line 10493
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 10494
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 10495
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->accountId_:Ljava/lang/Object;

    .line 10498
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 10449
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCallid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 10837
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->callid_:Ljava/lang/Object;

    .line 10838
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 10839
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 10840
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->callid_:Ljava/lang/Object;

    .line 10843
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getCameraPosition()Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;
    .locals 1

    .prologue
    .line 10873
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->cameraPosition_:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 9956
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 9956
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;
    .locals 1

    .prologue
    .line 10033
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceContactId()J
    .locals 2

    .prologue
    .line 10702
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->deviceContactId_:J

    return-wide v0
.end method

.method public getDirection()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;
    .locals 1

    .prologue
    .line 10621
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->direction_:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    return-object v0
.end method

.method public getDisplaymessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 10723
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->displaymessage_:Ljava/lang/Object;

    .line 10724
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 10725
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 10726
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->displaymessage_:Ljava/lang/Object;

    .line 10729
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getDisplayname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 10666
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->displayname_:Ljava/lang/Object;

    .line 10667
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 10668
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 10669
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->displayname_:Ljava/lang/Object;

    .line 10672
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getEmptySlotCount()I
    .locals 1

    .prologue
    .line 11166
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->emptySlotCount_:I

    return v0
.end method

.method public getFromUi()Z
    .locals 1

    .prologue
    .line 10897
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->fromUi_:Z

    return v0
.end method

.method public getLocalDisplayname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 11208
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->localDisplayname_:Ljava/lang/Object;

    .line 11209
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 11210
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 11211
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->localDisplayname_:Ljava/lang/Object;

    .line 11214
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getMuted()Z
    .locals 1

    .prologue
    .line 10795
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->muted_:Z

    return v0
.end method

.method public getNetworkmessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 10759
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->networkmessage_:Ljava/lang/Object;

    .line 10760
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 10761
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 10762
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->networkmessage_:Ljava/lang/Object;

    .line 10765
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getPresent()Z
    .locals 1

    .prologue
    .line 10600
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->present_:Z

    return v0
.end method

.method public getSessionId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 10564
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->sessionId_:Ljava/lang/Object;

    .line 10565
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 10566
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 10567
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->sessionId_:Ljava/lang/Object;

    .line 10570
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getShowWand()Z
    .locals 1

    .prologue
    .line 11187
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->showWand_:Z

    return v0
.end method

.method public getSpeakerOn()Z
    .locals 1

    .prologue
    .line 10645
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->speakerOn_:Z

    return v0
.end method

.method public getTimestamp()I
    .locals 1

    .prologue
    .line 10816
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->timestamp_:I

    return v0
.end method

.method public getType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 10528
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->type_:Ljava/lang/Object;

    .line 10529
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 10530
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 10531
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->type_:Ljava/lang/Object;

    .line 10534
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getUnawsered()Z
    .locals 1

    .prologue
    .line 10918
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->unawsered_:Z

    return v0
.end method

.method public getVgoodBundle(I)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    .locals 1
    .parameter

    .prologue
    .line 11088
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    return-object v0
.end method

.method public getVgoodBundleCount()I
    .locals 1

    .prologue
    .line 11085
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getVgoodBundleList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 11082
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodBundle_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getVgoodSupport()Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;
    .locals 1

    .prologue
    .line 10960
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodSupport_:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    return-object v0
.end method

.method public getVgoodsPurchased()Z
    .locals 1

    .prologue
    .line 10939
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodsPurchased_:Z

    return v0
.end method

.method public getVideoMode()Z
    .locals 1

    .prologue
    .line 10984
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->videoMode_:Z

    return v0
.end method

.method public getVideoRingback()Ljava/lang/String;
    .locals 2

    .prologue
    .line 11005
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->videoRingback_:Ljava/lang/Object;

    .line 11006
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 11007
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 11008
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->videoRingback_:Ljava/lang/Object;

    .line 11011
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getVideoRingbackPrologue()Ljava/lang/String;
    .locals 2

    .prologue
    .line 11041
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->videoRingbackPrologue_:Ljava/lang/Object;

    .line 11042
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 11043
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 11044
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->videoRingbackPrologue_:Ljava/lang/Object;

    .line 11047
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasAccountId()Z
    .locals 2

    .prologue
    .line 10489
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 10446
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCallid()Z
    .locals 2

    .prologue
    .line 10834
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCameraPosition()Z
    .locals 2

    .prologue
    .line 10870
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDeviceContactId()Z
    .locals 2

    .prologue
    .line 10699
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDirection()Z
    .locals 2

    .prologue
    .line 10618
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDisplaymessage()Z
    .locals 2

    .prologue
    .line 10720
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDisplayname()Z
    .locals 2

    .prologue
    .line 10663
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasEmptySlotCount()Z
    .locals 2

    .prologue
    const/high16 v1, 0x80

    .line 11163
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFromUi()Z
    .locals 2

    .prologue
    const v1, 0x8000

    .line 10894
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLocalDisplayname()Z
    .locals 2

    .prologue
    const/high16 v1, 0x200

    .line 11205
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMuted()Z
    .locals 2

    .prologue
    .line 10792
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNetworkmessage()Z
    .locals 2

    .prologue
    .line 10756
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPresent()Z
    .locals 2

    .prologue
    .line 10597
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSessionId()Z
    .locals 2

    .prologue
    .line 10561
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasShowWand()Z
    .locals 2

    .prologue
    const/high16 v1, 0x100

    .line 11184
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSpeakerOn()Z
    .locals 2

    .prologue
    .line 10642
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTimestamp()Z
    .locals 2

    .prologue
    .line 10813
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    .prologue
    .line 10525
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUnawsered()Z
    .locals 2

    .prologue
    const/high16 v1, 0x1

    .line 10915
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVgoodSupport()Z
    .locals 2

    .prologue
    const/high16 v1, 0x4

    .line 10957
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVgoodsPurchased()Z
    .locals 2

    .prologue
    const/high16 v1, 0x2

    .line 10936
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoMode()Z
    .locals 2

    .prologue
    const/high16 v1, 0x8

    .line 10981
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoRingback()Z
    .locals 2

    .prologue
    const/high16 v1, 0x10

    .line 11002
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoRingbackPrologue()Z
    .locals 2

    .prologue
    const/high16 v1, 0x20

    .line 11038
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10258
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 10270
    :goto_0
    return v0

    .line 10262
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->hasAccountId()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 10264
    goto :goto_0

    .line 10266
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 10268
    goto :goto_0

    .line 10270
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 10468
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 10470
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 10476
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10477
    return-object p0

    .line 10473
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 9956
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 9956
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 9956
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 10278
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 10279
    sparse-switch v0, :sswitch_data_0

    .line 10284
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 10286
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 10282
    goto :goto_1

    .line 10291
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 10292
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 10293
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 10295
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 10296
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    goto :goto_0

    .line 10300
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10301
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->accountId_:Ljava/lang/Object;

    goto :goto_0

    .line 10305
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10306
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->type_:Ljava/lang/Object;

    goto :goto_0

    .line 10310
    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10311
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->sessionId_:Ljava/lang/Object;

    goto :goto_0

    .line 10315
    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10316
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->present_:Z

    goto :goto_0

    .line 10320
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 10321
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    move-result-object v0

    .line 10322
    if-eqz v0, :cond_0

    .line 10323
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10324
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->direction_:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    goto :goto_0

    .line 10329
    :sswitch_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10330
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->speakerOn_:Z

    goto :goto_0

    .line 10334
    :sswitch_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10335
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->displayname_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 10339
    :sswitch_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10340
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->deviceContactId_:J

    goto/16 :goto_0

    .line 10344
    :sswitch_a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10345
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->displaymessage_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 10349
    :sswitch_b
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10350
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->networkmessage_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 10354
    :sswitch_c
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10355
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->muted_:Z

    goto/16 :goto_0

    .line 10359
    :sswitch_d
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10360
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->timestamp_:I

    goto/16 :goto_0

    .line 10364
    :sswitch_e
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10365
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->callid_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 10369
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 10370
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    move-result-object v0

    .line 10371
    if-eqz v0, :cond_0

    .line 10372
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit16 v1, v1, 0x4000

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10373
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->cameraPosition_:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    goto/16 :goto_0

    .line 10378
    :sswitch_10
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const v1, 0x8000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10379
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->fromUi_:Z

    goto/16 :goto_0

    .line 10383
    :sswitch_11
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const/high16 v1, 0x1

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10384
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->unawsered_:Z

    goto/16 :goto_0

    .line 10388
    :sswitch_12
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const/high16 v1, 0x2

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10389
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodsPurchased_:Z

    goto/16 :goto_0

    .line 10393
    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 10394
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    move-result-object v0

    .line 10395
    if-eqz v0, :cond_0

    .line 10396
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const/high16 v2, 0x4

    or-int/2addr v1, v2

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10397
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodSupport_:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    goto/16 :goto_0

    .line 10402
    :sswitch_14
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const/high16 v1, 0x8

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10403
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->videoMode_:Z

    goto/16 :goto_0

    .line 10407
    :sswitch_15
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const/high16 v1, 0x10

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10408
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->videoRingback_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 10412
    :sswitch_16
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const/high16 v1, 0x20

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10413
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->videoRingbackPrologue_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 10417
    :sswitch_17
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    .line 10418
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 10419
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->addVgoodBundle(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    goto/16 :goto_0

    .line 10423
    :sswitch_18
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const/high16 v1, 0x80

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10424
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->emptySlotCount_:I

    goto/16 :goto_0

    .line 10428
    :sswitch_19
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const/high16 v1, 0x100

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10429
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->showWand_:Z

    goto/16 :goto_0

    .line 10433
    :sswitch_1a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const/high16 v1, 0x200

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10434
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->localDisplayname_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 10279
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x72 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x88 -> :sswitch_11
        0x90 -> :sswitch_12
        0x98 -> :sswitch_13
        0xa0 -> :sswitch_14
        0xaa -> :sswitch_15
        0xb2 -> :sswitch_16
        0xba -> :sswitch_17
        0xc0 -> :sswitch_18
        0xc8 -> :sswitch_19
        0xd2 -> :sswitch_1a
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 10168
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 10254
    :goto_0
    return-object v0

    .line 10169
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 10170
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    .line 10172
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasAccountId()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 10173
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getAccountId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->setAccountId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    .line 10175
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasType()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 10176
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->setType(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    .line 10178
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasSessionId()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 10179
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->setSessionId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    .line 10181
    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasPresent()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 10182
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getPresent()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->setPresent(Z)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    .line 10184
    :cond_5
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasDirection()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 10185
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDirection()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->setDirection(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    .line 10187
    :cond_6
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasSpeakerOn()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 10188
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getSpeakerOn()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->setSpeakerOn(Z)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    .line 10190
    :cond_7
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasDisplayname()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 10191
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDisplayname()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->setDisplayname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    .line 10193
    :cond_8
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasDeviceContactId()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 10194
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDeviceContactId()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->setDeviceContactId(J)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    .line 10196
    :cond_9
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasDisplaymessage()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 10197
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDisplaymessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->setDisplaymessage(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    .line 10199
    :cond_a
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasNetworkmessage()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 10200
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getNetworkmessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->setNetworkmessage(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    .line 10202
    :cond_b
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasMuted()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 10203
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getMuted()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->setMuted(Z)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    .line 10205
    :cond_c
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasTimestamp()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 10206
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getTimestamp()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->setTimestamp(I)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    .line 10208
    :cond_d
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasCallid()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 10209
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getCallid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->setCallid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    .line 10211
    :cond_e
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasCameraPosition()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 10212
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getCameraPosition()Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->setCameraPosition(Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    .line 10214
    :cond_f
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasFromUi()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 10215
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getFromUi()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->setFromUi(Z)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    .line 10217
    :cond_10
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasUnawsered()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 10218
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getUnawsered()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->setUnawsered(Z)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    .line 10220
    :cond_11
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasVgoodsPurchased()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 10221
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getVgoodsPurchased()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->setVgoodsPurchased(Z)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    .line 10223
    :cond_12
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasVgoodSupport()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 10224
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getVgoodSupport()Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->setVgoodSupport(Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    .line 10226
    :cond_13
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasVideoMode()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 10227
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getVideoMode()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->setVideoMode(Z)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    .line 10229
    :cond_14
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasVideoRingback()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 10230
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getVideoRingback()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->setVideoRingback(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    .line 10232
    :cond_15
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasVideoRingbackPrologue()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 10233
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getVideoRingbackPrologue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->setVideoRingbackPrologue(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    .line 10235
    :cond_16
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->vgoodBundle_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->access$13300(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_17

    .line 10236
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 10237
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->vgoodBundle_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->access$13300(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodBundle_:Ljava/util/List;

    .line 10238
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const v1, -0x400001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10245
    :cond_17
    :goto_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasEmptySlotCount()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 10246
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getEmptySlotCount()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->setEmptySlotCount(I)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    .line 10248
    :cond_18
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasShowWand()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 10249
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getShowWand()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->setShowWand(Z)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    .line 10251
    :cond_19
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasLocalDisplayname()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 10252
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getLocalDisplayname()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->setLocalDisplayname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    :cond_1a
    move-object v0, p0

    .line 10254
    goto/16 :goto_0

    .line 10240
    :cond_1b
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->ensureVgoodBundleIsMutable()V

    .line 10241
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodBundle_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->vgoodBundle_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->access$13300(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public removeVgoodBundle(I)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 11154
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->ensureVgoodBundleIsMutable()V

    .line 11155
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 11157
    return-object p0
.end method

.method public setAccountId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 10502
    if-nez p1, :cond_0

    .line 10503
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 10505
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10506
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->accountId_:Ljava/lang/Object;

    .line 10508
    return-object p0
.end method

.method setAccountId(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 10517
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10518
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->accountId_:Ljava/lang/Object;

    .line 10520
    return-void
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 10462
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 10464
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10465
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 10452
    if-nez p1, :cond_0

    .line 10453
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 10455
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 10457
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10458
    return-object p0
.end method

.method public setCallid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 10847
    if-nez p1, :cond_0

    .line 10848
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 10850
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10851
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->callid_:Ljava/lang/Object;

    .line 10853
    return-object p0
.end method

.method setCallid(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 10862
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10863
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->callid_:Ljava/lang/Object;

    .line 10865
    return-void
.end method

.method public setCameraPosition(Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 10876
    if-nez p1, :cond_0

    .line 10877
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 10879
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10880
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->cameraPosition_:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    .line 10882
    return-object p0
.end method

.method public setDeviceContactId(J)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 10705
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10706
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->deviceContactId_:J

    .line 10708
    return-object p0
.end method

.method public setDirection(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 10624
    if-nez p1, :cond_0

    .line 10625
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 10627
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10628
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->direction_:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    .line 10630
    return-object p0
.end method

.method public setDisplaymessage(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 10733
    if-nez p1, :cond_0

    .line 10734
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 10736
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10737
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->displaymessage_:Ljava/lang/Object;

    .line 10739
    return-object p0
.end method

.method setDisplaymessage(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 10748
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10749
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->displaymessage_:Ljava/lang/Object;

    .line 10751
    return-void
.end method

.method public setDisplayname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 10676
    if-nez p1, :cond_0

    .line 10677
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 10679
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10680
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->displayname_:Ljava/lang/Object;

    .line 10682
    return-object p0
.end method

.method setDisplayname(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 10691
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10692
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->displayname_:Ljava/lang/Object;

    .line 10694
    return-void
.end method

.method public setEmptySlotCount(I)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 11169
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const/high16 v1, 0x80

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 11170
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->emptySlotCount_:I

    .line 11172
    return-object p0
.end method

.method public setFromUi(Z)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 10900
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const v1, 0x8000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10901
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->fromUi_:Z

    .line 10903
    return-object p0
.end method

.method public setLocalDisplayname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 11218
    if-nez p1, :cond_0

    .line 11219
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 11221
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const/high16 v1, 0x200

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 11222
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->localDisplayname_:Ljava/lang/Object;

    .line 11224
    return-object p0
.end method

.method setLocalDisplayname(Lcom/google/protobuf/ByteString;)V
    .locals 2
    .parameter

    .prologue
    .line 11233
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const/high16 v1, 0x200

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 11234
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->localDisplayname_:Ljava/lang/Object;

    .line 11236
    return-void
.end method

.method public setMuted(Z)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 10798
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10799
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->muted_:Z

    .line 10801
    return-object p0
.end method

.method public setNetworkmessage(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 10769
    if-nez p1, :cond_0

    .line 10770
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 10772
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10773
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->networkmessage_:Ljava/lang/Object;

    .line 10775
    return-object p0
.end method

.method setNetworkmessage(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 10784
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10785
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->networkmessage_:Ljava/lang/Object;

    .line 10787
    return-void
.end method

.method public setPresent(Z)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 10603
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10604
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->present_:Z

    .line 10606
    return-object p0
.end method

.method public setSessionId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 10574
    if-nez p1, :cond_0

    .line 10575
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 10577
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10578
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->sessionId_:Ljava/lang/Object;

    .line 10580
    return-object p0
.end method

.method setSessionId(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 10589
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10590
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->sessionId_:Ljava/lang/Object;

    .line 10592
    return-void
.end method

.method public setShowWand(Z)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 11190
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const/high16 v1, 0x100

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 11191
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->showWand_:Z

    .line 11193
    return-object p0
.end method

.method public setSpeakerOn(Z)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 10648
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10649
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->speakerOn_:Z

    .line 10651
    return-object p0
.end method

.method public setTimestamp(I)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 10819
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10820
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->timestamp_:I

    .line 10822
    return-object p0
.end method

.method public setType(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 10538
    if-nez p1, :cond_0

    .line 10539
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 10541
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10542
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->type_:Ljava/lang/Object;

    .line 10544
    return-object p0
.end method

.method setType(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 10553
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10554
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->type_:Ljava/lang/Object;

    .line 10556
    return-void
.end method

.method public setUnawsered(Z)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 10921
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const/high16 v1, 0x1

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10922
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->unawsered_:Z

    .line 10924
    return-object p0
.end method

.method public setVgoodBundle(ILcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 11102
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->ensureVgoodBundleIsMutable()V

    .line 11103
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodBundle_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 11105
    return-object p0
.end method

.method public setVgoodBundle(ILcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 11092
    if-nez p2, :cond_0

    .line 11093
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 11095
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->ensureVgoodBundleIsMutable()V

    .line 11096
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 11098
    return-object p0
.end method

.method public setVgoodSupport(Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 10963
    if-nez p1, :cond_0

    .line 10964
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 10966
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const/high16 v1, 0x4

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10967
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodSupport_:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    .line 10969
    return-object p0
.end method

.method public setVgoodsPurchased(Z)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 10942
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const/high16 v1, 0x2

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10943
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->vgoodsPurchased_:Z

    .line 10945
    return-object p0
.end method

.method public setVideoMode(Z)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 10987
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const/high16 v1, 0x8

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 10988
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->videoMode_:Z

    .line 10990
    return-object p0
.end method

.method public setVideoRingback(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 11015
    if-nez p1, :cond_0

    .line 11016
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 11018
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const/high16 v1, 0x10

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 11019
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->videoRingback_:Ljava/lang/Object;

    .line 11021
    return-object p0
.end method

.method setVideoRingback(Lcom/google/protobuf/ByteString;)V
    .locals 2
    .parameter

    .prologue
    .line 11030
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const/high16 v1, 0x10

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 11031
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->videoRingback_:Ljava/lang/Object;

    .line 11033
    return-void
.end method

.method public setVideoRingbackPrologue(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 11051
    if-nez p1, :cond_0

    .line 11052
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 11054
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const/high16 v1, 0x20

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 11055
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->videoRingbackPrologue_:Ljava/lang/Object;

    .line 11057
    return-object p0
.end method

.method setVideoRingbackPrologue(Lcom/google/protobuf/ByteString;)V
    .locals 2
    .parameter

    .prologue
    .line 11066
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    const/high16 v1, 0x20

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->bitField0_:I

    .line 11067
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->videoRingbackPrologue_:Ljava/lang/Object;

    .line 11069
    return-void
.end method
