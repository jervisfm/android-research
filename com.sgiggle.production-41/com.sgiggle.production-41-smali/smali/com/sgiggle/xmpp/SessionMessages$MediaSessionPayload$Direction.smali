.class public final enum Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Direction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction; = null

.field public static final enum BOTH:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction; = null

.field public static final BOTH_VALUE:I = 0x3

.field public static final enum NONE:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction; = null

.field public static final NONE_VALUE:I = 0x0

.field public static final enum RECEIVE:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction; = null

.field public static final RECEIVE_VALUE:I = 0x2

.field public static final enum SEND:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction; = null

.field public static final SEND_VALUE:I = 0x1

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 9094
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    .line 9095
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    const-string v1, "SEND"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->SEND:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    .line 9096
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    const-string v1, "RECEIVE"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->RECEIVE:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    .line 9097
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    const-string v1, "BOTH"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    .line 9092
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->SEND:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->RECEIVE:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    .line 9123
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 9132
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 9133
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->value:I

    .line 9134
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 9120
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;
    .locals 1
    .parameter

    .prologue
    .line 9109
    packed-switch p0, :pswitch_data_0

    .line 9114
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 9110
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    goto :goto_0

    .line 9111
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->SEND:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    goto :goto_0

    .line 9112
    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->RECEIVE:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    goto :goto_0

    .line 9113
    :pswitch_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    goto :goto_0

    .line 9109
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;
    .locals 1
    .parameter

    .prologue
    .line 9092
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;
    .locals 1

    .prologue
    .line 9092
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 9106
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->value:I

    return v0
.end method
