.class public final Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MediaSessionPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;,
        Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;
    }
.end annotation


# static fields
.field public static final ACCOUNTID_FIELD_NUMBER:I = 0x2

.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CALLID_FIELD_NUMBER:I = 0xe

.field public static final CAMERAPOSITION_FIELD_NUMBER:I = 0xf

.field public static final DEVICECONTACTID_FIELD_NUMBER:I = 0x9

.field public static final DIRECTION_FIELD_NUMBER:I = 0x6

.field public static final DISPLAYMESSAGE_FIELD_NUMBER:I = 0xa

.field public static final DISPLAYNAME_FIELD_NUMBER:I = 0x8

.field public static final EMPTY_SLOT_COUNT_FIELD_NUMBER:I = 0x18

.field public static final FROM_UI_FIELD_NUMBER:I = 0x10

.field public static final LOCAL_DISPLAYNAME_FIELD_NUMBER:I = 0x1a

.field public static final MUTED_FIELD_NUMBER:I = 0xc

.field public static final NETWORKMESSAGE_FIELD_NUMBER:I = 0xb

.field public static final PRESENT_FIELD_NUMBER:I = 0x5

.field public static final SESSIONID_FIELD_NUMBER:I = 0x4

.field public static final SHOW_WAND_FIELD_NUMBER:I = 0x19

.field public static final SPEAKERON_FIELD_NUMBER:I = 0x7

.field public static final TIMESTAMP_FIELD_NUMBER:I = 0xd

.field public static final TYPE_FIELD_NUMBER:I = 0x3

.field public static final UNAWSERED_FIELD_NUMBER:I = 0x11

.field public static final VGOODSPURCHASED_FIELD_NUMBER:I = 0x12

.field public static final VGOODSUPPORT_FIELD_NUMBER:I = 0x13

.field public static final VGOOD_BUNDLE_FIELD_NUMBER:I = 0x17

.field public static final VIDEO_MODE_FIELD_NUMBER:I = 0x14

.field public static final VIDEO_RINGBACK_FIELD_NUMBER:I = 0x15

.field public static final VIDEO_RINGBACK_PROLOGUE_FIELD_NUMBER:I = 0x16

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;


# instance fields
.field private accountId_:Ljava/lang/Object;

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private callid_:Ljava/lang/Object;

.field private cameraPosition_:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

.field private deviceContactId_:J

.field private direction_:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

.field private displaymessage_:Ljava/lang/Object;

.field private displayname_:Ljava/lang/Object;

.field private emptySlotCount_:I

.field private fromUi_:Z

.field private localDisplayname_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private muted_:Z

.field private networkmessage_:Ljava/lang/Object;

.field private present_:Z

.field private sessionId_:Ljava/lang/Object;

.field private showWand_:Z

.field private speakerOn_:Z

.field private timestamp_:I

.field private type_:Ljava/lang/Object;

.field private unawsered_:Z

.field private vgoodBundle_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;",
            ">;"
        }
    .end annotation
.end field

.field private vgoodSupport_:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

.field private vgoodsPurchased_:Z

.field private videoMode_:Z

.field private videoRingbackPrologue_:Ljava/lang/Object;

.field private videoRingback_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 11242
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    .line 11243
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->initFields()V

    .line 11244
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 9079
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 9659
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->memoizedIsInitialized:B

    .line 9763
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->memoizedSerializedSize:I

    .line 9080
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 9074
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 9081
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 9659
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->memoizedIsInitialized:B

    .line 9763
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->memoizedSerializedSize:I

    .line 9081
    return-void
.end method

.method static synthetic access$11102(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 9074
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$11202(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 9074
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->accountId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$11302(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 9074
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->type_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$11402(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 9074
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->sessionId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$11502(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 9074
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->present_:Z

    return p1
.end method

.method static synthetic access$11602(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 9074
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->direction_:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    return-object p1
.end method

.method static synthetic access$11702(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 9074
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->speakerOn_:Z

    return p1
.end method

.method static synthetic access$11802(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 9074
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->displayname_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$11902(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;J)J
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 9074
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->deviceContactId_:J

    return-wide p1
.end method

.method static synthetic access$12002(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 9074
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->displaymessage_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$12102(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 9074
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->networkmessage_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$12202(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 9074
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->muted_:Z

    return p1
.end method

.method static synthetic access$12302(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 9074
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->timestamp_:I

    return p1
.end method

.method static synthetic access$12402(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 9074
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->callid_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$12502(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;)Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 9074
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->cameraPosition_:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    return-object p1
.end method

.method static synthetic access$12602(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 9074
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->fromUi_:Z

    return p1
.end method

.method static synthetic access$12702(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 9074
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->unawsered_:Z

    return p1
.end method

.method static synthetic access$12802(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 9074
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->vgoodsPurchased_:Z

    return p1
.end method

.method static synthetic access$12902(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 9074
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->vgoodSupport_:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    return-object p1
.end method

.method static synthetic access$13002(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 9074
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->videoMode_:Z

    return p1
.end method

.method static synthetic access$13102(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 9074
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->videoRingback_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$13202(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 9074
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->videoRingbackPrologue_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$13300(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 9074
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->vgoodBundle_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$13302(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 9074
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->vgoodBundle_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$13402(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 9074
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->emptySlotCount_:I

    return p1
.end method

.method static synthetic access$13502(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 9074
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->showWand_:Z

    return p1
.end method

.method static synthetic access$13602(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 9074
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->localDisplayname_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$13702(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 9074
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    return p1
.end method

.method private getAccountIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 9171
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->accountId_:Ljava/lang/Object;

    .line 9172
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 9173
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 9175
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->accountId_:Ljava/lang/Object;

    .line 9178
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getCallidBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 9423
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->callid_:Ljava/lang/Object;

    .line 9424
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 9425
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 9427
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->callid_:Ljava/lang/Object;

    .line 9430
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;
    .locals 1

    .prologue
    .line 9085
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    return-object v0
.end method

.method private getDisplaymessageBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 9339
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->displaymessage_:Ljava/lang/Object;

    .line 9340
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 9341
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 9343
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->displaymessage_:Ljava/lang/Object;

    .line 9346
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getDisplaynameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 9297
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->displayname_:Ljava/lang/Object;

    .line 9298
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 9299
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 9301
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->displayname_:Ljava/lang/Object;

    .line 9304
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getLocalDisplaynameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 9620
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->localDisplayname_:Ljava/lang/Object;

    .line 9621
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 9622
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 9624
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->localDisplayname_:Ljava/lang/Object;

    .line 9627
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getNetworkmessageBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 9371
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->networkmessage_:Ljava/lang/Object;

    .line 9372
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 9373
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 9375
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->networkmessage_:Ljava/lang/Object;

    .line 9378
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getSessionIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 9235
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->sessionId_:Ljava/lang/Object;

    .line 9236
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 9237
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 9239
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->sessionId_:Ljava/lang/Object;

    .line 9242
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getTypeBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 9203
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->type_:Ljava/lang/Object;

    .line 9204
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 9205
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 9207
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->type_:Ljava/lang/Object;

    .line 9210
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getVideoRingbackBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 9515
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->videoRingback_:Ljava/lang/Object;

    .line 9516
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 9517
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 9519
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->videoRingback_:Ljava/lang/Object;

    .line 9522
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getVideoRingbackPrologueBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 9547
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->videoRingbackPrologue_:Ljava/lang/Object;

    .line 9548
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 9549
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 9551
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->videoRingbackPrologue_:Ljava/lang/Object;

    .line 9554
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 9632
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 9633
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->accountId_:Ljava/lang/Object;

    .line 9634
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->type_:Ljava/lang/Object;

    .line 9635
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->sessionId_:Ljava/lang/Object;

    .line 9636
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->present_:Z

    .line 9637
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->direction_:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    .line 9638
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->speakerOn_:Z

    .line 9639
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->displayname_:Ljava/lang/Object;

    .line 9640
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->deviceContactId_:J

    .line 9641
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->displaymessage_:Ljava/lang/Object;

    .line 9642
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->networkmessage_:Ljava/lang/Object;

    .line 9643
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->muted_:Z

    .line 9644
    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->timestamp_:I

    .line 9645
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->callid_:Ljava/lang/Object;

    .line 9646
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->CAM_NONE:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->cameraPosition_:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    .line 9647
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->fromUi_:Z

    .line 9648
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->unawsered_:Z

    .line 9649
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->vgoodsPurchased_:Z

    .line 9650
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->VGOOD_YES:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->vgoodSupport_:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    .line 9651
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->videoMode_:Z

    .line 9652
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->videoRingback_:Ljava/lang/Object;

    .line 9653
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->videoRingbackPrologue_:Ljava/lang/Object;

    .line 9654
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->vgoodBundle_:Ljava/util/List;

    .line 9655
    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->emptySlotCount_:I

    .line 9656
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->showWand_:Z

    .line 9657
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->localDisplayname_:Ljava/lang/Object;

    .line 9658
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1

    .prologue
    .line 9949
    #calls: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->access$10900()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 9952
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 9918
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    .line 9919
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9920
    #calls: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->access$10800(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    .line 9922
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 9929
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    .line 9930
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9931
    #calls: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->access$10800(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    .line 9933
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 9885
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->access$10800(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 9891
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->access$10800(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 9939
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->access$10800(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 9945
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->access$10800(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 9907
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->access$10800(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 9913
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->access$10800(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 9896
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->access$10800(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 9902
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;->access$10800(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAccountId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 9157
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->accountId_:Ljava/lang/Object;

    .line 9158
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 9159
    check-cast v0, Ljava/lang/String;

    .line 9167
    :goto_0
    return-object v0

    .line 9161
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 9163
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 9164
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 9165
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->accountId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 9167
    goto :goto_0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 9147
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCallid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 9409
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->callid_:Ljava/lang/Object;

    .line 9410
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 9411
    check-cast v0, Ljava/lang/String;

    .line 9419
    :goto_0
    return-object v0

    .line 9413
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 9415
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 9416
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 9417
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->callid_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 9419
    goto :goto_0
.end method

.method public getCameraPosition()Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;
    .locals 1

    .prologue
    .line 9441
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->cameraPosition_:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 9074
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;
    .locals 1

    .prologue
    .line 9089
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    return-object v0
.end method

.method public getDeviceContactId()J
    .locals 2

    .prologue
    .line 9315
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->deviceContactId_:J

    return-wide v0
.end method

.method public getDirection()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;
    .locals 1

    .prologue
    .line 9263
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->direction_:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    return-object v0
.end method

.method public getDisplaymessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 9325
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->displaymessage_:Ljava/lang/Object;

    .line 9326
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 9327
    check-cast v0, Ljava/lang/String;

    .line 9335
    :goto_0
    return-object v0

    .line 9329
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 9331
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 9332
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 9333
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->displaymessage_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 9335
    goto :goto_0
.end method

.method public getDisplayname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 9283
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->displayname_:Ljava/lang/Object;

    .line 9284
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 9285
    check-cast v0, Ljava/lang/String;

    .line 9293
    :goto_0
    return-object v0

    .line 9287
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 9289
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 9290
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 9291
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->displayname_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 9293
    goto :goto_0
.end method

.method public getEmptySlotCount()I
    .locals 1

    .prologue
    .line 9586
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->emptySlotCount_:I

    return v0
.end method

.method public getFromUi()Z
    .locals 1

    .prologue
    .line 9451
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->fromUi_:Z

    return v0
.end method

.method public getLocalDisplayname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 9606
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->localDisplayname_:Ljava/lang/Object;

    .line 9607
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 9608
    check-cast v0, Ljava/lang/String;

    .line 9616
    :goto_0
    return-object v0

    .line 9610
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 9612
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 9613
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 9614
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->localDisplayname_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 9616
    goto :goto_0
.end method

.method public getMuted()Z
    .locals 1

    .prologue
    .line 9389
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->muted_:Z

    return v0
.end method

.method public getNetworkmessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 9357
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->networkmessage_:Ljava/lang/Object;

    .line 9358
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 9359
    check-cast v0, Ljava/lang/String;

    .line 9367
    :goto_0
    return-object v0

    .line 9361
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 9363
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 9364
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 9365
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->networkmessage_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 9367
    goto :goto_0
.end method

.method public getPresent()Z
    .locals 1

    .prologue
    .line 9253
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->present_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 9765
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->memoizedSerializedSize:I

    .line 9766
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 9874
    :goto_0
    return v0

    .line 9769
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_1a

    .line 9770
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v4

    .line 9773
    :goto_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_1

    .line 9774
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getAccountIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9777
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_2

    .line 9778
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getTypeBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9781
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v6, :cond_3

    .line 9782
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getSessionIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9785
    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_4

    .line 9786
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->present_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 9789
    :cond_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_5

    .line 9790
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->direction_:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9793
    :cond_5
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_6

    .line 9794
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->speakerOn_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 9797
    :cond_6
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_7

    .line 9798
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDisplaynameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v6, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9801
    :cond_7
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_8

    .line 9802
    const/16 v1, 0x9

    iget-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->deviceContactId_:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 9805
    :cond_8
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_9

    .line 9806
    const/16 v1, 0xa

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDisplaymessageBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9809
    :cond_9
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x400

    const/16 v2, 0x400

    if-ne v1, v2, :cond_a

    .line 9810
    const/16 v1, 0xb

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getNetworkmessageBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9813
    :cond_a
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x800

    const/16 v2, 0x800

    if-ne v1, v2, :cond_b

    .line 9814
    const/16 v1, 0xc

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->muted_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 9817
    :cond_b
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x1000

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_c

    .line 9818
    const/16 v1, 0xd

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->timestamp_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9821
    :cond_c
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x2000

    const/16 v2, 0x2000

    if-ne v1, v2, :cond_d

    .line 9822
    const/16 v1, 0xe

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getCallidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9825
    :cond_d
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x4000

    const/16 v2, 0x4000

    if-ne v1, v2, :cond_e

    .line 9826
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->cameraPosition_:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9829
    :cond_e
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    const v2, 0x8000

    and-int/2addr v1, v2

    const v2, 0x8000

    if-ne v1, v2, :cond_f

    .line 9830
    const/16 v1, 0x10

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->fromUi_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 9833
    :cond_f
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    const/high16 v2, 0x1

    and-int/2addr v1, v2

    const/high16 v2, 0x1

    if-ne v1, v2, :cond_10

    .line 9834
    const/16 v1, 0x11

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->unawsered_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 9837
    :cond_10
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    const/high16 v2, 0x2

    and-int/2addr v1, v2

    const/high16 v2, 0x2

    if-ne v1, v2, :cond_11

    .line 9838
    const/16 v1, 0x12

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->vgoodsPurchased_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 9841
    :cond_11
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    const/high16 v2, 0x4

    and-int/2addr v1, v2

    const/high16 v2, 0x4

    if-ne v1, v2, :cond_12

    .line 9842
    const/16 v1, 0x13

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->vgoodSupport_:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9845
    :cond_12
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    const/high16 v2, 0x8

    and-int/2addr v1, v2

    const/high16 v2, 0x8

    if-ne v1, v2, :cond_13

    .line 9846
    const/16 v1, 0x14

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->videoMode_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 9849
    :cond_13
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    const/high16 v2, 0x10

    and-int/2addr v1, v2

    const/high16 v2, 0x10

    if-ne v1, v2, :cond_14

    .line 9850
    const/16 v1, 0x15

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getVideoRingbackBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9853
    :cond_14
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    const/high16 v2, 0x20

    and-int/2addr v1, v2

    const/high16 v2, 0x20

    if-ne v1, v2, :cond_15

    .line 9854
    const/16 v1, 0x16

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getVideoRingbackPrologueBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_15
    move v1, v4

    move v2, v0

    .line 9857
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_16

    .line 9858
    const/16 v3, 0x17

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 9857
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 9861
    :cond_16
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    const/high16 v1, 0x40

    and-int/2addr v0, v1

    const/high16 v1, 0x40

    if-ne v0, v1, :cond_19

    .line 9862
    const/16 v0, 0x18

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->emptySlotCount_:I

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v0

    add-int/2addr v0, v2

    .line 9865
    :goto_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    const/high16 v2, 0x80

    and-int/2addr v1, v2

    const/high16 v2, 0x80

    if-ne v1, v2, :cond_17

    .line 9866
    const/16 v1, 0x19

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->showWand_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 9869
    :cond_17
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    const/high16 v2, 0x100

    and-int/2addr v1, v2

    const/high16 v2, 0x100

    if-ne v1, v2, :cond_18

    .line 9870
    const/16 v1, 0x1a

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getLocalDisplaynameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9873
    :cond_18
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->memoizedSerializedSize:I

    goto/16 :goto_0

    :cond_19
    move v0, v2

    goto :goto_3

    :cond_1a
    move v0, v4

    goto/16 :goto_1
.end method

.method public getSessionId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 9221
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->sessionId_:Ljava/lang/Object;

    .line 9222
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 9223
    check-cast v0, Ljava/lang/String;

    .line 9231
    :goto_0
    return-object v0

    .line 9225
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 9227
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 9228
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 9229
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->sessionId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 9231
    goto :goto_0
.end method

.method public getShowWand()Z
    .locals 1

    .prologue
    .line 9596
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->showWand_:Z

    return v0
.end method

.method public getSpeakerOn()Z
    .locals 1

    .prologue
    .line 9273
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->speakerOn_:Z

    return v0
.end method

.method public getTimestamp()I
    .locals 1

    .prologue
    .line 9399
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->timestamp_:I

    return v0
.end method

.method public getType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 9189
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->type_:Ljava/lang/Object;

    .line 9190
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 9191
    check-cast v0, Ljava/lang/String;

    .line 9199
    :goto_0
    return-object v0

    .line 9193
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 9195
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 9196
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 9197
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->type_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 9199
    goto :goto_0
.end method

.method public getUnawsered()Z
    .locals 1

    .prologue
    .line 9461
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->unawsered_:Z

    return v0
.end method

.method public getVgoodBundle(I)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    .locals 1
    .parameter

    .prologue
    .line 9572
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    return-object v0
.end method

.method public getVgoodBundleCount()I
    .locals 1

    .prologue
    .line 9569
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getVgoodBundleList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 9562
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->vgoodBundle_:Ljava/util/List;

    return-object v0
.end method

.method public getVgoodBundleOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundleOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 9576
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundleOrBuilder;

    return-object v0
.end method

.method public getVgoodBundleOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodBundleOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 9566
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->vgoodBundle_:Ljava/util/List;

    return-object v0
.end method

.method public getVgoodSupport()Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;
    .locals 1

    .prologue
    .line 9481
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->vgoodSupport_:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    return-object v0
.end method

.method public getVgoodsPurchased()Z
    .locals 1

    .prologue
    .line 9471
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->vgoodsPurchased_:Z

    return v0
.end method

.method public getVideoMode()Z
    .locals 1

    .prologue
    .line 9491
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->videoMode_:Z

    return v0
.end method

.method public getVideoRingback()Ljava/lang/String;
    .locals 2

    .prologue
    .line 9501
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->videoRingback_:Ljava/lang/Object;

    .line 9502
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 9503
    check-cast v0, Ljava/lang/String;

    .line 9511
    :goto_0
    return-object v0

    .line 9505
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 9507
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 9508
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 9509
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->videoRingback_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 9511
    goto :goto_0
.end method

.method public getVideoRingbackPrologue()Ljava/lang/String;
    .locals 2

    .prologue
    .line 9533
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->videoRingbackPrologue_:Ljava/lang/Object;

    .line 9534
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 9535
    check-cast v0, Ljava/lang/String;

    .line 9543
    :goto_0
    return-object v0

    .line 9537
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 9539
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 9540
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 9541
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->videoRingbackPrologue_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 9543
    goto :goto_0
.end method

.method public hasAccountId()Z
    .locals 2

    .prologue
    .line 9154
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 9144
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCallid()Z
    .locals 2

    .prologue
    .line 9406
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCameraPosition()Z
    .locals 2

    .prologue
    .line 9438
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDeviceContactId()Z
    .locals 2

    .prologue
    .line 9312
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDirection()Z
    .locals 2

    .prologue
    .line 9260
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDisplaymessage()Z
    .locals 2

    .prologue
    .line 9322
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDisplayname()Z
    .locals 2

    .prologue
    .line 9280
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasEmptySlotCount()Z
    .locals 2

    .prologue
    const/high16 v1, 0x40

    .line 9583
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFromUi()Z
    .locals 2

    .prologue
    const v1, 0x8000

    .line 9448
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLocalDisplayname()Z
    .locals 2

    .prologue
    const/high16 v1, 0x100

    .line 9603
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMuted()Z
    .locals 2

    .prologue
    .line 9386
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNetworkmessage()Z
    .locals 2

    .prologue
    .line 9354
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPresent()Z
    .locals 2

    .prologue
    .line 9250
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSessionId()Z
    .locals 2

    .prologue
    .line 9218
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasShowWand()Z
    .locals 2

    .prologue
    const/high16 v1, 0x80

    .line 9593
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSpeakerOn()Z
    .locals 2

    .prologue
    .line 9270
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTimestamp()Z
    .locals 2

    .prologue
    .line 9396
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    .prologue
    .line 9186
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUnawsered()Z
    .locals 2

    .prologue
    const/high16 v1, 0x1

    .line 9458
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVgoodSupport()Z
    .locals 2

    .prologue
    const/high16 v1, 0x4

    .line 9478
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVgoodsPurchased()Z
    .locals 2

    .prologue
    const/high16 v1, 0x2

    .line 9468
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoMode()Z
    .locals 2

    .prologue
    const/high16 v1, 0x8

    .line 9488
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoRingback()Z
    .locals 2

    .prologue
    const/high16 v1, 0x10

    .line 9498
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoRingbackPrologue()Z
    .locals 2

    .prologue
    const/high16 v1, 0x20

    .line 9530
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 9661
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->memoizedIsInitialized:B

    .line 9662
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 9677
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 9662
    goto :goto_0

    .line 9664
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 9665
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 9666
    goto :goto_0

    .line 9668
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasAccountId()Z

    move-result v0

    if-nez v0, :cond_3

    .line 9669
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 9670
    goto :goto_0

    .line 9672
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    .line 9673
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 9674
    goto :goto_0

    .line 9676
    :cond_4
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 9677
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 9074
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1

    .prologue
    .line 9950
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 9074
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;
    .locals 1

    .prologue
    .line 9954
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;)Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 9879
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 6
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x10

    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 9682
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getSerializedSize()I

    .line 9683
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 9684
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 9686
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 9687
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getAccountIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 9689
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 9690
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getTypeBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 9692
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 9693
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getSessionIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 9695
    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v5, :cond_4

    .line 9696
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->present_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 9698
    :cond_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 9699
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->direction_:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 9701
    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 9702
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->speakerOn_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 9704
    :cond_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 9705
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDisplaynameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 9707
    :cond_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    .line 9708
    const/16 v0, 0x9

    iget-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->deviceContactId_:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 9710
    :cond_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_9

    .line 9711
    const/16 v0, 0xa

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDisplaymessageBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 9713
    :cond_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_a

    .line 9714
    const/16 v0, 0xb

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getNetworkmessageBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 9716
    :cond_a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_b

    .line 9717
    const/16 v0, 0xc

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->muted_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 9719
    :cond_b
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_c

    .line 9720
    const/16 v0, 0xd

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->timestamp_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 9722
    :cond_c
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_d

    .line 9723
    const/16 v0, 0xe

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getCallidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 9725
    :cond_d
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_e

    .line 9726
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->cameraPosition_:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 9728
    :cond_e
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_f

    .line 9729
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->fromUi_:Z

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 9731
    :cond_f
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    const/high16 v1, 0x1

    and-int/2addr v0, v1

    const/high16 v1, 0x1

    if-ne v0, v1, :cond_10

    .line 9732
    const/16 v0, 0x11

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->unawsered_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 9734
    :cond_10
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    const/high16 v1, 0x2

    and-int/2addr v0, v1

    const/high16 v1, 0x2

    if-ne v0, v1, :cond_11

    .line 9735
    const/16 v0, 0x12

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->vgoodsPurchased_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 9737
    :cond_11
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    const/high16 v1, 0x4

    and-int/2addr v0, v1

    const/high16 v1, 0x4

    if-ne v0, v1, :cond_12

    .line 9738
    const/16 v0, 0x13

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->vgoodSupport_:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 9740
    :cond_12
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    const/high16 v1, 0x8

    and-int/2addr v0, v1

    const/high16 v1, 0x8

    if-ne v0, v1, :cond_13

    .line 9741
    const/16 v0, 0x14

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->videoMode_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 9743
    :cond_13
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    const/high16 v1, 0x10

    and-int/2addr v0, v1

    const/high16 v1, 0x10

    if-ne v0, v1, :cond_14

    .line 9744
    const/16 v0, 0x15

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getVideoRingbackBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 9746
    :cond_14
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    const/high16 v1, 0x20

    and-int/2addr v0, v1

    const/high16 v1, 0x20

    if-ne v0, v1, :cond_15

    .line 9747
    const/16 v0, 0x16

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getVideoRingbackPrologueBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 9749
    :cond_15
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_16

    .line 9750
    const/16 v2, 0x17

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 9749
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 9752
    :cond_16
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    const/high16 v1, 0x40

    and-int/2addr v0, v1

    const/high16 v1, 0x40

    if-ne v0, v1, :cond_17

    .line 9753
    const/16 v0, 0x18

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->emptySlotCount_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 9755
    :cond_17
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    const/high16 v1, 0x80

    and-int/2addr v0, v1

    const/high16 v1, 0x80

    if-ne v0, v1, :cond_18

    .line 9756
    const/16 v0, 0x19

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->showWand_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 9758
    :cond_18
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->bitField0_:I

    const/high16 v1, 0x100

    and-int/2addr v0, v1

    const/high16 v1, 0x100

    if-ne v0, v1, :cond_19

    .line 9759
    const/16 v0, 0x1a

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getLocalDisplaynameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 9761
    :cond_19
    return-void
.end method
