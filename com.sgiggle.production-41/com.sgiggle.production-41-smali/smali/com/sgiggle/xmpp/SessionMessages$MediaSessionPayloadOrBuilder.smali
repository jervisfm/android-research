.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "MediaSessionPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getAccountId()Ljava/lang/String;
.end method

.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getCallid()Ljava/lang/String;
.end method

.method public abstract getCameraPosition()Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;
.end method

.method public abstract getDeviceContactId()J
.end method

.method public abstract getDirection()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;
.end method

.method public abstract getDisplaymessage()Ljava/lang/String;
.end method

.method public abstract getDisplayname()Ljava/lang/String;
.end method

.method public abstract getEmptySlotCount()I
.end method

.method public abstract getFromUi()Z
.end method

.method public abstract getLocalDisplayname()Ljava/lang/String;
.end method

.method public abstract getMuted()Z
.end method

.method public abstract getNetworkmessage()Ljava/lang/String;
.end method

.method public abstract getPresent()Z
.end method

.method public abstract getSessionId()Ljava/lang/String;
.end method

.method public abstract getShowWand()Z
.end method

.method public abstract getSpeakerOn()Z
.end method

.method public abstract getTimestamp()I
.end method

.method public abstract getType()Ljava/lang/String;
.end method

.method public abstract getUnawsered()Z
.end method

.method public abstract getVgoodBundle(I)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
.end method

.method public abstract getVgoodBundleCount()I
.end method

.method public abstract getVgoodBundleList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getVgoodSupport()Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;
.end method

.method public abstract getVgoodsPurchased()Z
.end method

.method public abstract getVideoMode()Z
.end method

.method public abstract getVideoRingback()Ljava/lang/String;
.end method

.method public abstract getVideoRingbackPrologue()Ljava/lang/String;
.end method

.method public abstract hasAccountId()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasCallid()Z
.end method

.method public abstract hasCameraPosition()Z
.end method

.method public abstract hasDeviceContactId()Z
.end method

.method public abstract hasDirection()Z
.end method

.method public abstract hasDisplaymessage()Z
.end method

.method public abstract hasDisplayname()Z
.end method

.method public abstract hasEmptySlotCount()Z
.end method

.method public abstract hasFromUi()Z
.end method

.method public abstract hasLocalDisplayname()Z
.end method

.method public abstract hasMuted()Z
.end method

.method public abstract hasNetworkmessage()Z
.end method

.method public abstract hasPresent()Z
.end method

.method public abstract hasSessionId()Z
.end method

.method public abstract hasShowWand()Z
.end method

.method public abstract hasSpeakerOn()Z
.end method

.method public abstract hasTimestamp()Z
.end method

.method public abstract hasType()Z
.end method

.method public abstract hasUnawsered()Z
.end method

.method public abstract hasVgoodSupport()Z
.end method

.method public abstract hasVgoodsPurchased()Z
.end method

.method public abstract hasVideoMode()Z
.end method

.method public abstract hasVideoRingback()Z
.end method

.method public abstract hasVideoRingbackPrologue()Z
.end method
