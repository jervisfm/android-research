.class public final enum Lcom/sgiggle/xmpp/SessionMessages$MediaType;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MediaType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$MediaType;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$MediaType; = null

.field public static final enum EMAIL:Lcom/sgiggle/xmpp/SessionMessages$MediaType; = null

.field public static final EMAIL_VALUE:I = 0x2

.field public static final enum NO_MEDIA:Lcom/sgiggle/xmpp/SessionMessages$MediaType; = null

.field public static final NO_MEDIA_VALUE:I = 0x0

.field public static final enum SMS:Lcom/sgiggle/xmpp/SessionMessages$MediaType; = null

.field public static final enum SMS_EMAIL:Lcom/sgiggle/xmpp/SessionMessages$MediaType; = null

.field public static final SMS_EMAIL_VALUE:I = 0x3

.field public static final SMS_VALUE:I = 0x1

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$MediaType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 348
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    const-string v1, "NO_MEDIA"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sgiggle/xmpp/SessionMessages$MediaType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaType;->NO_MEDIA:Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    .line 349
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    const-string v1, "SMS"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sgiggle/xmpp/SessionMessages$MediaType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaType;->SMS:Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    .line 350
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    const-string v1, "EMAIL"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/sgiggle/xmpp/SessionMessages$MediaType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaType;->EMAIL:Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    .line 351
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    const-string v1, "SMS_EMAIL"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/sgiggle/xmpp/SessionMessages$MediaType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaType;->SMS_EMAIL:Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    .line 346
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaType;->NO_MEDIA:Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaType;->SMS:Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaType;->EMAIL:Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaType;->SMS_EMAIL:Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    .line 377
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$MediaType$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaType$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 386
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 387
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaType;->value:I

    .line 388
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$MediaType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 374
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$MediaType;
    .locals 1
    .parameter

    .prologue
    .line 363
    packed-switch p0, :pswitch_data_0

    .line 368
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 364
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaType;->NO_MEDIA:Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    goto :goto_0

    .line 365
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaType;->SMS:Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    goto :goto_0

    .line 366
    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaType;->EMAIL:Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    goto :goto_0

    .line 367
    :pswitch_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaType;->SMS_EMAIL:Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    goto :goto_0

    .line 363
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$MediaType;
    .locals 1
    .parameter

    .prologue
    .line 346
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$MediaType;
    .locals 1

    .prologue
    .line 346
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$MediaType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 360
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$MediaType;->value:I

    return v0
.end method
