.class public final Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntriesOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;",
        "Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntriesOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private entry_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 23959
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 24068
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->entry_:Ljava/util/List;

    .line 23960
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->maybeForceBuilderInitialization()V

    .line 23961
    return-void
.end method

.method static synthetic access$30500(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 23954
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$30600()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;
    .locals 1

    .prologue
    .line 23954
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 23994
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;

    move-result-object v0

    .line 23995
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 23996
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 23999
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;
    .locals 1

    .prologue
    .line 23966
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;-><init>()V

    return-object v0
.end method

.method private ensureEntryIsMutable()V
    .locals 2

    .prologue
    .line 24071
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 24072
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->entry_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->entry_:Ljava/util/List;

    .line 24073
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->bitField0_:I

    .line 24075
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 23964
    return-void
.end method


# virtual methods
.method public addAllEntry(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;"
        }
    .end annotation

    .prologue
    .line 24138
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->ensureEntryIsMutable()V

    .line 24139
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->entry_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 24141
    return-object p0
.end method

.method public addEntry(ILcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 24131
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->ensureEntryIsMutable()V

    .line 24132
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->entry_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 24134
    return-object p0
.end method

.method public addEntry(ILcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 24114
    if-nez p2, :cond_0

    .line 24115
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 24117
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->ensureEntryIsMutable()V

    .line 24118
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->entry_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 24120
    return-object p0
.end method

.method public addEntry(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;
    .locals 2
    .parameter

    .prologue
    .line 24124
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->ensureEntryIsMutable()V

    .line 24125
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->entry_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 24127
    return-object p0
.end method

.method public addEntry(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;
    .locals 1
    .parameter

    .prologue
    .line 24104
    if-nez p1, :cond_0

    .line 24105
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 24107
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->ensureEntryIsMutable()V

    .line 24108
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->entry_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 24110
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 23954
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;
    .locals 2

    .prologue
    .line 23985
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;

    move-result-object v0

    .line 23986
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 23987
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 23989
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 23954
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;
    .locals 3

    .prologue
    .line 24003
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;-><init>(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 24004
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->bitField0_:I

    .line 24005
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 24006
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->entry_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->entry_:Ljava/util/List;

    .line 24007
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->bitField0_:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->bitField0_:I

    .line 24009
    :cond_0
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->entry_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->entry_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->access$30802(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;Ljava/util/List;)Ljava/util/List;

    .line 24010
    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 23954
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 23954
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;
    .locals 1

    .prologue
    .line 23970
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 23971
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->entry_:Ljava/util/List;

    .line 23972
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->bitField0_:I

    .line 23973
    return-object p0
.end method

.method public clearEntry()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;
    .locals 1

    .prologue
    .line 24144
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->entry_:Ljava/util/List;

    .line 24145
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->bitField0_:I

    .line 24147
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 23954
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 23954
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 23954
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;
    .locals 2

    .prologue
    .line 23977
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 23954
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 23954
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 23954
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;
    .locals 1

    .prologue
    .line 23981
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;

    move-result-object v0

    return-object v0
.end method

.method public getEntry(I)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
    .locals 1
    .parameter

    .prologue
    .line 24084
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->entry_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    return-object v0
.end method

.method public getEntryCount()I
    .locals 1

    .prologue
    .line 24081
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->entry_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getEntryList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 24078
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->entry_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 24029
    move v0, v2

    :goto_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->getEntryCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 24030
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->getEntry(I)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    move v0, v2

    .line 24035
    :goto_1
    return v0

    .line 24029
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 24035
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23954
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 23954
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23954
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24043
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 24044
    sparse-switch v0, :sswitch_data_0

    .line 24049
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 24051
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 24047
    goto :goto_1

    .line 24056
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    .line 24057
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 24058
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->addEntry(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    goto :goto_0

    .line 24044
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;
    .locals 2
    .parameter

    .prologue
    .line 24014
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 24025
    :goto_0
    return-object v0

    .line 24015
    :cond_0
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->entry_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->access$30800(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 24016
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->entry_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 24017
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->entry_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->access$30800(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->entry_:Ljava/util/List;

    .line 24018
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->bitField0_:I

    :cond_1
    :goto_1
    move-object v0, p0

    .line 24025
    goto :goto_0

    .line 24020
    :cond_2
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->ensureEntryIsMutable()V

    .line 24021
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->entry_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->entry_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->access$30800(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public removeEntry(I)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;
    .locals 1
    .parameter

    .prologue
    .line 24150
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->ensureEntryIsMutable()V

    .line 24151
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->entry_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 24153
    return-object p0
.end method

.method public setEntry(ILcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 24098
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->ensureEntryIsMutable()V

    .line 24099
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->entry_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 24101
    return-object p0
.end method

.method public setEntry(ILcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 24088
    if-nez p2, :cond_0

    .line 24089
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 24091
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->ensureEntryIsMutable()V

    .line 24092
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->entry_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 24094
    return-object p0
.end method
