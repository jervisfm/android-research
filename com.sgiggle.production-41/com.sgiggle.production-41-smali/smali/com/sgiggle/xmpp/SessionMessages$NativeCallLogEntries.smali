.class public final Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntriesOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NativeCallLogEntries"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;
    }
.end annotation


# static fields
.field public static final ENTRY_FIELD_NUMBER:I = 0x1

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;


# instance fields
.field private entry_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 24160
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;

    .line 24161
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->initFields()V

    .line 24162
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 23801
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 23838
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->memoizedIsInitialized:B

    .line 23861
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->memoizedSerializedSize:I

    .line 23802
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 23796
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;-><init>(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 23803
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 23838
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->memoizedIsInitialized:B

    .line 23861
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->memoizedSerializedSize:I

    .line 23803
    return-void
.end method

.method static synthetic access$30800(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 23796
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->entry_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$30802(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 23796
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->entry_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;
    .locals 1

    .prologue
    .line 23807
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 23836
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->entry_:Ljava/util/List;

    .line 23837
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;
    .locals 1

    .prologue
    .line 23947
    #calls: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->access$30600()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;
    .locals 1
    .parameter

    .prologue
    .line 23950
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23916
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    move-result-object v0

    .line 23917
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 23918
    #calls: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->access$30500(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;

    move-result-object v0

    .line 23920
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23927
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    move-result-object v0

    .line 23928
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 23929
    #calls: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->access$30500(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;

    move-result-object v0

    .line 23931
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 23883
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->access$30500(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 23889
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->access$30500(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23937
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->access$30500(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23943
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->access$30500(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23905
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->access$30500(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23911
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->access$30500(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 23894
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->access$30500(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 23900
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;->access$30500(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 23796
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;
    .locals 1

    .prologue
    .line 23811
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;

    return-object v0
.end method

.method public getEntry(I)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
    .locals 1
    .parameter

    .prologue
    .line 23828
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->entry_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    return-object v0
.end method

.method public getEntryCount()I
    .locals 1

    .prologue
    .line 23825
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->entry_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getEntryList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 23818
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->entry_:Ljava/util/List;

    return-object v0
.end method

.method public getEntryOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntryOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 23832
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->entry_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntryOrBuilder;

    return-object v0
.end method

.method public getEntryOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntryOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 23822
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->entry_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 23863
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->memoizedSerializedSize:I

    .line 23864
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 23872
    :goto_0
    return v0

    :cond_0
    move v1, v2

    .line 23867
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->entry_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 23868
    const/4 v3, 0x1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->entry_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 23867
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_1

    .line 23871
    :cond_1
    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->memoizedSerializedSize:I

    move v0, v2

    .line 23872
    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 23840
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->memoizedIsInitialized:B

    .line 23841
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 23850
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 23841
    goto :goto_0

    :cond_1
    move v0, v2

    .line 23843
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->getEntryCount()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 23844
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->getEntry(I)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_2

    .line 23845
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->memoizedIsInitialized:B

    move v0, v2

    .line 23846
    goto :goto_0

    .line 23843
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 23849
    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->memoizedIsInitialized:B

    move v0, v3

    .line 23850
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 23796
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;
    .locals 1

    .prologue
    .line 23948
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 23796
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;
    .locals 1

    .prologue
    .line 23952
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 23877
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23855
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->getSerializedSize()I

    .line 23856
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->entry_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 23857
    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntries;->entry_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 23856
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 23859
    :cond_0
    return-void
.end method
