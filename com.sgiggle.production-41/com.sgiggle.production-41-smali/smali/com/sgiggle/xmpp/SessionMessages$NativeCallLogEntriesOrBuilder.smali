.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntriesOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "NativeCallLogEntriesOrBuilder"
.end annotation


# virtual methods
.method public abstract getEntry(I)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
.end method

.method public abstract getEntryCount()I
.end method

.method public abstract getEntryList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;",
            ">;"
        }
    .end annotation
.end method
