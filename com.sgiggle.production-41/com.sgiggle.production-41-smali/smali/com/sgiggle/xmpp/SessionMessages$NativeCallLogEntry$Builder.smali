.class public final Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntryOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;",
        "Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntryOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private callType_:Lcom/sgiggle/xmpp/SessionMessages$CallType;

.field private contact_:Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

.field private duration_:J

.field private phoneNumber_:Ljava/lang/Object;

.field private startTime_:J


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 23450
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 23632
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->phoneNumber_:Ljava/lang/Object;

    .line 23668
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    .line 23711
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallType;->CALL_TYPE_INBOUND_CONNECTED:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->callType_:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    .line 23451
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->maybeForceBuilderInitialization()V

    .line 23452
    return-void
.end method

.method static synthetic access$29600(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 23445
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$29700()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;
    .locals 1

    .prologue
    .line 23445
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 23493
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    move-result-object v0

    .line 23494
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 23495
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 23498
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;
    .locals 1

    .prologue
    .line 23457
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 23455
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 23445
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
    .locals 2

    .prologue
    .line 23484
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    move-result-object v0

    .line 23485
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 23486
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 23488
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 23445
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
    .locals 5

    .prologue
    .line 23502
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;-><init>(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 23503
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    .line 23504
    const/4 v2, 0x0

    .line 23505
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 23506
    or-int/lit8 v2, v2, 0x1

    .line 23508
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->phoneNumber_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->phoneNumber_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->access$29902(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23509
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 23510
    or-int/lit8 v2, v2, 0x2

    .line 23512
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->contact_:Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->access$30002(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;Lcom/sgiggle/xmpp/SessionMessages$ContactItem;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    .line 23513
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 23514
    or-int/lit8 v2, v2, 0x4

    .line 23516
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->callType_:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->callType_:Lcom/sgiggle/xmpp/SessionMessages$CallType;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->access$30102(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;Lcom/sgiggle/xmpp/SessionMessages$CallType;)Lcom/sgiggle/xmpp/SessionMessages$CallType;

    .line 23517
    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 23518
    or-int/lit8 v2, v2, 0x8

    .line 23520
    :cond_3
    iget-wide v3, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->startTime_:J

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->startTime_:J
    invoke-static {v0, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->access$30202(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;J)J

    .line 23521
    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_4

    .line 23522
    or-int/lit8 v1, v2, 0x10

    .line 23524
    :goto_0
    iget-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->duration_:J

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->duration_:J
    invoke-static {v0, v2, v3}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->access$30302(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;J)J

    .line 23525
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->access$30402(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;I)I

    .line 23526
    return-object v0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 23445
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 23445
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 23461
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 23462
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->phoneNumber_:Ljava/lang/Object;

    .line 23463
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    .line 23464
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    .line 23465
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    .line 23466
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallType;->CALL_TYPE_INBOUND_CONNECTED:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->callType_:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    .line 23467
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    .line 23468
    iput-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->startTime_:J

    .line 23469
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    .line 23470
    iput-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->duration_:J

    .line 23471
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    .line 23472
    return-object p0
.end method

.method public clearCallType()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;
    .locals 1

    .prologue
    .line 23728
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    .line 23729
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallType;->CALL_TYPE_INBOUND_CONNECTED:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->callType_:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    .line 23731
    return-object p0
.end method

.method public clearContact()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;
    .locals 1

    .prologue
    .line 23704
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    .line 23706
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    .line 23707
    return-object p0
.end method

.method public clearDuration()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;
    .locals 2

    .prologue
    .line 23770
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    .line 23771
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->duration_:J

    .line 23773
    return-object p0
.end method

.method public clearPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;
    .locals 1

    .prologue
    .line 23656
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    .line 23657
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->getPhoneNumber()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->phoneNumber_:Ljava/lang/Object;

    .line 23659
    return-object p0
.end method

.method public clearStartTime()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;
    .locals 2

    .prologue
    .line 23749
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    .line 23750
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->startTime_:J

    .line 23752
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 23445
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 23445
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 23445
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;
    .locals 2

    .prologue
    .line 23476
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 23445
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getCallType()Lcom/sgiggle/xmpp/SessionMessages$CallType;
    .locals 1

    .prologue
    .line 23716
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->callType_:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    return-object v0
.end method

.method public getContact()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    .locals 1

    .prologue
    .line 23673
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 23445
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 23445
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
    .locals 1

    .prologue
    .line 23480
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    move-result-object v0

    return-object v0
.end method

.method public getDuration()J
    .locals 2

    .prologue
    .line 23761
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->duration_:J

    return-wide v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 2

    .prologue
    .line 23637
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->phoneNumber_:Ljava/lang/Object;

    .line 23638
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 23639
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 23640
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->phoneNumber_:Ljava/lang/Object;

    .line 23643
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getStartTime()J
    .locals 2

    .prologue
    .line 23740
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->startTime_:J

    return-wide v0
.end method

.method public hasCallType()Z
    .locals 2

    .prologue
    .line 23713
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasContact()Z
    .locals 2

    .prologue
    .line 23670
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDuration()Z
    .locals 2

    .prologue
    .line 23758
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPhoneNumber()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 23634
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasStartTime()Z
    .locals 2

    .prologue
    .line 23737
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 23550
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->hasPhoneNumber()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 23572
    :goto_0
    return v0

    .line 23554
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->hasCallType()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 23556
    goto :goto_0

    .line 23558
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->hasStartTime()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 23560
    goto :goto_0

    .line 23562
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->hasDuration()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 23564
    goto :goto_0

    .line 23566
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->hasContact()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 23567
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->getContact()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 23569
    goto :goto_0

    .line 23572
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeContact(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;
    .locals 2
    .parameter

    .prologue
    .line 23692
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 23694
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    .line 23700
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    .line 23701
    return-object p0

    .line 23697
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23445
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 23445
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23445
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23580
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 23581
    sparse-switch v0, :sswitch_data_0

    .line 23586
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 23588
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 23584
    goto :goto_1

    .line 23593
    :sswitch_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    .line 23594
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->phoneNumber_:Ljava/lang/Object;

    goto :goto_0

    .line 23598
    :sswitch_2
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    .line 23599
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->hasContact()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 23600
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->getContact()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    .line 23602
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 23603
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->setContact(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    goto :goto_0

    .line 23607
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 23608
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallType;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$CallType;

    move-result-object v0

    .line 23609
    if-eqz v0, :cond_0

    .line 23610
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    .line 23611
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->callType_:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    goto :goto_0

    .line 23616
    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    .line 23617
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->startTime_:J

    goto :goto_0

    .line 23621
    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    .line 23622
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->duration_:J

    goto :goto_0

    .line 23581
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;
    .locals 2
    .parameter

    .prologue
    .line 23530
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 23546
    :goto_0
    return-object v0

    .line 23531
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->hasPhoneNumber()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 23532
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->getPhoneNumber()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->setPhoneNumber(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    .line 23534
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->hasContact()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 23535
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->getContact()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->mergeContact(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    .line 23537
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->hasCallType()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 23538
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->getCallType()Lcom/sgiggle/xmpp/SessionMessages$CallType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->setCallType(Lcom/sgiggle/xmpp/SessionMessages$CallType;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    .line 23540
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->hasStartTime()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 23541
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->getStartTime()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->setStartTime(J)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    .line 23543
    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->hasDuration()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 23544
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->getDuration()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->setDuration(J)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    :cond_5
    move-object v0, p0

    .line 23546
    goto :goto_0
.end method

.method public setCallType(Lcom/sgiggle/xmpp/SessionMessages$CallType;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 23719
    if-nez p1, :cond_0

    .line 23720
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 23722
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    .line 23723
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->callType_:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    .line 23725
    return-object p0
.end method

.method public setContact(Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 23686
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    .line 23688
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    .line 23689
    return-object p0
.end method

.method public setContact(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 23676
    if-nez p1, :cond_0

    .line 23677
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 23679
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    .line 23681
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    .line 23682
    return-object p0
.end method

.method public setDuration(J)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 23764
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    .line 23765
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->duration_:J

    .line 23767
    return-object p0
.end method

.method public setPhoneNumber(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 23647
    if-nez p1, :cond_0

    .line 23648
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 23650
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    .line 23651
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->phoneNumber_:Ljava/lang/Object;

    .line 23653
    return-object p0
.end method

.method setPhoneNumber(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 23662
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    .line 23663
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->phoneNumber_:Ljava/lang/Object;

    .line 23665
    return-void
.end method

.method public setStartTime(J)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 23743
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->bitField0_:I

    .line 23744
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->startTime_:J

    .line 23746
    return-object p0
.end method
