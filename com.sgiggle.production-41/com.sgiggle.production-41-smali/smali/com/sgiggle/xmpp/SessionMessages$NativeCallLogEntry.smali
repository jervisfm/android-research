.class public final Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntryOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NativeCallLogEntry"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;
    }
.end annotation


# static fields
.field public static final CALL_TYPE_FIELD_NUMBER:I = 0x3

.field public static final CONTACT_FIELD_NUMBER:I = 0x2

.field public static final DURATION_FIELD_NUMBER:I = 0x5

.field public static final PHONE_NUMBER_FIELD_NUMBER:I = 0x1

.field public static final START_TIME_FIELD_NUMBER:I = 0x4

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;


# instance fields
.field private bitField0_:I

.field private callType_:Lcom/sgiggle/xmpp/SessionMessages$CallType;

.field private contact_:Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

.field private duration_:J

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private phoneNumber_:Ljava/lang/Object;

.field private startTime_:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23780
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    .line 23781
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->initFields()V

    .line 23782
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 23192
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 23285
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->memoizedIsInitialized:B

    .line 23336
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->memoizedSerializedSize:I

    .line 23193
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 23187
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;-><init>(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 23194
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 23285
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->memoizedIsInitialized:B

    .line 23336
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->memoizedSerializedSize:I

    .line 23194
    return-void
.end method

.method static synthetic access$29902(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 23187
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->phoneNumber_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$30002(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;Lcom/sgiggle/xmpp/SessionMessages$ContactItem;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 23187
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->contact_:Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    return-object p1
.end method

.method static synthetic access$30102(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;Lcom/sgiggle/xmpp/SessionMessages$CallType;)Lcom/sgiggle/xmpp/SessionMessages$CallType;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 23187
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->callType_:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    return-object p1
.end method

.method static synthetic access$30202(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;J)J
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 23187
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->startTime_:J

    return-wide p1
.end method

.method static synthetic access$30302(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;J)J
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 23187
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->duration_:J

    return-wide p1
.end method

.method static synthetic access$30402(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 23187
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
    .locals 1

    .prologue
    .line 23198
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    return-object v0
.end method

.method private getPhoneNumberBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 23227
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->phoneNumber_:Ljava/lang/Object;

    .line 23228
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 23229
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 23231
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->phoneNumber_:Ljava/lang/Object;

    .line 23234
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 23279
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->phoneNumber_:Ljava/lang/Object;

    .line 23280
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->contact_:Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    .line 23281
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallType;->CALL_TYPE_INBOUND_CONNECTED:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->callType_:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    .line 23282
    iput-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->startTime_:J

    .line 23283
    iput-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->duration_:J

    .line 23284
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;
    .locals 1

    .prologue
    .line 23438
    #calls: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->access$29700()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 23441
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23407
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    .line 23408
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 23409
    #calls: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->access$29600(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    move-result-object v0

    .line 23411
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23418
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    .line 23419
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 23420
    #calls: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->access$29600(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    move-result-object v0

    .line 23422
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 23374
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->access$29600(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 23380
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->access$29600(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23428
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->access$29600(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23434
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->access$29600(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23396
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->access$29600(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23402
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->access$29600(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 23385
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->access$29600(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 23391
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;->access$29600(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCallType()Lcom/sgiggle/xmpp/SessionMessages$CallType;
    .locals 1

    .prologue
    .line 23255
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->callType_:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    return-object v0
.end method

.method public getContact()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    .locals 1

    .prologue
    .line 23245
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->contact_:Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 23187
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;
    .locals 1

    .prologue
    .line 23202
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;

    return-object v0
.end method

.method public getDuration()J
    .locals 2

    .prologue
    .line 23275
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->duration_:J

    return-wide v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 2

    .prologue
    .line 23213
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->phoneNumber_:Ljava/lang/Object;

    .line 23214
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 23215
    check-cast v0, Ljava/lang/String;

    .line 23223
    :goto_0
    return-object v0

    .line 23217
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 23219
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 23220
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 23221
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->phoneNumber_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 23223
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 23338
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->memoizedSerializedSize:I

    .line 23339
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 23363
    :goto_0
    return v0

    .line 23341
    :cond_0
    const/4 v0, 0x0

    .line 23342
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 23343
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->getPhoneNumberBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23346
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 23347
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->contact_:Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23350
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 23351
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->callType_:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$CallType;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 23354
    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 23355
    iget-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->startTime_:J

    invoke-static {v4, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 23358
    :cond_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 23359
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->duration_:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 23362
    :cond_5
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getStartTime()J
    .locals 2

    .prologue
    .line 23265
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->startTime_:J

    return-wide v0
.end method

.method public hasCallType()Z
    .locals 2

    .prologue
    .line 23252
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasContact()Z
    .locals 2

    .prologue
    .line 23242
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDuration()Z
    .locals 2

    .prologue
    .line 23272
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPhoneNumber()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 23210
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasStartTime()Z
    .locals 2

    .prologue
    .line 23262
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 23287
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->memoizedIsInitialized:B

    .line 23288
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 23313
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 23288
    goto :goto_0

    .line 23290
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->hasPhoneNumber()Z

    move-result v0

    if-nez v0, :cond_2

    .line 23291
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->memoizedIsInitialized:B

    move v0, v2

    .line 23292
    goto :goto_0

    .line 23294
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->hasCallType()Z

    move-result v0

    if-nez v0, :cond_3

    .line 23295
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->memoizedIsInitialized:B

    move v0, v2

    .line 23296
    goto :goto_0

    .line 23298
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->hasStartTime()Z

    move-result v0

    if-nez v0, :cond_4

    .line 23299
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->memoizedIsInitialized:B

    move v0, v2

    .line 23300
    goto :goto_0

    .line 23302
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->hasDuration()Z

    move-result v0

    if-nez v0, :cond_5

    .line 23303
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->memoizedIsInitialized:B

    move v0, v2

    .line 23304
    goto :goto_0

    .line 23306
    :cond_5
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->hasContact()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 23307
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->getContact()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_6

    .line 23308
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->memoizedIsInitialized:B

    move v0, v2

    .line 23309
    goto :goto_0

    .line 23312
    :cond_6
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->memoizedIsInitialized:B

    move v0, v3

    .line 23313
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 23187
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;
    .locals 1

    .prologue
    .line 23439
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 23187
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;
    .locals 1

    .prologue
    .line 23443
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;)Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 23368
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 23318
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->getSerializedSize()I

    .line 23319
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 23320
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->getPhoneNumberBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 23322
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 23323
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->contact_:Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 23325
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 23326
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->callType_:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$CallType;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 23328
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 23329
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->startTime_:J

    invoke-virtual {p1, v3, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt64(IJ)V

    .line 23331
    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 23332
    const/4 v0, 0x5

    iget-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntry;->duration_:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeUInt64(IJ)V

    .line 23334
    :cond_4
    return-void
.end method
