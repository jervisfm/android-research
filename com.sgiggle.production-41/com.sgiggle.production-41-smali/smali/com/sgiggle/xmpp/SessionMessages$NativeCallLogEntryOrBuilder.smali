.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$NativeCallLogEntryOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "NativeCallLogEntryOrBuilder"
.end annotation


# virtual methods
.method public abstract getCallType()Lcom/sgiggle/xmpp/SessionMessages$CallType;
.end method

.method public abstract getContact()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
.end method

.method public abstract getDuration()J
.end method

.method public abstract getPhoneNumber()Ljava/lang/String;
.end method

.method public abstract getStartTime()J
.end method

.method public abstract hasCallType()Z
.end method

.method public abstract hasContact()Z
.end method

.method public abstract hasDuration()Z
.end method

.method public abstract hasPhoneNumber()Z
.end method

.method public abstract hasStartTime()Z
.end method
