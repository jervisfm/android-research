.class public final Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private accountid_:Ljava/lang/Object;

.field private actionClass_:Ljava/lang/Object;

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private displayname_:Ljava/lang/Object;

.field private expiration_:J

.field private firstname_:Ljava/lang/Object;

.field private lastname_:Ljava/lang/Object;

.field private message_:Ljava/lang/Object;

.field private middlename_:Ljava/lang/Object;

.field private nameprefix_:Ljava/lang/Object;

.field private namesuffix_:Ljava/lang/Object;

.field private notificationid_:Ljava/lang/Object;

.field private title_:Ljava/lang/Object;

.field private when_:J


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 47892
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 48182
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 48225
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->title_:Ljava/lang/Object;

    .line 48261
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->message_:Ljava/lang/Object;

    .line 48339
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->actionClass_:Ljava/lang/Object;

    .line 48375
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->notificationid_:Ljava/lang/Object;

    .line 48411
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->lastname_:Ljava/lang/Object;

    .line 48447
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->firstname_:Ljava/lang/Object;

    .line 48483
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->accountid_:Ljava/lang/Object;

    .line 48519
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->nameprefix_:Ljava/lang/Object;

    .line 48555
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->middlename_:Ljava/lang/Object;

    .line 48591
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->namesuffix_:Ljava/lang/Object;

    .line 48627
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->displayname_:Ljava/lang/Object;

    .line 47893
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->maybeForceBuilderInitialization()V

    .line 47894
    return-void
.end method

.method static synthetic access$60500(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 47887
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$60600()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1

    .prologue
    .line 47887
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 47953
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    .line 47954
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 47955
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 47958
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1

    .prologue
    .line 47899
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 47897
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 47887
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;
    .locals 2

    .prologue
    .line 47944
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    .line 47945
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 47946
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 47948
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 47887
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;
    .locals 5

    .prologue
    .line 47962
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 47963
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 47964
    const/4 v2, 0x0

    .line 47965
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 47966
    or-int/lit8 v2, v2, 0x1

    .line 47968
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->access$60802(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 47969
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 47970
    or-int/lit8 v2, v2, 0x2

    .line 47972
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->title_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->title_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->access$60902(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47973
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 47974
    or-int/lit8 v2, v2, 0x4

    .line 47976
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->message_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->message_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->access$61002(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47977
    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 47978
    or-int/lit8 v2, v2, 0x8

    .line 47980
    :cond_3
    iget-wide v3, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->when_:J

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->when_:J
    invoke-static {v0, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->access$61102(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;J)J

    .line 47981
    and-int/lit8 v3, v1, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 47982
    or-int/lit8 v2, v2, 0x10

    .line 47984
    :cond_4
    iget-wide v3, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->expiration_:J

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->expiration_:J
    invoke-static {v0, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->access$61202(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;J)J

    .line 47985
    and-int/lit8 v3, v1, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 47986
    or-int/lit8 v2, v2, 0x20

    .line 47988
    :cond_5
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->actionClass_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->actionClass_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->access$61302(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47989
    and-int/lit8 v3, v1, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    .line 47990
    or-int/lit8 v2, v2, 0x40

    .line 47992
    :cond_6
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->notificationid_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->notificationid_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->access$61402(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47993
    and-int/lit16 v3, v1, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    .line 47994
    or-int/lit16 v2, v2, 0x80

    .line 47996
    :cond_7
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->lastname_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->lastname_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->access$61502(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47997
    and-int/lit16 v3, v1, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_8

    .line 47998
    or-int/lit16 v2, v2, 0x100

    .line 48000
    :cond_8
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->firstname_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->firstname_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->access$61602(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48001
    and-int/lit16 v3, v1, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_9

    .line 48002
    or-int/lit16 v2, v2, 0x200

    .line 48004
    :cond_9
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->accountid_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->accountid_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->access$61702(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48005
    and-int/lit16 v3, v1, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_a

    .line 48006
    or-int/lit16 v2, v2, 0x400

    .line 48008
    :cond_a
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->nameprefix_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->nameprefix_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->access$61802(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48009
    and-int/lit16 v3, v1, 0x800

    const/16 v4, 0x800

    if-ne v3, v4, :cond_b

    .line 48010
    or-int/lit16 v2, v2, 0x800

    .line 48012
    :cond_b
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->middlename_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->middlename_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->access$61902(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48013
    and-int/lit16 v3, v1, 0x1000

    const/16 v4, 0x1000

    if-ne v3, v4, :cond_c

    .line 48014
    or-int/lit16 v2, v2, 0x1000

    .line 48016
    :cond_c
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->namesuffix_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->namesuffix_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->access$62002(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48017
    and-int/lit16 v1, v1, 0x2000

    const/16 v3, 0x2000

    if-ne v1, v3, :cond_d

    .line 48018
    or-int/lit16 v1, v2, 0x2000

    .line 48020
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->displayname_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->displayname_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->access$62102(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48021
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->access$62202(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;I)I

    .line 48022
    return-object v0

    :cond_d
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 47887
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 47887
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 47903
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 47904
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 47905
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 47906
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->title_:Ljava/lang/Object;

    .line 47907
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 47908
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->message_:Ljava/lang/Object;

    .line 47909
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 47910
    iput-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->when_:J

    .line 47911
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 47912
    iput-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->expiration_:J

    .line 47913
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 47914
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->actionClass_:Ljava/lang/Object;

    .line 47915
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 47916
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->notificationid_:Ljava/lang/Object;

    .line 47917
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 47918
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->lastname_:Ljava/lang/Object;

    .line 47919
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 47920
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->firstname_:Ljava/lang/Object;

    .line 47921
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 47922
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->accountid_:Ljava/lang/Object;

    .line 47923
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 47924
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->nameprefix_:Ljava/lang/Object;

    .line 47925
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 47926
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->middlename_:Ljava/lang/Object;

    .line 47927
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 47928
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->namesuffix_:Ljava/lang/Object;

    .line 47929
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 47930
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->displayname_:Ljava/lang/Object;

    .line 47931
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x2001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 47932
    return-object p0
.end method

.method public clearAccountid()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1

    .prologue
    .line 48507
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48508
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getAccountid()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->accountid_:Ljava/lang/Object;

    .line 48510
    return-object p0
.end method

.method public clearActionClass()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1

    .prologue
    .line 48363
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48364
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getActionClass()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->actionClass_:Ljava/lang/Object;

    .line 48366
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1

    .prologue
    .line 48218
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 48220
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48221
    return-object p0
.end method

.method public clearDisplayname()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1

    .prologue
    .line 48651
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x2001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48652
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getDisplayname()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->displayname_:Ljava/lang/Object;

    .line 48654
    return-object p0
.end method

.method public clearExpiration()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 2

    .prologue
    .line 48332
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48333
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->expiration_:J

    .line 48335
    return-object p0
.end method

.method public clearFirstname()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1

    .prologue
    .line 48471
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48472
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getFirstname()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->firstname_:Ljava/lang/Object;

    .line 48474
    return-object p0
.end method

.method public clearLastname()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1

    .prologue
    .line 48435
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48436
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getLastname()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->lastname_:Ljava/lang/Object;

    .line 48438
    return-object p0
.end method

.method public clearMessage()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1

    .prologue
    .line 48285
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48286
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->message_:Ljava/lang/Object;

    .line 48288
    return-object p0
.end method

.method public clearMiddlename()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1

    .prologue
    .line 48579
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48580
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getMiddlename()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->middlename_:Ljava/lang/Object;

    .line 48582
    return-object p0
.end method

.method public clearNameprefix()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1

    .prologue
    .line 48543
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48544
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getNameprefix()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->nameprefix_:Ljava/lang/Object;

    .line 48546
    return-object p0
.end method

.method public clearNamesuffix()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1

    .prologue
    .line 48615
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48616
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getNamesuffix()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->namesuffix_:Ljava/lang/Object;

    .line 48618
    return-object p0
.end method

.method public clearNotificationid()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1

    .prologue
    .line 48399
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48400
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getNotificationid()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->notificationid_:Ljava/lang/Object;

    .line 48402
    return-object p0
.end method

.method public clearTitle()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1

    .prologue
    .line 48249
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48250
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getTitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->title_:Ljava/lang/Object;

    .line 48252
    return-object p0
.end method

.method public clearWhen()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 2

    .prologue
    .line 48311
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48312
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->when_:J

    .line 48314
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 47887
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 47887
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 47887
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 2

    .prologue
    .line 47936
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 47887
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAccountid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 48488
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->accountid_:Ljava/lang/Object;

    .line 48489
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 48490
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 48491
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->accountid_:Ljava/lang/Object;

    .line 48494
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getActionClass()Ljava/lang/String;
    .locals 2

    .prologue
    .line 48344
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->actionClass_:Ljava/lang/Object;

    .line 48345
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 48346
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 48347
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->actionClass_:Ljava/lang/Object;

    .line 48350
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 48187
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 47887
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 47887
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;
    .locals 1

    .prologue
    .line 47940
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 48632
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->displayname_:Ljava/lang/Object;

    .line 48633
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 48634
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 48635
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->displayname_:Ljava/lang/Object;

    .line 48638
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getExpiration()J
    .locals 2

    .prologue
    .line 48323
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->expiration_:J

    return-wide v0
.end method

.method public getFirstname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 48452
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->firstname_:Ljava/lang/Object;

    .line 48453
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 48454
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 48455
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->firstname_:Ljava/lang/Object;

    .line 48458
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getLastname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 48416
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->lastname_:Ljava/lang/Object;

    .line 48417
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 48418
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 48419
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->lastname_:Ljava/lang/Object;

    .line 48422
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 48266
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->message_:Ljava/lang/Object;

    .line 48267
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 48268
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 48269
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->message_:Ljava/lang/Object;

    .line 48272
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getMiddlename()Ljava/lang/String;
    .locals 2

    .prologue
    .line 48560
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->middlename_:Ljava/lang/Object;

    .line 48561
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 48562
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 48563
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->middlename_:Ljava/lang/Object;

    .line 48566
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getNameprefix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 48524
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->nameprefix_:Ljava/lang/Object;

    .line 48525
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 48526
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 48527
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->nameprefix_:Ljava/lang/Object;

    .line 48530
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getNamesuffix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 48596
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->namesuffix_:Ljava/lang/Object;

    .line 48597
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 48598
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 48599
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->namesuffix_:Ljava/lang/Object;

    .line 48602
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getNotificationid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 48380
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->notificationid_:Ljava/lang/Object;

    .line 48381
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 48382
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 48383
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->notificationid_:Ljava/lang/Object;

    .line 48386
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 48230
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->title_:Ljava/lang/Object;

    .line 48231
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 48232
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 48233
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->title_:Ljava/lang/Object;

    .line 48236
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getWhen()J
    .locals 2

    .prologue
    .line 48302
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->when_:J

    return-wide v0
.end method

.method public hasAccountid()Z
    .locals 2

    .prologue
    .line 48485
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasActionClass()Z
    .locals 2

    .prologue
    .line 48341
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 48184
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDisplayname()Z
    .locals 2

    .prologue
    .line 48629
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasExpiration()Z
    .locals 2

    .prologue
    .line 48320
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFirstname()Z
    .locals 2

    .prologue
    .line 48449
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLastname()Z
    .locals 2

    .prologue
    .line 48413
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMessage()Z
    .locals 2

    .prologue
    .line 48263
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMiddlename()Z
    .locals 2

    .prologue
    .line 48557
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNameprefix()Z
    .locals 2

    .prologue
    .line 48521
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNamesuffix()Z
    .locals 2

    .prologue
    .line 48593
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNotificationid()Z
    .locals 2

    .prologue
    .line 48377
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTitle()Z
    .locals 2

    .prologue
    .line 48227
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasWhen()Z
    .locals 2

    .prologue
    .line 48299
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 48073
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 48081
    :goto_0
    return v0

    .line 48077
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 48079
    goto :goto_0

    .line 48081
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 48206
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 48208
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 48214
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48215
    return-object p0

    .line 48211
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47887
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 47887
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47887
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48089
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 48090
    sparse-switch v0, :sswitch_data_0

    .line 48095
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 48097
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 48093
    goto :goto_1

    .line 48102
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 48103
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 48104
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 48106
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 48107
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    goto :goto_0

    .line 48111
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48112
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->title_:Ljava/lang/Object;

    goto :goto_0

    .line 48116
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48117
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->message_:Ljava/lang/Object;

    goto :goto_0

    .line 48121
    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48122
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->when_:J

    goto :goto_0

    .line 48126
    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48127
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->expiration_:J

    goto :goto_0

    .line 48131
    :sswitch_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48132
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->actionClass_:Ljava/lang/Object;

    goto :goto_0

    .line 48136
    :sswitch_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48137
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->notificationid_:Ljava/lang/Object;

    goto :goto_0

    .line 48141
    :sswitch_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48142
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->lastname_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 48146
    :sswitch_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48147
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->firstname_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 48151
    :sswitch_a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48152
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->accountid_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 48156
    :sswitch_b
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48157
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->nameprefix_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 48161
    :sswitch_c
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48162
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->middlename_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 48166
    :sswitch_d
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48167
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->namesuffix_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 48171
    :sswitch_e
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48172
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->displayname_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 48090
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 48026
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 48069
    :goto_0
    return-object v0

    .line 48027
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 48028
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    .line 48030
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->hasTitle()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 48031
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->setTitle(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    .line 48033
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->hasMessage()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 48034
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->setMessage(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    .line 48036
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->hasWhen()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 48037
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getWhen()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->setWhen(J)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    .line 48039
    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->hasExpiration()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 48040
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getExpiration()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->setExpiration(J)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    .line 48042
    :cond_5
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->hasActionClass()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 48043
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getActionClass()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->setActionClass(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    .line 48045
    :cond_6
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->hasNotificationid()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 48046
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getNotificationid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->setNotificationid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    .line 48048
    :cond_7
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->hasLastname()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 48049
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getLastname()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->setLastname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    .line 48051
    :cond_8
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->hasFirstname()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 48052
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getFirstname()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->setFirstname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    .line 48054
    :cond_9
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->hasAccountid()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 48055
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getAccountid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->setAccountid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    .line 48057
    :cond_a
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->hasNameprefix()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 48058
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getNameprefix()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->setNameprefix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    .line 48060
    :cond_b
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->hasMiddlename()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 48061
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getMiddlename()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->setMiddlename(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    .line 48063
    :cond_c
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->hasNamesuffix()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 48064
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getNamesuffix()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->setNamesuffix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    .line 48066
    :cond_d
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->hasDisplayname()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 48067
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getDisplayname()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->setDisplayname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    :cond_e
    move-object v0, p0

    .line 48069
    goto/16 :goto_0
.end method

.method public setAccountid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 48498
    if-nez p1, :cond_0

    .line 48499
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 48501
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48502
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->accountid_:Ljava/lang/Object;

    .line 48504
    return-object p0
.end method

.method setAccountid(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 48513
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48514
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->accountid_:Ljava/lang/Object;

    .line 48516
    return-void
.end method

.method public setActionClass(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 48354
    if-nez p1, :cond_0

    .line 48355
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 48357
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48358
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->actionClass_:Ljava/lang/Object;

    .line 48360
    return-object p0
.end method

.method setActionClass(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 48369
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48370
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->actionClass_:Ljava/lang/Object;

    .line 48372
    return-void
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 48200
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 48202
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48203
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 48190
    if-nez p1, :cond_0

    .line 48191
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 48193
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 48195
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48196
    return-object p0
.end method

.method public setDisplayname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 48642
    if-nez p1, :cond_0

    .line 48643
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 48645
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48646
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->displayname_:Ljava/lang/Object;

    .line 48648
    return-object p0
.end method

.method setDisplayname(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 48657
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48658
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->displayname_:Ljava/lang/Object;

    .line 48660
    return-void
.end method

.method public setExpiration(J)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 48326
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48327
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->expiration_:J

    .line 48329
    return-object p0
.end method

.method public setFirstname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 48462
    if-nez p1, :cond_0

    .line 48463
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 48465
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48466
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->firstname_:Ljava/lang/Object;

    .line 48468
    return-object p0
.end method

.method setFirstname(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 48477
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48478
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->firstname_:Ljava/lang/Object;

    .line 48480
    return-void
.end method

.method public setLastname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 48426
    if-nez p1, :cond_0

    .line 48427
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 48429
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48430
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->lastname_:Ljava/lang/Object;

    .line 48432
    return-object p0
.end method

.method setLastname(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 48441
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48442
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->lastname_:Ljava/lang/Object;

    .line 48444
    return-void
.end method

.method public setMessage(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 48276
    if-nez p1, :cond_0

    .line 48277
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 48279
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48280
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->message_:Ljava/lang/Object;

    .line 48282
    return-object p0
.end method

.method setMessage(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 48291
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48292
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->message_:Ljava/lang/Object;

    .line 48294
    return-void
.end method

.method public setMiddlename(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 48570
    if-nez p1, :cond_0

    .line 48571
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 48573
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48574
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->middlename_:Ljava/lang/Object;

    .line 48576
    return-object p0
.end method

.method setMiddlename(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 48585
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48586
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->middlename_:Ljava/lang/Object;

    .line 48588
    return-void
.end method

.method public setNameprefix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 48534
    if-nez p1, :cond_0

    .line 48535
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 48537
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48538
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->nameprefix_:Ljava/lang/Object;

    .line 48540
    return-object p0
.end method

.method setNameprefix(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 48549
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48550
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->nameprefix_:Ljava/lang/Object;

    .line 48552
    return-void
.end method

.method public setNamesuffix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 48606
    if-nez p1, :cond_0

    .line 48607
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 48609
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48610
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->namesuffix_:Ljava/lang/Object;

    .line 48612
    return-object p0
.end method

.method setNamesuffix(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 48621
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48622
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->namesuffix_:Ljava/lang/Object;

    .line 48624
    return-void
.end method

.method public setNotificationid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 48390
    if-nez p1, :cond_0

    .line 48391
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 48393
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48394
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->notificationid_:Ljava/lang/Object;

    .line 48396
    return-object p0
.end method

.method setNotificationid(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 48405
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48406
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->notificationid_:Ljava/lang/Object;

    .line 48408
    return-void
.end method

.method public setTitle(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 48240
    if-nez p1, :cond_0

    .line 48241
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 48243
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48244
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->title_:Ljava/lang/Object;

    .line 48246
    return-object p0
.end method

.method setTitle(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 48255
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48256
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->title_:Ljava/lang/Object;

    .line 48258
    return-void
.end method

.method public setWhen(J)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 48305
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->bitField0_:I

    .line 48306
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->when_:J

    .line 48308
    return-object p0
.end method
