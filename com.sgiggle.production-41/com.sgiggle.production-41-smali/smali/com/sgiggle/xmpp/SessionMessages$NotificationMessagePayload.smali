.class public final Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NotificationMessagePayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    }
.end annotation


# static fields
.field public static final ACCOUNTID_FIELD_NUMBER:I = 0xa

.field public static final ACTION_CLASS_FIELD_NUMBER:I = 0x6

.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final DISPLAYNAME_FIELD_NUMBER:I = 0xe

.field public static final EXPIRATION_FIELD_NUMBER:I = 0x5

.field public static final FIRSTNAME_FIELD_NUMBER:I = 0x9

.field public static final LASTNAME_FIELD_NUMBER:I = 0x8

.field public static final MESSAGE_FIELD_NUMBER:I = 0x3

.field public static final MIDDLENAME_FIELD_NUMBER:I = 0xc

.field public static final NAMEPREFIX_FIELD_NUMBER:I = 0xb

.field public static final NAMESUFFIX_FIELD_NUMBER:I = 0xd

.field public static final NOTIFICATIONID_FIELD_NUMBER:I = 0x7

.field public static final TITLE_FIELD_NUMBER:I = 0x2

.field public static final WHEN_FIELD_NUMBER:I = 0x4

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;


# instance fields
.field private accountid_:Ljava/lang/Object;

.field private actionClass_:Ljava/lang/Object;

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private displayname_:Ljava/lang/Object;

.field private expiration_:J

.field private firstname_:Ljava/lang/Object;

.field private lastname_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private message_:Ljava/lang/Object;

.field private middlename_:Ljava/lang/Object;

.field private nameprefix_:Ljava/lang/Object;

.field private namesuffix_:Ljava/lang/Object;

.field private notificationid_:Ljava/lang/Object;

.field private title_:Ljava/lang/Object;

.field private when_:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 48666
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    .line 48667
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->initFields()V

    .line 48668
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 47266
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 47678
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->memoizedIsInitialized:B

    .line 47742
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->memoizedSerializedSize:I

    .line 47267
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 47261
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 47268
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 47678
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->memoizedIsInitialized:B

    .line 47742
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->memoizedSerializedSize:I

    .line 47268
    return-void
.end method

.method static synthetic access$60802(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 47261
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$60902(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 47261
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->title_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$61002(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 47261
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->message_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$61102(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;J)J
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 47261
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->when_:J

    return-wide p1
.end method

.method static synthetic access$61202(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;J)J
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 47261
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->expiration_:J

    return-wide p1
.end method

.method static synthetic access$61302(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 47261
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->actionClass_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$61402(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 47261
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->notificationid_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$61502(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 47261
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->lastname_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$61602(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 47261
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->firstname_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$61702(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 47261
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->accountid_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$61802(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 47261
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->nameprefix_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$61902(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 47261
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->middlename_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$62002(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 47261
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->namesuffix_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$62102(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 47261
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->displayname_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$62202(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 47261
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    return p1
.end method

.method private getAccountidBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 47523
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->accountid_:Ljava/lang/Object;

    .line 47524
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 47525
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 47527
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->accountid_:Ljava/lang/Object;

    .line 47530
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getActionClassBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 47395
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->actionClass_:Ljava/lang/Object;

    .line 47396
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 47397
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 47399
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->actionClass_:Ljava/lang/Object;

    .line 47402
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;
    .locals 1

    .prologue
    .line 47272
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    return-object v0
.end method

.method private getDisplaynameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 47651
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->displayname_:Ljava/lang/Object;

    .line 47652
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 47653
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 47655
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->displayname_:Ljava/lang/Object;

    .line 47658
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getFirstnameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 47491
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->firstname_:Ljava/lang/Object;

    .line 47492
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 47493
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 47495
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->firstname_:Ljava/lang/Object;

    .line 47498
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getLastnameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 47459
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->lastname_:Ljava/lang/Object;

    .line 47460
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 47461
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 47463
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->lastname_:Ljava/lang/Object;

    .line 47466
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getMessageBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 47343
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->message_:Ljava/lang/Object;

    .line 47344
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 47345
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 47347
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->message_:Ljava/lang/Object;

    .line 47350
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getMiddlenameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 47587
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->middlename_:Ljava/lang/Object;

    .line 47588
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 47589
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 47591
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->middlename_:Ljava/lang/Object;

    .line 47594
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getNameprefixBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 47555
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->nameprefix_:Ljava/lang/Object;

    .line 47556
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 47557
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 47559
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->nameprefix_:Ljava/lang/Object;

    .line 47562
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getNamesuffixBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 47619
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->namesuffix_:Ljava/lang/Object;

    .line 47620
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 47621
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 47623
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->namesuffix_:Ljava/lang/Object;

    .line 47626
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getNotificationidBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 47427
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->notificationid_:Ljava/lang/Object;

    .line 47428
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 47429
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 47431
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->notificationid_:Ljava/lang/Object;

    .line 47434
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getTitleBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 47311
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->title_:Ljava/lang/Object;

    .line 47312
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 47313
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 47315
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->title_:Ljava/lang/Object;

    .line 47318
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 47663
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 47664
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->title_:Ljava/lang/Object;

    .line 47665
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->message_:Ljava/lang/Object;

    .line 47666
    iput-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->when_:J

    .line 47667
    iput-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->expiration_:J

    .line 47668
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->actionClass_:Ljava/lang/Object;

    .line 47669
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->notificationid_:Ljava/lang/Object;

    .line 47670
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->lastname_:Ljava/lang/Object;

    .line 47671
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->firstname_:Ljava/lang/Object;

    .line 47672
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->accountid_:Ljava/lang/Object;

    .line 47673
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->nameprefix_:Ljava/lang/Object;

    .line 47674
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->middlename_:Ljava/lang/Object;

    .line 47675
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->namesuffix_:Ljava/lang/Object;

    .line 47676
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->displayname_:Ljava/lang/Object;

    .line 47677
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1

    .prologue
    .line 47880
    #calls: Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->access$60600()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 47883
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47849
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    move-result-object v0

    .line 47850
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 47851
    #calls: Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->access$60500(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    .line 47853
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47860
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    move-result-object v0

    .line 47861
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 47862
    #calls: Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->access$60500(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    .line 47864
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 47816
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->access$60500(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 47822
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->access$60500(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47870
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->access$60500(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47876
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->access$60500(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47838
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->access$60500(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47844
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->access$60500(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 47827
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->access$60500(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 47833
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;->access$60500(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAccountid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 47509
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->accountid_:Ljava/lang/Object;

    .line 47510
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 47511
    check-cast v0, Ljava/lang/String;

    .line 47519
    :goto_0
    return-object v0

    .line 47513
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 47515
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 47516
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 47517
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->accountid_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 47519
    goto :goto_0
.end method

.method public getActionClass()Ljava/lang/String;
    .locals 2

    .prologue
    .line 47381
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->actionClass_:Ljava/lang/Object;

    .line 47382
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 47383
    check-cast v0, Ljava/lang/String;

    .line 47391
    :goto_0
    return-object v0

    .line 47385
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 47387
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 47388
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 47389
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->actionClass_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 47391
    goto :goto_0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 47287
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 47261
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;
    .locals 1

    .prologue
    .line 47276
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    return-object v0
.end method

.method public getDisplayname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 47637
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->displayname_:Ljava/lang/Object;

    .line 47638
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 47639
    check-cast v0, Ljava/lang/String;

    .line 47647
    :goto_0
    return-object v0

    .line 47641
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 47643
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 47644
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 47645
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->displayname_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 47647
    goto :goto_0
.end method

.method public getExpiration()J
    .locals 2

    .prologue
    .line 47371
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->expiration_:J

    return-wide v0
.end method

.method public getFirstname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 47477
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->firstname_:Ljava/lang/Object;

    .line 47478
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 47479
    check-cast v0, Ljava/lang/String;

    .line 47487
    :goto_0
    return-object v0

    .line 47481
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 47483
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 47484
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 47485
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->firstname_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 47487
    goto :goto_0
.end method

.method public getLastname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 47445
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->lastname_:Ljava/lang/Object;

    .line 47446
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 47447
    check-cast v0, Ljava/lang/String;

    .line 47455
    :goto_0
    return-object v0

    .line 47449
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 47451
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 47452
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 47453
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->lastname_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 47455
    goto :goto_0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 47329
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->message_:Ljava/lang/Object;

    .line 47330
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 47331
    check-cast v0, Ljava/lang/String;

    .line 47339
    :goto_0
    return-object v0

    .line 47333
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 47335
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 47336
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 47337
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->message_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 47339
    goto :goto_0
.end method

.method public getMiddlename()Ljava/lang/String;
    .locals 2

    .prologue
    .line 47573
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->middlename_:Ljava/lang/Object;

    .line 47574
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 47575
    check-cast v0, Ljava/lang/String;

    .line 47583
    :goto_0
    return-object v0

    .line 47577
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 47579
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 47580
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 47581
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->middlename_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 47583
    goto :goto_0
.end method

.method public getNameprefix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 47541
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->nameprefix_:Ljava/lang/Object;

    .line 47542
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 47543
    check-cast v0, Ljava/lang/String;

    .line 47551
    :goto_0
    return-object v0

    .line 47545
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 47547
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 47548
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 47549
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->nameprefix_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 47551
    goto :goto_0
.end method

.method public getNamesuffix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 47605
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->namesuffix_:Ljava/lang/Object;

    .line 47606
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 47607
    check-cast v0, Ljava/lang/String;

    .line 47615
    :goto_0
    return-object v0

    .line 47609
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 47611
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 47612
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 47613
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->namesuffix_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 47615
    goto :goto_0
.end method

.method public getNotificationid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 47413
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->notificationid_:Ljava/lang/Object;

    .line 47414
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 47415
    check-cast v0, Ljava/lang/String;

    .line 47423
    :goto_0
    return-object v0

    .line 47417
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 47419
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 47420
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 47421
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->notificationid_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 47423
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 47744
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->memoizedSerializedSize:I

    .line 47745
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 47805
    :goto_0
    return v0

    .line 47747
    :cond_0
    const/4 v0, 0x0

    .line 47748
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 47749
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 47752
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 47753
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getTitleBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 47756
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 47757
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getMessageBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 47760
    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_4

    .line 47761
    iget-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->when_:J

    invoke-static {v4, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 47764
    :cond_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 47765
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->expiration_:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 47768
    :cond_5
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    .line 47769
    const/4 v1, 0x6

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getActionClassBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 47772
    :cond_6
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    .line 47773
    const/4 v1, 0x7

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getNotificationidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 47776
    :cond_7
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_8

    .line 47777
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getLastnameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 47780
    :cond_8
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_9

    .line 47781
    const/16 v1, 0x9

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getFirstnameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 47784
    :cond_9
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_a

    .line 47785
    const/16 v1, 0xa

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getAccountidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 47788
    :cond_a
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x400

    const/16 v2, 0x400

    if-ne v1, v2, :cond_b

    .line 47789
    const/16 v1, 0xb

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getNameprefixBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 47792
    :cond_b
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x800

    const/16 v2, 0x800

    if-ne v1, v2, :cond_c

    .line 47793
    const/16 v1, 0xc

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getMiddlenameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 47796
    :cond_c
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x1000

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_d

    .line 47797
    const/16 v1, 0xd

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getNamesuffixBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 47800
    :cond_d
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x2000

    const/16 v2, 0x2000

    if-ne v1, v2, :cond_e

    .line 47801
    const/16 v1, 0xe

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getDisplaynameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 47804
    :cond_e
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->memoizedSerializedSize:I

    goto/16 :goto_0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 47297
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->title_:Ljava/lang/Object;

    .line 47298
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 47299
    check-cast v0, Ljava/lang/String;

    .line 47307
    :goto_0
    return-object v0

    .line 47301
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 47303
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 47304
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 47305
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->title_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 47307
    goto :goto_0
.end method

.method public getWhen()J
    .locals 2

    .prologue
    .line 47361
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->when_:J

    return-wide v0
.end method

.method public hasAccountid()Z
    .locals 2

    .prologue
    .line 47506
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasActionClass()Z
    .locals 2

    .prologue
    .line 47378
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 47284
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDisplayname()Z
    .locals 2

    .prologue
    .line 47634
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasExpiration()Z
    .locals 2

    .prologue
    .line 47368
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFirstname()Z
    .locals 2

    .prologue
    .line 47474
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLastname()Z
    .locals 2

    .prologue
    .line 47442
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMessage()Z
    .locals 2

    .prologue
    .line 47326
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMiddlename()Z
    .locals 2

    .prologue
    .line 47570
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNameprefix()Z
    .locals 2

    .prologue
    .line 47538
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNamesuffix()Z
    .locals 2

    .prologue
    .line 47602
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNotificationid()Z
    .locals 2

    .prologue
    .line 47410
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTitle()Z
    .locals 2

    .prologue
    .line 47294
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasWhen()Z
    .locals 2

    .prologue
    .line 47358
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 47680
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->memoizedIsInitialized:B

    .line 47681
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 47692
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 47681
    goto :goto_0

    .line 47683
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 47684
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 47685
    goto :goto_0

    .line 47687
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 47688
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 47689
    goto :goto_0

    .line 47691
    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->memoizedIsInitialized:B

    move v0, v3

    .line 47692
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 47261
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1

    .prologue
    .line 47881
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 47261
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;
    .locals 1

    .prologue
    .line 47885
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;)Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 47810
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 47697
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getSerializedSize()I

    .line 47698
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 47699
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 47701
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 47702
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getTitleBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 47704
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 47705
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getMessageBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 47707
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 47708
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->when_:J

    invoke-virtual {p1, v3, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt64(IJ)V

    .line 47710
    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 47711
    const/4 v0, 0x5

    iget-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->expiration_:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeUInt64(IJ)V

    .line 47713
    :cond_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 47714
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getActionClassBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 47716
    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 47717
    const/4 v0, 0x7

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getNotificationidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 47719
    :cond_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 47720
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getLastnameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 47722
    :cond_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    .line 47723
    const/16 v0, 0x9

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getFirstnameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 47725
    :cond_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_9

    .line 47726
    const/16 v0, 0xa

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getAccountidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 47728
    :cond_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_a

    .line 47729
    const/16 v0, 0xb

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getNameprefixBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 47731
    :cond_a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_b

    .line 47732
    const/16 v0, 0xc

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getMiddlenameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 47734
    :cond_b
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_c

    .line 47735
    const/16 v0, 0xd

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getNamesuffixBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 47737
    :cond_c
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_d

    .line 47738
    const/16 v0, 0xe

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getDisplaynameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 47740
    :cond_d
    return-void
.end method
