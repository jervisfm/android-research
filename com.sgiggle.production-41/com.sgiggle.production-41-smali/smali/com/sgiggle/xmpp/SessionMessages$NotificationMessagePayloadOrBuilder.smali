.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "NotificationMessagePayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getAccountid()Ljava/lang/String;
.end method

.method public abstract getActionClass()Ljava/lang/String;
.end method

.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getDisplayname()Ljava/lang/String;
.end method

.method public abstract getExpiration()J
.end method

.method public abstract getFirstname()Ljava/lang/String;
.end method

.method public abstract getLastname()Ljava/lang/String;
.end method

.method public abstract getMessage()Ljava/lang/String;
.end method

.method public abstract getMiddlename()Ljava/lang/String;
.end method

.method public abstract getNameprefix()Ljava/lang/String;
.end method

.method public abstract getNamesuffix()Ljava/lang/String;
.end method

.method public abstract getNotificationid()Ljava/lang/String;
.end method

.method public abstract getTitle()Ljava/lang/String;
.end method

.method public abstract getWhen()J
.end method

.method public abstract hasAccountid()Z
.end method

.method public abstract hasActionClass()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasDisplayname()Z
.end method

.method public abstract hasExpiration()Z
.end method

.method public abstract hasFirstname()Z
.end method

.method public abstract hasLastname()Z
.end method

.method public abstract hasMessage()Z
.end method

.method public abstract hasMiddlename()Z
.end method

.method public abstract hasNameprefix()Z
.end method

.method public abstract hasNamesuffix()Z
.end method

.method public abstract hasNotificationid()Z
.end method

.method public abstract hasTitle()Z
.end method

.method public abstract hasWhen()Z
.end method
