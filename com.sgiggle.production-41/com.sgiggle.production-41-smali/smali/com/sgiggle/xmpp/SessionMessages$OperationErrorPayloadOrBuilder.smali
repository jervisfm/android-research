.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$OperationErrorPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OperationErrorPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getError()Lcom/sgiggle/xmpp/SessionMessages$ErrorType;
.end method

.method public abstract getOperation()Ljava/lang/String;
.end method

.method public abstract getReason()Ljava/lang/String;
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasError()Z
.end method

.method public abstract hasOperation()Z
.end method

.method public abstract hasReason()Z
.end method
