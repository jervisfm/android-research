.class public final enum Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AlertSeverity"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity; = null

.field public static final enum AS_CALL_KEY_INVALID:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity; = null

.field public static final AS_CALL_KEY_INVALID_VALUE:I = 0x10

.field public static final enum AS_ITEM_MESSAGING:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity; = null

.field public static final AS_ITEM_MESSAGING_VALUE:I = 0x20

.field public static final enum AS_LOCAL_NOTIFICATION:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity; = null

.field public static final AS_LOCAL_NOTIFICATION_VALUE:I = 0x80

.field public static final enum AS_OK:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity; = null

.field public static final AS_OK_VALUE:I = 0x0

.field public static final enum AS_REGISTRATION:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity; = null

.field public static final enum AS_REGISTRATION_EMAIL:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity; = null

.field public static final AS_REGISTRATION_EMAIL_VALUE:I = 0x8

.field public static final AS_REGISTRATION_VALUE:I = 0x1

.field public static final enum AS_UPGRADE:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity; = null

.field public static final AS_UPGRADE_VALUE:I = 0x40

.field public static final enum AS_VALIDATE_EMAIL:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity; = null

.field public static final enum AS_VALIDATE_EMAIL_NUMBER:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity; = null

.field public static final AS_VALIDATE_EMAIL_NUMBER_VALUE:I = 0x6

.field public static final AS_VALIDATE_EMAIL_VALUE:I = 0x4

.field public static final enum AS_VALIDATE_NUMBER:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity; = null

.field public static final AS_VALIDATE_NUMBER_VALUE:I = 0x2

.field public static final enum AS_VALIDATE_REGISTRATION_EMAIL:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity; = null

.field public static final AS_VALIDATE_REGISTRATION_EMAIL_VALUE:I = 0xa

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x6

    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 24206
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    const-string v1, "AS_OK"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_OK:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    .line 24207
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    const-string v1, "AS_REGISTRATION"

    invoke-direct {v0, v1, v6, v6, v6}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_REGISTRATION:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    .line 24208
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    const-string v1, "AS_VALIDATE_NUMBER"

    invoke-direct {v0, v1, v7, v7, v7}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_VALIDATE_NUMBER:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    .line 24209
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    const-string v1, "AS_VALIDATE_EMAIL"

    const/4 v2, 0x3

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3, v8}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_VALIDATE_EMAIL:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    .line 24210
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    const-string v1, "AS_VALIDATE_EMAIL_NUMBER"

    invoke-direct {v0, v1, v8, v8, v9}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_VALIDATE_EMAIL_NUMBER:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    .line 24211
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    const-string v1, "AS_REGISTRATION_EMAIL"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const/16 v4, 0x8

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_REGISTRATION_EMAIL:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    .line 24212
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    const-string v1, "AS_VALIDATE_REGISTRATION_EMAIL"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v9, v9, v2}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_VALIDATE_REGISTRATION_EMAIL:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    .line 24213
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    const-string v1, "AS_CALL_KEY_INVALID"

    const/4 v2, 0x7

    const/4 v3, 0x7

    const/16 v4, 0x10

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_CALL_KEY_INVALID:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    .line 24214
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    const-string v1, "AS_ITEM_MESSAGING"

    const/16 v2, 0x8

    const/16 v3, 0x8

    const/16 v4, 0x20

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_ITEM_MESSAGING:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    .line 24215
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    const-string v1, "AS_UPGRADE"

    const/16 v2, 0x9

    const/16 v3, 0x9

    const/16 v4, 0x40

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_UPGRADE:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    .line 24216
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    const-string v1, "AS_LOCAL_NOTIFICATION"

    const/16 v2, 0xa

    const/16 v3, 0xa

    const/16 v4, 0x80

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_LOCAL_NOTIFICATION:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    .line 24204
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_OK:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_REGISTRATION:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_VALIDATE_NUMBER:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    aput-object v1, v0, v7

    const/4 v1, 0x3

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_VALIDATE_EMAIL:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    aput-object v2, v0, v1

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_VALIDATE_EMAIL_NUMBER:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_REGISTRATION_EMAIL:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    aput-object v2, v0, v1

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_VALIDATE_REGISTRATION_EMAIL:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    aput-object v1, v0, v9

    const/4 v1, 0x7

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_CALL_KEY_INVALID:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_ITEM_MESSAGING:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_UPGRADE:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_LOCAL_NOTIFICATION:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    .line 24256
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 24265
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 24266
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->value:I

    .line 24267
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 24253
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;
    .locals 1
    .parameter

    .prologue
    .line 24235
    sparse-switch p0, :sswitch_data_0

    .line 24247
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 24236
    :sswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_OK:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    goto :goto_0

    .line 24237
    :sswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_REGISTRATION:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    goto :goto_0

    .line 24238
    :sswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_VALIDATE_NUMBER:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    goto :goto_0

    .line 24239
    :sswitch_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_VALIDATE_EMAIL:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    goto :goto_0

    .line 24240
    :sswitch_4
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_VALIDATE_EMAIL_NUMBER:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    goto :goto_0

    .line 24241
    :sswitch_5
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_REGISTRATION_EMAIL:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    goto :goto_0

    .line 24242
    :sswitch_6
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_VALIDATE_REGISTRATION_EMAIL:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    goto :goto_0

    .line 24243
    :sswitch_7
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_CALL_KEY_INVALID:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    goto :goto_0

    .line 24244
    :sswitch_8
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_ITEM_MESSAGING:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    goto :goto_0

    .line 24245
    :sswitch_9
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_UPGRADE:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    goto :goto_0

    .line 24246
    :sswitch_a
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_LOCAL_NOTIFICATION:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    goto :goto_0

    .line 24235
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x4 -> :sswitch_3
        0x6 -> :sswitch_4
        0x8 -> :sswitch_5
        0xa -> :sswitch_6
        0x10 -> :sswitch_7
        0x20 -> :sswitch_8
        0x40 -> :sswitch_9
        0x80 -> :sswitch_a
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;
    .locals 1
    .parameter

    .prologue
    .line 24204
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;
    .locals 1

    .prologue
    .line 24204
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 24232
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->value:I

    return v0
.end method
