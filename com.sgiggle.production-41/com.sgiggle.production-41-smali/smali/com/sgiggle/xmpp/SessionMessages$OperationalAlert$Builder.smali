.class public final Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$OperationalAlertOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
        "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlertOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private message_:Ljava/lang/Object;

.field private severity_:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

.field private title_:Ljava/lang/Object;

.field private type_:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 24499
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 24662
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->message_:Ljava/lang/Object;

    .line 24698
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_OK:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->severity_:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    .line 24722
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->title_:Ljava/lang/Object;

    .line 24500
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->maybeForceBuilderInitialization()V

    .line 24501
    return-void
.end method

.method static synthetic access$30900(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 24494
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$31000()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;
    .locals 1

    .prologue
    .line 24494
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 24540
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    .line 24541
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 24542
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 24545
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;
    .locals 1

    .prologue
    .line 24506
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 24504
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 24494
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 2

    .prologue
    .line 24531
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    .line 24532
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 24533
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 24535
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 24494
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 5

    .prologue
    .line 24549
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;-><init>(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 24550
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    .line 24551
    const/4 v2, 0x0

    .line 24552
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 24553
    or-int/lit8 v2, v2, 0x1

    .line 24555
    :cond_0
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->type_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->type_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->access$31202(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;I)I

    .line 24556
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 24557
    or-int/lit8 v2, v2, 0x2

    .line 24559
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->message_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->message_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->access$31302(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24560
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 24561
    or-int/lit8 v2, v2, 0x4

    .line 24563
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->severity_:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->severity_:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->access$31402(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    .line 24564
    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_3

    .line 24565
    or-int/lit8 v1, v2, 0x8

    .line 24567
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->title_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->title_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->access$31502(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24568
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->access$31602(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;I)I

    .line 24569
    return-object v0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 24494
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 24494
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;
    .locals 1

    .prologue
    .line 24510
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 24511
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->type_:I

    .line 24512
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    .line 24513
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->message_:Ljava/lang/Object;

    .line 24514
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    .line 24515
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_OK:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->severity_:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    .line 24516
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    .line 24517
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->title_:Ljava/lang/Object;

    .line 24518
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    .line 24519
    return-object p0
.end method

.method public clearMessage()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;
    .locals 1

    .prologue
    .line 24686
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    .line 24687
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->message_:Ljava/lang/Object;

    .line 24689
    return-object p0
.end method

.method public clearSeverity()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;
    .locals 1

    .prologue
    .line 24715
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    .line 24716
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_OK:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->severity_:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    .line 24718
    return-object p0
.end method

.method public clearTitle()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;
    .locals 1

    .prologue
    .line 24746
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    .line 24747
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getTitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->title_:Ljava/lang/Object;

    .line 24749
    return-object p0
.end method

.method public clearType()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;
    .locals 1

    .prologue
    .line 24655
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    .line 24656
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->type_:I

    .line 24658
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 24494
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 24494
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 24494
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;
    .locals 2

    .prologue
    .line 24523
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 24494
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 24494
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 24494
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 1

    .prologue
    .line 24527
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 24667
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->message_:Ljava/lang/Object;

    .line 24668
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 24669
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 24670
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->message_:Ljava/lang/Object;

    .line 24673
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getSeverity()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;
    .locals 1

    .prologue
    .line 24703
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->severity_:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 24727
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->title_:Ljava/lang/Object;

    .line 24728
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 24729
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 24730
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->title_:Ljava/lang/Object;

    .line 24733
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 24646
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->type_:I

    return v0
.end method

.method public hasMessage()Z
    .locals 2

    .prologue
    .line 24664
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSeverity()Z
    .locals 2

    .prologue
    .line 24700
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTitle()Z
    .locals 2

    .prologue
    .line 24724
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 24643
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 24590
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24494
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 24494
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24494
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24598
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 24599
    sparse-switch v0, :sswitch_data_0

    .line 24604
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 24606
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 24602
    goto :goto_1

    .line 24611
    :sswitch_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    .line 24612
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->type_:I

    goto :goto_0

    .line 24616
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    .line 24617
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->message_:Ljava/lang/Object;

    goto :goto_0

    .line 24621
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 24622
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    move-result-object v0

    .line 24623
    if-eqz v0, :cond_0

    .line 24624
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    .line 24625
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->severity_:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    goto :goto_0

    .line 24630
    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    .line 24631
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->title_:Ljava/lang/Object;

    goto :goto_0

    .line 24599
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;
    .locals 1
    .parameter

    .prologue
    .line 24573
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 24586
    :goto_0
    return-object v0

    .line 24574
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->hasType()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 24575
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getType()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->setType(I)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    .line 24577
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->hasMessage()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 24578
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->setMessage(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    .line 24580
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->hasSeverity()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 24581
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getSeverity()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->setSeverity(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    .line 24583
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->hasTitle()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 24584
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->setTitle(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    :cond_4
    move-object v0, p0

    .line 24586
    goto :goto_0
.end method

.method public setMessage(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;
    .locals 1
    .parameter

    .prologue
    .line 24677
    if-nez p1, :cond_0

    .line 24678
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 24680
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    .line 24681
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->message_:Ljava/lang/Object;

    .line 24683
    return-object p0
.end method

.method setMessage(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 24692
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    .line 24693
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->message_:Ljava/lang/Object;

    .line 24695
    return-void
.end method

.method public setSeverity(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;
    .locals 1
    .parameter

    .prologue
    .line 24706
    if-nez p1, :cond_0

    .line 24707
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 24709
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    .line 24710
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->severity_:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    .line 24712
    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;
    .locals 1
    .parameter

    .prologue
    .line 24737
    if-nez p1, :cond_0

    .line 24738
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 24740
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    .line 24741
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->title_:Ljava/lang/Object;

    .line 24743
    return-object p0
.end method

.method setTitle(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 24752
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    .line 24753
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->title_:Ljava/lang/Object;

    .line 24755
    return-void
.end method

.method public setType(I)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;
    .locals 1
    .parameter

    .prologue
    .line 24649
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->bitField0_:I

    .line 24650
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->type_:I

    .line 24652
    return-object p0
.end method
