.class public final Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$OperationalAlertOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OperationalAlert"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;,
        Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;
    }
.end annotation


# static fields
.field public static final MESSAGE_FIELD_NUMBER:I = 0x2

.field public static final SEVERITY_FIELD_NUMBER:I = 0x3

.field public static final TITLE_FIELD_NUMBER:I = 0x4

.field public static final TYPE_FIELD_NUMBER:I = 0x1

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;


# instance fields
.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private message_:Ljava/lang/Object;

.field private severity_:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

.field private title_:Ljava/lang/Object;

.field private type_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 24761
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    .line 24762
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->initFields()V

    .line 24763
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 24191
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 24363
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->memoizedIsInitialized:B

    .line 24389
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->memoizedSerializedSize:I

    .line 24192
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24186
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;-><init>(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 24193
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 24363
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->memoizedIsInitialized:B

    .line 24389
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->memoizedSerializedSize:I

    .line 24193
    return-void
.end method

.method static synthetic access$31202(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24186
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->type_:I

    return p1
.end method

.method static synthetic access$31302(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24186
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->message_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$31402(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24186
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->severity_:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    return-object p1
.end method

.method static synthetic access$31502(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24186
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->title_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$31602(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24186
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 1

    .prologue
    .line 24197
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    return-object v0
.end method

.method private getMessageBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 24304
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->message_:Ljava/lang/Object;

    .line 24305
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 24306
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 24308
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->message_:Ljava/lang/Object;

    .line 24311
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getTitleBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 24346
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->title_:Ljava/lang/Object;

    .line 24347
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 24348
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 24350
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->title_:Ljava/lang/Object;

    .line 24353
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 24358
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->type_:I

    .line 24359
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->message_:Ljava/lang/Object;

    .line 24360
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_OK:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->severity_:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    .line 24361
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->title_:Ljava/lang/Object;

    .line 24362
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;
    .locals 1

    .prologue
    .line 24487
    #calls: Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->access$31000()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;
    .locals 1
    .parameter

    .prologue
    .line 24490
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24456
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    .line 24457
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 24458
    #calls: Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->access$30900(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    .line 24460
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24467
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    .line 24468
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 24469
    #calls: Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->access$30900(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    .line 24471
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 24423
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->access$30900(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 24429
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->access$30900(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24477
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->access$30900(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24483
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->access$30900(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24445
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->access$30900(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24451
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->access$30900(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 24434
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->access$30900(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 24440
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->access$30900(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 24186
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 1

    .prologue
    .line 24201
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 24290
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->message_:Ljava/lang/Object;

    .line 24291
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 24292
    check-cast v0, Ljava/lang/String;

    .line 24300
    :goto_0
    return-object v0

    .line 24294
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 24296
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 24297
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 24298
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->message_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 24300
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 24391
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->memoizedSerializedSize:I

    .line 24392
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 24412
    :goto_0
    return v0

    .line 24394
    :cond_0
    const/4 v0, 0x0

    .line 24395
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 24396
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->type_:I

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 24399
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 24400
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getMessageBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24403
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 24404
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->severity_:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 24407
    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 24408
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getTitleBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24411
    :cond_4
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getSeverity()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;
    .locals 1

    .prologue
    .line 24322
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->severity_:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 24332
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->title_:Ljava/lang/Object;

    .line 24333
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 24334
    check-cast v0, Ljava/lang/String;

    .line 24342
    :goto_0
    return-object v0

    .line 24336
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 24338
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 24339
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 24340
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->title_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 24342
    goto :goto_0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 24280
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->type_:I

    return v0
.end method

.method public hasMessage()Z
    .locals 2

    .prologue
    .line 24287
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSeverity()Z
    .locals 2

    .prologue
    .line 24319
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTitle()Z
    .locals 2

    .prologue
    .line 24329
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 24277
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 24365
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->memoizedIsInitialized:B

    .line 24366
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v2, :cond_0

    move v0, v2

    .line 24369
    :goto_0
    return v0

    .line 24366
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 24368
    :cond_1
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->memoizedIsInitialized:B

    move v0, v2

    .line 24369
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 24186
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;
    .locals 1

    .prologue
    .line 24488
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 24186
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;
    .locals 1

    .prologue
    .line 24492
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 24417
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 24374
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getSerializedSize()I

    .line 24375
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 24376
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->type_:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 24378
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 24379
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getMessageBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 24381
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 24382
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->severity_:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 24384
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 24385
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getTitleBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 24387
    :cond_3
    return-void
.end method
