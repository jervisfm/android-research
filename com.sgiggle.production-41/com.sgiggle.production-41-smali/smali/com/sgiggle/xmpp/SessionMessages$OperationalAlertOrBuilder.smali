.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$OperationalAlertOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OperationalAlertOrBuilder"
.end annotation


# virtual methods
.method public abstract getMessage()Ljava/lang/String;
.end method

.method public abstract getSeverity()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;
.end method

.method public abstract getTitle()Ljava/lang/String;
.end method

.method public abstract getType()I
.end method

.method public abstract hasMessage()Z
.end method

.method public abstract hasSeverity()Z
.end method

.method public abstract hasTitle()Z
.end method

.method public abstract hasType()Z
.end method
