.class public final Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OtherRegisteredDevicePayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final REGISTERED_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private registered_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 51823
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;

    .line 51824
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->initFields()V

    .line 51825
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 51458
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 51496
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->memoizedIsInitialized:B

    .line 51528
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->memoizedSerializedSize:I

    .line 51459
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 51453
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 51460
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 51496
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->memoizedIsInitialized:B

    .line 51528
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->memoizedSerializedSize:I

    .line 51460
    return-void
.end method

.method static synthetic access$66302(Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 51453
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$66402(Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 51453
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->registered_:Z

    return p1
.end method

.method static synthetic access$66502(Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 51453
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;
    .locals 1

    .prologue
    .line 51464
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 51493
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 51494
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->registered_:Z

    .line 51495
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;
    .locals 1

    .prologue
    .line 51618
    #calls: Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->access$66100()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;)Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 51621
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;)Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51587
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;

    move-result-object v0

    .line 51588
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 51589
    #calls: Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->access$66000(Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;

    move-result-object v0

    .line 51591
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51598
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;

    move-result-object v0

    .line 51599
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 51600
    #calls: Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->access$66000(Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;

    move-result-object v0

    .line 51602
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 51554
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->access$66000(Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 51560
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->access$66000(Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51608
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->access$66000(Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51614
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->access$66000(Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51576
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->access$66000(Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51582
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->access$66000(Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 51565
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->access$66000(Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 51571
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;->access$66000(Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 51479
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 51453
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;
    .locals 1

    .prologue
    .line 51468
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;

    return-object v0
.end method

.method public getRegistered()Z
    .locals 1

    .prologue
    .line 51489
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->registered_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 51530
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->memoizedSerializedSize:I

    .line 51531
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 51543
    :goto_0
    return v0

    .line 51533
    :cond_0
    const/4 v0, 0x0

    .line 51534
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 51535
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51538
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 51539
    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->registered_:Z

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 51542
    :cond_2
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 51476
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRegistered()Z
    .locals 2

    .prologue
    .line 51486
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 51498
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->memoizedIsInitialized:B

    .line 51499
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 51514
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 51499
    goto :goto_0

    .line 51501
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 51502
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 51503
    goto :goto_0

    .line 51505
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->hasRegistered()Z

    move-result v0

    if-nez v0, :cond_3

    .line 51506
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 51507
    goto :goto_0

    .line 51509
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    .line 51510
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 51511
    goto :goto_0

    .line 51513
    :cond_4
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->memoizedIsInitialized:B

    move v0, v3

    .line 51514
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 51453
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;
    .locals 1

    .prologue
    .line 51619
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 51453
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;
    .locals 1

    .prologue
    .line 51623
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;)Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 51548
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 51519
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->getSerializedSize()I

    .line 51520
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 51521
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 51523
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 51524
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$OtherRegisteredDevicePayload;->registered_:Z

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 51526
    :cond_1
    return-void
.end method
