.class public final Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private candidates_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 20512
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 20634
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 20677
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->candidates_:Ljava/lang/Object;

    .line 20513
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->maybeForceBuilderInitialization()V

    .line 20514
    return-void
.end method

.method static synthetic access$25700(Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 20507
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$25800()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;
    .locals 1

    .prologue
    .line 20507
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 20549
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;

    move-result-object v0

    .line 20550
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 20551
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 20554
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;
    .locals 1

    .prologue
    .line 20519
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 20517
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 20507
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;
    .locals 2

    .prologue
    .line 20540
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;

    move-result-object v0

    .line 20541
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 20542
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 20544
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 20507
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;
    .locals 5

    .prologue
    .line 20558
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 20559
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->bitField0_:I

    .line 20560
    const/4 v2, 0x0

    .line 20561
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 20562
    or-int/lit8 v2, v2, 0x1

    .line 20564
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->access$26002(Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 20565
    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    .line 20566
    or-int/lit8 v1, v2, 0x2

    .line 20568
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->candidates_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->candidates_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->access$26102(Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20569
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->access$26202(Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;I)I

    .line 20570
    return-object v0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 20507
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 20507
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;
    .locals 1

    .prologue
    .line 20523
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 20524
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 20525
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->bitField0_:I

    .line 20526
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->candidates_:Ljava/lang/Object;

    .line 20527
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->bitField0_:I

    .line 20528
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;
    .locals 1

    .prologue
    .line 20670
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 20672
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->bitField0_:I

    .line 20673
    return-object p0
.end method

.method public clearCandidates()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;
    .locals 1

    .prologue
    .line 20701
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->bitField0_:I

    .line 20702
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->getCandidates()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->candidates_:Ljava/lang/Object;

    .line 20704
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 20507
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 20507
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 20507
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;
    .locals 2

    .prologue
    .line 20532
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 20507
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 20639
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCandidates()Ljava/lang/String;
    .locals 2

    .prologue
    .line 20682
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->candidates_:Ljava/lang/Object;

    .line 20683
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 20684
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 20685
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->candidates_:Ljava/lang/Object;

    .line 20688
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 20507
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 20507
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;
    .locals 1

    .prologue
    .line 20536
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;

    move-result-object v0

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 20636
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCandidates()Z
    .locals 2

    .prologue
    .line 20679
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 20585
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 20593
    :goto_0
    return v0

    .line 20589
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 20591
    goto :goto_0

    .line 20593
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 20658
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 20660
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 20666
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->bitField0_:I

    .line 20667
    return-object p0

    .line 20663
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20507
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 20507
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20507
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20601
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 20602
    sparse-switch v0, :sswitch_data_0

    .line 20607
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 20609
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 20605
    goto :goto_1

    .line 20614
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 20615
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 20616
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 20618
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 20619
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    goto :goto_0

    .line 20623
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->bitField0_:I

    .line 20624
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->candidates_:Ljava/lang/Object;

    goto :goto_0

    .line 20602
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 20574
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 20581
    :goto_0
    return-object v0

    .line 20575
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 20576
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    .line 20578
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->hasCandidates()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 20579
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->getCandidates()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->setCandidates(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    :cond_2
    move-object v0, p0

    .line 20581
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 20652
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 20654
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->bitField0_:I

    .line 20655
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 20642
    if-nez p1, :cond_0

    .line 20643
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 20645
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 20647
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->bitField0_:I

    .line 20648
    return-object p0
.end method

.method public setCandidates(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 20692
    if-nez p1, :cond_0

    .line 20693
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 20695
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->bitField0_:I

    .line 20696
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->candidates_:Ljava/lang/Object;

    .line 20698
    return-object p0
.end method

.method setCandidates(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 20707
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->bitField0_:I

    .line 20708
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->candidates_:Ljava/lang/Object;

    .line 20710
    return-void
.end method
