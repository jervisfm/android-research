.class public final Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "P2PCandidatesPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CANDIDATES_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private candidates_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20716
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;

    .line 20717
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->initFields()V

    .line 20718
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 20322
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 20382
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->memoizedIsInitialized:B

    .line 20410
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->memoizedSerializedSize:I

    .line 20323
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 20317
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 20324
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 20382
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->memoizedIsInitialized:B

    .line 20410
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->memoizedSerializedSize:I

    .line 20324
    return-void
.end method

.method static synthetic access$26002(Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 20317
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$26102(Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 20317
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->candidates_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$26202(Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 20317
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->bitField0_:I

    return p1
.end method

.method private getCandidatesBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 20367
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->candidates_:Ljava/lang/Object;

    .line 20368
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 20369
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 20371
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->candidates_:Ljava/lang/Object;

    .line 20374
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;
    .locals 1

    .prologue
    .line 20328
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 20379
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 20380
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->candidates_:Ljava/lang/Object;

    .line 20381
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;
    .locals 1

    .prologue
    .line 20500
    #calls: Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->access$25800()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 20503
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20469
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    move-result-object v0

    .line 20470
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 20471
    #calls: Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->access$25700(Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;

    move-result-object v0

    .line 20473
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20480
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    move-result-object v0

    .line 20481
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 20482
    #calls: Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->access$25700(Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;

    move-result-object v0

    .line 20484
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 20436
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->access$25700(Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 20442
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->access$25700(Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20490
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->access$25700(Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20496
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->access$25700(Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20458
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->access$25700(Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20464
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->access$25700(Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 20447
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->access$25700(Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 20453
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;->access$25700(Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 20343
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCandidates()Ljava/lang/String;
    .locals 2

    .prologue
    .line 20353
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->candidates_:Ljava/lang/Object;

    .line 20354
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 20355
    check-cast v0, Ljava/lang/String;

    .line 20363
    :goto_0
    return-object v0

    .line 20357
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 20359
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 20360
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 20361
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->candidates_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 20363
    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 20317
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;
    .locals 1

    .prologue
    .line 20332
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 20412
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->memoizedSerializedSize:I

    .line 20413
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 20425
    :goto_0
    return v0

    .line 20415
    :cond_0
    const/4 v0, 0x0

    .line 20416
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 20417
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20420
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 20421
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->getCandidatesBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20424
    :cond_2
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 20340
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCandidates()Z
    .locals 2

    .prologue
    .line 20350
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 20384
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->memoizedIsInitialized:B

    .line 20385
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 20396
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 20385
    goto :goto_0

    .line 20387
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 20388
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 20389
    goto :goto_0

    .line 20391
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 20392
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 20393
    goto :goto_0

    .line 20395
    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 20396
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 20317
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;
    .locals 1

    .prologue
    .line 20501
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 20317
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;
    .locals 1

    .prologue
    .line 20505
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;)Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 20430
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 20401
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->getSerializedSize()I

    .line 20402
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 20403
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 20405
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 20406
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$P2PCandidatesPayload;->getCandidatesBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 20408
    :cond_1
    return-void
.end method
