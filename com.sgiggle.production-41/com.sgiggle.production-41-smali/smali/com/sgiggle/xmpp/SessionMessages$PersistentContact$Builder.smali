.class public final Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$PersistentContactOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;",
        "Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$PersistentContactOrBuilder;"
    }
.end annotation


# instance fields
.field private accountid_:Ljava/lang/Object;

.field private bitField0_:I

.field private displayname_:Ljava/lang/Object;

.field private email_:Ljava/lang/Object;

.field private firstname_:Ljava/lang/Object;

.field private hasPicture_:Z

.field private hash_:Ljava/lang/Object;

.field private lastname_:Ljava/lang/Object;

.field private middlename_:Ljava/lang/Object;

.field private nameprefix_:Ljava/lang/Object;

.field private namesuffix_:Ljava/lang/Object;

.field private nativeFavorite_:Z

.field private phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

.field private sha1Hash_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 31388
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 31678
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->firstname_:Ljava/lang/Object;

    .line 31714
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->lastname_:Ljava/lang/Object;

    .line 31750
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->hash_:Ljava/lang/Object;

    .line 31786
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->accountid_:Ljava/lang/Object;

    .line 31822
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->sha1Hash_:Ljava/lang/Object;

    .line 31858
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    .line 31901
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->email_:Ljava/lang/Object;

    .line 31958
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->nameprefix_:Ljava/lang/Object;

    .line 31994
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->middlename_:Ljava/lang/Object;

    .line 32030
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->namesuffix_:Ljava/lang/Object;

    .line 32066
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->displayname_:Ljava/lang/Object;

    .line 31389
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->maybeForceBuilderInitialization()V

    .line 31390
    return-void
.end method

.method static synthetic access$39200(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 31383
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$39300()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1

    .prologue
    .line 31383
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 31447
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    .line 31448
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 31449
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 31452
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1

    .prologue
    .line 31395
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 31393
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 31383
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
    .locals 2

    .prologue
    .line 31438
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    .line 31439
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 31440
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 31442
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 31383
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
    .locals 5

    .prologue
    .line 31456
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;-><init>(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 31457
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31458
    const/4 v2, 0x0

    .line 31459
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 31460
    or-int/lit8 v2, v2, 0x1

    .line 31462
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->firstname_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->firstname_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->access$39502(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31463
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 31464
    or-int/lit8 v2, v2, 0x2

    .line 31466
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->lastname_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->lastname_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->access$39602(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31467
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 31468
    or-int/lit8 v2, v2, 0x4

    .line 31470
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->hash_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hash_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->access$39702(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31471
    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 31472
    or-int/lit8 v2, v2, 0x8

    .line 31474
    :cond_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->accountid_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->accountid_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->access$39802(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31475
    and-int/lit8 v3, v1, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 31476
    or-int/lit8 v2, v2, 0x10

    .line 31478
    :cond_4
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->sha1Hash_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->sha1Hash_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->access$39902(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31479
    and-int/lit8 v3, v1, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 31480
    or-int/lit8 v2, v2, 0x20

    .line 31482
    :cond_5
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->access$40002(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    .line 31483
    and-int/lit8 v3, v1, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    .line 31484
    or-int/lit8 v2, v2, 0x40

    .line 31486
    :cond_6
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->email_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->email_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->access$40102(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31487
    and-int/lit16 v3, v1, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    .line 31488
    or-int/lit16 v2, v2, 0x80

    .line 31490
    :cond_7
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->nativeFavorite_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->nativeFavorite_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->access$40202(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;Z)Z

    .line 31491
    and-int/lit16 v3, v1, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_8

    .line 31492
    or-int/lit16 v2, v2, 0x100

    .line 31494
    :cond_8
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->nameprefix_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->nameprefix_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->access$40302(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31495
    and-int/lit16 v3, v1, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_9

    .line 31496
    or-int/lit16 v2, v2, 0x200

    .line 31498
    :cond_9
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->middlename_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->middlename_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->access$40402(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31499
    and-int/lit16 v3, v1, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_a

    .line 31500
    or-int/lit16 v2, v2, 0x400

    .line 31502
    :cond_a
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->namesuffix_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->namesuffix_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->access$40502(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31503
    and-int/lit16 v3, v1, 0x800

    const/16 v4, 0x800

    if-ne v3, v4, :cond_b

    .line 31504
    or-int/lit16 v2, v2, 0x800

    .line 31506
    :cond_b
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->displayname_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->displayname_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->access$40602(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31507
    and-int/lit16 v1, v1, 0x1000

    const/16 v3, 0x1000

    if-ne v1, v3, :cond_c

    .line 31508
    or-int/lit16 v1, v2, 0x1000

    .line 31510
    :goto_0
    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->hasPicture_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hasPicture_:Z
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->access$40702(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;Z)Z

    .line 31511
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->access$40802(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;I)I

    .line 31512
    return-object v0

    :cond_c
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 31383
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 31383
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 31399
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 31400
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->firstname_:Ljava/lang/Object;

    .line 31401
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31402
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->lastname_:Ljava/lang/Object;

    .line 31403
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31404
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->hash_:Ljava/lang/Object;

    .line 31405
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31406
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->accountid_:Ljava/lang/Object;

    .line 31407
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31408
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->sha1Hash_:Ljava/lang/Object;

    .line 31409
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31410
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    .line 31411
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31412
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->email_:Ljava/lang/Object;

    .line 31413
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31414
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->nativeFavorite_:Z

    .line 31415
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31416
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->nameprefix_:Ljava/lang/Object;

    .line 31417
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31418
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->middlename_:Ljava/lang/Object;

    .line 31419
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31420
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->namesuffix_:Ljava/lang/Object;

    .line 31421
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31422
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->displayname_:Ljava/lang/Object;

    .line 31423
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31424
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->hasPicture_:Z

    .line 31425
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31426
    return-object p0
.end method

.method public clearAccountid()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1

    .prologue
    .line 31810
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31811
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getAccountid()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->accountid_:Ljava/lang/Object;

    .line 31813
    return-object p0
.end method

.method public clearDisplayname()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1

    .prologue
    .line 32090
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 32091
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getDisplayname()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->displayname_:Ljava/lang/Object;

    .line 32093
    return-object p0
.end method

.method public clearEmail()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1

    .prologue
    .line 31925
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31926
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getEmail()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->email_:Ljava/lang/Object;

    .line 31928
    return-object p0
.end method

.method public clearFirstname()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1

    .prologue
    .line 31702
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31703
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getFirstname()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->firstname_:Ljava/lang/Object;

    .line 31705
    return-object p0
.end method

.method public clearHasPicture()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1

    .prologue
    .line 32116
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 32117
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->hasPicture_:Z

    .line 32119
    return-object p0
.end method

.method public clearHash()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1

    .prologue
    .line 31774
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31775
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getHash()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->hash_:Ljava/lang/Object;

    .line 31777
    return-object p0
.end method

.method public clearLastname()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1

    .prologue
    .line 31738
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31739
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getLastname()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->lastname_:Ljava/lang/Object;

    .line 31741
    return-object p0
.end method

.method public clearMiddlename()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1

    .prologue
    .line 32018
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 32019
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getMiddlename()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->middlename_:Ljava/lang/Object;

    .line 32021
    return-object p0
.end method

.method public clearNameprefix()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1

    .prologue
    .line 31982
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31983
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getNameprefix()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->nameprefix_:Ljava/lang/Object;

    .line 31985
    return-object p0
.end method

.method public clearNamesuffix()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1

    .prologue
    .line 32054
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 32055
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getNamesuffix()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->namesuffix_:Ljava/lang/Object;

    .line 32057
    return-object p0
.end method

.method public clearNativeFavorite()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1

    .prologue
    .line 31951
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31952
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->nativeFavorite_:Z

    .line 31954
    return-object p0
.end method

.method public clearPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1

    .prologue
    .line 31894
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    .line 31896
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31897
    return-object p0
.end method

.method public clearSha1Hash()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1

    .prologue
    .line 31846
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31847
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getSha1Hash()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->sha1Hash_:Ljava/lang/Object;

    .line 31849
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 31383
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 31383
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 31383
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 2

    .prologue
    .line 31430
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 31383
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAccountid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 31791
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->accountid_:Ljava/lang/Object;

    .line 31792
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 31793
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 31794
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->accountid_:Ljava/lang/Object;

    .line 31797
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 31383
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 31383
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
    .locals 1

    .prologue
    .line 31434
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 32071
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->displayname_:Ljava/lang/Object;

    .line 32072
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 32073
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 32074
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->displayname_:Ljava/lang/Object;

    .line 32077
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 2

    .prologue
    .line 31906
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->email_:Ljava/lang/Object;

    .line 31907
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 31908
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 31909
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->email_:Ljava/lang/Object;

    .line 31912
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getFirstname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 31683
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->firstname_:Ljava/lang/Object;

    .line 31684
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 31685
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 31686
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->firstname_:Ljava/lang/Object;

    .line 31689
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getHasPicture()Z
    .locals 1

    .prologue
    .line 32107
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->hasPicture_:Z

    return v0
.end method

.method public getHash()Ljava/lang/String;
    .locals 2

    .prologue
    .line 31755
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->hash_:Ljava/lang/Object;

    .line 31756
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 31757
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 31758
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->hash_:Ljava/lang/Object;

    .line 31761
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getLastname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 31719
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->lastname_:Ljava/lang/Object;

    .line 31720
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 31721
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 31722
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->lastname_:Ljava/lang/Object;

    .line 31725
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getMiddlename()Ljava/lang/String;
    .locals 2

    .prologue
    .line 31999
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->middlename_:Ljava/lang/Object;

    .line 32000
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 32001
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 32002
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->middlename_:Ljava/lang/Object;

    .line 32005
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getNameprefix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 31963
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->nameprefix_:Ljava/lang/Object;

    .line 31964
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 31965
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 31966
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->nameprefix_:Ljava/lang/Object;

    .line 31969
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getNamesuffix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 32035
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->namesuffix_:Ljava/lang/Object;

    .line 32036
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 32037
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 32038
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->namesuffix_:Ljava/lang/Object;

    .line 32041
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getNativeFavorite()Z
    .locals 1

    .prologue
    .line 31942
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->nativeFavorite_:Z

    return v0
.end method

.method public getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    .locals 1

    .prologue
    .line 31863
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    return-object v0
.end method

.method public getSha1Hash()Ljava/lang/String;
    .locals 2

    .prologue
    .line 31827
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->sha1Hash_:Ljava/lang/Object;

    .line 31828
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 31829
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 31830
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->sha1Hash_:Ljava/lang/Object;

    .line 31833
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasAccountid()Z
    .locals 2

    .prologue
    .line 31788
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDisplayname()Z
    .locals 2

    .prologue
    .line 32068
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasEmail()Z
    .locals 2

    .prologue
    .line 31903
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFirstname()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 31680
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHasPicture()Z
    .locals 2

    .prologue
    .line 32104
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHash()Z
    .locals 2

    .prologue
    .line 31752
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLastname()Z
    .locals 2

    .prologue
    .line 31716
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMiddlename()Z
    .locals 2

    .prologue
    .line 31996
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNameprefix()Z
    .locals 2

    .prologue
    .line 31960
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNamesuffix()Z
    .locals 2

    .prologue
    .line 32032
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNativeFavorite()Z
    .locals 2

    .prologue
    .line 31939
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPhoneNumber()Z
    .locals 2

    .prologue
    .line 31860
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSha1Hash()Z
    .locals 2

    .prologue
    .line 31824
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 31560
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->hasFirstname()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 31582
    :goto_0
    return v0

    .line 31564
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->hasLastname()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 31566
    goto :goto_0

    .line 31568
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->hasHash()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 31570
    goto :goto_0

    .line 31572
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->hasAccountid()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 31574
    goto :goto_0

    .line 31576
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->hasPhoneNumber()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 31577
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 31579
    goto :goto_0

    .line 31582
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31383
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 31383
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31383
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31590
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 31591
    sparse-switch v0, :sswitch_data_0

    .line 31596
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 31598
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 31594
    goto :goto_1

    .line 31603
    :sswitch_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31604
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->firstname_:Ljava/lang/Object;

    goto :goto_0

    .line 31608
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31609
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->lastname_:Ljava/lang/Object;

    goto :goto_0

    .line 31613
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31614
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->hash_:Ljava/lang/Object;

    goto :goto_0

    .line 31618
    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31619
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->accountid_:Ljava/lang/Object;

    goto :goto_0

    .line 31623
    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31624
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->sha1Hash_:Ljava/lang/Object;

    goto :goto_0

    .line 31628
    :sswitch_6
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    .line 31629
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->hasPhoneNumber()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 31630
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    .line 31632
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 31633
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->setPhoneNumber(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    goto :goto_0

    .line 31637
    :sswitch_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31638
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->email_:Ljava/lang/Object;

    goto :goto_0

    .line 31642
    :sswitch_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31643
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->nativeFavorite_:Z

    goto/16 :goto_0

    .line 31647
    :sswitch_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31648
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->nameprefix_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 31652
    :sswitch_a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31653
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->middlename_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 31657
    :sswitch_b
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31658
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->namesuffix_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 31662
    :sswitch_c
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31663
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->displayname_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 31667
    :sswitch_d
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31668
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->hasPicture_:Z

    goto/16 :goto_0

    .line 31591
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 31516
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 31556
    :goto_0
    return-object v0

    .line 31517
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hasFirstname()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 31518
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getFirstname()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->setFirstname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    .line 31520
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hasLastname()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 31521
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getLastname()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->setLastname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    .line 31523
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hasHash()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 31524
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getHash()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->setHash(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    .line 31526
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hasAccountid()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 31527
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getAccountid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->setAccountid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    .line 31529
    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hasSha1Hash()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 31530
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getSha1Hash()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->setSha1Hash(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    .line 31532
    :cond_5
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hasPhoneNumber()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 31533
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->mergePhoneNumber(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    .line 31535
    :cond_6
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hasEmail()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 31536
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getEmail()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->setEmail(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    .line 31538
    :cond_7
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hasNativeFavorite()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 31539
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getNativeFavorite()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->setNativeFavorite(Z)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    .line 31541
    :cond_8
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hasNameprefix()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 31542
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getNameprefix()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->setNameprefix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    .line 31544
    :cond_9
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hasMiddlename()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 31545
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getMiddlename()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->setMiddlename(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    .line 31547
    :cond_a
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hasNamesuffix()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 31548
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getNamesuffix()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->setNamesuffix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    .line 31550
    :cond_b
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hasDisplayname()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 31551
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getDisplayname()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->setDisplayname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    .line 31553
    :cond_c
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hasHasPicture()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 31554
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getHasPicture()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->setHasPicture(Z)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    :cond_d
    move-object v0, p0

    .line 31556
    goto/16 :goto_0
.end method

.method public mergePhoneNumber(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 2
    .parameter

    .prologue
    .line 31882
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 31884
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    .line 31890
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31891
    return-object p0

    .line 31887
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    goto :goto_0
.end method

.method public setAccountid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 31801
    if-nez p1, :cond_0

    .line 31802
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 31804
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31805
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->accountid_:Ljava/lang/Object;

    .line 31807
    return-object p0
.end method

.method setAccountid(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 31816
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31817
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->accountid_:Ljava/lang/Object;

    .line 31819
    return-void
.end method

.method public setDisplayname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 32081
    if-nez p1, :cond_0

    .line 32082
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 32084
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 32085
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->displayname_:Ljava/lang/Object;

    .line 32087
    return-object p0
.end method

.method setDisplayname(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 32096
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 32097
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->displayname_:Ljava/lang/Object;

    .line 32099
    return-void
.end method

.method public setEmail(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 31916
    if-nez p1, :cond_0

    .line 31917
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 31919
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31920
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->email_:Ljava/lang/Object;

    .line 31922
    return-object p0
.end method

.method setEmail(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 31931
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31932
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->email_:Ljava/lang/Object;

    .line 31934
    return-void
.end method

.method public setFirstname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 31693
    if-nez p1, :cond_0

    .line 31694
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 31696
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31697
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->firstname_:Ljava/lang/Object;

    .line 31699
    return-object p0
.end method

.method setFirstname(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 31708
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31709
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->firstname_:Ljava/lang/Object;

    .line 31711
    return-void
.end method

.method public setHasPicture(Z)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 32110
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 32111
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->hasPicture_:Z

    .line 32113
    return-object p0
.end method

.method public setHash(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 31765
    if-nez p1, :cond_0

    .line 31766
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 31768
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31769
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->hash_:Ljava/lang/Object;

    .line 31771
    return-object p0
.end method

.method setHash(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 31780
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31781
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->hash_:Ljava/lang/Object;

    .line 31783
    return-void
.end method

.method public setLastname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 31729
    if-nez p1, :cond_0

    .line 31730
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 31732
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31733
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->lastname_:Ljava/lang/Object;

    .line 31735
    return-object p0
.end method

.method setLastname(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 31744
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31745
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->lastname_:Ljava/lang/Object;

    .line 31747
    return-void
.end method

.method public setMiddlename(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 32009
    if-nez p1, :cond_0

    .line 32010
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 32012
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 32013
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->middlename_:Ljava/lang/Object;

    .line 32015
    return-object p0
.end method

.method setMiddlename(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 32024
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 32025
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->middlename_:Ljava/lang/Object;

    .line 32027
    return-void
.end method

.method public setNameprefix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 31973
    if-nez p1, :cond_0

    .line 31974
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 31976
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31977
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->nameprefix_:Ljava/lang/Object;

    .line 31979
    return-object p0
.end method

.method setNameprefix(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 31988
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31989
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->nameprefix_:Ljava/lang/Object;

    .line 31991
    return-void
.end method

.method public setNamesuffix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 32045
    if-nez p1, :cond_0

    .line 32046
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 32048
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 32049
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->namesuffix_:Ljava/lang/Object;

    .line 32051
    return-object p0
.end method

.method setNamesuffix(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 32060
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 32061
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->namesuffix_:Ljava/lang/Object;

    .line 32063
    return-void
.end method

.method public setNativeFavorite(Z)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 31945
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31946
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->nativeFavorite_:Z

    .line 31948
    return-object p0
.end method

.method public setPhoneNumber(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 31876
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    .line 31878
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31879
    return-object p0
.end method

.method public setPhoneNumber(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 31866
    if-nez p1, :cond_0

    .line 31867
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 31869
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    .line 31871
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31872
    return-object p0
.end method

.method public setSha1Hash(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 31837
    if-nez p1, :cond_0

    .line 31838
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 31840
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31841
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->sha1Hash_:Ljava/lang/Object;

    .line 31843
    return-object p0
.end method

.method setSha1Hash(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 31852
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->bitField0_:I

    .line 31853
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->sha1Hash_:Ljava/lang/Object;

    .line 31855
    return-void
.end method
