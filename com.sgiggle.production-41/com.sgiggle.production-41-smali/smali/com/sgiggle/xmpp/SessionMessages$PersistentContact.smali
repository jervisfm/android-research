.class public final Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$PersistentContactOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PersistentContact"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    }
.end annotation


# static fields
.field public static final ACCOUNTID_FIELD_NUMBER:I = 0x4

.field public static final DISPLAYNAME_FIELD_NUMBER:I = 0xc

.field public static final EMAIL_FIELD_NUMBER:I = 0x7

.field public static final FIRSTNAME_FIELD_NUMBER:I = 0x1

.field public static final HASH_FIELD_NUMBER:I = 0x3

.field public static final HASPICTURE_FIELD_NUMBER:I = 0xd

.field public static final LASTNAME_FIELD_NUMBER:I = 0x2

.field public static final MIDDLENAME_FIELD_NUMBER:I = 0xa

.field public static final NAMEPREFIX_FIELD_NUMBER:I = 0x9

.field public static final NAMESUFFIX_FIELD_NUMBER:I = 0xb

.field public static final NATIVEFAVORITE_FIELD_NUMBER:I = 0x8

.field public static final PHONENUMBER_FIELD_NUMBER:I = 0x6

.field public static final SHA1HASH_FIELD_NUMBER:I = 0x5

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;


# instance fields
.field private accountid_:Ljava/lang/Object;

.field private bitField0_:I

.field private displayname_:Ljava/lang/Object;

.field private email_:Ljava/lang/Object;

.field private firstname_:Ljava/lang/Object;

.field private hasPicture_:Z

.field private hash_:Ljava/lang/Object;

.field private lastname_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private middlename_:Ljava/lang/Object;

.field private nameprefix_:Ljava/lang/Object;

.field private namesuffix_:Ljava/lang/Object;

.field private nativeFavorite_:Z

.field private phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

.field private sha1Hash_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32126
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    .line 32127
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->initFields()V

    .line 32128
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 30788
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 31167
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->memoizedIsInitialized:B

    .line 31242
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->memoizedSerializedSize:I

    .line 30789
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30783
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;-><init>(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 30790
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 31167
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->memoizedIsInitialized:B

    .line 31242
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->memoizedSerializedSize:I

    .line 30790
    return-void
.end method

.method static synthetic access$39502(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30783
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->firstname_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$39602(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30783
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->lastname_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$39702(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30783
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hash_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$39802(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30783
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->accountid_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$39902(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30783
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->sha1Hash_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$40002(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30783
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    return-object p1
.end method

.method static synthetic access$40102(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30783
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->email_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$40202(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30783
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->nativeFavorite_:Z

    return p1
.end method

.method static synthetic access$40302(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30783
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->nameprefix_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$40402(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30783
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->middlename_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$40502(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30783
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->namesuffix_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$40602(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30783
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->displayname_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$40702(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30783
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hasPicture_:Z

    return p1
.end method

.method static synthetic access$40802(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30783
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    return p1
.end method

.method private getAccountidBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 30919
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->accountid_:Ljava/lang/Object;

    .line 30920
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 30921
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 30923
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->accountid_:Ljava/lang/Object;

    .line 30926
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
    .locals 1

    .prologue
    .line 30794
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    return-object v0
.end method

.method private getDisplaynameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 31131
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->displayname_:Ljava/lang/Object;

    .line 31132
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 31133
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 31135
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->displayname_:Ljava/lang/Object;

    .line 31138
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getEmailBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 30993
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->email_:Ljava/lang/Object;

    .line 30994
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 30995
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 30997
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->email_:Ljava/lang/Object;

    .line 31000
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getFirstnameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 30823
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->firstname_:Ljava/lang/Object;

    .line 30824
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 30825
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 30827
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->firstname_:Ljava/lang/Object;

    .line 30830
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getHashBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 30887
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hash_:Ljava/lang/Object;

    .line 30888
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 30889
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 30891
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hash_:Ljava/lang/Object;

    .line 30894
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getLastnameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 30855
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->lastname_:Ljava/lang/Object;

    .line 30856
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 30857
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 30859
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->lastname_:Ljava/lang/Object;

    .line 30862
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getMiddlenameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 31067
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->middlename_:Ljava/lang/Object;

    .line 31068
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 31069
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 31071
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->middlename_:Ljava/lang/Object;

    .line 31074
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getNameprefixBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 31035
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->nameprefix_:Ljava/lang/Object;

    .line 31036
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 31037
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 31039
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->nameprefix_:Ljava/lang/Object;

    .line 31042
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getNamesuffixBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 31099
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->namesuffix_:Ljava/lang/Object;

    .line 31100
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 31101
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 31103
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->namesuffix_:Ljava/lang/Object;

    .line 31106
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getSha1HashBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 30951
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->sha1Hash_:Ljava/lang/Object;

    .line 30952
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 30953
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 30955
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->sha1Hash_:Ljava/lang/Object;

    .line 30958
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 31153
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->firstname_:Ljava/lang/Object;

    .line 31154
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->lastname_:Ljava/lang/Object;

    .line 31155
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hash_:Ljava/lang/Object;

    .line 31156
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->accountid_:Ljava/lang/Object;

    .line 31157
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->sha1Hash_:Ljava/lang/Object;

    .line 31158
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    .line 31159
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->email_:Ljava/lang/Object;

    .line 31160
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->nativeFavorite_:Z

    .line 31161
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->nameprefix_:Ljava/lang/Object;

    .line 31162
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->middlename_:Ljava/lang/Object;

    .line 31163
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->namesuffix_:Ljava/lang/Object;

    .line 31164
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->displayname_:Ljava/lang/Object;

    .line 31165
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hasPicture_:Z

    .line 31166
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1

    .prologue
    .line 31376
    #calls: Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->access$39300()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 31379
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31345
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    .line 31346
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 31347
    #calls: Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->access$39200(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    .line 31349
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31356
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    .line 31357
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 31358
    #calls: Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->access$39200(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    .line 31360
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 31312
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->access$39200(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 31318
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->access$39200(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31366
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->access$39200(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31372
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->access$39200(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31334
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->access$39200(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31340
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->access$39200(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 31323
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->access$39200(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 31329
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->access$39200(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAccountid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 30905
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->accountid_:Ljava/lang/Object;

    .line 30906
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 30907
    check-cast v0, Ljava/lang/String;

    .line 30915
    :goto_0
    return-object v0

    .line 30909
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 30911
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 30912
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 30913
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->accountid_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 30915
    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 30783
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
    .locals 1

    .prologue
    .line 30798
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    return-object v0
.end method

.method public getDisplayname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 31117
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->displayname_:Ljava/lang/Object;

    .line 31118
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 31119
    check-cast v0, Ljava/lang/String;

    .line 31127
    :goto_0
    return-object v0

    .line 31121
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 31123
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 31124
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 31125
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->displayname_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 31127
    goto :goto_0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 2

    .prologue
    .line 30979
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->email_:Ljava/lang/Object;

    .line 30980
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 30981
    check-cast v0, Ljava/lang/String;

    .line 30989
    :goto_0
    return-object v0

    .line 30983
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 30985
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 30986
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 30987
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->email_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 30989
    goto :goto_0
.end method

.method public getFirstname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 30809
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->firstname_:Ljava/lang/Object;

    .line 30810
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 30811
    check-cast v0, Ljava/lang/String;

    .line 30819
    :goto_0
    return-object v0

    .line 30813
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 30815
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 30816
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 30817
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->firstname_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 30819
    goto :goto_0
.end method

.method public getHasPicture()Z
    .locals 1

    .prologue
    .line 31149
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hasPicture_:Z

    return v0
.end method

.method public getHash()Ljava/lang/String;
    .locals 2

    .prologue
    .line 30873
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hash_:Ljava/lang/Object;

    .line 30874
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 30875
    check-cast v0, Ljava/lang/String;

    .line 30883
    :goto_0
    return-object v0

    .line 30877
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 30879
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 30880
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 30881
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hash_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 30883
    goto :goto_0
.end method

.method public getLastname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 30841
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->lastname_:Ljava/lang/Object;

    .line 30842
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 30843
    check-cast v0, Ljava/lang/String;

    .line 30851
    :goto_0
    return-object v0

    .line 30845
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 30847
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 30848
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 30849
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->lastname_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 30851
    goto :goto_0
.end method

.method public getMiddlename()Ljava/lang/String;
    .locals 2

    .prologue
    .line 31053
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->middlename_:Ljava/lang/Object;

    .line 31054
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 31055
    check-cast v0, Ljava/lang/String;

    .line 31063
    :goto_0
    return-object v0

    .line 31057
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 31059
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 31060
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 31061
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->middlename_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 31063
    goto :goto_0
.end method

.method public getNameprefix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 31021
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->nameprefix_:Ljava/lang/Object;

    .line 31022
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 31023
    check-cast v0, Ljava/lang/String;

    .line 31031
    :goto_0
    return-object v0

    .line 31025
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 31027
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 31028
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 31029
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->nameprefix_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 31031
    goto :goto_0
.end method

.method public getNamesuffix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 31085
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->namesuffix_:Ljava/lang/Object;

    .line 31086
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 31087
    check-cast v0, Ljava/lang/String;

    .line 31095
    :goto_0
    return-object v0

    .line 31089
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 31091
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 31092
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 31093
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->namesuffix_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 31095
    goto :goto_0
.end method

.method public getNativeFavorite()Z
    .locals 1

    .prologue
    .line 31011
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->nativeFavorite_:Z

    return v0
.end method

.method public getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    .locals 1

    .prologue
    .line 30969
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 31244
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->memoizedSerializedSize:I

    .line 31245
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 31301
    :goto_0
    return v0

    .line 31247
    :cond_0
    const/4 v0, 0x0

    .line 31248
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 31249
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getFirstnameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31252
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 31253
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getLastnameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31256
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 31257
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getHashBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31260
    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_4

    .line 31261
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getAccountidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31264
    :cond_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 31265
    const/4 v1, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getSha1HashBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31268
    :cond_5
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    .line 31269
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31272
    :cond_6
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    .line 31273
    const/4 v1, 0x7

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getEmailBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31276
    :cond_7
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_8

    .line 31277
    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->nativeFavorite_:Z

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 31280
    :cond_8
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_9

    .line 31281
    const/16 v1, 0x9

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getNameprefixBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31284
    :cond_9
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_a

    .line 31285
    const/16 v1, 0xa

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getMiddlenameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31288
    :cond_a
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit16 v1, v1, 0x400

    const/16 v2, 0x400

    if-ne v1, v2, :cond_b

    .line 31289
    const/16 v1, 0xb

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getNamesuffixBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31292
    :cond_b
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit16 v1, v1, 0x800

    const/16 v2, 0x800

    if-ne v1, v2, :cond_c

    .line 31293
    const/16 v1, 0xc

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getDisplaynameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31296
    :cond_c
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit16 v1, v1, 0x1000

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_d

    .line 31297
    const/16 v1, 0xd

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hasPicture_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 31300
    :cond_d
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->memoizedSerializedSize:I

    goto/16 :goto_0
.end method

.method public getSha1Hash()Ljava/lang/String;
    .locals 2

    .prologue
    .line 30937
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->sha1Hash_:Ljava/lang/Object;

    .line 30938
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 30939
    check-cast v0, Ljava/lang/String;

    .line 30947
    :goto_0
    return-object v0

    .line 30941
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 30943
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 30944
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 30945
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->sha1Hash_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 30947
    goto :goto_0
.end method

.method public hasAccountid()Z
    .locals 2

    .prologue
    .line 30902
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDisplayname()Z
    .locals 2

    .prologue
    .line 31114
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasEmail()Z
    .locals 2

    .prologue
    .line 30976
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFirstname()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 30806
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHasPicture()Z
    .locals 2

    .prologue
    .line 31146
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHash()Z
    .locals 2

    .prologue
    .line 30870
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLastname()Z
    .locals 2

    .prologue
    .line 30838
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMiddlename()Z
    .locals 2

    .prologue
    .line 31050
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNameprefix()Z
    .locals 2

    .prologue
    .line 31018
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNamesuffix()Z
    .locals 2

    .prologue
    .line 31082
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNativeFavorite()Z
    .locals 2

    .prologue
    .line 31008
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPhoneNumber()Z
    .locals 2

    .prologue
    .line 30966
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSha1Hash()Z
    .locals 2

    .prologue
    .line 30934
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 31169
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->memoizedIsInitialized:B

    .line 31170
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 31195
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 31170
    goto :goto_0

    .line 31172
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hasFirstname()Z

    move-result v0

    if-nez v0, :cond_2

    .line 31173
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->memoizedIsInitialized:B

    move v0, v2

    .line 31174
    goto :goto_0

    .line 31176
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hasLastname()Z

    move-result v0

    if-nez v0, :cond_3

    .line 31177
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->memoizedIsInitialized:B

    move v0, v2

    .line 31178
    goto :goto_0

    .line 31180
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hasHash()Z

    move-result v0

    if-nez v0, :cond_4

    .line 31181
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->memoizedIsInitialized:B

    move v0, v2

    .line 31182
    goto :goto_0

    .line 31184
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hasAccountid()Z

    move-result v0

    if-nez v0, :cond_5

    .line 31185
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->memoizedIsInitialized:B

    move v0, v2

    .line 31186
    goto :goto_0

    .line 31188
    :cond_5
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hasPhoneNumber()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 31189
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_6

    .line 31190
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->memoizedIsInitialized:B

    move v0, v2

    .line 31191
    goto :goto_0

    .line 31194
    :cond_6
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->memoizedIsInitialized:B

    move v0, v3

    .line 31195
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 30783
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1

    .prologue
    .line 31377
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 30783
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;
    .locals 1

    .prologue
    .line 31381
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 31306
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 31200
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getSerializedSize()I

    .line 31201
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 31202
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getFirstnameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 31204
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 31205
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getLastnameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 31207
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 31208
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getHashBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 31210
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 31211
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getAccountidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 31213
    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 31214
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getSha1HashBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 31216
    :cond_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 31217
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 31219
    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 31220
    const/4 v0, 0x7

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getEmailBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 31222
    :cond_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 31223
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->nativeFavorite_:Z

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 31225
    :cond_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    .line 31226
    const/16 v0, 0x9

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getNameprefixBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 31228
    :cond_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_9

    .line 31229
    const/16 v0, 0xa

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getMiddlenameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 31231
    :cond_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_a

    .line 31232
    const/16 v0, 0xb

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getNamesuffixBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 31234
    :cond_a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_b

    .line 31235
    const/16 v0, 0xc

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->getDisplaynameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 31237
    :cond_b
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_c

    .line 31238
    const/16 v0, 0xd

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->hasPicture_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 31240
    :cond_c
    return-void
.end method
