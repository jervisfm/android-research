.class public final Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$PersistentContactListOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;",
        "Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$PersistentContactListOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private contacts_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 32305
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 32414
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->contacts_:Ljava/util/List;

    .line 32306
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->maybeForceBuilderInitialization()V

    .line 32307
    return-void
.end method

.method static synthetic access$40900(Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 32300
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$41000()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;
    .locals 1

    .prologue
    .line 32300
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 32340
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;

    move-result-object v0

    .line 32341
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 32342
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 32345
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;
    .locals 1

    .prologue
    .line 32312
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;-><init>()V

    return-object v0
.end method

.method private ensureContactsIsMutable()V
    .locals 2

    .prologue
    .line 32417
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 32418
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->contacts_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->contacts_:Ljava/util/List;

    .line 32419
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->bitField0_:I

    .line 32421
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 32310
    return-void
.end method


# virtual methods
.method public addAllContacts(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;"
        }
    .end annotation

    .prologue
    .line 32484
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->ensureContactsIsMutable()V

    .line 32485
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->contacts_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 32487
    return-object p0
.end method

.method public addContacts(ILcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 32477
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->ensureContactsIsMutable()V

    .line 32478
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->contacts_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 32480
    return-object p0
.end method

.method public addContacts(ILcom/sgiggle/xmpp/SessionMessages$PersistentContact;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 32460
    if-nez p2, :cond_0

    .line 32461
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 32463
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->ensureContactsIsMutable()V

    .line 32464
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 32466
    return-object p0
.end method

.method public addContacts(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;
    .locals 2
    .parameter

    .prologue
    .line 32470
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->ensureContactsIsMutable()V

    .line 32471
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->contacts_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 32473
    return-object p0
.end method

.method public addContacts(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;
    .locals 1
    .parameter

    .prologue
    .line 32450
    if-nez p1, :cond_0

    .line 32451
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 32453
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->ensureContactsIsMutable()V

    .line 32454
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 32456
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 32300
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;
    .locals 2

    .prologue
    .line 32331
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;

    move-result-object v0

    .line 32332
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 32333
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 32335
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 32300
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;
    .locals 3

    .prologue
    .line 32349
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;-><init>(Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 32350
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->bitField0_:I

    .line 32351
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 32352
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->contacts_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->contacts_:Ljava/util/List;

    .line 32353
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->bitField0_:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->bitField0_:I

    .line 32355
    :cond_0
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->contacts_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->contacts_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->access$41202(Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;Ljava/util/List;)Ljava/util/List;

    .line 32356
    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 32300
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 32300
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;
    .locals 1

    .prologue
    .line 32316
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 32317
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->contacts_:Ljava/util/List;

    .line 32318
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->bitField0_:I

    .line 32319
    return-object p0
.end method

.method public clearContacts()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;
    .locals 1

    .prologue
    .line 32490
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->contacts_:Ljava/util/List;

    .line 32491
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->bitField0_:I

    .line 32493
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 32300
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 32300
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 32300
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;
    .locals 2

    .prologue
    .line 32323
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 32300
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getContacts(I)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
    .locals 1
    .parameter

    .prologue
    .line 32430
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    return-object v0
.end method

.method public getContactsCount()I
    .locals 1

    .prologue
    .line 32427
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getContactsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32424
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->contacts_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 32300
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 32300
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;
    .locals 1

    .prologue
    .line 32327
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 32375
    move v0, v2

    :goto_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->getContactsCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 32376
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->getContacts(I)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    move v0, v2

    .line 32381
    :goto_1
    return v0

    .line 32375
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 32381
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32300
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 32300
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32300
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32389
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 32390
    sparse-switch v0, :sswitch_data_0

    .line 32395
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 32397
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 32393
    goto :goto_1

    .line 32402
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;

    move-result-object v0

    .line 32403
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 32404
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->addContacts(Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    goto :goto_0

    .line 32390
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;
    .locals 2
    .parameter

    .prologue
    .line 32360
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 32371
    :goto_0
    return-object v0

    .line 32361
    :cond_0
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->contacts_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->access$41200(Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 32362
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 32363
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->contacts_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->access$41200(Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->contacts_:Ljava/util/List;

    .line 32364
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->bitField0_:I

    :cond_1
    :goto_1
    move-object v0, p0

    .line 32371
    goto :goto_0

    .line 32366
    :cond_2
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->ensureContactsIsMutable()V

    .line 32367
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->contacts_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->contacts_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->access$41200(Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public removeContacts(I)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;
    .locals 1
    .parameter

    .prologue
    .line 32496
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->ensureContactsIsMutable()V

    .line 32497
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 32499
    return-object p0
.end method

.method public setContacts(ILcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 32444
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->ensureContactsIsMutable()V

    .line 32445
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->contacts_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 32447
    return-object p0
.end method

.method public setContacts(ILcom/sgiggle/xmpp/SessionMessages$PersistentContact;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 32434
    if-nez p2, :cond_0

    .line 32435
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 32437
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->ensureContactsIsMutable()V

    .line 32438
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 32440
    return-object p0
.end method
