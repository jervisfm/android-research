.class public final Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$PersistentContactListOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PersistentContactList"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;
    }
.end annotation


# static fields
.field public static final CONTACTS_FIELD_NUMBER:I = 0x1

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;


# instance fields
.field private contacts_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32506
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;

    .line 32507
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->initFields()V

    .line 32508
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 32147
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 32184
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->memoizedIsInitialized:B

    .line 32207
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->memoizedSerializedSize:I

    .line 32148
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 32142
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;-><init>(Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 32149
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 32184
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->memoizedIsInitialized:B

    .line 32207
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->memoizedSerializedSize:I

    .line 32149
    return-void
.end method

.method static synthetic access$41200(Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 32142
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->contacts_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$41202(Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 32142
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->contacts_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;
    .locals 1

    .prologue
    .line 32153
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 32182
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->contacts_:Ljava/util/List;

    .line 32183
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;
    .locals 1

    .prologue
    .line 32293
    #calls: Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->access$41000()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;
    .locals 1
    .parameter

    .prologue
    .line 32296
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32262
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    move-result-object v0

    .line 32263
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 32264
    #calls: Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->access$40900(Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;

    move-result-object v0

    .line 32266
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32273
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    move-result-object v0

    .line 32274
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 32275
    #calls: Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->access$40900(Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;

    move-result-object v0

    .line 32277
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 32229
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->access$40900(Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 32235
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->access$40900(Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32283
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->access$40900(Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32289
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->access$40900(Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32251
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->access$40900(Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32257
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->access$40900(Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 32240
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->access$40900(Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 32246
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;->access$40900(Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getContacts(I)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
    .locals 1
    .parameter

    .prologue
    .line 32174
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    return-object v0
.end method

.method public getContactsCount()I
    .locals 1

    .prologue
    .line 32171
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->contacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getContactsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32164
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->contacts_:Ljava/util/List;

    return-object v0
.end method

.method public getContactsOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 32178
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactOrBuilder;

    return-object v0
.end method

.method public getContactsOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$PersistentContactOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32168
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->contacts_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 32142
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;
    .locals 1

    .prologue
    .line 32157
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 32209
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->memoizedSerializedSize:I

    .line 32210
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 32218
    :goto_0
    return v0

    :cond_0
    move v1, v2

    .line 32213
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->contacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 32214
    const/4 v3, 0x1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->contacts_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 32213
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_1

    .line 32217
    :cond_1
    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->memoizedSerializedSize:I

    move v0, v2

    .line 32218
    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 32186
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->memoizedIsInitialized:B

    .line 32187
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 32196
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 32187
    goto :goto_0

    :cond_1
    move v0, v2

    .line 32189
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->getContactsCount()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 32190
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->getContacts(I)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_2

    .line 32191
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->memoizedIsInitialized:B

    move v0, v2

    .line 32192
    goto :goto_0

    .line 32189
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 32195
    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->memoizedIsInitialized:B

    move v0, v3

    .line 32196
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 32142
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;
    .locals 1

    .prologue
    .line 32294
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 32142
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;
    .locals 1

    .prologue
    .line 32298
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;)Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 32223
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32201
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->getSerializedSize()I

    .line 32202
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->contacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 32203
    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PersistentContactList;->contacts_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 32202
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 32205
    :cond_0
    return-void
.end method
