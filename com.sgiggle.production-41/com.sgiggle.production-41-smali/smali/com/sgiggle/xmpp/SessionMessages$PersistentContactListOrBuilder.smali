.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$PersistentContactListOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PersistentContactListOrBuilder"
.end annotation


# virtual methods
.method public abstract getContacts(I)Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;
.end method

.method public abstract getContactsCount()I
.end method

.method public abstract getContactsList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$PersistentContact;",
            ">;"
        }
    .end annotation
.end method
