.class public final Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$PhoneNumberOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;",
        "Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$PhoneNumberOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private countryCode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

.field private subscriberNumber_:Ljava/lang/Object;

.field private type_:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 21557
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 21695
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->countryCode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 21738
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->subscriberNumber_:Ljava/lang/Object;

    .line 21774
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_GENERIC:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    .line 21558
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->maybeForceBuilderInitialization()V

    .line 21559
    return-void
.end method

.method static synthetic access$27100(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 21552
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$27200()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;
    .locals 1

    .prologue
    .line 21552
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 21596
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    .line 21597
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 21598
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 21601
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;
    .locals 1

    .prologue
    .line 21564
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 21562
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 21552
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    .locals 2

    .prologue
    .line 21587
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    .line 21588
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 21589
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 21591
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 21552
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    .locals 5

    .prologue
    .line 21605
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;-><init>(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 21606
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    .line 21607
    const/4 v2, 0x0

    .line 21608
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 21609
    or-int/lit8 v2, v2, 0x1

    .line 21611
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->countryCode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->countryCode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->access$27402(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 21612
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 21613
    or-int/lit8 v2, v2, 0x2

    .line 21615
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->subscriberNumber_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->subscriberNumber_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->access$27502(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21616
    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_2

    .line 21617
    or-int/lit8 v1, v2, 0x4

    .line 21619
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->type_:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->access$27602(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;Lcom/sgiggle/xmpp/SessionMessages$PhoneType;)Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    .line 21620
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->access$27702(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;I)I

    .line 21621
    return-object v0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 21552
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 21552
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;
    .locals 1

    .prologue
    .line 21568
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 21569
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->countryCode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 21570
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    .line 21571
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->subscriberNumber_:Ljava/lang/Object;

    .line 21572
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    .line 21573
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_GENERIC:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    .line 21574
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    .line 21575
    return-object p0
.end method

.method public clearCountryCode()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;
    .locals 1

    .prologue
    .line 21731
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->countryCode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 21733
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    .line 21734
    return-object p0
.end method

.method public clearSubscriberNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;
    .locals 1

    .prologue
    .line 21762
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    .line 21763
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getSubscriberNumber()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->subscriberNumber_:Ljava/lang/Object;

    .line 21765
    return-object p0
.end method

.method public clearType()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;
    .locals 1

    .prologue
    .line 21791
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    .line 21792
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_GENERIC:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    .line 21794
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 21552
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 21552
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 21552
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;
    .locals 2

    .prologue
    .line 21579
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 21552
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getCountryCode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 1

    .prologue
    .line 21700
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->countryCode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 21552
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 21552
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    .locals 1

    .prologue
    .line 21583
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method public getSubscriberNumber()Ljava/lang/String;
    .locals 2

    .prologue
    .line 21743
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->subscriberNumber_:Ljava/lang/Object;

    .line 21744
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 21745
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 21746
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->subscriberNumber_:Ljava/lang/Object;

    .line 21749
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getType()Lcom/sgiggle/xmpp/SessionMessages$PhoneType;
    .locals 1

    .prologue
    .line 21779
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    return-object v0
.end method

.method public hasCountryCode()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 21697
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSubscriberNumber()Z
    .locals 2

    .prologue
    .line 21740
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    .prologue
    .line 21776
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 21639
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->hasCountryCode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 21640
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->getCountryCode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 21642
    const/4 v0, 0x0

    .line 21645
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeCountryCode(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;
    .locals 2
    .parameter

    .prologue
    .line 21719
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->countryCode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 21721
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->countryCode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->countryCode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 21727
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    .line 21728
    return-object p0

    .line 21724
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->countryCode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21552
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 21552
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21552
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21653
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 21654
    sparse-switch v0, :sswitch_data_0

    .line 21659
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 21661
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 21657
    goto :goto_1

    .line 21666
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    .line 21667
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->hasCountryCode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 21668
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->getCountryCode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    .line 21670
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 21671
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->setCountryCode(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    goto :goto_0

    .line 21675
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    .line 21676
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->subscriberNumber_:Ljava/lang/Object;

    goto :goto_0

    .line 21680
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 21681
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    move-result-object v0

    .line 21682
    if-eqz v0, :cond_0

    .line 21683
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    .line 21684
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    goto :goto_0

    .line 21654
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;
    .locals 1
    .parameter

    .prologue
    .line 21625
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 21635
    :goto_0
    return-object v0

    .line 21626
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->hasCountryCode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 21627
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getCountryCode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->mergeCountryCode(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    .line 21629
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->hasSubscriberNumber()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 21630
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getSubscriberNumber()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->setSubscriberNumber(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    .line 21632
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->hasType()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 21633
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getType()Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$PhoneType;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    :cond_3
    move-object v0, p0

    .line 21635
    goto :goto_0
.end method

.method public setCountryCode(Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;
    .locals 1
    .parameter

    .prologue
    .line 21713
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->countryCode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 21715
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    .line 21716
    return-object p0
.end method

.method public setCountryCode(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;
    .locals 1
    .parameter

    .prologue
    .line 21703
    if-nez p1, :cond_0

    .line 21704
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 21706
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->countryCode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 21708
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    .line 21709
    return-object p0
.end method

.method public setSubscriberNumber(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;
    .locals 1
    .parameter

    .prologue
    .line 21753
    if-nez p1, :cond_0

    .line 21754
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 21756
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    .line 21757
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->subscriberNumber_:Ljava/lang/Object;

    .line 21759
    return-object p0
.end method

.method setSubscriberNumber(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 21768
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    .line 21769
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->subscriberNumber_:Ljava/lang/Object;

    .line 21771
    return-void
.end method

.method public setType(Lcom/sgiggle/xmpp/SessionMessages$PhoneType;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;
    .locals 1
    .parameter

    .prologue
    .line 21782
    if-nez p1, :cond_0

    .line 21783
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 21785
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->bitField0_:I

    .line 21786
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    .line 21788
    return-object p0
.end method
