.class public final Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$PhoneNumberOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PhoneNumber"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;
    }
.end annotation


# static fields
.field public static final COUNTRYCODE_FIELD_NUMBER:I = 0x1

.field public static final SUBSCRIBERNUMBER_FIELD_NUMBER:I = 0x2

.field public static final TYPE_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;


# instance fields
.field private bitField0_:I

.field private countryCode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private subscriberNumber_:Ljava/lang/Object;

.field private type_:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21801
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    .line 21802
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->initFields()V

    .line 21803
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 21351
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 21422
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->memoizedIsInitialized:B

    .line 21451
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->memoizedSerializedSize:I

    .line 21352
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 21346
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;-><init>(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 21353
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 21422
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->memoizedIsInitialized:B

    .line 21451
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->memoizedSerializedSize:I

    .line 21353
    return-void
.end method

.method static synthetic access$27402(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 21346
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->countryCode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    return-object p1
.end method

.method static synthetic access$27502(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 21346
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->subscriberNumber_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$27602(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;Lcom/sgiggle/xmpp/SessionMessages$PhoneType;)Lcom/sgiggle/xmpp/SessionMessages$PhoneType;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 21346
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->type_:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    return-object p1
.end method

.method static synthetic access$27702(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 21346
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    .locals 1

    .prologue
    .line 21357
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    return-object v0
.end method

.method private getSubscriberNumberBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 21396
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->subscriberNumber_:Ljava/lang/Object;

    .line 21397
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 21398
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 21400
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->subscriberNumber_:Ljava/lang/Object;

    .line 21403
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 21418
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->countryCode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 21419
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->subscriberNumber_:Ljava/lang/Object;

    .line 21420
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_GENERIC:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->type_:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    .line 21421
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;
    .locals 1

    .prologue
    .line 21545
    #calls: Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->access$27200()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;
    .locals 1
    .parameter

    .prologue
    .line 21548
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21514
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    .line 21515
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 21516
    #calls: Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->access$27100(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    .line 21518
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21525
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    .line 21526
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 21527
    #calls: Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->access$27100(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    .line 21529
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 21481
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->access$27100(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 21487
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->access$27100(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21535
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->access$27100(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21541
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->access$27100(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21503
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->access$27100(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21509
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->access$27100(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 21492
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->access$27100(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 21498
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->access$27100(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCountryCode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 1

    .prologue
    .line 21372
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->countryCode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 21346
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    .locals 1

    .prologue
    .line 21361
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 21453
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->memoizedSerializedSize:I

    .line 21454
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 21470
    :goto_0
    return v0

    .line 21456
    :cond_0
    const/4 v0, 0x0

    .line 21457
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 21458
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->countryCode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21461
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 21462
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getSubscriberNumberBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21465
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 21466
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->type_:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 21469
    :cond_3
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getSubscriberNumber()Ljava/lang/String;
    .locals 2

    .prologue
    .line 21382
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->subscriberNumber_:Ljava/lang/Object;

    .line 21383
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 21384
    check-cast v0, Ljava/lang/String;

    .line 21392
    :goto_0
    return-object v0

    .line 21386
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 21388
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 21389
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 21390
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->subscriberNumber_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 21392
    goto :goto_0
.end method

.method public getType()Lcom/sgiggle/xmpp/SessionMessages$PhoneType;
    .locals 1

    .prologue
    .line 21414
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->type_:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    return-object v0
.end method

.method public hasCountryCode()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 21369
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSubscriberNumber()Z
    .locals 2

    .prologue
    .line 21379
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    .prologue
    .line 21411
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 21424
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->memoizedIsInitialized:B

    .line 21425
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v2, :cond_0

    move v0, v2

    .line 21434
    :goto_0
    return v0

    :cond_0
    move v0, v3

    .line 21425
    goto :goto_0

    .line 21427
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->hasCountryCode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 21428
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getCountryCode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    .line 21429
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->memoizedIsInitialized:B

    move v0, v3

    .line 21430
    goto :goto_0

    .line 21433
    :cond_2
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->memoizedIsInitialized:B

    move v0, v2

    .line 21434
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 21346
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;
    .locals 1

    .prologue
    .line 21546
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 21346
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;
    .locals 1

    .prologue
    .line 21550
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 21475
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 21439
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getSerializedSize()I

    .line 21440
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 21441
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->countryCode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 21443
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 21444
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getSubscriberNumberBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 21446
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 21447
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->type_:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 21449
    :cond_2
    return-void
.end method
