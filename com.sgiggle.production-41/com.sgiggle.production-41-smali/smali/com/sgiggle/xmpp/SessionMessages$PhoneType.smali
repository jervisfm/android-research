.class public final enum Lcom/sgiggle/xmpp/SessionMessages$PhoneType;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PhoneType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$PhoneType;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$PhoneType; = null

.field public static final enum PHONE_TYPE_GENERIC:Lcom/sgiggle/xmpp/SessionMessages$PhoneType; = null

.field public static final PHONE_TYPE_GENERIC_VALUE:I = 0x0

.field public static final enum PHONE_TYPE_HOME:Lcom/sgiggle/xmpp/SessionMessages$PhoneType; = null

.field public static final PHONE_TYPE_HOME_VALUE:I = 0x2

.field public static final enum PHONE_TYPE_MAIN:Lcom/sgiggle/xmpp/SessionMessages$PhoneType; = null

.field public static final PHONE_TYPE_MAIN_VALUE:I = 0x4

.field public static final enum PHONE_TYPE_MOBILE:Lcom/sgiggle/xmpp/SessionMessages$PhoneType; = null

.field public static final PHONE_TYPE_MOBILE_VALUE:I = 0x1

.field public static final enum PHONE_TYPE_WORK:Lcom/sgiggle/xmpp/SessionMessages$PhoneType; = null

.field public static final PHONE_TYPE_WORK_VALUE:I = 0x3

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$PhoneType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 207
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    const-string v1, "PHONE_TYPE_GENERIC"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_GENERIC:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    .line 208
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    const-string v1, "PHONE_TYPE_MOBILE"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_MOBILE:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    .line 209
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    const-string v1, "PHONE_TYPE_HOME"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_HOME:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    .line 210
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    const-string v1, "PHONE_TYPE_WORK"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_WORK:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    .line 211
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    const-string v1, "PHONE_TYPE_MAIN"

    invoke-direct {v0, v1, v6, v6, v6}, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_MAIN:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    .line 205
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_GENERIC:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_MOBILE:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_HOME:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_WORK:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_MAIN:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    .line 239
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneType$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 248
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 249
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->value:I

    .line 250
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$PhoneType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 236
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$PhoneType;
    .locals 1
    .parameter

    .prologue
    .line 224
    packed-switch p0, :pswitch_data_0

    .line 230
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 225
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_GENERIC:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    goto :goto_0

    .line 226
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_MOBILE:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    goto :goto_0

    .line 227
    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_HOME:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    goto :goto_0

    .line 228
    :pswitch_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_WORK:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    goto :goto_0

    .line 229
    :pswitch_4
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_MAIN:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    goto :goto_0

    .line 224
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PhoneType;
    .locals 1
    .parameter

    .prologue
    .line 205
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$PhoneType;
    .locals 1

    .prologue
    .line 205
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 221
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->value:I

    return v0
.end method
