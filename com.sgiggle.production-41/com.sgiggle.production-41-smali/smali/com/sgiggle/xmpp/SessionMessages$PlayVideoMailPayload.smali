.class public final Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlayVideoMailPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final VIDEO_MAIL_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private videoMail_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 65056
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;

    .line 65057
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->initFields()V

    .line 65058
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 64657
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 64695
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->memoizedIsInitialized:B

    .line 64731
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->memoizedSerializedSize:I

    .line 64658
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 64652
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 64659
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 64695
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->memoizedIsInitialized:B

    .line 64731
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->memoizedSerializedSize:I

    .line 64659
    return-void
.end method

.method static synthetic access$83802(Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 64652
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$83902(Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 64652
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->videoMail_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    return-object p1
.end method

.method static synthetic access$84002(Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 64652
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;
    .locals 1

    .prologue
    .line 64663
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 64692
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 64693
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->videoMail_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    .line 64694
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 64821
    #calls: Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->access$83600()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 64824
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64790
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;

    move-result-object v0

    .line 64791
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 64792
    #calls: Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->access$83500(Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;

    move-result-object v0

    .line 64794
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64801
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;

    move-result-object v0

    .line 64802
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 64803
    #calls: Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->access$83500(Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;

    move-result-object v0

    .line 64805
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 64757
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->access$83500(Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 64763
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->access$83500(Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64811
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->access$83500(Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64817
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->access$83500(Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64779
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->access$83500(Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64785
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->access$83500(Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 64768
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->access$83500(Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 64774
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;->access$83500(Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 64678
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 64652
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;
    .locals 1

    .prologue
    .line 64667
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 64733
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->memoizedSerializedSize:I

    .line 64734
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 64746
    :goto_0
    return v0

    .line 64736
    :cond_0
    const/4 v0, 0x0

    .line 64737
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 64738
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64741
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 64742
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->videoMail_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64745
    :cond_2
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getVideoMail()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    .locals 1

    .prologue
    .line 64688
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->videoMail_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 64675
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoMail()Z
    .locals 2

    .prologue
    .line 64685
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 64697
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->memoizedIsInitialized:B

    .line 64698
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 64717
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 64698
    goto :goto_0

    .line 64700
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 64701
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 64702
    goto :goto_0

    .line 64704
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->hasVideoMail()Z

    move-result v0

    if-nez v0, :cond_3

    .line 64705
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 64706
    goto :goto_0

    .line 64708
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    .line 64709
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 64710
    goto :goto_0

    .line 64712
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->getVideoMail()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_5

    .line 64713
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 64714
    goto :goto_0

    .line 64716
    :cond_5
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 64717
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 64652
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 64822
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 64652
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 64826
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 64751
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 64722
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->getSerializedSize()I

    .line 64723
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 64724
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 64726
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 64727
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->videoMail_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 64729
    :cond_1
    return-void
.end method
