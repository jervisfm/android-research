.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PlayVideoMailPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getVideoMail()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasVideoMail()Z
.end method
