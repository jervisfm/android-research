.class public final enum Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PostCallContentType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType; = null

.field public static final enum POSTCALL_APPSTORE:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType; = null

.field public static final POSTCALL_APPSTORE_VALUE:I = 0x3

.field public static final enum POSTCALL_CALLQUALITY:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType; = null

.field public static final POSTCALL_CALLQUALITY_VALUE:I = 0x5

.field public static final enum POSTCALL_FACEBOOK:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType; = null

.field public static final POSTCALL_FACEBOOK_VALUE:I = 0x2

.field public static final enum POSTCALL_INVITE:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType; = null

.field public static final POSTCALL_INVITE_VALUE:I = 0x4

.field public static final enum POSTCALL_NONE:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType; = null

.field public static final POSTCALL_NONE_VALUE:I = 0x0

.field public static final enum POSTCALL_VGOOD:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType; = null

.field public static final POSTCALL_VGOOD_VALUE:I = 0x1

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 489
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    const-string v1, "POSTCALL_NONE"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->POSTCALL_NONE:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    .line 490
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    const-string v1, "POSTCALL_VGOOD"

    invoke-direct {v0, v1, v6, v6, v6}, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->POSTCALL_VGOOD:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    .line 491
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    const-string v1, "POSTCALL_FACEBOOK"

    invoke-direct {v0, v1, v7, v7, v7}, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->POSTCALL_FACEBOOK:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    .line 492
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    const-string v1, "POSTCALL_APPSTORE"

    invoke-direct {v0, v1, v8, v8, v8}, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->POSTCALL_APPSTORE:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    .line 493
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    const-string v1, "POSTCALL_INVITE"

    invoke-direct {v0, v1, v9, v9, v9}, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->POSTCALL_INVITE:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    .line 494
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    const-string v1, "POSTCALL_CALLQUALITY"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const/4 v4, 0x5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->POSTCALL_CALLQUALITY:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    .line 487
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->POSTCALL_NONE:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->POSTCALL_VGOOD:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->POSTCALL_FACEBOOK:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->POSTCALL_APPSTORE:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->POSTCALL_INVITE:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->POSTCALL_CALLQUALITY:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    .line 524
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 533
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 534
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->value:I

    .line 535
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 521
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;
    .locals 1
    .parameter

    .prologue
    .line 508
    packed-switch p0, :pswitch_data_0

    .line 515
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 509
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->POSTCALL_NONE:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    goto :goto_0

    .line 510
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->POSTCALL_VGOOD:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    goto :goto_0

    .line 511
    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->POSTCALL_FACEBOOK:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    goto :goto_0

    .line 512
    :pswitch_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->POSTCALL_APPSTORE:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    goto :goto_0

    .line 513
    :pswitch_4
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->POSTCALL_INVITE:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    goto :goto_0

    .line 514
    :pswitch_5
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->POSTCALL_CALLQUALITY:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    goto :goto_0

    .line 508
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;
    .locals 1
    .parameter

    .prologue
    .line 487
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;
    .locals 1

    .prologue
    .line 487
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 505
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->value:I

    return v0
.end method
