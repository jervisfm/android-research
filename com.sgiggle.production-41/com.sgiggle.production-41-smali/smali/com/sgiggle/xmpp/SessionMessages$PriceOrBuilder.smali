.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$PriceOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PriceOrBuilder"
.end annotation


# virtual methods
.method public abstract getLabel()Ljava/lang/String;
.end method

.method public abstract getLocalCurrencyCode()Ljava/lang/String;
.end method

.method public abstract getValue()F
.end method

.method public abstract hasLabel()Z
.end method

.method public abstract hasLocalCurrencyCode()Z
.end method

.method public abstract hasValue()Z
.end method
