.class public final Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntryOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;",
        "Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntryOrBuilder;"
    }
.end annotation


# instance fields
.field private beginTime_:J

.field private bitField0_:I

.field private categoryKey_:Ljava/lang/Object;

.field private categorySubkey_:Ljava/lang/Object;

.field private category_:Ljava/lang/Object;

.field private endTime_:J

.field private externalMarketId_:Ljava/lang/Object;

.field private imagePath_:Ljava/lang/Object;

.field private leaseDuration_:I

.field private marketId_:I

.field private priceId_:Ljava/lang/Object;

.field private price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

.field private productDescription_:Ljava/lang/Object;

.field private productId_:J

.field private productMarketId_:Ljava/lang/Object;

.field private productName_:Ljava/lang/Object;

.field private purchased_:Z

.field private sKU_:Ljava/lang/Object;

.field private sortOrder_:I


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->sKU_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productName_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productDescription_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->categoryKey_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->category_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productMarketId_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->externalMarketId_:Ljava/lang/Object;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Price;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->imagePath_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->categorySubkey_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->priceId_:Ljava/lang/Object;

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->maybeForceBuilderInitialization()V

    return-void
.end method

.method static synthetic access$99000(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$99100()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    .locals 2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    .locals 8

    const/high16 v7, 0x2

    const/high16 v6, 0x1

    const v5, 0x8000

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    or-int/lit8 v2, v2, 0x1

    :cond_0
    iget-wide v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productId_:J

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->productId_:J
    invoke-static {v0, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->access$99302(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;J)J

    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->sKU_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->sKU_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->access$99402(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productName_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->productName_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->access$99502(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    or-int/lit8 v2, v2, 0x8

    :cond_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productDescription_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->productDescription_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->access$99602(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v1, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    or-int/lit8 v2, v2, 0x10

    :cond_4
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->categoryKey_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->categoryKey_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->access$99702(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v1, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    or-int/lit8 v2, v2, 0x20

    :cond_5
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->category_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->category_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->access$99802(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v1, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    or-int/lit8 v2, v2, 0x40

    :cond_6
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->marketId_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->marketId_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->access$99902(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;I)I

    and-int/lit16 v3, v1, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    or-int/lit16 v2, v2, 0x80

    :cond_7
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productMarketId_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->productMarketId_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->access$100002(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit16 v3, v1, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_8

    or-int/lit16 v2, v2, 0x100

    :cond_8
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->externalMarketId_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->externalMarketId_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->access$100102(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit16 v3, v1, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_9

    or-int/lit16 v2, v2, 0x200

    :cond_9
    iget-wide v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->beginTime_:J

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->beginTime_:J
    invoke-static {v0, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->access$100202(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;J)J

    and-int/lit16 v3, v1, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_a

    or-int/lit16 v2, v2, 0x400

    :cond_a
    iget-wide v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->endTime_:J

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->endTime_:J
    invoke-static {v0, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->access$100302(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;J)J

    and-int/lit16 v3, v1, 0x800

    const/16 v4, 0x800

    if-ne v3, v4, :cond_b

    or-int/lit16 v2, v2, 0x800

    :cond_b
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->leaseDuration_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->leaseDuration_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->access$100402(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;I)I

    and-int/lit16 v3, v1, 0x1000

    const/16 v4, 0x1000

    if-ne v3, v4, :cond_c

    or-int/lit16 v2, v2, 0x1000

    :cond_c
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->access$100502(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;Lcom/sgiggle/xmpp/SessionMessages$Price;)Lcom/sgiggle/xmpp/SessionMessages$Price;

    and-int/lit16 v3, v1, 0x2000

    const/16 v4, 0x2000

    if-ne v3, v4, :cond_d

    or-int/lit16 v2, v2, 0x2000

    :cond_d
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->sortOrder_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->sortOrder_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->access$100602(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;I)I

    and-int/lit16 v3, v1, 0x4000

    const/16 v4, 0x4000

    if-ne v3, v4, :cond_e

    or-int/lit16 v2, v2, 0x4000

    :cond_e
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->purchased_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->purchased_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->access$100702(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;Z)Z

    and-int v3, v1, v5

    if-ne v3, v5, :cond_f

    or-int/2addr v2, v5

    :cond_f
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->imagePath_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->imagePath_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->access$100802(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;Ljava/lang/Object;)Ljava/lang/Object;

    and-int v3, v1, v6

    if-ne v3, v6, :cond_10

    or-int/2addr v2, v6

    :cond_10
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->categorySubkey_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->categorySubkey_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->access$100902(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/2addr v1, v7

    if-ne v1, v7, :cond_11

    or-int v1, v2, v7

    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->priceId_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->priceId_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->access$101002(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;Ljava/lang/Object;)Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->access$101102(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;I)I

    return-object v0

    :cond_11
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    iput-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productId_:J

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->sKU_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productName_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productDescription_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->categoryKey_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->category_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->marketId_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productMarketId_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->externalMarketId_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->beginTime_:J

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->endTime_:J

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->leaseDuration_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Price;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->sortOrder_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x2001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->purchased_:Z

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x4001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->imagePath_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const v1, -0x8001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->categorySubkey_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const v1, -0x10001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->priceId_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const v1, -0x20001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearBeginTime()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->beginTime_:J

    return-object p0
.end method

.method public clearCategory()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getCategory()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->category_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearCategoryKey()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getCategoryKey()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->categoryKey_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearCategorySubkey()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const v1, -0x10001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getCategorySubkey()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->categorySubkey_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearEndTime()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->endTime_:J

    return-object p0
.end method

.method public clearExternalMarketId()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getExternalMarketId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->externalMarketId_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearImagePath()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const v1, -0x8001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getImagePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->imagePath_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearLeaseDuration()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->leaseDuration_:I

    return-object p0
.end method

.method public clearMarketId()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->marketId_:I

    return-object p0
.end method

.method public clearPrice()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Price;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearPriceId()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const v1, -0x20001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getPriceId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->priceId_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearProductDescription()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductDescription()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productDescription_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearProductId()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productId_:J

    return-object p0
.end method

.method public clearProductMarketId()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductMarketId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productMarketId_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearProductName()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productName_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearPurchased()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x4001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->purchased_:Z

    return-object p0
.end method

.method public clearSKU()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getSKU()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->sKU_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearSortOrder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x2001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->sortOrder_:I

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBeginTime()J
    .locals 2

    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->beginTime_:J

    return-wide v0
.end method

.method public getCategory()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->category_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->category_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getCategoryKey()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->categoryKey_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->categoryKey_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getCategorySubkey()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->categorySubkey_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->categorySubkey_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    return-object v0
.end method

.method public getEndTime()J
    .locals 2

    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->endTime_:J

    return-wide v0
.end method

.method public getExternalMarketId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->externalMarketId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->externalMarketId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getImagePath()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->imagePath_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->imagePath_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getLeaseDuration()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->leaseDuration_:I

    return v0
.end method

.method public getMarketId()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->marketId_:I

    return v0
.end method

.method public getPrice()Lcom/sgiggle/xmpp/SessionMessages$Price;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    return-object v0
.end method

.method public getPriceId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->priceId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->priceId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getProductDescription()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productDescription_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productDescription_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getProductId()J
    .locals 2

    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productId_:J

    return-wide v0
.end method

.method public getProductMarketId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productMarketId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productMarketId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getProductName()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productName_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productName_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getPurchased()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->purchased_:Z

    return v0
.end method

.method public getSKU()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->sKU_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->sKU_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getSortOrder()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->sortOrder_:I

    return v0
.end method

.method public hasBeginTime()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCategory()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCategoryKey()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCategorySubkey()Z
    .locals 2

    const/high16 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasEndTime()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasExternalMarketId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasImagePath()Z
    .locals 2

    const v1, 0x8000

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLeaseDuration()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMarketId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPrice()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPriceId()Z
    .locals 2

    const/high16 v1, 0x2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasProductDescription()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasProductId()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasProductMarketId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasProductName()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPurchased()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSKU()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSortOrder()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->hasProductId()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->hasProductName()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->hasCategoryKey()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->hasCategory()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->hasMarketId()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->hasProductMarketId()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->hasExternalMarketId()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->hasBeginTime()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->hasEndTime()Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->hasLeaseDuration()Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->hasPrice()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->getPrice()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Price;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_a

    move v0, v1

    goto :goto_0

    :cond_a
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    goto :goto_1

    :sswitch_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productId_:J

    goto :goto_0

    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->sKU_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productName_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productDescription_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->categoryKey_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->category_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->marketId_:I

    goto :goto_0

    :sswitch_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productMarketId_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->externalMarketId_:Ljava/lang/Object;

    goto/16 :goto_0

    :sswitch_a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->beginTime_:J

    goto/16 :goto_0

    :sswitch_b
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->endTime_:J

    goto/16 :goto_0

    :sswitch_c
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->leaseDuration_:I

    goto/16 :goto_0

    :sswitch_d
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Price;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Price$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->hasPrice()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->getPrice()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Price$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Price;)Lcom/sgiggle/xmpp/SessionMessages$Price$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Price$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->setPrice(Lcom/sgiggle/xmpp/SessionMessages$Price;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    goto/16 :goto_0

    :sswitch_e
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->sortOrder_:I

    goto/16 :goto_0

    :sswitch_f
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->purchased_:Z

    goto/16 :goto_0

    :sswitch_10
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const v1, 0x8000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->imagePath_:Ljava/lang/Object;

    goto/16 :goto_0

    :sswitch_11
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const/high16 v1, 0x1

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->categorySubkey_:Ljava/lang/Object;

    goto/16 :goto_0

    :sswitch_12
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const/high16 v1, 0x2

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->priceId_:Ljava/lang/Object;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
        0x92 -> :sswitch_12
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasProductId()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductId()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->setProductId(J)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasSKU()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getSKU()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->setSKU(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasProductName()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->setProductName(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasProductDescription()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductDescription()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->setProductDescription(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasCategoryKey()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getCategoryKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->setCategoryKey(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    :cond_5
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasCategory()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getCategory()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->setCategory(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    :cond_6
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasMarketId()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getMarketId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->setMarketId(I)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    :cond_7
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasProductMarketId()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductMarketId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->setProductMarketId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    :cond_8
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasExternalMarketId()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getExternalMarketId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->setExternalMarketId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    :cond_9
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasBeginTime()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getBeginTime()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->setBeginTime(J)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    :cond_a
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasEndTime()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getEndTime()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->setEndTime(J)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    :cond_b
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasLeaseDuration()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getLeaseDuration()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->setLeaseDuration(I)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    :cond_c
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasPrice()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getPrice()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->mergePrice(Lcom/sgiggle/xmpp/SessionMessages$Price;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    :cond_d
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasSortOrder()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getSortOrder()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->setSortOrder(I)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    :cond_e
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasPurchased()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getPurchased()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->setPurchased(Z)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    :cond_f
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasImagePath()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getImagePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->setImagePath(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    :cond_10
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasCategorySubkey()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getCategorySubkey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->setCategorySubkey(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    :cond_11
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasPriceId()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getPriceId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->setPriceId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    :cond_12
    move-object v0, p0

    goto/16 :goto_0
.end method

.method public mergePrice(Lcom/sgiggle/xmpp/SessionMessages$Price;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Price;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Price;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Price;)Lcom/sgiggle/xmpp/SessionMessages$Price$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Price$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Price;)Lcom/sgiggle/xmpp/SessionMessages$Price$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Price$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    goto :goto_0
.end method

.method public setBeginTime(J)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->beginTime_:J

    return-object p0
.end method

.method public setCategory(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->category_:Ljava/lang/Object;

    return-object p0
.end method

.method setCategory(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->category_:Ljava/lang/Object;

    return-void
.end method

.method public setCategoryKey(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->categoryKey_:Ljava/lang/Object;

    return-object p0
.end method

.method setCategoryKey(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->categoryKey_:Ljava/lang/Object;

    return-void
.end method

.method public setCategorySubkey(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const/high16 v1, 0x1

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->categorySubkey_:Ljava/lang/Object;

    return-object p0
.end method

.method setCategorySubkey(Lcom/google/protobuf/ByteString;)V
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const/high16 v1, 0x1

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->categorySubkey_:Ljava/lang/Object;

    return-void
.end method

.method public setEndTime(J)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->endTime_:J

    return-object p0
.end method

.method public setExternalMarketId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->externalMarketId_:Ljava/lang/Object;

    return-object p0
.end method

.method setExternalMarketId(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->externalMarketId_:Ljava/lang/Object;

    return-void
.end method

.method public setImagePath(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const v1, 0x8000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->imagePath_:Ljava/lang/Object;

    return-object p0
.end method

.method setImagePath(Lcom/google/protobuf/ByteString;)V
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const v1, 0x8000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->imagePath_:Ljava/lang/Object;

    return-void
.end method

.method public setLeaseDuration(I)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->leaseDuration_:I

    return-object p0
.end method

.method public setMarketId(I)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->marketId_:I

    return-object p0
.end method

.method public setPrice(Lcom/sgiggle/xmpp/SessionMessages$Price$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Price$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    return-object p0
.end method

.method public setPrice(Lcom/sgiggle/xmpp/SessionMessages$Price;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    return-object p0
.end method

.method public setPriceId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const/high16 v1, 0x2

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->priceId_:Ljava/lang/Object;

    return-object p0
.end method

.method setPriceId(Lcom/google/protobuf/ByteString;)V
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    const/high16 v1, 0x2

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->priceId_:Ljava/lang/Object;

    return-void
.end method

.method public setProductDescription(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productDescription_:Ljava/lang/Object;

    return-object p0
.end method

.method setProductDescription(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productDescription_:Ljava/lang/Object;

    return-void
.end method

.method public setProductId(J)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productId_:J

    return-object p0
.end method

.method public setProductMarketId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productMarketId_:Ljava/lang/Object;

    return-object p0
.end method

.method setProductMarketId(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productMarketId_:Ljava/lang/Object;

    return-void
.end method

.method public setProductName(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productName_:Ljava/lang/Object;

    return-object p0
.end method

.method setProductName(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->productName_:Ljava/lang/Object;

    return-void
.end method

.method public setPurchased(Z)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->purchased_:Z

    return-object p0
.end method

.method public setSKU(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->sKU_:Ljava/lang/Object;

    return-object p0
.end method

.method setSKU(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->sKU_:Ljava/lang/Object;

    return-void
.end method

.method public setSortOrder(I)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->bitField0_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->sortOrder_:I

    return-object p0
.end method
