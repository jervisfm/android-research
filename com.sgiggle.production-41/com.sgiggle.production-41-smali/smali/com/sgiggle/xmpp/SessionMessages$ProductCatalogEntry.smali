.class public final Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntryOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProductCatalogEntry"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    }
.end annotation


# static fields
.field public static final BEGIN_TIME_FIELD_NUMBER:I = 0xa

.field public static final CATEGORY_FIELD_NUMBER:I = 0x6

.field public static final CATEGORY_KEY_FIELD_NUMBER:I = 0x5

.field public static final CATEGORY_SUBKEY_FIELD_NUMBER:I = 0x11

.field public static final END_TIME_FIELD_NUMBER:I = 0xb

.field public static final EXTERNAL_MARKET_ID_FIELD_NUMBER:I = 0x9

.field public static final IMAGE_PATH_FIELD_NUMBER:I = 0x10

.field public static final LEASE_DURATION_FIELD_NUMBER:I = 0xc

.field public static final MARKET_ID_FIELD_NUMBER:I = 0x7

.field public static final PRICE_FIELD_NUMBER:I = 0xd

.field public static final PRICE_ID_FIELD_NUMBER:I = 0x12

.field public static final PRODUCT_DESCRIPTION_FIELD_NUMBER:I = 0x4

.field public static final PRODUCT_ID_FIELD_NUMBER:I = 0x1

.field public static final PRODUCT_MARKET_ID_FIELD_NUMBER:I = 0x8

.field public static final PRODUCT_NAME_FIELD_NUMBER:I = 0x3

.field public static final PURCHASED_FIELD_NUMBER:I = 0xf

.field public static final SKU_FIELD_NUMBER:I = 0x2

.field public static final SORT_ORDER_FIELD_NUMBER:I = 0xe

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;


# instance fields
.field private beginTime_:J

.field private bitField0_:I

.field private categoryKey_:Ljava/lang/Object;

.field private categorySubkey_:Ljava/lang/Object;

.field private category_:Ljava/lang/Object;

.field private endTime_:J

.field private externalMarketId_:Ljava/lang/Object;

.field private imagePath_:Ljava/lang/Object;

.field private leaseDuration_:I

.field private marketId_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private priceId_:Ljava/lang/Object;

.field private price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

.field private productDescription_:Ljava/lang/Object;

.field private productId_:J

.field private productMarketId_:Ljava/lang/Object;

.field private productName_:Ljava/lang/Object;

.field private purchased_:Z

.field private sKU_:Ljava/lang/Object;

.field private sortOrder_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->initFields()V

    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$100002(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->productMarketId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$100102(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->externalMarketId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$100202(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;J)J
    .locals 0

    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->beginTime_:J

    return-wide p1
.end method

.method static synthetic access$100302(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;J)J
    .locals 0

    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->endTime_:J

    return-wide p1
.end method

.method static synthetic access$100402(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->leaseDuration_:I

    return p1
.end method

.method static synthetic access$100502(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;Lcom/sgiggle/xmpp/SessionMessages$Price;)Lcom/sgiggle/xmpp/SessionMessages$Price;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    return-object p1
.end method

.method static synthetic access$100602(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->sortOrder_:I

    return p1
.end method

.method static synthetic access$100702(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->purchased_:Z

    return p1
.end method

.method static synthetic access$100802(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->imagePath_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$100902(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->categorySubkey_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$101002(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->priceId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$101102(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    return p1
.end method

.method static synthetic access$99302(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;J)J
    .locals 0

    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->productId_:J

    return-wide p1
.end method

.method static synthetic access$99402(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->sKU_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$99502(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->productName_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$99602(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->productDescription_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$99702(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->categoryKey_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$99802(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->category_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$99902(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->marketId_:I

    return p1
.end method

.method private getCategoryBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->category_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->category_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getCategoryKeyBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->categoryKey_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->categoryKey_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getCategorySubkeyBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->categorySubkey_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->categorySubkey_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    return-object v0
.end method

.method private getExternalMarketIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->externalMarketId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->externalMarketId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getImagePathBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->imagePath_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->imagePath_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getPriceIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->priceId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->priceId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getProductDescriptionBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->productDescription_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->productDescription_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getProductMarketIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->productMarketId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->productMarketId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getProductNameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->productName_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->productName_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getSKUBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->sKU_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->sKU_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    iput-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->productId_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->sKU_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->productName_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->productDescription_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->categoryKey_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->category_:Ljava/lang/Object;

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->marketId_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->productMarketId_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->externalMarketId_:Ljava/lang/Object;

    iput-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->beginTime_:J

    iput-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->endTime_:J

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->leaseDuration_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Price;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->sortOrder_:I

    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->purchased_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->imagePath_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->categorySubkey_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->priceId_:Ljava/lang/Object;

    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->access$99100()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->access$99000(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->access$99000(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->access$99000(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->access$99000(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->access$99000(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->access$99000(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->access$99000(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->access$99000(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->access$99000(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->access$99000(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBeginTime()J
    .locals 2

    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->beginTime_:J

    return-wide v0
.end method

.method public getCategory()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->category_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->category_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getCategoryKey()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->categoryKey_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->categoryKey_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getCategorySubkey()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->categorySubkey_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->categorySubkey_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    return-object v0
.end method

.method public getEndTime()J
    .locals 2

    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->endTime_:J

    return-wide v0
.end method

.method public getExternalMarketId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->externalMarketId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->externalMarketId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getImagePath()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->imagePath_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->imagePath_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getLeaseDuration()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->leaseDuration_:I

    return v0
.end method

.method public getMarketId()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->marketId_:I

    return v0
.end method

.method public getPrice()Lcom/sgiggle/xmpp/SessionMessages$Price;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    return-object v0
.end method

.method public getPriceId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->priceId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->priceId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getProductDescription()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->productDescription_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->productDescription_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getProductId()J
    .locals 2

    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->productId_:J

    return-wide v0
.end method

.method public getProductMarketId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->productMarketId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->productMarketId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getProductName()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->productName_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->productName_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getPurchased()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->purchased_:Z

    return v0
.end method

.method public getSKU()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->sKU_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->sKU_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 8

    const/16 v7, 0x10

    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    iget-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->productId_:J

    invoke-static {v3, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getSKUBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_3

    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v6, :cond_4

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductDescriptionBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    if-ne v1, v7, :cond_5

    const/4 v1, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getCategoryKeyBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    const/4 v1, 0x6

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getCategoryBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    const/4 v1, 0x7

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->marketId_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_8

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductMarketIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v6, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_9

    const/16 v1, 0x9

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getExternalMarketIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_a

    const/16 v1, 0xa

    iget-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->beginTime_:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit16 v1, v1, 0x400

    const/16 v2, 0x400

    if-ne v1, v2, :cond_b

    const/16 v1, 0xb

    iget-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->endTime_:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit16 v1, v1, 0x800

    const/16 v2, 0x800

    if-ne v1, v2, :cond_c

    const/16 v1, 0xc

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->leaseDuration_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit16 v1, v1, 0x1000

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_d

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit16 v1, v1, 0x2000

    const/16 v2, 0x2000

    if-ne v1, v2, :cond_e

    const/16 v1, 0xe

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->sortOrder_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_e
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit16 v1, v1, 0x4000

    const/16 v2, 0x4000

    if-ne v1, v2, :cond_f

    const/16 v1, 0xf

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->purchased_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_f
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    const v2, 0x8000

    and-int/2addr v1, v2

    const v2, 0x8000

    if-ne v1, v2, :cond_10

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getImagePathBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v7, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_10
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    const/high16 v2, 0x1

    and-int/2addr v1, v2

    const/high16 v2, 0x1

    if-ne v1, v2, :cond_11

    const/16 v1, 0x11

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getCategorySubkeyBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_11
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    const/high16 v2, 0x2

    and-int/2addr v1, v2

    const/high16 v2, 0x2

    if-ne v1, v2, :cond_12

    const/16 v1, 0x12

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getPriceIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_12
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->memoizedSerializedSize:I

    goto/16 :goto_0
.end method

.method public getSortOrder()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->sortOrder_:I

    return v0
.end method

.method public hasBeginTime()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCategory()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCategoryKey()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCategorySubkey()Z
    .locals 2

    const/high16 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasEndTime()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasExternalMarketId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasImagePath()Z
    .locals 2

    const v1, 0x8000

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLeaseDuration()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMarketId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPrice()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPriceId()Z
    .locals 2

    const/high16 v1, 0x2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasProductDescription()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasProductId()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasProductMarketId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasProductName()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPurchased()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSKU()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSortOrder()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->memoizedIsInitialized:B

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasProductId()Z

    move-result v0

    if-nez v0, :cond_2

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasProductName()Z

    move-result v0

    if-nez v0, :cond_3

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasCategoryKey()Z

    move-result v0

    if-nez v0, :cond_4

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasCategory()Z

    move-result v0

    if-nez v0, :cond_5

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasMarketId()Z

    move-result v0

    if-nez v0, :cond_6

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasProductMarketId()Z

    move-result v0

    if-nez v0, :cond_7

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasExternalMarketId()Z

    move-result v0

    if-nez v0, :cond_8

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_8
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasBeginTime()Z

    move-result v0

    if-nez v0, :cond_9

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_9
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasEndTime()Z

    move-result v0

    if-nez v0, :cond_a

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_a
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasLeaseDuration()Z

    move-result v0

    if-nez v0, :cond_b

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_b
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasPrice()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getPrice()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Price;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_c

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_c
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->memoizedIsInitialized:B

    move v0, v3

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;
    .locals 1

    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v6, 0x10

    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getSerializedSize()I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->productId_:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getSKUBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductDescriptionBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v6, :cond_4

    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getCategoryKeyBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getCategoryBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    const/4 v0, 0x7

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->marketId_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductMarketIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    const/16 v0, 0x9

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getExternalMarketIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_9

    const/16 v0, 0xa

    iget-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->beginTime_:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeUInt64(IJ)V

    :cond_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_a

    const/16 v0, 0xb

    iget-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->endTime_:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeUInt64(IJ)V

    :cond_a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_b

    const/16 v0, 0xc

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->leaseDuration_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_b
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_c

    const/16 v0, 0xd

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_c
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_d

    const/16 v0, 0xe

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->sortOrder_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    :cond_d
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_e

    const/16 v0, 0xf

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->purchased_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_e
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_f

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getImagePathBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v6, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_f
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    const/high16 v1, 0x1

    and-int/2addr v0, v1

    const/high16 v1, 0x1

    if-ne v0, v1, :cond_10

    const/16 v0, 0x11

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getCategorySubkeyBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_10
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->bitField0_:I

    const/high16 v1, 0x2

    and-int/2addr v0, v1

    const/high16 v1, 0x2

    if-ne v0, v1, :cond_11

    const/16 v0, 0x12

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getPriceIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_11
    return-void
.end method
