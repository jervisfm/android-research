.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntryOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ProductCatalogEntryOrBuilder"
.end annotation


# virtual methods
.method public abstract getBeginTime()J
.end method

.method public abstract getCategory()Ljava/lang/String;
.end method

.method public abstract getCategoryKey()Ljava/lang/String;
.end method

.method public abstract getCategorySubkey()Ljava/lang/String;
.end method

.method public abstract getEndTime()J
.end method

.method public abstract getExternalMarketId()Ljava/lang/String;
.end method

.method public abstract getImagePath()Ljava/lang/String;
.end method

.method public abstract getLeaseDuration()I
.end method

.method public abstract getMarketId()I
.end method

.method public abstract getPrice()Lcom/sgiggle/xmpp/SessionMessages$Price;
.end method

.method public abstract getPriceId()Ljava/lang/String;
.end method

.method public abstract getProductDescription()Ljava/lang/String;
.end method

.method public abstract getProductId()J
.end method

.method public abstract getProductMarketId()Ljava/lang/String;
.end method

.method public abstract getProductName()Ljava/lang/String;
.end method

.method public abstract getPurchased()Z
.end method

.method public abstract getSKU()Ljava/lang/String;
.end method

.method public abstract getSortOrder()I
.end method

.method public abstract hasBeginTime()Z
.end method

.method public abstract hasCategory()Z
.end method

.method public abstract hasCategoryKey()Z
.end method

.method public abstract hasCategorySubkey()Z
.end method

.method public abstract hasEndTime()Z
.end method

.method public abstract hasExternalMarketId()Z
.end method

.method public abstract hasImagePath()Z
.end method

.method public abstract hasLeaseDuration()Z
.end method

.method public abstract hasMarketId()Z
.end method

.method public abstract hasPrice()Z
.end method

.method public abstract hasPriceId()Z
.end method

.method public abstract hasProductDescription()Z
.end method

.method public abstract hasProductId()Z
.end method

.method public abstract hasProductMarketId()Z
.end method

.method public abstract hasProductName()Z
.end method

.method public abstract hasPurchased()Z
.end method

.method public abstract hasSKU()Z
.end method

.method public abstract hasSortOrder()Z
.end method
