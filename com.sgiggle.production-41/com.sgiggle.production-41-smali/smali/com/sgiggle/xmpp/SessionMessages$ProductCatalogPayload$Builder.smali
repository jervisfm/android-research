.class public final Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private alerts_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;"
        }
    .end annotation
.end field

.field private allCached_:Z

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private entry_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;",
            ">;"
        }
    .end annotation
.end field

.field private error_:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

.field private reason_:Ljava/lang/Object;

.field private vmailUpgradeable_:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->entry_:Ljava/util/List;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;->NONE_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->error_:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->reason_:Ljava/lang/Object;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->vmailUpgradeable_:Z

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->alerts_:Ljava/util/List;

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->maybeForceBuilderInitialization()V

    return-void
.end method

.method static synthetic access$101200(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$101300()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 1

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureAlertsIsMutable()V
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->alerts_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->alerts_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private ensureEntryIsMutable()V
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->entry_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->entry_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    return-void
.end method


# virtual methods
.method public addAlerts(ILcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->ensureAlertsIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->alerts_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public addAlerts(ILcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 1

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->ensureAlertsIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->alerts_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public addAlerts(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->ensureAlertsIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->alerts_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addAlerts(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->ensureAlertsIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->alerts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addAllAlerts(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->ensureAlertsIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->alerts_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public addAllEntry(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->ensureEntryIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->entry_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public addEntry(ILcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->ensureEntryIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->entry_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public addEntry(ILcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 1

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->ensureEntryIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->entry_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public addEntry(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->ensureEntryIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->entry_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addEntry(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->ensureEntryIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->entry_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;
    .locals 2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;
    .locals 5

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    or-int/lit8 v2, v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->access$101502(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->entry_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->entry_:Ljava/util/List;

    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x3

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->entry_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->entry_:Ljava/util/List;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->access$101602(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;Ljava/util/List;)Ljava/util/List;

    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x2

    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->error_:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->error_:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->access$101702(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;Lcom/sgiggle/xmpp/SessionMessages$ErrorType;)Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    or-int/lit8 v2, v2, 0x4

    :cond_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->reason_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->reason_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->access$101802(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v1, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    or-int/lit8 v2, v2, 0x8

    :cond_4
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->vmailUpgradeable_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->vmailUpgradeable_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->access$101902(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;Z)Z

    and-int/lit8 v1, v1, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_6

    or-int/lit8 v1, v2, 0x10

    :goto_0
    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->allCached_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->allCached_:Z
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->access$102002(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;Z)Z

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_5

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->alerts_:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->alerts_:Ljava/util/List;

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    and-int/lit8 v2, v2, -0x41

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    :cond_5
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->alerts_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->alerts_:Ljava/util/List;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->access$102102(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;Ljava/util/List;)Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->access$102202(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;I)I

    return-object v0

    :cond_6
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 1

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->entry_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;->NONE_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->error_:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->reason_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->vmailUpgradeable_:Z

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->allCached_:Z

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->alerts_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearAlerts()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->alerts_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearAllCached()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->allCached_:Z

    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearEntry()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->entry_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearError()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;->NONE_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->error_:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    return-object p0
.end method

.method public clearReason()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->getReason()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->reason_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearVmailUpgradeable()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->vmailUpgradeable_:Z

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAlerts(I)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->alerts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    return-object v0
.end method

.method public getAlertsCount()I
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->alerts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getAlertsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->alerts_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getAllCached()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->allCached_:Z

    return v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    move-result-object v0

    return-object v0
.end method

.method public getEntry(I)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->entry_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    return-object v0
.end method

.method public getEntryCount()I
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->entry_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getEntryList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->entry_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getError()Lcom/sgiggle/xmpp/SessionMessages$ErrorType;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->error_:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    return-object v0
.end method

.method public getReason()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->reason_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->reason_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getVmailUpgradeable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->vmailUpgradeable_:Z

    return v0
.end method

.method public hasAllCached()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasError()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasReason()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVmailUpgradeable()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->getEntryCount()I

    move-result v1

    if-ge v0, v1, :cond_3

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->getEntry(I)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    goto :goto_1

    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->addEntry(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->error_:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    goto :goto_0

    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->reason_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->vmailUpgradeable_:Z

    goto :goto_0

    :sswitch_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->allCached_:Z

    goto :goto_0

    :sswitch_7
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->addAlerts(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    :cond_1
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->entry_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->access$101600(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->entry_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->entry_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->access$101600(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->entry_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    :cond_2
    :goto_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->hasError()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->getError()Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->setError(Lcom/sgiggle/xmpp/SessionMessages$ErrorType;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->hasReason()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->getReason()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->setReason(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->hasVmailUpgradeable()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->getVmailUpgradeable()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->setVmailUpgradeable(Z)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    :cond_5
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->hasAllCached()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->getAllCached()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->setAllCached(Z)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    :cond_6
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->alerts_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->access$102100(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->alerts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->alerts_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->access$102100(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->alerts_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    :cond_7
    :goto_2
    move-object v0, p0

    goto :goto_0

    :cond_8
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->ensureEntryIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->entry_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->entry_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->access$101600(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_9
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->ensureAlertsIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->alerts_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->alerts_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->access$102100(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public removeAlerts(I)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->ensureAlertsIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->alerts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    return-object p0
.end method

.method public removeEntry(I)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->ensureEntryIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->entry_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    return-object p0
.end method

.method public setAlerts(ILcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->ensureAlertsIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->alerts_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setAlerts(ILcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 1

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->ensureAlertsIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->alerts_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setAllCached(Z)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->allCached_:Z

    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setEntry(ILcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->ensureEntryIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->entry_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setEntry(ILcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 1

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->ensureEntryIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->entry_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setError(Lcom/sgiggle/xmpp/SessionMessages$ErrorType;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->error_:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    return-object p0
.end method

.method public setReason(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->reason_:Ljava/lang/Object;

    return-object p0
.end method

.method setReason(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->reason_:Ljava/lang/Object;

    return-void
.end method

.method public setVmailUpgradeable(Z)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->vmailUpgradeable_:Z

    return-object p0
.end method
