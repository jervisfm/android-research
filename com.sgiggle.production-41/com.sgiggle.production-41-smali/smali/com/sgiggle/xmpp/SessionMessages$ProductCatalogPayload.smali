.class public final Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProductCatalogPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    }
.end annotation


# static fields
.field public static final ALERTS_FIELD_NUMBER:I = 0x7

.field public static final ALLCACHED_FIELD_NUMBER:I = 0x6

.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final ENTRY_FIELD_NUMBER:I = 0x2

.field public static final ERROR_FIELD_NUMBER:I = 0x3

.field public static final REASON_FIELD_NUMBER:I = 0x4

.field public static final VMAILUPGRADEABLE_FIELD_NUMBER:I = 0x5

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;


# instance fields
.field private alerts_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;"
        }
    .end annotation
.end field

.field private allCached_:Z

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private entry_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;",
            ">;"
        }
    .end annotation
.end field

.field private error_:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private reason_:Ljava/lang/Object;

.field private vmailUpgradeable_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->initFields()V

    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$101502(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$101600(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->entry_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$101602(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->entry_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$101702(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;Lcom/sgiggle/xmpp/SessionMessages$ErrorType;)Lcom/sgiggle/xmpp/SessionMessages$ErrorType;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->error_:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    return-object p1
.end method

.method static synthetic access$101802(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->reason_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$101902(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->vmailUpgradeable_:Z

    return p1
.end method

.method static synthetic access$102002(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->allCached_:Z

    return p1
.end method

.method static synthetic access$102100(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->alerts_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$102102(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->alerts_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$102202(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    return-object v0
.end method

.method private getReasonBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->reason_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->reason_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->entry_:Ljava/util/List;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;->NONE_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->error_:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->reason_:Ljava/lang/Object;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->vmailUpgradeable_:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->allCached_:Z

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->alerts_:Ljava/util/List;

    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 1

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->access$101300()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->access$101200(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->access$101200(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->access$101200(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->access$101200(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->access$101200(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->access$101200(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->access$101200(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->access$101200(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->access$101200(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;->access$101200(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAlerts(I)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->alerts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    return-object v0
.end method

.method public getAlertsCount()I
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->alerts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getAlertsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->alerts_:Ljava/util/List;

    return-object v0
.end method

.method public getAlertsOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlertOrBuilder;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->alerts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlertOrBuilder;

    return-object v0
.end method

.method public getAlertsOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlertOrBuilder;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->alerts_:Ljava/util/List;

    return-object v0
.end method

.method public getAllCached()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->allCached_:Z

    return v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    return-object v0
.end method

.method public getEntry(I)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->entry_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    return-object v0
.end method

.method public getEntryCount()I
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->entry_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getEntryList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->entry_:Ljava/util/List;

    return-object v0
.end method

.method public getEntryOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntryOrBuilder;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->entry_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntryOrBuilder;

    return-object v0
.end method

.method public getEntryOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntryOrBuilder;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->entry_:Ljava/util/List;

    return-object v0
.end method

.method public getError()Lcom/sgiggle/xmpp/SessionMessages$ErrorType;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->error_:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    return-object v0
.end method

.method public getReason()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->reason_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->reason_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_7

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v3

    :goto_1
    move v1, v3

    move v2, v0

    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->entry_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->entry_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_6

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->error_:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;->getNumber()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v0

    add-int/2addr v0, v2

    :goto_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->getReasonBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_3

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->vmailUpgradeable_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_4

    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->allCached_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    move v1, v3

    move v2, v0

    :goto_4
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->alerts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    const/4 v3, 0x7

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->alerts_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_4

    :cond_5
    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->memoizedSerializedSize:I

    move v0, v2

    goto/16 :goto_0

    :cond_6
    move v0, v2

    goto :goto_3

    :cond_7
    move v0, v3

    goto :goto_1
.end method

.method public getVmailUpgradeable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->vmailUpgradeable_:Z

    return v0
.end method

.method public hasAllCached()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasError()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasReason()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVmailUpgradeable()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->memoizedIsInitialized:B

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v2

    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->getEntryCount()I

    move-result v1

    if-ge v0, v1, :cond_5

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->getEntry(I)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_4

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->memoizedIsInitialized:B

    move v0, v3

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;
    .locals 1

    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->getSerializedSize()I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->entry_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->entry_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->error_:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->getReasonBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->vmailUpgradeable_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->allCached_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_5
    move v1, v2

    :goto_1
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->alerts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    const/4 v2, 0x7

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->alerts_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_6
    return-void
.end method
