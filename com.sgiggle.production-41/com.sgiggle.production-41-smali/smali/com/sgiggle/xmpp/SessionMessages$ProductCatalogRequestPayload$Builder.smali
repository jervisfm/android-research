.class public final Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private categoryKey_:Lcom/google/protobuf/LazyStringList;

.field private marketId_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->marketId_:Ljava/util/List;

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->categoryKey_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->maybeForceBuilderInitialization()V

    return-void
.end method

.method static synthetic access$98300(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$98400()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;
    .locals 1

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureCategoryKeyIsMutable()V
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->categoryKey_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->categoryKey_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private ensureMarketIdIsMutable()V
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->marketId_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->marketId_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    return-void
.end method


# virtual methods
.method public addAllCategoryKey(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->ensureCategoryKeyIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->categoryKey_:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public addAllMarketId(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->ensureMarketIdIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->marketId_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public addCategoryKey(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->ensureCategoryKeyIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->categoryKey_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method addCategoryKey(Lcom/google/protobuf/ByteString;)V
    .locals 1

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->ensureCategoryKeyIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->categoryKey_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    return-void
.end method

.method public addMarketId(I)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;
    .locals 2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->ensureMarketIdIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->marketId_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;
    .locals 2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;
    .locals 4

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v1, v1, 0x1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_2

    or-int/lit8 v1, v2, 0x1

    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->access$98602(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->marketId_:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->marketId_:Ljava/util/List;

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    :cond_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->marketId_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->marketId_:Ljava/util/List;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->access$98702(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;Ljava/util/List;)Ljava/util/List;

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    new-instance v2, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->categoryKey_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v2, v3}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->categoryKey_:Lcom/google/protobuf/LazyStringList;

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    :cond_1
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->categoryKey_:Lcom/google/protobuf/LazyStringList;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->categoryKey_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->access$98802(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->access$98902(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;I)I

    return-object v0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;
    .locals 1

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->marketId_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->categoryKey_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearCategoryKey()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;
    .locals 1

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->categoryKey_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearMarketId()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->marketId_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCategoryKey(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->categoryKey_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getCategoryKeyCount()I
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->categoryKey_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public getCategoryKeyList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->categoryKey_:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public getMarketId(I)I
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->marketId_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getMarketIdCount()I
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->marketId_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getMarketIdList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->marketId_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    goto :goto_1

    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    goto :goto_0

    :sswitch_2
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->ensureMarketIdIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->marketId_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readRawVarint32()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->pushLimit(I)I

    move-result v0

    :goto_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->getBytesUntilLimit()I

    move-result v1

    if-lez v1, :cond_2

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->addMarketId(I)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    goto :goto_2

    :cond_2
    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->popLimit(I)V

    goto :goto_0

    :sswitch_4
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->ensureCategoryKeyIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->categoryKey_:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x12 -> :sswitch_3
        0x1a -> :sswitch_4
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    :cond_1
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->marketId_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->access$98700(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->marketId_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->marketId_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->access$98700(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->marketId_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    :cond_2
    :goto_1
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->categoryKey_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->access$98800(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->categoryKey_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->categoryKey_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->access$98800(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->categoryKey_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    :cond_3
    :goto_2
    move-object v0, p0

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->ensureMarketIdIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->marketId_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->marketId_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->access$98700(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_5
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->ensureCategoryKeyIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->categoryKey_:Lcom/google/protobuf/LazyStringList;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->categoryKey_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->access$98800(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setCategoryKey(ILjava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;
    .locals 1

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->ensureCategoryKeyIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->categoryKey_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/LazyStringList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setMarketId(II)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;
    .locals 2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->ensureMarketIdIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->marketId_:Ljava/util/List;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method
