.class public final Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProductCatalogRequestPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CATEGORY_KEY_FIELD_NUMBER:I = 0x3

.field public static final MARKET_ID_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private categoryKey_:Lcom/google/protobuf/LazyStringList;

.field private marketId_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->initFields()V

    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$98602(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$98700(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->marketId_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$98702(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->marketId_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$98800(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;)Lcom/google/protobuf/LazyStringList;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->categoryKey_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic access$98802(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->categoryKey_:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic access$98902(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->marketId_:Ljava/util/List;

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->categoryKey_:Lcom/google/protobuf/LazyStringList;

    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;
    .locals 1

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->access$98400()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->access$98300(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->access$98300(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->access$98300(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->access$98300(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->access$98300(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->access$98300(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->access$98300(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->access$98300(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->access$98300(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;->access$98300(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCategoryKey(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->categoryKey_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getCategoryKeyCount()I
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->categoryKey_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public getCategoryKeyList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->categoryKey_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;

    return-object v0
.end method

.method public getMarketId(I)I
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->marketId_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getMarketIdCount()I
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->marketId_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getMarketIdList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->marketId_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x1

    const/4 v4, 0x0

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_3

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v4

    move v1, v0

    :goto_1
    move v2, v4

    move v3, v4

    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->marketId_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->marketId_:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v0

    add-int/2addr v0, v3

    add-int/lit8 v2, v2, 0x1

    move v3, v0

    goto :goto_2

    :cond_1
    add-int v0, v1, v3

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->getMarketIdList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    move v1, v4

    move v2, v4

    :goto_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->categoryKey_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->categoryKey_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_2
    add-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->getCategoryKeyList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->memoizedSerializedSize:I

    goto :goto_0

    :cond_3
    move v1, v4

    goto :goto_1
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->memoizedIsInitialized:B

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->memoizedIsInitialized:B

    move v0, v3

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;
    .locals 1

    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->getSerializedSize()I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    move v1, v3

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->marketId_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    const/4 v2, 0x2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->marketId_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v3

    :goto_1
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->categoryKey_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogRequestPayload;->categoryKey_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    return-void
.end method
