.class public final Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProductDetailsPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;
    }
.end annotation


# static fields
.field public static final ALLCACHED_FIELD_NUMBER:I = 0x5

.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final PRODUCT_FIELD_NUMBER:I = 0x2

.field public static final SHOWWAND_FIELD_NUMBER:I = 0x3

.field public static final VGOOD_BUNDLE_FIELD_NUMBER:I = 0x4

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;


# instance fields
.field private allCached_:Z

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private product_:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

.field private showWand_:Z

.field private vgoodBundle_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->initFields()V

    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$102602(Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$102702(Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->product_:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    return-object p1
.end method

.method static synthetic access$102802(Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->showWand_:Z

    return p1
.end method

.method static synthetic access$102900(Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->vgoodBundle_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$102902(Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->vgoodBundle_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$103002(Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->allCached_:Z

    return p1
.end method

.method static synthetic access$103102(Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->product_:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->showWand_:Z

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->vgoodBundle_:Ljava/util/List;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->allCached_:Z

    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;
    .locals 1

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->access$102400()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;)Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;)Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->access$102300(Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->access$102300(Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->access$102300(Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->access$102300(Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->access$102300(Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->access$102300(Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->access$102300(Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->access$102300(Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->access$102300(Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;->access$102300(Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAllCached()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->allCached_:Z

    return v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;

    return-object v0
.end method

.method public getProduct()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->product_:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_5

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v3

    :goto_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_1

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->product_:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_2

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->showWand_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    move v1, v3

    move v2, v0

    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v5, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->allCached_:Z

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v0

    add-int/2addr v0, v2

    :goto_3
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->memoizedSerializedSize:I

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    move v0, v3

    goto :goto_1
.end method

.method public getShowWand()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->showWand_:Z

    return v0
.end method

.method public getVgoodBundle(I)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    return-object v0
.end method

.method public getVgoodBundleCount()I
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getVgoodBundleList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->vgoodBundle_:Ljava/util/List;

    return-object v0
.end method

.method public getVgoodBundleOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundleOrBuilder;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundleOrBuilder;

    return-object v0
.end method

.method public getVgoodBundleOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodBundleOrBuilder;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->vgoodBundle_:Ljava/util/List;

    return-object v0
.end method

.method public hasAllCached()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasProduct()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasShowWand()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->memoizedIsInitialized:B

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->hasProduct()Z

    move-result v0

    if-nez v0, :cond_3

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->getProduct()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_5

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_5
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->memoizedIsInitialized:B

    move v0, v3

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;
    .locals 1

    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;)Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->getSerializedSize()I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->product_:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->showWand_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_2
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->allCached_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_4
    return-void
.end method
