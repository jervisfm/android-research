.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ProductDetailsPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getAllCached()Z
.end method

.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getProduct()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
.end method

.method public abstract getShowWand()Z
.end method

.method public abstract getVgoodBundle(I)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
.end method

.method public abstract getVgoodBundleCount()I
.end method

.method public abstract getVgoodBundleList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;",
            ">;"
        }
    .end annotation
.end method

.method public abstract hasAllCached()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasProduct()Z
.end method

.method public abstract hasShowWand()Z
.end method
