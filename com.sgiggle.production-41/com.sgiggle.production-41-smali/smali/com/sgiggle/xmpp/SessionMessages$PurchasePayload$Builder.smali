.class public final Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$PurchasePayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$PurchasePayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private externalMarketId_:Ljava/lang/Object;

.field private isFree_:Z

.field private isrestore_:Z

.field private marketId_:I

.field private price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

.field private productMarketId_:Ljava/lang/Object;

.field private receipt_:Ljava/lang/Object;

.field private signature_:Ljava/lang/Object;

.field private time_:J

.field private transactionId_:Ljava/lang/Object;

.field private type_:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->productMarketId_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->externalMarketId_:Ljava/lang/Object;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;->PURCHASE:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->transactionId_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->receipt_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->signature_:Ljava/lang/Object;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Price;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->maybeForceBuilderInitialization()V

    return-void
.end method

.method static synthetic access$94900(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$95000()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;
    .locals 2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;
    .locals 5

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    or-int/lit8 v2, v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->access$95202(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->productMarketId_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->productMarketId_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->access$95302(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->externalMarketId_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->externalMarketId_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->access$95402(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    or-int/lit8 v2, v2, 0x8

    :cond_3
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->marketId_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->marketId_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->access$95502(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;I)I

    and-int/lit8 v3, v1, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    or-int/lit8 v2, v2, 0x10

    :cond_4
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->access$95602(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    and-int/lit8 v3, v1, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    or-int/lit8 v2, v2, 0x20

    :cond_5
    iget-wide v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->time_:J

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->time_:J
    invoke-static {v0, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->access$95702(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;J)J

    and-int/lit8 v3, v1, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    or-int/lit8 v2, v2, 0x40

    :cond_6
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->transactionId_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->transactionId_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->access$95802(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit16 v3, v1, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    or-int/lit16 v2, v2, 0x80

    :cond_7
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->receipt_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->receipt_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->access$95902(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit16 v3, v1, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_8

    or-int/lit16 v2, v2, 0x100

    :cond_8
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->signature_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->signature_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->access$96002(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit16 v3, v1, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_9

    or-int/lit16 v2, v2, 0x200

    :cond_9
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->access$96102(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;Lcom/sgiggle/xmpp/SessionMessages$Price;)Lcom/sgiggle/xmpp/SessionMessages$Price;

    and-int/lit16 v3, v1, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_a

    or-int/lit16 v2, v2, 0x400

    :cond_a
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->isrestore_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->isrestore_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->access$96202(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;Z)Z

    and-int/lit16 v1, v1, 0x800

    const/16 v3, 0x800

    if-ne v1, v3, :cond_b

    or-int/lit16 v1, v2, 0x800

    :goto_0
    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->isFree_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->isFree_:Z
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->access$96302(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;Z)Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->access$96402(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;I)I

    return-object v0

    :cond_b
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->productMarketId_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->externalMarketId_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->marketId_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;->PURCHASE:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->time_:J

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->transactionId_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->receipt_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->signature_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Price;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->isrestore_:Z

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->isFree_:Z

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearExternalMarketId()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getExternalMarketId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->externalMarketId_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearIsFree()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->isFree_:Z

    return-object p0
.end method

.method public clearIsrestore()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->isrestore_:Z

    return-object p0
.end method

.method public clearMarketId()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->marketId_:I

    return-object p0
.end method

.method public clearPrice()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Price;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearProductMarketId()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getProductMarketId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->productMarketId_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearReceipt()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getReceipt()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->receipt_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearSignature()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getSignature()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->signature_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearTime()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->time_:J

    return-object p0
.end method

.method public clearTransactionId()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getTransactionId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->transactionId_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearType()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;->PURCHASE:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    move-result-object v0

    return-object v0
.end method

.method public getExternalMarketId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->externalMarketId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->externalMarketId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getIsFree()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->isFree_:Z

    return v0
.end method

.method public getIsrestore()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->isrestore_:Z

    return v0
.end method

.method public getMarketId()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->marketId_:I

    return v0
.end method

.method public getPrice()Lcom/sgiggle/xmpp/SessionMessages$Price;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    return-object v0
.end method

.method public getProductMarketId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->productMarketId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->productMarketId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getReceipt()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->receipt_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->receipt_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getSignature()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->signature_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->signature_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getTime()J
    .locals 2

    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->time_:J

    return-wide v0
.end method

.method public getTransactionId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->transactionId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->transactionId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getType()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasExternalMarketId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIsFree()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIsrestore()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMarketId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPrice()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasProductMarketId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasReceipt()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSignature()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTime()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTransactionId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->hasProductMarketId()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->hasExternalMarketId()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->hasMarketId()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->hasType()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->hasTime()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->hasPrice()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->getPrice()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Price;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    goto :goto_1

    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    goto :goto_0

    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->productMarketId_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->externalMarketId_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->marketId_:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    goto :goto_0

    :sswitch_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->time_:J

    goto :goto_0

    :sswitch_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->transactionId_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->receipt_:Ljava/lang/Object;

    goto/16 :goto_0

    :sswitch_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->signature_:Ljava/lang/Object;

    goto/16 :goto_0

    :sswitch_a
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Price;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Price$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->hasPrice()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->getPrice()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Price$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Price;)Lcom/sgiggle/xmpp/SessionMessages$Price$Builder;

    :cond_2
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Price$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setPrice(Lcom/sgiggle/xmpp/SessionMessages$Price;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    goto/16 :goto_0

    :sswitch_b
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->isrestore_:Z

    goto/16 :goto_0

    :sswitch_c
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->isFree_:Z

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->hasProductMarketId()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getProductMarketId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setProductMarketId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->hasExternalMarketId()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getExternalMarketId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setExternalMarketId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->hasMarketId()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getMarketId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setMarketId(I)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->hasType()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getType()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    :cond_5
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->hasTime()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getTime()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setTime(J)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    :cond_6
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->hasTransactionId()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getTransactionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setTransactionId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    :cond_7
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->hasReceipt()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getReceipt()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setReceipt(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    :cond_8
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->hasSignature()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getSignature()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setSignature(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    :cond_9
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->hasPrice()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getPrice()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->mergePrice(Lcom/sgiggle/xmpp/SessionMessages$Price;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    :cond_a
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->hasIsrestore()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getIsrestore()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setIsrestore(Z)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    :cond_b
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->hasIsFree()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getIsFree()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->setIsFree(Z)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    :cond_c
    move-object v0, p0

    goto/16 :goto_0
.end method

.method public mergePrice(Lcom/sgiggle/xmpp/SessionMessages$Price;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Price;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Price;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Price;)Lcom/sgiggle/xmpp/SessionMessages$Price$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Price$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Price;)Lcom/sgiggle/xmpp/SessionMessages$Price$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Price$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setExternalMarketId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->externalMarketId_:Ljava/lang/Object;

    return-object p0
.end method

.method setExternalMarketId(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->externalMarketId_:Ljava/lang/Object;

    return-void
.end method

.method public setIsFree(Z)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->isFree_:Z

    return-object p0
.end method

.method public setIsrestore(Z)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->isrestore_:Z

    return-object p0
.end method

.method public setMarketId(I)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->marketId_:I

    return-object p0
.end method

.method public setPrice(Lcom/sgiggle/xmpp/SessionMessages$Price$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Price$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setPrice(Lcom/sgiggle/xmpp/SessionMessages$Price;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setProductMarketId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->productMarketId_:Ljava/lang/Object;

    return-object p0
.end method

.method setProductMarketId(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->productMarketId_:Ljava/lang/Object;

    return-void
.end method

.method public setReceipt(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->receipt_:Ljava/lang/Object;

    return-object p0
.end method

.method setReceipt(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->receipt_:Ljava/lang/Object;

    return-void
.end method

.method public setSignature(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->signature_:Ljava/lang/Object;

    return-object p0
.end method

.method setSignature(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->signature_:Ljava/lang/Object;

    return-void
.end method

.method public setTime(J)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->time_:J

    return-object p0
.end method

.method public setTransactionId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->transactionId_:Ljava/lang/Object;

    return-object p0
.end method

.method setTransactionId(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->transactionId_:Ljava/lang/Object;

    return-void
.end method

.method public setType(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    return-object p0
.end method
