.class public final enum Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type; = null

.field public static final enum PURCHASE:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type; = null

.field public static final PURCHASE_VALUE:I = 0x0

.field public static final enum REFUND:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type; = null

.field public static final REFUND_VALUE:I = 0x2

.field public static final enum RESTORE:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type; = null

.field public static final RESTORE_VALUE:I = 0x1

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    const-string v1, "PURCHASE"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;->PURCHASE:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    const-string v1, "RESTORE"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;->RESTORE:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    const-string v1, "REFUND"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;->REFUND:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;->PURCHASE:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;->RESTORE:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;->REFUND:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;->value:I

    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;
    .locals 1

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;->PURCHASE:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;->RESTORE:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;->REFUND:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;
    .locals 1

    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;->value:I

    return v0
.end method
