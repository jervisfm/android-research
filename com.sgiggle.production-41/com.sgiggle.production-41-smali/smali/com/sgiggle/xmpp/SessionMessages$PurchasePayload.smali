.class public final Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$PurchasePayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PurchasePayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;,
        Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final EXTERNAL_MARKET_ID_FIELD_NUMBER:I = 0x3

.field public static final ISRESTORE_FIELD_NUMBER:I = 0xb

.field public static final IS_FREE_FIELD_NUMBER:I = 0xc

.field public static final MARKET_ID_FIELD_NUMBER:I = 0x4

.field public static final PRICE_FIELD_NUMBER:I = 0xa

.field public static final PRODUCT_MARKET_ID_FIELD_NUMBER:I = 0x2

.field public static final RECEIPT_FIELD_NUMBER:I = 0x8

.field public static final SIGNATURE_FIELD_NUMBER:I = 0x9

.field public static final TIME_FIELD_NUMBER:I = 0x6

.field public static final TRANSACTION_ID_FIELD_NUMBER:I = 0x7

.field public static final TYPE_FIELD_NUMBER:I = 0x5

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private externalMarketId_:Ljava/lang/Object;

.field private isFree_:Z

.field private isrestore_:Z

.field private marketId_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

.field private productMarketId_:Ljava/lang/Object;

.field private receipt_:Ljava/lang/Object;

.field private signature_:Ljava/lang/Object;

.field private time_:J

.field private transactionId_:Ljava/lang/Object;

.field private type_:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->initFields()V

    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$95202(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$95302(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->productMarketId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$95402(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->externalMarketId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$95502(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->marketId_:I

    return p1
.end method

.method static synthetic access$95602(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    return-object p1
.end method

.method static synthetic access$95702(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;J)J
    .locals 0

    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->time_:J

    return-wide p1
.end method

.method static synthetic access$95802(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->transactionId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$95902(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->receipt_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$96002(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->signature_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$96102(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;Lcom/sgiggle/xmpp/SessionMessages$Price;)Lcom/sgiggle/xmpp/SessionMessages$Price;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    return-object p1
.end method

.method static synthetic access$96202(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->isrestore_:Z

    return p1
.end method

.method static synthetic access$96302(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->isFree_:Z

    return p1
.end method

.method static synthetic access$96402(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    return-object v0
.end method

.method private getExternalMarketIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->externalMarketId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->externalMarketId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getProductMarketIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->productMarketId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->productMarketId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getReceiptBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->receipt_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->receipt_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getSignatureBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->signature_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->signature_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getTransactionIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->transactionId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->transactionId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 3

    const/4 v2, 0x0

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->productMarketId_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->externalMarketId_:Ljava/lang/Object;

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->marketId_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;->PURCHASE:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->time_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->transactionId_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->receipt_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->signature_:Ljava/lang/Object;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Price;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->isrestore_:Z

    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->isFree_:Z

    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->access$95000()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->access$94900(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->access$94900(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->access$94900(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->access$94900(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->access$94900(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->access$94900(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->access$94900(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->access$94900(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->access$94900(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;->access$94900(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;

    return-object v0
.end method

.method public getExternalMarketId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->externalMarketId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->externalMarketId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getIsFree()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->isFree_:Z

    return v0
.end method

.method public getIsrestore()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->isrestore_:Z

    return v0
.end method

.method public getMarketId()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->marketId_:I

    return v0
.end method

.method public getPrice()Lcom/sgiggle/xmpp/SessionMessages$Price;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    return-object v0
.end method

.method public getProductMarketId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->productMarketId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->productMarketId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getReceipt()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->receipt_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->receipt_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 6

    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getProductMarketIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getExternalMarketIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_4

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->marketId_:I

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->time_:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    const/4 v1, 0x7

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getTransactionIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_8

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getReceiptBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_9

    const/16 v1, 0x9

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getSignatureBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_a

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x400

    const/16 v2, 0x400

    if-ne v1, v2, :cond_b

    const/16 v1, 0xb

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->isrestore_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x800

    const/16 v2, 0x800

    if-ne v1, v2, :cond_c

    const/16 v1, 0xc

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->isFree_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->memoizedSerializedSize:I

    goto/16 :goto_0
.end method

.method public getSignature()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->signature_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->signature_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getTime()J
    .locals 2

    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->time_:J

    return-wide v0
.end method

.method public getTransactionId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->transactionId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->transactionId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getType()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasExternalMarketId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIsFree()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIsrestore()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMarketId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPrice()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasProductMarketId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasReceipt()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSignature()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTime()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTransactionId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->memoizedIsInitialized:B

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->hasProductMarketId()Z

    move-result v0

    if-nez v0, :cond_3

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->hasExternalMarketId()Z

    move-result v0

    if-nez v0, :cond_4

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->hasMarketId()Z

    move-result v0

    if-nez v0, :cond_5

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->hasType()Z

    move-result v0

    if-nez v0, :cond_6

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->hasTime()Z

    move-result v0

    if-nez v0, :cond_7

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_8

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_8
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->hasPrice()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getPrice()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Price;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_9

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_9
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->memoizedIsInitialized:B

    move v0, v3

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;
    .locals 1

    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;)Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getSerializedSize()I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getProductMarketIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getExternalMarketIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->marketId_:I

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    const/4 v0, 0x6

    iget-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->time_:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeUInt64(IJ)V

    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    const/4 v0, 0x7

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getTransactionIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getReceiptBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    const/16 v0, 0x9

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->getSignatureBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_9

    const/16 v0, 0xa

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->price_:Lcom/sgiggle/xmpp/SessionMessages$Price;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_a

    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->isrestore_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_b

    const/16 v0, 0xc

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload;->isFree_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_b
    return-void
.end method
