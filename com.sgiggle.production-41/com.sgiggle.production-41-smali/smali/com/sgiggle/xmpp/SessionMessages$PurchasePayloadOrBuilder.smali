.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$PurchasePayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PurchasePayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getExternalMarketId()Ljava/lang/String;
.end method

.method public abstract getIsFree()Z
.end method

.method public abstract getIsrestore()Z
.end method

.method public abstract getMarketId()I
.end method

.method public abstract getPrice()Lcom/sgiggle/xmpp/SessionMessages$Price;
.end method

.method public abstract getProductMarketId()Ljava/lang/String;
.end method

.method public abstract getReceipt()Ljava/lang/String;
.end method

.method public abstract getSignature()Ljava/lang/String;
.end method

.method public abstract getTime()J
.end method

.method public abstract getTransactionId()Ljava/lang/String;
.end method

.method public abstract getType()Lcom/sgiggle/xmpp/SessionMessages$PurchasePayload$Type;
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasExternalMarketId()Z
.end method

.method public abstract hasIsFree()Z
.end method

.method public abstract hasIsrestore()Z
.end method

.method public abstract hasMarketId()Z
.end method

.method public abstract hasPrice()Z
.end method

.method public abstract hasProductMarketId()Z
.end method

.method public abstract hasReceipt()Z
.end method

.method public abstract hasSignature()Z
.end method

.method public abstract hasTime()Z
.end method

.method public abstract hasTransactionId()Z
.end method

.method public abstract hasType()Z
.end method
