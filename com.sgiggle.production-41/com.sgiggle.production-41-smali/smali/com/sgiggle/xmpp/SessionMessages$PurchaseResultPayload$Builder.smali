.class public final Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private error_:I

.field private marketId_:I

.field private priceLabel_:Ljava/lang/Object;

.field private productMarketId_:Ljava/lang/Object;

.field private reason_:Ljava/lang/Object;

.field private recorded_:Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$RecordType;

.field private time_:J


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->productMarketId_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->priceLabel_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->reason_:Ljava/lang/Object;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$RecordType;->SERVER:Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$RecordType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->recorded_:Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$RecordType;

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->maybeForceBuilderInitialization()V

    return-void
.end method

.method static synthetic access$96500(Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$96600()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;
    .locals 1

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;
    .locals 2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;
    .locals 5

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    or-int/lit8 v2, v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->access$96802(Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->productMarketId_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->productMarketId_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->access$96902(Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->marketId_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->marketId_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->access$97002(Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;I)I

    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    or-int/lit8 v2, v2, 0x8

    :cond_3
    iget-wide v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->time_:J

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->time_:J
    invoke-static {v0, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->access$97102(Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;J)J

    and-int/lit8 v3, v1, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    or-int/lit8 v2, v2, 0x10

    :cond_4
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->priceLabel_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->priceLabel_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->access$97202(Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v1, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    or-int/lit8 v2, v2, 0x20

    :cond_5
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->error_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->error_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->access$97302(Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;I)I

    and-int/lit8 v3, v1, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    or-int/lit8 v2, v2, 0x40

    :cond_6
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->reason_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->reason_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->access$97402(Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit16 v1, v1, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_7

    or-int/lit16 v1, v2, 0x80

    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->recorded_:Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$RecordType;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->recorded_:Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$RecordType;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->access$97502(Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$RecordType;)Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$RecordType;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->access$97602(Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;I)I

    return-object v0

    :cond_7
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->productMarketId_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->marketId_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->time_:J

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->priceLabel_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->error_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->reason_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$RecordType;->SERVER:Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$RecordType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->recorded_:Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$RecordType;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearError()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->error_:I

    return-object p0
.end method

.method public clearMarketId()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->marketId_:I

    return-object p0
.end method

.method public clearPriceLabel()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->getPriceLabel()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->priceLabel_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearProductMarketId()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->getProductMarketId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->productMarketId_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearReason()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->getReason()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->reason_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearRecorded()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$RecordType;->SERVER:Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$RecordType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->recorded_:Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$RecordType;

    return-object p0
.end method

.method public clearTime()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->time_:J

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;)Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public getError()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->error_:I

    return v0
.end method

.method public getMarketId()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->marketId_:I

    return v0
.end method

.method public getPriceLabel()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->priceLabel_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->priceLabel_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getProductMarketId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->productMarketId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->productMarketId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getReason()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->reason_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->reason_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getRecorded()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$RecordType;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->recorded_:Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$RecordType;

    return-object v0
.end method

.method public getTime()J
    .locals 2

    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->time_:J

    return-wide v0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasError()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMarketId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPriceLabel()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasProductMarketId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasReason()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRecorded()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTime()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->hasProductMarketId()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;)Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    goto :goto_1

    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;

    goto :goto_0

    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->productMarketId_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->marketId_:I

    goto :goto_0

    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->time_:J

    goto :goto_0

    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->priceLabel_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->error_:I

    goto :goto_0

    :sswitch_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->reason_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$RecordType;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$RecordType;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->recorded_:Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$RecordType;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;)Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;

    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->hasProductMarketId()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->getProductMarketId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->setProductMarketId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->hasMarketId()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->getMarketId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->setMarketId(I)Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->hasTime()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->getTime()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->setTime(J)Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->hasPriceLabel()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->getPriceLabel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->setPriceLabel(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;

    :cond_5
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->hasError()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->getError()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->setError(I)Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;

    :cond_6
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->hasReason()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->getReason()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->setReason(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;

    :cond_7
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->hasRecorded()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->getRecorded()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$RecordType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->setRecorded(Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$RecordType;)Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;

    :cond_8
    move-object v0, p0

    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setError(I)Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->error_:I

    return-object p0
.end method

.method public setMarketId(I)Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->marketId_:I

    return-object p0
.end method

.method public setPriceLabel(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->priceLabel_:Ljava/lang/Object;

    return-object p0
.end method

.method setPriceLabel(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->priceLabel_:Ljava/lang/Object;

    return-void
.end method

.method public setProductMarketId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->productMarketId_:Ljava/lang/Object;

    return-object p0
.end method

.method setProductMarketId(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->productMarketId_:Ljava/lang/Object;

    return-void
.end method

.method public setReason(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->reason_:Ljava/lang/Object;

    return-object p0
.end method

.method setReason(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->reason_:Ljava/lang/Object;

    return-void
.end method

.method public setRecorded(Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$RecordType;)Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->recorded_:Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$RecordType;

    return-object p0
.end method

.method public setTime(J)Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->bitField0_:I

    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$Builder;->time_:J

    return-object p0
.end method
