.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PurchaseResultPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getError()I
.end method

.method public abstract getMarketId()I
.end method

.method public abstract getPriceLabel()Ljava/lang/String;
.end method

.method public abstract getProductMarketId()Ljava/lang/String;
.end method

.method public abstract getReason()Ljava/lang/String;
.end method

.method public abstract getRecorded()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$RecordType;
.end method

.method public abstract getTime()J
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasError()Z
.end method

.method public abstract hasMarketId()Z
.end method

.method public abstract hasPriceLabel()Z
.end method

.method public abstract hasProductMarketId()Z
.end method

.method public abstract hasReason()Z
.end method

.method public abstract hasRecorded()Z
.end method

.method public abstract hasTime()Z
.end method
