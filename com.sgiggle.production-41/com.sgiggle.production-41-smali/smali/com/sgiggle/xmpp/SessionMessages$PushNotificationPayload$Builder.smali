.class public final Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private accountIdFrom_:Ljava/lang/Object;

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private callid_:Ljava/lang/Object;

.field private displaynameFrom_:Ljava/lang/Object;

.field private pushType_:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

.field private sessionid_:Ljava/lang/Object;

.field private swiftServerIp_:Ljava/lang/Object;

.field private swiftTcpPort_:I

.field private swiftUdpPort_:I

.field private timestamp_:I

.field private usernameFrom_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 43825
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 44093
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 44136
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->usernameFrom_:Ljava/lang/Object;

    .line 44172
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->callid_:Ljava/lang/Object;

    .line 44208
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->sessionid_:Ljava/lang/Object;

    .line 44244
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->UNKNOWN_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->pushType_:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    .line 44268
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->swiftServerIp_:Ljava/lang/Object;

    .line 44346
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->displaynameFrom_:Ljava/lang/Object;

    .line 44382
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->accountIdFrom_:Ljava/lang/Object;

    .line 43826
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->maybeForceBuilderInitialization()V

    .line 43827
    return-void
.end method

.method static synthetic access$55500(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 43820
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$55600()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 1

    .prologue
    .line 43820
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 43880
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    move-result-object v0

    .line 43881
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 43882
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 43885
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 1

    .prologue
    .line 43832
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 43830
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 43820
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;
    .locals 2

    .prologue
    .line 43871
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    move-result-object v0

    .line 43872
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 43873
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 43875
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 43820
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;
    .locals 5

    .prologue
    .line 43889
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 43890
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 43891
    const/4 v2, 0x0

    .line 43892
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 43893
    or-int/lit8 v2, v2, 0x1

    .line 43895
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->access$55802(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 43896
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 43897
    or-int/lit8 v2, v2, 0x2

    .line 43899
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->usernameFrom_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->usernameFrom_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->access$55902(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43900
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 43901
    or-int/lit8 v2, v2, 0x4

    .line 43903
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->callid_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->callid_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->access$56002(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43904
    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 43905
    or-int/lit8 v2, v2, 0x8

    .line 43907
    :cond_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->sessionid_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->sessionid_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->access$56102(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43908
    and-int/lit8 v3, v1, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 43909
    or-int/lit8 v2, v2, 0x10

    .line 43911
    :cond_4
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->pushType_:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->pushType_:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->access$56202(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    .line 43912
    and-int/lit8 v3, v1, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 43913
    or-int/lit8 v2, v2, 0x20

    .line 43915
    :cond_5
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->swiftServerIp_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->swiftServerIp_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->access$56302(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43916
    and-int/lit8 v3, v1, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    .line 43917
    or-int/lit8 v2, v2, 0x40

    .line 43919
    :cond_6
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->swiftTcpPort_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->swiftTcpPort_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->access$56402(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;I)I

    .line 43920
    and-int/lit16 v3, v1, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    .line 43921
    or-int/lit16 v2, v2, 0x80

    .line 43923
    :cond_7
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->swiftUdpPort_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->swiftUdpPort_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->access$56502(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;I)I

    .line 43924
    and-int/lit16 v3, v1, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_8

    .line 43925
    or-int/lit16 v2, v2, 0x100

    .line 43927
    :cond_8
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->displaynameFrom_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->displaynameFrom_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->access$56602(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43928
    and-int/lit16 v3, v1, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_9

    .line 43929
    or-int/lit16 v2, v2, 0x200

    .line 43931
    :cond_9
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->accountIdFrom_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->accountIdFrom_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->access$56702(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43932
    and-int/lit16 v1, v1, 0x400

    const/16 v3, 0x400

    if-ne v1, v3, :cond_a

    .line 43933
    or-int/lit16 v1, v2, 0x400

    .line 43935
    :goto_0
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->timestamp_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->timestamp_:I
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->access$56802(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;I)I

    .line 43936
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->access$56902(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;I)I

    .line 43937
    return-object v0

    :cond_a
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 43820
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 43820
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 43836
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 43837
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 43838
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 43839
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->usernameFrom_:Ljava/lang/Object;

    .line 43840
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 43841
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->callid_:Ljava/lang/Object;

    .line 43842
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 43843
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->sessionid_:Ljava/lang/Object;

    .line 43844
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 43845
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->UNKNOWN_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->pushType_:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    .line 43846
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 43847
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->swiftServerIp_:Ljava/lang/Object;

    .line 43848
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 43849
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->swiftTcpPort_:I

    .line 43850
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 43851
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->swiftUdpPort_:I

    .line 43852
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 43853
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->displaynameFrom_:Ljava/lang/Object;

    .line 43854
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 43855
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->accountIdFrom_:Ljava/lang/Object;

    .line 43856
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 43857
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->timestamp_:I

    .line 43858
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 43859
    return-object p0
.end method

.method public clearAccountIdFrom()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 1

    .prologue
    .line 44406
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44407
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getAccountIdFrom()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->accountIdFrom_:Ljava/lang/Object;

    .line 44409
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 1

    .prologue
    .line 44129
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 44131
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44132
    return-object p0
.end method

.method public clearCallid()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 1

    .prologue
    .line 44196
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44197
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getCallid()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->callid_:Ljava/lang/Object;

    .line 44199
    return-object p0
.end method

.method public clearDisplaynameFrom()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 1

    .prologue
    .line 44370
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44371
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getDisplaynameFrom()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->displaynameFrom_:Ljava/lang/Object;

    .line 44373
    return-object p0
.end method

.method public clearPushType()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 1

    .prologue
    .line 44261
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44262
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->UNKNOWN_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->pushType_:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    .line 44264
    return-object p0
.end method

.method public clearSessionid()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 1

    .prologue
    .line 44232
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44233
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getSessionid()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->sessionid_:Ljava/lang/Object;

    .line 44235
    return-object p0
.end method

.method public clearSwiftServerIp()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 1

    .prologue
    .line 44292
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44293
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getSwiftServerIp()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->swiftServerIp_:Ljava/lang/Object;

    .line 44295
    return-object p0
.end method

.method public clearSwiftTcpPort()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 1

    .prologue
    .line 44318
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44319
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->swiftTcpPort_:I

    .line 44321
    return-object p0
.end method

.method public clearSwiftUdpPort()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 1

    .prologue
    .line 44339
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44340
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->swiftUdpPort_:I

    .line 44342
    return-object p0
.end method

.method public clearTimestamp()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 1

    .prologue
    .line 44432
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44433
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->timestamp_:I

    .line 44435
    return-object p0
.end method

.method public clearUsernameFrom()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 1

    .prologue
    .line 44160
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44161
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getUsernameFrom()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->usernameFrom_:Ljava/lang/Object;

    .line 44163
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 43820
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 43820
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 43820
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 2

    .prologue
    .line 43863
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 43820
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAccountIdFrom()Ljava/lang/String;
    .locals 2

    .prologue
    .line 44387
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->accountIdFrom_:Ljava/lang/Object;

    .line 44388
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 44389
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 44390
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->accountIdFrom_:Ljava/lang/Object;

    .line 44393
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 44098
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCallid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 44177
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->callid_:Ljava/lang/Object;

    .line 44178
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 44179
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 44180
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->callid_:Ljava/lang/Object;

    .line 44183
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 43820
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 43820
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;
    .locals 1

    .prologue
    .line 43867
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDisplaynameFrom()Ljava/lang/String;
    .locals 2

    .prologue
    .line 44351
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->displaynameFrom_:Ljava/lang/Object;

    .line 44352
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 44353
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 44354
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->displaynameFrom_:Ljava/lang/Object;

    .line 44357
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getPushType()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;
    .locals 1

    .prologue
    .line 44249
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->pushType_:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    return-object v0
.end method

.method public getSessionid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 44213
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->sessionid_:Ljava/lang/Object;

    .line 44214
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 44215
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 44216
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->sessionid_:Ljava/lang/Object;

    .line 44219
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getSwiftServerIp()Ljava/lang/String;
    .locals 2

    .prologue
    .line 44273
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->swiftServerIp_:Ljava/lang/Object;

    .line 44274
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 44275
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 44276
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->swiftServerIp_:Ljava/lang/Object;

    .line 44279
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getSwiftTcpPort()I
    .locals 1

    .prologue
    .line 44309
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->swiftTcpPort_:I

    return v0
.end method

.method public getSwiftUdpPort()I
    .locals 1

    .prologue
    .line 44330
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->swiftUdpPort_:I

    return v0
.end method

.method public getTimestamp()I
    .locals 1

    .prologue
    .line 44423
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->timestamp_:I

    return v0
.end method

.method public getUsernameFrom()Ljava/lang/String;
    .locals 2

    .prologue
    .line 44141
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->usernameFrom_:Ljava/lang/Object;

    .line 44142
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 44143
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 44144
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->usernameFrom_:Ljava/lang/Object;

    .line 44147
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasAccountIdFrom()Z
    .locals 2

    .prologue
    .line 44384
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 44095
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCallid()Z
    .locals 2

    .prologue
    .line 44174
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDisplaynameFrom()Z
    .locals 2

    .prologue
    .line 44348
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPushType()Z
    .locals 2

    .prologue
    .line 44246
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSessionid()Z
    .locals 2

    .prologue
    .line 44210
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSwiftServerIp()Z
    .locals 2

    .prologue
    .line 44270
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSwiftTcpPort()Z
    .locals 2

    .prologue
    .line 44306
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSwiftUdpPort()Z
    .locals 2

    .prologue
    .line 44327
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTimestamp()Z
    .locals 2

    .prologue
    .line 44420
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUsernameFrom()Z
    .locals 2

    .prologue
    .line 44138
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 43979
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 44003
    :goto_0
    return v0

    .line 43983
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->hasUsernameFrom()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 43985
    goto :goto_0

    .line 43987
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->hasCallid()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 43989
    goto :goto_0

    .line 43991
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->hasSessionid()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 43993
    goto :goto_0

    .line 43995
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->hasPushType()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 43997
    goto :goto_0

    .line 43999
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 44001
    goto :goto_0

    .line 44003
    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 44117
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 44119
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 44125
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44126
    return-object p0

    .line 44122
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43820
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 43820
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43820
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44011
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 44012
    sparse-switch v0, :sswitch_data_0

    .line 44017
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 44019
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 44015
    goto :goto_1

    .line 44024
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 44025
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 44026
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 44028
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 44029
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    goto :goto_0

    .line 44033
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44034
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->usernameFrom_:Ljava/lang/Object;

    goto :goto_0

    .line 44038
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44039
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->callid_:Ljava/lang/Object;

    goto :goto_0

    .line 44043
    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44044
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->sessionid_:Ljava/lang/Object;

    goto :goto_0

    .line 44048
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 44049
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    move-result-object v0

    .line 44050
    if-eqz v0, :cond_0

    .line 44051
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44052
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->pushType_:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    goto :goto_0

    .line 44057
    :sswitch_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44058
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->swiftServerIp_:Ljava/lang/Object;

    goto :goto_0

    .line 44062
    :sswitch_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44063
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->swiftTcpPort_:I

    goto :goto_0

    .line 44067
    :sswitch_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44068
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->swiftUdpPort_:I

    goto/16 :goto_0

    .line 44072
    :sswitch_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44073
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->displaynameFrom_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 44077
    :sswitch_a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44078
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->accountIdFrom_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 44082
    :sswitch_b
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44083
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->timestamp_:I

    goto/16 :goto_0

    .line 44012
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 43941
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 43975
    :goto_0
    return-object v0

    .line 43942
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 43943
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    .line 43945
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->hasUsernameFrom()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 43946
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getUsernameFrom()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->setUsernameFrom(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    .line 43948
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->hasCallid()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 43949
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getCallid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->setCallid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    .line 43951
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->hasSessionid()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 43952
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getSessionid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->setSessionid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    .line 43954
    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->hasPushType()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 43955
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getPushType()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->setPushType(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    .line 43957
    :cond_5
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->hasSwiftServerIp()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 43958
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getSwiftServerIp()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->setSwiftServerIp(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    .line 43960
    :cond_6
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->hasSwiftTcpPort()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 43961
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getSwiftTcpPort()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->setSwiftTcpPort(I)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    .line 43963
    :cond_7
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->hasSwiftUdpPort()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 43964
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getSwiftUdpPort()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->setSwiftUdpPort(I)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    .line 43966
    :cond_8
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->hasDisplaynameFrom()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 43967
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getDisplaynameFrom()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->setDisplaynameFrom(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    .line 43969
    :cond_9
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->hasAccountIdFrom()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 43970
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getAccountIdFrom()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->setAccountIdFrom(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    .line 43972
    :cond_a
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->hasTimestamp()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 43973
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getTimestamp()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->setTimestamp(I)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    :cond_b
    move-object v0, p0

    .line 43975
    goto/16 :goto_0
.end method

.method public setAccountIdFrom(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 44397
    if-nez p1, :cond_0

    .line 44398
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 44400
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44401
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->accountIdFrom_:Ljava/lang/Object;

    .line 44403
    return-object p0
.end method

.method setAccountIdFrom(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 44412
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44413
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->accountIdFrom_:Ljava/lang/Object;

    .line 44415
    return-void
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 44111
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 44113
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44114
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 44101
    if-nez p1, :cond_0

    .line 44102
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 44104
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 44106
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44107
    return-object p0
.end method

.method public setCallid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 44187
    if-nez p1, :cond_0

    .line 44188
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 44190
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44191
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->callid_:Ljava/lang/Object;

    .line 44193
    return-object p0
.end method

.method setCallid(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 44202
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44203
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->callid_:Ljava/lang/Object;

    .line 44205
    return-void
.end method

.method public setDisplaynameFrom(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 44361
    if-nez p1, :cond_0

    .line 44362
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 44364
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44365
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->displaynameFrom_:Ljava/lang/Object;

    .line 44367
    return-object p0
.end method

.method setDisplaynameFrom(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 44376
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44377
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->displaynameFrom_:Ljava/lang/Object;

    .line 44379
    return-void
.end method

.method public setPushType(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 44252
    if-nez p1, :cond_0

    .line 44253
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 44255
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44256
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->pushType_:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    .line 44258
    return-object p0
.end method

.method public setSessionid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 44223
    if-nez p1, :cond_0

    .line 44224
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 44226
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44227
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->sessionid_:Ljava/lang/Object;

    .line 44229
    return-object p0
.end method

.method setSessionid(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 44238
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44239
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->sessionid_:Ljava/lang/Object;

    .line 44241
    return-void
.end method

.method public setSwiftServerIp(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 44283
    if-nez p1, :cond_0

    .line 44284
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 44286
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44287
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->swiftServerIp_:Ljava/lang/Object;

    .line 44289
    return-object p0
.end method

.method setSwiftServerIp(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 44298
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44299
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->swiftServerIp_:Ljava/lang/Object;

    .line 44301
    return-void
.end method

.method public setSwiftTcpPort(I)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 44312
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44313
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->swiftTcpPort_:I

    .line 44315
    return-object p0
.end method

.method public setSwiftUdpPort(I)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 44333
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44334
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->swiftUdpPort_:I

    .line 44336
    return-object p0
.end method

.method public setTimestamp(I)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 44426
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44427
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->timestamp_:I

    .line 44429
    return-object p0
.end method

.method public setUsernameFrom(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 44151
    if-nez p1, :cond_0

    .line 44152
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 44154
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44155
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->usernameFrom_:Ljava/lang/Object;

    .line 44157
    return-object p0
.end method

.method setUsernameFrom(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 44166
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->bitField0_:I

    .line 44167
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->usernameFrom_:Ljava/lang/Object;

    .line 44169
    return-void
.end method
