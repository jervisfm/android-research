.class public final Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PushNotificationPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    }
.end annotation


# static fields
.field public static final ACCOUNT_ID_FROM_FIELD_NUMBER:I = 0xa

.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CALLID_FIELD_NUMBER:I = 0x3

.field public static final DISPLAYNAME_FROM_FIELD_NUMBER:I = 0x9

.field public static final PUSH_TYPE_FIELD_NUMBER:I = 0x5

.field public static final SESSIONID_FIELD_NUMBER:I = 0x4

.field public static final SWIFTSERVERIP_FIELD_NUMBER:I = 0x6

.field public static final SWIFTTCPPORT_FIELD_NUMBER:I = 0x7

.field public static final SWIFTUDPPORT_FIELD_NUMBER:I = 0x8

.field public static final TIMESTAMP_FIELD_NUMBER:I = 0xb

.field public static final USERNAME_FROM_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;


# instance fields
.field private accountIdFrom_:Ljava/lang/Object;

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private callid_:Ljava/lang/Object;

.field private displaynameFrom_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private pushType_:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

.field private sessionid_:Ljava/lang/Object;

.field private swiftServerIp_:Ljava/lang/Object;

.field private swiftTcpPort_:I

.field private swiftUdpPort_:I

.field private timestamp_:I

.field private usernameFrom_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 44442
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    .line 44443
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->initFields()V

    .line 44444
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 43347
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 43616
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->memoizedIsInitialized:B

    .line 43687
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->memoizedSerializedSize:I

    .line 43348
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 43342
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 43349
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 43616
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->memoizedIsInitialized:B

    .line 43687
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->memoizedSerializedSize:I

    .line 43349
    return-void
.end method

.method static synthetic access$55802(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 43342
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$55902(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 43342
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->usernameFrom_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$56002(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 43342
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->callid_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$56102(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 43342
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->sessionid_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$56202(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 43342
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->pushType_:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    return-object p1
.end method

.method static synthetic access$56302(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 43342
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->swiftServerIp_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$56402(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 43342
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->swiftTcpPort_:I

    return p1
.end method

.method static synthetic access$56502(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 43342
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->swiftUdpPort_:I

    return p1
.end method

.method static synthetic access$56602(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 43342
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->displaynameFrom_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$56702(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 43342
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->accountIdFrom_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$56802(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 43342
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->timestamp_:I

    return p1
.end method

.method static synthetic access$56902(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 43342
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    return p1
.end method

.method private getAccountIdFromBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 43582
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->accountIdFrom_:Ljava/lang/Object;

    .line 43583
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 43584
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 43586
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->accountIdFrom_:Ljava/lang/Object;

    .line 43589
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getCallidBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 43424
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->callid_:Ljava/lang/Object;

    .line 43425
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 43426
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 43428
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->callid_:Ljava/lang/Object;

    .line 43431
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;
    .locals 1

    .prologue
    .line 43353
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    return-object v0
.end method

.method private getDisplaynameFromBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 43550
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->displaynameFrom_:Ljava/lang/Object;

    .line 43551
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 43552
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 43554
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->displaynameFrom_:Ljava/lang/Object;

    .line 43557
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getSessionidBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 43456
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->sessionid_:Ljava/lang/Object;

    .line 43457
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 43458
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 43460
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->sessionid_:Ljava/lang/Object;

    .line 43463
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getSwiftServerIpBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 43498
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->swiftServerIp_:Ljava/lang/Object;

    .line 43499
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 43500
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 43502
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->swiftServerIp_:Ljava/lang/Object;

    .line 43505
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getUsernameFromBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 43392
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->usernameFrom_:Ljava/lang/Object;

    .line 43393
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 43394
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 43396
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->usernameFrom_:Ljava/lang/Object;

    .line 43399
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 43604
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 43605
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->usernameFrom_:Ljava/lang/Object;

    .line 43606
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->callid_:Ljava/lang/Object;

    .line 43607
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->sessionid_:Ljava/lang/Object;

    .line 43608
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->UNKNOWN_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->pushType_:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    .line 43609
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->swiftServerIp_:Ljava/lang/Object;

    .line 43610
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->swiftTcpPort_:I

    .line 43611
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->swiftUdpPort_:I

    .line 43612
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->displaynameFrom_:Ljava/lang/Object;

    .line 43613
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->accountIdFrom_:Ljava/lang/Object;

    .line 43614
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->timestamp_:I

    .line 43615
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 1

    .prologue
    .line 43813
    #calls: Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->access$55600()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 43816
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43782
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    .line 43783
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 43784
    #calls: Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->access$55500(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    move-result-object v0

    .line 43786
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43793
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    .line 43794
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 43795
    #calls: Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->access$55500(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    move-result-object v0

    .line 43797
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 43749
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->access$55500(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 43755
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->access$55500(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43803
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->access$55500(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43809
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->access$55500(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43771
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->access$55500(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43777
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->access$55500(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 43760
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->access$55500(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 43766
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;->access$55500(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAccountIdFrom()Ljava/lang/String;
    .locals 2

    .prologue
    .line 43568
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->accountIdFrom_:Ljava/lang/Object;

    .line 43569
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 43570
    check-cast v0, Ljava/lang/String;

    .line 43578
    :goto_0
    return-object v0

    .line 43572
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 43574
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 43575
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 43576
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->accountIdFrom_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 43578
    goto :goto_0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 43368
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCallid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 43410
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->callid_:Ljava/lang/Object;

    .line 43411
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 43412
    check-cast v0, Ljava/lang/String;

    .line 43420
    :goto_0
    return-object v0

    .line 43414
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 43416
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 43417
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 43418
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->callid_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 43420
    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 43342
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;
    .locals 1

    .prologue
    .line 43357
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;

    return-object v0
.end method

.method public getDisplaynameFrom()Ljava/lang/String;
    .locals 2

    .prologue
    .line 43536
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->displaynameFrom_:Ljava/lang/Object;

    .line 43537
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 43538
    check-cast v0, Ljava/lang/String;

    .line 43546
    :goto_0
    return-object v0

    .line 43540
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 43542
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 43543
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 43544
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->displaynameFrom_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 43546
    goto :goto_0
.end method

.method public getPushType()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;
    .locals 1

    .prologue
    .line 43474
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->pushType_:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 43689
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->memoizedSerializedSize:I

    .line 43690
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 43738
    :goto_0
    return v0

    .line 43692
    :cond_0
    const/4 v0, 0x0

    .line 43693
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 43694
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 43697
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 43698
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getUsernameFromBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 43701
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 43702
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getCallidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 43705
    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_4

    .line 43706
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getSessionidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 43709
    :cond_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 43710
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->pushType_:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 43713
    :cond_5
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    .line 43714
    const/4 v1, 0x6

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getSwiftServerIpBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 43717
    :cond_6
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    .line 43718
    const/4 v1, 0x7

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->swiftTcpPort_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 43721
    :cond_7
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_8

    .line 43722
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->swiftUdpPort_:I

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 43725
    :cond_8
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_9

    .line 43726
    const/16 v1, 0x9

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getDisplaynameFromBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 43729
    :cond_9
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_a

    .line 43730
    const/16 v1, 0xa

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getAccountIdFromBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 43733
    :cond_a
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x400

    const/16 v2, 0x400

    if-ne v1, v2, :cond_b

    .line 43734
    const/16 v1, 0xb

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->timestamp_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 43737
    :cond_b
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->memoizedSerializedSize:I

    goto/16 :goto_0
.end method

.method public getSessionid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 43442
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->sessionid_:Ljava/lang/Object;

    .line 43443
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 43444
    check-cast v0, Ljava/lang/String;

    .line 43452
    :goto_0
    return-object v0

    .line 43446
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 43448
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 43449
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 43450
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->sessionid_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 43452
    goto :goto_0
.end method

.method public getSwiftServerIp()Ljava/lang/String;
    .locals 2

    .prologue
    .line 43484
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->swiftServerIp_:Ljava/lang/Object;

    .line 43485
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 43486
    check-cast v0, Ljava/lang/String;

    .line 43494
    :goto_0
    return-object v0

    .line 43488
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 43490
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 43491
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 43492
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->swiftServerIp_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 43494
    goto :goto_0
.end method

.method public getSwiftTcpPort()I
    .locals 1

    .prologue
    .line 43516
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->swiftTcpPort_:I

    return v0
.end method

.method public getSwiftUdpPort()I
    .locals 1

    .prologue
    .line 43526
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->swiftUdpPort_:I

    return v0
.end method

.method public getTimestamp()I
    .locals 1

    .prologue
    .line 43600
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->timestamp_:I

    return v0
.end method

.method public getUsernameFrom()Ljava/lang/String;
    .locals 2

    .prologue
    .line 43378
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->usernameFrom_:Ljava/lang/Object;

    .line 43379
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 43380
    check-cast v0, Ljava/lang/String;

    .line 43388
    :goto_0
    return-object v0

    .line 43382
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 43384
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 43385
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 43386
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->usernameFrom_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 43388
    goto :goto_0
.end method

.method public hasAccountIdFrom()Z
    .locals 2

    .prologue
    .line 43565
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 43365
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCallid()Z
    .locals 2

    .prologue
    .line 43407
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDisplaynameFrom()Z
    .locals 2

    .prologue
    .line 43533
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPushType()Z
    .locals 2

    .prologue
    .line 43471
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSessionid()Z
    .locals 2

    .prologue
    .line 43439
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSwiftServerIp()Z
    .locals 2

    .prologue
    .line 43481
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSwiftTcpPort()Z
    .locals 2

    .prologue
    .line 43513
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSwiftUdpPort()Z
    .locals 2

    .prologue
    .line 43523
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTimestamp()Z
    .locals 2

    .prologue
    .line 43597
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUsernameFrom()Z
    .locals 2

    .prologue
    .line 43375
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 43618
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->memoizedIsInitialized:B

    .line 43619
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 43646
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 43619
    goto :goto_0

    .line 43621
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 43622
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 43623
    goto :goto_0

    .line 43625
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->hasUsernameFrom()Z

    move-result v0

    if-nez v0, :cond_3

    .line 43626
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 43627
    goto :goto_0

    .line 43629
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->hasCallid()Z

    move-result v0

    if-nez v0, :cond_4

    .line 43630
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 43631
    goto :goto_0

    .line 43633
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->hasSessionid()Z

    move-result v0

    if-nez v0, :cond_5

    .line 43634
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 43635
    goto :goto_0

    .line 43637
    :cond_5
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->hasPushType()Z

    move-result v0

    if-nez v0, :cond_6

    .line 43638
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 43639
    goto :goto_0

    .line 43641
    :cond_6
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_7

    .line 43642
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 43643
    goto :goto_0

    .line 43645
    :cond_7
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 43646
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 43342
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 1

    .prologue
    .line 43814
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 43342
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;
    .locals 1

    .prologue
    .line 43818
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 43743
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 43651
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getSerializedSize()I

    .line 43652
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 43653
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 43655
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 43656
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getUsernameFromBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 43658
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 43659
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getCallidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 43661
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 43662
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getSessionidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 43664
    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 43665
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->pushType_:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 43667
    :cond_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 43668
    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getSwiftServerIpBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 43670
    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 43671
    const/4 v0, 0x7

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->swiftTcpPort_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 43673
    :cond_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 43674
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->swiftUdpPort_:I

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 43676
    :cond_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    .line 43677
    const/16 v0, 0x9

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getDisplaynameFromBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 43679
    :cond_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_9

    .line 43680
    const/16 v0, 0xa

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->getAccountIdFromBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 43682
    :cond_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_a

    .line 43683
    const/16 v0, 0xb

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayload;->timestamp_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 43685
    :cond_a
    return-void
.end method
