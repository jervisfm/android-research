.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$PushNotificationPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PushNotificationPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getAccountIdFrom()Ljava/lang/String;
.end method

.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getCallid()Ljava/lang/String;
.end method

.method public abstract getDisplaynameFrom()Ljava/lang/String;
.end method

.method public abstract getPushType()Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;
.end method

.method public abstract getSessionid()Ljava/lang/String;
.end method

.method public abstract getSwiftServerIp()Ljava/lang/String;
.end method

.method public abstract getSwiftTcpPort()I
.end method

.method public abstract getSwiftUdpPort()I
.end method

.method public abstract getTimestamp()I
.end method

.method public abstract getUsernameFrom()Ljava/lang/String;
.end method

.method public abstract hasAccountIdFrom()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasCallid()Z
.end method

.method public abstract hasDisplaynameFrom()Z
.end method

.method public abstract hasPushType()Z
.end method

.method public abstract hasSessionid()Z
.end method

.method public abstract hasSwiftServerIp()Z
.end method

.method public abstract hasSwiftTcpPort()Z
.end method

.method public abstract hasSwiftUdpPort()Z
.end method

.method public abstract hasTimestamp()Z
.end method

.method public abstract hasUsernameFrom()Z
.end method
