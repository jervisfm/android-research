.class public final enum Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PushNotificationType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType; = null

.field public static final enum ANDROID_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType; = null

.field public static final ANDROID_PUSH_VALUE:I = 0x2

.field public static final enum FAST_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType; = null

.field public static final FAST_PUSH_VALUE:I = 0x4

.field public static final enum IPHONE_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType; = null

.field public static final IPHONE_PUSH_VALUE:I = 0x1

.field public static final enum TANGO_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType; = null

.field public static final TANGO_PUSH_VALUE:I = 0x0

.field public static final enum UNKNOWN_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType; = null

.field public static final UNKNOWN_PUSH_VALUE:I = -0x1

.field public static final enum WINPHONE_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType; = null

.field public static final WINPHONE_PUSH_VALUE:I = 0x3

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 13
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    const-string v1, "UNKNOWN_PUSH"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v4, v2}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->UNKNOWN_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    .line 14
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    const-string v1, "TANGO_PUSH"

    invoke-direct {v0, v1, v5, v5, v4}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->TANGO_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    .line 15
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    const-string v1, "IPHONE_PUSH"

    invoke-direct {v0, v1, v6, v6, v5}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->IPHONE_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    .line 16
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    const-string v1, "ANDROID_PUSH"

    invoke-direct {v0, v1, v7, v7, v6}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->ANDROID_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    .line 17
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    const-string v1, "WINPHONE_PUSH"

    invoke-direct {v0, v1, v8, v8, v7}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->WINPHONE_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    .line 18
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    const-string v1, "FAST_PUSH"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3, v8}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->FAST_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    .line 11
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->UNKNOWN_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->TANGO_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->IPHONE_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->ANDROID_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->WINPHONE_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->FAST_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    .line 48
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 58
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->value:I

    .line 59
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;
    .locals 1
    .parameter

    .prologue
    .line 32
    packed-switch p0, :pswitch_data_0

    .line 39
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 33
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->UNKNOWN_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    goto :goto_0

    .line 34
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->TANGO_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    goto :goto_0

    .line 35
    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->IPHONE_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    goto :goto_0

    .line 36
    :pswitch_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->ANDROID_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    goto :goto_0

    .line 37
    :pswitch_4
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->WINPHONE_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    goto :goto_0

    .line 38
    :pswitch_5
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->FAST_PUSH:Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    goto :goto_0

    .line 32
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;
    .locals 1
    .parameter

    .prologue
    .line 11
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PushNotificationType;->value:I

    return v0
.end method
