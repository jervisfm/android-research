.class public final Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private doLogin_:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 39029
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 39155
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 39198
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->doLogin_:Z

    .line 39030
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->maybeForceBuilderInitialization()V

    .line 39031
    return-void
.end method

.method static synthetic access$49700(Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 39024
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$49800()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;
    .locals 1

    .prologue
    .line 39024
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 39066
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;

    move-result-object v0

    .line 39067
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 39068
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 39071
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;
    .locals 1

    .prologue
    .line 39036
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 39034
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 39024
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;
    .locals 2

    .prologue
    .line 39057
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;

    move-result-object v0

    .line 39058
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 39059
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 39061
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 39024
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;
    .locals 5

    .prologue
    .line 39075
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 39076
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->bitField0_:I

    .line 39077
    const/4 v2, 0x0

    .line 39078
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 39079
    or-int/lit8 v2, v2, 0x1

    .line 39081
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->access$50002(Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 39082
    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    .line 39083
    or-int/lit8 v1, v2, 0x2

    .line 39085
    :goto_0
    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->doLogin_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->doLogin_:Z
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->access$50102(Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;Z)Z

    .line 39086
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->access$50202(Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;I)I

    .line 39087
    return-object v0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 39024
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 39024
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;
    .locals 1

    .prologue
    .line 39040
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 39041
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 39042
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->bitField0_:I

    .line 39043
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->doLogin_:Z

    .line 39044
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->bitField0_:I

    .line 39045
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;
    .locals 1

    .prologue
    .line 39191
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 39193
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->bitField0_:I

    .line 39194
    return-object p0
.end method

.method public clearDoLogin()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;
    .locals 1

    .prologue
    .line 39212
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->bitField0_:I

    .line 39213
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->doLogin_:Z

    .line 39215
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 39024
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 39024
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 39024
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;
    .locals 2

    .prologue
    .line 39049
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 39024
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 39160
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 39024
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 39024
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;
    .locals 1

    .prologue
    .line 39053
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDoLogin()Z
    .locals 1

    .prologue
    .line 39203
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->doLogin_:Z

    return v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 39157
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDoLogin()Z
    .locals 2

    .prologue
    .line 39200
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 39102
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 39114
    :goto_0
    return v0

    .line 39106
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->hasDoLogin()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 39108
    goto :goto_0

    .line 39110
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 39112
    goto :goto_0

    .line 39114
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 39179
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 39181
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 39187
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->bitField0_:I

    .line 39188
    return-object p0

    .line 39184
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39024
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 39024
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39024
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39122
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 39123
    sparse-switch v0, :sswitch_data_0

    .line 39128
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 39130
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 39126
    goto :goto_1

    .line 39135
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 39136
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 39137
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 39139
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 39140
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    goto :goto_0

    .line 39144
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->bitField0_:I

    .line 39145
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->doLogin_:Z

    goto :goto_0

    .line 39123
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 39091
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 39098
    :goto_0
    return-object v0

    .line 39092
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 39093
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    .line 39095
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->hasDoLogin()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 39096
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->getDoLogin()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->setDoLogin(Z)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    :cond_2
    move-object v0, p0

    .line 39098
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 39173
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 39175
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->bitField0_:I

    .line 39176
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 39163
    if-nez p1, :cond_0

    .line 39164
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 39166
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 39168
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->bitField0_:I

    .line 39169
    return-object p0
.end method

.method public setDoLogin(Z)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 39206
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->bitField0_:I

    .line 39207
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->doLogin_:Z

    .line 39209
    return-object p0
.end method
