.class public final Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PutAppInForegroundPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final DO_LOGIN_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private doLogin_:Z

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39222
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;

    .line 39223
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->initFields()V

    .line 39224
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 38857
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 38895
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->memoizedIsInitialized:B

    .line 38927
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->memoizedSerializedSize:I

    .line 38858
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 38852
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 38859
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 38895
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->memoizedIsInitialized:B

    .line 38927
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->memoizedSerializedSize:I

    .line 38859
    return-void
.end method

.method static synthetic access$50002(Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 38852
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$50102(Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 38852
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->doLogin_:Z

    return p1
.end method

.method static synthetic access$50202(Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 38852
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;
    .locals 1

    .prologue
    .line 38863
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 38892
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 38893
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->doLogin_:Z

    .line 38894
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;
    .locals 1

    .prologue
    .line 39017
    #calls: Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->access$49800()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 39020
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38986
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    .line 38987
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 38988
    #calls: Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->access$49700(Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;

    move-result-object v0

    .line 38990
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38997
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    .line 38998
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 38999
    #calls: Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->access$49700(Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;

    move-result-object v0

    .line 39001
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 38953
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->access$49700(Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 38959
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->access$49700(Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39007
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->access$49700(Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39013
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->access$49700(Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38975
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->access$49700(Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38981
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->access$49700(Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 38964
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->access$49700(Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 38970
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;->access$49700(Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 38878
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 38852
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;
    .locals 1

    .prologue
    .line 38867
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;

    return-object v0
.end method

.method public getDoLogin()Z
    .locals 1

    .prologue
    .line 38888
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->doLogin_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 38929
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->memoizedSerializedSize:I

    .line 38930
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 38942
    :goto_0
    return v0

    .line 38932
    :cond_0
    const/4 v0, 0x0

    .line 38933
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 38934
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 38937
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 38938
    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->doLogin_:Z

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 38941
    :cond_2
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 38875
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDoLogin()Z
    .locals 2

    .prologue
    .line 38885
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 38897
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->memoizedIsInitialized:B

    .line 38898
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 38913
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 38898
    goto :goto_0

    .line 38900
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 38901
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 38902
    goto :goto_0

    .line 38904
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->hasDoLogin()Z

    move-result v0

    if-nez v0, :cond_3

    .line 38905
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 38906
    goto :goto_0

    .line 38908
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    .line 38909
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 38910
    goto :goto_0

    .line 38912
    :cond_4
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 38913
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 38852
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;
    .locals 1

    .prologue
    .line 39018
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 38852
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;
    .locals 1

    .prologue
    .line 39022
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;)Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 38947
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 38918
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->getSerializedSize()I

    .line 38919
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 38920
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 38922
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 38923
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$PutAppInForegroundPayload;->doLogin_:Z

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 38925
    :cond_1
    return-void
.end method
