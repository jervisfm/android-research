.class public final Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessageOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;",
        "Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessageOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private imageUrl_:Ljava/lang/Object;

.field private message_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;",
            ">;"
        }
    .end annotation
.end field

.field private text_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->text_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->imageUrl_:Ljava/lang/Object;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->message_:Ljava/util/List;

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->maybeForceBuilderInitialization()V

    return-void
.end method

.method static synthetic access$127800(Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$127900()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;
    .locals 1

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;-><init>()V

    return-object v0
.end method

.method private ensureMessageIsMutable()V
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->message_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->message_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    return-void
.end method


# virtual methods
.method public addAllMessage(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->ensureMessageIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->message_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public addMessage(ILcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;
    .locals 2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->ensureMessageIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->message_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public addMessage(ILcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;
    .locals 1

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->ensureMessageIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->message_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public addMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;
    .locals 2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->ensureMessageIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->message_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->ensureMessageIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->message_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;
    .locals 2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;
    .locals 5

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    or-int/lit8 v2, v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->text_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;->text_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;->access$128102(Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_2

    or-int/lit8 v1, v2, 0x2

    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->imageUrl_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;->imageUrl_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;->access$128202(Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;Ljava/lang/Object;)Ljava/lang/Object;

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->message_:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->message_:Ljava/util/List;

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    :cond_1
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->message_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;->message_:Ljava/util/List;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;->access$128302(Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;Ljava/util/List;)Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;->access$128402(Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;I)I

    return-object v0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;
    .locals 1

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->text_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->imageUrl_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->message_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearImageUrl()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->imageUrl_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearMessage()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->message_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearText()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;->getText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->text_:Ljava/lang/Object;

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;)Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;

    move-result-object v0

    return-object v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->imageUrl_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->imageUrl_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getMessage(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->message_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    return-object v0
.end method

.method public getMessageCount()I
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->message_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getMessageList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->message_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->text_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->text_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasImageUrl()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasText()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->hasText()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    move v0, v2

    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->getMessageCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->getMessage(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;)Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    goto :goto_1

    :sswitch_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->text_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->imageUrl_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_3
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->addMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;)Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;->hasText()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->setText(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;

    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;->hasImageUrl()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->setImageUrl(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;

    :cond_2
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;->message_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;->access$128300(Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->message_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;->message_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;->access$128300(Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->message_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    :cond_3
    :goto_1
    move-object v0, p0

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->ensureMessageIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->message_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;->message_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;->access$128300(Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public removeMessage(I)Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->ensureMessageIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->message_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    return-object p0
.end method

.method public setImageUrl(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->imageUrl_:Ljava/lang/Object;

    return-object p0
.end method

.method setImageUrl(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->imageUrl_:Ljava/lang/Object;

    return-void
.end method

.method public setMessage(ILcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;
    .locals 2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->ensureMessageIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->message_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setMessage(ILcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;
    .locals 1

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->ensureMessageIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->message_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setText(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->text_:Ljava/lang/Object;

    return-object p0
.end method

.method setText(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessage$Builder;->text_:Ljava/lang/Object;

    return-void
.end method
