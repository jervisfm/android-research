.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$QuickReplyMessageOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "QuickReplyMessageOrBuilder"
.end annotation


# virtual methods
.method public abstract getImageUrl()Ljava/lang/String;
.end method

.method public abstract getMessage(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
.end method

.method public abstract getMessageCount()I
.end method

.method public abstract getMessageList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getText()Ljava/lang/String;
.end method

.method public abstract hasImageUrl()Z
.end method

.method public abstract hasText()Z
.end method
