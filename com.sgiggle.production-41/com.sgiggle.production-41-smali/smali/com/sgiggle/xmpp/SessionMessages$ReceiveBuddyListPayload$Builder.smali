.class public final Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private buddies_:Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 7309
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 7443
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 7486
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->buddies_:Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    .line 7310
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->maybeForceBuilderInitialization()V

    .line 7311
    return-void
.end method

.method static synthetic access$8200(Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 7304
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$8300()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;
    .locals 1

    .prologue
    .line 7304
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 7346
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;

    move-result-object v0

    .line 7347
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 7348
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 7351
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;
    .locals 1

    .prologue
    .line 7316
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 7314
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 7304
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;
    .locals 2

    .prologue
    .line 7337
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;

    move-result-object v0

    .line 7338
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 7339
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 7341
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 7304
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;
    .locals 5

    .prologue
    .line 7355
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 7356
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->bitField0_:I

    .line 7357
    const/4 v2, 0x0

    .line 7358
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 7359
    or-int/lit8 v2, v2, 0x1

    .line 7361
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->access$8502(Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 7362
    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    .line 7363
    or-int/lit8 v1, v2, 0x2

    .line 7365
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->buddies_:Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->buddies_:Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->access$8602(Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    .line 7366
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->access$8702(Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;I)I

    .line 7367
    return-object v0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 7304
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 7304
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;
    .locals 1

    .prologue
    .line 7320
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 7321
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 7322
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->bitField0_:I

    .line 7323
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->buddies_:Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    .line 7324
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->bitField0_:I

    .line 7325
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;
    .locals 1

    .prologue
    .line 7479
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 7481
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->bitField0_:I

    .line 7482
    return-object p0
.end method

.method public clearBuddies()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;
    .locals 1

    .prologue
    .line 7522
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->buddies_:Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    .line 7524
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->bitField0_:I

    .line 7525
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 7304
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 7304
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 7304
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;
    .locals 2

    .prologue
    .line 7329
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 7304
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 7448
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getBuddies()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    .locals 1

    .prologue
    .line 7491
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->buddies_:Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 7304
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 7304
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;
    .locals 1

    .prologue
    .line 7333
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;

    move-result-object v0

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 7445
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBuddies()Z
    .locals 2

    .prologue
    .line 7488
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 7382
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 7398
    :goto_0
    return v0

    .line 7386
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->hasBuddies()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 7388
    goto :goto_0

    .line 7390
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 7392
    goto :goto_0

    .line 7394
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->getBuddies()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 7396
    goto :goto_0

    .line 7398
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 7467
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 7469
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 7475
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->bitField0_:I

    .line 7476
    return-object p0

    .line 7472
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public mergeBuddies(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 7510
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->buddies_:Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 7512
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->buddies_:Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->buddies_:Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    .line 7518
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->bitField0_:I

    .line 7519
    return-object p0

    .line 7515
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->buddies_:Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7304
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 7304
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7304
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7406
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 7407
    sparse-switch v0, :sswitch_data_0

    .line 7412
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 7414
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 7410
    goto :goto_1

    .line 7419
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 7420
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 7421
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 7423
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 7424
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    goto :goto_0

    .line 7428
    :sswitch_2
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    .line 7429
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->hasBuddies()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 7430
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->getBuddies()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    .line 7432
    :cond_2
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 7433
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->setBuddies(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    goto :goto_0

    .line 7407
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 7371
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 7378
    :goto_0
    return-object v0

    .line 7372
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 7373
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    .line 7375
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->hasBuddies()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 7376
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->getBuddies()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->mergeBuddies(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    :cond_2
    move-object v0, p0

    .line 7378
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 7461
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 7463
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->bitField0_:I

    .line 7464
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 7451
    if-nez p1, :cond_0

    .line 7452
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7454
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 7456
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->bitField0_:I

    .line 7457
    return-object p0
.end method

.method public setBuddies(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 7504
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->buddies_:Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    .line 7506
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->bitField0_:I

    .line 7507
    return-object p0
.end method

.method public setBuddies(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 7494
    if-nez p1, :cond_0

    .line 7495
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7497
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->buddies_:Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    .line 7499
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->bitField0_:I

    .line 7500
    return-object p0
.end method
