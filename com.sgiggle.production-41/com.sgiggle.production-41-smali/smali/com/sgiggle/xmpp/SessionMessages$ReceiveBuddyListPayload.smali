.class public final Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReceiveBuddyListPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final BUDDIES_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private buddies_:Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 7532
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;

    .line 7533
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->initFields()V

    .line 7534
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 7133
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 7171
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->memoizedIsInitialized:B

    .line 7207
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->memoizedSerializedSize:I

    .line 7134
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 7128
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 7135
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 7171
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->memoizedIsInitialized:B

    .line 7207
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->memoizedSerializedSize:I

    .line 7135
    return-void
.end method

.method static synthetic access$8502(Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 7128
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$8602(Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 7128
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->buddies_:Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    return-object p1
.end method

.method static synthetic access$8702(Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 7128
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;
    .locals 1

    .prologue
    .line 7139
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 7168
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 7169
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->buddies_:Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    .line 7170
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;
    .locals 1

    .prologue
    .line 7297
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->access$8300()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 7300
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7266
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    move-result-object v0

    .line 7267
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7268
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->access$8200(Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;

    move-result-object v0

    .line 7270
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7277
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    move-result-object v0

    .line 7278
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7279
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->access$8200(Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;

    move-result-object v0

    .line 7281
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 7233
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->access$8200(Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 7239
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->access$8200(Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7287
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->access$8200(Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7293
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->access$8200(Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7255
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->access$8200(Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7261
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->access$8200(Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 7244
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->access$8200(Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 7250
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;->access$8200(Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 7154
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getBuddies()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    .locals 1

    .prologue
    .line 7164
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->buddies_:Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 7128
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;
    .locals 1

    .prologue
    .line 7143
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 7209
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->memoizedSerializedSize:I

    .line 7210
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 7222
    :goto_0
    return v0

    .line 7212
    :cond_0
    const/4 v0, 0x0

    .line 7213
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 7214
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7217
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 7218
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->buddies_:Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7221
    :cond_2
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 7151
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBuddies()Z
    .locals 2

    .prologue
    .line 7161
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 7173
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->memoizedIsInitialized:B

    .line 7174
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 7193
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 7174
    goto :goto_0

    .line 7176
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 7177
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 7178
    goto :goto_0

    .line 7180
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->hasBuddies()Z

    move-result v0

    if-nez v0, :cond_3

    .line 7181
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 7182
    goto :goto_0

    .line 7184
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    .line 7185
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 7186
    goto :goto_0

    .line 7188
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->getBuddies()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_5

    .line 7189
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 7190
    goto :goto_0

    .line 7192
    :cond_5
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 7193
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 7128
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;
    .locals 1

    .prologue
    .line 7298
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 7128
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;
    .locals 1

    .prologue
    .line 7302
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;)Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 7227
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 7198
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->getSerializedSize()I

    .line 7199
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 7200
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 7202
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 7203
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReceiveBuddyListPayload;->buddies_:Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 7205
    :cond_1
    return-void
.end method
