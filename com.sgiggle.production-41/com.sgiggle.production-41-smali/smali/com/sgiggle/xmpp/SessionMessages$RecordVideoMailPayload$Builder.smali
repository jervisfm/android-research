.class public final Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private callees_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private maxRecordingDuration_:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 59190
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 59341
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 59384
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->callees_:Ljava/util/List;

    .line 59191
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->maybeForceBuilderInitialization()V

    .line 59192
    return-void
.end method

.method static synthetic access$76200(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 59185
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$76300()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 59185
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 59229
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    move-result-object v0

    .line 59230
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 59231
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 59234
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 59197
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureCalleesIsMutable()V
    .locals 2

    .prologue
    .line 59387
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 59388
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->callees_:Ljava/util/List;

    .line 59389
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    .line 59391
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 59195
    return-void
.end method


# virtual methods
.method public addAllCallees(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;"
        }
    .end annotation

    .prologue
    .line 59454
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->ensureCalleesIsMutable()V

    .line 59455
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 59457
    return-object p0
.end method

.method public addCallees(ILcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 59447
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->ensureCalleesIsMutable()V

    .line 59448
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 59450
    return-object p0
.end method

.method public addCallees(ILcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 59430
    if-nez p2, :cond_0

    .line 59431
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 59433
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->ensureCalleesIsMutable()V

    .line 59434
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 59436
    return-object p0
.end method

.method public addCallees(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 59440
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->ensureCalleesIsMutable()V

    .line 59441
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59443
    return-object p0
.end method

.method public addCallees(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 59420
    if-nez p1, :cond_0

    .line 59421
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 59423
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->ensureCalleesIsMutable()V

    .line 59424
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59426
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 59185
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;
    .locals 2

    .prologue
    .line 59220
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    move-result-object v0

    .line 59221
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 59222
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 59224
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 59185
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;
    .locals 5

    .prologue
    .line 59238
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 59239
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    .line 59240
    const/4 v2, 0x0

    .line 59241
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 59242
    or-int/lit8 v2, v2, 0x1

    .line 59244
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->access$76502(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 59245
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 59246
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->callees_:Ljava/util/List;

    .line 59247
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x3

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    .line 59249
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->callees_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->callees_:Ljava/util/List;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->access$76602(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;Ljava/util/List;)Ljava/util/List;

    .line 59250
    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_2

    .line 59251
    or-int/lit8 v1, v2, 0x2

    .line 59253
    :goto_0
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->maxRecordingDuration_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->maxRecordingDuration_:I
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->access$76702(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;I)I

    .line 59254
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->access$76802(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;I)I

    .line 59255
    return-object v0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 59185
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 59185
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 59201
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 59202
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 59203
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    .line 59204
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->callees_:Ljava/util/List;

    .line 59205
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    .line 59206
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->maxRecordingDuration_:I

    .line 59207
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    .line 59208
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 59377
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 59379
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    .line 59380
    return-object p0
.end method

.method public clearCallees()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 59460
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->callees_:Ljava/util/List;

    .line 59461
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    .line 59463
    return-object p0
.end method

.method public clearMaxRecordingDuration()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 59487
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    .line 59488
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->maxRecordingDuration_:I

    .line 59490
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 59185
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 59185
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 59185
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;
    .locals 2

    .prologue
    .line 59212
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 59185
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 59346
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCallees(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter

    .prologue
    .line 59400
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getCalleesCount()I
    .locals 1

    .prologue
    .line 59397
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCalleesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59394
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 59185
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 59185
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;
    .locals 1

    .prologue
    .line 59216
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public getMaxRecordingDuration()I
    .locals 1

    .prologue
    .line 59478
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->maxRecordingDuration_:I

    return v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 59343
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMaxRecordingDuration()Z
    .locals 2

    .prologue
    .line 59475
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 59280
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 59294
    :goto_0
    return v0

    .line 59284
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 59286
    goto :goto_0

    :cond_1
    move v0, v2

    .line 59288
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->getCalleesCount()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 59289
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->getCallees(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_2

    move v0, v2

    .line 59291
    goto :goto_0

    .line 59288
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 59294
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 59365
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 59367
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 59373
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    .line 59374
    return-object p0

    .line 59370
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59185
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 59185
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59185
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59302
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 59303
    sparse-switch v0, :sswitch_data_0

    .line 59308
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 59310
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 59306
    goto :goto_1

    .line 59315
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 59316
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 59317
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 59319
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 59320
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    goto :goto_0

    .line 59324
    :sswitch_2
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    .line 59325
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 59326
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->addCallees(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    goto :goto_0

    .line 59330
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    .line 59331
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->maxRecordingDuration_:I

    goto :goto_0

    .line 59303
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 59259
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 59276
    :goto_0
    return-object v0

    .line 59260
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 59261
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    .line 59263
    :cond_1
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->callees_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->access$76600(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 59264
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 59265
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->callees_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->access$76600(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->callees_:Ljava/util/List;

    .line 59266
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    .line 59273
    :cond_2
    :goto_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->hasMaxRecordingDuration()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 59274
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->getMaxRecordingDuration()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->setMaxRecordingDuration(I)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    :cond_3
    move-object v0, p0

    .line 59276
    goto :goto_0

    .line 59268
    :cond_4
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->ensureCalleesIsMutable()V

    .line 59269
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->callees_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->callees_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->access$76600(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public removeCallees(I)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 59466
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->ensureCalleesIsMutable()V

    .line 59467
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 59469
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 59359
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 59361
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    .line 59362
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 59349
    if-nez p1, :cond_0

    .line 59350
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 59352
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 59354
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    .line 59355
    return-object p0
.end method

.method public setCallees(ILcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 59414
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->ensureCalleesIsMutable()V

    .line 59415
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 59417
    return-object p0
.end method

.method public setCallees(ILcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 59404
    if-nez p2, :cond_0

    .line 59405
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 59407
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->ensureCalleesIsMutable()V

    .line 59408
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 59410
    return-object p0
.end method

.method public setMaxRecordingDuration(I)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 59481
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->bitField0_:I

    .line 59482
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->maxRecordingDuration_:I

    .line 59484
    return-object p0
.end method
