.class public final Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RecordVideoMailPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CALLEES_FIELD_NUMBER:I = 0x2

.field public static final MAX_RECORDING_DURATION_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private callees_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private maxRecordingDuration_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 59497
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    .line 59498
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->initFields()V

    .line 59499
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 58987
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 59047
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->memoizedIsInitialized:B

    .line 59084
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->memoizedSerializedSize:I

    .line 58988
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 58982
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 58989
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 59047
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->memoizedIsInitialized:B

    .line 59084
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->memoizedSerializedSize:I

    .line 58989
    return-void
.end method

.method static synthetic access$76502(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 58982
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$76600(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 58982
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->callees_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$76602(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 58982
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->callees_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$76702(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 58982
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->maxRecordingDuration_:I

    return p1
.end method

.method static synthetic access$76802(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 58982
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;
    .locals 1

    .prologue
    .line 58993
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 59043
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 59044
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->callees_:Ljava/util/List;

    .line 59045
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->maxRecordingDuration_:I

    .line 59046
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 59178
    #calls: Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->access$76300()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 59181
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59147
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    move-result-object v0

    .line 59148
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 59149
    #calls: Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->access$76200(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    move-result-object v0

    .line 59151
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59158
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    move-result-object v0

    .line 59159
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 59160
    #calls: Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->access$76200(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    move-result-object v0

    .line 59162
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 59114
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->access$76200(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 59120
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->access$76200(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59168
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->access$76200(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59174
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->access$76200(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59136
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->access$76200(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59142
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->access$76200(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 59125
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->access$76200(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 59131
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;->access$76200(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 59008
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCallees(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter

    .prologue
    .line 59025
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->callees_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getCalleesCount()I
    .locals 1

    .prologue
    .line 59022
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->callees_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCalleesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59015
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->callees_:Ljava/util/List;

    return-object v0
.end method

.method public getCalleesOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 59029
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->callees_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;

    return-object v0
.end method

.method public getCalleesOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59019
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->callees_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 58982
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;
    .locals 1

    .prologue
    .line 58997
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    return-object v0
.end method

.method public getMaxRecordingDuration()I
    .locals 1

    .prologue
    .line 59039
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->maxRecordingDuration_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 59086
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->memoizedSerializedSize:I

    .line 59087
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 59103
    :goto_0
    return v0

    .line 59090
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 59091
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    :goto_1
    move v1, v2

    move v2, v0

    .line 59094
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->callees_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 59095
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->callees_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 59094
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 59098
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_2

    .line 59099
    const/4 v0, 0x3

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->maxRecordingDuration_:I

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v0

    add-int/2addr v0, v2

    .line 59102
    :goto_3
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->memoizedSerializedSize:I

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 59005
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMaxRecordingDuration()Z
    .locals 2

    .prologue
    .line 59036
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 59049
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->memoizedIsInitialized:B

    .line 59050
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 59067
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 59050
    goto :goto_0

    .line 59052
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 59053
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 59054
    goto :goto_0

    .line 59056
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 59057
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 59058
    goto :goto_0

    :cond_3
    move v0, v2

    .line 59060
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->getCalleesCount()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 59061
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->getCallees(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_4

    .line 59062
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 59063
    goto :goto_0

    .line 59060
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 59066
    :cond_5
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 59067
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 58982
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 59179
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 58982
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 59183
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 59108
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 59072
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->getSerializedSize()I

    .line 59073
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 59074
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 59076
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->callees_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 59077
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->callees_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 59076
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 59079
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 59080
    const/4 v0, 0x3

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->maxRecordingDuration_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 59082
    :cond_2
    return-void
.end method
