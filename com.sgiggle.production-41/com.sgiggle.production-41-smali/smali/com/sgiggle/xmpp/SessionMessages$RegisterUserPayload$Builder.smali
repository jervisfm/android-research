.class public final Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private accessAddressBook_:Z

.field private alerts_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;"
        }
    .end annotation
.end field

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private byWaitingAbookLoading_:Z

.field private contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

.field private countryCode_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CountryCode;",
            ">;"
        }
    .end annotation
.end field

.field private deviceId_:Ljava/lang/Object;

.field private deviceType_:I

.field private goWithAddressBookCompare_:Z

.field private linkAccounts_:Z

.field private locale_:Ljava/lang/Object;

.field private minorDeviceType_:I

.field private registered_:Z

.field private registrationOptions_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

.field private storeAddressBook_:Z

.field private validationcode_:Ljava/lang/Object;

.field private vgoodPurchaseDaysLeft_:I

.field private vmailUpgradeable_:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 25408
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 25792
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 25856
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 25899
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->validationcode_:Ljava/lang/Object;

    .line 25935
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->locale_:Ljava/lang/Object;

    .line 25971
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->countryCode_:Ljava/util/List;

    .line 26060
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->deviceId_:Ljava/lang/Object;

    .line 26138
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->alerts_:Ljava/util/List;

    .line 26290
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->vmailUpgradeable_:Z

    .line 26374
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->registrationOptions_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    .line 25409
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->maybeForceBuilderInitialization()V

    .line 25410
    return-void
.end method

.method static synthetic access$31700(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 25403
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$31800()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1

    .prologue
    .line 25403
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 25477
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    move-result-object v0

    .line 25478
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 25479
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 25482
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1

    .prologue
    .line 25415
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureAlertsIsMutable()V
    .locals 2

    .prologue
    .line 26141
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-eq v0, v1, :cond_0

    .line 26142
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->alerts_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->alerts_:Ljava/util/List;

    .line 26143
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 26145
    :cond_0
    return-void
.end method

.method private ensureCountryCodeIsMutable()V
    .locals 2

    .prologue
    .line 25974
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    .line 25975
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->countryCode_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->countryCode_:Ljava/util/List;

    .line 25976
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25978
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 25413
    return-void
.end method


# virtual methods
.method public addAlerts(ILcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 26201
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->ensureAlertsIsMutable()V

    .line 26202
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->alerts_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 26204
    return-object p0
.end method

.method public addAlerts(ILcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 26184
    if-nez p2, :cond_0

    .line 26185
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 26187
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->ensureAlertsIsMutable()V

    .line 26188
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->alerts_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 26190
    return-object p0
.end method

.method public addAlerts(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 26194
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->ensureAlertsIsMutable()V

    .line 26195
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->alerts_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 26197
    return-object p0
.end method

.method public addAlerts(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 26174
    if-nez p1, :cond_0

    .line 26175
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 26177
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->ensureAlertsIsMutable()V

    .line 26178
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->alerts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 26180
    return-object p0
.end method

.method public addAllAlerts(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;"
        }
    .end annotation

    .prologue
    .line 26208
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->ensureAlertsIsMutable()V

    .line 26209
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->alerts_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 26211
    return-object p0
.end method

.method public addAllCountryCode(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$CountryCode;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;"
        }
    .end annotation

    .prologue
    .line 26041
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->ensureCountryCodeIsMutable()V

    .line 26042
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->countryCode_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 26044
    return-object p0
.end method

.method public addCountryCode(ILcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 26034
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->ensureCountryCodeIsMutable()V

    .line 26035
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->countryCode_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 26037
    return-object p0
.end method

.method public addCountryCode(ILcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 26017
    if-nez p2, :cond_0

    .line 26018
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 26020
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->ensureCountryCodeIsMutable()V

    .line 26021
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->countryCode_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 26023
    return-object p0
.end method

.method public addCountryCode(Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 26027
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->ensureCountryCodeIsMutable()V

    .line 26028
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->countryCode_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 26030
    return-object p0
.end method

.method public addCountryCode(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 26007
    if-nez p1, :cond_0

    .line 26008
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 26010
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->ensureCountryCodeIsMutable()V

    .line 26011
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->countryCode_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 26013
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 25403
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;
    .locals 2

    .prologue
    .line 25468
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    move-result-object v0

    .line 25469
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 25470
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 25472
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 25403
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;
    .locals 8

    .prologue
    const/high16 v7, 0x2

    const/high16 v6, 0x1

    const v5, 0x8000

    .line 25486
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 25487
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25488
    const/4 v2, 0x0

    .line 25489
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 25490
    or-int/lit8 v2, v2, 0x1

    .line 25492
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->access$32002(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 25493
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 25494
    or-int/lit8 v2, v2, 0x2

    .line 25496
    :cond_1
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->accessAddressBook_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->accessAddressBook_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->access$32102(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Z)Z

    .line 25497
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 25498
    or-int/lit8 v2, v2, 0x4

    .line 25500
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->access$32202(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 25501
    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 25502
    or-int/lit8 v2, v2, 0x8

    .line 25504
    :cond_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->validationcode_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->validationcode_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->access$32302(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25505
    and-int/lit8 v3, v1, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 25506
    or-int/lit8 v2, v2, 0x10

    .line 25508
    :cond_4
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->locale_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->locale_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->access$32402(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25509
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 25510
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->countryCode_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->countryCode_:Ljava/util/List;

    .line 25511
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x21

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25513
    :cond_5
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->countryCode_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->countryCode_:Ljava/util/List;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->access$32502(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Ljava/util/List;)Ljava/util/List;

    .line 25514
    and-int/lit8 v3, v1, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    .line 25515
    or-int/lit8 v2, v2, 0x20

    .line 25517
    :cond_6
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->deviceId_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->deviceId_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->access$32602(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25518
    and-int/lit16 v3, v1, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    .line 25519
    or-int/lit8 v2, v2, 0x40

    .line 25521
    :cond_7
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->deviceType_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->deviceType_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->access$32702(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;I)I

    .line 25522
    and-int/lit16 v3, v1, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_8

    .line 25523
    or-int/lit16 v2, v2, 0x80

    .line 25525
    :cond_8
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->minorDeviceType_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->minorDeviceType_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->access$32802(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;I)I

    .line 25526
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit16 v3, v3, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_9

    .line 25527
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->alerts_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->alerts_:Ljava/util/List;

    .line 25528
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit16 v3, v3, -0x201

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25530
    :cond_9
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->alerts_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->alerts_:Ljava/util/List;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->access$32902(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Ljava/util/List;)Ljava/util/List;

    .line 25531
    and-int/lit16 v3, v1, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_a

    .line 25532
    or-int/lit16 v2, v2, 0x100

    .line 25534
    :cond_a
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->storeAddressBook_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->storeAddressBook_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->access$33002(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Z)Z

    .line 25535
    and-int/lit16 v3, v1, 0x800

    const/16 v4, 0x800

    if-ne v3, v4, :cond_b

    .line 25536
    or-int/lit16 v2, v2, 0x200

    .line 25538
    :cond_b
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->linkAccounts_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->linkAccounts_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->access$33102(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Z)Z

    .line 25539
    and-int/lit16 v3, v1, 0x1000

    const/16 v4, 0x1000

    if-ne v3, v4, :cond_c

    .line 25540
    or-int/lit16 v2, v2, 0x400

    .line 25542
    :cond_c
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->vgoodPurchaseDaysLeft_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->vgoodPurchaseDaysLeft_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->access$33202(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;I)I

    .line 25543
    and-int/lit16 v3, v1, 0x2000

    const/16 v4, 0x2000

    if-ne v3, v4, :cond_d

    .line 25544
    or-int/lit16 v2, v2, 0x800

    .line 25546
    :cond_d
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->vmailUpgradeable_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->vmailUpgradeable_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->access$33302(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Z)Z

    .line 25547
    and-int/lit16 v3, v1, 0x4000

    const/16 v4, 0x4000

    if-ne v3, v4, :cond_e

    .line 25548
    or-int/lit16 v2, v2, 0x1000

    .line 25550
    :cond_e
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->goWithAddressBookCompare_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->goWithAddressBookCompare_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->access$33402(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Z)Z

    .line 25551
    and-int v3, v1, v5

    if-ne v3, v5, :cond_f

    .line 25552
    or-int/lit16 v2, v2, 0x2000

    .line 25554
    :cond_f
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->byWaitingAbookLoading_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->byWaitingAbookLoading_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->access$33502(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Z)Z

    .line 25555
    and-int v3, v1, v6

    if-ne v3, v6, :cond_10

    .line 25556
    or-int/lit16 v2, v2, 0x4000

    .line 25558
    :cond_10
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->registered_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->registered_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->access$33602(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Z)Z

    .line 25559
    and-int/2addr v1, v7

    if-ne v1, v7, :cond_11

    .line 25560
    or-int v1, v2, v5

    .line 25562
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->registrationOptions_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->registrationOptions_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->access$33702(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    .line 25563
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->access$33802(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;I)I

    .line 25564
    return-object v0

    :cond_11
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 25403
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 25403
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 25419
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 25420
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 25421
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25422
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->accessAddressBook_:Z

    .line 25423
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25424
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 25425
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25426
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->validationcode_:Ljava/lang/Object;

    .line 25427
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25428
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->locale_:Ljava/lang/Object;

    .line 25429
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25430
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->countryCode_:Ljava/util/List;

    .line 25431
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25432
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->deviceId_:Ljava/lang/Object;

    .line 25433
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25434
    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->deviceType_:I

    .line 25435
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25436
    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->minorDeviceType_:I

    .line 25437
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25438
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->alerts_:Ljava/util/List;

    .line 25439
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25440
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->storeAddressBook_:Z

    .line 25441
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25442
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->linkAccounts_:Z

    .line 25443
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25444
    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->vgoodPurchaseDaysLeft_:I

    .line 25445
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25446
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->vmailUpgradeable_:Z

    .line 25447
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x2001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25448
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->goWithAddressBookCompare_:Z

    .line 25449
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x4001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25450
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->byWaitingAbookLoading_:Z

    .line 25451
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    const v1, -0x8001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25452
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->registered_:Z

    .line 25453
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    const v1, -0x10001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25454
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->registrationOptions_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    .line 25455
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    const v1, -0x20001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25456
    return-object p0
.end method

.method public clearAccessAddressBook()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1

    .prologue
    .line 25849
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25850
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->accessAddressBook_:Z

    .line 25852
    return-object p0
.end method

.method public clearAlerts()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1

    .prologue
    .line 26214
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->alerts_:Ljava/util/List;

    .line 26215
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 26217
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1

    .prologue
    .line 25828
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 25830
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25831
    return-object p0
.end method

.method public clearByWaitingAbookLoading()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 2

    .prologue
    .line 26346
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    const v1, -0x8001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 26347
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->byWaitingAbookLoading_:Z

    .line 26349
    return-object p0
.end method

.method public clearContact()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1

    .prologue
    .line 25892
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 25894
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25895
    return-object p0
.end method

.method public clearCountryCode()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1

    .prologue
    .line 26047
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->countryCode_:Ljava/util/List;

    .line 26048
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 26050
    return-object p0
.end method

.method public clearDeviceId()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1

    .prologue
    .line 26084
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 26085
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->deviceId_:Ljava/lang/Object;

    .line 26087
    return-object p0
.end method

.method public clearDeviceType()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1

    .prologue
    .line 26110
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 26111
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->deviceType_:I

    .line 26113
    return-object p0
.end method

.method public clearGoWithAddressBookCompare()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1

    .prologue
    .line 26325
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x4001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 26326
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->goWithAddressBookCompare_:Z

    .line 26328
    return-object p0
.end method

.method public clearLinkAccounts()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1

    .prologue
    .line 26262
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 26263
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->linkAccounts_:Z

    .line 26265
    return-object p0
.end method

.method public clearLocale()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1

    .prologue
    .line 25959
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25960
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getLocale()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->locale_:Ljava/lang/Object;

    .line 25962
    return-object p0
.end method

.method public clearMinorDeviceType()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1

    .prologue
    .line 26131
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 26132
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->minorDeviceType_:I

    .line 26134
    return-object p0
.end method

.method public clearRegistered()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 2

    .prologue
    .line 26367
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    const v1, -0x10001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 26368
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->registered_:Z

    .line 26370
    return-object p0
.end method

.method public clearRegistrationOptions()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 2

    .prologue
    .line 26410
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->registrationOptions_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    .line 26412
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    const v1, -0x20001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 26413
    return-object p0
.end method

.method public clearStoreAddressBook()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1

    .prologue
    .line 26241
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 26242
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->storeAddressBook_:Z

    .line 26244
    return-object p0
.end method

.method public clearValidationcode()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1

    .prologue
    .line 25923
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25924
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getValidationcode()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->validationcode_:Ljava/lang/Object;

    .line 25926
    return-object p0
.end method

.method public clearVgoodPurchaseDaysLeft()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1

    .prologue
    .line 26283
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 26284
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->vgoodPurchaseDaysLeft_:I

    .line 26286
    return-object p0
.end method

.method public clearVmailUpgradeable()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1

    .prologue
    .line 26304
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x2001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 26305
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->vmailUpgradeable_:Z

    .line 26307
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 25403
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 25403
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 25403
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 2

    .prologue
    .line 25460
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 25403
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAccessAddressBook()Z
    .locals 1

    .prologue
    .line 25840
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->accessAddressBook_:Z

    return v0
.end method

.method public getAlerts(I)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 1
    .parameter

    .prologue
    .line 26154
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->alerts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    return-object v0
.end method

.method public getAlertsCount()I
    .locals 1

    .prologue
    .line 26151
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->alerts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getAlertsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26148
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->alerts_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 25797
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getByWaitingAbookLoading()Z
    .locals 1

    .prologue
    .line 26337
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->byWaitingAbookLoading_:Z

    return v0
.end method

.method public getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1

    .prologue
    .line 25861
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getCountryCode(I)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 1
    .parameter

    .prologue
    .line 25987
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->countryCode_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    return-object v0
.end method

.method public getCountryCodeCount()I
    .locals 1

    .prologue
    .line 25984
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->countryCode_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCountryCodeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CountryCode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25981
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->countryCode_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 25403
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 25403
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;
    .locals 1

    .prologue
    .line 25464
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 26065
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->deviceId_:Ljava/lang/Object;

    .line 26066
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 26067
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 26068
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->deviceId_:Ljava/lang/Object;

    .line 26071
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getDeviceType()I
    .locals 1

    .prologue
    .line 26101
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->deviceType_:I

    return v0
.end method

.method public getGoWithAddressBookCompare()Z
    .locals 1

    .prologue
    .line 26316
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->goWithAddressBookCompare_:Z

    return v0
.end method

.method public getLinkAccounts()Z
    .locals 1

    .prologue
    .line 26253
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->linkAccounts_:Z

    return v0
.end method

.method public getLocale()Ljava/lang/String;
    .locals 2

    .prologue
    .line 25940
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->locale_:Ljava/lang/Object;

    .line 25941
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 25942
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 25943
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->locale_:Ljava/lang/Object;

    .line 25946
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getMinorDeviceType()I
    .locals 1

    .prologue
    .line 26122
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->minorDeviceType_:I

    return v0
.end method

.method public getRegistered()Z
    .locals 1

    .prologue
    .line 26358
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->registered_:Z

    return v0
.end method

.method public getRegistrationOptions()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    .locals 1

    .prologue
    .line 26379
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->registrationOptions_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    return-object v0
.end method

.method public getStoreAddressBook()Z
    .locals 1

    .prologue
    .line 26232
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->storeAddressBook_:Z

    return v0
.end method

.method public getValidationcode()Ljava/lang/String;
    .locals 2

    .prologue
    .line 25904
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->validationcode_:Ljava/lang/Object;

    .line 25905
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 25906
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 25907
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->validationcode_:Ljava/lang/Object;

    .line 25910
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getVgoodPurchaseDaysLeft()I
    .locals 1

    .prologue
    .line 26274
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->vgoodPurchaseDaysLeft_:I

    return v0
.end method

.method public getVmailUpgradeable()Z
    .locals 1

    .prologue
    .line 26295
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->vmailUpgradeable_:Z

    return v0
.end method

.method public hasAccessAddressBook()Z
    .locals 2

    .prologue
    .line 25837
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 25794
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasByWaitingAbookLoading()Z
    .locals 2

    .prologue
    const v1, 0x8000

    .line 26334
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasContact()Z
    .locals 2

    .prologue
    .line 25858
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDeviceId()Z
    .locals 2

    .prologue
    .line 26062
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDeviceType()Z
    .locals 2

    .prologue
    .line 26098
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasGoWithAddressBookCompare()Z
    .locals 2

    .prologue
    .line 26313
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLinkAccounts()Z
    .locals 2

    .prologue
    .line 26250
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLocale()Z
    .locals 2

    .prologue
    .line 25937
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMinorDeviceType()Z
    .locals 2

    .prologue
    .line 26119
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRegistered()Z
    .locals 2

    .prologue
    const/high16 v1, 0x1

    .line 26355
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRegistrationOptions()Z
    .locals 2

    .prologue
    const/high16 v1, 0x2

    .line 26376
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasStoreAddressBook()Z
    .locals 2

    .prologue
    .line 26229
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasValidationcode()Z
    .locals 2

    .prologue
    .line 25901
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVgoodPurchaseDaysLeft()Z
    .locals 2

    .prologue
    .line 26271
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVmailUpgradeable()Z
    .locals 2

    .prologue
    .line 26292
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 25641
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 25661
    :goto_0
    return v0

    .line 25645
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 25647
    goto :goto_0

    .line 25649
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->hasContact()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 25650
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    .line 25652
    goto :goto_0

    :cond_2
    move v0, v2

    .line 25655
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->getCountryCodeCount()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 25656
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->getCountryCode(I)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_3

    move v0, v2

    .line 25658
    goto :goto_0

    .line 25655
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 25661
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 25816
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 25818
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 25824
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25825
    return-object p0

    .line 25821
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public mergeContact(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 25880
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 25882
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 25888
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25889
    return-object p0

    .line 25885
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25403
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 25403
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25403
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25669
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 25670
    sparse-switch v0, :sswitch_data_0

    .line 25675
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 25677
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 25673
    goto :goto_1

    .line 25682
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 25683
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 25684
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 25686
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 25687
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    goto :goto_0

    .line 25691
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25692
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->accessAddressBook_:Z

    goto :goto_0

    .line 25696
    :sswitch_3
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    .line 25697
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->hasContact()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 25698
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    .line 25700
    :cond_2
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 25701
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->setContact(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    goto :goto_0

    .line 25705
    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25706
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->validationcode_:Ljava/lang/Object;

    goto :goto_0

    .line 25710
    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25711
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->locale_:Ljava/lang/Object;

    goto :goto_0

    .line 25715
    :sswitch_6
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    .line 25716
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 25717
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->addCountryCode(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    goto :goto_0

    .line 25721
    :sswitch_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25722
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->deviceId_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 25726
    :sswitch_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25727
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->deviceType_:I

    goto/16 :goto_0

    .line 25731
    :sswitch_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25732
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->minorDeviceType_:I

    goto/16 :goto_0

    .line 25736
    :sswitch_a
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    .line 25737
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 25738
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->addAlerts(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    goto/16 :goto_0

    .line 25742
    :sswitch_b
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25743
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->storeAddressBook_:Z

    goto/16 :goto_0

    .line 25747
    :sswitch_c
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25748
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->linkAccounts_:Z

    goto/16 :goto_0

    .line 25752
    :sswitch_d
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25753
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->vgoodPurchaseDaysLeft_:I

    goto/16 :goto_0

    .line 25757
    :sswitch_e
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25758
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->vmailUpgradeable_:Z

    goto/16 :goto_0

    .line 25762
    :sswitch_f
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25763
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->goWithAddressBookCompare_:Z

    goto/16 :goto_0

    .line 25767
    :sswitch_10
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    const v1, 0x8000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25768
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->byWaitingAbookLoading_:Z

    goto/16 :goto_0

    .line 25772
    :sswitch_11
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    const/high16 v1, 0x1

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25773
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->registered_:Z

    goto/16 :goto_0

    .line 25777
    :sswitch_12
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    .line 25778
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->hasRegistrationOptions()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 25779
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->getRegistrationOptions()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    .line 25781
    :cond_3
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 25782
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->setRegistrationOptions(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    goto/16 :goto_0

    .line 25670
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x88 -> :sswitch_11
        0x92 -> :sswitch_12
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 25568
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 25637
    :goto_0
    return-object v0

    .line 25569
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 25570
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    .line 25572
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->hasAccessAddressBook()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 25573
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getAccessAddressBook()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->setAccessAddressBook(Z)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    .line 25575
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->hasContact()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 25576
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->mergeContact(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    .line 25578
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->hasValidationcode()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 25579
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getValidationcode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->setValidationcode(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    .line 25581
    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->hasLocale()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 25582
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getLocale()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->setLocale(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    .line 25584
    :cond_5
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->countryCode_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->access$32500(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 25585
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->countryCode_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 25586
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->countryCode_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->access$32500(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->countryCode_:Ljava/util/List;

    .line 25587
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25594
    :cond_6
    :goto_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->hasDeviceId()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 25595
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->setDeviceId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    .line 25597
    :cond_7
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->hasDeviceType()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 25598
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getDeviceType()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->setDeviceType(I)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    .line 25600
    :cond_8
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->hasMinorDeviceType()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 25601
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getMinorDeviceType()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->setMinorDeviceType(I)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    .line 25603
    :cond_9
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->alerts_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->access$32900(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 25604
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->alerts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 25605
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->alerts_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->access$32900(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->alerts_:Ljava/util/List;

    .line 25606
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25613
    :cond_a
    :goto_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->hasStoreAddressBook()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 25614
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getStoreAddressBook()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->setStoreAddressBook(Z)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    .line 25616
    :cond_b
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->hasLinkAccounts()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 25617
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getLinkAccounts()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->setLinkAccounts(Z)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    .line 25619
    :cond_c
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->hasVgoodPurchaseDaysLeft()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 25620
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getVgoodPurchaseDaysLeft()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->setVgoodPurchaseDaysLeft(I)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    .line 25622
    :cond_d
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->hasVmailUpgradeable()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 25623
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getVmailUpgradeable()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->setVmailUpgradeable(Z)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    .line 25625
    :cond_e
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->hasGoWithAddressBookCompare()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 25626
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getGoWithAddressBookCompare()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->setGoWithAddressBookCompare(Z)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    .line 25628
    :cond_f
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->hasByWaitingAbookLoading()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 25629
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getByWaitingAbookLoading()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->setByWaitingAbookLoading(Z)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    .line 25631
    :cond_10
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->hasRegistered()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 25632
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getRegistered()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->setRegistered(Z)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    .line 25634
    :cond_11
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->hasRegistrationOptions()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 25635
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getRegistrationOptions()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->mergeRegistrationOptions(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    :cond_12
    move-object v0, p0

    .line 25637
    goto/16 :goto_0

    .line 25589
    :cond_13
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->ensureCountryCodeIsMutable()V

    .line 25590
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->countryCode_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->countryCode_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->access$32500(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    .line 25608
    :cond_14
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->ensureAlertsIsMutable()V

    .line 25609
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->alerts_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->alerts_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->access$32900(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_2
.end method

.method public mergeRegistrationOptions(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 3
    .parameter

    .prologue
    const/high16 v2, 0x2

    .line 26398
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    and-int/2addr v0, v2

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->registrationOptions_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 26400
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->registrationOptions_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->registrationOptions_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    .line 26406
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/2addr v0, v2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 26407
    return-object p0

    .line 26403
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->registrationOptions_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    goto :goto_0
.end method

.method public removeAlerts(I)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 26220
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->ensureAlertsIsMutable()V

    .line 26221
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->alerts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 26223
    return-object p0
.end method

.method public removeCountryCode(I)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 26053
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->ensureCountryCodeIsMutable()V

    .line 26054
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->countryCode_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 26056
    return-object p0
.end method

.method public setAccessAddressBook(Z)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 25843
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25844
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->accessAddressBook_:Z

    .line 25846
    return-object p0
.end method

.method public setAlerts(ILcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 26168
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->ensureAlertsIsMutable()V

    .line 26169
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->alerts_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 26171
    return-object p0
.end method

.method public setAlerts(ILcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 26158
    if-nez p2, :cond_0

    .line 26159
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 26161
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->ensureAlertsIsMutable()V

    .line 26162
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->alerts_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 26164
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 25810
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 25812
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25813
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 25800
    if-nez p1, :cond_0

    .line 25801
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 25803
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 25805
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25806
    return-object p0
.end method

.method public setByWaitingAbookLoading(Z)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 26340
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    const v1, 0x8000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 26341
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->byWaitingAbookLoading_:Z

    .line 26343
    return-object p0
.end method

.method public setContact(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 25874
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 25876
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25877
    return-object p0
.end method

.method public setContact(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 25864
    if-nez p1, :cond_0

    .line 25865
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 25867
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 25869
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25870
    return-object p0
.end method

.method public setCountryCode(ILcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 26001
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->ensureCountryCodeIsMutable()V

    .line 26002
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->countryCode_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 26004
    return-object p0
.end method

.method public setCountryCode(ILcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 25991
    if-nez p2, :cond_0

    .line 25992
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 25994
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->ensureCountryCodeIsMutable()V

    .line 25995
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->countryCode_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 25997
    return-object p0
.end method

.method public setDeviceId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 26075
    if-nez p1, :cond_0

    .line 26076
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 26078
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 26079
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->deviceId_:Ljava/lang/Object;

    .line 26081
    return-object p0
.end method

.method setDeviceId(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 26090
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 26091
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->deviceId_:Ljava/lang/Object;

    .line 26093
    return-void
.end method

.method public setDeviceType(I)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 26104
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 26105
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->deviceType_:I

    .line 26107
    return-object p0
.end method

.method public setGoWithAddressBookCompare(Z)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 26319
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 26320
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->goWithAddressBookCompare_:Z

    .line 26322
    return-object p0
.end method

.method public setLinkAccounts(Z)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 26256
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 26257
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->linkAccounts_:Z

    .line 26259
    return-object p0
.end method

.method public setLocale(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 25950
    if-nez p1, :cond_0

    .line 25951
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 25953
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25954
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->locale_:Ljava/lang/Object;

    .line 25956
    return-object p0
.end method

.method setLocale(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 25965
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25966
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->locale_:Ljava/lang/Object;

    .line 25968
    return-void
.end method

.method public setMinorDeviceType(I)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 26125
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 26126
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->minorDeviceType_:I

    .line 26128
    return-object p0
.end method

.method public setRegistered(Z)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 26361
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    const/high16 v1, 0x1

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 26362
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->registered_:Z

    .line 26364
    return-object p0
.end method

.method public setRegistrationOptions(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 26392
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->registrationOptions_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    .line 26394
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    const/high16 v1, 0x2

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 26395
    return-object p0
.end method

.method public setRegistrationOptions(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 26382
    if-nez p1, :cond_0

    .line 26383
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 26385
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->registrationOptions_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    .line 26387
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    const/high16 v1, 0x2

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 26388
    return-object p0
.end method

.method public setStoreAddressBook(Z)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 26235
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 26236
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->storeAddressBook_:Z

    .line 26238
    return-object p0
.end method

.method public setValidationcode(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 25914
    if-nez p1, :cond_0

    .line 25915
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 25917
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25918
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->validationcode_:Ljava/lang/Object;

    .line 25920
    return-object p0
.end method

.method setValidationcode(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 25929
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 25930
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->validationcode_:Ljava/lang/Object;

    .line 25932
    return-void
.end method

.method public setVgoodPurchaseDaysLeft(I)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 26277
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 26278
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->vgoodPurchaseDaysLeft_:I

    .line 26280
    return-object p0
.end method

.method public setVmailUpgradeable(Z)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 26298
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->bitField0_:I

    .line 26299
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->vmailUpgradeable_:Z

    .line 26301
    return-object p0
.end method
