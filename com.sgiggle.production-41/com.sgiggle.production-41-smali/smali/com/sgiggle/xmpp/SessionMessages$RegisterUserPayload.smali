.class public final Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RegisterUserPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    }
.end annotation


# static fields
.field public static final ACCESSADDRESSBOOK_FIELD_NUMBER:I = 0x2

.field public static final ALERTS_FIELD_NUMBER:I = 0xa

.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final BY_WAITING_ABOOK_LOADING_FIELD_NUMBER:I = 0x10

.field public static final CONTACT_FIELD_NUMBER:I = 0x3

.field public static final COUNTRYCODE_FIELD_NUMBER:I = 0x6

.field public static final DEVICEID_FIELD_NUMBER:I = 0x7

.field public static final DEVICETYPE_FIELD_NUMBER:I = 0x8

.field public static final GO_WITH_ADDRESS_BOOK_COMPARE_FIELD_NUMBER:I = 0xf

.field public static final LINKACCOUNTS_FIELD_NUMBER:I = 0xc

.field public static final LOCALE_FIELD_NUMBER:I = 0x5

.field public static final MINORDEVICETYPE_FIELD_NUMBER:I = 0x9

.field public static final REGISTERED_FIELD_NUMBER:I = 0x11

.field public static final REGISTRATIONOPTIONS_FIELD_NUMBER:I = 0x12

.field public static final STOREADDRESSBOOK_FIELD_NUMBER:I = 0xb

.field public static final VALIDATIONCODE_FIELD_NUMBER:I = 0x4

.field public static final VGOODPURCHASEDAYSLEFT_FIELD_NUMBER:I = 0xd

.field public static final VMAILUPGRADEABLE_FIELD_NUMBER:I = 0xe

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;


# instance fields
.field private accessAddressBook_:Z

.field private alerts_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;"
        }
    .end annotation
.end field

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private byWaitingAbookLoading_:Z

.field private contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

.field private countryCode_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CountryCode;",
            ">;"
        }
    .end annotation
.end field

.field private deviceId_:Ljava/lang/Object;

.field private deviceType_:I

.field private goWithAddressBookCompare_:Z

.field private linkAccounts_:Z

.field private locale_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private minorDeviceType_:I

.field private registered_:Z

.field private registrationOptions_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

.field private storeAddressBook_:Z

.field private validationcode_:Ljava/lang/Object;

.field private vgoodPurchaseDaysLeft_:I

.field private vmailUpgradeable_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26420
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    .line 26421
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->initFields()V

    .line 26422
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 24852
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 25154
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->memoizedIsInitialized:B

    .line 25242
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->memoizedSerializedSize:I

    .line 24853
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24847
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 24854
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 25154
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->memoizedIsInitialized:B

    .line 25242
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->memoizedSerializedSize:I

    .line 24854
    return-void
.end method

.method static synthetic access$32002(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24847
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$32102(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24847
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->accessAddressBook_:Z

    return p1
.end method

.method static synthetic access$32202(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24847
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object p1
.end method

.method static synthetic access$32302(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24847
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->validationcode_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$32402(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24847
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->locale_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$32500(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 24847
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->countryCode_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$32502(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24847
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->countryCode_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$32602(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24847
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->deviceId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$32702(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24847
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->deviceType_:I

    return p1
.end method

.method static synthetic access$32802(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24847
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->minorDeviceType_:I

    return p1
.end method

.method static synthetic access$32900(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 24847
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->alerts_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$32902(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24847
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->alerts_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$33002(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24847
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->storeAddressBook_:Z

    return p1
.end method

.method static synthetic access$33102(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24847
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->linkAccounts_:Z

    return p1
.end method

.method static synthetic access$33202(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24847
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->vgoodPurchaseDaysLeft_:I

    return p1
.end method

.method static synthetic access$33302(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24847
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->vmailUpgradeable_:Z

    return p1
.end method

.method static synthetic access$33402(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24847
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->goWithAddressBookCompare_:Z

    return p1
.end method

.method static synthetic access$33502(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24847
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->byWaitingAbookLoading_:Z

    return p1
.end method

.method static synthetic access$33602(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24847
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->registered_:Z

    return p1
.end method

.method static synthetic access$33702(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24847
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->registrationOptions_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    return-object p1
.end method

.method static synthetic access$33802(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24847
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;
    .locals 1

    .prologue
    .line 24858
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    return-object v0
.end method

.method private getDeviceIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 25002
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->deviceId_:Ljava/lang/Object;

    .line 25003
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 25004
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 25006
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->deviceId_:Ljava/lang/Object;

    .line 25009
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getLocaleBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 24949
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->locale_:Ljava/lang/Object;

    .line 24950
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 24951
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 24953
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->locale_:Ljava/lang/Object;

    .line 24956
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getValidationcodeBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 24917
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->validationcode_:Ljava/lang/Object;

    .line 24918
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 24919
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 24921
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->validationcode_:Ljava/lang/Object;

    .line 24924
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 25135
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 25136
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->accessAddressBook_:Z

    .line 25137
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 25138
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->validationcode_:Ljava/lang/Object;

    .line 25139
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->locale_:Ljava/lang/Object;

    .line 25140
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->countryCode_:Ljava/util/List;

    .line 25141
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->deviceId_:Ljava/lang/Object;

    .line 25142
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->deviceType_:I

    .line 25143
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->minorDeviceType_:I

    .line 25144
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->alerts_:Ljava/util/List;

    .line 25145
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->storeAddressBook_:Z

    .line 25146
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->linkAccounts_:Z

    .line 25147
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->vgoodPurchaseDaysLeft_:I

    .line 25148
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->vmailUpgradeable_:Z

    .line 25149
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->goWithAddressBookCompare_:Z

    .line 25150
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->byWaitingAbookLoading_:Z

    .line 25151
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->registered_:Z

    .line 25152
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->registrationOptions_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    .line 25153
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1

    .prologue
    .line 25396
    #calls: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->access$31800()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 25399
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25365
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    .line 25366
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 25367
    #calls: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->access$31700(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    move-result-object v0

    .line 25369
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25376
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    .line 25377
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 25378
    #calls: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->access$31700(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    move-result-object v0

    .line 25380
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 25332
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->access$31700(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 25338
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->access$31700(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25386
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->access$31700(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25392
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->access$31700(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25354
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->access$31700(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25360
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->access$31700(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 25343
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->access$31700(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 25349
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;->access$31700(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAccessAddressBook()Z
    .locals 1

    .prologue
    .line 24883
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->accessAddressBook_:Z

    return v0
.end method

.method public getAlerts(I)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 1
    .parameter

    .prologue
    .line 25047
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->alerts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    return-object v0
.end method

.method public getAlertsCount()I
    .locals 1

    .prologue
    .line 25044
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->alerts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getAlertsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25037
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->alerts_:Ljava/util/List;

    return-object v0
.end method

.method public getAlertsOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlertOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 25051
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->alerts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlertOrBuilder;

    return-object v0
.end method

.method public getAlertsOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlertOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25041
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->alerts_:Ljava/util/List;

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 24873
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getByWaitingAbookLoading()Z
    .locals 1

    .prologue
    .line 25111
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->byWaitingAbookLoading_:Z

    return v0
.end method

.method public getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1

    .prologue
    .line 24893
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getCountryCode(I)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 1
    .parameter

    .prologue
    .line 24974
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->countryCode_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    return-object v0
.end method

.method public getCountryCodeCount()I
    .locals 1

    .prologue
    .line 24971
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->countryCode_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCountryCodeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CountryCode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 24964
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->countryCode_:Ljava/util/List;

    return-object v0
.end method

.method public getCountryCodeOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$CountryCodeOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 24978
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->countryCode_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodeOrBuilder;

    return-object v0
.end method

.method public getCountryCodeOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$CountryCodeOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 24968
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->countryCode_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 24847
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;
    .locals 1

    .prologue
    .line 24862
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    return-object v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 24988
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->deviceId_:Ljava/lang/Object;

    .line 24989
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 24990
    check-cast v0, Ljava/lang/String;

    .line 24998
    :goto_0
    return-object v0

    .line 24992
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 24994
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 24995
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 24996
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->deviceId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 24998
    goto :goto_0
.end method

.method public getDeviceType()I
    .locals 1

    .prologue
    .line 25020
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->deviceType_:I

    return v0
.end method

.method public getGoWithAddressBookCompare()Z
    .locals 1

    .prologue
    .line 25101
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->goWithAddressBookCompare_:Z

    return v0
.end method

.method public getLinkAccounts()Z
    .locals 1

    .prologue
    .line 25071
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->linkAccounts_:Z

    return v0
.end method

.method public getLocale()Ljava/lang/String;
    .locals 2

    .prologue
    .line 24935
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->locale_:Ljava/lang/Object;

    .line 24936
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 24937
    check-cast v0, Ljava/lang/String;

    .line 24945
    :goto_0
    return-object v0

    .line 24939
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 24941
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 24942
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 24943
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->locale_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 24945
    goto :goto_0
.end method

.method public getMinorDeviceType()I
    .locals 1

    .prologue
    .line 25030
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->minorDeviceType_:I

    return v0
.end method

.method public getRegistered()Z
    .locals 1

    .prologue
    .line 25121
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->registered_:Z

    return v0
.end method

.method public getRegistrationOptions()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    .locals 1

    .prologue
    .line 25131
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->registrationOptions_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 25244
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->memoizedSerializedSize:I

    .line 25245
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 25321
    :goto_0
    return v0

    .line 25248
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_12

    .line 25249
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v4

    .line 25252
    :goto_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_1

    .line 25253
    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->accessAddressBook_:Z

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 25256
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_2

    .line 25257
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25260
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v6, :cond_3

    .line 25261
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getValidationcodeBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25264
    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_4

    .line 25265
    const/4 v1, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getLocaleBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    move v1, v4

    move v2, v0

    .line 25268
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->countryCode_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 25269
    const/4 v3, 0x6

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->countryCode_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 25268
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 25272
    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_11

    .line 25273
    const/4 v0, 0x7

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getDeviceIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/2addr v0, v2

    .line 25276
    :goto_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_6

    .line 25277
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->deviceType_:I

    invoke-static {v6, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 25280
    :cond_6
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_7

    .line 25281
    const/16 v1, 0x9

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->minorDeviceType_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    move v1, v4

    move v2, v0

    .line 25284
    :goto_4
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->alerts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 25285
    const/16 v3, 0xa

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->alerts_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 25284
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_4

    .line 25288
    :cond_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_10

    .line 25289
    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->storeAddressBook_:Z

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v0

    add-int/2addr v0, v2

    .line 25292
    :goto_5
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_9

    .line 25293
    const/16 v1, 0xc

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->linkAccounts_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 25296
    :cond_9
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x400

    const/16 v2, 0x400

    if-ne v1, v2, :cond_a

    .line 25297
    const/16 v1, 0xd

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->vgoodPurchaseDaysLeft_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 25300
    :cond_a
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x800

    const/16 v2, 0x800

    if-ne v1, v2, :cond_b

    .line 25301
    const/16 v1, 0xe

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->vmailUpgradeable_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 25304
    :cond_b
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x1000

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_c

    .line 25305
    const/16 v1, 0xf

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->goWithAddressBookCompare_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 25308
    :cond_c
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x2000

    const/16 v2, 0x2000

    if-ne v1, v2, :cond_d

    .line 25309
    const/16 v1, 0x10

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->byWaitingAbookLoading_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 25312
    :cond_d
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x4000

    const/16 v2, 0x4000

    if-ne v1, v2, :cond_e

    .line 25313
    const/16 v1, 0x11

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->registered_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 25316
    :cond_e
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    const v2, 0x8000

    and-int/2addr v1, v2

    const v2, 0x8000

    if-ne v1, v2, :cond_f

    .line 25317
    const/16 v1, 0x12

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->registrationOptions_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25320
    :cond_f
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->memoizedSerializedSize:I

    goto/16 :goto_0

    :cond_10
    move v0, v2

    goto :goto_5

    :cond_11
    move v0, v2

    goto/16 :goto_3

    :cond_12
    move v0, v4

    goto/16 :goto_1
.end method

.method public getStoreAddressBook()Z
    .locals 1

    .prologue
    .line 25061
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->storeAddressBook_:Z

    return v0
.end method

.method public getValidationcode()Ljava/lang/String;
    .locals 2

    .prologue
    .line 24903
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->validationcode_:Ljava/lang/Object;

    .line 24904
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 24905
    check-cast v0, Ljava/lang/String;

    .line 24913
    :goto_0
    return-object v0

    .line 24907
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 24909
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 24910
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 24911
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->validationcode_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 24913
    goto :goto_0
.end method

.method public getVgoodPurchaseDaysLeft()I
    .locals 1

    .prologue
    .line 25081
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->vgoodPurchaseDaysLeft_:I

    return v0
.end method

.method public getVmailUpgradeable()Z
    .locals 1

    .prologue
    .line 25091
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->vmailUpgradeable_:Z

    return v0
.end method

.method public hasAccessAddressBook()Z
    .locals 2

    .prologue
    .line 24880
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 24870
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasByWaitingAbookLoading()Z
    .locals 2

    .prologue
    .line 25108
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasContact()Z
    .locals 2

    .prologue
    .line 24890
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDeviceId()Z
    .locals 2

    .prologue
    .line 24985
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDeviceType()Z
    .locals 2

    .prologue
    .line 25017
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasGoWithAddressBookCompare()Z
    .locals 2

    .prologue
    .line 25098
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLinkAccounts()Z
    .locals 2

    .prologue
    .line 25068
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLocale()Z
    .locals 2

    .prologue
    .line 24932
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMinorDeviceType()Z
    .locals 2

    .prologue
    .line 25027
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRegistered()Z
    .locals 2

    .prologue
    .line 25118
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRegistrationOptions()Z
    .locals 2

    .prologue
    const v1, 0x8000

    .line 25128
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasStoreAddressBook()Z
    .locals 2

    .prologue
    .line 25058
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasValidationcode()Z
    .locals 2

    .prologue
    .line 24900
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVgoodPurchaseDaysLeft()Z
    .locals 2

    .prologue
    .line 25078
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVmailUpgradeable()Z
    .locals 2

    .prologue
    .line 25088
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 25156
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->memoizedIsInitialized:B

    .line 25157
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 25180
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 25157
    goto :goto_0

    .line 25159
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 25160
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 25161
    goto :goto_0

    .line 25163
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 25164
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 25165
    goto :goto_0

    .line 25167
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->hasContact()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 25168
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    .line 25169
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 25170
    goto :goto_0

    :cond_4
    move v0, v2

    .line 25173
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getCountryCodeCount()I

    move-result v1

    if-ge v0, v1, :cond_6

    .line 25174
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getCountryCode(I)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_5

    .line 25175
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 25176
    goto :goto_0

    .line 25173
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 25179
    :cond_6
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 25180
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 24847
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1

    .prologue
    .line 25397
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 24847
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;
    .locals 1

    .prologue
    .line 25401
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;)Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 25326
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 6
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 25185
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getSerializedSize()I

    .line 25186
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 25187
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 25189
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 25190
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->accessAddressBook_:Z

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 25192
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 25193
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 25195
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_3

    .line 25196
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getValidationcodeBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 25198
    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 25199
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getLocaleBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_4
    move v1, v3

    .line 25201
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->countryCode_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 25202
    const/4 v2, 0x6

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->countryCode_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 25201
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 25204
    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 25205
    const/4 v0, 0x7

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getDeviceIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 25207
    :cond_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_7

    .line 25208
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->deviceType_:I

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 25210
    :cond_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_8

    .line 25211
    const/16 v0, 0x9

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->minorDeviceType_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_8
    move v1, v3

    .line 25213
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->alerts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 25214
    const/16 v2, 0xa

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->alerts_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 25213
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 25216
    :cond_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_a

    .line 25217
    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->storeAddressBook_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 25219
    :cond_a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_b

    .line 25220
    const/16 v0, 0xc

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->linkAccounts_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 25222
    :cond_b
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_c

    .line 25223
    const/16 v0, 0xd

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->vgoodPurchaseDaysLeft_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 25225
    :cond_c
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_d

    .line 25226
    const/16 v0, 0xe

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->vmailUpgradeable_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 25228
    :cond_d
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_e

    .line 25229
    const/16 v0, 0xf

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->goWithAddressBookCompare_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 25231
    :cond_e
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_f

    .line 25232
    const/16 v0, 0x10

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->byWaitingAbookLoading_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 25234
    :cond_f
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_10

    .line 25235
    const/16 v0, 0x11

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->registered_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 25237
    :cond_10
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->bitField0_:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_11

    .line 25238
    const/16 v0, 0x12

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->registrationOptions_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 25240
    :cond_11
    return-void
.end method
