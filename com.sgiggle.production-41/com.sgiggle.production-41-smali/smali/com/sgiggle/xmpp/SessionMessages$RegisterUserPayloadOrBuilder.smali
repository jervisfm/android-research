.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "RegisterUserPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getAccessAddressBook()Z
.end method

.method public abstract getAlerts(I)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
.end method

.method public abstract getAlertsCount()I
.end method

.method public abstract getAlertsList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getByWaitingAbookLoading()Z
.end method

.method public abstract getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;
.end method

.method public abstract getCountryCode(I)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
.end method

.method public abstract getCountryCodeCount()I
.end method

.method public abstract getCountryCodeList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CountryCode;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getDeviceId()Ljava/lang/String;
.end method

.method public abstract getDeviceType()I
.end method

.method public abstract getGoWithAddressBookCompare()Z
.end method

.method public abstract getLinkAccounts()Z
.end method

.method public abstract getLocale()Ljava/lang/String;
.end method

.method public abstract getMinorDeviceType()I
.end method

.method public abstract getRegistered()Z
.end method

.method public abstract getRegistrationOptions()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
.end method

.method public abstract getStoreAddressBook()Z
.end method

.method public abstract getValidationcode()Ljava/lang/String;
.end method

.method public abstract getVgoodPurchaseDaysLeft()I
.end method

.method public abstract getVmailUpgradeable()Z
.end method

.method public abstract hasAccessAddressBook()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasByWaitingAbookLoading()Z
.end method

.method public abstract hasContact()Z
.end method

.method public abstract hasDeviceId()Z
.end method

.method public abstract hasDeviceType()Z
.end method

.method public abstract hasGoWithAddressBookCompare()Z
.end method

.method public abstract hasLinkAccounts()Z
.end method

.method public abstract hasLocale()Z
.end method

.method public abstract hasMinorDeviceType()Z
.end method

.method public abstract hasRegistered()Z
.end method

.method public abstract hasRegistrationOptions()Z
.end method

.method public abstract hasStoreAddressBook()Z
.end method

.method public abstract hasValidationcode()Z
.end method

.method public abstract hasVgoodPurchaseDaysLeft()Z
.end method

.method public abstract hasVmailUpgradeable()Z
.end method
