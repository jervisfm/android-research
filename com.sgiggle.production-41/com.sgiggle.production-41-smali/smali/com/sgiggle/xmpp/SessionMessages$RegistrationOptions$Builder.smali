.class public final Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptionsOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;",
        "Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptionsOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

.field private facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

.field private facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

.field private prefillContactInfo_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

.field private registrationLayout_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 26784
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 26971
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;->TANGO_BOTTOM:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->registrationLayout_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    .line 26995
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

    .line 27051
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

    .line 27107
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

    .line 27163
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;->PREFILL_ENABLE:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->prefillContactInfo_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    .line 26785
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->maybeForceBuilderInitialization()V

    .line 26786
    return-void
.end method

.method static synthetic access$33900(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 26779
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$34000()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;
    .locals 1

    .prologue
    .line 26779
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 26827
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v0

    .line 26828
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 26829
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 26832
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;
    .locals 1

    .prologue
    .line 26791
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;-><init>()V

    return-object v0
.end method

.method private ensureFacebookExtendedPermissionsIsMutable()V
    .locals 2

    .prologue
    .line 27109
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 27110
    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

    .line 27111
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    .line 27113
    :cond_0
    return-void
.end method

.method private ensureFacebookOpenGraphPermissionsIsMutable()V
    .locals 2

    .prologue
    .line 27053
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 27054
    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

    .line 27055
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    .line 27057
    :cond_0
    return-void
.end method

.method private ensureFacebookUserAndFriendsPermissionsIsMutable()V
    .locals 2

    .prologue
    .line 26997
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 26998
    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

    .line 26999
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    .line 27001
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 26789
    return-void
.end method


# virtual methods
.method public addAllFacebookExtendedPermissions(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;"
        }
    .end annotation

    .prologue
    .line 27145
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->ensureFacebookExtendedPermissionsIsMutable()V

    .line 27146
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 27148
    return-object p0
.end method

.method public addAllFacebookOpenGraphPermissions(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;"
        }
    .end annotation

    .prologue
    .line 27089
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->ensureFacebookOpenGraphPermissionsIsMutable()V

    .line 27090
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 27092
    return-object p0
.end method

.method public addAllFacebookUserAndFriendsPermissions(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;"
        }
    .end annotation

    .prologue
    .line 27033
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->ensureFacebookUserAndFriendsPermissionsIsMutable()V

    .line 27034
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 27036
    return-object p0
.end method

.method public addFacebookExtendedPermissions(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;
    .locals 1
    .parameter

    .prologue
    .line 27135
    if-nez p1, :cond_0

    .line 27136
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 27138
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->ensureFacebookExtendedPermissionsIsMutable()V

    .line 27139
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    .line 27141
    return-object p0
.end method

.method addFacebookExtendedPermissions(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 27157
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->ensureFacebookExtendedPermissionsIsMutable()V

    .line 27158
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    .line 27160
    return-void
.end method

.method public addFacebookOpenGraphPermissions(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;
    .locals 1
    .parameter

    .prologue
    .line 27079
    if-nez p1, :cond_0

    .line 27080
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 27082
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->ensureFacebookOpenGraphPermissionsIsMutable()V

    .line 27083
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    .line 27085
    return-object p0
.end method

.method addFacebookOpenGraphPermissions(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 27101
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->ensureFacebookOpenGraphPermissionsIsMutable()V

    .line 27102
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    .line 27104
    return-void
.end method

.method public addFacebookUserAndFriendsPermissions(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;
    .locals 1
    .parameter

    .prologue
    .line 27023
    if-nez p1, :cond_0

    .line 27024
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 27026
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->ensureFacebookUserAndFriendsPermissionsIsMutable()V

    .line 27027
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    .line 27029
    return-object p0
.end method

.method addFacebookUserAndFriendsPermissions(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 27045
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->ensureFacebookUserAndFriendsPermissionsIsMutable()V

    .line 27046
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    .line 27048
    return-void
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 26779
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    .locals 2

    .prologue
    .line 26818
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v0

    .line 26819
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 26820
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 26822
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 26779
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    .locals 5

    .prologue
    .line 26836
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;-><init>(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 26837
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    .line 26838
    const/4 v2, 0x0

    .line 26839
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 26840
    or-int/lit8 v2, v2, 0x1

    .line 26842
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->registrationLayout_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->registrationLayout_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->access$34202(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    .line 26843
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 26844
    new-instance v3, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v3, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

    .line 26846
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x3

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    .line 26848
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->access$34302(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    .line 26849
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 26850
    new-instance v3, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v3, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

    .line 26852
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x5

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    .line 26854
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->access$34402(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    .line 26855
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 26856
    new-instance v3, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v3, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

    .line 26858
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x9

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    .line 26860
    :cond_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->access$34502(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    .line 26861
    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_4

    .line 26862
    or-int/lit8 v1, v2, 0x2

    .line 26864
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->prefillContactInfo_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->prefillContactInfo_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->access$34602(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    .line 26865
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->access$34702(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;I)I

    .line 26866
    return-object v0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 26779
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 26779
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;
    .locals 1

    .prologue
    .line 26795
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 26796
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;->TANGO_BOTTOM:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->registrationLayout_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    .line 26797
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    .line 26798
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

    .line 26799
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    .line 26800
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

    .line 26801
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    .line 26802
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

    .line 26803
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    .line 26804
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;->PREFILL_ENABLE:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->prefillContactInfo_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    .line 26805
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    .line 26806
    return-object p0
.end method

.method public clearFacebookExtendedPermissions()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;
    .locals 1

    .prologue
    .line 27151
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

    .line 27152
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    .line 27154
    return-object p0
.end method

.method public clearFacebookOpenGraphPermissions()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;
    .locals 1

    .prologue
    .line 27095
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

    .line 27096
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    .line 27098
    return-object p0
.end method

.method public clearFacebookUserAndFriendsPermissions()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;
    .locals 1

    .prologue
    .line 27039
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

    .line 27040
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    .line 27042
    return-object p0
.end method

.method public clearPrefillContactInfo()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;
    .locals 1

    .prologue
    .line 27180
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    .line 27181
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;->PREFILL_ENABLE:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->prefillContactInfo_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    .line 27183
    return-object p0
.end method

.method public clearRegistrationLayout()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;
    .locals 1

    .prologue
    .line 26988
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    .line 26989
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;->TANGO_BOTTOM:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->registrationLayout_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    .line 26991
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 26779
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 26779
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 26779
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;
    .locals 2

    .prologue
    .line 26810
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 26779
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 26779
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 26779
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    .locals 1

    .prologue
    .line 26814
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v0

    return-object v0
.end method

.method public getFacebookExtendedPermissions(I)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 27122
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getFacebookExtendedPermissionsCount()I
    .locals 1

    .prologue
    .line 27119
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public getFacebookExtendedPermissionsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27116
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFacebookOpenGraphPermissions(I)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 27066
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getFacebookOpenGraphPermissionsCount()I
    .locals 1

    .prologue
    .line 27063
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public getFacebookOpenGraphPermissionsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27060
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFacebookUserAndFriendsPermissions(I)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 27010
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getFacebookUserAndFriendsPermissionsCount()I
    .locals 1

    .prologue
    .line 27007
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public getFacebookUserAndFriendsPermissionsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27004
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPrefillContactInfo()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;
    .locals 1

    .prologue
    .line 27168
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->prefillContactInfo_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    return-object v0
.end method

.method public getRegistrationLayout()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;
    .locals 1

    .prologue
    .line 26976
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->registrationLayout_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    return-object v0
.end method

.method public hasPrefillContactInfo()Z
    .locals 2

    .prologue
    .line 27165
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRegistrationLayout()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 26973
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 26911
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 26779
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 26779
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 26779
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 26919
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 26920
    sparse-switch v0, :sswitch_data_0

    .line 26925
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 26927
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 26923
    goto :goto_1

    .line 26932
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 26933
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    move-result-object v0

    .line 26934
    if-eqz v0, :cond_0

    .line 26935
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    .line 26936
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->registrationLayout_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    goto :goto_0

    .line 26941
    :sswitch_2
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->ensureFacebookUserAndFriendsPermissionsIsMutable()V

    .line 26942
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto :goto_0

    .line 26946
    :sswitch_3
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->ensureFacebookOpenGraphPermissionsIsMutable()V

    .line 26947
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto :goto_0

    .line 26951
    :sswitch_4
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->ensureFacebookExtendedPermissionsIsMutable()V

    .line 26952
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto :goto_0

    .line 26956
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 26957
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    move-result-object v0

    .line 26958
    if-eqz v0, :cond_0

    .line 26959
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    .line 26960
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->prefillContactInfo_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    goto :goto_0

    .line 26920
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;
    .locals 2
    .parameter

    .prologue
    .line 26870
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 26907
    :goto_0
    return-object v0

    .line 26871
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->hasRegistrationLayout()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 26872
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->getRegistrationLayout()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->setRegistrationLayout(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    .line 26874
    :cond_1
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->access$34300(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 26875
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 26876
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->access$34300(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

    .line 26877
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    .line 26884
    :cond_2
    :goto_1
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->access$34400(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 26885
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 26886
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->access$34400(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

    .line 26887
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    .line 26894
    :cond_3
    :goto_2
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->access$34500(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 26895
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 26896
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->access$34500(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

    .line 26897
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    .line 26904
    :cond_4
    :goto_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->hasPrefillContactInfo()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 26905
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->getPrefillContactInfo()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->setPrefillContactInfo(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    :cond_5
    move-object v0, p0

    .line 26907
    goto :goto_0

    .line 26879
    :cond_6
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->ensureFacebookUserAndFriendsPermissionsIsMutable()V

    .line 26880
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->access$34300(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 26889
    :cond_7
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->ensureFacebookOpenGraphPermissionsIsMutable()V

    .line 26890
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->access$34400(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 26899
    :cond_8
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->ensureFacebookExtendedPermissionsIsMutable()V

    .line 26900
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->access$34500(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto :goto_3
.end method

.method public setFacebookExtendedPermissions(ILjava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 27126
    if-nez p2, :cond_0

    .line 27127
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 27129
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->ensureFacebookExtendedPermissionsIsMutable()V

    .line 27130
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/LazyStringList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 27132
    return-object p0
.end method

.method public setFacebookOpenGraphPermissions(ILjava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 27070
    if-nez p2, :cond_0

    .line 27071
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 27073
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->ensureFacebookOpenGraphPermissionsIsMutable()V

    .line 27074
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/LazyStringList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 27076
    return-object p0
.end method

.method public setFacebookUserAndFriendsPermissions(ILjava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 27014
    if-nez p2, :cond_0

    .line 27015
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 27017
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->ensureFacebookUserAndFriendsPermissionsIsMutable()V

    .line 27018
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/LazyStringList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 27020
    return-object p0
.end method

.method public setPrefillContactInfo(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;
    .locals 1
    .parameter

    .prologue
    .line 27171
    if-nez p1, :cond_0

    .line 27172
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 27174
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    .line 27175
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->prefillContactInfo_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    .line 27177
    return-object p0
.end method

.method public setRegistrationLayout(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;
    .locals 1
    .parameter

    .prologue
    .line 26979
    if-nez p1, :cond_0

    .line 26980
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 26982
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->bitField0_:I

    .line 26983
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->registrationLayout_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    .line 26985
    return-object p0
.end method
