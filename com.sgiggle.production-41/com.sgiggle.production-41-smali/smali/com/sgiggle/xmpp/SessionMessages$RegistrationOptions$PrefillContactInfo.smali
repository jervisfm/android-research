.class public final enum Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PrefillContactInfo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo; = null

.field public static final enum PREFILL_DISABLE:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo; = null

.field public static final PREFILL_DISABLE_VALUE:I = 0x1

.field public static final enum PREFILL_ENABLE:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

.field public static final PREFILL_ENABLE_VALUE:I

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 26517
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    const-string v1, "PREFILL_ENABLE"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;->PREFILL_ENABLE:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    .line 26518
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    const-string v1, "PREFILL_DISABLE"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;->PREFILL_DISABLE:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    .line 26515
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;->PREFILL_ENABLE:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;->PREFILL_DISABLE:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    .line 26540
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 26549
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 26550
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;->value:I

    .line 26551
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26537
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;
    .locals 1
    .parameter

    .prologue
    .line 26528
    packed-switch p0, :pswitch_data_0

    .line 26531
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 26529
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;->PREFILL_ENABLE:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    goto :goto_0

    .line 26530
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;->PREFILL_DISABLE:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    goto :goto_0

    .line 26528
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;
    .locals 1
    .parameter

    .prologue
    .line 26515
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;
    .locals 1

    .prologue
    .line 26515
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 26525
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;->value:I

    return v0
.end method
