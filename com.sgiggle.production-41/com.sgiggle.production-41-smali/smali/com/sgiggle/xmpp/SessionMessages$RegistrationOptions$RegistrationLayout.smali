.class public final enum Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RegistrationLayout"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout; = null

.field public static final enum TANGO_BOTTOM:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout; = null

.field public static final enum TANGO_BOTTOM_AND_FB_BOTTOM:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout; = null

.field public static final TANGO_BOTTOM_AND_FB_BOTTOM_VALUE:I = 0x2

.field public static final enum TANGO_BOTTOM_AND_FB_TOP:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout; = null

.field public static final TANGO_BOTTOM_AND_FB_TOP_VALUE:I = 0x1

.field public static final TANGO_BOTTOM_VALUE:I

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 26473
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    const-string v1, "TANGO_BOTTOM"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;->TANGO_BOTTOM:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    .line 26474
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    const-string v1, "TANGO_BOTTOM_AND_FB_TOP"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;->TANGO_BOTTOM_AND_FB_TOP:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    .line 26475
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    const-string v1, "TANGO_BOTTOM_AND_FB_BOTTOM"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;->TANGO_BOTTOM_AND_FB_BOTTOM:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    .line 26471
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;->TANGO_BOTTOM:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;->TANGO_BOTTOM_AND_FB_TOP:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;->TANGO_BOTTOM_AND_FB_BOTTOM:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    .line 26499
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 26508
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 26509
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;->value:I

    .line 26510
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26496
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;
    .locals 1
    .parameter

    .prologue
    .line 26486
    packed-switch p0, :pswitch_data_0

    .line 26490
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 26487
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;->TANGO_BOTTOM:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    goto :goto_0

    .line 26488
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;->TANGO_BOTTOM_AND_FB_TOP:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    goto :goto_0

    .line 26489
    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;->TANGO_BOTTOM_AND_FB_BOTTOM:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    goto :goto_0

    .line 26486
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;
    .locals 1
    .parameter

    .prologue
    .line 26471
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;
    .locals 1

    .prologue
    .line 26471
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 26483
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;->value:I

    return v0
.end method
