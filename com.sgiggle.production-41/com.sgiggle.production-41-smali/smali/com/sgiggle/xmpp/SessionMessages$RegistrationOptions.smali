.class public final Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptionsOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RegistrationOptions"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;,
        Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;,
        Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;
    }
.end annotation


# static fields
.field public static final FACEBOOK_EXTENDED_PERMISSIONS_FIELD_NUMBER:I = 0x4

.field public static final FACEBOOK_OPEN_GRAPH_PERMISSIONS_FIELD_NUMBER:I = 0x3

.field public static final FACEBOOK_USER_AND_FRIENDS_PERMISSIONS_FIELD_NUMBER:I = 0x2

.field public static final PREFILL_CONTACT_INFO_FIELD_NUMBER:I = 0x5

.field public static final REGISTRATION_LAYOUT_FIELD_NUMBER:I = 0x1

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;


# instance fields
.field private bitField0_:I

.field private facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

.field private facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

.field private facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private prefillContactInfo_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

.field private registrationLayout_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27190
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    .line 27191
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->initFields()V

    .line 27192
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 26458
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 26626
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->memoizedIsInitialized:B

    .line 26655
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->memoizedSerializedSize:I

    .line 26459
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 26453
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;-><init>(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 26460
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 26626
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->memoizedIsInitialized:B

    .line 26655
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->memoizedSerializedSize:I

    .line 26460
    return-void
.end method

.method static synthetic access$34202(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 26453
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->registrationLayout_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    return-object p1
.end method

.method static synthetic access$34300(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;)Lcom/google/protobuf/LazyStringList;
    .locals 1
    .parameter

    .prologue
    .line 26453
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic access$34302(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 26453
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic access$34400(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;)Lcom/google/protobuf/LazyStringList;
    .locals 1
    .parameter

    .prologue
    .line 26453
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic access$34402(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 26453
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic access$34500(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;)Lcom/google/protobuf/LazyStringList;
    .locals 1
    .parameter

    .prologue
    .line 26453
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic access$34502(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 26453
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic access$34602(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 26453
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->prefillContactInfo_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    return-object p1
.end method

.method static synthetic access$34702(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 26453
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    .locals 1

    .prologue
    .line 26464
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 26620
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;->TANGO_BOTTOM:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->registrationLayout_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    .line 26621
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

    .line 26622
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

    .line 26623
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

    .line 26624
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;->PREFILL_ENABLE:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->prefillContactInfo_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    .line 26625
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;
    .locals 1

    .prologue
    .line 26772
    #calls: Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->access$34000()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;
    .locals 1
    .parameter

    .prologue
    .line 26775
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 26741
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    .line 26742
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 26743
    #calls: Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->access$33900(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v0

    .line 26745
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 26752
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    .line 26753
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 26754
    #calls: Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->access$33900(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v0

    .line 26756
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 26708
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->access$33900(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 26714
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->access$33900(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 26762
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->access$33900(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 26768
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->access$33900(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 26730
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->access$33900(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 26736
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->access$33900(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 26719
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->access$33900(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 26725
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;->access$33900(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 26453
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;
    .locals 1

    .prologue
    .line 26468
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    return-object v0
.end method

.method public getFacebookExtendedPermissions(I)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 26606
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getFacebookExtendedPermissionsCount()I
    .locals 1

    .prologue
    .line 26603
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public getFacebookExtendedPermissionsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26600
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public getFacebookOpenGraphPermissions(I)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 26592
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getFacebookOpenGraphPermissionsCount()I
    .locals 1

    .prologue
    .line 26589
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public getFacebookOpenGraphPermissionsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26586
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public getFacebookUserAndFriendsPermissions(I)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 26578
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getFacebookUserAndFriendsPermissionsCount()I
    .locals 1

    .prologue
    .line 26575
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public getFacebookUserAndFriendsPermissionsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26572
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public getPrefillContactInfo()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;
    .locals 1

    .prologue
    .line 26616
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->prefillContactInfo_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    return-object v0
.end method

.method public getRegistrationLayout()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;
    .locals 1

    .prologue
    .line 26564
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->registrationLayout_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 26657
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->memoizedSerializedSize:I

    .line 26658
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 26697
    :goto_0
    return v0

    .line 26661
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_5

    .line 26662
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->registrationLayout_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;->getNumber()I

    move-result v0

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v0

    add-int/2addr v0, v4

    :goto_1
    move v1, v4

    move v2, v4

    .line 26667
    :goto_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 26668
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v3

    add-int/2addr v2, v3

    .line 26667
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 26671
    :cond_1
    add-int/2addr v0, v2

    .line 26672
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->getFacebookUserAndFriendsPermissionsList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    move v1, v4

    move v2, v4

    .line 26676
    :goto_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 26677
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v3

    add-int/2addr v2, v3

    .line 26676
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 26680
    :cond_2
    add-int/2addr v0, v2

    .line 26681
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->getFacebookOpenGraphPermissionsList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    move v1, v4

    move v2, v4

    .line 26685
    :goto_4
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 26686
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v3

    add-int/2addr v2, v3

    .line 26685
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 26689
    :cond_3
    add-int/2addr v0, v2

    .line 26690
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->getFacebookExtendedPermissionsList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 26692
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_4

    .line 26693
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->prefillContactInfo_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 26696
    :cond_4
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->memoizedSerializedSize:I

    goto/16 :goto_0

    :cond_5
    move v0, v4

    goto/16 :goto_1
.end method

.method public hasPrefillContactInfo()Z
    .locals 2

    .prologue
    .line 26613
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRegistrationLayout()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 26561
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 26628
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->memoizedIsInitialized:B

    .line 26629
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v2, :cond_0

    move v0, v2

    .line 26632
    :goto_0
    return v0

    .line 26629
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 26631
    :cond_1
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->memoizedIsInitialized:B

    move v0, v2

    .line 26632
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 26453
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;
    .locals 1

    .prologue
    .line 26773
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 26453
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;
    .locals 1

    .prologue
    .line 26777
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 26702
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 26637
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->getSerializedSize()I

    .line 26638
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 26639
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->registrationLayout_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;->getNumber()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_0
    move v0, v3

    .line 26641
    :goto_0
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 26642
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookUserAndFriendsPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v4, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 26641
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v3

    .line 26644
    :goto_1
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 26645
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookOpenGraphPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 26644
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v0, v3

    .line 26647
    :goto_2
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 26648
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->facebookExtendedPermissions_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 26647
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 26650
    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_4

    .line 26651
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->prefillContactInfo_:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 26653
    :cond_4
    return-void
.end method
