.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptionsOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "RegistrationOptionsOrBuilder"
.end annotation


# virtual methods
.method public abstract getFacebookExtendedPermissions(I)Ljava/lang/String;
.end method

.method public abstract getFacebookExtendedPermissionsCount()I
.end method

.method public abstract getFacebookExtendedPermissionsList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getFacebookOpenGraphPermissions(I)Ljava/lang/String;
.end method

.method public abstract getFacebookOpenGraphPermissionsCount()I
.end method

.method public abstract getFacebookOpenGraphPermissionsList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getFacebookUserAndFriendsPermissions(I)Ljava/lang/String;
.end method

.method public abstract getFacebookUserAndFriendsPermissionsCount()I
.end method

.method public abstract getFacebookUserAndFriendsPermissionsList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getPrefillContactInfo()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;
.end method

.method public abstract getRegistrationLayout()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;
.end method

.method public abstract hasPrefillContactInfo()Z
.end method

.method public abstract hasRegistrationLayout()Z
.end method
