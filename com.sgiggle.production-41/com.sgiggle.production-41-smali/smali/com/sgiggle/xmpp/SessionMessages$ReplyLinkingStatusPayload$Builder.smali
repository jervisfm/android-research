.class public final Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private status_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 55078
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 55204
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 55247
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->status_:Ljava/lang/Object;

    .line 55079
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->maybeForceBuilderInitialization()V

    .line 55080
    return-void
.end method

.method static synthetic access$70900(Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 55073
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$71000()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;
    .locals 1

    .prologue
    .line 55073
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 55115
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;

    move-result-object v0

    .line 55116
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 55117
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 55120
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;
    .locals 1

    .prologue
    .line 55085
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 55083
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 55073
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;
    .locals 2

    .prologue
    .line 55106
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;

    move-result-object v0

    .line 55107
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 55108
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 55110
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 55073
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;
    .locals 5

    .prologue
    .line 55124
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 55125
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->bitField0_:I

    .line 55126
    const/4 v2, 0x0

    .line 55127
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 55128
    or-int/lit8 v2, v2, 0x1

    .line 55130
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->access$71202(Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 55131
    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    .line 55132
    or-int/lit8 v1, v2, 0x2

    .line 55134
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->status_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->status_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->access$71302(Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55135
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->access$71402(Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;I)I

    .line 55136
    return-object v0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 55073
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 55073
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;
    .locals 1

    .prologue
    .line 55089
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 55090
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 55091
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->bitField0_:I

    .line 55092
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->status_:Ljava/lang/Object;

    .line 55093
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->bitField0_:I

    .line 55094
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;
    .locals 1

    .prologue
    .line 55240
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 55242
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->bitField0_:I

    .line 55243
    return-object p0
.end method

.method public clearStatus()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;
    .locals 1

    .prologue
    .line 55271
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->bitField0_:I

    .line 55272
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->getStatus()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->status_:Ljava/lang/Object;

    .line 55274
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 55073
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 55073
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 55073
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;
    .locals 2

    .prologue
    .line 55098
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 55073
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 55209
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 55073
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 55073
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;
    .locals 1

    .prologue
    .line 55102
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;

    move-result-object v0

    return-object v0
.end method

.method public getStatus()Ljava/lang/String;
    .locals 2

    .prologue
    .line 55252
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->status_:Ljava/lang/Object;

    .line 55253
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 55254
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 55255
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->status_:Ljava/lang/Object;

    .line 55258
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 55206
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasStatus()Z
    .locals 2

    .prologue
    .line 55249
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 55151
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 55163
    :goto_0
    return v0

    .line 55155
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->hasStatus()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 55157
    goto :goto_0

    .line 55159
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 55161
    goto :goto_0

    .line 55163
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 55228
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 55230
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 55236
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->bitField0_:I

    .line 55237
    return-object p0

    .line 55233
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55073
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 55073
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55073
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55171
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 55172
    sparse-switch v0, :sswitch_data_0

    .line 55177
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 55179
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 55175
    goto :goto_1

    .line 55184
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 55185
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 55186
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 55188
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 55189
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    goto :goto_0

    .line 55193
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->bitField0_:I

    .line 55194
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->status_:Ljava/lang/Object;

    goto :goto_0

    .line 55172
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 55140
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 55147
    :goto_0
    return-object v0

    .line 55141
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55142
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    .line 55144
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->hasStatus()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 55145
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->getStatus()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->setStatus(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    :cond_2
    move-object v0, p0

    .line 55147
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 55222
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 55224
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->bitField0_:I

    .line 55225
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 55212
    if-nez p1, :cond_0

    .line 55213
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 55215
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 55217
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->bitField0_:I

    .line 55218
    return-object p0
.end method

.method public setStatus(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 55262
    if-nez p1, :cond_0

    .line 55263
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 55265
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->bitField0_:I

    .line 55266
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->status_:Ljava/lang/Object;

    .line 55268
    return-object p0
.end method

.method setStatus(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 55277
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->bitField0_:I

    .line 55278
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->status_:Ljava/lang/Object;

    .line 55280
    return-void
.end method
