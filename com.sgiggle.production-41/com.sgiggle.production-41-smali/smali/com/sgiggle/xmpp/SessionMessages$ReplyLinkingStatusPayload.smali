.class public final Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReplyLinkingStatusPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final STATUS_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private status_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 55286
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;

    .line 55287
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->initFields()V

    .line 55288
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 54884
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 54944
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->memoizedIsInitialized:B

    .line 54976
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->memoizedSerializedSize:I

    .line 54885
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 54879
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 54886
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 54944
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->memoizedIsInitialized:B

    .line 54976
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->memoizedSerializedSize:I

    .line 54886
    return-void
.end method

.method static synthetic access$71202(Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 54879
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$71302(Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 54879
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->status_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$71402(Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 54879
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;
    .locals 1

    .prologue
    .line 54890
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;

    return-object v0
.end method

.method private getStatusBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 54929
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->status_:Ljava/lang/Object;

    .line 54930
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 54931
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 54933
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->status_:Ljava/lang/Object;

    .line 54936
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 54941
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 54942
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->status_:Ljava/lang/Object;

    .line 54943
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;
    .locals 1

    .prologue
    .line 55066
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->access$71000()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 55069
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55035
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    move-result-object v0

    .line 55036
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 55037
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->access$70900(Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;

    move-result-object v0

    .line 55039
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55046
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    move-result-object v0

    .line 55047
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 55048
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->access$70900(Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;

    move-result-object v0

    .line 55050
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 55002
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->access$70900(Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 55008
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->access$70900(Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55056
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->access$70900(Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55062
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->access$70900(Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55024
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->access$70900(Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55030
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->access$70900(Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 55013
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->access$70900(Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 55019
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;->access$70900(Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 54905
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 54879
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;
    .locals 1

    .prologue
    .line 54894
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 54978
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->memoizedSerializedSize:I

    .line 54979
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 54991
    :goto_0
    return v0

    .line 54981
    :cond_0
    const/4 v0, 0x0

    .line 54982
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 54983
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 54986
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 54987
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->getStatusBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 54990
    :cond_2
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getStatus()Ljava/lang/String;
    .locals 2

    .prologue
    .line 54915
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->status_:Ljava/lang/Object;

    .line 54916
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 54917
    check-cast v0, Ljava/lang/String;

    .line 54925
    :goto_0
    return-object v0

    .line 54919
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 54921
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 54922
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 54923
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->status_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 54925
    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 54902
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasStatus()Z
    .locals 2

    .prologue
    .line 54912
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 54946
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->memoizedIsInitialized:B

    .line 54947
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 54962
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 54947
    goto :goto_0

    .line 54949
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 54950
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 54951
    goto :goto_0

    .line 54953
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->hasStatus()Z

    move-result v0

    if-nez v0, :cond_3

    .line 54954
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 54955
    goto :goto_0

    .line 54957
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    .line 54958
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 54959
    goto :goto_0

    .line 54961
    :cond_4
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 54962
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 54879
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;
    .locals 1

    .prologue
    .line 55067
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 54879
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;
    .locals 1

    .prologue
    .line 55071
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;)Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 54996
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 54967
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->getSerializedSize()I

    .line 54968
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 54969
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 54971
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 54972
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReplyLinkingStatusPayload;->getStatusBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 54974
    :cond_1
    return-void
.end method
