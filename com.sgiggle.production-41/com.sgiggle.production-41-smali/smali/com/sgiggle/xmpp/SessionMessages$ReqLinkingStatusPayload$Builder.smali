.class public final Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private countryCode_:Ljava/lang/Object;

.field private primarySubscriberNumber_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 54599
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 54743
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 54786
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->countryCode_:Ljava/lang/Object;

    .line 54822
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->primarySubscriberNumber_:Ljava/lang/Object;

    .line 54600
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->maybeForceBuilderInitialization()V

    .line 54601
    return-void
.end method

.method static synthetic access$70200(Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 54594
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$70300()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;
    .locals 1

    .prologue
    .line 54594
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 54638
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;

    move-result-object v0

    .line 54639
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 54640
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 54643
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;
    .locals 1

    .prologue
    .line 54606
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 54604
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 54594
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;
    .locals 2

    .prologue
    .line 54629
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;

    move-result-object v0

    .line 54630
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 54631
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 54633
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 54594
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;
    .locals 5

    .prologue
    .line 54647
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 54648
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    .line 54649
    const/4 v2, 0x0

    .line 54650
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 54651
    or-int/lit8 v2, v2, 0x1

    .line 54653
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->access$70502(Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 54654
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 54655
    or-int/lit8 v2, v2, 0x2

    .line 54657
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->countryCode_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->countryCode_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->access$70602(Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54658
    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_2

    .line 54659
    or-int/lit8 v1, v2, 0x4

    .line 54661
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->primarySubscriberNumber_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->primarySubscriberNumber_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->access$70702(Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54662
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->access$70802(Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;I)I

    .line 54663
    return-object v0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 54594
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 54594
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;
    .locals 1

    .prologue
    .line 54610
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 54611
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 54612
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    .line 54613
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->countryCode_:Ljava/lang/Object;

    .line 54614
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    .line 54615
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->primarySubscriberNumber_:Ljava/lang/Object;

    .line 54616
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    .line 54617
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;
    .locals 1

    .prologue
    .line 54779
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 54781
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    .line 54782
    return-object p0
.end method

.method public clearCountryCode()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;
    .locals 1

    .prologue
    .line 54810
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    .line 54811
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->getCountryCode()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->countryCode_:Ljava/lang/Object;

    .line 54813
    return-object p0
.end method

.method public clearPrimarySubscriberNumber()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;
    .locals 1

    .prologue
    .line 54846
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    .line 54847
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->getPrimarySubscriberNumber()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->primarySubscriberNumber_:Ljava/lang/Object;

    .line 54849
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 54594
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 54594
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 54594
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;
    .locals 2

    .prologue
    .line 54621
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 54594
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 54748
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCountryCode()Ljava/lang/String;
    .locals 2

    .prologue
    .line 54791
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->countryCode_:Ljava/lang/Object;

    .line 54792
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 54793
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 54794
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->countryCode_:Ljava/lang/Object;

    .line 54797
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 54594
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 54594
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;
    .locals 1

    .prologue
    .line 54625
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;

    move-result-object v0

    return-object v0
.end method

.method public getPrimarySubscriberNumber()Ljava/lang/String;
    .locals 2

    .prologue
    .line 54827
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->primarySubscriberNumber_:Ljava/lang/Object;

    .line 54828
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 54829
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 54830
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->primarySubscriberNumber_:Ljava/lang/Object;

    .line 54833
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 54745
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCountryCode()Z
    .locals 2

    .prologue
    .line 54788
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPrimarySubscriberNumber()Z
    .locals 2

    .prologue
    .line 54824
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 54681
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 54697
    :goto_0
    return v0

    .line 54685
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->hasCountryCode()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 54687
    goto :goto_0

    .line 54689
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->hasPrimarySubscriberNumber()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 54691
    goto :goto_0

    .line 54693
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 54695
    goto :goto_0

    .line 54697
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 54767
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 54769
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 54775
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    .line 54776
    return-object p0

    .line 54772
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54594
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 54594
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54594
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54705
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 54706
    sparse-switch v0, :sswitch_data_0

    .line 54711
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 54713
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 54709
    goto :goto_1

    .line 54718
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 54719
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 54720
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 54722
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 54723
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    goto :goto_0

    .line 54727
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    .line 54728
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->countryCode_:Ljava/lang/Object;

    goto :goto_0

    .line 54732
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    .line 54733
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->primarySubscriberNumber_:Ljava/lang/Object;

    goto :goto_0

    .line 54706
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 54667
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 54677
    :goto_0
    return-object v0

    .line 54668
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 54669
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    .line 54671
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->hasCountryCode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 54672
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->getCountryCode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->setCountryCode(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    .line 54674
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->hasPrimarySubscriberNumber()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 54675
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->getPrimarySubscriberNumber()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->setPrimarySubscriberNumber(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    :cond_3
    move-object v0, p0

    .line 54677
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 54761
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 54763
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    .line 54764
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 54751
    if-nez p1, :cond_0

    .line 54752
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 54754
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 54756
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    .line 54757
    return-object p0
.end method

.method public setCountryCode(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 54801
    if-nez p1, :cond_0

    .line 54802
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 54804
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    .line 54805
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->countryCode_:Ljava/lang/Object;

    .line 54807
    return-object p0
.end method

.method setCountryCode(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 54816
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    .line 54817
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->countryCode_:Ljava/lang/Object;

    .line 54819
    return-void
.end method

.method public setPrimarySubscriberNumber(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 54837
    if-nez p1, :cond_0

    .line 54838
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 54840
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    .line 54841
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->primarySubscriberNumber_:Ljava/lang/Object;

    .line 54843
    return-object p0
.end method

.method setPrimarySubscriberNumber(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 54852
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->bitField0_:I

    .line 54853
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->primarySubscriberNumber_:Ljava/lang/Object;

    .line 54855
    return-void
.end method
