.class public final Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReqLinkingStatusPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final COUNTRY_CODE_FIELD_NUMBER:I = 0x2

.field public static final PRIMARY_SUBSCRIBER_NUMBER_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private countryCode_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private primarySubscriberNumber_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 54861
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;

    .line 54862
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->initFields()V

    .line 54863
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 54361
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 54454
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->memoizedIsInitialized:B

    .line 54493
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->memoizedSerializedSize:I

    .line 54362
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 54356
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 54363
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 54454
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->memoizedIsInitialized:B

    .line 54493
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->memoizedSerializedSize:I

    .line 54363
    return-void
.end method

.method static synthetic access$70502(Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 54356
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$70602(Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 54356
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->countryCode_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$70702(Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 54356
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->primarySubscriberNumber_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$70802(Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 54356
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->bitField0_:I

    return p1
.end method

.method private getCountryCodeBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 54406
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->countryCode_:Ljava/lang/Object;

    .line 54407
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 54408
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 54410
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->countryCode_:Ljava/lang/Object;

    .line 54413
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;
    .locals 1

    .prologue
    .line 54367
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;

    return-object v0
.end method

.method private getPrimarySubscriberNumberBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 54438
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->primarySubscriberNumber_:Ljava/lang/Object;

    .line 54439
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 54440
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 54442
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->primarySubscriberNumber_:Ljava/lang/Object;

    .line 54445
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 54450
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 54451
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->countryCode_:Ljava/lang/Object;

    .line 54452
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->primarySubscriberNumber_:Ljava/lang/Object;

    .line 54453
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;
    .locals 1

    .prologue
    .line 54587
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->access$70300()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 54590
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54556
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    move-result-object v0

    .line 54557
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 54558
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->access$70200(Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;

    move-result-object v0

    .line 54560
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54567
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    move-result-object v0

    .line 54568
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 54569
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->access$70200(Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;

    move-result-object v0

    .line 54571
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 54523
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->access$70200(Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 54529
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->access$70200(Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54577
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->access$70200(Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54583
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->access$70200(Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54545
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->access$70200(Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54551
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->access$70200(Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 54534
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->access$70200(Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 54540
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;->access$70200(Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 54382
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCountryCode()Ljava/lang/String;
    .locals 2

    .prologue
    .line 54392
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->countryCode_:Ljava/lang/Object;

    .line 54393
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 54394
    check-cast v0, Ljava/lang/String;

    .line 54402
    :goto_0
    return-object v0

    .line 54396
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 54398
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 54399
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 54400
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->countryCode_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 54402
    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 54356
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;
    .locals 1

    .prologue
    .line 54371
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;

    return-object v0
.end method

.method public getPrimarySubscriberNumber()Ljava/lang/String;
    .locals 2

    .prologue
    .line 54424
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->primarySubscriberNumber_:Ljava/lang/Object;

    .line 54425
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 54426
    check-cast v0, Ljava/lang/String;

    .line 54434
    :goto_0
    return-object v0

    .line 54428
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 54430
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 54431
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 54432
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->primarySubscriberNumber_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 54434
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 54495
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->memoizedSerializedSize:I

    .line 54496
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 54512
    :goto_0
    return v0

    .line 54498
    :cond_0
    const/4 v0, 0x0

    .line 54499
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 54500
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 54503
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 54504
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->getCountryCodeBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 54507
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 54508
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->getPrimarySubscriberNumberBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 54511
    :cond_3
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 54379
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCountryCode()Z
    .locals 2

    .prologue
    .line 54389
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPrimarySubscriberNumber()Z
    .locals 2

    .prologue
    .line 54421
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 54456
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->memoizedIsInitialized:B

    .line 54457
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 54476
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 54457
    goto :goto_0

    .line 54459
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 54460
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 54461
    goto :goto_0

    .line 54463
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->hasCountryCode()Z

    move-result v0

    if-nez v0, :cond_3

    .line 54464
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 54465
    goto :goto_0

    .line 54467
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->hasPrimarySubscriberNumber()Z

    move-result v0

    if-nez v0, :cond_4

    .line 54468
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 54469
    goto :goto_0

    .line 54471
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_5

    .line 54472
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 54473
    goto :goto_0

    .line 54475
    :cond_5
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 54476
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 54356
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;
    .locals 1

    .prologue
    .line 54588
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 54356
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;
    .locals 1

    .prologue
    .line 54592
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;)Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 54517
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 54481
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->getSerializedSize()I

    .line 54482
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 54483
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 54485
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 54486
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->getCountryCodeBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 54488
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 54489
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayload;->getPrimarySubscriberNumberBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 54491
    :cond_2
    return-void
.end method
