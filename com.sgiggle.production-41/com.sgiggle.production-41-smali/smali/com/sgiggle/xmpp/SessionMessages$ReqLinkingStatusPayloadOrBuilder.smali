.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$ReqLinkingStatusPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ReqLinkingStatusPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getCountryCode()Ljava/lang/String;
.end method

.method public abstract getPrimarySubscriberNumber()Ljava/lang/String;
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasCountryCode()Z
.end method

.method public abstract hasPrimarySubscriberNumber()Z
.end method
