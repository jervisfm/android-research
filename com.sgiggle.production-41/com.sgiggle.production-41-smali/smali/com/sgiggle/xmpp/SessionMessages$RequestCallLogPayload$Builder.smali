.class public final Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private clearUnreadNumber_:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 53272
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 53394
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 53273
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->maybeForceBuilderInitialization()V

    .line 53274
    return-void
.end method

.method static synthetic access$68400(Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 53267
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$68500()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;
    .locals 1

    .prologue
    .line 53267
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 53309
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;

    move-result-object v0

    .line 53310
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 53311
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 53314
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;
    .locals 1

    .prologue
    .line 53279
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 53277
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 53267
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;
    .locals 2

    .prologue
    .line 53300
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;

    move-result-object v0

    .line 53301
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 53302
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 53304
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 53267
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;
    .locals 5

    .prologue
    .line 53318
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 53319
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->bitField0_:I

    .line 53320
    const/4 v2, 0x0

    .line 53321
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 53322
    or-int/lit8 v2, v2, 0x1

    .line 53324
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->access$68702(Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 53325
    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    .line 53326
    or-int/lit8 v1, v2, 0x2

    .line 53328
    :goto_0
    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->clearUnreadNumber_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->clearUnreadNumber_:Z
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->access$68802(Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;Z)Z

    .line 53329
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->access$68902(Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;I)I

    .line 53330
    return-object v0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 53267
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 53267
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;
    .locals 1

    .prologue
    .line 53283
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 53284
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 53285
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->bitField0_:I

    .line 53286
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->clearUnreadNumber_:Z

    .line 53287
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->bitField0_:I

    .line 53288
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;
    .locals 1

    .prologue
    .line 53430
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 53432
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->bitField0_:I

    .line 53433
    return-object p0
.end method

.method public clearClearUnreadNumber()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;
    .locals 1

    .prologue
    .line 53451
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->bitField0_:I

    .line 53452
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->clearUnreadNumber_:Z

    .line 53454
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 53267
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 53267
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 53267
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;
    .locals 2

    .prologue
    .line 53292
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 53267
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 53399
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getClearUnreadNumber()Z
    .locals 1

    .prologue
    .line 53442
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->clearUnreadNumber_:Z

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 53267
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 53267
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;
    .locals 1

    .prologue
    .line 53296
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;

    move-result-object v0

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 53396
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasClearUnreadNumber()Z
    .locals 2

    .prologue
    .line 53439
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 53345
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 53353
    :goto_0
    return v0

    .line 53349
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 53351
    goto :goto_0

    .line 53353
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 53418
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 53420
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 53426
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->bitField0_:I

    .line 53427
    return-object p0

    .line 53423
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53267
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 53267
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53267
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53361
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 53362
    sparse-switch v0, :sswitch_data_0

    .line 53367
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 53369
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 53365
    goto :goto_1

    .line 53374
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 53375
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 53376
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 53378
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 53379
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    goto :goto_0

    .line 53383
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->bitField0_:I

    .line 53384
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->clearUnreadNumber_:Z

    goto :goto_0

    .line 53362
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 53334
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 53341
    :goto_0
    return-object v0

    .line 53335
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 53336
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    .line 53338
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->hasClearUnreadNumber()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 53339
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->getClearUnreadNumber()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->setClearUnreadNumber(Z)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    :cond_2
    move-object v0, p0

    .line 53341
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 53412
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 53414
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->bitField0_:I

    .line 53415
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 53402
    if-nez p1, :cond_0

    .line 53403
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 53405
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 53407
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->bitField0_:I

    .line 53408
    return-object p0
.end method

.method public setClearUnreadNumber(Z)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 53445
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->bitField0_:I

    .line 53446
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->clearUnreadNumber_:Z

    .line 53448
    return-object p0
.end method
