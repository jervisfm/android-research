.class public final Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RequestCallLogPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CLEAR_UNREAD_NUMBER_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private clearUnreadNumber_:Z

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 53461
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;

    .line 53462
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->initFields()V

    .line 53463
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 53104
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 53142
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->memoizedIsInitialized:B

    .line 53170
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->memoizedSerializedSize:I

    .line 53105
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 53099
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 53106
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 53142
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->memoizedIsInitialized:B

    .line 53170
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->memoizedSerializedSize:I

    .line 53106
    return-void
.end method

.method static synthetic access$68702(Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 53099
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$68802(Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 53099
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->clearUnreadNumber_:Z

    return p1
.end method

.method static synthetic access$68902(Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 53099
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;
    .locals 1

    .prologue
    .line 53110
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 53139
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 53140
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->clearUnreadNumber_:Z

    .line 53141
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;
    .locals 1

    .prologue
    .line 53260
    #calls: Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->access$68500()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 53263
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53229
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    move-result-object v0

    .line 53230
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 53231
    #calls: Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->access$68400(Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;

    move-result-object v0

    .line 53233
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53240
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    move-result-object v0

    .line 53241
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 53242
    #calls: Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->access$68400(Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;

    move-result-object v0

    .line 53244
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 53196
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->access$68400(Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 53202
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->access$68400(Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53250
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->access$68400(Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53256
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->access$68400(Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53218
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->access$68400(Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53224
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->access$68400(Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 53207
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->access$68400(Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 53213
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;->access$68400(Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 53125
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getClearUnreadNumber()Z
    .locals 1

    .prologue
    .line 53135
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->clearUnreadNumber_:Z

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 53099
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;
    .locals 1

    .prologue
    .line 53114
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 53172
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->memoizedSerializedSize:I

    .line 53173
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 53185
    :goto_0
    return v0

    .line 53175
    :cond_0
    const/4 v0, 0x0

    .line 53176
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 53177
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 53180
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 53181
    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->clearUnreadNumber_:Z

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 53184
    :cond_2
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 53122
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasClearUnreadNumber()Z
    .locals 2

    .prologue
    .line 53132
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 53144
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->memoizedIsInitialized:B

    .line 53145
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 53156
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 53145
    goto :goto_0

    .line 53147
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 53148
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 53149
    goto :goto_0

    .line 53151
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 53152
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 53153
    goto :goto_0

    .line 53155
    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 53156
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 53099
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;
    .locals 1

    .prologue
    .line 53261
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 53099
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;
    .locals 1

    .prologue
    .line 53265
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;)Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 53190
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 53161
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->getSerializedSize()I

    .line 53162
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 53163
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 53165
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 53166
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RequestCallLogPayload;->clearUnreadNumber_:Z

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 53168
    :cond_1
    return-void
.end method
