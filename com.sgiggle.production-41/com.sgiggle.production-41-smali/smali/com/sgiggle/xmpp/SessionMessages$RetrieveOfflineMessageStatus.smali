.class public final enum Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RetrieveOfflineMessageStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus; = null

.field public static final enum OFFLINE_MESSAGE_STATUS_IDLE:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus; = null

.field public static final OFFLINE_MESSAGE_STATUS_IDLE_VALUE:I = 0x0

.field public static final enum OFFLINE_MESSAGE_STATUS_RETRIEVE_FAIL:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus; = null

.field public static final OFFLINE_MESSAGE_STATUS_RETRIEVE_FAIL_VALUE:I = 0x3

.field public static final enum OFFLINE_MESSAGE_STATUS_RETRIEVE_SUCCESS:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus; = null

.field public static final OFFLINE_MESSAGE_STATUS_RETRIEVE_SUCCESS_VALUE:I = 0x2

.field public static final enum OFFLINE_MESSAGE_STATUS_RETRIEVING:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus; = null

.field public static final OFFLINE_MESSAGE_STATUS_RETRIEVING_VALUE:I = 0x1

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1063
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    const-string v1, "OFFLINE_MESSAGE_STATUS_IDLE"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->OFFLINE_MESSAGE_STATUS_IDLE:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    .line 1064
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    const-string v1, "OFFLINE_MESSAGE_STATUS_RETRIEVING"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->OFFLINE_MESSAGE_STATUS_RETRIEVING:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    .line 1065
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    const-string v1, "OFFLINE_MESSAGE_STATUS_RETRIEVE_SUCCESS"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->OFFLINE_MESSAGE_STATUS_RETRIEVE_SUCCESS:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    .line 1066
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    const-string v1, "OFFLINE_MESSAGE_STATUS_RETRIEVE_FAIL"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->OFFLINE_MESSAGE_STATUS_RETRIEVE_FAIL:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    .line 1061
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->OFFLINE_MESSAGE_STATUS_IDLE:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->OFFLINE_MESSAGE_STATUS_RETRIEVING:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->OFFLINE_MESSAGE_STATUS_RETRIEVE_SUCCESS:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->OFFLINE_MESSAGE_STATUS_RETRIEVE_FAIL:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    .line 1092
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1101
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1102
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->value:I

    .line 1103
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1089
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;
    .locals 1
    .parameter

    .prologue
    .line 1078
    packed-switch p0, :pswitch_data_0

    .line 1083
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1079
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->OFFLINE_MESSAGE_STATUS_IDLE:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    goto :goto_0

    .line 1080
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->OFFLINE_MESSAGE_STATUS_RETRIEVING:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    goto :goto_0

    .line 1081
    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->OFFLINE_MESSAGE_STATUS_RETRIEVE_SUCCESS:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    goto :goto_0

    .line 1082
    :pswitch_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->OFFLINE_MESSAGE_STATUS_RETRIEVE_FAIL:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    goto :goto_0

    .line 1078
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;
    .locals 1
    .parameter

    .prologue
    .line 1061
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;
    .locals 1

    .prologue
    .line 1061
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 1075
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->value:I

    return v0
.end method
