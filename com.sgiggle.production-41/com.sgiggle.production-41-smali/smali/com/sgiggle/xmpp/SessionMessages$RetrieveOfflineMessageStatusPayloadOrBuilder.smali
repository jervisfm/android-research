.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatusPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "RetrieveOfflineMessageStatusPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getStatus()Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasStatus()Z
.end method
