.class public final Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private askToSendSms_:Z

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private dialogMessage_:Ljava/lang/Object;

.field private dialogTitle_:Ljava/lang/Object;

.field private info_:Ljava/lang/Object;

.field private receivers_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private type_:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

.field private userChooseToSend_:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;->SMS_COMPOSER_FORWARD_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->receivers_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->info_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->dialogTitle_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->dialogMessage_:Ljava/lang/Object;

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->maybeForceBuilderInitialization()V

    return-void
.end method

.method static synthetic access$137600(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$137700()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 1

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureReceiversIsMutable()V
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->receivers_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->receivers_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    return-void
.end method


# virtual methods
.method public addAllReceivers(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->ensureReceiversIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->receivers_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public addReceivers(ILcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->ensureReceiversIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->receivers_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public addReceivers(ILcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 1

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->ensureReceiversIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->receivers_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public addReceivers(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->ensureReceiversIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->receivers_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addReceivers(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->ensureReceiversIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->receivers_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;
    .locals 2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;
    .locals 5

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    or-int/lit8 v2, v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->access$137902(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->access$138002(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->userChooseToSend_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->userChooseToSend_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->access$138102(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;Z)Z

    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->receivers_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->receivers_:Ljava/util/List;

    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x9

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    :cond_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->receivers_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->receivers_:Ljava/util/List;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->access$138202(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;Ljava/util/List;)Ljava/util/List;

    and-int/lit8 v3, v1, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    or-int/lit8 v2, v2, 0x8

    :cond_4
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->info_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->info_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->access$138302(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v1, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    or-int/lit8 v2, v2, 0x10

    :cond_5
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->askToSendSms_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->askToSendSms_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->access$138402(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;Z)Z

    and-int/lit8 v3, v1, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    or-int/lit8 v2, v2, 0x20

    :cond_6
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->dialogTitle_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->dialogTitle_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->access$138502(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit16 v1, v1, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_7

    or-int/lit8 v1, v2, 0x40

    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->dialogMessage_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->dialogMessage_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->access$138602(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;Ljava/lang/Object;)Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->access$138702(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;I)I

    return-object v0

    :cond_7
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;->SMS_COMPOSER_FORWARD_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->userChooseToSend_:Z

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->receivers_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->info_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->askToSendSms_:Z

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->dialogTitle_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->dialogMessage_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearAskToSendSms()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->askToSendSms_:Z

    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearDialogMessage()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getDialogMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->dialogMessage_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearDialogTitle()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getDialogTitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->dialogTitle_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearInfo()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getInfo()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->info_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearReceivers()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->receivers_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearType()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;->SMS_COMPOSER_FORWARD_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    return-object p0
.end method

.method public clearUserChooseToSend()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->userChooseToSend_:Z

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAskToSendSms()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->askToSendSms_:Z

    return v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDialogMessage()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->dialogMessage_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->dialogMessage_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getDialogTitle()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->dialogTitle_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->dialogTitle_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getInfo()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->info_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->info_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getReceivers(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->receivers_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getReceiversCount()I
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->receivers_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getReceiversList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->receivers_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getType()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    return-object v0
.end method

.method public getUserChooseToSend()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->userChooseToSend_:Z

    return v0
.end method

.method public hasAskToSendSms()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDialogMessage()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDialogTitle()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasInfo()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUserChooseToSend()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->hasType()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->hasUserChooseToSend()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v2

    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->getReceiversCount()I

    move-result v1

    if-ge v0, v1, :cond_5

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->getReceivers(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_4

    move v0, v2

    goto :goto_0

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    goto :goto_1

    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    goto :goto_0

    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->userChooseToSend_:Z

    goto :goto_0

    :sswitch_4
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->addReceivers(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    goto :goto_0

    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->info_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->askToSendSms_:Z

    goto :goto_0

    :sswitch_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->dialogTitle_:Ljava/lang/Object;

    goto/16 :goto_0

    :sswitch_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->dialogMessage_:Ljava/lang/Object;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->hasType()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getType()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->hasUserChooseToSend()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getUserChooseToSend()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->setUserChooseToSend(Z)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    :cond_3
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->receivers_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->access$138200(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->receivers_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->receivers_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->access$138200(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->receivers_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    :cond_4
    :goto_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->hasInfo()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getInfo()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->setInfo(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    :cond_5
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->hasAskToSendSms()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getAskToSendSms()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->setAskToSendSms(Z)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    :cond_6
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->hasDialogTitle()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getDialogTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->setDialogTitle(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    :cond_7
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->hasDialogMessage()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getDialogMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->setDialogMessage(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    :cond_8
    move-object v0, p0

    goto :goto_0

    :cond_9
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->ensureReceiversIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->receivers_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->receivers_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->access$138200(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public removeReceivers(I)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->ensureReceiversIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->receivers_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    return-object p0
.end method

.method public setAskToSendSms(Z)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->askToSendSms_:Z

    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setDialogMessage(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->dialogMessage_:Ljava/lang/Object;

    return-object p0
.end method

.method setDialogMessage(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->dialogMessage_:Ljava/lang/Object;

    return-void
.end method

.method public setDialogTitle(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->dialogTitle_:Ljava/lang/Object;

    return-object p0
.end method

.method setDialogTitle(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->dialogTitle_:Ljava/lang/Object;

    return-void
.end method

.method public setInfo(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->info_:Ljava/lang/Object;

    return-object p0
.end method

.method setInfo(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->info_:Ljava/lang/Object;

    return-void
.end method

.method public setReceivers(ILcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->ensureReceiversIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->receivers_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setReceivers(ILcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 1

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->ensureReceiversIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->receivers_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setType(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    return-object p0
.end method

.method public setUserChooseToSend(Z)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->userChooseToSend_:Z

    return-object p0
.end method
