.class public final Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SMSComposerPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    }
.end annotation


# static fields
.field public static final ASK_TO_SEND_SMS_FIELD_NUMBER:I = 0x6

.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final DIALOG_MESSAGE_FIELD_NUMBER:I = 0x8

.field public static final DIALOG_TITLE_FIELD_NUMBER:I = 0x7

.field public static final INFO_FIELD_NUMBER:I = 0x5

.field public static final RECEIVERS_FIELD_NUMBER:I = 0x4

.field public static final TYPE_FIELD_NUMBER:I = 0x2

.field public static final USER_CHOOSE_TO_SEND_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;


# instance fields
.field private askToSendSms_:Z

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private dialogMessage_:Ljava/lang/Object;

.field private dialogTitle_:Ljava/lang/Object;

.field private info_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private receivers_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private type_:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

.field private userChooseToSend_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->initFields()V

    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$137902(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$138002(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    return-object p1
.end method

.method static synthetic access$138102(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->userChooseToSend_:Z

    return p1
.end method

.method static synthetic access$138200(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->receivers_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$138202(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->receivers_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$138302(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->info_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$138402(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->askToSendSms_:Z

    return p1
.end method

.method static synthetic access$138502(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->dialogTitle_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$138602(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->dialogMessage_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$138702(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    return-object v0
.end method

.method private getDialogMessageBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->dialogMessage_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->dialogMessage_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getDialogTitleBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->dialogTitle_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->dialogTitle_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getInfoBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->info_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->info_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 2

    const/4 v1, 0x0

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;->SMS_COMPOSER_FORWARD_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->userChooseToSend_:Z

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->receivers_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->info_:Ljava/lang/Object;

    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->askToSendSms_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->dialogTitle_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->dialogMessage_:Ljava/lang/Object;

    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 1

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->access$137700()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->access$137600(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->access$137600(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->access$137600(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->access$137600(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->access$137600(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->access$137600(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->access$137600(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->access$137600(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->access$137600(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->access$137600(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAskToSendSms()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->askToSendSms_:Z

    return v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    return-object v0
.end method

.method public getDialogMessage()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->dialogMessage_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->dialogMessage_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getDialogTitle()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->dialogTitle_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->dialogTitle_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getInfo()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->info_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->info_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getReceivers(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->receivers_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getReceiversCount()I
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->receivers_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getReceiversList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->receivers_:Ljava/util/List;

    return-object v0
.end method

.method public getReceiversOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->receivers_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;

    return-object v0
.end method

.method public getReceiversOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->receivers_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_8

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v3

    :goto_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_1

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;->getNumber()I

    move-result v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_2

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->userChooseToSend_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    move v1, v3

    move v2, v0

    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->receivers_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->receivers_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v5, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_7

    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getInfoBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/2addr v0, v2

    :goto_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_4

    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->askToSendSms_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_5

    const/4 v1, 0x7

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getDialogTitleBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_6

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getDialogMessageBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v6, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->memoizedSerializedSize:I

    goto/16 :goto_0

    :cond_7
    move v0, v2

    goto :goto_3

    :cond_8
    move v0, v3

    goto/16 :goto_1
.end method

.method public getType()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    return-object v0
.end method

.method public getUserChooseToSend()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->userChooseToSend_:Z

    return v0
.end method

.method public hasAskToSendSms()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDialogMessage()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDialogTitle()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasInfo()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUserChooseToSend()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->memoizedIsInitialized:B

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->hasType()Z

    move-result v0

    if-nez v0, :cond_3

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->hasUserChooseToSend()Z

    move-result v0

    if-nez v0, :cond_4

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_5

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_5
    move v0, v2

    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getReceiversCount()I

    move-result v1

    if-ge v0, v1, :cond_7

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getReceivers(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_6

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_7
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->memoizedIsInitialized:B

    move v0, v3

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;
    .locals 1

    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getSerializedSize()I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;->getNumber()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->userChooseToSend_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_2
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->receivers_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->receivers_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_4

    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getInfoBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->askToSendSms_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    const/4 v0, 0x7

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getDialogTitleBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_7

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getDialogMessageBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_7
    return-void
.end method
