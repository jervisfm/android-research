.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SMSComposerPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getAskToSendSms()Z
.end method

.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getDialogMessage()Ljava/lang/String;
.end method

.method public abstract getDialogTitle()Ljava/lang/String;
.end method

.method public abstract getInfo()Ljava/lang/String;
.end method

.method public abstract getReceivers(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
.end method

.method public abstract getReceiversCount()I
.end method

.method public abstract getReceiversList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getType()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;
.end method

.method public abstract getUserChooseToSend()Z
.end method

.method public abstract hasAskToSendSms()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasDialogMessage()Z
.end method

.method public abstract hasDialogTitle()Z
.end method

.method public abstract hasInfo()Z
.end method

.method public abstract hasType()Z
.end method

.method public abstract hasUserChooseToSend()Z
.end method
