.class public final enum Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SMSComposerType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType; = null

.field public static final enum SMS_COMPOSER_FORWARD_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType; = null

.field public static final SMS_COMPOSER_FORWARD_MESSAGE_VALUE:I = 0x1

.field public static final enum SMS_COMPOSER_TEXT_MESSAGE_FOR_NON_TANGO_USER:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType; = null

.field public static final SMS_COMPOSER_TEXT_MESSAGE_FOR_NON_TANGO_USER_VALUE:I = 0x2

.field public static final enum SMS_COMPOSER_VIDEO_MESSAGE_FOR_NON_TANGO_USER:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType; = null

.field public static final SMS_COMPOSER_VIDEO_MESSAGE_FOR_NON_TANGO_USER_VALUE:I = 0x3

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 1154
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    const-string v1, "SMS_COMPOSER_FORWARD_MESSAGE"

    invoke-direct {v0, v1, v4, v4, v2}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;->SMS_COMPOSER_FORWARD_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    .line 1155
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    const-string v1, "SMS_COMPOSER_TEXT_MESSAGE_FOR_NON_TANGO_USER"

    invoke-direct {v0, v1, v2, v2, v3}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;->SMS_COMPOSER_TEXT_MESSAGE_FOR_NON_TANGO_USER:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    .line 1156
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    const-string v1, "SMS_COMPOSER_VIDEO_MESSAGE_FOR_NON_TANGO_USER"

    invoke-direct {v0, v1, v3, v3, v5}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;->SMS_COMPOSER_VIDEO_MESSAGE_FOR_NON_TANGO_USER:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    .line 1152
    new-array v0, v5, [Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;->SMS_COMPOSER_FORWARD_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;->SMS_COMPOSER_TEXT_MESSAGE_FOR_NON_TANGO_USER:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;->SMS_COMPOSER_VIDEO_MESSAGE_FOR_NON_TANGO_USER:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    .line 1180
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1189
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1190
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;->value:I

    .line 1191
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1177
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;
    .locals 1
    .parameter

    .prologue
    .line 1167
    packed-switch p0, :pswitch_data_0

    .line 1171
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1168
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;->SMS_COMPOSER_FORWARD_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    goto :goto_0

    .line 1169
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;->SMS_COMPOSER_TEXT_MESSAGE_FOR_NON_TANGO_USER:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    goto :goto_0

    .line 1170
    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;->SMS_COMPOSER_VIDEO_MESSAGE_FOR_NON_TANGO_USER:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    goto :goto_0

    .line 1167
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;
    .locals 1
    .parameter

    .prologue
    .line 1152
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;
    .locals 1

    .prologue
    .line 1152
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 1164
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;->value:I

    return v0
.end method
