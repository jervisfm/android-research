.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$SMSVerificationPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SMSVerificationPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getNormalizedNumber()Ljava/lang/String;
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasNormalizedNumber()Z
.end method
