.class public final Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SelectContactPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CONTACTS_FIELD_NUMBER:I = 0x3

.field public static final MAX_SELECTION_FIELD_NUMBER:I = 0x4

.field public static final TYPE_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private contacts_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private maxSelection_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private type_:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->initFields()V

    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$139802(Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$139902(Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    return-object p1
.end method

.method static synthetic access$140000(Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->contacts_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$140002(Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->contacts_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$140102(Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->maxSelection_:I

    return p1
.end method

.method static synthetic access$140202(Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->SELECT_TO_START_CONVERSATION:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->contacts_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->maxSelection_:I

    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;
    .locals 1

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->access$139600()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->access$139500(Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->access$139500(Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->access$139500(Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->access$139500(Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->access$139500(Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->access$139500(Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->access$139500(Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->access$139500(Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->access$139500(Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;->access$139500(Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getContacts(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getContactsCount()I
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->contacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getContactsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->contacts_:Ljava/util/List;

    return-object v0
.end method

.method public getContactsOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;

    return-object v0
.end method

.method public getContactsOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->contacts_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;

    return-object v0
.end method

.method public getMaxSelection()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->maxSelection_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    :goto_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_1

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->getNumber()I

    move-result v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    move v1, v2

    move v2, v0

    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->contacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    const/4 v3, 0x3

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->contacts_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_3

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->maxSelection_:I

    invoke-static {v5, v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v0

    add-int/2addr v0, v2

    :goto_3
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->memoizedSerializedSize:I

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method public getType()Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMaxSelection()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->memoizedIsInitialized:B

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->hasType()Z

    move-result v0

    if-nez v0, :cond_3

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_4
    move v0, v2

    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->getContactsCount()I

    move-result v1

    if-ge v0, v1, :cond_6

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->getContacts(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_5

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_6
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->memoizedIsInitialized:B

    move v0, v3

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;
    .locals 1

    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->getSerializedSize()I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->getNumber()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->contacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    const/4 v2, 0x3

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->contacts_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_3

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->maxSelection_:I

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_3
    return-void
.end method
