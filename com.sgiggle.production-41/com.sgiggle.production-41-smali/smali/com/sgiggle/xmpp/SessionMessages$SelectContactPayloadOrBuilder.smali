.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SelectContactPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getContacts(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
.end method

.method public abstract getContactsCount()I
.end method

.method public abstract getContactsList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getMaxSelection()I
.end method

.method public abstract getType()Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasMaxSelection()Z
.end method

.method public abstract hasType()Z
.end method
