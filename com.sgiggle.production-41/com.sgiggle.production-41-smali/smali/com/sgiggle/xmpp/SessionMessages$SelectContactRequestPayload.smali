.class public final Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SelectContactRequestPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final MESSAGE_TYPE_FIELD_NUMBER:I = 0x3

.field public static final TYPE_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private messageType_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

.field private type_:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->initFields()V

    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$139102(Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$139202(Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    return-object p1
.end method

.method static synthetic access$139302(Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->messageType_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    return-object p1
.end method

.method static synthetic access$139402(Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->SELECT_TO_START_CONVERSATION:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->TEXT_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->messageType_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;
    .locals 1

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->access$138900()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->access$138800(Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->access$138800(Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->access$138800(Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->access$138800(Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->access$138800(Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->access$138800(Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->access$138800(Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->access$138800(Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->access$138800(Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;->access$138800(Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;

    return-object v0
.end method

.method public getMessageType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->messageType_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->getNumber()I

    move-result v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->messageType_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getType()Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMessageType()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->memoizedIsInitialized:B

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->hasType()Z

    move-result v0

    if-nez v0, :cond_3

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_4
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->memoizedIsInitialized:B

    move v0, v3

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;
    .locals 1

    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->getSerializedSize()I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->getNumber()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayload;->messageType_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_2
    return-void
.end method
