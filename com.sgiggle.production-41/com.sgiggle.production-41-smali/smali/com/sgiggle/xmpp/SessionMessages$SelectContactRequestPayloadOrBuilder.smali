.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$SelectContactRequestPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SelectContactRequestPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getMessageType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;
.end method

.method public abstract getType()Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasMessageType()Z
.end method

.method public abstract hasType()Z
.end method
