.class public final enum Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SelectContactType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$SelectContactType; = null

.field public static final enum SELECT_TO_BUY_ECARD:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType; = null

.field public static final SELECT_TO_BUY_ECARD_VALUE:I = 0x3

.field public static final enum SELECT_TO_FORWARD_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType; = null

.field public static final SELECT_TO_FORWARD_MESSAGE_VALUE:I = 0x1

.field public static final enum SELECT_TO_SEND_ECARD:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType; = null

.field public static final SELECT_TO_SEND_ECARD_VALUE:I = 0x2

.field public static final enum SELECT_TO_START_CONVERSATION:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

.field public static final SELECT_TO_START_CONVERSATION_VALUE:I

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1198
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    const-string v1, "SELECT_TO_START_CONVERSATION"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->SELECT_TO_START_CONVERSATION:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    .line 1199
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    const-string v1, "SELECT_TO_FORWARD_MESSAGE"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->SELECT_TO_FORWARD_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    .line 1200
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    const-string v1, "SELECT_TO_SEND_ECARD"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->SELECT_TO_SEND_ECARD:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    .line 1201
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    const-string v1, "SELECT_TO_BUY_ECARD"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->SELECT_TO_BUY_ECARD:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    .line 1196
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->SELECT_TO_START_CONVERSATION:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->SELECT_TO_FORWARD_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->SELECT_TO_SEND_ECARD:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->SELECT_TO_BUY_ECARD:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    .line 1227
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1236
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1237
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->value:I

    .line 1238
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1224
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;
    .locals 1
    .parameter

    .prologue
    .line 1213
    packed-switch p0, :pswitch_data_0

    .line 1218
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1214
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->SELECT_TO_START_CONVERSATION:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    goto :goto_0

    .line 1215
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->SELECT_TO_FORWARD_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    goto :goto_0

    .line 1216
    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->SELECT_TO_SEND_ECARD:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    goto :goto_0

    .line 1217
    :pswitch_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->SELECT_TO_BUY_ECARD:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    goto :goto_0

    .line 1213
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;
    .locals 1
    .parameter

    .prologue
    .line 1196
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;
    .locals 1

    .prologue
    .line 1196
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 1210
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->value:I

    return v0
.end method
