.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$SendVideoGreetingPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SendVideoGreetingPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getRecipients(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
.end method

.method public abstract getRecipientsCount()I
.end method

.method public abstract getRecipientsList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getVideoId()Ljava/lang/String;
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasVideoId()Z
.end method
