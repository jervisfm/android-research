.class public final Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ShakeModePayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final SHAKEABLE_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private shakeable_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 52636
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;

    .line 52637
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->initFields()V

    .line 52638
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 52271
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 52309
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->memoizedIsInitialized:B

    .line 52341
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->memoizedSerializedSize:I

    .line 52272
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 52266
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 52273
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 52309
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->memoizedIsInitialized:B

    .line 52341
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->memoizedSerializedSize:I

    .line 52273
    return-void
.end method

.method static synthetic access$67502(Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 52266
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$67602(Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 52266
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->shakeable_:Z

    return p1
.end method

.method static synthetic access$67702(Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 52266
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;
    .locals 1

    .prologue
    .line 52277
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 52306
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 52307
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->shakeable_:Z

    .line 52308
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;
    .locals 1

    .prologue
    .line 52431
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->access$67300()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;)Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 52434
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;)Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52400
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;

    move-result-object v0

    .line 52401
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 52402
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->access$67200(Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;

    move-result-object v0

    .line 52404
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52411
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;

    move-result-object v0

    .line 52412
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 52413
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->access$67200(Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;

    move-result-object v0

    .line 52415
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 52367
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->access$67200(Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 52373
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->access$67200(Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52421
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->access$67200(Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52427
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->access$67200(Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52389
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->access$67200(Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52395
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->access$67200(Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 52378
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->access$67200(Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 52384
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;->access$67200(Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 52292
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 52266
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;
    .locals 1

    .prologue
    .line 52281
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 52343
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->memoizedSerializedSize:I

    .line 52344
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 52356
    :goto_0
    return v0

    .line 52346
    :cond_0
    const/4 v0, 0x0

    .line 52347
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 52348
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 52351
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 52352
    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->shakeable_:Z

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 52355
    :cond_2
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getShakeable()Z
    .locals 1

    .prologue
    .line 52302
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->shakeable_:Z

    return v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 52289
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasShakeable()Z
    .locals 2

    .prologue
    .line 52299
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 52311
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->memoizedIsInitialized:B

    .line 52312
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 52327
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 52312
    goto :goto_0

    .line 52314
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 52315
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 52316
    goto :goto_0

    .line 52318
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->hasShakeable()Z

    move-result v0

    if-nez v0, :cond_3

    .line 52319
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 52320
    goto :goto_0

    .line 52322
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    .line 52323
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 52324
    goto :goto_0

    .line 52326
    :cond_4
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->memoizedIsInitialized:B

    move v0, v3

    .line 52327
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 52266
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;
    .locals 1

    .prologue
    .line 52432
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 52266
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;
    .locals 1

    .prologue
    .line 52436
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;)Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 52361
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 52332
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->getSerializedSize()I

    .line 52333
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 52334
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 52336
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 52337
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ShakeModePayload;->shakeable_:Z

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 52339
    :cond_1
    return-void
.end method
