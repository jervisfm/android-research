.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$SnsAuthResultPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SnsAuthResultPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getAccessToken()Ljava/lang/String;
.end method

.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getExpire()I
.end method

.method public abstract getPinCode()Ljava/lang/String;
.end method

.method public abstract getRefreshToken()Ljava/lang/String;
.end method

.method public abstract getSuccess()Z
.end method

.method public abstract hasAccessToken()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasExpire()Z
.end method

.method public abstract hasPinCode()Z
.end method

.method public abstract hasRefreshToken()Z
.end method

.method public abstract hasSuccess()Z
.end method
