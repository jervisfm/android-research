.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$SnsProcessResultPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SnsProcessResultPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getErrorType()Lcom/sgiggle/xmpp/SessionMessages$SnsProcessResultPayload$Error;
.end method

.method public abstract getSuccess()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasErrorType()Z
.end method

.method public abstract hasSuccess()Z
.end method
