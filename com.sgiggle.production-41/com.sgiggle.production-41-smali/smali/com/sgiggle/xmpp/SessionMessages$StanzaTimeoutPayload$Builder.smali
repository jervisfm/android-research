.class public final Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private iqType_:Ljava/lang/Object;

.field private jid_:Ljava/lang/Object;

.field private stanzaName_:Ljava/lang/Object;

.field private stanzaSeq_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 35883
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 36059
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 36102
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->stanzaName_:Ljava/lang/Object;

    .line 36138
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->iqType_:Ljava/lang/Object;

    .line 36174
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->stanzaSeq_:Ljava/lang/Object;

    .line 36210
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->jid_:Ljava/lang/Object;

    .line 35884
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->maybeForceBuilderInitialization()V

    .line 35885
    return-void
.end method

.method static synthetic access$45300(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 35878
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$45400()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;
    .locals 1

    .prologue
    .line 35878
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 35926
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    move-result-object v0

    .line 35927
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 35928
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 35931
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;
    .locals 1

    .prologue
    .line 35890
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 35888
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 35878
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;
    .locals 2

    .prologue
    .line 35917
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    move-result-object v0

    .line 35918
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 35919
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 35921
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 35878
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;
    .locals 5

    .prologue
    .line 35935
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 35936
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    .line 35937
    const/4 v2, 0x0

    .line 35938
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 35939
    or-int/lit8 v2, v2, 0x1

    .line 35941
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->access$45602(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 35942
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 35943
    or-int/lit8 v2, v2, 0x2

    .line 35945
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->stanzaName_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->stanzaName_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->access$45702(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35946
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 35947
    or-int/lit8 v2, v2, 0x4

    .line 35949
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->iqType_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->iqType_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->access$45802(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35950
    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 35951
    or-int/lit8 v2, v2, 0x8

    .line 35953
    :cond_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->stanzaSeq_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->stanzaSeq_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->access$45902(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35954
    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_4

    .line 35955
    or-int/lit8 v1, v2, 0x10

    .line 35957
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->jid_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->jid_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->access$46002(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35958
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->access$46102(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;I)I

    .line 35959
    return-object v0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 35878
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 35878
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;
    .locals 1

    .prologue
    .line 35894
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 35895
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 35896
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    .line 35897
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->stanzaName_:Ljava/lang/Object;

    .line 35898
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    .line 35899
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->iqType_:Ljava/lang/Object;

    .line 35900
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    .line 35901
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->stanzaSeq_:Ljava/lang/Object;

    .line 35902
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    .line 35903
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->jid_:Ljava/lang/Object;

    .line 35904
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    .line 35905
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;
    .locals 1

    .prologue
    .line 36095
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 36097
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    .line 36098
    return-object p0
.end method

.method public clearIqType()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;
    .locals 1

    .prologue
    .line 36162
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    .line 36163
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->getIqType()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->iqType_:Ljava/lang/Object;

    .line 36165
    return-object p0
.end method

.method public clearJid()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;
    .locals 1

    .prologue
    .line 36234
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    .line 36235
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->getJid()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->jid_:Ljava/lang/Object;

    .line 36237
    return-object p0
.end method

.method public clearStanzaName()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;
    .locals 1

    .prologue
    .line 36126
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    .line 36127
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->getStanzaName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->stanzaName_:Ljava/lang/Object;

    .line 36129
    return-object p0
.end method

.method public clearStanzaSeq()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;
    .locals 1

    .prologue
    .line 36198
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    .line 36199
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->getStanzaSeq()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->stanzaSeq_:Ljava/lang/Object;

    .line 36201
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 35878
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 35878
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 35878
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;
    .locals 2

    .prologue
    .line 35909
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 35878
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 36064
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 35878
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 35878
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;
    .locals 1

    .prologue
    .line 35913
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public getIqType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 36143
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->iqType_:Ljava/lang/Object;

    .line 36144
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 36145
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 36146
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->iqType_:Ljava/lang/Object;

    .line 36149
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getJid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 36215
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->jid_:Ljava/lang/Object;

    .line 36216
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 36217
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 36218
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->jid_:Ljava/lang/Object;

    .line 36221
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getStanzaName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 36107
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->stanzaName_:Ljava/lang/Object;

    .line 36108
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 36109
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 36110
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->stanzaName_:Ljava/lang/Object;

    .line 36113
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getStanzaSeq()Ljava/lang/String;
    .locals 2

    .prologue
    .line 36179
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->stanzaSeq_:Ljava/lang/Object;

    .line 36180
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 36181
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 36182
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->stanzaSeq_:Ljava/lang/Object;

    .line 36185
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 36061
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIqType()Z
    .locals 2

    .prologue
    .line 36140
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasJid()Z
    .locals 2

    .prologue
    .line 36212
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasStanzaName()Z
    .locals 2

    .prologue
    .line 36104
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasStanzaSeq()Z
    .locals 2

    .prologue
    .line 36176
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35983
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 36003
    :goto_0
    return v0

    .line 35987
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->hasStanzaName()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 35989
    goto :goto_0

    .line 35991
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->hasIqType()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 35993
    goto :goto_0

    .line 35995
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->hasStanzaSeq()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 35997
    goto :goto_0

    .line 35999
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 36001
    goto :goto_0

    .line 36003
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 36083
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 36085
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 36091
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    .line 36092
    return-object p0

    .line 36088
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35878
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 35878
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35878
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36011
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 36012
    sparse-switch v0, :sswitch_data_0

    .line 36017
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 36019
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 36015
    goto :goto_1

    .line 36024
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 36025
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 36026
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 36028
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 36029
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    goto :goto_0

    .line 36033
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    .line 36034
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->stanzaName_:Ljava/lang/Object;

    goto :goto_0

    .line 36038
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    .line 36039
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->iqType_:Ljava/lang/Object;

    goto :goto_0

    .line 36043
    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    .line 36044
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->stanzaSeq_:Ljava/lang/Object;

    goto :goto_0

    .line 36048
    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    .line 36049
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->jid_:Ljava/lang/Object;

    goto :goto_0

    .line 36012
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 35963
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 35979
    :goto_0
    return-object v0

    .line 35964
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 35965
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    .line 35967
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->hasStanzaName()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 35968
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->getStanzaName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->setStanzaName(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    .line 35970
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->hasIqType()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 35971
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->getIqType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->setIqType(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    .line 35973
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->hasStanzaSeq()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 35974
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->getStanzaSeq()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->setStanzaSeq(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    .line 35976
    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->hasJid()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 35977
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->getJid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->setJid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    :cond_5
    move-object v0, p0

    .line 35979
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 36077
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 36079
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    .line 36080
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 36067
    if-nez p1, :cond_0

    .line 36068
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 36070
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 36072
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    .line 36073
    return-object p0
.end method

.method public setIqType(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 36153
    if-nez p1, :cond_0

    .line 36154
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 36156
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    .line 36157
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->iqType_:Ljava/lang/Object;

    .line 36159
    return-object p0
.end method

.method setIqType(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 36168
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    .line 36169
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->iqType_:Ljava/lang/Object;

    .line 36171
    return-void
.end method

.method public setJid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 36225
    if-nez p1, :cond_0

    .line 36226
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 36228
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    .line 36229
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->jid_:Ljava/lang/Object;

    .line 36231
    return-object p0
.end method

.method setJid(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 36240
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    .line 36241
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->jid_:Ljava/lang/Object;

    .line 36243
    return-void
.end method

.method public setStanzaName(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 36117
    if-nez p1, :cond_0

    .line 36118
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 36120
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    .line 36121
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->stanzaName_:Ljava/lang/Object;

    .line 36123
    return-object p0
.end method

.method setStanzaName(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 36132
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    .line 36133
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->stanzaName_:Ljava/lang/Object;

    .line 36135
    return-void
.end method

.method public setStanzaSeq(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 36189
    if-nez p1, :cond_0

    .line 36190
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 36192
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    .line 36193
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->stanzaSeq_:Ljava/lang/Object;

    .line 36195
    return-object p0
.end method

.method setStanzaSeq(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 36204
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->bitField0_:I

    .line 36205
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->stanzaSeq_:Ljava/lang/Object;

    .line 36207
    return-void
.end method
