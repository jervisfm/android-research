.class public final Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StanzaTimeoutPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final IQ_TYPE_FIELD_NUMBER:I = 0x3

.field public static final JID_FIELD_NUMBER:I = 0x5

.field public static final STANZA_NAME_FIELD_NUMBER:I = 0x2

.field public static final STANZA_SEQ_FIELD_NUMBER:I = 0x4

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private iqType_:Ljava/lang/Object;

.field private jid_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private stanzaName_:Ljava/lang/Object;

.field private stanzaSeq_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36249
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    .line 36250
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->initFields()V

    .line 36251
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 35561
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 35720
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->memoizedIsInitialized:B

    .line 35769
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->memoizedSerializedSize:I

    .line 35562
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 35556
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 35563
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 35720
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->memoizedIsInitialized:B

    .line 35769
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->memoizedSerializedSize:I

    .line 35563
    return-void
.end method

.method static synthetic access$45602(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 35556
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$45702(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 35556
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->stanzaName_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$45802(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 35556
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->iqType_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$45902(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 35556
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->stanzaSeq_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$46002(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 35556
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->jid_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$46102(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 35556
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;
    .locals 1

    .prologue
    .line 35567
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    return-object v0
.end method

.method private getIqTypeBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 35638
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->iqType_:Ljava/lang/Object;

    .line 35639
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 35640
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 35642
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->iqType_:Ljava/lang/Object;

    .line 35645
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getJidBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 35702
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->jid_:Ljava/lang/Object;

    .line 35703
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 35704
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 35706
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->jid_:Ljava/lang/Object;

    .line 35709
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getStanzaNameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 35606
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->stanzaName_:Ljava/lang/Object;

    .line 35607
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 35608
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 35610
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->stanzaName_:Ljava/lang/Object;

    .line 35613
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getStanzaSeqBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 35670
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->stanzaSeq_:Ljava/lang/Object;

    .line 35671
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 35672
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 35674
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->stanzaSeq_:Ljava/lang/Object;

    .line 35677
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 35714
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 35715
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->stanzaName_:Ljava/lang/Object;

    .line 35716
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->iqType_:Ljava/lang/Object;

    .line 35717
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->stanzaSeq_:Ljava/lang/Object;

    .line 35718
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->jid_:Ljava/lang/Object;

    .line 35719
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;
    .locals 1

    .prologue
    .line 35871
    #calls: Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->access$45400()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 35874
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35840
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    move-result-object v0

    .line 35841
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 35842
    #calls: Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->access$45300(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    move-result-object v0

    .line 35844
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35851
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    move-result-object v0

    .line 35852
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 35853
    #calls: Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->access$45300(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    move-result-object v0

    .line 35855
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 35807
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->access$45300(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 35813
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->access$45300(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35861
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->access$45300(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35867
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->access$45300(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35829
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->access$45300(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35835
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->access$45300(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 35818
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->access$45300(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 35824
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;->access$45300(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 35582
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 35556
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;
    .locals 1

    .prologue
    .line 35571
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;

    return-object v0
.end method

.method public getIqType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 35624
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->iqType_:Ljava/lang/Object;

    .line 35625
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 35626
    check-cast v0, Ljava/lang/String;

    .line 35634
    :goto_0
    return-object v0

    .line 35628
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 35630
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 35631
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 35632
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->iqType_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 35634
    goto :goto_0
.end method

.method public getJid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 35688
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->jid_:Ljava/lang/Object;

    .line 35689
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 35690
    check-cast v0, Ljava/lang/String;

    .line 35698
    :goto_0
    return-object v0

    .line 35692
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 35694
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 35695
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 35696
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->jid_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 35698
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 35771
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->memoizedSerializedSize:I

    .line 35772
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 35796
    :goto_0
    return v0

    .line 35774
    :cond_0
    const/4 v0, 0x0

    .line 35775
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 35776
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 35779
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 35780
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->getStanzaNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 35783
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 35784
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->getIqTypeBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 35787
    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 35788
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->getStanzaSeqBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 35791
    :cond_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 35792
    const/4 v1, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->getJidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 35795
    :cond_5
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getStanzaName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 35592
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->stanzaName_:Ljava/lang/Object;

    .line 35593
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 35594
    check-cast v0, Ljava/lang/String;

    .line 35602
    :goto_0
    return-object v0

    .line 35596
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 35598
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 35599
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 35600
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->stanzaName_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 35602
    goto :goto_0
.end method

.method public getStanzaSeq()Ljava/lang/String;
    .locals 2

    .prologue
    .line 35656
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->stanzaSeq_:Ljava/lang/Object;

    .line 35657
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 35658
    check-cast v0, Ljava/lang/String;

    .line 35666
    :goto_0
    return-object v0

    .line 35660
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 35662
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 35663
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 35664
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->stanzaSeq_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 35666
    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 35579
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIqType()Z
    .locals 2

    .prologue
    .line 35621
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasJid()Z
    .locals 2

    .prologue
    .line 35685
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasStanzaName()Z
    .locals 2

    .prologue
    .line 35589
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasStanzaSeq()Z
    .locals 2

    .prologue
    .line 35653
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 35722
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->memoizedIsInitialized:B

    .line 35723
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 35746
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 35723
    goto :goto_0

    .line 35725
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 35726
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 35727
    goto :goto_0

    .line 35729
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->hasStanzaName()Z

    move-result v0

    if-nez v0, :cond_3

    .line 35730
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 35731
    goto :goto_0

    .line 35733
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->hasIqType()Z

    move-result v0

    if-nez v0, :cond_4

    .line 35734
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 35735
    goto :goto_0

    .line 35737
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->hasStanzaSeq()Z

    move-result v0

    if-nez v0, :cond_5

    .line 35738
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 35739
    goto :goto_0

    .line 35741
    :cond_5
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_6

    .line 35742
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 35743
    goto :goto_0

    .line 35745
    :cond_6
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 35746
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 35556
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;
    .locals 1

    .prologue
    .line 35872
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 35556
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;
    .locals 1

    .prologue
    .line 35876
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;)Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 35801
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 35751
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->getSerializedSize()I

    .line 35752
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 35753
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 35755
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 35756
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->getStanzaNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 35758
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 35759
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->getIqTypeBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 35761
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 35762
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->getStanzaSeqBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 35764
    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 35765
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayload;->getJidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 35767
    :cond_4
    return-void
.end method
