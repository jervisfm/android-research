.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$StanzaTimeoutPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "StanzaTimeoutPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getIqType()Ljava/lang/String;
.end method

.method public abstract getJid()Ljava/lang/String;
.end method

.method public abstract getStanzaName()Ljava/lang/String;
.end method

.method public abstract getStanzaSeq()Ljava/lang/String;
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasIqType()Z
.end method

.method public abstract hasJid()Z
.end method

.method public abstract hasStanzaName()Z
.end method

.method public abstract hasStanzaSeq()Z
.end method
