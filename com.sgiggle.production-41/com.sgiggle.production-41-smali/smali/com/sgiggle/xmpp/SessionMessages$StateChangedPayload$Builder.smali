.class public final Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private error_:Ljava/lang/Object;

.field private state_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2664
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 2804
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 2847
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->state_:Ljava/lang/Object;

    .line 2883
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->error_:Ljava/lang/Object;

    .line 2665
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->maybeForceBuilderInitialization()V

    .line 2666
    return-void
.end method

.method static synthetic access$1800(Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2659
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1900()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;
    .locals 1

    .prologue
    .line 2659
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2703
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;

    move-result-object v0

    .line 2704
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2705
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 2708
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;
    .locals 1

    .prologue
    .line 2671
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 2669
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2659
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;
    .locals 2

    .prologue
    .line 2694
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;

    move-result-object v0

    .line 2695
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2696
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 2698
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2659
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;
    .locals 5

    .prologue
    .line 2712
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 2713
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    .line 2714
    const/4 v2, 0x0

    .line 2715
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 2716
    or-int/lit8 v2, v2, 0x1

    .line 2718
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->access$2102(Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 2719
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 2720
    or-int/lit8 v2, v2, 0x2

    .line 2722
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->state_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->state_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->access$2202(Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2723
    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_2

    .line 2724
    or-int/lit8 v1, v2, 0x4

    .line 2726
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->error_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->error_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->access$2302(Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2727
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->access$2402(Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;I)I

    .line 2728
    return-object v0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 2659
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2659
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;
    .locals 1

    .prologue
    .line 2675
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 2676
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 2677
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    .line 2678
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->state_:Ljava/lang/Object;

    .line 2679
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    .line 2680
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->error_:Ljava/lang/Object;

    .line 2681
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    .line 2682
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;
    .locals 1

    .prologue
    .line 2840
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 2842
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    .line 2843
    return-object p0
.end method

.method public clearError()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;
    .locals 1

    .prologue
    .line 2907
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    .line 2908
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->getError()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->error_:Ljava/lang/Object;

    .line 2910
    return-object p0
.end method

.method public clearState()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;
    .locals 1

    .prologue
    .line 2871
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    .line 2872
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->getState()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->state_:Ljava/lang/Object;

    .line 2874
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 2659
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 2659
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2659
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;
    .locals 2

    .prologue
    .line 2686
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2659
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 2809
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 2659
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2659
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;
    .locals 1

    .prologue
    .line 2690
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;

    move-result-object v0

    return-object v0
.end method

.method public getError()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2888
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->error_:Ljava/lang/Object;

    .line 2889
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2890
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 2891
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->error_:Ljava/lang/Object;

    .line 2894
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getState()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2852
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->state_:Ljava/lang/Object;

    .line 2853
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2854
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 2855
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->state_:Ljava/lang/Object;

    .line 2858
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2806
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasError()Z
    .locals 2

    .prologue
    .line 2885
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasState()Z
    .locals 2

    .prologue
    .line 2849
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2746
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2758
    :goto_0
    return v0

    .line 2750
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->hasState()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 2752
    goto :goto_0

    .line 2754
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 2756
    goto :goto_0

    .line 2758
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 2828
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 2830
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 2836
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    .line 2837
    return-object p0

    .line 2833
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2659
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2659
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2659
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2766
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 2767
    sparse-switch v0, :sswitch_data_0

    .line 2772
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 2774
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 2770
    goto :goto_1

    .line 2779
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 2780
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2781
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 2783
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 2784
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    goto :goto_0

    .line 2788
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    .line 2789
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->state_:Ljava/lang/Object;

    goto :goto_0

    .line 2793
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    .line 2794
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->error_:Ljava/lang/Object;

    goto :goto_0

    .line 2767
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2732
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 2742
    :goto_0
    return-object v0

    .line 2733
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2734
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    .line 2736
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->hasState()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2737
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->getState()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->setState(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    .line 2739
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->hasError()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2740
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->getError()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->setError(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    :cond_3
    move-object v0, p0

    .line 2742
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2822
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 2824
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    .line 2825
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2812
    if-nez p1, :cond_0

    .line 2813
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2815
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 2817
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    .line 2818
    return-object p0
.end method

.method public setError(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2898
    if-nez p1, :cond_0

    .line 2899
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2901
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    .line 2902
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->error_:Ljava/lang/Object;

    .line 2904
    return-object p0
.end method

.method setError(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 2913
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    .line 2914
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->error_:Ljava/lang/Object;

    .line 2916
    return-void
.end method

.method public setState(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2862
    if-nez p1, :cond_0

    .line 2863
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2865
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    .line 2866
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->state_:Ljava/lang/Object;

    .line 2868
    return-object p0
.end method

.method setState(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 2877
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->bitField0_:I

    .line 2878
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->state_:Ljava/lang/Object;

    .line 2880
    return-void
.end method
