.class public final Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StateChangedPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final ERROR_FIELD_NUMBER:I = 0x3

.field public static final STATE_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private error_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private state_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2922
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;

    .line 2923
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->initFields()V

    .line 2924
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 2430
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 2523
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->memoizedIsInitialized:B

    .line 2558
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->memoizedSerializedSize:I

    .line 2431
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 2425
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 2432
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 2523
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->memoizedIsInitialized:B

    .line 2558
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->memoizedSerializedSize:I

    .line 2432
    return-void
.end method

.method static synthetic access$2102(Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 2425
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$2202(Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 2425
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->state_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$2302(Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 2425
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->error_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$2402(Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 2425
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;
    .locals 1

    .prologue
    .line 2436
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;

    return-object v0
.end method

.method private getErrorBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 2507
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->error_:Ljava/lang/Object;

    .line 2508
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2509
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 2511
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->error_:Ljava/lang/Object;

    .line 2514
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getStateBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 2475
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->state_:Ljava/lang/Object;

    .line 2476
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2477
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 2479
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->state_:Ljava/lang/Object;

    .line 2482
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 2519
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 2520
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->state_:Ljava/lang/Object;

    .line 2521
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->error_:Ljava/lang/Object;

    .line 2522
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;
    .locals 1

    .prologue
    .line 2652
    #calls: Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->access$1900()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2655
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2621
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    move-result-object v0

    .line 2622
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2623
    #calls: Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->access$1800(Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;

    move-result-object v0

    .line 2625
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2632
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    move-result-object v0

    .line 2633
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2634
    #calls: Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->access$1800(Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;

    move-result-object v0

    .line 2636
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2588
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->access$1800(Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2594
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->access$1800(Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2642
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->access$1800(Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2648
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->access$1800(Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2610
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->access$1800(Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2616
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->access$1800(Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2599
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->access$1800(Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2605
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;->access$1800(Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 2451
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2425
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;
    .locals 1

    .prologue
    .line 2440
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;

    return-object v0
.end method

.method public getError()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2493
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->error_:Ljava/lang/Object;

    .line 2494
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2495
    check-cast v0, Ljava/lang/String;

    .line 2503
    :goto_0
    return-object v0

    .line 2497
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 2499
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 2500
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2501
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->error_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2503
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2560
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->memoizedSerializedSize:I

    .line 2561
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2577
    :goto_0
    return v0

    .line 2563
    :cond_0
    const/4 v0, 0x0

    .line 2564
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 2565
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2568
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 2569
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->getStateBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2572
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 2573
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->getErrorBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2576
    :cond_3
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getState()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2461
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->state_:Ljava/lang/Object;

    .line 2462
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2463
    check-cast v0, Ljava/lang/String;

    .line 2471
    :goto_0
    return-object v0

    .line 2465
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 2467
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 2468
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2469
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->state_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2471
    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2448
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasError()Z
    .locals 2

    .prologue
    .line 2490
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasState()Z
    .locals 2

    .prologue
    .line 2458
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2525
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->memoizedIsInitialized:B

    .line 2526
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 2541
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 2526
    goto :goto_0

    .line 2528
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2529
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 2530
    goto :goto_0

    .line 2532
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->hasState()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2533
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 2534
    goto :goto_0

    .line 2536
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2537
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 2538
    goto :goto_0

    .line 2540
    :cond_4
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 2541
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2425
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;
    .locals 1

    .prologue
    .line 2653
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2425
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;
    .locals 1

    .prologue
    .line 2657
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;)Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 2582
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2546
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->getSerializedSize()I

    .line 2547
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2548
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 2550
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2551
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->getStateBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 2553
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 2554
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$StateChangedPayload;->getErrorBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 2556
    :cond_2
    return-void
.end method
