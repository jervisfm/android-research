.class public final Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StatsCollectorLogPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;,
        Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$LogLevel;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final LOGLEVEL_FIELD_NUMBER:I = 0x2

.field public static final PARAMETERS_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private logLevel_:Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$LogLevel;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private parameters_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->initFields()V

    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$148502(Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$148602(Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$LogLevel;)Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$LogLevel;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->logLevel_:Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$LogLevel;

    return-object p1
.end method

.method static synthetic access$148700(Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->parameters_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$148702(Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->parameters_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$148802(Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$LogLevel;->LOG_INFO:Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$LogLevel;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->logLevel_:Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$LogLevel;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->parameters_:Ljava/util/List;

    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;
    .locals 1

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->access$148300()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;)Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;)Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->access$148200(Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->access$148200(Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->access$148200(Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->access$148200(Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->access$148200(Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->access$148200(Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->access$148200(Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->access$148200(Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->access$148200(Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->access$148200(Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;

    return-object v0
.end method

.method public getLogLevel()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$LogLevel;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->logLevel_:Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$LogLevel;

    return-object v0
.end method

.method public getParameters(I)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->parameters_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    return-object v0
.end method

.method public getParametersCount()I
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->parameters_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getParametersList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->parameters_:Ljava/util/List;

    return-object v0
.end method

.method public getParametersOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePairOrBuilder;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->parameters_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePairOrBuilder;

    return-object v0
.end method

.method public getParametersOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$KeyValuePairOrBuilder;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->parameters_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    :goto_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_1

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->logLevel_:Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$LogLevel;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$LogLevel;->getNumber()I

    move-result v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    move v1, v2

    move v2, v0

    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->parameters_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    const/4 v3, 0x3

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->parameters_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    :cond_2
    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->memoizedSerializedSize:I

    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLogLevel()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->memoizedIsInitialized:B

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v2

    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->getParametersCount()I

    move-result v1

    if-ge v0, v1, :cond_5

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->getParameters(I)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_4

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->memoizedIsInitialized:B

    move v0, v3

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;
    .locals 1

    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;)Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->getSerializedSize()I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->logLevel_:Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$LogLevel;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$LogLevel;->getNumber()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->parameters_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    const/4 v2, 0x3

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->parameters_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    return-void
.end method
