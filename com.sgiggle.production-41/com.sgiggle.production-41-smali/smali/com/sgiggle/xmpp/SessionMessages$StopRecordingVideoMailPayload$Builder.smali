.class public final Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private type_:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 59736
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 59866
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 59909
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->TIMEOUT:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    .line 59737
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->maybeForceBuilderInitialization()V

    .line 59738
    return-void
.end method

.method static synthetic access$76900(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 59731
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$77000()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 59731
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 59773
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;

    move-result-object v0

    .line 59774
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 59775
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 59778
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 59743
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 59741
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 59731
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;
    .locals 2

    .prologue
    .line 59764
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;

    move-result-object v0

    .line 59765
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 59766
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 59768
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 59731
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;
    .locals 5

    .prologue
    .line 59782
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 59783
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->bitField0_:I

    .line 59784
    const/4 v2, 0x0

    .line 59785
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 59786
    or-int/lit8 v2, v2, 0x1

    .line 59788
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->access$77202(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 59789
    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    .line 59790
    or-int/lit8 v1, v2, 0x2

    .line 59792
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->access$77302(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    .line 59793
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->access$77402(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;I)I

    .line 59794
    return-object v0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 59731
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 59731
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 59747
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 59748
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 59749
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->bitField0_:I

    .line 59750
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->TIMEOUT:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    .line 59751
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->bitField0_:I

    .line 59752
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 59902
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 59904
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->bitField0_:I

    .line 59905
    return-object p0
.end method

.method public clearType()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 59926
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->bitField0_:I

    .line 59927
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->TIMEOUT:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    .line 59929
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 59731
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 59731
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 59731
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;
    .locals 2

    .prologue
    .line 59756
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 59731
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 59871
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 59731
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 59731
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;
    .locals 1

    .prologue
    .line 59760
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public getType()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;
    .locals 1

    .prologue
    .line 59914
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 59868
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    .prologue
    .line 59911
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 59809
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 59821
    :goto_0
    return v0

    .line 59813
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->hasType()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 59815
    goto :goto_0

    .line 59817
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 59819
    goto :goto_0

    .line 59821
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 59890
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 59892
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 59898
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->bitField0_:I

    .line 59899
    return-object p0

    .line 59895
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59731
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 59731
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59731
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59829
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 59830
    sparse-switch v0, :sswitch_data_0

    .line 59835
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 59837
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 59833
    goto :goto_1

    .line 59842
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 59843
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 59844
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 59846
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 59847
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    goto :goto_0

    .line 59851
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 59852
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    move-result-object v0

    .line 59853
    if-eqz v0, :cond_0

    .line 59854
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->bitField0_:I

    .line 59855
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    goto :goto_0

    .line 59830
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 59798
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 59805
    :goto_0
    return-object v0

    .line 59799
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 59800
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    .line 59802
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->hasType()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 59803
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->getType()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    :cond_2
    move-object v0, p0

    .line 59805
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 59884
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 59886
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->bitField0_:I

    .line 59887
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 59874
    if-nez p1, :cond_0

    .line 59875
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 59877
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 59879
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->bitField0_:I

    .line 59880
    return-object p0
.end method

.method public setType(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 59917
    if-nez p1, :cond_0

    .line 59918
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 59920
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->bitField0_:I

    .line 59921
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    .line 59923
    return-object p0
.end method
