.class public final enum Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type; = null

.field public static final enum ABORT:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type; = null

.field public static final ABORT_VALUE:I = 0x3

.field public static final enum STOP:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type; = null

.field public static final STOP_VALUE:I = 0x2

.field public static final enum TIMEOUT:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type; = null

.field public static final TIMEOUT_VALUE:I = 0x1

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 59535
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    const-string v1, "TIMEOUT"

    invoke-direct {v0, v1, v4, v4, v2}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->TIMEOUT:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    .line 59536
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    const-string v1, "STOP"

    invoke-direct {v0, v1, v2, v2, v3}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->STOP:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    .line 59537
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    const-string v1, "ABORT"

    invoke-direct {v0, v1, v3, v3, v5}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->ABORT:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    .line 59533
    new-array v0, v5, [Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->TIMEOUT:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->STOP:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->ABORT:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    .line 59561
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 59570
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 59571
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->value:I

    .line 59572
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59558
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;
    .locals 1
    .parameter

    .prologue
    .line 59548
    packed-switch p0, :pswitch_data_0

    .line 59552
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 59549
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->TIMEOUT:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    goto :goto_0

    .line 59550
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->STOP:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    goto :goto_0

    .line 59551
    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->ABORT:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    goto :goto_0

    .line 59548
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;
    .locals 1
    .parameter

    .prologue
    .line 59533
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;
    .locals 1

    .prologue
    .line 59533
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 59545
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->value:I

    return v0
.end method
