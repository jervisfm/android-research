.class public final Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StopRecordingVideoMailPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;,
        Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final TYPE_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private type_:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 59936
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;

    .line 59937
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->initFields()V

    .line 59938
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 59520
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 59602
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->memoizedIsInitialized:B

    .line 59634
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->memoizedSerializedSize:I

    .line 59521
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 59515
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 59522
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 59602
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->memoizedIsInitialized:B

    .line 59634
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->memoizedSerializedSize:I

    .line 59522
    return-void
.end method

.method static synthetic access$77202(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 59515
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$77302(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 59515
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    return-object p1
.end method

.method static synthetic access$77402(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 59515
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;
    .locals 1

    .prologue
    .line 59526
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 59599
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 59600
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->TIMEOUT:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    .line 59601
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 59724
    #calls: Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->access$77000()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 59727
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59693
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    .line 59694
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 59695
    #calls: Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->access$76900(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;

    move-result-object v0

    .line 59697
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59704
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    .line 59705
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 59706
    #calls: Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->access$76900(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;

    move-result-object v0

    .line 59708
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 59660
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->access$76900(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 59666
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->access$76900(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59714
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->access$76900(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59720
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->access$76900(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59682
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->access$76900(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59688
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->access$76900(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 59671
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->access$76900(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 59677
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->access$76900(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 59585
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 59515
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;
    .locals 1

    .prologue
    .line 59530
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 59636
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->memoizedSerializedSize:I

    .line 59637
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 59649
    :goto_0
    return v0

    .line 59639
    :cond_0
    const/4 v0, 0x0

    .line 59640
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 59641
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 59644
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 59645
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->getNumber()I

    move-result v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 59648
    :cond_2
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getType()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;
    .locals 1

    .prologue
    .line 59595
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 59582
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    .prologue
    .line 59592
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 59604
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->memoizedIsInitialized:B

    .line 59605
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 59620
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 59605
    goto :goto_0

    .line 59607
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 59608
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 59609
    goto :goto_0

    .line 59611
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->hasType()Z

    move-result v0

    if-nez v0, :cond_3

    .line 59612
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 59613
    goto :goto_0

    .line 59615
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    .line 59616
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 59617
    goto :goto_0

    .line 59619
    :cond_4
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 59620
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 59515
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 59725
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 59515
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 59729
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 59654
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 59625
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->getSerializedSize()I

    .line 59626
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 59627
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 59629
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 59630
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->getNumber()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 59632
    :cond_1
    return-void
.end method
