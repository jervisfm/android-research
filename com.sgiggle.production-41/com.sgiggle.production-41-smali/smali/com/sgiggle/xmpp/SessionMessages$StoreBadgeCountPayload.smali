.class public final Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StoreBadgeCountPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;
    }
.end annotation


# static fields
.field public static final AVATARBADGECOUNT_FIELD_NUMBER:I = 0x5

.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final GAMEBADGECOUNT_FIELD_NUMBER:I = 0x4

.field public static final VGOODBADGECOUNT_FIELD_NUMBER:I = 0x2

.field public static final VGREETINGBADGECOUNT_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;


# instance fields
.field private avatarBadgeCount_:I

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private gameBadgeCount_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private vgoodBadgeCount_:I

.field private vgreetingBadgeCount_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->initFields()V

    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$142302(Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$142402(Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->vgoodBadgeCount_:I

    return p1
.end method

.method static synthetic access$142502(Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->vgreetingBadgeCount_:I

    return p1
.end method

.method static synthetic access$142602(Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->gameBadgeCount_:I

    return p1
.end method

.method static synthetic access$142702(Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->avatarBadgeCount_:I

    return p1
.end method

.method static synthetic access$142802(Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 2

    const/4 v1, 0x0

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->vgoodBadgeCount_:I

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->vgreetingBadgeCount_:I

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->gameBadgeCount_:I

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->avatarBadgeCount_:I

    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;
    .locals 1

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->access$142100()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;)Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;)Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->access$142000(Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->access$142000(Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->access$142000(Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->access$142000(Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->access$142000(Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->access$142000(Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->access$142000(Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->access$142000(Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->access$142000(Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;->access$142000(Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAvatarBadgeCount()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->avatarBadgeCount_:I

    return v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;

    return-object v0
.end method

.method public getGameBadgeCount()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->gameBadgeCount_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->vgoodBadgeCount_:I

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    const/4 v1, 0x3

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->vgreetingBadgeCount_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->gameBadgeCount_:I

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    const/4 v1, 0x5

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->avatarBadgeCount_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getVgoodBadgeCount()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->vgoodBadgeCount_:I

    return v0
.end method

.method public getVgreetingBadgeCount()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->vgreetingBadgeCount_:I

    return v0
.end method

.method public hasAvatarBadgeCount()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasGameBadgeCount()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVgoodBadgeCount()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVgreetingBadgeCount()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->memoizedIsInitialized:B

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->memoizedIsInitialized:B

    move v0, v3

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;
    .locals 1

    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;)Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->getSerializedSize()I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->vgoodBadgeCount_:I

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    const/4 v0, 0x3

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->vgreetingBadgeCount_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->gameBadgeCount_:I

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    const/4 v0, 0x5

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->avatarBadgeCount_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    :cond_4
    return-void
.end method
