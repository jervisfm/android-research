.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "StoreBadgeCountPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getAvatarBadgeCount()I
.end method

.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getGameBadgeCount()I
.end method

.method public abstract getVgoodBadgeCount()I
.end method

.method public abstract getVgreetingBadgeCount()I
.end method

.method public abstract hasAvatarBadgeCount()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasGameBadgeCount()Z
.end method

.method public abstract hasVgoodBadgeCount()Z
.end method

.method public abstract hasVgreetingBadgeCount()Z
.end method
