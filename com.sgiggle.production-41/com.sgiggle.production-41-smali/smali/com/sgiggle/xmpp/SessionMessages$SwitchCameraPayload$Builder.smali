.class public final Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private cameraType_:I

.field private peer_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 33603
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 33743
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 33786
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->peer_:Ljava/lang/Object;

    .line 33604
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->maybeForceBuilderInitialization()V

    .line 33605
    return-void
.end method

.method static synthetic access$42300(Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 33598
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$42400()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;
    .locals 1

    .prologue
    .line 33598
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 33642
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;

    move-result-object v0

    .line 33643
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 33644
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 33647
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;
    .locals 1

    .prologue
    .line 33610
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 33608
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 33598
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;
    .locals 2

    .prologue
    .line 33633
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;

    move-result-object v0

    .line 33634
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 33635
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 33637
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 33598
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;
    .locals 5

    .prologue
    .line 33651
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 33652
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    .line 33653
    const/4 v2, 0x0

    .line 33654
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 33655
    or-int/lit8 v2, v2, 0x1

    .line 33657
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->access$42602(Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 33658
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 33659
    or-int/lit8 v2, v2, 0x2

    .line 33661
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->peer_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->peer_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->access$42702(Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33662
    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_2

    .line 33663
    or-int/lit8 v1, v2, 0x4

    .line 33665
    :goto_0
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->cameraType_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->cameraType_:I
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->access$42802(Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;I)I

    .line 33666
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->access$42902(Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;I)I

    .line 33667
    return-object v0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 33598
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 33598
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;
    .locals 1

    .prologue
    .line 33614
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 33615
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 33616
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    .line 33617
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->peer_:Ljava/lang/Object;

    .line 33618
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    .line 33619
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->cameraType_:I

    .line 33620
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    .line 33621
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;
    .locals 1

    .prologue
    .line 33779
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 33781
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    .line 33782
    return-object p0
.end method

.method public clearCameraType()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;
    .locals 1

    .prologue
    .line 33836
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    .line 33837
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->cameraType_:I

    .line 33839
    return-object p0
.end method

.method public clearPeer()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;
    .locals 1

    .prologue
    .line 33810
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    .line 33811
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->getPeer()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->peer_:Ljava/lang/Object;

    .line 33813
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 33598
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 33598
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 33598
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;
    .locals 2

    .prologue
    .line 33625
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 33598
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 33748
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCameraType()I
    .locals 1

    .prologue
    .line 33827
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->cameraType_:I

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 33598
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 33598
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;
    .locals 1

    .prologue
    .line 33629
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;

    move-result-object v0

    return-object v0
.end method

.method public getPeer()Ljava/lang/String;
    .locals 2

    .prologue
    .line 33791
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->peer_:Ljava/lang/Object;

    .line 33792
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 33793
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 33794
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->peer_:Ljava/lang/Object;

    .line 33797
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 33745
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCameraType()Z
    .locals 2

    .prologue
    .line 33824
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPeer()Z
    .locals 2

    .prologue
    .line 33788
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33685
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 33697
    :goto_0
    return v0

    .line 33689
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->hasPeer()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 33691
    goto :goto_0

    .line 33693
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 33695
    goto :goto_0

    .line 33697
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 33767
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 33769
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 33775
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    .line 33776
    return-object p0

    .line 33772
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 33598
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 33598
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 33598
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 33705
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 33706
    sparse-switch v0, :sswitch_data_0

    .line 33711
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 33713
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 33709
    goto :goto_1

    .line 33718
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 33719
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 33720
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 33722
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 33723
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    goto :goto_0

    .line 33727
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    .line 33728
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->peer_:Ljava/lang/Object;

    goto :goto_0

    .line 33732
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    .line 33733
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->cameraType_:I

    goto :goto_0

    .line 33706
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 33671
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 33681
    :goto_0
    return-object v0

    .line 33672
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 33673
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    .line 33675
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->hasPeer()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 33676
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->getPeer()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->setPeer(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    .line 33678
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->hasCameraType()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 33679
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->getCameraType()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->setCameraType(I)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    :cond_3
    move-object v0, p0

    .line 33681
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 33761
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 33763
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    .line 33764
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 33751
    if-nez p1, :cond_0

    .line 33752
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 33754
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 33756
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    .line 33757
    return-object p0
.end method

.method public setCameraType(I)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 33830
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    .line 33831
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->cameraType_:I

    .line 33833
    return-object p0
.end method

.method public setPeer(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 33801
    if-nez p1, :cond_0

    .line 33802
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 33804
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    .line 33805
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->peer_:Ljava/lang/Object;

    .line 33807
    return-object p0
.end method

.method setPeer(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 33816
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->bitField0_:I

    .line 33817
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->peer_:Ljava/lang/Object;

    .line 33819
    return-void
.end method
