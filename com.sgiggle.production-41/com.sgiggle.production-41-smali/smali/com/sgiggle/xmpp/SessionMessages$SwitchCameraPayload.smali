.class public final Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SwitchCameraPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CAMERATYPE_FIELD_NUMBER:I = 0x3

.field public static final PEER_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private cameraType_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private peer_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33846
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;

    .line 33847
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->initFields()V

    .line 33848
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 33391
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 33462
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->memoizedIsInitialized:B

    .line 33497
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->memoizedSerializedSize:I

    .line 33392
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 33386
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 33393
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 33462
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->memoizedIsInitialized:B

    .line 33497
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->memoizedSerializedSize:I

    .line 33393
    return-void
.end method

.method static synthetic access$42602(Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 33386
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$42702(Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 33386
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->peer_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$42802(Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 33386
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->cameraType_:I

    return p1
.end method

.method static synthetic access$42902(Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 33386
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;
    .locals 1

    .prologue
    .line 33397
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;

    return-object v0
.end method

.method private getPeerBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 33436
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->peer_:Ljava/lang/Object;

    .line 33437
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 33438
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 33440
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->peer_:Ljava/lang/Object;

    .line 33443
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 33458
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 33459
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->peer_:Ljava/lang/Object;

    .line 33460
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->cameraType_:I

    .line 33461
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;
    .locals 1

    .prologue
    .line 33591
    #calls: Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->access$42400()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 33594
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 33560
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    move-result-object v0

    .line 33561
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 33562
    #calls: Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->access$42300(Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;

    move-result-object v0

    .line 33564
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 33571
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    move-result-object v0

    .line 33572
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 33573
    #calls: Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->access$42300(Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;

    move-result-object v0

    .line 33575
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 33527
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->access$42300(Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 33533
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->access$42300(Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 33581
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->access$42300(Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 33587
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->access$42300(Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 33549
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->access$42300(Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 33555
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->access$42300(Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 33538
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->access$42300(Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 33544
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;->access$42300(Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 33412
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCameraType()I
    .locals 1

    .prologue
    .line 33454
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->cameraType_:I

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 33386
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;
    .locals 1

    .prologue
    .line 33401
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;

    return-object v0
.end method

.method public getPeer()Ljava/lang/String;
    .locals 2

    .prologue
    .line 33422
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->peer_:Ljava/lang/Object;

    .line 33423
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 33424
    check-cast v0, Ljava/lang/String;

    .line 33432
    :goto_0
    return-object v0

    .line 33426
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 33428
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 33429
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 33430
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->peer_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 33432
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 33499
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->memoizedSerializedSize:I

    .line 33500
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 33516
    :goto_0
    return v0

    .line 33502
    :cond_0
    const/4 v0, 0x0

    .line 33503
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 33504
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33507
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 33508
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->getPeerBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33511
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 33512
    const/4 v1, 0x3

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->cameraType_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 33515
    :cond_3
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 33409
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCameraType()Z
    .locals 2

    .prologue
    .line 33451
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPeer()Z
    .locals 2

    .prologue
    .line 33419
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 33464
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->memoizedIsInitialized:B

    .line 33465
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 33480
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 33465
    goto :goto_0

    .line 33467
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 33468
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 33469
    goto :goto_0

    .line 33471
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->hasPeer()Z

    move-result v0

    if-nez v0, :cond_3

    .line 33472
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 33473
    goto :goto_0

    .line 33475
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    .line 33476
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 33477
    goto :goto_0

    .line 33479
    :cond_4
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 33480
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 33386
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;
    .locals 1

    .prologue
    .line 33592
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 33386
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;
    .locals 1

    .prologue
    .line 33596
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;)Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 33521
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 33485
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->getSerializedSize()I

    .line 33486
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 33487
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 33489
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 33490
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->getPeerBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 33492
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 33493
    const/4 v0, 0x3

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayload;->cameraType_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 33495
    :cond_2
    return-void
.end method
