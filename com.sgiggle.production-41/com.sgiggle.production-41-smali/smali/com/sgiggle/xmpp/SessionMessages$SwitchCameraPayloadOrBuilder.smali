.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$SwitchCameraPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SwitchCameraPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getCameraType()I
.end method

.method public abstract getPeer()Ljava/lang/String;
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasCameraType()Z
.end method

.method public abstract hasPeer()Z
.end method
