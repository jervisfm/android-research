.class public final Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private devicetoken_:Ljava/lang/Object;

.field private devicetokentype_:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 12134
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 12274
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 12317
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->devicetoken_:Ljava/lang/Object;

    .line 12353
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;->DEVICE_TOKEN_UNKNOWN:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->devicetokentype_:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    .line 12135
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->maybeForceBuilderInitialization()V

    .line 12136
    return-void
.end method

.method static synthetic access$14700(Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 12129
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$14800()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;
    .locals 1

    .prologue
    .line 12129
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 12173
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    move-result-object v0

    .line 12174
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 12175
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 12178
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;
    .locals 1

    .prologue
    .line 12141
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 12139
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 12129
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;
    .locals 2

    .prologue
    .line 12164
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    move-result-object v0

    .line 12165
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 12166
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 12168
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 12129
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;
    .locals 5

    .prologue
    .line 12182
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 12183
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    .line 12184
    const/4 v2, 0x0

    .line 12185
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 12186
    or-int/lit8 v2, v2, 0x1

    .line 12188
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->access$15002(Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 12189
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 12190
    or-int/lit8 v2, v2, 0x2

    .line 12192
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->devicetoken_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->devicetoken_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->access$15102(Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 12193
    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_2

    .line 12194
    or-int/lit8 v1, v2, 0x4

    .line 12196
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->devicetokentype_:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->devicetokentype_:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->access$15202(Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;)Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    .line 12197
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->access$15302(Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;I)I

    .line 12198
    return-object v0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 12129
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 12129
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;
    .locals 1

    .prologue
    .line 12145
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 12146
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 12147
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    .line 12148
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->devicetoken_:Ljava/lang/Object;

    .line 12149
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    .line 12150
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;->DEVICE_TOKEN_UNKNOWN:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->devicetokentype_:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    .line 12151
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    .line 12152
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;
    .locals 1

    .prologue
    .line 12310
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 12312
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    .line 12313
    return-object p0
.end method

.method public clearDevicetoken()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;
    .locals 1

    .prologue
    .line 12341
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    .line 12342
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->getDevicetoken()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->devicetoken_:Ljava/lang/Object;

    .line 12344
    return-object p0
.end method

.method public clearDevicetokentype()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;
    .locals 1

    .prologue
    .line 12370
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    .line 12371
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;->DEVICE_TOKEN_UNKNOWN:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->devicetokentype_:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    .line 12373
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 12129
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 12129
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 12129
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;
    .locals 2

    .prologue
    .line 12156
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 12129
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 12279
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 12129
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 12129
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;
    .locals 1

    .prologue
    .line 12160
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDevicetoken()Ljava/lang/String;
    .locals 2

    .prologue
    .line 12322
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->devicetoken_:Ljava/lang/Object;

    .line 12323
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 12324
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 12325
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->devicetoken_:Ljava/lang/Object;

    .line 12328
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getDevicetokentype()Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;
    .locals 1

    .prologue
    .line 12358
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->devicetokentype_:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 12276
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDevicetoken()Z
    .locals 2

    .prologue
    .line 12319
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDevicetokentype()Z
    .locals 2

    .prologue
    .line 12355
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 12216
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 12224
    :goto_0
    return v0

    .line 12220
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 12222
    goto :goto_0

    .line 12224
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 12298
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 12300
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 12306
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    .line 12307
    return-object p0

    .line 12303
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 12129
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 12129
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 12129
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 12232
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 12233
    sparse-switch v0, :sswitch_data_0

    .line 12238
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 12240
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 12236
    goto :goto_1

    .line 12245
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 12246
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 12247
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 12249
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 12250
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    goto :goto_0

    .line 12254
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    .line 12255
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->devicetoken_:Ljava/lang/Object;

    goto :goto_0

    .line 12259
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 12260
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    move-result-object v0

    .line 12261
    if-eqz v0, :cond_0

    .line 12262
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    .line 12263
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->devicetokentype_:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    goto :goto_0

    .line 12233
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 12202
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 12212
    :goto_0
    return-object v0

    .line 12203
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 12204
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    .line 12206
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->hasDevicetoken()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 12207
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->getDevicetoken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->setDevicetoken(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    .line 12209
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->hasDevicetokentype()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 12210
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->getDevicetokentype()Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->setDevicetokentype(Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    :cond_3
    move-object v0, p0

    .line 12212
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 12292
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 12294
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    .line 12295
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 12282
    if-nez p1, :cond_0

    .line 12283
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 12285
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 12287
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    .line 12288
    return-object p0
.end method

.method public setDevicetoken(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 12332
    if-nez p1, :cond_0

    .line 12333
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 12335
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    .line 12336
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->devicetoken_:Ljava/lang/Object;

    .line 12338
    return-object p0
.end method

.method setDevicetoken(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 12347
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    .line 12348
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->devicetoken_:Ljava/lang/Object;

    .line 12350
    return-void
.end method

.method public setDevicetokentype(Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 12361
    if-nez p1, :cond_0

    .line 12362
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 12364
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->bitField0_:I

    .line 12365
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->devicetokentype_:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    .line 12367
    return-object p0
.end method
