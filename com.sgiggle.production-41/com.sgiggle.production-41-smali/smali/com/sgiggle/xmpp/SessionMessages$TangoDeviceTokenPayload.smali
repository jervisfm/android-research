.class public final Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TangoDeviceTokenPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final DEVICETOKENTYPE_FIELD_NUMBER:I = 0x3

.field public static final DEVICETOKEN_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private devicetoken_:Ljava/lang/Object;

.field private devicetokentype_:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 12380
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    .line 12381
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->initFields()V

    .line 12382
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 11926
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 11997
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->memoizedIsInitialized:B

    .line 12028
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->memoizedSerializedSize:I

    .line 11927
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 11921
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 11928
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 11997
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->memoizedIsInitialized:B

    .line 12028
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->memoizedSerializedSize:I

    .line 11928
    return-void
.end method

.method static synthetic access$15002(Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 11921
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$15102(Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 11921
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->devicetoken_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$15202(Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;)Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 11921
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->devicetokentype_:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    return-object p1
.end method

.method static synthetic access$15302(Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 11921
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;
    .locals 1

    .prologue
    .line 11932
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    return-object v0
.end method

.method private getDevicetokenBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 11971
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->devicetoken_:Ljava/lang/Object;

    .line 11972
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 11973
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 11975
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->devicetoken_:Ljava/lang/Object;

    .line 11978
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 11993
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 11994
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->devicetoken_:Ljava/lang/Object;

    .line 11995
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;->DEVICE_TOKEN_UNKNOWN:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->devicetokentype_:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    .line 11996
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;
    .locals 1

    .prologue
    .line 12122
    #calls: Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->access$14800()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 12125
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 12091
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    .line 12092
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 12093
    #calls: Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->access$14700(Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    move-result-object v0

    .line 12095
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 12102
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    .line 12103
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 12104
    #calls: Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->access$14700(Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    move-result-object v0

    .line 12106
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 12058
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->access$14700(Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 12064
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->access$14700(Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 12112
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->access$14700(Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 12118
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->access$14700(Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 12080
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->access$14700(Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 12086
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->access$14700(Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 12069
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->access$14700(Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 12075
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->access$14700(Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 11947
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 11921
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;
    .locals 1

    .prologue
    .line 11936
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    return-object v0
.end method

.method public getDevicetoken()Ljava/lang/String;
    .locals 2

    .prologue
    .line 11957
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->devicetoken_:Ljava/lang/Object;

    .line 11958
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 11959
    check-cast v0, Ljava/lang/String;

    .line 11967
    :goto_0
    return-object v0

    .line 11961
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 11963
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 11964
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 11965
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->devicetoken_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 11967
    goto :goto_0
.end method

.method public getDevicetokentype()Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;
    .locals 1

    .prologue
    .line 11989
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->devicetokentype_:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 12030
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->memoizedSerializedSize:I

    .line 12031
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 12047
    :goto_0
    return v0

    .line 12033
    :cond_0
    const/4 v0, 0x0

    .line 12034
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 12035
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12038
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 12039
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->getDevicetokenBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12042
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 12043
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->devicetokentype_:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 12046
    :cond_3
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 11944
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDevicetoken()Z
    .locals 2

    .prologue
    .line 11954
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDevicetokentype()Z
    .locals 2

    .prologue
    .line 11986
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 11999
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->memoizedIsInitialized:B

    .line 12000
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 12011
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 12000
    goto :goto_0

    .line 12002
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 12003
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 12004
    goto :goto_0

    .line 12006
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 12007
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 12008
    goto :goto_0

    .line 12010
    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 12011
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 11921
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;
    .locals 1

    .prologue
    .line 12123
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 11921
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;
    .locals 1

    .prologue
    .line 12127
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 12052
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 12016
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->getSerializedSize()I

    .line 12017
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 12018
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 12020
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 12021
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->getDevicetokenBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 12023
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 12024
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->devicetokentype_:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 12026
    :cond_2
    return-void
.end method
