.class public final enum Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TangoPushDisplayType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType; = null

.field public static final enum TP_CANCEL_INCOMING_CALL:Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType; = null

.field public static final TP_CANCEL_INCOMING_CALL_VALUE:I = 0x2

.field public static final enum TP_DISPLAY_INCOMING_CALL:Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType; = null

.field public static final TP_DISPLAY_INCOMING_CALL_VALUE:I = 0x1

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 683
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;

    const-string v1, "TP_DISPLAY_INCOMING_CALL"

    invoke-direct {v0, v1, v3, v3, v2}, Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;->TP_DISPLAY_INCOMING_CALL:Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;

    .line 684
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;

    const-string v1, "TP_CANCEL_INCOMING_CALL"

    invoke-direct {v0, v1, v2, v2, v4}, Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;->TP_CANCEL_INCOMING_CALL:Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;

    .line 681
    new-array v0, v4, [Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;->TP_DISPLAY_INCOMING_CALL:Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;->TP_CANCEL_INCOMING_CALL:Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;

    .line 706
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 715
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 716
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;->value:I

    .line 717
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 703
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;
    .locals 1
    .parameter

    .prologue
    .line 694
    packed-switch p0, :pswitch_data_0

    .line 697
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 695
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;->TP_DISPLAY_INCOMING_CALL:Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;

    goto :goto_0

    .line 696
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;->TP_CANCEL_INCOMING_CALL:Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;

    goto :goto_0

    .line 694
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;
    .locals 1
    .parameter

    .prologue
    .line 681
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;
    .locals 1

    .prologue
    .line 681
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 691
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;->value:I

    return v0
.end method
