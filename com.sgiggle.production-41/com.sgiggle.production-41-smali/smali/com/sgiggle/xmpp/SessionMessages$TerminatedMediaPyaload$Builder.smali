.class public final Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;",
        "Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaloadOrBuilder;"
    }
.end annotation


# instance fields
.field private accountId_:Ljava/lang/Object;

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private calleeMissedCall_:Z

.field private displayname_:Ljava/lang/Object;

.field private shouldEnterBackground_:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 11559
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 11739
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 11782
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->accountId_:Ljava/lang/Object;

    .line 11818
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->displayname_:Ljava/lang/Object;

    .line 11560
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->maybeForceBuilderInitialization()V

    .line 11561
    return-void
.end method

.method static synthetic access$13800(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 11554
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$13900()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;
    .locals 1

    .prologue
    .line 11554
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 11602
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;

    move-result-object v0

    .line 11603
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 11604
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 11607
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;
    .locals 1

    .prologue
    .line 11566
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 11564
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 11554
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;
    .locals 2

    .prologue
    .line 11593
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;

    move-result-object v0

    .line 11594
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 11595
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 11597
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 11554
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;
    .locals 5

    .prologue
    .line 11611
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 11612
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    .line 11613
    const/4 v2, 0x0

    .line 11614
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 11615
    or-int/lit8 v2, v2, 0x1

    .line 11617
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->access$14102(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 11618
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 11619
    or-int/lit8 v2, v2, 0x2

    .line 11621
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->accountId_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->accountId_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->access$14202(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 11622
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 11623
    or-int/lit8 v2, v2, 0x4

    .line 11625
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->displayname_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->displayname_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->access$14302(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 11626
    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 11627
    or-int/lit8 v2, v2, 0x8

    .line 11629
    :cond_3
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->calleeMissedCall_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->calleeMissedCall_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->access$14402(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;Z)Z

    .line 11630
    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_4

    .line 11631
    or-int/lit8 v1, v2, 0x10

    .line 11633
    :goto_0
    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->shouldEnterBackground_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->shouldEnterBackground_:Z
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->access$14502(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;Z)Z

    .line 11634
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->access$14602(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;I)I

    .line 11635
    return-object v0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 11554
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 11554
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 11570
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 11571
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 11572
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    .line 11573
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->accountId_:Ljava/lang/Object;

    .line 11574
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    .line 11575
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->displayname_:Ljava/lang/Object;

    .line 11576
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    .line 11577
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->calleeMissedCall_:Z

    .line 11578
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    .line 11579
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->shouldEnterBackground_:Z

    .line 11580
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    .line 11581
    return-object p0
.end method

.method public clearAccountId()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;
    .locals 1

    .prologue
    .line 11806
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    .line 11807
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->getAccountId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->accountId_:Ljava/lang/Object;

    .line 11809
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;
    .locals 1

    .prologue
    .line 11775
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 11777
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    .line 11778
    return-object p0
.end method

.method public clearCalleeMissedCall()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;
    .locals 1

    .prologue
    .line 11868
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    .line 11869
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->calleeMissedCall_:Z

    .line 11871
    return-object p0
.end method

.method public clearDisplayname()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;
    .locals 1

    .prologue
    .line 11842
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    .line 11843
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->getDisplayname()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->displayname_:Ljava/lang/Object;

    .line 11845
    return-object p0
.end method

.method public clearShouldEnterBackground()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;
    .locals 1

    .prologue
    .line 11889
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    .line 11890
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->shouldEnterBackground_:Z

    .line 11892
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 11554
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 11554
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 11554
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;
    .locals 2

    .prologue
    .line 11585
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 11554
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAccountId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 11787
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->accountId_:Ljava/lang/Object;

    .line 11788
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 11789
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 11790
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->accountId_:Ljava/lang/Object;

    .line 11793
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 11744
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCalleeMissedCall()Z
    .locals 1

    .prologue
    .line 11859
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->calleeMissedCall_:Z

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 11554
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 11554
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;
    .locals 1

    .prologue
    .line 11589
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 11823
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->displayname_:Ljava/lang/Object;

    .line 11824
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 11825
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 11826
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->displayname_:Ljava/lang/Object;

    .line 11829
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getShouldEnterBackground()Z
    .locals 1

    .prologue
    .line 11880
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->shouldEnterBackground_:Z

    return v0
.end method

.method public hasAccountId()Z
    .locals 2

    .prologue
    .line 11784
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 11741
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCalleeMissedCall()Z
    .locals 2

    .prologue
    .line 11856
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDisplayname()Z
    .locals 2

    .prologue
    .line 11820
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasShouldEnterBackground()Z
    .locals 2

    .prologue
    .line 11877
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 11659
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 11683
    :goto_0
    return v0

    .line 11663
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->hasAccountId()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 11665
    goto :goto_0

    .line 11667
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->hasDisplayname()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 11669
    goto :goto_0

    .line 11671
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->hasCalleeMissedCall()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 11673
    goto :goto_0

    .line 11675
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->hasShouldEnterBackground()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 11677
    goto :goto_0

    .line 11679
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 11681
    goto :goto_0

    .line 11683
    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 11763
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 11765
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 11771
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    .line 11772
    return-object p0

    .line 11768
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 11554
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 11554
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 11554
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 11691
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 11692
    sparse-switch v0, :sswitch_data_0

    .line 11697
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 11699
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 11695
    goto :goto_1

    .line 11704
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 11705
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 11706
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 11708
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 11709
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    goto :goto_0

    .line 11713
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    .line 11714
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->accountId_:Ljava/lang/Object;

    goto :goto_0

    .line 11718
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    .line 11719
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->displayname_:Ljava/lang/Object;

    goto :goto_0

    .line 11723
    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    .line 11724
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->calleeMissedCall_:Z

    goto :goto_0

    .line 11728
    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    .line 11729
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->shouldEnterBackground_:Z

    goto :goto_0

    .line 11692
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 11639
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 11655
    :goto_0
    return-object v0

    .line 11640
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 11641
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    .line 11643
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->hasAccountId()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 11644
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->getAccountId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->setAccountId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    .line 11646
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->hasDisplayname()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 11647
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->getDisplayname()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->setDisplayname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    .line 11649
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->hasCalleeMissedCall()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 11650
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->getCalleeMissedCall()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->setCalleeMissedCall(Z)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    .line 11652
    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->hasShouldEnterBackground()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 11653
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->getShouldEnterBackground()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->setShouldEnterBackground(Z)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    :cond_5
    move-object v0, p0

    .line 11655
    goto :goto_0
.end method

.method public setAccountId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 11797
    if-nez p1, :cond_0

    .line 11798
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 11800
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    .line 11801
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->accountId_:Ljava/lang/Object;

    .line 11803
    return-object p0
.end method

.method setAccountId(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 11812
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    .line 11813
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->accountId_:Ljava/lang/Object;

    .line 11815
    return-void
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 11757
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 11759
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    .line 11760
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 11747
    if-nez p1, :cond_0

    .line 11748
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 11750
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 11752
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    .line 11753
    return-object p0
.end method

.method public setCalleeMissedCall(Z)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 11862
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    .line 11863
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->calleeMissedCall_:Z

    .line 11865
    return-object p0
.end method

.method public setDisplayname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 11833
    if-nez p1, :cond_0

    .line 11834
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 11836
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    .line 11837
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->displayname_:Ljava/lang/Object;

    .line 11839
    return-object p0
.end method

.method setDisplayname(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 11848
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    .line 11849
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->displayname_:Ljava/lang/Object;

    .line 11851
    return-void
.end method

.method public setShouldEnterBackground(Z)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 11883
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->bitField0_:I

    .line 11884
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->shouldEnterBackground_:Z

    .line 11886
    return-object p0
.end method
