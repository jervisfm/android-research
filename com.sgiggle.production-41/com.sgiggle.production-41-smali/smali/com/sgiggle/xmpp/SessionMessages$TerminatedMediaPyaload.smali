.class public final Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TerminatedMediaPyaload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;
    }
.end annotation


# static fields
.field public static final ACCOUNTID_FIELD_NUMBER:I = 0x2

.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CALLEE_MISSED_CALL_FIELD_NUMBER:I = 0x4

.field public static final DISPLAYNAME_FIELD_NUMBER:I = 0x3

.field public static final SHOULD_ENTER_BACKGROUND_FIELD_NUMBER:I = 0x5

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;


# instance fields
.field private accountId_:Ljava/lang/Object;

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private calleeMissedCall_:Z

.field private displayname_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private shouldEnterBackground_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 11899
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;

    .line 11900
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->initFields()V

    .line 11901
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 11277
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 11392
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->memoizedIsInitialized:B

    .line 11445
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->memoizedSerializedSize:I

    .line 11278
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 11272
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 11279
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 11392
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->memoizedIsInitialized:B

    .line 11445
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->memoizedSerializedSize:I

    .line 11279
    return-void
.end method

.method static synthetic access$14102(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 11272
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$14202(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 11272
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->accountId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$14302(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 11272
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->displayname_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$14402(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 11272
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->calleeMissedCall_:Z

    return p1
.end method

.method static synthetic access$14502(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 11272
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->shouldEnterBackground_:Z

    return p1
.end method

.method static synthetic access$14602(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 11272
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->bitField0_:I

    return p1
.end method

.method private getAccountIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 11322
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->accountId_:Ljava/lang/Object;

    .line 11323
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 11324
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 11326
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->accountId_:Ljava/lang/Object;

    .line 11329
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;
    .locals 1

    .prologue
    .line 11283
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;

    return-object v0
.end method

.method private getDisplaynameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 11354
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->displayname_:Ljava/lang/Object;

    .line 11355
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 11356
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 11358
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->displayname_:Ljava/lang/Object;

    .line 11361
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 11386
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 11387
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->accountId_:Ljava/lang/Object;

    .line 11388
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->displayname_:Ljava/lang/Object;

    .line 11389
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->calleeMissedCall_:Z

    .line 11390
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->shouldEnterBackground_:Z

    .line 11391
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;
    .locals 1

    .prologue
    .line 11547
    #calls: Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->access$13900()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 11550
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 11516
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    move-result-object v0

    .line 11517
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11518
    #calls: Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->access$13800(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;

    move-result-object v0

    .line 11520
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 11527
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    move-result-object v0

    .line 11528
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 11529
    #calls: Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->access$13800(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;

    move-result-object v0

    .line 11531
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 11483
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->access$13800(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 11489
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->access$13800(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 11537
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->access$13800(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 11543
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->access$13800(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 11505
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->access$13800(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 11511
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->access$13800(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 11494
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->access$13800(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 11500
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;->access$13800(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAccountId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 11308
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->accountId_:Ljava/lang/Object;

    .line 11309
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 11310
    check-cast v0, Ljava/lang/String;

    .line 11318
    :goto_0
    return-object v0

    .line 11312
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 11314
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 11315
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 11316
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->accountId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 11318
    goto :goto_0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 11298
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCalleeMissedCall()Z
    .locals 1

    .prologue
    .line 11372
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->calleeMissedCall_:Z

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 11272
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;
    .locals 1

    .prologue
    .line 11287
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;

    return-object v0
.end method

.method public getDisplayname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 11340
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->displayname_:Ljava/lang/Object;

    .line 11341
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 11342
    check-cast v0, Ljava/lang/String;

    .line 11350
    :goto_0
    return-object v0

    .line 11344
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 11346
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 11347
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 11348
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->displayname_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 11350
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 11447
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->memoizedSerializedSize:I

    .line 11448
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 11472
    :goto_0
    return v0

    .line 11450
    :cond_0
    const/4 v0, 0x0

    .line 11451
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 11452
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11455
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 11456
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->getAccountIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11459
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 11460
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->getDisplaynameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11463
    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 11464
    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->calleeMissedCall_:Z

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 11467
    :cond_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 11468
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->shouldEnterBackground_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 11471
    :cond_5
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getShouldEnterBackground()Z
    .locals 1

    .prologue
    .line 11382
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->shouldEnterBackground_:Z

    return v0
.end method

.method public hasAccountId()Z
    .locals 2

    .prologue
    .line 11305
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 11295
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCalleeMissedCall()Z
    .locals 2

    .prologue
    .line 11369
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDisplayname()Z
    .locals 2

    .prologue
    .line 11337
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasShouldEnterBackground()Z
    .locals 2

    .prologue
    .line 11379
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 11394
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->memoizedIsInitialized:B

    .line 11395
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 11422
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 11395
    goto :goto_0

    .line 11397
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 11398
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->memoizedIsInitialized:B

    move v0, v2

    .line 11399
    goto :goto_0

    .line 11401
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->hasAccountId()Z

    move-result v0

    if-nez v0, :cond_3

    .line 11402
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->memoizedIsInitialized:B

    move v0, v2

    .line 11403
    goto :goto_0

    .line 11405
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->hasDisplayname()Z

    move-result v0

    if-nez v0, :cond_4

    .line 11406
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->memoizedIsInitialized:B

    move v0, v2

    .line 11407
    goto :goto_0

    .line 11409
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->hasCalleeMissedCall()Z

    move-result v0

    if-nez v0, :cond_5

    .line 11410
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->memoizedIsInitialized:B

    move v0, v2

    .line 11411
    goto :goto_0

    .line 11413
    :cond_5
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->hasShouldEnterBackground()Z

    move-result v0

    if-nez v0, :cond_6

    .line 11414
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->memoizedIsInitialized:B

    move v0, v2

    .line 11415
    goto :goto_0

    .line 11417
    :cond_6
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_7

    .line 11418
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->memoizedIsInitialized:B

    move v0, v2

    .line 11419
    goto :goto_0

    .line 11421
    :cond_7
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->memoizedIsInitialized:B

    move v0, v3

    .line 11422
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 11272
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;
    .locals 1

    .prologue
    .line 11548
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 11272
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;
    .locals 1

    .prologue
    .line 11552
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;)Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 11477
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 11427
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->getSerializedSize()I

    .line 11428
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 11429
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 11431
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 11432
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->getAccountIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 11434
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 11435
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->getDisplaynameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 11437
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 11438
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->calleeMissedCall_:Z

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 11440
    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 11441
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaload;->shouldEnterBackground_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 11443
    :cond_4
    return-void
.end method
