.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$TerminatedMediaPyaloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TerminatedMediaPyaloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getAccountId()Ljava/lang/String;
.end method

.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getCalleeMissedCall()Z
.end method

.method public abstract getDisplayname()Ljava/lang/String;
.end method

.method public abstract getShouldEnterBackground()Z
.end method

.method public abstract hasAccountId()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasCalleeMissedCall()Z
.end method

.method public abstract hasDisplayname()Z
.end method

.method public abstract hasShouldEnterBackground()Z
.end method
