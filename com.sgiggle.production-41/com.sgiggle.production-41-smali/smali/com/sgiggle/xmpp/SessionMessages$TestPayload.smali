.class public final Lcom/sgiggle/xmpp/SessionMessages$TestPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$TestPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TestPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;
    }
.end annotation


# static fields
.field public static final BAR_FIELD_NUMBER:I = 0x3

.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final FOO_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$TestPayload;


# instance fields
.field private bar_:I

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private foo_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 12870
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$TestPayload;

    .line 12871
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$TestPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->initFields()V

    .line 12872
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 12407
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 12478
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->memoizedIsInitialized:B

    .line 12517
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->memoizedSerializedSize:I

    .line 12408
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 12402
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 12409
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 12478
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->memoizedIsInitialized:B

    .line 12517
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->memoizedSerializedSize:I

    .line 12409
    return-void
.end method

.method static synthetic access$15702(Lcom/sgiggle/xmpp/SessionMessages$TestPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 12402
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$15802(Lcom/sgiggle/xmpp/SessionMessages$TestPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 12402
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->foo_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$15902(Lcom/sgiggle/xmpp/SessionMessages$TestPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 12402
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->bar_:I

    return p1
.end method

.method static synthetic access$16002(Lcom/sgiggle/xmpp/SessionMessages$TestPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 12402
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$TestPayload;
    .locals 1

    .prologue
    .line 12413
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$TestPayload;

    return-object v0
.end method

.method private getFooBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 12452
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->foo_:Ljava/lang/Object;

    .line 12453
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 12454
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 12456
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->foo_:Ljava/lang/Object;

    .line 12459
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 12474
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 12475
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->foo_:Ljava/lang/Object;

    .line 12476
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->bar_:I

    .line 12477
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;
    .locals 1

    .prologue
    .line 12611
    #calls: Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->access$15500()Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$TestPayload;)Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 12614
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$TestPayload;)Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$TestPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 12580
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;

    move-result-object v0

    .line 12581
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 12582
    #calls: Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->access$15400(Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TestPayload;

    move-result-object v0

    .line 12584
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TestPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 12591
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;

    move-result-object v0

    .line 12592
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 12593
    #calls: Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->access$15400(Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TestPayload;

    move-result-object v0

    .line 12595
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$TestPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 12547
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->access$15400(Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TestPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 12553
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->access$15400(Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$TestPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 12601
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->access$15400(Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TestPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 12607
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->access$15400(Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$TestPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 12569
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->access$15400(Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TestPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 12575
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->access$15400(Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$TestPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 12558
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->access$15400(Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TestPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 12564
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->access$15400(Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TestPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBar()I
    .locals 1

    .prologue
    .line 12470
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->bar_:I

    return v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 12428
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 12402
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$TestPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$TestPayload;
    .locals 1

    .prologue
    .line 12417
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$TestPayload;

    return-object v0
.end method

.method public getFoo()Ljava/lang/String;
    .locals 2

    .prologue
    .line 12438
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->foo_:Ljava/lang/Object;

    .line 12439
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 12440
    check-cast v0, Ljava/lang/String;

    .line 12448
    :goto_0
    return-object v0

    .line 12442
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 12444
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 12445
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 12446
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->foo_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 12448
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 12519
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->memoizedSerializedSize:I

    .line 12520
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 12536
    :goto_0
    return v0

    .line 12522
    :cond_0
    const/4 v0, 0x0

    .line 12523
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 12524
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12527
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 12528
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->getFooBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12531
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 12532
    const/4 v1, 0x3

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->bar_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 12535
    :cond_3
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public hasBar()Z
    .locals 2

    .prologue
    .line 12467
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 12425
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFoo()Z
    .locals 2

    .prologue
    .line 12435
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 12480
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->memoizedIsInitialized:B

    .line 12481
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 12500
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 12481
    goto :goto_0

    .line 12483
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 12484
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 12485
    goto :goto_0

    .line 12487
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->hasFoo()Z

    move-result v0

    if-nez v0, :cond_3

    .line 12488
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 12489
    goto :goto_0

    .line 12491
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->hasBar()Z

    move-result v0

    if-nez v0, :cond_4

    .line 12492
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 12493
    goto :goto_0

    .line 12495
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_5

    .line 12496
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 12497
    goto :goto_0

    .line 12499
    :cond_5
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 12500
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 12402
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;
    .locals 1

    .prologue
    .line 12612
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 12402
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;
    .locals 1

    .prologue
    .line 12616
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$TestPayload;)Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 12541
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 12505
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->getSerializedSize()I

    .line 12506
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 12507
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 12509
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 12510
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->getFooBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 12512
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 12513
    const/4 v0, 0x3

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->bar_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 12515
    :cond_2
    return-void
.end method
