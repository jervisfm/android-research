.class public final Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private delay_:I

.field private message_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 13105
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 13241
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 13305
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->message_:Ljava/lang/Object;

    .line 13106
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->maybeForceBuilderInitialization()V

    .line 13107
    return-void
.end method

.method static synthetic access$16100(Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 13100
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$16200()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;
    .locals 1

    .prologue
    .line 13100
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 13144
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;

    move-result-object v0

    .line 13145
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 13146
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 13149
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;
    .locals 1

    .prologue
    .line 13112
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 13110
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 13100
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;
    .locals 2

    .prologue
    .line 13135
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;

    move-result-object v0

    .line 13136
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 13137
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 13139
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 13100
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;
    .locals 5

    .prologue
    .line 13153
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 13154
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    .line 13155
    const/4 v2, 0x0

    .line 13156
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 13157
    or-int/lit8 v2, v2, 0x1

    .line 13159
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->access$16402(Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 13160
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 13161
    or-int/lit8 v2, v2, 0x2

    .line 13163
    :cond_1
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->delay_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->delay_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->access$16502(Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;I)I

    .line 13164
    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_2

    .line 13165
    or-int/lit8 v1, v2, 0x4

    .line 13167
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->message_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->message_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->access$16602(Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 13168
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->access$16702(Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;I)I

    .line 13169
    return-object v0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 13100
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 13100
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;
    .locals 1

    .prologue
    .line 13116
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 13117
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 13118
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    .line 13119
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->delay_:I

    .line 13120
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    .line 13121
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->message_:Ljava/lang/Object;

    .line 13122
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    .line 13123
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;
    .locals 1

    .prologue
    .line 13277
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 13279
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    .line 13280
    return-object p0
.end method

.method public clearDelay()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;
    .locals 1

    .prologue
    .line 13298
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    .line 13299
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->delay_:I

    .line 13301
    return-object p0
.end method

.method public clearMessage()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;
    .locals 1

    .prologue
    .line 13329
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    .line 13330
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->message_:Ljava/lang/Object;

    .line 13332
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 13100
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 13100
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 13100
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;
    .locals 2

    .prologue
    .line 13127
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 13100
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 13246
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 13100
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 13100
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;
    .locals 1

    .prologue
    .line 13131
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDelay()I
    .locals 1

    .prologue
    .line 13289
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->delay_:I

    return v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 13310
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->message_:Ljava/lang/Object;

    .line 13311
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 13312
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 13313
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->message_:Ljava/lang/Object;

    .line 13316
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 13243
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDelay()Z
    .locals 2

    .prologue
    .line 13286
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMessage()Z
    .locals 2

    .prologue
    .line 13307
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 13187
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 13195
    :goto_0
    return v0

    .line 13191
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 13193
    goto :goto_0

    .line 13195
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 13265
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 13267
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 13273
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    .line 13274
    return-object p0

    .line 13270
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13100
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 13100
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13100
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13203
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 13204
    sparse-switch v0, :sswitch_data_0

    .line 13209
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 13211
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 13207
    goto :goto_1

    .line 13216
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 13217
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 13218
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 13220
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 13221
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    goto :goto_0

    .line 13225
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    .line 13226
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->delay_:I

    goto :goto_0

    .line 13230
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    .line 13231
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->message_:Ljava/lang/Object;

    goto :goto_0

    .line 13204
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 13173
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 13183
    :goto_0
    return-object v0

    .line 13174
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 13175
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    .line 13177
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->hasDelay()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 13178
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->getDelay()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->setDelay(I)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    .line 13180
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->hasMessage()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 13181
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->setMessage(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    :cond_3
    move-object v0, p0

    .line 13183
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 13259
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 13261
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    .line 13262
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 13249
    if-nez p1, :cond_0

    .line 13250
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 13252
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 13254
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    .line 13255
    return-object p0
.end method

.method public setDelay(I)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 13292
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    .line 13293
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->delay_:I

    .line 13295
    return-object p0
.end method

.method public setMessage(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 13320
    if-nez p1, :cond_0

    .line 13321
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 13323
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    .line 13324
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->message_:Ljava/lang/Object;

    .line 13326
    return-object p0
.end method

.method setMessage(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 13335
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->bitField0_:I

    .line 13336
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->message_:Ljava/lang/Object;

    .line 13338
    return-void
.end method
