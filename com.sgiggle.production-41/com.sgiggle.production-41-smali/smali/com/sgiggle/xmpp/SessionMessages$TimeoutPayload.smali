.class public final Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TimeoutPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final DELAY_FIELD_NUMBER:I = 0x2

.field public static final MESSAGE_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private delay_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private message_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 13344
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;

    .line 13345
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->initFields()V

    .line 13346
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 12897
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 12968
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->memoizedIsInitialized:B

    .line 12999
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->memoizedSerializedSize:I

    .line 12898
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 12892
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 12899
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 12968
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->memoizedIsInitialized:B

    .line 12999
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->memoizedSerializedSize:I

    .line 12899
    return-void
.end method

.method static synthetic access$16402(Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 12892
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$16502(Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 12892
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->delay_:I

    return p1
.end method

.method static synthetic access$16602(Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 12892
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->message_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$16702(Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 12892
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;
    .locals 1

    .prologue
    .line 12903
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;

    return-object v0
.end method

.method private getMessageBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 12952
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->message_:Ljava/lang/Object;

    .line 12953
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 12954
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 12956
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->message_:Ljava/lang/Object;

    .line 12959
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 12964
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 12965
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->delay_:I

    .line 12966
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->message_:Ljava/lang/Object;

    .line 12967
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;
    .locals 1

    .prologue
    .line 13093
    #calls: Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->access$16200()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 13096
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13062
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    move-result-object v0

    .line 13063
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13064
    #calls: Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->access$16100(Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;

    move-result-object v0

    .line 13066
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13073
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    move-result-object v0

    .line 13074
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13075
    #calls: Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->access$16100(Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;

    move-result-object v0

    .line 13077
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 13029
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->access$16100(Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 13035
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->access$16100(Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13083
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->access$16100(Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13089
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->access$16100(Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13051
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->access$16100(Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13057
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->access$16100(Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 13040
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->access$16100(Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 13046
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;->access$16100(Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 12918
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 12892
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;
    .locals 1

    .prologue
    .line 12907
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;

    return-object v0
.end method

.method public getDelay()I
    .locals 1

    .prologue
    .line 12928
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->delay_:I

    return v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 12938
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->message_:Ljava/lang/Object;

    .line 12939
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 12940
    check-cast v0, Ljava/lang/String;

    .line 12948
    :goto_0
    return-object v0

    .line 12942
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 12944
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 12945
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 12946
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->message_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 12948
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 13001
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->memoizedSerializedSize:I

    .line 13002
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 13018
    :goto_0
    return v0

    .line 13004
    :cond_0
    const/4 v0, 0x0

    .line 13005
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 13006
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13009
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 13010
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->delay_:I

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 13013
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 13014
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->getMessageBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13017
    :cond_3
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 12915
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDelay()Z
    .locals 2

    .prologue
    .line 12925
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMessage()Z
    .locals 2

    .prologue
    .line 12935
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 12970
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->memoizedIsInitialized:B

    .line 12971
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 12982
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 12971
    goto :goto_0

    .line 12973
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 12974
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 12975
    goto :goto_0

    .line 12977
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 12978
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 12979
    goto :goto_0

    .line 12981
    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 12982
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 12892
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;
    .locals 1

    .prologue
    .line 13094
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 12892
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;
    .locals 1

    .prologue
    .line 13098
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;)Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 13023
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 12987
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->getSerializedSize()I

    .line 12988
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 12989
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 12991
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 12992
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->delay_:I

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 12994
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 12995
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$TimeoutPayload;->getMessageBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 12997
    :cond_2
    return-void
.end method
