.class public final Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$TriggerContactsOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TriggerContacts"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1913
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;

    .line 1914
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->initFields()V

    .line 1915
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 1609
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 1636
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->memoizedIsInitialized:B

    .line 1661
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->memoizedSerializedSize:I

    .line 1610
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1604
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;-><init>(Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 1611
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 1636
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->memoizedIsInitialized:B

    .line 1661
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->memoizedSerializedSize:I

    .line 1611
    return-void
.end method

.method static synthetic access$1002(Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1604
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->bitField0_:I

    return p1
.end method

.method static synthetic access$902(Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1604
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;
    .locals 1

    .prologue
    .line 1615
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 1634
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 1635
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;
    .locals 1

    .prologue
    .line 1747
    #calls: Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->access$700()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;)Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;
    .locals 1
    .parameter

    .prologue
    .line 1750
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;)Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1716
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;

    move-result-object v0

    .line 1717
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1718
    #calls: Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->access$600(Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;

    move-result-object v0

    .line 1720
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1727
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;

    move-result-object v0

    .line 1728
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1729
    #calls: Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->access$600(Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;

    move-result-object v0

    .line 1731
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 1683
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->access$600(Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 1689
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->access$600(Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1737
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->access$600(Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1743
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->access$600(Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1705
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->access$600(Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1711
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->access$600(Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 1694
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->access$600(Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 1700
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;->access$600(Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;)Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 1630
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1604
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;
    .locals 1

    .prologue
    .line 1619
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1663
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->memoizedSerializedSize:I

    .line 1664
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1672
    :goto_0
    return v0

    .line 1666
    :cond_0
    const/4 v0, 0x0

    .line 1667
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 1668
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1671
    :cond_1
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1627
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1638
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->memoizedIsInitialized:B

    .line 1639
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 1650
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 1639
    goto :goto_0

    .line 1641
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1642
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->memoizedIsInitialized:B

    move v0, v2

    .line 1643
    goto :goto_0

    .line 1645
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1646
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->memoizedIsInitialized:B

    move v0, v2

    .line 1647
    goto :goto_0

    .line 1649
    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->memoizedIsInitialized:B

    move v0, v3

    .line 1650
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1604
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;
    .locals 1

    .prologue
    .line 1748
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1604
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;
    .locals 1

    .prologue
    .line 1752
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;)Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 1677
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1655
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->getSerializedSize()I

    .line 1656
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1657
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$TriggerContacts;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 1659
    :cond_0
    return-void
.end method
