.class public final Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private alerts_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;"
        }
    .end annotation
.end field

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 37964
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 38095
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 38138
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->alerts_:Ljava/util/List;

    .line 37965
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->maybeForceBuilderInitialization()V

    .line 37966
    return-void
.end method

.method static synthetic access$48200(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 37959
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$48300()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;
    .locals 1

    .prologue
    .line 37959
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 38001
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;

    move-result-object v0

    .line 38002
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 38003
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 38006
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;
    .locals 1

    .prologue
    .line 37971
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureAlertsIsMutable()V
    .locals 2

    .prologue
    .line 38141
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 38142
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->alerts_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->alerts_:Ljava/util/List;

    .line 38143
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->bitField0_:I

    .line 38145
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 37969
    return-void
.end method


# virtual methods
.method public addAlerts(ILcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 38201
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->ensureAlertsIsMutable()V

    .line 38202
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->alerts_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 38204
    return-object p0
.end method

.method public addAlerts(ILcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 38184
    if-nez p2, :cond_0

    .line 38185
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 38187
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->ensureAlertsIsMutable()V

    .line 38188
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->alerts_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 38190
    return-object p0
.end method

.method public addAlerts(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 38194
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->ensureAlertsIsMutable()V

    .line 38195
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->alerts_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38197
    return-object p0
.end method

.method public addAlerts(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 38174
    if-nez p1, :cond_0

    .line 38175
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 38177
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->ensureAlertsIsMutable()V

    .line 38178
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->alerts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38180
    return-object p0
.end method

.method public addAllAlerts(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;"
        }
    .end annotation

    .prologue
    .line 38208
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->ensureAlertsIsMutable()V

    .line 38209
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->alerts_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 38211
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 37959
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;
    .locals 2

    .prologue
    .line 37992
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;

    move-result-object v0

    .line 37993
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 37994
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 37996
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 37959
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;
    .locals 4

    .prologue
    .line 38010
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 38011
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->bitField0_:I

    .line 38012
    const/4 v2, 0x0

    .line 38013
    and-int/lit8 v1, v1, 0x1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_1

    .line 38014
    or-int/lit8 v1, v2, 0x1

    .line 38016
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->access$48502(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 38017
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 38018
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->alerts_:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->alerts_:Ljava/util/List;

    .line 38019
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->bitField0_:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->bitField0_:I

    .line 38021
    :cond_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->alerts_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->alerts_:Ljava/util/List;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->access$48602(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;Ljava/util/List;)Ljava/util/List;

    .line 38022
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->access$48702(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;I)I

    .line 38023
    return-object v0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 37959
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 37959
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;
    .locals 1

    .prologue
    .line 37975
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 37976
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 37977
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->bitField0_:I

    .line 37978
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->alerts_:Ljava/util/List;

    .line 37979
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->bitField0_:I

    .line 37980
    return-object p0
.end method

.method public clearAlerts()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;
    .locals 1

    .prologue
    .line 38214
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->alerts_:Ljava/util/List;

    .line 38215
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->bitField0_:I

    .line 38217
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;
    .locals 1

    .prologue
    .line 38131
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 38133
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->bitField0_:I

    .line 38134
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 37959
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 37959
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 37959
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;
    .locals 2

    .prologue
    .line 37984
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 37959
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAlerts(I)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 1
    .parameter

    .prologue
    .line 38154
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->alerts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    return-object v0
.end method

.method public getAlertsCount()I
    .locals 1

    .prologue
    .line 38151
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->alerts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getAlertsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38148
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->alerts_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 38100
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 37959
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 37959
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;
    .locals 1

    .prologue
    .line 37988
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;

    move-result-object v0

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 38097
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38045
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 38053
    :goto_0
    return v0

    .line 38049
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 38051
    goto :goto_0

    .line 38053
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 38119
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 38121
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 38127
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->bitField0_:I

    .line 38128
    return-object p0

    .line 38124
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37959
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 37959
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37959
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38061
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 38062
    sparse-switch v0, :sswitch_data_0

    .line 38067
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 38069
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 38065
    goto :goto_1

    .line 38074
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 38075
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 38076
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 38078
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 38079
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    goto :goto_0

    .line 38083
    :sswitch_2
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    .line 38084
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 38085
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->addAlerts(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    goto :goto_0

    .line 38062
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 38027
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 38041
    :goto_0
    return-object v0

    .line 38028
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 38029
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    .line 38031
    :cond_1
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->alerts_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->access$48600(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 38032
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->alerts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 38033
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->alerts_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->access$48600(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->alerts_:Ljava/util/List;

    .line 38034
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->bitField0_:I

    :cond_2
    :goto_1
    move-object v0, p0

    .line 38041
    goto :goto_0

    .line 38036
    :cond_3
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->ensureAlertsIsMutable()V

    .line 38037
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->alerts_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->alerts_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->access$48600(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public removeAlerts(I)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 38220
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->ensureAlertsIsMutable()V

    .line 38221
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->alerts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 38223
    return-object p0
.end method

.method public setAlerts(ILcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 38168
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->ensureAlertsIsMutable()V

    .line 38169
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->alerts_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 38171
    return-object p0
.end method

.method public setAlerts(ILcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 38158
    if-nez p2, :cond_0

    .line 38159
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 38161
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->ensureAlertsIsMutable()V

    .line 38162
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->alerts_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 38164
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 38113
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 38115
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->bitField0_:I

    .line 38116
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 38103
    if-nez p1, :cond_0

    .line 38104
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 38106
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 38108
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->bitField0_:I

    .line 38109
    return-object p0
.end method
