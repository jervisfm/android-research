.class public final Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UpdateAlertsPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;
    }
.end annotation


# static fields
.field public static final ALERTS_FIELD_NUMBER:I = 0x2

.field public static final BASE_FIELD_NUMBER:I = 0x1

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;


# instance fields
.field private alerts_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;"
        }
    .end annotation
.end field

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38230
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;

    .line 38231
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->initFields()V

    .line 38232
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 37785
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 37834
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->memoizedIsInitialized:B

    .line 37862
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->memoizedSerializedSize:I

    .line 37786
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 37780
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 37787
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 37834
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->memoizedIsInitialized:B

    .line 37862
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->memoizedSerializedSize:I

    .line 37787
    return-void
.end method

.method static synthetic access$48502(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 37780
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$48600(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 37780
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->alerts_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$48602(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 37780
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->alerts_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$48702(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 37780
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;
    .locals 1

    .prologue
    .line 37791
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 37831
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 37832
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->alerts_:Ljava/util/List;

    .line 37833
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;
    .locals 1

    .prologue
    .line 37952
    #calls: Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->access$48300()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 37955
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37921
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    move-result-object v0

    .line 37922
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 37923
    #calls: Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->access$48200(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;

    move-result-object v0

    .line 37925
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37932
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    move-result-object v0

    .line 37933
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 37934
    #calls: Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->access$48200(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;

    move-result-object v0

    .line 37936
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 37888
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->access$48200(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 37894
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->access$48200(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37942
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->access$48200(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37948
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->access$48200(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37910
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->access$48200(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37916
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->access$48200(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 37899
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->access$48200(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 37905
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;->access$48200(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAlerts(I)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 1
    .parameter

    .prologue
    .line 37823
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->alerts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    return-object v0
.end method

.method public getAlertsCount()I
    .locals 1

    .prologue
    .line 37820
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->alerts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getAlertsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37813
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->alerts_:Ljava/util/List;

    return-object v0
.end method

.method public getAlertsOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlertOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 37827
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->alerts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlertOrBuilder;

    return-object v0
.end method

.method public getAlertsOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlertOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37817
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->alerts_:Ljava/util/List;

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 37806
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 37780
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;
    .locals 1

    .prologue
    .line 37795
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 37864
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->memoizedSerializedSize:I

    .line 37865
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 37877
    :goto_0
    return v0

    .line 37868
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 37869
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    :goto_1
    move v1, v2

    move v2, v0

    .line 37872
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->alerts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 37873
    const/4 v3, 0x2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->alerts_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 37872
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 37876
    :cond_1
    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->memoizedSerializedSize:I

    move v0, v2

    .line 37877
    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 37803
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 37836
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->memoizedIsInitialized:B

    .line 37837
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 37848
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 37837
    goto :goto_0

    .line 37839
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 37840
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 37841
    goto :goto_0

    .line 37843
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 37844
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 37845
    goto :goto_0

    .line 37847
    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 37848
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 37780
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;
    .locals 1

    .prologue
    .line 37953
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 37780
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;
    .locals 1

    .prologue
    .line 37957
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;)Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 37882
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 37853
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->getSerializedSize()I

    .line 37854
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 37855
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 37857
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->alerts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 37858
    const/4 v2, 0x2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->alerts_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 37857
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 37860
    :cond_1
    return-void
.end method
