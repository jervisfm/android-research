.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "UpdateAlertsPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getAlerts(I)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
.end method

.method public abstract getAlertsCount()I
.end method

.method public abstract getAlertsList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract hasBase()Z
.end method
