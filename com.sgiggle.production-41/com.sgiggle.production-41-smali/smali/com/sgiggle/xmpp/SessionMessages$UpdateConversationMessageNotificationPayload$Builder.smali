.class public final Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private conversationId_:Ljava/lang/Object;

.field private firstMessageSendErrorNotification_:Z

.field private lastUnreadMsgStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

.field private message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

.field private playAlertSound_:Z

.field private unreadConversationCount_:I

.field private unreadMessageCount_:I

.field private updateNotification_:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->conversationId_:Ljava/lang/Object;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_SENT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->lastUnreadMsgStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->maybeForceBuilderInitialization()V

    return-void
.end method

.method static synthetic access$135400(Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$135500()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;
    .locals 1

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;
    .locals 2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;
    .locals 5

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    or-int/lit8 v2, v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->access$135702(Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->playAlertSound_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->playAlertSound_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->access$135802(Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;Z)Z

    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->unreadConversationCount_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->unreadConversationCount_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->access$135902(Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;I)I

    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    or-int/lit8 v2, v2, 0x8

    :cond_3
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->updateNotification_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->updateNotification_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->access$136002(Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;Z)Z

    and-int/lit8 v3, v1, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    or-int/lit8 v2, v2, 0x10

    :cond_4
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->conversationId_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->conversationId_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->access$136102(Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v1, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    or-int/lit8 v2, v2, 0x20

    :cond_5
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->unreadMessageCount_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->unreadMessageCount_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->access$136202(Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;I)I

    and-int/lit8 v3, v1, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    or-int/lit8 v2, v2, 0x40

    :cond_6
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->access$136302(Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    and-int/lit16 v3, v1, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    or-int/lit16 v2, v2, 0x80

    :cond_7
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->lastUnreadMsgStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->lastUnreadMsgStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->access$136402(Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    and-int/lit16 v1, v1, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_8

    or-int/lit16 v1, v2, 0x100

    :goto_0
    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->firstMessageSendErrorNotification_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->firstMessageSendErrorNotification_:Z
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->access$136502(Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;Z)Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->access$136602(Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;I)I

    return-object v0

    :cond_8
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->playAlertSound_:Z

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->unreadConversationCount_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->updateNotification_:Z

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->conversationId_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->unreadMessageCount_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_SENT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->lastUnreadMsgStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->firstMessageSendErrorNotification_:Z

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearConversationId()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->getConversationId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->conversationId_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearFirstMessageSendErrorNotification()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->firstMessageSendErrorNotification_:Z

    return-object p0
.end method

.method public clearLastUnreadMsgStatus()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_SENT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->lastUnreadMsgStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    return-object p0
.end method

.method public clearMessage()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearPlayAlertSound()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->playAlertSound_:Z

    return-object p0
.end method

.method public clearUnreadConversationCount()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->unreadConversationCount_:I

    return-object p0
.end method

.method public clearUnreadMessageCount()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->unreadMessageCount_:I

    return-object p0
.end method

.method public clearUpdateNotification()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->updateNotification_:Z

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getConversationId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->conversationId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->conversationId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;

    move-result-object v0

    return-object v0
.end method

.method public getFirstMessageSendErrorNotification()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->firstMessageSendErrorNotification_:Z

    return v0
.end method

.method public getLastUnreadMsgStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->lastUnreadMsgStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    return-object v0
.end method

.method public getMessage()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    return-object v0
.end method

.method public getPlayAlertSound()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->playAlertSound_:Z

    return v0
.end method

.method public getUnreadConversationCount()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->unreadConversationCount_:I

    return v0
.end method

.method public getUnreadMessageCount()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->unreadMessageCount_:I

    return v0
.end method

.method public getUpdateNotification()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->updateNotification_:Z

    return v0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasConversationId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFirstMessageSendErrorNotification()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLastUnreadMsgStatus()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMessage()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPlayAlertSound()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUnreadConversationCount()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUnreadMessageCount()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUpdateNotification()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->hasPlayAlertSound()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->hasUnreadConversationCount()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->hasUpdateNotification()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->hasMessage()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->getMessage()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    goto :goto_1

    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;

    goto :goto_0

    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->playAlertSound_:Z

    goto :goto_0

    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->unreadConversationCount_:I

    goto :goto_0

    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->updateNotification_:Z

    goto :goto_0

    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->conversationId_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->unreadMessageCount_:I

    goto :goto_0

    :sswitch_7
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->hasMessage()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->getMessage()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_2
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->setMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->lastUnreadMsgStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    goto/16 :goto_0

    :sswitch_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->firstMessageSendErrorNotification_:Z

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x58 -> :sswitch_9
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;

    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->hasPlayAlertSound()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->getPlayAlertSound()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->setPlayAlertSound(Z)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->hasUnreadConversationCount()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->getUnreadConversationCount()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->setUnreadConversationCount(I)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->hasUpdateNotification()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->getUpdateNotification()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->setUpdateNotification(Z)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->hasConversationId()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->getConversationId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->setConversationId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;

    :cond_5
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->hasUnreadMessageCount()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->getUnreadMessageCount()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->setUnreadMessageCount(I)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;

    :cond_6
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->hasMessage()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->getMessage()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->mergeMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;

    :cond_7
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->hasLastUnreadMsgStatus()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->getLastUnreadMsgStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->setLastUnreadMsgStatus(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;

    :cond_8
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->hasFirstMessageSendErrorNotification()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->getFirstMessageSendErrorNotification()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->setFirstMessageSendErrorNotification(Z)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;

    :cond_9
    move-object v0, p0

    goto :goto_0
.end method

.method public mergeMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setConversationId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->conversationId_:Ljava/lang/Object;

    return-object p0
.end method

.method setConversationId(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->conversationId_:Ljava/lang/Object;

    return-void
.end method

.method public setFirstMessageSendErrorNotification(Z)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->firstMessageSendErrorNotification_:Z

    return-object p0
.end method

.method public setLastUnreadMsgStatus(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->lastUnreadMsgStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    return-object p0
.end method

.method public setMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setPlayAlertSound(Z)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->playAlertSound_:Z

    return-object p0
.end method

.method public setUnreadConversationCount(I)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->unreadConversationCount_:I

    return-object p0
.end method

.method public setUnreadMessageCount(I)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->unreadMessageCount_:I

    return-object p0
.end method

.method public setUpdateNotification(Z)Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload$Builder;->updateNotification_:Z

    return-object p0
.end method
