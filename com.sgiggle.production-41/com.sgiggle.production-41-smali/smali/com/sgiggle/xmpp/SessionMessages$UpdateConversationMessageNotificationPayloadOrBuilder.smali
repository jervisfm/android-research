.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "UpdateConversationMessageNotificationPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getConversationId()Ljava/lang/String;
.end method

.method public abstract getFirstMessageSendErrorNotification()Z
.end method

.method public abstract getLastUnreadMsgStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;
.end method

.method public abstract getMessage()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
.end method

.method public abstract getPlayAlertSound()Z
.end method

.method public abstract getUnreadConversationCount()I
.end method

.method public abstract getUnreadMessageCount()I
.end method

.method public abstract getUpdateNotification()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasConversationId()Z
.end method

.method public abstract hasFirstMessageSendErrorNotification()Z
.end method

.method public abstract hasLastUnreadMsgStatus()Z
.end method

.method public abstract hasMessage()Z
.end method

.method public abstract hasPlayAlertSound()Z
.end method

.method public abstract hasUnreadConversationCount()Z
.end method

.method public abstract hasUnreadMessageCount()Z
.end method

.method public abstract hasUpdateNotification()Z
.end method
