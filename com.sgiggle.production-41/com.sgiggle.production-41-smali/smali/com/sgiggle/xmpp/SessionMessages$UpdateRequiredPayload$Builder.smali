.class public final Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private action_:Ljava/lang/Object;

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private message_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 34111
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 34255
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 34298
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->message_:Ljava/lang/Object;

    .line 34334
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->action_:Ljava/lang/Object;

    .line 34112
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->maybeForceBuilderInitialization()V

    .line 34113
    return-void
.end method

.method static synthetic access$43000(Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 34106
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$43100()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;
    .locals 1

    .prologue
    .line 34106
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 34150
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    move-result-object v0

    .line 34151
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 34152
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 34155
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;
    .locals 1

    .prologue
    .line 34118
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 34116
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 34106
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;
    .locals 2

    .prologue
    .line 34141
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    move-result-object v0

    .line 34142
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 34143
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 34145
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 34106
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;
    .locals 5

    .prologue
    .line 34159
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 34160
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    .line 34161
    const/4 v2, 0x0

    .line 34162
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 34163
    or-int/lit8 v2, v2, 0x1

    .line 34165
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->access$43302(Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 34166
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 34167
    or-int/lit8 v2, v2, 0x2

    .line 34169
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->message_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->message_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->access$43402(Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34170
    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_2

    .line 34171
    or-int/lit8 v1, v2, 0x4

    .line 34173
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->action_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->action_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->access$43502(Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34174
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->access$43602(Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;I)I

    .line 34175
    return-object v0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 34106
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 34106
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;
    .locals 1

    .prologue
    .line 34122
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 34123
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 34124
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    .line 34125
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->message_:Ljava/lang/Object;

    .line 34126
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    .line 34127
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->action_:Ljava/lang/Object;

    .line 34128
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    .line 34129
    return-object p0
.end method

.method public clearAction()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;
    .locals 1

    .prologue
    .line 34358
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    .line 34359
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->getAction()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->action_:Ljava/lang/Object;

    .line 34361
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;
    .locals 1

    .prologue
    .line 34291
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 34293
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    .line 34294
    return-object p0
.end method

.method public clearMessage()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;
    .locals 1

    .prologue
    .line 34322
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    .line 34323
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->message_:Ljava/lang/Object;

    .line 34325
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 34106
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 34106
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 34106
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;
    .locals 2

    .prologue
    .line 34133
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 34106
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAction()Ljava/lang/String;
    .locals 2

    .prologue
    .line 34339
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->action_:Ljava/lang/Object;

    .line 34340
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 34341
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 34342
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->action_:Ljava/lang/Object;

    .line 34345
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 34260
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 34106
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 34106
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;
    .locals 1

    .prologue
    .line 34137
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 34303
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->message_:Ljava/lang/Object;

    .line 34304
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 34305
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 34306
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->message_:Ljava/lang/Object;

    .line 34309
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasAction()Z
    .locals 2

    .prologue
    .line 34336
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 34257
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMessage()Z
    .locals 2

    .prologue
    .line 34300
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 34193
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 34209
    :goto_0
    return v0

    .line 34197
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->hasMessage()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 34199
    goto :goto_0

    .line 34201
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->hasAction()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 34203
    goto :goto_0

    .line 34205
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 34207
    goto :goto_0

    .line 34209
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 34279
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 34281
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 34287
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    .line 34288
    return-object p0

    .line 34284
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 34106
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 34106
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 34106
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 34217
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 34218
    sparse-switch v0, :sswitch_data_0

    .line 34223
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 34225
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 34221
    goto :goto_1

    .line 34230
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 34231
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 34232
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 34234
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 34235
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    goto :goto_0

    .line 34239
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    .line 34240
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->message_:Ljava/lang/Object;

    goto :goto_0

    .line 34244
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    .line 34245
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->action_:Ljava/lang/Object;

    goto :goto_0

    .line 34218
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 34179
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 34189
    :goto_0
    return-object v0

    .line 34180
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 34181
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    .line 34183
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->hasMessage()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 34184
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->setMessage(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    .line 34186
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->hasAction()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 34187
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->setAction(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    :cond_3
    move-object v0, p0

    .line 34189
    goto :goto_0
.end method

.method public setAction(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 34349
    if-nez p1, :cond_0

    .line 34350
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 34352
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    .line 34353
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->action_:Ljava/lang/Object;

    .line 34355
    return-object p0
.end method

.method setAction(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 34364
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    .line 34365
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->action_:Ljava/lang/Object;

    .line 34367
    return-void
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 34273
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 34275
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    .line 34276
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 34263
    if-nez p1, :cond_0

    .line 34264
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 34266
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 34268
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    .line 34269
    return-object p0
.end method

.method public setMessage(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 34313
    if-nez p1, :cond_0

    .line 34314
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 34316
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    .line 34317
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->message_:Ljava/lang/Object;

    .line 34319
    return-object p0
.end method

.method setMessage(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 34328
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->bitField0_:I

    .line 34329
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->message_:Ljava/lang/Object;

    .line 34331
    return-void
.end method
