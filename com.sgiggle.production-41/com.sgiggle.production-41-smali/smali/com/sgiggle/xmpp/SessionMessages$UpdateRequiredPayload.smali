.class public final Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UpdateRequiredPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;
    }
.end annotation


# static fields
.field public static final ACTION_FIELD_NUMBER:I = 0x3

.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final MESSAGE_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;


# instance fields
.field private action_:Ljava/lang/Object;

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private message_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34373
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    .line 34374
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->initFields()V

    .line 34375
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 33873
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 33966
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->memoizedIsInitialized:B

    .line 34005
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->memoizedSerializedSize:I

    .line 33874
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 33868
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 33875
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 33966
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->memoizedIsInitialized:B

    .line 34005
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->memoizedSerializedSize:I

    .line 33875
    return-void
.end method

.method static synthetic access$43302(Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 33868
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$43402(Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 33868
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->message_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$43502(Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 33868
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->action_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$43602(Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 33868
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->bitField0_:I

    return p1
.end method

.method private getActionBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 33950
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->action_:Ljava/lang/Object;

    .line 33951
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 33952
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 33954
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->action_:Ljava/lang/Object;

    .line 33957
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;
    .locals 1

    .prologue
    .line 33879
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    return-object v0
.end method

.method private getMessageBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 33918
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->message_:Ljava/lang/Object;

    .line 33919
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 33920
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 33922
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->message_:Ljava/lang/Object;

    .line 33925
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 33962
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 33963
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->message_:Ljava/lang/Object;

    .line 33964
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->action_:Ljava/lang/Object;

    .line 33965
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;
    .locals 1

    .prologue
    .line 34099
    #calls: Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->access$43100()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 34102
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 34068
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    move-result-object v0

    .line 34069
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 34070
    #calls: Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->access$43000(Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    move-result-object v0

    .line 34072
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 34079
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    move-result-object v0

    .line 34080
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 34081
    #calls: Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->access$43000(Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    move-result-object v0

    .line 34083
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 34035
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->access$43000(Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 34041
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->access$43000(Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 34089
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->access$43000(Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 34095
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->access$43000(Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 34057
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->access$43000(Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 34063
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->access$43000(Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 34046
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->access$43000(Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 34052
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;->access$43000(Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAction()Ljava/lang/String;
    .locals 2

    .prologue
    .line 33936
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->action_:Ljava/lang/Object;

    .line 33937
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 33938
    check-cast v0, Ljava/lang/String;

    .line 33946
    :goto_0
    return-object v0

    .line 33940
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 33942
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 33943
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 33944
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->action_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 33946
    goto :goto_0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 33894
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 33868
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;
    .locals 1

    .prologue
    .line 33883
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 33904
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->message_:Ljava/lang/Object;

    .line 33905
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 33906
    check-cast v0, Ljava/lang/String;

    .line 33914
    :goto_0
    return-object v0

    .line 33908
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 33910
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 33911
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 33912
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->message_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 33914
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 34007
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->memoizedSerializedSize:I

    .line 34008
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 34024
    :goto_0
    return v0

    .line 34010
    :cond_0
    const/4 v0, 0x0

    .line 34011
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 34012
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 34015
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 34016
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->getMessageBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 34019
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 34020
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->getActionBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 34023
    :cond_3
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public hasAction()Z
    .locals 2

    .prologue
    .line 33933
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 33891
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMessage()Z
    .locals 2

    .prologue
    .line 33901
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 33968
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->memoizedIsInitialized:B

    .line 33969
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 33988
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 33969
    goto :goto_0

    .line 33971
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 33972
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 33973
    goto :goto_0

    .line 33975
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->hasMessage()Z

    move-result v0

    if-nez v0, :cond_3

    .line 33976
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 33977
    goto :goto_0

    .line 33979
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->hasAction()Z

    move-result v0

    if-nez v0, :cond_4

    .line 33980
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 33981
    goto :goto_0

    .line 33983
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_5

    .line 33984
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 33985
    goto :goto_0

    .line 33987
    :cond_5
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 33988
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 33868
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;
    .locals 1

    .prologue
    .line 34100
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 33868
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;
    .locals 1

    .prologue
    .line 34104
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;)Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 34029
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 33993
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->getSerializedSize()I

    .line 33994
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 33995
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 33997
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 33998
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->getMessageBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 34000
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 34001
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->getActionBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 34003
    :cond_2
    return-void
.end method
