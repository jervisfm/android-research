.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "UpdateRequiredPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getAction()Ljava/lang/String;
.end method

.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getMessage()Ljava/lang/String;
.end method

.method public abstract hasAction()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasMessage()Z
.end method
