.class public final Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private attributes_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;",
            ">;"
        }
    .end annotation
.end field

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private callees_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private content_:Ljava/lang/Object;

.field private duration_:I

.field private flip_:Z

.field private mime_:Ljava/lang/Object;

.field private rotation_:I

.field private size_:J

.field private timeCreated_:J


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 60833
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 61117
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 61160
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->mime_:Ljava/lang/Object;

    .line 61259
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->callees_:Ljava/util/List;

    .line 61348
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->content_:Ljava/lang/Object;

    .line 61426
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->attributes_:Ljava/util/List;

    .line 60834
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->maybeForceBuilderInitialization()V

    .line 60835
    return-void
.end method

.method static synthetic access$78100(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 60828
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$78200()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 60828
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 60886
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    move-result-object v0

    .line 60887
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 60888
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 60891
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 60840
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureAttributesIsMutable()V
    .locals 2

    .prologue
    .line 61429
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-eq v0, v1, :cond_0

    .line 61430
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->attributes_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->attributes_:Ljava/util/List;

    .line 61431
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61433
    :cond_0
    return-void
.end method

.method private ensureCalleesIsMutable()V
    .locals 2

    .prologue
    .line 61262
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    .line 61263
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->callees_:Ljava/util/List;

    .line 61264
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61266
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 60838
    return-void
.end method


# virtual methods
.method public addAllAttributes(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;"
        }
    .end annotation

    .prologue
    .line 61496
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->ensureAttributesIsMutable()V

    .line 61497
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->attributes_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 61499
    return-object p0
.end method

.method public addAllCallees(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;"
        }
    .end annotation

    .prologue
    .line 61329
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->ensureCalleesIsMutable()V

    .line 61330
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 61332
    return-object p0
.end method

.method public addAttributes(ILcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 61489
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->ensureAttributesIsMutable()V

    .line 61490
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->attributes_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 61492
    return-object p0
.end method

.method public addAttributes(ILcom/sgiggle/xmpp/SessionMessages$KeyValuePair;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 61472
    if-nez p2, :cond_0

    .line 61473
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 61475
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->ensureAttributesIsMutable()V

    .line 61476
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->attributes_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 61478
    return-object p0
.end method

.method public addAttributes(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 61482
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->ensureAttributesIsMutable()V

    .line 61483
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->attributes_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61485
    return-object p0
.end method

.method public addAttributes(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 61462
    if-nez p1, :cond_0

    .line 61463
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 61465
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->ensureAttributesIsMutable()V

    .line 61466
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->attributes_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61468
    return-object p0
.end method

.method public addCallees(ILcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 61322
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->ensureCalleesIsMutable()V

    .line 61323
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 61325
    return-object p0
.end method

.method public addCallees(ILcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 61305
    if-nez p2, :cond_0

    .line 61306
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 61308
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->ensureCalleesIsMutable()V

    .line 61309
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 61311
    return-object p0
.end method

.method public addCallees(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 61315
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->ensureCalleesIsMutable()V

    .line 61316
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61318
    return-object p0
.end method

.method public addCallees(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 61295
    if-nez p1, :cond_0

    .line 61296
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 61298
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->ensureCalleesIsMutable()V

    .line 61299
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61301
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 60828
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;
    .locals 2

    .prologue
    .line 60877
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    move-result-object v0

    .line 60878
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 60879
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 60881
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 60828
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;
    .locals 5

    .prologue
    .line 60895
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 60896
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 60897
    const/4 v2, 0x0

    .line 60898
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 60899
    or-int/lit8 v2, v2, 0x1

    .line 60901
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->access$78402(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 60902
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 60903
    or-int/lit8 v2, v2, 0x2

    .line 60905
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->mime_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->mime_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->access$78502(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60906
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 60907
    or-int/lit8 v2, v2, 0x4

    .line 60909
    :cond_2
    iget-wide v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->size_:J

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->size_:J
    invoke-static {v0, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->access$78602(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;J)J

    .line 60910
    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 60911
    or-int/lit8 v2, v2, 0x8

    .line 60913
    :cond_3
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->duration_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->duration_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->access$78702(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;I)I

    .line 60914
    and-int/lit8 v3, v1, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 60915
    or-int/lit8 v2, v2, 0x10

    .line 60917
    :cond_4
    iget-wide v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->timeCreated_:J

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->timeCreated_:J
    invoke-static {v0, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->access$78802(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;J)J

    .line 60918
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 60919
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->callees_:Ljava/util/List;

    .line 60920
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x21

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 60922
    :cond_5
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->callees_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->callees_:Ljava/util/List;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->access$78902(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;Ljava/util/List;)Ljava/util/List;

    .line 60923
    and-int/lit8 v3, v1, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    .line 60924
    or-int/lit8 v2, v2, 0x20

    .line 60926
    :cond_6
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->content_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->content_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->access$79002(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60927
    and-int/lit16 v3, v1, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    .line 60928
    or-int/lit8 v2, v2, 0x40

    .line 60930
    :cond_7
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->rotation_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->rotation_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->access$79102(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;I)I

    .line 60931
    and-int/lit16 v1, v1, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_9

    .line 60932
    or-int/lit16 v1, v2, 0x80

    .line 60934
    :goto_0
    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->flip_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->flip_:Z
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->access$79202(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;Z)Z

    .line 60935
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_8

    .line 60936
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->attributes_:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->attributes_:Ljava/util/List;

    .line 60937
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit16 v2, v2, -0x201

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 60939
    :cond_8
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->attributes_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->attributes_:Ljava/util/List;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->access$79302(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;Ljava/util/List;)Ljava/util/List;

    .line 60940
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->access$79402(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;I)I

    .line 60941
    return-object v0

    :cond_9
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 60828
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 60828
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 60844
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 60845
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 60846
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 60847
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->mime_:Ljava/lang/Object;

    .line 60848
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 60849
    iput-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->size_:J

    .line 60850
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 60851
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->duration_:I

    .line 60852
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 60853
    iput-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->timeCreated_:J

    .line 60854
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 60855
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->callees_:Ljava/util/List;

    .line 60856
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 60857
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->content_:Ljava/lang/Object;

    .line 60858
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 60859
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->rotation_:I

    .line 60860
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 60861
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->flip_:Z

    .line 60862
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 60863
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->attributes_:Ljava/util/List;

    .line 60864
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 60865
    return-object p0
.end method

.method public clearAttributes()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 61502
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->attributes_:Ljava/util/List;

    .line 61503
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61505
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 61153
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 61155
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61156
    return-object p0
.end method

.method public clearCallees()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 61335
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->callees_:Ljava/util/List;

    .line 61336
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61338
    return-object p0
.end method

.method public clearContent()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 61372
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61373
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->getContent()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->content_:Ljava/lang/Object;

    .line 61375
    return-object p0
.end method

.method public clearDuration()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 61231
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61232
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->duration_:I

    .line 61234
    return-object p0
.end method

.method public clearFlip()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 61419
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61420
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->flip_:Z

    .line 61422
    return-object p0
.end method

.method public clearMime()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 61184
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61185
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->getMime()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->mime_:Ljava/lang/Object;

    .line 61187
    return-object p0
.end method

.method public clearRotation()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 61398
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61399
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->rotation_:I

    .line 61401
    return-object p0
.end method

.method public clearSize()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 2

    .prologue
    .line 61210
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61211
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->size_:J

    .line 61213
    return-object p0
.end method

.method public clearTimeCreated()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 2

    .prologue
    .line 61252
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61253
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->timeCreated_:J

    .line 61255
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 60828
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 60828
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 60828
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 2

    .prologue
    .line 60869
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 60828
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAttributes(I)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    .locals 1
    .parameter

    .prologue
    .line 61442
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->attributes_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    return-object v0
.end method

.method public getAttributesCount()I
    .locals 1

    .prologue
    .line 61439
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->attributes_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getAttributesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61436
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->attributes_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 61122
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCallees(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter

    .prologue
    .line 61275
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getCalleesCount()I
    .locals 1

    .prologue
    .line 61272
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCalleesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61269
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getContent()Ljava/lang/String;
    .locals 2

    .prologue
    .line 61353
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->content_:Ljava/lang/Object;

    .line 61354
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 61355
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 61356
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->content_:Ljava/lang/Object;

    .line 61359
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 60828
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 60828
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;
    .locals 1

    .prologue
    .line 60873
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDuration()I
    .locals 1

    .prologue
    .line 61222
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->duration_:I

    return v0
.end method

.method public getFlip()Z
    .locals 1

    .prologue
    .line 61410
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->flip_:Z

    return v0
.end method

.method public getMime()Ljava/lang/String;
    .locals 2

    .prologue
    .line 61165
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->mime_:Ljava/lang/Object;

    .line 61166
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 61167
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 61168
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->mime_:Ljava/lang/Object;

    .line 61171
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getRotation()I
    .locals 1

    .prologue
    .line 61389
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->rotation_:I

    return v0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 61201
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->size_:J

    return-wide v0
.end method

.method public getTimeCreated()J
    .locals 2

    .prologue
    .line 61243
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->timeCreated_:J

    return-wide v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 61119
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasContent()Z
    .locals 2

    .prologue
    .line 61350
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDuration()Z
    .locals 2

    .prologue
    .line 61219
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFlip()Z
    .locals 2

    .prologue
    .line 61407
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMime()Z
    .locals 2

    .prologue
    .line 61162
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRotation()Z
    .locals 2

    .prologue
    .line 61386
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSize()Z
    .locals 2

    .prologue
    .line 61198
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTimeCreated()Z
    .locals 2

    .prologue
    .line 61240
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 60994
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 61034
    :goto_0
    return v0

    .line 60998
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->hasMime()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 61000
    goto :goto_0

    .line 61002
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->hasSize()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    .line 61004
    goto :goto_0

    .line 61006
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->hasDuration()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    .line 61008
    goto :goto_0

    .line 61010
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->hasTimeCreated()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v2

    .line 61012
    goto :goto_0

    .line 61014
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->hasContent()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v2

    .line 61016
    goto :goto_0

    .line 61018
    :cond_5
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v2

    .line 61020
    goto :goto_0

    :cond_6
    move v0, v2

    .line 61022
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->getCalleesCount()I

    move-result v1

    if-ge v0, v1, :cond_8

    .line 61023
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->getCallees(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_7

    move v0, v2

    .line 61025
    goto :goto_0

    .line 61022
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_8
    move v0, v2

    .line 61028
    :goto_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->getAttributesCount()I

    move-result v1

    if-ge v0, v1, :cond_a

    .line 61029
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->getAttributes(I)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_9

    move v0, v2

    .line 61031
    goto :goto_0

    .line 61028
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 61034
    :cond_a
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 61141
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 61143
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 61149
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61150
    return-object p0

    .line 61146
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60828
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 60828
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60828
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61042
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 61043
    sparse-switch v0, :sswitch_data_0

    .line 61048
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 61050
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 61046
    goto :goto_1

    .line 61055
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 61056
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 61057
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 61059
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 61060
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    goto :goto_0

    .line 61064
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61065
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->mime_:Ljava/lang/Object;

    goto :goto_0

    .line 61069
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61070
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->size_:J

    goto :goto_0

    .line 61074
    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61075
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->duration_:I

    goto :goto_0

    .line 61079
    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61080
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->timeCreated_:J

    goto :goto_0

    .line 61084
    :sswitch_6
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    .line 61085
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 61086
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->addCallees(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    goto :goto_0

    .line 61090
    :sswitch_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61091
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->content_:Ljava/lang/Object;

    goto :goto_0

    .line 61095
    :sswitch_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61096
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->rotation_:I

    goto/16 :goto_0

    .line 61100
    :sswitch_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61101
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->flip_:Z

    goto/16 :goto_0

    .line 61105
    :sswitch_a
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    .line 61106
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 61107
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->addAttributes(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    goto/16 :goto_0

    .line 61043
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 60945
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 60990
    :goto_0
    return-object v0

    .line 60946
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60947
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    .line 60949
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->hasMime()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 60950
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->getMime()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->setMime(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    .line 60952
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->hasSize()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 60953
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->getSize()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->setSize(J)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    .line 60955
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->hasDuration()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 60956
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->getDuration()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->setDuration(I)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    .line 60958
    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->hasTimeCreated()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 60959
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->getTimeCreated()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->setTimeCreated(J)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    .line 60961
    :cond_5
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->callees_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->access$78900(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 60962
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 60963
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->callees_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->access$78900(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->callees_:Ljava/util/List;

    .line 60964
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 60971
    :cond_6
    :goto_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->hasContent()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 60972
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->getContent()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->setContent(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    .line 60974
    :cond_7
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->hasRotation()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 60975
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->getRotation()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->setRotation(I)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    .line 60977
    :cond_8
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->hasFlip()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 60978
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->getFlip()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->setFlip(Z)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    .line 60980
    :cond_9
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->attributes_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->access$79300(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 60981
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->attributes_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 60982
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->attributes_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->access$79300(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->attributes_:Ljava/util/List;

    .line 60983
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    :cond_a
    :goto_2
    move-object v0, p0

    .line 60990
    goto/16 :goto_0

    .line 60966
    :cond_b
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->ensureCalleesIsMutable()V

    .line 60967
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->callees_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->callees_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->access$78900(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 60985
    :cond_c
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->ensureAttributesIsMutable()V

    .line 60986
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->attributes_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->attributes_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->access$79300(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public removeAttributes(I)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 61508
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->ensureAttributesIsMutable()V

    .line 61509
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->attributes_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 61511
    return-object p0
.end method

.method public removeCallees(I)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 61341
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->ensureCalleesIsMutable()V

    .line 61342
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 61344
    return-object p0
.end method

.method public setAttributes(ILcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 61456
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->ensureAttributesIsMutable()V

    .line 61457
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->attributes_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 61459
    return-object p0
.end method

.method public setAttributes(ILcom/sgiggle/xmpp/SessionMessages$KeyValuePair;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 61446
    if-nez p2, :cond_0

    .line 61447
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 61449
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->ensureAttributesIsMutable()V

    .line 61450
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->attributes_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 61452
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 61135
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 61137
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61138
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 61125
    if-nez p1, :cond_0

    .line 61126
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 61128
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 61130
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61131
    return-object p0
.end method

.method public setCallees(ILcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 61289
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->ensureCalleesIsMutable()V

    .line 61290
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 61292
    return-object p0
.end method

.method public setCallees(ILcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 61279
    if-nez p2, :cond_0

    .line 61280
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 61282
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->ensureCalleesIsMutable()V

    .line 61283
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 61285
    return-object p0
.end method

.method public setContent(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 61363
    if-nez p1, :cond_0

    .line 61364
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 61366
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61367
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->content_:Ljava/lang/Object;

    .line 61369
    return-object p0
.end method

.method setContent(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 61378
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61379
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->content_:Ljava/lang/Object;

    .line 61381
    return-void
.end method

.method public setDuration(I)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 61225
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61226
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->duration_:I

    .line 61228
    return-object p0
.end method

.method public setFlip(Z)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 61413
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61414
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->flip_:Z

    .line 61416
    return-object p0
.end method

.method public setMime(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 61175
    if-nez p1, :cond_0

    .line 61176
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 61178
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61179
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->mime_:Ljava/lang/Object;

    .line 61181
    return-object p0
.end method

.method setMime(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 61190
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61191
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->mime_:Ljava/lang/Object;

    .line 61193
    return-void
.end method

.method public setRotation(I)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 61392
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61393
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->rotation_:I

    .line 61395
    return-object p0
.end method

.method public setSize(J)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 61204
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61205
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->size_:J

    .line 61207
    return-object p0
.end method

.method public setTimeCreated(J)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 61246
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->bitField0_:I

    .line 61247
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->timeCreated_:J

    .line 61249
    return-object p0
.end method
