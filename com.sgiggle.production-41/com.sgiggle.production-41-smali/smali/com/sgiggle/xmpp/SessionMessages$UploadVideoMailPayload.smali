.class public final Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UploadVideoMailPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    }
.end annotation


# static fields
.field public static final ATTRIBUTES_FIELD_NUMBER:I = 0xa

.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CALLEES_FIELD_NUMBER:I = 0x6

.field public static final CONTENT_FIELD_NUMBER:I = 0x7

.field public static final DURATION_FIELD_NUMBER:I = 0x4

.field public static final FLIP_FIELD_NUMBER:I = 0x9

.field public static final MIME_FIELD_NUMBER:I = 0x2

.field public static final ROTATION_FIELD_NUMBER:I = 0x8

.field public static final SIZE_FIELD_NUMBER:I = 0x3

.field public static final TIME_CREATED_FIELD_NUMBER:I = 0x5

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;


# instance fields
.field private attributes_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;",
            ">;"
        }
    .end annotation
.end field

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private callees_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private content_:Ljava/lang/Object;

.field private duration_:I

.field private flip_:Z

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private mime_:Ljava/lang/Object;

.field private rotation_:I

.field private size_:J

.field private timeCreated_:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 61518
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    .line 61519
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->initFields()V

    .line 61520
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 60423
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 60615
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->memoizedIsInitialized:B

    .line 60699
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->memoizedSerializedSize:I

    .line 60424
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 60418
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 60425
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 60615
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->memoizedIsInitialized:B

    .line 60699
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->memoizedSerializedSize:I

    .line 60425
    return-void
.end method

.method static synthetic access$78402(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 60418
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$78502(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 60418
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->mime_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$78602(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;J)J
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 60418
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->size_:J

    return-wide p1
.end method

.method static synthetic access$78702(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 60418
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->duration_:I

    return p1
.end method

.method static synthetic access$78802(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;J)J
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 60418
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->timeCreated_:J

    return-wide p1
.end method

.method static synthetic access$78900(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 60418
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->callees_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$78902(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 60418
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->callees_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$79002(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 60418
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->content_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$79102(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 60418
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->rotation_:I

    return p1
.end method

.method static synthetic access$79202(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 60418
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->flip_:Z

    return p1
.end method

.method static synthetic access$79300(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 60418
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->attributes_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$79302(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 60418
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->attributes_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$79402(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 60418
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->bitField0_:I

    return p1
.end method

.method private getContentBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 60551
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->content_:Ljava/lang/Object;

    .line 60552
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 60553
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 60555
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->content_:Ljava/lang/Object;

    .line 60558
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;
    .locals 1

    .prologue
    .line 60429
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    return-object v0
.end method

.method private getMimeBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 60468
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->mime_:Ljava/lang/Object;

    .line 60469
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 60470
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 60472
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->mime_:Ljava/lang/Object;

    .line 60475
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 60604
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 60605
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->mime_:Ljava/lang/Object;

    .line 60606
    iput-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->size_:J

    .line 60607
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->duration_:I

    .line 60608
    iput-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->timeCreated_:J

    .line 60609
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->callees_:Ljava/util/List;

    .line 60610
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->content_:Ljava/lang/Object;

    .line 60611
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->rotation_:I

    .line 60612
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->flip_:Z

    .line 60613
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->attributes_:Ljava/util/List;

    .line 60614
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 60821
    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->access$78200()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 60824
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60790
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    .line 60791
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 60792
    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->access$78100(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    move-result-object v0

    .line 60794
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60801
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    .line 60802
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 60803
    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->access$78100(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    move-result-object v0

    .line 60805
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 60757
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->access$78100(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 60763
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->access$78100(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60811
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->access$78100(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60817
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->access$78100(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60779
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->access$78100(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60785
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->access$78100(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 60768
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->access$78100(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 60774
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->access$78100(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAttributes(I)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    .locals 1
    .parameter

    .prologue
    .line 60596
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->attributes_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    return-object v0
.end method

.method public getAttributesCount()I
    .locals 1

    .prologue
    .line 60593
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->attributes_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getAttributesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60586
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->attributes_:Ljava/util/List;

    return-object v0
.end method

.method public getAttributesOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePairOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 60600
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->attributes_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePairOrBuilder;

    return-object v0
.end method

.method public getAttributesOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$KeyValuePairOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60590
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->attributes_:Ljava/util/List;

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 60444
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCallees(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter

    .prologue
    .line 60523
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->callees_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getCalleesCount()I
    .locals 1

    .prologue
    .line 60520
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->callees_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCalleesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60513
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->callees_:Ljava/util/List;

    return-object v0
.end method

.method public getCalleesOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 60527
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->callees_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;

    return-object v0
.end method

.method public getCalleesOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60517
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->callees_:Ljava/util/List;

    return-object v0
.end method

.method public getContent()Ljava/lang/String;
    .locals 2

    .prologue
    .line 60537
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->content_:Ljava/lang/Object;

    .line 60538
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 60539
    check-cast v0, Ljava/lang/String;

    .line 60547
    :goto_0
    return-object v0

    .line 60541
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 60543
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 60544
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60545
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->content_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 60547
    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 60418
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;
    .locals 1

    .prologue
    .line 60433
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    return-object v0
.end method

.method public getDuration()I
    .locals 1

    .prologue
    .line 60496
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->duration_:I

    return v0
.end method

.method public getFlip()Z
    .locals 1

    .prologue
    .line 60579
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->flip_:Z

    return v0
.end method

.method public getMime()Ljava/lang/String;
    .locals 2

    .prologue
    .line 60454
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->mime_:Ljava/lang/Object;

    .line 60455
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 60456
    check-cast v0, Ljava/lang/String;

    .line 60464
    :goto_0
    return-object v0

    .line 60458
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 60460
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 60461
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60462
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->mime_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 60464
    goto :goto_0
.end method

.method public getRotation()I
    .locals 1

    .prologue
    .line 60569
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->rotation_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 60701
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->memoizedSerializedSize:I

    .line 60702
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 60746
    :goto_0
    return v0

    .line 60705
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_a

    .line 60706
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v4

    .line 60709
    :goto_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_1

    .line 60710
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->getMimeBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 60713
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_2

    .line 60714
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->size_:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 60717
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v6, :cond_3

    .line 60718
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->duration_:I

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 60721
    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_4

    .line 60722
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->timeCreated_:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    move v1, v4

    move v2, v0

    .line 60725
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->callees_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 60726
    const/4 v3, 0x6

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->callees_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 60725
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 60729
    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_9

    .line 60730
    const/4 v0, 0x7

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->getContentBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/2addr v0, v2

    .line 60733
    :goto_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_6

    .line 60734
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->rotation_:I

    invoke-static {v6, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 60737
    :cond_6
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_7

    .line 60738
    const/16 v1, 0x9

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->flip_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    move v1, v4

    move v2, v0

    .line 60741
    :goto_4
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->attributes_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 60742
    const/16 v3, 0xa

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->attributes_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 60741
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_4

    .line 60745
    :cond_8
    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->memoizedSerializedSize:I

    move v0, v2

    .line 60746
    goto/16 :goto_0

    :cond_9
    move v0, v2

    goto :goto_3

    :cond_a
    move v0, v4

    goto/16 :goto_1
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 60486
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->size_:J

    return-wide v0
.end method

.method public getTimeCreated()J
    .locals 2

    .prologue
    .line 60506
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->timeCreated_:J

    return-wide v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 60441
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasContent()Z
    .locals 2

    .prologue
    .line 60534
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDuration()Z
    .locals 2

    .prologue
    .line 60493
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFlip()Z
    .locals 2

    .prologue
    .line 60576
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMime()Z
    .locals 2

    .prologue
    .line 60451
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRotation()Z
    .locals 2

    .prologue
    .line 60566
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSize()Z
    .locals 2

    .prologue
    .line 60483
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTimeCreated()Z
    .locals 2

    .prologue
    .line 60503
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 60617
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->memoizedIsInitialized:B

    .line 60618
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 60661
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 60618
    goto :goto_0

    .line 60620
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 60621
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 60622
    goto :goto_0

    .line 60624
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->hasMime()Z

    move-result v0

    if-nez v0, :cond_3

    .line 60625
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 60626
    goto :goto_0

    .line 60628
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->hasSize()Z

    move-result v0

    if-nez v0, :cond_4

    .line 60629
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 60630
    goto :goto_0

    .line 60632
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->hasDuration()Z

    move-result v0

    if-nez v0, :cond_5

    .line 60633
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 60634
    goto :goto_0

    .line 60636
    :cond_5
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->hasTimeCreated()Z

    move-result v0

    if-nez v0, :cond_6

    .line 60637
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 60638
    goto :goto_0

    .line 60640
    :cond_6
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->hasContent()Z

    move-result v0

    if-nez v0, :cond_7

    .line 60641
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 60642
    goto :goto_0

    .line 60644
    :cond_7
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_8

    .line 60645
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 60646
    goto :goto_0

    :cond_8
    move v0, v2

    .line 60648
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->getCalleesCount()I

    move-result v1

    if-ge v0, v1, :cond_a

    .line 60649
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->getCallees(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_9

    .line 60650
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 60651
    goto :goto_0

    .line 60648
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_a
    move v0, v2

    .line 60654
    :goto_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->getAttributesCount()I

    move-result v1

    if-ge v0, v1, :cond_c

    .line 60655
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->getAttributes(I)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_b

    .line 60656
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 60657
    goto :goto_0

    .line 60654
    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 60660
    :cond_c
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 60661
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 60418
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 60822
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 60418
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 60826
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 60751
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 6
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 60666
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->getSerializedSize()I

    .line 60667
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 60668
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 60670
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 60671
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->getMimeBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 60673
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 60674
    const/4 v0, 0x3

    iget-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->size_:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeUInt64(IJ)V

    .line 60676
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_3

    .line 60677
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->duration_:I

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 60679
    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 60680
    const/4 v0, 0x5

    iget-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->timeCreated_:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeUInt64(IJ)V

    :cond_4
    move v1, v3

    .line 60682
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->callees_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 60683
    const/4 v2, 0x6

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->callees_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 60682
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 60685
    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 60686
    const/4 v0, 0x7

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->getContentBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 60688
    :cond_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_7

    .line 60689
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->rotation_:I

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 60691
    :cond_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_8

    .line 60692
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->flip_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_8
    move v1, v3

    .line 60694
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->attributes_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 60695
    const/16 v2, 0xa

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->attributes_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 60694
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 60697
    :cond_9
    return-void
.end method
