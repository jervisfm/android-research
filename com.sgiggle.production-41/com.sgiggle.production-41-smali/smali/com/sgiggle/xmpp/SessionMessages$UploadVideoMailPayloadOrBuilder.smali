.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "UploadVideoMailPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getAttributes(I)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
.end method

.method public abstract getAttributesCount()I
.end method

.method public abstract getAttributesList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getCallees(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
.end method

.method public abstract getCalleesCount()I
.end method

.method public abstract getCalleesList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getContent()Ljava/lang/String;
.end method

.method public abstract getDuration()I
.end method

.method public abstract getFlip()Z
.end method

.method public abstract getMime()Ljava/lang/String;
.end method

.method public abstract getRotation()I
.end method

.method public abstract getSize()J
.end method

.method public abstract getTimeCreated()J
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasContent()Z
.end method

.method public abstract hasDuration()Z
.end method

.method public abstract hasFlip()Z
.end method

.method public abstract hasMime()Z
.end method

.method public abstract hasRotation()Z
.end method

.method public abstract hasSize()Z
.end method

.method public abstract hasTimeCreated()Z
.end method
