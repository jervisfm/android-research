.class public final Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private total_:I

.field private uploaded_:I

.field private videoMailId_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 61787
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 61949
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 61992
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 61788
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->maybeForceBuilderInitialization()V

    .line 61789
    return-void
.end method

.method static synthetic access$79500(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 61782
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$79600()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;
    .locals 1

    .prologue
    .line 61782
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 61828
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;

    move-result-object v0

    .line 61829
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 61830
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 61833
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;
    .locals 1

    .prologue
    .line 61794
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 61792
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 61782
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;
    .locals 2

    .prologue
    .line 61819
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;

    move-result-object v0

    .line 61820
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 61821
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 61823
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 61782
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;
    .locals 5

    .prologue
    .line 61837
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 61838
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    .line 61839
    const/4 v2, 0x0

    .line 61840
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 61841
    or-int/lit8 v2, v2, 0x1

    .line 61843
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->access$79802(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 61844
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 61845
    or-int/lit8 v2, v2, 0x2

    .line 61847
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->videoMailId_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->videoMailId_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->access$79902(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61848
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 61849
    or-int/lit8 v2, v2, 0x4

    .line 61851
    :cond_2
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->uploaded_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->uploaded_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->access$80002(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;I)I

    .line 61852
    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_3

    .line 61853
    or-int/lit8 v1, v2, 0x8

    .line 61855
    :goto_0
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->total_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->total_:I
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->access$80102(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;I)I

    .line 61856
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->access$80202(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;I)I

    .line 61857
    return-object v0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 61782
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 61782
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 61798
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 61799
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 61800
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    .line 61801
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 61802
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    .line 61803
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->uploaded_:I

    .line 61804
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    .line 61805
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->total_:I

    .line 61806
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    .line 61807
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;
    .locals 1

    .prologue
    .line 61985
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 61987
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    .line 61988
    return-object p0
.end method

.method public clearTotal()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;
    .locals 1

    .prologue
    .line 62063
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    .line 62064
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->total_:I

    .line 62066
    return-object p0
.end method

.method public clearUploaded()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;
    .locals 1

    .prologue
    .line 62042
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    .line 62043
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->uploaded_:I

    .line 62045
    return-object p0
.end method

.method public clearVideoMailId()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;
    .locals 1

    .prologue
    .line 62016
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    .line 62017
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->getVideoMailId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 62019
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 61782
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 61782
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 61782
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;
    .locals 2

    .prologue
    .line 61811
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 61782
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 61954
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 61782
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 61782
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;
    .locals 1

    .prologue
    .line 61815
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;

    move-result-object v0

    return-object v0
.end method

.method public getTotal()I
    .locals 1

    .prologue
    .line 62054
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->total_:I

    return v0
.end method

.method public getUploaded()I
    .locals 1

    .prologue
    .line 62033
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->uploaded_:I

    return v0
.end method

.method public getVideoMailId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 61997
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 61998
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 61999
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 62000
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 62003
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 61951
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTotal()Z
    .locals 2

    .prologue
    .line 62051
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUploaded()Z
    .locals 2

    .prologue
    .line 62030
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoMailId()Z
    .locals 2

    .prologue
    .line 61994
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 61878
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 61898
    :goto_0
    return v0

    .line 61882
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->hasVideoMailId()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 61884
    goto :goto_0

    .line 61886
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->hasUploaded()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 61888
    goto :goto_0

    .line 61890
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->hasTotal()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 61892
    goto :goto_0

    .line 61894
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 61896
    goto :goto_0

    .line 61898
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 61973
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 61975
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 61981
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    .line 61982
    return-object p0

    .line 61978
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61782
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 61782
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61782
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61906
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 61907
    sparse-switch v0, :sswitch_data_0

    .line 61912
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 61914
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 61910
    goto :goto_1

    .line 61919
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 61920
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 61921
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 61923
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 61924
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    goto :goto_0

    .line 61928
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    .line 61929
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->videoMailId_:Ljava/lang/Object;

    goto :goto_0

    .line 61933
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    .line 61934
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->uploaded_:I

    goto :goto_0

    .line 61938
    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    .line 61939
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->total_:I

    goto :goto_0

    .line 61907
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 61861
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 61874
    :goto_0
    return-object v0

    .line 61862
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 61863
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    .line 61865
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->hasVideoMailId()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 61866
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->getVideoMailId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->setVideoMailId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    .line 61868
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->hasUploaded()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 61869
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->getUploaded()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->setUploaded(I)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    .line 61871
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->hasTotal()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 61872
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->getTotal()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->setTotal(I)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    :cond_4
    move-object v0, p0

    .line 61874
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 61967
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 61969
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    .line 61970
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 61957
    if-nez p1, :cond_0

    .line 61958
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 61960
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 61962
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    .line 61963
    return-object p0
.end method

.method public setTotal(I)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 62057
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    .line 62058
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->total_:I

    .line 62060
    return-object p0
.end method

.method public setUploaded(I)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 62036
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    .line 62037
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->uploaded_:I

    .line 62039
    return-object p0
.end method

.method public setVideoMailId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 62007
    if-nez p1, :cond_0

    .line 62008
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 62010
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    .line 62011
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 62013
    return-object p0
.end method

.method setVideoMailId(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 62022
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->bitField0_:I

    .line 62023
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 62025
    return-void
.end method
