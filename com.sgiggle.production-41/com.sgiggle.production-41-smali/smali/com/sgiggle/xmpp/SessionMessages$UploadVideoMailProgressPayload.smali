.class public final Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UploadVideoMailProgressPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final TOTAL_FIELD_NUMBER:I = 0x4

.field public static final UPLOADED_FIELD_NUMBER:I = 0x3

.field public static final VIDEO_MAIL_ID_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private total_:I

.field private uploaded_:I

.field private videoMailId_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 62073
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;

    .line 62074
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->initFields()V

    .line 62075
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 61549
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 61631
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->memoizedIsInitialized:B

    .line 61677
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->memoizedSerializedSize:I

    .line 61550
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 61544
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 61551
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 61631
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->memoizedIsInitialized:B

    .line 61677
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->memoizedSerializedSize:I

    .line 61551
    return-void
.end method

.method static synthetic access$79802(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 61544
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$79902(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 61544
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->videoMailId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$80002(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 61544
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->uploaded_:I

    return p1
.end method

.method static synthetic access$80102(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 61544
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->total_:I

    return p1
.end method

.method static synthetic access$80202(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 61544
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;
    .locals 1

    .prologue
    .line 61555
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;

    return-object v0
.end method

.method private getVideoMailIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 61594
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->videoMailId_:Ljava/lang/Object;

    .line 61595
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 61596
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 61598
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->videoMailId_:Ljava/lang/Object;

    .line 61601
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 61626
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 61627
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->videoMailId_:Ljava/lang/Object;

    .line 61628
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->uploaded_:I

    .line 61629
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->total_:I

    .line 61630
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;
    .locals 1

    .prologue
    .line 61775
    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->access$79600()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 61778
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61744
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    move-result-object v0

    .line 61745
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 61746
    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->access$79500(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;

    move-result-object v0

    .line 61748
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61755
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    move-result-object v0

    .line 61756
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 61757
    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->access$79500(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;

    move-result-object v0

    .line 61759
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 61711
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->access$79500(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 61717
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->access$79500(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61765
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->access$79500(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61771
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->access$79500(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61733
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->access$79500(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61739
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->access$79500(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 61722
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->access$79500(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 61728
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;->access$79500(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 61570
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 61544
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;
    .locals 1

    .prologue
    .line 61559
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 61679
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->memoizedSerializedSize:I

    .line 61680
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 61700
    :goto_0
    return v0

    .line 61682
    :cond_0
    const/4 v0, 0x0

    .line 61683
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 61684
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 61687
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 61688
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->getVideoMailIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 61691
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 61692
    const/4 v1, 0x3

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->uploaded_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 61695
    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 61696
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->total_:I

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 61699
    :cond_4
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getTotal()I
    .locals 1

    .prologue
    .line 61622
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->total_:I

    return v0
.end method

.method public getUploaded()I
    .locals 1

    .prologue
    .line 61612
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->uploaded_:I

    return v0
.end method

.method public getVideoMailId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 61580
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->videoMailId_:Ljava/lang/Object;

    .line 61581
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 61582
    check-cast v0, Ljava/lang/String;

    .line 61590
    :goto_0
    return-object v0

    .line 61584
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 61586
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 61587
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 61588
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->videoMailId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 61590
    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 61567
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTotal()Z
    .locals 2

    .prologue
    .line 61619
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUploaded()Z
    .locals 2

    .prologue
    .line 61609
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoMailId()Z
    .locals 2

    .prologue
    .line 61577
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 61633
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->memoizedIsInitialized:B

    .line 61634
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 61657
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 61634
    goto :goto_0

    .line 61636
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 61637
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 61638
    goto :goto_0

    .line 61640
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->hasVideoMailId()Z

    move-result v0

    if-nez v0, :cond_3

    .line 61641
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 61642
    goto :goto_0

    .line 61644
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->hasUploaded()Z

    move-result v0

    if-nez v0, :cond_4

    .line 61645
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 61646
    goto :goto_0

    .line 61648
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->hasTotal()Z

    move-result v0

    if-nez v0, :cond_5

    .line 61649
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 61650
    goto :goto_0

    .line 61652
    :cond_5
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_6

    .line 61653
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 61654
    goto :goto_0

    .line 61656
    :cond_6
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 61657
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 61544
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;
    .locals 1

    .prologue
    .line 61776
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 61544
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;
    .locals 1

    .prologue
    .line 61780
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 61705
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 61662
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->getSerializedSize()I

    .line 61663
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 61664
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 61666
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 61667
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->getVideoMailIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 61669
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 61670
    const/4 v0, 0x3

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->uploaded_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 61672
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 61673
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayload;->total_:I

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 61675
    :cond_3
    return-void
.end method
