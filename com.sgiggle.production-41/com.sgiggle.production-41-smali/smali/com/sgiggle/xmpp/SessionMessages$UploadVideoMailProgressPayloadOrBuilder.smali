.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailProgressPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "UploadVideoMailProgressPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getTotal()I
.end method

.method public abstract getUploaded()I
.end method

.method public abstract getVideoMailId()Ljava/lang/String;
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasTotal()Z
.end method

.method public abstract hasUploaded()Z
.end method

.method public abstract hasVideoMailId()Z
.end method
