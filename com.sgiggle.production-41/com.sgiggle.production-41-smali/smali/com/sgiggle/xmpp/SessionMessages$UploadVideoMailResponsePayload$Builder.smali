.class public final Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private resumePos_:I

.field private videoMailId_:Ljava/lang/Object;

.field private videoMailUrl_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 62360
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 62518
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 62561
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 62597
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->videoMailUrl_:Ljava/lang/Object;

    .line 62361
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->maybeForceBuilderInitialization()V

    .line 62362
    return-void
.end method

.method static synthetic access$80300(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 62355
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$80400()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;
    .locals 1

    .prologue
    .line 62355
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 62401
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    move-result-object v0

    .line 62402
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 62403
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 62406
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;
    .locals 1

    .prologue
    .line 62367
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 62365
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 62355
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;
    .locals 2

    .prologue
    .line 62392
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    move-result-object v0

    .line 62393
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 62394
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 62396
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 62355
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;
    .locals 5

    .prologue
    .line 62410
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 62411
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    .line 62412
    const/4 v2, 0x0

    .line 62413
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 62414
    or-int/lit8 v2, v2, 0x1

    .line 62416
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->access$80602(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 62417
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 62418
    or-int/lit8 v2, v2, 0x2

    .line 62420
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->videoMailId_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->videoMailId_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->access$80702(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62421
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 62422
    or-int/lit8 v2, v2, 0x4

    .line 62424
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->videoMailUrl_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->videoMailUrl_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->access$80802(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62425
    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_3

    .line 62426
    or-int/lit8 v1, v2, 0x8

    .line 62428
    :goto_0
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->resumePos_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->resumePos_:I
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->access$80902(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;I)I

    .line 62429
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->access$81002(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;I)I

    .line 62430
    return-object v0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 62355
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 62355
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;
    .locals 1

    .prologue
    .line 62371
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 62372
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 62373
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    .line 62374
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 62375
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    .line 62376
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->videoMailUrl_:Ljava/lang/Object;

    .line 62377
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    .line 62378
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->resumePos_:I

    .line 62379
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    .line 62380
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;
    .locals 1

    .prologue
    .line 62554
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 62556
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    .line 62557
    return-object p0
.end method

.method public clearResumePos()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;
    .locals 1

    .prologue
    .line 62647
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    .line 62648
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->resumePos_:I

    .line 62650
    return-object p0
.end method

.method public clearVideoMailId()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;
    .locals 1

    .prologue
    .line 62585
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    .line 62586
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->getVideoMailId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 62588
    return-object p0
.end method

.method public clearVideoMailUrl()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;
    .locals 1

    .prologue
    .line 62621
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    .line 62622
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->getVideoMailUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->videoMailUrl_:Ljava/lang/Object;

    .line 62624
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 62355
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 62355
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 62355
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;
    .locals 2

    .prologue
    .line 62384
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 62355
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 62523
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 62355
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 62355
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;
    .locals 1

    .prologue
    .line 62388
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    move-result-object v0

    return-object v0
.end method

.method public getResumePos()I
    .locals 1

    .prologue
    .line 62638
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->resumePos_:I

    return v0
.end method

.method public getVideoMailId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 62566
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 62567
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 62568
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 62569
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 62572
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getVideoMailUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 62602
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->videoMailUrl_:Ljava/lang/Object;

    .line 62603
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 62604
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 62605
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->videoMailUrl_:Ljava/lang/Object;

    .line 62608
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 62520
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasResumePos()Z
    .locals 2

    .prologue
    .line 62635
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoMailId()Z
    .locals 2

    .prologue
    .line 62563
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoMailUrl()Z
    .locals 2

    .prologue
    .line 62599
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 62451
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 62467
    :goto_0
    return v0

    .line 62455
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->hasVideoMailId()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 62457
    goto :goto_0

    .line 62459
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->hasVideoMailUrl()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 62461
    goto :goto_0

    .line 62463
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 62465
    goto :goto_0

    .line 62467
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 62542
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 62544
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 62550
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    .line 62551
    return-object p0

    .line 62547
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62355
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 62355
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62355
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62475
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 62476
    sparse-switch v0, :sswitch_data_0

    .line 62481
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 62483
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 62479
    goto :goto_1

    .line 62488
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 62489
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 62490
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 62492
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 62493
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    goto :goto_0

    .line 62497
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    .line 62498
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->videoMailId_:Ljava/lang/Object;

    goto :goto_0

    .line 62502
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    .line 62503
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->videoMailUrl_:Ljava/lang/Object;

    goto :goto_0

    .line 62507
    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    .line 62508
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->resumePos_:I

    goto :goto_0

    .line 62476
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 62434
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 62447
    :goto_0
    return-object v0

    .line 62435
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62436
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    .line 62438
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->hasVideoMailId()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 62439
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->getVideoMailId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->setVideoMailId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    .line 62441
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->hasVideoMailUrl()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 62442
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->getVideoMailUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->setVideoMailUrl(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    .line 62444
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->hasResumePos()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 62445
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->getResumePos()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->setResumePos(I)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    :cond_4
    move-object v0, p0

    .line 62447
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 62536
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 62538
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    .line 62539
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 62526
    if-nez p1, :cond_0

    .line 62527
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 62529
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 62531
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    .line 62532
    return-object p0
.end method

.method public setResumePos(I)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 62641
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    .line 62642
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->resumePos_:I

    .line 62644
    return-object p0
.end method

.method public setVideoMailId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 62576
    if-nez p1, :cond_0

    .line 62577
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 62579
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    .line 62580
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 62582
    return-object p0
.end method

.method setVideoMailId(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 62591
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    .line 62592
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 62594
    return-void
.end method

.method public setVideoMailUrl(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 62612
    if-nez p1, :cond_0

    .line 62613
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 62615
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    .line 62616
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->videoMailUrl_:Ljava/lang/Object;

    .line 62618
    return-object p0
.end method

.method setVideoMailUrl(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 62627
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->bitField0_:I

    .line 62628
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->videoMailUrl_:Ljava/lang/Object;

    .line 62630
    return-void
.end method
