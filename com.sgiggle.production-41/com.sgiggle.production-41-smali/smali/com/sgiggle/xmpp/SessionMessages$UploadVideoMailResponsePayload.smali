.class public final Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UploadVideoMailResponsePayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final RESUME_POS_FIELD_NUMBER:I = 0x4

.field public static final VIDEO_MAIL_ID_FIELD_NUMBER:I = 0x2

.field public static final VIDEO_MAIL_URL_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private resumePos_:I

.field private videoMailId_:Ljava/lang/Object;

.field private videoMailUrl_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 62657
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    .line 62658
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->initFields()V

    .line 62659
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 62104
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 62208
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->memoizedIsInitialized:B

    .line 62250
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->memoizedSerializedSize:I

    .line 62105
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 62099
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 62106
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 62208
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->memoizedIsInitialized:B

    .line 62250
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->memoizedSerializedSize:I

    .line 62106
    return-void
.end method

.method static synthetic access$80602(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 62099
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$80702(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 62099
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->videoMailId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$80802(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 62099
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->videoMailUrl_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$80902(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 62099
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->resumePos_:I

    return p1
.end method

.method static synthetic access$81002(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 62099
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;
    .locals 1

    .prologue
    .line 62110
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    return-object v0
.end method

.method private getVideoMailIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 62149
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->videoMailId_:Ljava/lang/Object;

    .line 62150
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 62151
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 62153
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->videoMailId_:Ljava/lang/Object;

    .line 62156
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getVideoMailUrlBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 62181
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->videoMailUrl_:Ljava/lang/Object;

    .line 62182
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 62183
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 62185
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->videoMailUrl_:Ljava/lang/Object;

    .line 62188
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 62203
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 62204
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->videoMailId_:Ljava/lang/Object;

    .line 62205
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->videoMailUrl_:Ljava/lang/Object;

    .line 62206
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->resumePos_:I

    .line 62207
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;
    .locals 1

    .prologue
    .line 62348
    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->access$80400()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 62351
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62317
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    move-result-object v0

    .line 62318
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 62319
    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->access$80300(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    move-result-object v0

    .line 62321
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62328
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    move-result-object v0

    .line 62329
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 62330
    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->access$80300(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    move-result-object v0

    .line 62332
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 62284
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->access$80300(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 62290
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->access$80300(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62338
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->access$80300(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62344
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->access$80300(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62306
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->access$80300(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62312
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->access$80300(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 62295
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->access$80300(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 62301
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;->access$80300(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 62125
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 62099
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;
    .locals 1

    .prologue
    .line 62114
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    return-object v0
.end method

.method public getResumePos()I
    .locals 1

    .prologue
    .line 62199
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->resumePos_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 62252
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->memoizedSerializedSize:I

    .line 62253
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 62273
    :goto_0
    return v0

    .line 62255
    :cond_0
    const/4 v0, 0x0

    .line 62256
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 62257
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 62260
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 62261
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->getVideoMailIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 62264
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 62265
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->getVideoMailUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 62268
    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 62269
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->resumePos_:I

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 62272
    :cond_4
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getVideoMailId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 62135
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->videoMailId_:Ljava/lang/Object;

    .line 62136
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 62137
    check-cast v0, Ljava/lang/String;

    .line 62145
    :goto_0
    return-object v0

    .line 62139
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 62141
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 62142
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62143
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->videoMailId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 62145
    goto :goto_0
.end method

.method public getVideoMailUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 62167
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->videoMailUrl_:Ljava/lang/Object;

    .line 62168
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 62169
    check-cast v0, Ljava/lang/String;

    .line 62177
    :goto_0
    return-object v0

    .line 62171
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 62173
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 62174
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62175
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->videoMailUrl_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 62177
    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 62122
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasResumePos()Z
    .locals 2

    .prologue
    .line 62196
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoMailId()Z
    .locals 2

    .prologue
    .line 62132
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoMailUrl()Z
    .locals 2

    .prologue
    .line 62164
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 62210
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->memoizedIsInitialized:B

    .line 62211
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 62230
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 62211
    goto :goto_0

    .line 62213
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 62214
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 62215
    goto :goto_0

    .line 62217
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->hasVideoMailId()Z

    move-result v0

    if-nez v0, :cond_3

    .line 62218
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 62219
    goto :goto_0

    .line 62221
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->hasVideoMailUrl()Z

    move-result v0

    if-nez v0, :cond_4

    .line 62222
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 62223
    goto :goto_0

    .line 62225
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_5

    .line 62226
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 62227
    goto :goto_0

    .line 62229
    :cond_5
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->memoizedIsInitialized:B

    move v0, v3

    .line 62230
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 62099
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;
    .locals 1

    .prologue
    .line 62349
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 62099
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;
    .locals 1

    .prologue
    .line 62353
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 62278
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 62235
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->getSerializedSize()I

    .line 62236
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 62237
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 62239
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 62240
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->getVideoMailIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 62242
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 62243
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->getVideoMailUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 62245
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 62246
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->resumePos_:I

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 62248
    :cond_3
    return-void
.end method
