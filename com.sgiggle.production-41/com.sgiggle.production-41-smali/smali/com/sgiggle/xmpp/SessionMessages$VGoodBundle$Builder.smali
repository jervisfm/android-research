.class public final Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$VGoodBundleOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;",
        "Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$VGoodBundleOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private cinematic_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

.field private dirty_:Z

.field private image_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8665
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 8802
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->image_:Ljava/util/List;

    .line 8891
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->cinematic_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    .line 8666
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->maybeForceBuilderInitialization()V

    .line 8667
    return-void
.end method

.method static synthetic access$10100(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 8660
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$10200()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;
    .locals 1

    .prologue
    .line 8660
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 8704
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    .line 8705
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 8706
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 8709
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;
    .locals 1

    .prologue
    .line 8672
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;-><init>()V

    return-object v0
.end method

.method private ensureImageIsMutable()V
    .locals 2

    .prologue
    .line 8805
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 8806
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->image_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->image_:Ljava/util/List;

    .line 8807
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    .line 8809
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 8670
    return-void
.end method


# virtual methods
.method public addAllImage(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;"
        }
    .end annotation

    .prologue
    .line 8872
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->ensureImageIsMutable()V

    .line 8873
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->image_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 8875
    return-object p0
.end method

.method public addImage(ILcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 8865
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->ensureImageIsMutable()V

    .line 8866
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->image_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 8868
    return-object p0
.end method

.method public addImage(ILcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 8848
    if-nez p2, :cond_0

    .line 8849
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 8851
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->ensureImageIsMutable()V

    .line 8852
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->image_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 8854
    return-object p0
.end method

.method public addImage(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;
    .locals 2
    .parameter

    .prologue
    .line 8858
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->ensureImageIsMutable()V

    .line 8859
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->image_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 8861
    return-object p0
.end method

.method public addImage(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;
    .locals 1
    .parameter

    .prologue
    .line 8838
    if-nez p1, :cond_0

    .line 8839
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 8841
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->ensureImageIsMutable()V

    .line 8842
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->image_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 8844
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 8660
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    .locals 2

    .prologue
    .line 8695
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    .line 8696
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 8697
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 8699
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 8660
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    .locals 5

    .prologue
    .line 8713
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;-><init>(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 8714
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    .line 8715
    const/4 v2, 0x0

    .line 8716
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 8717
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->image_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->image_:Ljava/util/List;

    .line 8718
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x2

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    .line 8720
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->image_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->image_:Ljava/util/List;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->access$10402(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;Ljava/util/List;)Ljava/util/List;

    .line 8721
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 8722
    or-int/lit8 v2, v2, 0x1

    .line 8724
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->cinematic_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->cinematic_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->access$10502(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    .line 8725
    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_2

    .line 8726
    or-int/lit8 v1, v2, 0x2

    .line 8728
    :goto_0
    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->dirty_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->dirty_:Z
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->access$10602(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;Z)Z

    .line 8729
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->access$10702(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;I)I

    .line 8730
    return-object v0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 8660
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 8660
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;
    .locals 1

    .prologue
    .line 8676
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 8677
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->image_:Ljava/util/List;

    .line 8678
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    .line 8679
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->cinematic_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    .line 8680
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    .line 8681
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->dirty_:Z

    .line 8682
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    .line 8683
    return-object p0
.end method

.method public clearCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;
    .locals 1

    .prologue
    .line 8927
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->cinematic_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    .line 8929
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    .line 8930
    return-object p0
.end method

.method public clearDirty()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;
    .locals 1

    .prologue
    .line 8948
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    .line 8949
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->dirty_:Z

    .line 8951
    return-object p0
.end method

.method public clearImage()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;
    .locals 1

    .prologue
    .line 8878
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->image_:Ljava/util/List;

    .line 8879
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    .line 8881
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 8660
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 8660
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 8660
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;
    .locals 2

    .prologue
    .line 8687
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 8660
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    .locals 1

    .prologue
    .line 8896
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->cinematic_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 8660
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 8660
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    .locals 1

    .prologue
    .line 8691
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    return-object v0
.end method

.method public getDirty()Z
    .locals 1

    .prologue
    .line 8939
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->dirty_:Z

    return v0
.end method

.method public getImage(I)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
    .locals 1
    .parameter

    .prologue
    .line 8818
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->image_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    return-object v0
.end method

.method public getImageCount()I
    .locals 1

    .prologue
    .line 8815
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->image_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getImageList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 8812
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->image_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hasCinematic()Z
    .locals 2

    .prologue
    .line 8893
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDirty()Z
    .locals 2

    .prologue
    .line 8936
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 8755
    const/4 v0, 0x1

    return v0
.end method

.method public mergeCinematic(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;
    .locals 2
    .parameter

    .prologue
    .line 8915
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->cinematic_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 8917
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->cinematic_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->cinematic_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    .line 8923
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    .line 8924
    return-object p0

    .line 8920
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->cinematic_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8660
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 8660
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8660
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8763
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 8764
    sparse-switch v0, :sswitch_data_0

    .line 8769
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 8771
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 8767
    goto :goto_1

    .line 8776
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    .line 8777
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 8778
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->addImage(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    goto :goto_0

    .line 8782
    :sswitch_2
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    .line 8783
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->hasCinematic()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 8784
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    .line 8786
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 8787
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->setCinematic(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    goto :goto_0

    .line 8791
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    .line 8792
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->dirty_:Z

    goto :goto_0

    .line 8764
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;
    .locals 2
    .parameter

    .prologue
    .line 8734
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 8751
    :goto_0
    return-object v0

    .line 8735
    :cond_0
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->image_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->access$10400(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 8736
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->image_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 8737
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->image_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->access$10400(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->image_:Ljava/util/List;

    .line 8738
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    .line 8745
    :cond_1
    :goto_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->hasCinematic()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 8746
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->mergeCinematic(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    .line 8748
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->hasDirty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 8749
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getDirty()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->setDirty(Z)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    :cond_3
    move-object v0, p0

    .line 8751
    goto :goto_0

    .line 8740
    :cond_4
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->ensureImageIsMutable()V

    .line 8741
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->image_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->image_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->access$10400(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public removeImage(I)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;
    .locals 1
    .parameter

    .prologue
    .line 8884
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->ensureImageIsMutable()V

    .line 8885
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->image_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 8887
    return-object p0
.end method

.method public setCinematic(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;
    .locals 1
    .parameter

    .prologue
    .line 8909
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->cinematic_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    .line 8911
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    .line 8912
    return-object p0
.end method

.method public setCinematic(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;
    .locals 1
    .parameter

    .prologue
    .line 8899
    if-nez p1, :cond_0

    .line 8900
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 8902
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->cinematic_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    .line 8904
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    .line 8905
    return-object p0
.end method

.method public setDirty(Z)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;
    .locals 1
    .parameter

    .prologue
    .line 8942
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->bitField0_:I

    .line 8943
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->dirty_:Z

    .line 8945
    return-object p0
.end method

.method public setImage(ILcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 8832
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->ensureImageIsMutable()V

    .line 8833
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->image_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 8835
    return-object p0
.end method

.method public setImage(ILcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 8822
    if-nez p2, :cond_0

    .line 8823
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 8825
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->ensureImageIsMutable()V

    .line 8826
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->image_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 8828
    return-object p0
.end method
