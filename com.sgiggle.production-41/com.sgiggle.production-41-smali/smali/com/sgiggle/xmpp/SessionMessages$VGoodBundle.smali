.class public final Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$VGoodBundleOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VGoodBundle"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;
    }
.end annotation


# static fields
.field public static final CINEMATIC_FIELD_NUMBER:I = 0x2

.field public static final DIRTY_FIELD_NUMBER:I = 0x3

.field public static final IMAGE_FIELD_NUMBER:I = 0x1

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;


# instance fields
.field private bitField0_:I

.field private cinematic_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

.field private dirty_:Z

.field private image_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 8958
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    .line 8959
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->initFields()V

    .line 8960
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 8476
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 8536
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->memoizedIsInitialized:B

    .line 8559
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->memoizedSerializedSize:I

    .line 8477
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 8471
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;-><init>(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 8478
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 8536
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->memoizedIsInitialized:B

    .line 8559
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->memoizedSerializedSize:I

    .line 8478
    return-void
.end method

.method static synthetic access$10400(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 8471
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->image_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$10402(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 8471
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->image_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$10502(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 8471
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->cinematic_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    return-object p1
.end method

.method static synthetic access$10602(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 8471
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->dirty_:Z

    return p1
.end method

.method static synthetic access$10702(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 8471
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    .locals 1

    .prologue
    .line 8482
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 8532
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->image_:Ljava/util/List;

    .line 8533
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->cinematic_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    .line 8534
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->dirty_:Z

    .line 8535
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;
    .locals 1

    .prologue
    .line 8653
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->access$10200()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;
    .locals 1
    .parameter

    .prologue
    .line 8656
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8622
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    .line 8623
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8624
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->access$10100(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    .line 8626
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8633
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    .line 8634
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8635
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->access$10100(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    .line 8637
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 8589
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->access$10100(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 8595
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->access$10100(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8643
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->access$10100(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8649
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->access$10100(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8611
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->access$10100(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8617
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->access$10100(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 8600
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->access$10100(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 8606
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->access$10100(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    .locals 1

    .prologue
    .line 8518
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->cinematic_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 8471
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    .locals 1

    .prologue
    .line 8486
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    return-object v0
.end method

.method public getDirty()Z
    .locals 1

    .prologue
    .line 8528
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->dirty_:Z

    return v0
.end method

.method public getImage(I)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
    .locals 1
    .parameter

    .prologue
    .line 8504
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->image_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    return-object v0
.end method

.method public getImageCount()I
    .locals 1

    .prologue
    .line 8501
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->image_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getImageList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 8494
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->image_:Ljava/util/List;

    return-object v0
.end method

.method public getImageOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImageOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 8508
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->image_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImageOrBuilder;

    return-object v0
.end method

.method public getImageOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImageOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 8498
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->image_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 8561
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->memoizedSerializedSize:I

    .line 8562
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 8578
    :goto_0
    return v0

    :cond_0
    move v1, v2

    .line 8565
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->image_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 8566
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->image_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 8565
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_1

    .line 8569
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 8570
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->cinematic_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 8573
    :goto_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_2

    .line 8574
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->dirty_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 8577
    :cond_2
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->memoizedSerializedSize:I

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_2
.end method

.method public hasCinematic()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 8515
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDirty()Z
    .locals 2

    .prologue
    .line 8525
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 8538
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->memoizedIsInitialized:B

    .line 8539
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v2, :cond_0

    move v0, v2

    .line 8542
    :goto_0
    return v0

    .line 8539
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 8541
    :cond_1
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->memoizedIsInitialized:B

    move v0, v2

    .line 8542
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 8471
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;
    .locals 1

    .prologue
    .line 8654
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 8471
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;
    .locals 1

    .prologue
    .line 8658
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 8583
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 8547
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getSerializedSize()I

    .line 8548
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->image_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 8549
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->image_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 8548
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 8551
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_1

    .line 8552
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->cinematic_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 8554
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_2

    .line 8555
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->dirty_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 8557
    :cond_2
    return-void
.end method
