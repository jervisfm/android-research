.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$VGoodBundleOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "VGoodBundleOrBuilder"
.end annotation


# virtual methods
.method public abstract getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
.end method

.method public abstract getDirty()Z
.end method

.method public abstract getImage(I)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
.end method

.method public abstract getImageCount()I
.end method

.method public abstract getImageList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;",
            ">;"
        }
    .end annotation
.end method

.method public abstract hasCinematic()Z
.end method

.method public abstract hasDirty()Z
.end method
