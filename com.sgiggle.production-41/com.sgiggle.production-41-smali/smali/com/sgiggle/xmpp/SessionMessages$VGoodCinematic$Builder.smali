.class public final Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematicOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;",
        "Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematicOrBuilder;"
    }
.end annotation


# instance fields
.field private assetId_:J

.field private assetPath_:Ljava/lang/Object;

.field private bitField0_:I

.field private type_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 8235
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 8384
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->assetPath_:Ljava/lang/Object;

    .line 8420
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->VGOOD_ANIMATION:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    .line 8236
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->maybeForceBuilderInitialization()V

    .line 8237
    return-void
.end method

.method static synthetic access$9400(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 8230
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$9500()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;
    .locals 1

    .prologue
    .line 8230
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 8274
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    .line 8275
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 8276
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 8279
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;
    .locals 1

    .prologue
    .line 8242
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 8240
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 8230
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    .locals 2

    .prologue
    .line 8265
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    .line 8266
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 8267
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 8269
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 8230
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    .locals 5

    .prologue
    .line 8283
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;-><init>(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 8284
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->bitField0_:I

    .line 8285
    const/4 v2, 0x0

    .line 8286
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 8287
    or-int/lit8 v2, v2, 0x1

    .line 8289
    :cond_0
    iget-wide v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->assetId_:J

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->assetId_:J
    invoke-static {v0, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->access$9702(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;J)J

    .line 8290
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 8291
    or-int/lit8 v2, v2, 0x2

    .line 8293
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->assetPath_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->assetPath_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->access$9802(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;Ljava/lang/Object;)Ljava/lang/Object;

    .line 8294
    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_2

    .line 8295
    or-int/lit8 v1, v2, 0x4

    .line 8297
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->type_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->access$9902(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    .line 8298
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->access$10002(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;I)I

    .line 8299
    return-object v0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 8230
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 8230
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;
    .locals 2

    .prologue
    .line 8246
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 8247
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->assetId_:J

    .line 8248
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->bitField0_:I

    .line 8249
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->assetPath_:Ljava/lang/Object;

    .line 8250
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->bitField0_:I

    .line 8251
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->VGOOD_ANIMATION:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    .line 8252
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->bitField0_:I

    .line 8253
    return-object p0
.end method

.method public clearAssetId()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;
    .locals 2

    .prologue
    .line 8377
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->bitField0_:I

    .line 8378
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->assetId_:J

    .line 8380
    return-object p0
.end method

.method public clearAssetPath()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;
    .locals 1

    .prologue
    .line 8408
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->bitField0_:I

    .line 8409
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getAssetPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->assetPath_:Ljava/lang/Object;

    .line 8411
    return-object p0
.end method

.method public clearType()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;
    .locals 1

    .prologue
    .line 8437
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->bitField0_:I

    .line 8438
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->VGOOD_ANIMATION:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    .line 8440
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 8230
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 8230
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 8230
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;
    .locals 2

    .prologue
    .line 8257
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 8230
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAssetId()J
    .locals 2

    .prologue
    .line 8368
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->assetId_:J

    return-wide v0
.end method

.method public getAssetPath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 8389
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->assetPath_:Ljava/lang/Object;

    .line 8390
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 8391
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 8392
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->assetPath_:Ljava/lang/Object;

    .line 8395
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 8230
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 8230
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    .locals 1

    .prologue
    .line 8261
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    return-object v0
.end method

.method public getType()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;
    .locals 1

    .prologue
    .line 8425
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    return-object v0
.end method

.method public hasAssetId()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 8365
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasAssetPath()Z
    .locals 2

    .prologue
    .line 8386
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    .prologue
    .line 8422
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 8317
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8230
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 8230
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8230
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8325
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 8326
    sparse-switch v0, :sswitch_data_0

    .line 8331
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 8333
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 8329
    goto :goto_1

    .line 8338
    :sswitch_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->bitField0_:I

    .line 8339
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->assetId_:J

    goto :goto_0

    .line 8343
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->bitField0_:I

    .line 8344
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->assetPath_:Ljava/lang/Object;

    goto :goto_0

    .line 8348
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 8349
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    move-result-object v0

    .line 8350
    if-eqz v0, :cond_0

    .line 8351
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->bitField0_:I

    .line 8352
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    goto :goto_0

    .line 8326
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;
    .locals 2
    .parameter

    .prologue
    .line 8303
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 8313
    :goto_0
    return-object v0

    .line 8304
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->hasAssetId()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 8305
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getAssetId()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->setAssetId(J)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    .line 8307
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->hasAssetPath()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 8308
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getAssetPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->setAssetPath(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    .line 8310
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->hasType()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 8311
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getType()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    :cond_3
    move-object v0, p0

    .line 8313
    goto :goto_0
.end method

.method public setAssetId(J)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;
    .locals 1
    .parameter

    .prologue
    .line 8371
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->bitField0_:I

    .line 8372
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->assetId_:J

    .line 8374
    return-object p0
.end method

.method public setAssetPath(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;
    .locals 1
    .parameter

    .prologue
    .line 8399
    if-nez p1, :cond_0

    .line 8400
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 8402
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->bitField0_:I

    .line 8403
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->assetPath_:Ljava/lang/Object;

    .line 8405
    return-object p0
.end method

.method setAssetPath(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 8414
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->bitField0_:I

    .line 8415
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->assetPath_:Ljava/lang/Object;

    .line 8417
    return-void
.end method

.method public setType(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;
    .locals 1
    .parameter

    .prologue
    .line 8428
    if-nez p1, :cond_0

    .line 8429
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 8431
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->bitField0_:I

    .line 8432
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    .line 8434
    return-object p0
.end method
