.class public final enum Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type; = null

.field public static final enum AVATAR:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type; = null

.field public static final AVATAR_VALUE:I = 0x1

.field public static final enum FILTER:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type; = null

.field public static final FILTER_VALUE:I = 0x3

.field public static final enum GAME:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type; = null

.field public static final GAME_VALUE:I = 0x2

.field public static final enum VGOOD_ANIMATION:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

.field public static final VGOOD_ANIMATION_VALUE:I

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 8003
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    const-string v1, "VGOOD_ANIMATION"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->VGOOD_ANIMATION:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    .line 8004
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    const-string v1, "AVATAR"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->AVATAR:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    .line 8005
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    const-string v1, "GAME"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->GAME:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    .line 8006
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    const-string v1, "FILTER"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->FILTER:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    .line 8001
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->VGOOD_ANIMATION:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->AVATAR:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->GAME:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->FILTER:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    .line 8032
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 8041
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 8042
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->value:I

    .line 8043
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;",
            ">;"
        }
    .end annotation

    .prologue
    .line 8029
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;
    .locals 1
    .parameter

    .prologue
    .line 8018
    packed-switch p0, :pswitch_data_0

    .line 8023
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 8019
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->VGOOD_ANIMATION:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    goto :goto_0

    .line 8020
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->AVATAR:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    goto :goto_0

    .line 8021
    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->GAME:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    goto :goto_0

    .line 8022
    :pswitch_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->FILTER:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    goto :goto_0

    .line 8018
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;
    .locals 1
    .parameter

    .prologue
    .line 8001
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;
    .locals 1

    .prologue
    .line 8001
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 8015
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->value:I

    return v0
.end method
