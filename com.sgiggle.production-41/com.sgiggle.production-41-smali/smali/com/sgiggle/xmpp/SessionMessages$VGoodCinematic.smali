.class public final Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematicOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VGoodCinematic"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;,
        Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;
    }
.end annotation


# static fields
.field public static final ASSET_ID_FIELD_NUMBER:I = 0x1

.field public static final ASSET_PATH_FIELD_NUMBER:I = 0x2

.field public static final TYPE_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;


# instance fields
.field private assetId_:J

.field private assetPath_:Ljava/lang/Object;

.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private type_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 8447
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    .line 8448
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->initFields()V

    .line 8449
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 7988
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 8106
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->memoizedIsInitialized:B

    .line 8129
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->memoizedSerializedSize:I

    .line 7989
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 7983
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;-><init>(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 7990
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 8106
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->memoizedIsInitialized:B

    .line 8129
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->memoizedSerializedSize:I

    .line 7990
    return-void
.end method

.method static synthetic access$10002(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 7983
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->bitField0_:I

    return p1
.end method

.method static synthetic access$9702(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;J)J
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 7983
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->assetId_:J

    return-wide p1
.end method

.method static synthetic access$9802(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 7983
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->assetPath_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$9902(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 7983
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->type_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    return-object p1
.end method

.method private getAssetPathBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 8080
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->assetPath_:Ljava/lang/Object;

    .line 8081
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 8082
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 8084
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->assetPath_:Ljava/lang/Object;

    .line 8087
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    .locals 1

    .prologue
    .line 7994
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    return-object v0
.end method

.method private initFields()V
    .locals 2

    .prologue
    .line 8102
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->assetId_:J

    .line 8103
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->assetPath_:Ljava/lang/Object;

    .line 8104
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->VGOOD_ANIMATION:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->type_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    .line 8105
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;
    .locals 1

    .prologue
    .line 8223
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->access$9500()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;
    .locals 1
    .parameter

    .prologue
    .line 8226
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8192
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    .line 8193
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8194
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->access$9400(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    .line 8196
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8203
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    .line 8204
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8205
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->access$9400(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    .line 8207
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 8159
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->access$9400(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 8165
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->access$9400(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8213
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->access$9400(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8219
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->access$9400(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8181
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->access$9400(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8187
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->access$9400(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 8170
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->access$9400(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 8176
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;->access$9400(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAssetId()J
    .locals 2

    .prologue
    .line 8056
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->assetId_:J

    return-wide v0
.end method

.method public getAssetPath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 8066
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->assetPath_:Ljava/lang/Object;

    .line 8067
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 8068
    check-cast v0, Ljava/lang/String;

    .line 8076
    :goto_0
    return-object v0

    .line 8070
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 8072
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 8073
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 8074
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->assetPath_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 8076
    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 7983
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;
    .locals 1

    .prologue
    .line 7998
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 8131
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->memoizedSerializedSize:I

    .line 8132
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 8148
    :goto_0
    return v0

    .line 8134
    :cond_0
    const/4 v0, 0x0

    .line 8135
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_1

    .line 8136
    iget-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->assetId_:J

    invoke-static {v3, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 8139
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_2

    .line 8140
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getAssetPathBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8143
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 8144
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->type_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8147
    :cond_3
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getType()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;
    .locals 1

    .prologue
    .line 8098
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->type_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    return-object v0
.end method

.method public hasAssetId()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 8053
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasAssetPath()Z
    .locals 2

    .prologue
    .line 8063
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    .prologue
    .line 8095
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 8108
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->memoizedIsInitialized:B

    .line 8109
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v2, :cond_0

    move v0, v2

    .line 8112
    :goto_0
    return v0

    .line 8109
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 8111
    :cond_1
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->memoizedIsInitialized:B

    move v0, v2

    .line 8112
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 7983
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;
    .locals 1

    .prologue
    .line 8224
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 7983
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;
    .locals 1

    .prologue
    .line 8228
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;)Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 8153
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 8117
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getSerializedSize()I

    .line 8118
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 8119
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->assetId_:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt64(IJ)V

    .line 8121
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 8122
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getAssetPathBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 8124
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 8125
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->type_:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 8127
    :cond_2
    return-void
.end method
