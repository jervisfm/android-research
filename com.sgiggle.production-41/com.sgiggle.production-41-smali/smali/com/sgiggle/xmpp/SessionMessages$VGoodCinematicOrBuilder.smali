.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematicOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "VGoodCinematicOrBuilder"
.end annotation


# virtual methods
.method public abstract getAssetId()J
.end method

.method public abstract getAssetPath()Ljava/lang/String;
.end method

.method public abstract getType()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;
.end method

.method public abstract hasAssetId()Z
.end method

.method public abstract hasAssetPath()Z
.end method

.method public abstract hasType()Z
.end method
