.class public final Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private error_:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

.field private extra_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;->TEST_TS_ERROR:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->error_:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->extra_:Ljava/lang/Object;

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->maybeForceBuilderInitialization()V

    return-void
.end method

.method static synthetic access$115400(Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$115500()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;
    .locals 1

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;
    .locals 2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;
    .locals 5

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    or-int/lit8 v2, v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;->access$115702(Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->error_:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;->error_:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;->access$115802(Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;)Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_2

    or-int/lit8 v1, v2, 0x4

    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->extra_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;->extra_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;->access$115902(Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;Ljava/lang/Object;)Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;->access$116002(Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;I)I

    return-object v0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;
    .locals 1

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;->TEST_TS_ERROR:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->error_:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->extra_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearError()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;->TEST_TS_ERROR:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->error_:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    return-object p0
.end method

.method public clearExtra()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;->getExtra()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->extra_:Ljava/lang/Object;

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;)Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;

    move-result-object v0

    return-object v0
.end method

.method public getError()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->error_:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    return-object v0
.end method

.method public getExtra()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->extra_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->extra_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasError()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasExtra()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->hasError()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->hasExtra()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;)Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    goto :goto_1

    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->error_:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    goto :goto_0

    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->extra_:Ljava/lang/Object;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;)Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;

    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;->hasError()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;->getError()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->setError(Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;)Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;->hasExtra()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;->getExtra()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->setExtra(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;

    :cond_3
    move-object v0, p0

    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setError(Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;)Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->error_:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    return-object p0
.end method

.method public setExtra(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->extra_:Ljava/lang/Object;

    return-object p0
.end method

.method setExtra(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Builder;->extra_:Ljava/lang/Object;

    return-void
.end method
