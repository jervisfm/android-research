.class public final enum Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Error"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error; = null

.field public static final enum LOCAL_FAILED:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error; = null

.field public static final LOCAL_FAILED_VALUE:I = 0x4

.field public static final enum LOCAL_TIMEOUT:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error; = null

.field public static final LOCAL_TIMEOUT_VALUE:I = 0x5

.field public static final enum REMOTE_FAILED:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error; = null

.field public static final REMOTE_FAILED_VALUE:I = 0x2

.field public static final enum REMOTE_TIMEOUT:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error; = null

.field public static final REMOTE_TIMEOUT_VALUE:I = 0x3

.field public static final enum TEST_TS_ERROR:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error; = null

.field public static final TEST_TS_ERROR_VALUE:I = 0x0

.field public static final enum UNSUPPORDED_BY_REMOTE:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error; = null

.field public static final UNSUPPORDED_BY_REMOTE_VALUE:I = 0x1

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    const-string v1, "TEST_TS_ERROR"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;->TEST_TS_ERROR:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    const-string v1, "UNSUPPORDED_BY_REMOTE"

    invoke-direct {v0, v1, v6, v6, v6}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;->UNSUPPORDED_BY_REMOTE:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    const-string v1, "REMOTE_FAILED"

    invoke-direct {v0, v1, v7, v7, v7}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;->REMOTE_FAILED:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    const-string v1, "REMOTE_TIMEOUT"

    invoke-direct {v0, v1, v8, v8, v8}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;->REMOTE_TIMEOUT:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    const-string v1, "LOCAL_FAILED"

    invoke-direct {v0, v1, v9, v9, v9}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;->LOCAL_FAILED:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    const-string v1, "LOCAL_TIMEOUT"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const/4 v4, 0x5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;->LOCAL_TIMEOUT:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;->TEST_TS_ERROR:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;->UNSUPPORDED_BY_REMOTE:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;->REMOTE_FAILED:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;->REMOTE_TIMEOUT:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    aput-object v1, v0, v8

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;->LOCAL_FAILED:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;->LOCAL_TIMEOUT:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;->value:I

    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;
    .locals 1

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;->TEST_TS_ERROR:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;->UNSUPPORDED_BY_REMOTE:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;->REMOTE_FAILED:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;->REMOTE_TIMEOUT:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    goto :goto_0

    :pswitch_4
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;->LOCAL_FAILED:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    goto :goto_0

    :pswitch_5
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;->LOCAL_TIMEOUT:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;
    .locals 1

    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;->value:I

    return v0
.end method
