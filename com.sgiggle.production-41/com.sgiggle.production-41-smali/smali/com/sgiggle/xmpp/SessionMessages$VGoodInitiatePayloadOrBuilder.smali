.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$VGoodInitiatePayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "VGoodInitiatePayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getProductMarketId()Ljava/lang/String;
.end method

.method public abstract getVgoodId()J
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasProductMarketId()Z
.end method

.method public abstract hasVgoodId()Z
.end method
