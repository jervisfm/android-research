.class public final Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImageOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;",
        "Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImageOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private imageType_:Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;

.field private path_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 7784
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 7898
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;->enabled:Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->imageType_:Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;

    .line 7922
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->path_:Ljava/lang/Object;

    .line 7785
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->maybeForceBuilderInitialization()V

    .line 7786
    return-void
.end method

.method static synthetic access$8800(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 7779
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$8900()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;
    .locals 1

    .prologue
    .line 7779
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 7821
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    move-result-object v0

    .line 7822
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 7823
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 7826
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;
    .locals 1

    .prologue
    .line 7791
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 7789
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 7779
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
    .locals 2

    .prologue
    .line 7812
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    move-result-object v0

    .line 7813
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 7814
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 7816
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 7779
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
    .locals 5

    .prologue
    .line 7830
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 7831
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->bitField0_:I

    .line 7832
    const/4 v2, 0x0

    .line 7833
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 7834
    or-int/lit8 v2, v2, 0x1

    .line 7836
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->imageType_:Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->imageType_:Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->access$9102(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;

    .line 7837
    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    .line 7838
    or-int/lit8 v1, v2, 0x2

    .line 7840
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->path_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->path_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->access$9202(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7841
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->access$9302(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;I)I

    .line 7842
    return-object v0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 7779
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 7779
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;
    .locals 1

    .prologue
    .line 7795
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 7796
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;->enabled:Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->imageType_:Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;

    .line 7797
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->bitField0_:I

    .line 7798
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->path_:Ljava/lang/Object;

    .line 7799
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->bitField0_:I

    .line 7800
    return-object p0
.end method

.method public clearImageType()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;
    .locals 1

    .prologue
    .line 7915
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->bitField0_:I

    .line 7916
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;->enabled:Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->imageType_:Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;

    .line 7918
    return-object p0
.end method

.method public clearPath()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;
    .locals 1

    .prologue
    .line 7946
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->bitField0_:I

    .line 7947
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->path_:Ljava/lang/Object;

    .line 7949
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 7779
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 7779
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 7779
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;
    .locals 2

    .prologue
    .line 7804
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 7779
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 7779
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 7779
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
    .locals 1

    .prologue
    .line 7808
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    move-result-object v0

    return-object v0
.end method

.method public getImageType()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;
    .locals 1

    .prologue
    .line 7903
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->imageType_:Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 7927
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->path_:Ljava/lang/Object;

    .line 7928
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 7929
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 7930
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->path_:Ljava/lang/Object;

    .line 7933
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasImageType()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 7900
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPath()Z
    .locals 2

    .prologue
    .line 7924
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 7857
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7779
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 7779
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7779
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7865
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 7866
    sparse-switch v0, :sswitch_data_0

    .line 7871
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 7873
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 7869
    goto :goto_1

    .line 7878
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 7879
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;

    move-result-object v0

    .line 7880
    if-eqz v0, :cond_0

    .line 7881
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->bitField0_:I

    .line 7882
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->imageType_:Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;

    goto :goto_0

    .line 7887
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->bitField0_:I

    .line 7888
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->path_:Ljava/lang/Object;

    goto :goto_0

    .line 7866
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;
    .locals 1
    .parameter

    .prologue
    .line 7846
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 7853
    :goto_0
    return-object v0

    .line 7847
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->hasImageType()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 7848
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->getImageType()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->setImageType(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    .line 7850
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->hasPath()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 7851
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->setPath(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    :cond_2
    move-object v0, p0

    .line 7853
    goto :goto_0
.end method

.method public setImageType(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;
    .locals 1
    .parameter

    .prologue
    .line 7906
    if-nez p1, :cond_0

    .line 7907
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7909
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->bitField0_:I

    .line 7910
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->imageType_:Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;

    .line 7912
    return-object p0
.end method

.method public setPath(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;
    .locals 1
    .parameter

    .prologue
    .line 7937
    if-nez p1, :cond_0

    .line 7938
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7940
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->bitField0_:I

    .line 7941
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->path_:Ljava/lang/Object;

    .line 7943
    return-object p0
.end method

.method setPath(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 7952
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->bitField0_:I

    .line 7953
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->path_:Ljava/lang/Object;

    .line 7955
    return-void
.end method
