.class public final Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImageOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VGoodSelectorImage"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;,
        Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;
    }
.end annotation


# static fields
.field public static final IMAGE_TYPE_FIELD_NUMBER:I = 0x1

.field public static final PATH_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;


# instance fields
.field private bitField0_:I

.field private imageType_:Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private path_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 7961
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    .line 7962
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->initFields()V

    .line 7963
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 7555
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 7662
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->memoizedIsInitialized:B

    .line 7682
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->memoizedSerializedSize:I

    .line 7556
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 7550
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 7557
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 7662
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->memoizedIsInitialized:B

    .line 7682
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->memoizedSerializedSize:I

    .line 7557
    return-void
.end method

.method static synthetic access$9102(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 7550
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->imageType_:Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;

    return-object p1
.end method

.method static synthetic access$9202(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 7550
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->path_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$9302(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 7550
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
    .locals 1

    .prologue
    .line 7561
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    return-object v0
.end method

.method private getPathBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 7647
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->path_:Ljava/lang/Object;

    .line 7648
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 7649
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 7651
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->path_:Ljava/lang/Object;

    .line 7654
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 7659
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;->enabled:Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->imageType_:Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;

    .line 7660
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->path_:Ljava/lang/Object;

    .line 7661
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;
    .locals 1

    .prologue
    .line 7772
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->access$8900()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;
    .locals 1
    .parameter

    .prologue
    .line 7775
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7741
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    .line 7742
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7743
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->access$8800(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    move-result-object v0

    .line 7745
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7752
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    .line 7753
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7754
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->access$8800(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    move-result-object v0

    .line 7756
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 7708
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->access$8800(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 7714
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->access$8800(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7762
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->access$8800(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7768
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->access$8800(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7730
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->access$8800(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 7736
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->access$8800(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 7719
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->access$8800(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 7725
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;->access$8800(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 7550
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;
    .locals 1

    .prologue
    .line 7565
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    return-object v0
.end method

.method public getImageType()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;
    .locals 1

    .prologue
    .line 7623
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->imageType_:Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 7633
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->path_:Ljava/lang/Object;

    .line 7634
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 7635
    check-cast v0, Ljava/lang/String;

    .line 7643
    :goto_0
    return-object v0

    .line 7637
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 7639
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 7640
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 7641
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->path_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 7643
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 7684
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->memoizedSerializedSize:I

    .line 7685
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 7697
    :goto_0
    return v0

    .line 7687
    :cond_0
    const/4 v0, 0x0

    .line 7688
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 7689
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->imageType_:Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;->getNumber()I

    move-result v1

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7692
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 7693
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->getPathBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7696
    :cond_2
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public hasImageType()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 7620
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPath()Z
    .locals 2

    .prologue
    .line 7630
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 7664
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->memoizedIsInitialized:B

    .line 7665
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v2, :cond_0

    move v0, v2

    .line 7668
    :goto_0
    return v0

    .line 7665
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 7667
    :cond_1
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->memoizedIsInitialized:B

    move v0, v2

    .line 7668
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 7550
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;
    .locals 1

    .prologue
    .line 7773
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 7550
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;
    .locals 1

    .prologue
    .line 7777
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 7702
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 7673
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->getSerializedSize()I

    .line 7674
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 7675
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->imageType_:Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;->getNumber()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 7677
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 7678
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->getPathBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 7680
    :cond_1
    return-void
.end method
