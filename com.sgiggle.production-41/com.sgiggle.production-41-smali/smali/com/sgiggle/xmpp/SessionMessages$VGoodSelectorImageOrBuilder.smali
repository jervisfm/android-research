.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImageOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "VGoodSelectorImageOrBuilder"
.end annotation


# virtual methods
.method public abstract getImageType()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;
.end method

.method public abstract getPath()Ljava/lang/String;
.end method

.method public abstract hasImageType()Z
.end method

.method public abstract hasPath()Z
.end method
