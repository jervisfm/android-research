.class public final enum Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "VGoodSupportType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType; = null

.field public static final enum VGOOD_DOWNLOADING:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType; = null

.field public static final VGOOD_DOWNLOADING_VALUE:I = 0x3

.field public static final enum VGOOD_INVALID_STATE:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType; = null

.field public static final VGOOD_INVALID_STATE_VALUE:I = 0x4

.field public static final enum VGOOD_UNSUPPORTED_CLIENT:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType; = null

.field public static final VGOOD_UNSUPPORTED_CLIENT_VALUE:I = 0x1

.field public static final enum VGOOD_UNSUPPORTED_PLATFORM:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType; = null

.field public static final VGOOD_UNSUPPORTED_PLATFORM_VALUE:I = 0x2

.field public static final enum VGOOD_YES:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

.field public static final VGOOD_YES_VALUE:I

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 395
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    const-string v1, "VGOOD_YES"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->VGOOD_YES:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    .line 396
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    const-string v1, "VGOOD_UNSUPPORTED_CLIENT"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->VGOOD_UNSUPPORTED_CLIENT:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    .line 397
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    const-string v1, "VGOOD_UNSUPPORTED_PLATFORM"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->VGOOD_UNSUPPORTED_PLATFORM:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    .line 398
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    const-string v1, "VGOOD_DOWNLOADING"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->VGOOD_DOWNLOADING:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    .line 399
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    const-string v1, "VGOOD_INVALID_STATE"

    invoke-direct {v0, v1, v6, v6, v6}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->VGOOD_INVALID_STATE:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    .line 393
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->VGOOD_YES:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->VGOOD_UNSUPPORTED_CLIENT:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->VGOOD_UNSUPPORTED_PLATFORM:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->VGOOD_DOWNLOADING:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->VGOOD_INVALID_STATE:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    .line 427
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 436
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 437
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->value:I

    .line 438
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 424
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;
    .locals 1
    .parameter

    .prologue
    .line 412
    packed-switch p0, :pswitch_data_0

    .line 418
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 413
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->VGOOD_YES:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    goto :goto_0

    .line 414
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->VGOOD_UNSUPPORTED_CLIENT:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    goto :goto_0

    .line 415
    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->VGOOD_UNSUPPORTED_PLATFORM:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    goto :goto_0

    .line 416
    :pswitch_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->VGOOD_DOWNLOADING:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    goto :goto_0

    .line 417
    :pswitch_4
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->VGOOD_INVALID_STATE:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    goto :goto_0

    .line 412
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;
    .locals 1
    .parameter

    .prologue
    .line 393
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;
    .locals 1

    .prologue
    .line 393
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 409
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->value:I

    return v0
.end method
