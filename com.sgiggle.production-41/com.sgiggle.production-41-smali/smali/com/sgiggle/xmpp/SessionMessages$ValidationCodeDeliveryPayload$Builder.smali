.class public final Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private method_:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 52881
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 53011
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 53054
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;->ANY:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->method_:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    .line 52882
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->maybeForceBuilderInitialization()V

    .line 52883
    return-void
.end method

.method static synthetic access$67800(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 52876
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$67900()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;
    .locals 1

    .prologue
    .line 52876
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 52918
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;

    move-result-object v0

    .line 52919
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 52920
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 52923
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;
    .locals 1

    .prologue
    .line 52888
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 52886
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 52876
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;
    .locals 2

    .prologue
    .line 52909
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;

    move-result-object v0

    .line 52910
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 52911
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 52913
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 52876
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;
    .locals 5

    .prologue
    .line 52927
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 52928
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->bitField0_:I

    .line 52929
    const/4 v2, 0x0

    .line 52930
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 52931
    or-int/lit8 v2, v2, 0x1

    .line 52933
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->access$68102(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 52934
    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    .line 52935
    or-int/lit8 v1, v2, 0x2

    .line 52937
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->method_:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->method_:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->access$68202(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    .line 52938
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->access$68302(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;I)I

    .line 52939
    return-object v0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 52876
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 52876
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;
    .locals 1

    .prologue
    .line 52892
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 52893
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 52894
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->bitField0_:I

    .line 52895
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;->ANY:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->method_:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    .line 52896
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->bitField0_:I

    .line 52897
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;
    .locals 1

    .prologue
    .line 53047
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 53049
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->bitField0_:I

    .line 53050
    return-object p0
.end method

.method public clearMethod()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;
    .locals 1

    .prologue
    .line 53071
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->bitField0_:I

    .line 53072
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;->ANY:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->method_:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    .line 53074
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 52876
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 52876
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 52876
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;
    .locals 2

    .prologue
    .line 52901
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 52876
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 53016
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 52876
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 52876
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;
    .locals 1

    .prologue
    .line 52905
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;

    move-result-object v0

    return-object v0
.end method

.method public getMethod()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;
    .locals 1

    .prologue
    .line 53059
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->method_:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 53013
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMethod()Z
    .locals 2

    .prologue
    .line 53056
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52954
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 52966
    :goto_0
    return v0

    .line 52958
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->hasMethod()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 52960
    goto :goto_0

    .line 52962
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 52964
    goto :goto_0

    .line 52966
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 53035
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 53037
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 53043
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->bitField0_:I

    .line 53044
    return-object p0

    .line 53040
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52876
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 52876
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52876
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52974
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 52975
    sparse-switch v0, :sswitch_data_0

    .line 52980
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 52982
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 52978
    goto :goto_1

    .line 52987
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 52988
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 52989
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 52991
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 52992
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    goto :goto_0

    .line 52996
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 52997
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    move-result-object v0

    .line 52998
    if-eqz v0, :cond_0

    .line 52999
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->bitField0_:I

    .line 53000
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->method_:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    goto :goto_0

    .line 52975
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 52943
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 52950
    :goto_0
    return-object v0

    .line 52944
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52945
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    .line 52947
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->hasMethod()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 52948
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->getMethod()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->setMethod(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    :cond_2
    move-object v0, p0

    .line 52950
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 53029
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 53031
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->bitField0_:I

    .line 53032
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 53019
    if-nez p1, :cond_0

    .line 53020
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 53022
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 53024
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->bitField0_:I

    .line 53025
    return-object p0
.end method

.method public setMethod(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 53062
    if-nez p1, :cond_0

    .line 53063
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 53065
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->bitField0_:I

    .line 53066
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->method_:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    .line 53068
    return-object p0
.end method
