.class public final enum Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ValidationCodeDeliveryType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType; = null

.field public static final enum ANY:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType; = null

.field public static final ANY_VALUE:I = 0x4

.field public static final enum APPLE_PUSH:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType; = null

.field public static final APPLE_PUSH_VALUE:I = 0x2

.field public static final enum GOOGLE_PUSH:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType; = null

.field public static final GOOGLE_PUSH_VALUE:I = 0x3

.field public static final enum SMS:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType; = null

.field public static final SMS_VALUE:I = 0x0

.field public static final enum TANGO_PUSH:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType; = null

.field public static final TANGO_PUSH_VALUE:I = 0x1

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 52674
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    const-string v1, "SMS"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;->SMS:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    .line 52675
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    const-string v1, "TANGO_PUSH"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;->TANGO_PUSH:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    .line 52676
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    const-string v1, "APPLE_PUSH"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;->APPLE_PUSH:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    .line 52677
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    const-string v1, "GOOGLE_PUSH"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;->GOOGLE_PUSH:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    .line 52678
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    const-string v1, "ANY"

    invoke-direct {v0, v1, v6, v6, v6}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;->ANY:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    .line 52672
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;->SMS:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;->TANGO_PUSH:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;->APPLE_PUSH:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;->GOOGLE_PUSH:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;->ANY:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    .line 52706
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 52715
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 52716
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;->value:I

    .line 52717
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52703
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;
    .locals 1
    .parameter

    .prologue
    .line 52691
    packed-switch p0, :pswitch_data_0

    .line 52697
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 52692
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;->SMS:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    goto :goto_0

    .line 52693
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;->TANGO_PUSH:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    goto :goto_0

    .line 52694
    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;->APPLE_PUSH:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    goto :goto_0

    .line 52695
    :pswitch_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;->GOOGLE_PUSH:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    goto :goto_0

    .line 52696
    :pswitch_4
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;->ANY:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    goto :goto_0

    .line 52691
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;
    .locals 1
    .parameter

    .prologue
    .line 52672
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;
    .locals 1

    .prologue
    .line 52672
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 52688
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;->value:I

    return v0
.end method
