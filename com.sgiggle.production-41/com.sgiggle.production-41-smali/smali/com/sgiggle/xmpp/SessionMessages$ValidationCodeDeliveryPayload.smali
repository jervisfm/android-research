.class public final Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ValidationCodeDeliveryPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;,
        Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final METHOD_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private method_:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 53081
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;

    .line 53082
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->initFields()V

    .line 53083
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 52659
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 52747
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->memoizedIsInitialized:B

    .line 52779
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->memoizedSerializedSize:I

    .line 52660
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 52654
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 52661
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 52747
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->memoizedIsInitialized:B

    .line 52779
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->memoizedSerializedSize:I

    .line 52661
    return-void
.end method

.method static synthetic access$68102(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 52654
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$68202(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 52654
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->method_:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    return-object p1
.end method

.method static synthetic access$68302(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 52654
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;
    .locals 1

    .prologue
    .line 52665
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 52744
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 52745
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;->ANY:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->method_:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    .line 52746
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;
    .locals 1

    .prologue
    .line 52869
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->access$67900()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 52872
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52838
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    move-result-object v0

    .line 52839
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 52840
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->access$67800(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;

    move-result-object v0

    .line 52842
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52849
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    move-result-object v0

    .line 52850
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 52851
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->access$67800(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;

    move-result-object v0

    .line 52853
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 52805
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->access$67800(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 52811
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->access$67800(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52859
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->access$67800(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52865
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->access$67800(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52827
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->access$67800(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52833
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->access$67800(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 52816
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->access$67800(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 52822
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;->access$67800(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 52730
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 52654
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;
    .locals 1

    .prologue
    .line 52669
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;

    return-object v0
.end method

.method public getMethod()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;
    .locals 1

    .prologue
    .line 52740
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->method_:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 52781
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->memoizedSerializedSize:I

    .line 52782
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 52794
    :goto_0
    return v0

    .line 52784
    :cond_0
    const/4 v0, 0x0

    .line 52785
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 52786
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 52789
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 52790
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->method_:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;->getNumber()I

    move-result v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 52793
    :cond_2
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 52727
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMethod()Z
    .locals 2

    .prologue
    .line 52737
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 52749
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->memoizedIsInitialized:B

    .line 52750
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 52765
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 52750
    goto :goto_0

    .line 52752
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 52753
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 52754
    goto :goto_0

    .line 52756
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->hasMethod()Z

    move-result v0

    if-nez v0, :cond_3

    .line 52757
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 52758
    goto :goto_0

    .line 52760
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    .line 52761
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 52762
    goto :goto_0

    .line 52764
    :cond_4
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 52765
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 52654
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;
    .locals 1

    .prologue
    .line 52870
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 52654
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;
    .locals 1

    .prologue
    .line 52874
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 52799
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 52770
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->getSerializedSize()I

    .line 52771
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 52772
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 52774
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 52775
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload;->method_:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;->getNumber()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 52777
    :cond_1
    return-void
.end method
