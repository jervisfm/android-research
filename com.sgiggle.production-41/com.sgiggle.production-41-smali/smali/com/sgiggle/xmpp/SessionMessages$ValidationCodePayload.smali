.class public final Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ValidationCodePayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CODE_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private code_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 51435
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;

    .line 51436
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->initFields()V

    .line 51437
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 51033
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 51093
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->memoizedIsInitialized:B

    .line 51125
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->memoizedSerializedSize:I

    .line 51034
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 51028
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 51035
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 51093
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->memoizedIsInitialized:B

    .line 51125
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->memoizedSerializedSize:I

    .line 51035
    return-void
.end method

.method static synthetic access$65702(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 51028
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$65802(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 51028
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->code_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$65902(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 51028
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->bitField0_:I

    return p1
.end method

.method private getCodeBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 51078
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->code_:Ljava/lang/Object;

    .line 51079
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 51080
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 51082
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->code_:Ljava/lang/Object;

    .line 51085
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;
    .locals 1

    .prologue
    .line 51039
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 51090
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 51091
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->code_:Ljava/lang/Object;

    .line 51092
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;
    .locals 1

    .prologue
    .line 51215
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->access$65500()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 51218
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51184
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;

    move-result-object v0

    .line 51185
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 51186
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->access$65400(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;

    move-result-object v0

    .line 51188
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51195
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;

    move-result-object v0

    .line 51196
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 51197
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->access$65400(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;

    move-result-object v0

    .line 51199
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 51151
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->access$65400(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 51157
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->access$65400(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51205
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->access$65400(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51211
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->access$65400(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51173
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->access$65400(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51179
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->access$65400(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 51162
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->access$65400(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 51168
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;->access$65400(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 51054
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCode()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51064
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->code_:Ljava/lang/Object;

    .line 51065
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 51066
    check-cast v0, Ljava/lang/String;

    .line 51074
    :goto_0
    return-object v0

    .line 51068
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 51070
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 51071
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 51072
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->code_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 51074
    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 51028
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;
    .locals 1

    .prologue
    .line 51043
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 51127
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->memoizedSerializedSize:I

    .line 51128
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 51140
    :goto_0
    return v0

    .line 51130
    :cond_0
    const/4 v0, 0x0

    .line 51131
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 51132
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51135
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 51136
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->getCodeBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51139
    :cond_2
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 51051
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCode()Z
    .locals 2

    .prologue
    .line 51061
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 51095
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->memoizedIsInitialized:B

    .line 51096
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 51111
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 51096
    goto :goto_0

    .line 51098
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 51099
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 51100
    goto :goto_0

    .line 51102
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->hasCode()Z

    move-result v0

    if-nez v0, :cond_3

    .line 51103
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 51104
    goto :goto_0

    .line 51106
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    .line 51107
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 51108
    goto :goto_0

    .line 51110
    :cond_4
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->memoizedIsInitialized:B

    move v0, v3

    .line 51111
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 51028
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;
    .locals 1

    .prologue
    .line 51216
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 51028
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;
    .locals 1

    .prologue
    .line 51220
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 51145
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 51116
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->getSerializedSize()I

    .line 51117
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 51118
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 51120
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 51121
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodePayload;->getCodeBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 51123
    :cond_1
    return-void
.end method
