.class public final Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private type_:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 50817
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 50943
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 50818
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->maybeForceBuilderInitialization()V

    .line 50819
    return-void
.end method

.method static synthetic access$64800(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 50812
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$64900()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;
    .locals 1

    .prologue
    .line 50812
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 50854
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;

    move-result-object v0

    .line 50855
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 50856
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 50859
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;
    .locals 1

    .prologue
    .line 50824
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 50822
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 50812
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;
    .locals 2

    .prologue
    .line 50845
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;

    move-result-object v0

    .line 50846
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 50847
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 50849
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 50812
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;
    .locals 5

    .prologue
    .line 50863
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 50864
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->bitField0_:I

    .line 50865
    const/4 v2, 0x0

    .line 50866
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 50867
    or-int/lit8 v2, v2, 0x1

    .line 50869
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->access$65102(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 50870
    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    .line 50871
    or-int/lit8 v1, v2, 0x2

    .line 50873
    :goto_0
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->type_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->type_:I
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->access$65202(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;I)I

    .line 50874
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->access$65302(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;I)I

    .line 50875
    return-object v0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 50812
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 50812
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;
    .locals 1

    .prologue
    .line 50828
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 50829
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 50830
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->bitField0_:I

    .line 50831
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->type_:I

    .line 50832
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->bitField0_:I

    .line 50833
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;
    .locals 1

    .prologue
    .line 50979
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 50981
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->bitField0_:I

    .line 50982
    return-object p0
.end method

.method public clearType()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;
    .locals 1

    .prologue
    .line 51000
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->bitField0_:I

    .line 51001
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->type_:I

    .line 51003
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 50812
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 50812
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 50812
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;
    .locals 2

    .prologue
    .line 50837
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 50812
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 50948
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 50812
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 50812
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;
    .locals 1

    .prologue
    .line 50841
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 50991
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->type_:I

    return v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 50945
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    .prologue
    .line 50988
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 50890
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 50902
    :goto_0
    return v0

    .line 50894
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->hasType()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 50896
    goto :goto_0

    .line 50898
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 50900
    goto :goto_0

    .line 50902
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 50967
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 50969
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 50975
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->bitField0_:I

    .line 50976
    return-object p0

    .line 50972
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50812
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 50812
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50812
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50910
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 50911
    sparse-switch v0, :sswitch_data_0

    .line 50916
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 50918
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 50914
    goto :goto_1

    .line 50923
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 50924
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 50925
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 50927
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 50928
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    goto :goto_0

    .line 50932
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->bitField0_:I

    .line 50933
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->type_:I

    goto :goto_0

    .line 50911
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 50879
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 50886
    :goto_0
    return-object v0

    .line 50880
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 50881
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    .line 50883
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->hasType()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 50884
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->getType()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->setType(I)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    :cond_2
    move-object v0, p0

    .line 50886
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 50961
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 50963
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->bitField0_:I

    .line 50964
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 50951
    if-nez p1, :cond_0

    .line 50952
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 50954
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 50956
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->bitField0_:I

    .line 50957
    return-object p0
.end method

.method public setType(I)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 50994
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->bitField0_:I

    .line 50995
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->type_:I

    .line 50997
    return-object p0
.end method
