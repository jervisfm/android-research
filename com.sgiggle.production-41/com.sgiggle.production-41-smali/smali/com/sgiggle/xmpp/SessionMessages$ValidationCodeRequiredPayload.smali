.class public final Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ValidationCodeRequiredPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final TYPE_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private type_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 51010
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;

    .line 51011
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->initFields()V

    .line 51012
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 50645
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 50683
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->memoizedIsInitialized:B

    .line 50715
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->memoizedSerializedSize:I

    .line 50646
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 50640
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 50647
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 50683
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->memoizedIsInitialized:B

    .line 50715
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->memoizedSerializedSize:I

    .line 50647
    return-void
.end method

.method static synthetic access$65102(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 50640
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$65202(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 50640
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->type_:I

    return p1
.end method

.method static synthetic access$65302(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 50640
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;
    .locals 1

    .prologue
    .line 50651
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 50680
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 50681
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->type_:I

    .line 50682
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;
    .locals 1

    .prologue
    .line 50805
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->access$64900()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 50808
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50774
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    move-result-object v0

    .line 50775
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 50776
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->access$64800(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;

    move-result-object v0

    .line 50778
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50785
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    move-result-object v0

    .line 50786
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 50787
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->access$64800(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;

    move-result-object v0

    .line 50789
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 50741
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->access$64800(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 50747
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->access$64800(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50795
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->access$64800(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50801
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->access$64800(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50763
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->access$64800(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50769
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->access$64800(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 50752
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->access$64800(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 50758
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;->access$64800(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 50666
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 50640
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;
    .locals 1

    .prologue
    .line 50655
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 50717
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->memoizedSerializedSize:I

    .line 50718
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 50730
    :goto_0
    return v0

    .line 50720
    :cond_0
    const/4 v0, 0x0

    .line 50721
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 50722
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 50725
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 50726
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->type_:I

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 50729
    :cond_2
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 50676
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->type_:I

    return v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 50663
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    .prologue
    .line 50673
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 50685
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->memoizedIsInitialized:B

    .line 50686
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 50701
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 50686
    goto :goto_0

    .line 50688
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 50689
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 50690
    goto :goto_0

    .line 50692
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->hasType()Z

    move-result v0

    if-nez v0, :cond_3

    .line 50693
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 50694
    goto :goto_0

    .line 50696
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    .line 50697
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 50698
    goto :goto_0

    .line 50700
    :cond_4
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 50701
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 50640
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;
    .locals 1

    .prologue
    .line 50806
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 50640
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;
    .locals 1

    .prologue
    .line 50810
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;)Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 50735
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 50706
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->getSerializedSize()I

    .line 50707
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 50708
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 50710
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 50711
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeRequiredPayload;->type_:I

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 50713
    :cond_1
    return-void
.end method
