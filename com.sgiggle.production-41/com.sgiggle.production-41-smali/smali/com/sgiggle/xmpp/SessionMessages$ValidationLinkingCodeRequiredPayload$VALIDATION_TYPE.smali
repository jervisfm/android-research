.class public final enum Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "VALIDATION_TYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE; = null

.field public static final enum ANY:Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE; = null

.field public static final ANY_VALUE:I = 0x3

.field public static final enum EMAIL:Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE; = null

.field public static final EMAIL_VALUE:I = 0x1

.field public static final enum PUSH:Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE; = null

.field public static final PUSH_VALUE:I = 0x2

.field public static final enum SMS:Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;

.field public static final SMS_VALUE:I

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;

    const-string v1, "SMS"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;->SMS:Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;

    const-string v1, "EMAIL"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;->EMAIL:Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;

    const-string v1, "PUSH"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;->PUSH:Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;

    const-string v1, "ANY"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;->ANY:Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;->SMS:Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;->EMAIL:Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;->PUSH:Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;->ANY:Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;->value:I

    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;
    .locals 1

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;->SMS:Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;->EMAIL:Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;->PUSH:Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;->ANY:Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;
    .locals 1

    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;->value:I

    return v0
.end method
