.class public final Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ValidationLinkingCodeRequiredPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;,
        Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CODE_TYPE_FIELD_NUMBER:I = 0x2

.field public static final VAL_TYPE_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private codeType_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private valType_:Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->initFields()V

    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$121202(Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$121302(Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->codeType_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$121402(Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;)Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->valType_:Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;

    return-object p1
.end method

.method static synthetic access$121502(Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->bitField0_:I

    return p1
.end method

.method private getCodeTypeBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->codeType_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->codeType_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->codeType_:Ljava/lang/Object;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;->SMS:Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->valType_:Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;

    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;
    .locals 1

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->access$121000()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;)Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;)Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->access$120900(Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->access$120900(Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->access$120900(Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->access$120900(Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->access$120900(Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->access$120900(Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->access$120900(Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->access$120900(Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->access$120900(Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;->access$120900(Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCodeType()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->codeType_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->codeType_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->getCodeTypeBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->valType_:Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getValType()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->valType_:Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCodeType()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasValType()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->memoizedIsInitialized:B

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->hasCodeType()Z

    move-result v0

    if-nez v0, :cond_3

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->hasValType()Z

    move-result v0

    if-nez v0, :cond_4

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_5

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_5
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->memoizedIsInitialized:B

    move v0, v3

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;
    .locals 1

    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;)Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->getSerializedSize()I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->getCodeTypeBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload;->valType_:Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationLinkingCodeRequiredPayload$VALIDATION_TYPE;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_2
    return-void
.end method
