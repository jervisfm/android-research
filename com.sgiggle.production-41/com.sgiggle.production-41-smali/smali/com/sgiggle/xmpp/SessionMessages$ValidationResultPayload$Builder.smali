.class public final Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private accountid_:Ljava/lang/Object;

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private result_:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 37036
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 37184
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 37227
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->NONE:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->result_:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    .line 37251
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->accountid_:Ljava/lang/Object;

    .line 37037
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->maybeForceBuilderInitialization()V

    .line 37038
    return-void
.end method

.method static synthetic access$46900(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 37031
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$47000()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;
    .locals 1

    .prologue
    .line 37031
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 37075
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    move-result-object v0

    .line 37076
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 37077
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 37080
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;
    .locals 1

    .prologue
    .line 37043
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 37041
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 37031
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;
    .locals 2

    .prologue
    .line 37066
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    move-result-object v0

    .line 37067
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 37068
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 37070
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 37031
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;
    .locals 5

    .prologue
    .line 37084
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 37085
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    .line 37086
    const/4 v2, 0x0

    .line 37087
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 37088
    or-int/lit8 v2, v2, 0x1

    .line 37090
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->access$47202(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 37091
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 37092
    or-int/lit8 v2, v2, 0x2

    .line 37094
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->result_:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->result_:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->access$47302(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    .line 37095
    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_2

    .line 37096
    or-int/lit8 v1, v2, 0x4

    .line 37098
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->accountid_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->accountid_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->access$47402(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37099
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->access$47502(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;I)I

    .line 37100
    return-object v0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 37031
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 37031
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;
    .locals 1

    .prologue
    .line 37047
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 37048
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 37049
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    .line 37050
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->NONE:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->result_:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    .line 37051
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    .line 37052
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->accountid_:Ljava/lang/Object;

    .line 37053
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    .line 37054
    return-object p0
.end method

.method public clearAccountid()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;
    .locals 1

    .prologue
    .line 37275
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    .line 37276
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->getAccountid()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->accountid_:Ljava/lang/Object;

    .line 37278
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;
    .locals 1

    .prologue
    .line 37220
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 37222
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    .line 37223
    return-object p0
.end method

.method public clearResult()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;
    .locals 1

    .prologue
    .line 37244
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    .line 37245
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->NONE:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->result_:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    .line 37247
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 37031
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 37031
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 37031
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;
    .locals 2

    .prologue
    .line 37058
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 37031
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAccountid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 37256
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->accountid_:Ljava/lang/Object;

    .line 37257
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 37258
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 37259
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->accountid_:Ljava/lang/Object;

    .line 37262
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 37189
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 37031
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 37031
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;
    .locals 1

    .prologue
    .line 37062
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public getResult()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;
    .locals 1

    .prologue
    .line 37232
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->result_:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    return-object v0
.end method

.method public hasAccountid()Z
    .locals 2

    .prologue
    .line 37253
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 37186
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasResult()Z
    .locals 2

    .prologue
    .line 37229
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37118
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 37134
    :goto_0
    return v0

    .line 37122
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->hasResult()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 37124
    goto :goto_0

    .line 37126
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->hasAccountid()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 37128
    goto :goto_0

    .line 37130
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 37132
    goto :goto_0

    .line 37134
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 37208
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 37210
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 37216
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    .line 37217
    return-object p0

    .line 37213
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37031
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 37031
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37031
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37142
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 37143
    sparse-switch v0, :sswitch_data_0

    .line 37148
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 37150
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 37146
    goto :goto_1

    .line 37155
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 37156
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 37157
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 37159
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 37160
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    goto :goto_0

    .line 37164
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 37165
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    move-result-object v0

    .line 37166
    if-eqz v0, :cond_0

    .line 37167
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    .line 37168
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->result_:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    goto :goto_0

    .line 37173
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    .line 37174
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->accountid_:Ljava/lang/Object;

    goto :goto_0

    .line 37143
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 37104
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 37114
    :goto_0
    return-object v0

    .line 37105
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 37106
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    .line 37108
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->hasResult()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 37109
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->getResult()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->setResult(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    .line 37111
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->hasAccountid()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 37112
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->getAccountid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->setAccountid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    :cond_3
    move-object v0, p0

    .line 37114
    goto :goto_0
.end method

.method public setAccountid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 37266
    if-nez p1, :cond_0

    .line 37267
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 37269
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    .line 37270
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->accountid_:Ljava/lang/Object;

    .line 37272
    return-object p0
.end method

.method setAccountid(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 37281
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    .line 37282
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->accountid_:Ljava/lang/Object;

    .line 37284
    return-void
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 37202
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 37204
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    .line 37205
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 37192
    if-nez p1, :cond_0

    .line 37193
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 37195
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 37197
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    .line 37198
    return-object p0
.end method

.method public setResult(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 37235
    if-nez p1, :cond_0

    .line 37236
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 37238
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->bitField0_:I

    .line 37239
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->result_:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    .line 37241
    return-object p0
.end method
