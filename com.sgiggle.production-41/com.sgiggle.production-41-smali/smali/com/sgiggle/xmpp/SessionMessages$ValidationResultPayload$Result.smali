.class public final enum Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Result"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result; = null

.field public static final enum ACCEPTED:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result; = null

.field public static final ACCEPTED_VALUE:I = 0x4

.field public static final enum CODEREQUIRED:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result; = null

.field public static final enum CODEREQUIREDDEVICE:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result; = null

.field public static final CODEREQUIREDDEVICE_VALUE:I = 0x3

.field public static final CODEREQUIRED_VALUE:I = 0x2

.field public static final enum EMAILCODEREQUIRED:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result; = null

.field public static final EMAILCODEREQUIRED_VALUE:I = 0x6

.field public static final enum FAILED:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result; = null

.field public static final FAILED_VALUE:I = 0x5

.field public static final enum NONE:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result; = null

.field public static final NONE_VALUE:I = 0x0

.field public static final enum PUSHCODEEMAILREQUIRED:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result; = null

.field public static final PUSHCODEEMAILREQUIRED_VALUE:I = 0x7

.field public static final enum PUSHCODEPHONEREQUIRED:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result; = null

.field public static final PUSHCODEPHONEREQUIRED_VALUE:I = 0x8

.field public static final enum REQUIRED:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result; = null

.field public static final REQUIRED_VALUE:I = 0x1

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 36773
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->NONE:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    .line 36774
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    const-string v1, "REQUIRED"

    invoke-direct {v0, v1, v6, v6, v6}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->REQUIRED:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    .line 36775
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    const-string v1, "CODEREQUIRED"

    invoke-direct {v0, v1, v7, v7, v7}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->CODEREQUIRED:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    .line 36776
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    const-string v1, "CODEREQUIREDDEVICE"

    invoke-direct {v0, v1, v8, v8, v8}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->CODEREQUIREDDEVICE:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    .line 36777
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    const-string v1, "ACCEPTED"

    invoke-direct {v0, v1, v9, v9, v9}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->ACCEPTED:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    .line 36778
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    const-string v1, "FAILED"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const/4 v4, 0x5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->FAILED:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    .line 36779
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    const-string v1, "EMAILCODEREQUIRED"

    const/4 v2, 0x6

    const/4 v3, 0x6

    const/4 v4, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->EMAILCODEREQUIRED:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    .line 36780
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    const-string v1, "PUSHCODEEMAILREQUIRED"

    const/4 v2, 0x7

    const/4 v3, 0x7

    const/4 v4, 0x7

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->PUSHCODEEMAILREQUIRED:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    .line 36781
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    const-string v1, "PUSHCODEPHONEREQUIRED"

    const/16 v2, 0x8

    const/16 v3, 0x8

    const/16 v4, 0x8

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->PUSHCODEPHONEREQUIRED:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    .line 36771
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->NONE:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->REQUIRED:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->CODEREQUIRED:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->CODEREQUIREDDEVICE:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    aput-object v1, v0, v8

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->ACCEPTED:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->FAILED:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->EMAILCODEREQUIRED:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->PUSHCODEEMAILREQUIRED:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->PUSHCODEPHONEREQUIRED:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    .line 36817
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 36826
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 36827
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->value:I

    .line 36828
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36814
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;
    .locals 1
    .parameter

    .prologue
    .line 36798
    packed-switch p0, :pswitch_data_0

    .line 36808
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 36799
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->NONE:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    goto :goto_0

    .line 36800
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->REQUIRED:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    goto :goto_0

    .line 36801
    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->CODEREQUIRED:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    goto :goto_0

    .line 36802
    :pswitch_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->CODEREQUIREDDEVICE:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    goto :goto_0

    .line 36803
    :pswitch_4
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->ACCEPTED:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    goto :goto_0

    .line 36804
    :pswitch_5
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->FAILED:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    goto :goto_0

    .line 36805
    :pswitch_6
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->EMAILCODEREQUIRED:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    goto :goto_0

    .line 36806
    :pswitch_7
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->PUSHCODEEMAILREQUIRED:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    goto :goto_0

    .line 36807
    :pswitch_8
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->PUSHCODEPHONEREQUIRED:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    goto :goto_0

    .line 36798
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;
    .locals 1
    .parameter

    .prologue
    .line 36771
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;
    .locals 1

    .prologue
    .line 36771
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 36795
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->value:I

    return v0
.end method
