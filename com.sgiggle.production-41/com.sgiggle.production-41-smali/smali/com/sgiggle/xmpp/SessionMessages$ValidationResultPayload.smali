.class public final Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ValidationResultPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;,
        Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;
    }
.end annotation


# static fields
.field public static final ACCOUNTID_FIELD_NUMBER:I = 0x3

.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final RESULT_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;


# instance fields
.field private accountid_:Ljava/lang/Object;

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private result_:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 37290
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    .line 37291
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->initFields()V

    .line 37292
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 36758
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 36891
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->memoizedIsInitialized:B

    .line 36930
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->memoizedSerializedSize:I

    .line 36759
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 36753
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 36760
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 36891
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->memoizedIsInitialized:B

    .line 36930
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->memoizedSerializedSize:I

    .line 36760
    return-void
.end method

.method static synthetic access$47202(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 36753
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$47302(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 36753
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->result_:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    return-object p1
.end method

.method static synthetic access$47402(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 36753
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->accountid_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$47502(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 36753
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->bitField0_:I

    return p1
.end method

.method private getAccountidBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 36875
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->accountid_:Ljava/lang/Object;

    .line 36876
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 36877
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 36879
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->accountid_:Ljava/lang/Object;

    .line 36882
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;
    .locals 1

    .prologue
    .line 36764
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 36887
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 36888
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->NONE:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->result_:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    .line 36889
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->accountid_:Ljava/lang/Object;

    .line 36890
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;
    .locals 1

    .prologue
    .line 37024
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->access$47000()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 37027
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36993
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    move-result-object v0

    .line 36994
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 36995
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->access$46900(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    move-result-object v0

    .line 36997
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37004
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    move-result-object v0

    .line 37005
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 37006
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->access$46900(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    move-result-object v0

    .line 37008
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 36960
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->access$46900(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 36966
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->access$46900(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37014
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->access$46900(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37020
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->access$46900(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36982
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->access$46900(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36988
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->access$46900(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 36971
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->access$46900(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 36977
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;->access$46900(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAccountid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 36861
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->accountid_:Ljava/lang/Object;

    .line 36862
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 36863
    check-cast v0, Ljava/lang/String;

    .line 36871
    :goto_0
    return-object v0

    .line 36865
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 36867
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 36868
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 36869
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->accountid_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 36871
    goto :goto_0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 36841
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 36753
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;
    .locals 1

    .prologue
    .line 36768
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    return-object v0
.end method

.method public getResult()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;
    .locals 1

    .prologue
    .line 36851
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->result_:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 36932
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->memoizedSerializedSize:I

    .line 36933
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 36949
    :goto_0
    return v0

    .line 36935
    :cond_0
    const/4 v0, 0x0

    .line 36936
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 36937
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 36940
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 36941
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->result_:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->getNumber()I

    move-result v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 36944
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 36945
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->getAccountidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 36948
    :cond_3
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public hasAccountid()Z
    .locals 2

    .prologue
    .line 36858
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 36838
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasResult()Z
    .locals 2

    .prologue
    .line 36848
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 36893
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->memoizedIsInitialized:B

    .line 36894
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 36913
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 36894
    goto :goto_0

    .line 36896
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 36897
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 36898
    goto :goto_0

    .line 36900
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->hasResult()Z

    move-result v0

    if-nez v0, :cond_3

    .line 36901
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 36902
    goto :goto_0

    .line 36904
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->hasAccountid()Z

    move-result v0

    if-nez v0, :cond_4

    .line 36905
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 36906
    goto :goto_0

    .line 36908
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_5

    .line 36909
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 36910
    goto :goto_0

    .line 36912
    :cond_5
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 36913
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 36753
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;
    .locals 1

    .prologue
    .line 37025
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 36753
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;
    .locals 1

    .prologue
    .line 37029
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;)Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 36954
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 36918
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->getSerializedSize()I

    .line 36919
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 36920
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 36922
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 36923
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->result_:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->getNumber()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 36925
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 36926
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->getAccountidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 36928
    :cond_2
    return-void
.end method
