.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ValidationResultPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getAccountid()Ljava/lang/String;
.end method

.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getResult()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;
.end method

.method public abstract hasAccountid()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasResult()Z
.end method
