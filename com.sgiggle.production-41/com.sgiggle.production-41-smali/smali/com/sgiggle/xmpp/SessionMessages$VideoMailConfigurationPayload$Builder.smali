.class public final Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private config_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->config_:Ljava/util/List;

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->maybeForceBuilderInitialization()V

    return-void
.end method

.method static synthetic access$86300(Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$86400()Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;
    .locals 1

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureConfigIsMutable()V
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->config_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->config_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    return-void
.end method


# virtual methods
.method public addAllConfig(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->ensureConfigIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->config_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public addConfig(ILcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;
    .locals 2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->ensureConfigIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->config_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public addConfig(ILcom/sgiggle/xmpp/SessionMessages$KeyValuePair;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;
    .locals 1

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->ensureConfigIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->config_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public addConfig(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;
    .locals 2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->ensureConfigIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->config_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addConfig(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->ensureConfigIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->config_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;
    .locals 2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;
    .locals 4

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v1, v1, 0x1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_1

    or-int/lit8 v1, v2, 0x1

    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;->access$86602(Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->config_:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->config_:Ljava/util/List;

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->bitField0_:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->bitField0_:I

    :cond_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->config_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;->config_:Ljava/util/List;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;->access$86702(Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;Ljava/util/List;)Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;->access$86802(Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;I)I

    return-object v0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;
    .locals 1

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->bitField0_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->config_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearConfig()Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->config_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getConfig(I)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->config_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    return-object v0
.end method

.method public getConfigCount()I
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->config_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getConfigList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->config_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;

    move-result-object v0

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->getConfigCount()I

    move-result v1

    if-ge v0, v1, :cond_3

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->getConfig(I)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    goto :goto_1

    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->addConfig(Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;

    :cond_1
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;->config_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;->access$86700(Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->config_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;->config_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;->access$86700(Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->config_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->bitField0_:I

    :cond_2
    :goto_1
    move-object v0, p0

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->ensureConfigIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->config_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;->config_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;->access$86700(Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public removeConfig(I)Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->ensureConfigIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->config_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setConfig(ILcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;
    .locals 2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->ensureConfigIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->config_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setConfig(ILcom/sgiggle/xmpp/SessionMessages$KeyValuePair;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;
    .locals 1

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->ensureConfigIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailConfigurationPayload$Builder;->config_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method
