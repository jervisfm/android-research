.class public final Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private videoMailUrl_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 65273
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 65399
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 65442
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->videoMailUrl_:Ljava/lang/Object;

    .line 65274
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->maybeForceBuilderInitialization()V

    .line 65275
    return-void
.end method

.method static synthetic access$84100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 65268
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$84200()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;
    .locals 1

    .prologue
    .line 65268
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 65310
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;

    move-result-object v0

    .line 65311
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 65312
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 65315
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;
    .locals 1

    .prologue
    .line 65280
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 65278
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 65268
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;
    .locals 2

    .prologue
    .line 65301
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;

    move-result-object v0

    .line 65302
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 65303
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 65305
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 65268
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;
    .locals 5

    .prologue
    .line 65319
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 65320
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->bitField0_:I

    .line 65321
    const/4 v2, 0x0

    .line 65322
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 65323
    or-int/lit8 v2, v2, 0x1

    .line 65325
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->access$84402(Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 65326
    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    .line 65327
    or-int/lit8 v1, v2, 0x2

    .line 65329
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->videoMailUrl_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->videoMailUrl_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->access$84502(Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65330
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->access$84602(Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;I)I

    .line 65331
    return-object v0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 65268
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 65268
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;
    .locals 1

    .prologue
    .line 65284
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 65285
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 65286
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->bitField0_:I

    .line 65287
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->videoMailUrl_:Ljava/lang/Object;

    .line 65288
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->bitField0_:I

    .line 65289
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;
    .locals 1

    .prologue
    .line 65435
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 65437
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->bitField0_:I

    .line 65438
    return-object p0
.end method

.method public clearVideoMailUrl()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;
    .locals 1

    .prologue
    .line 65466
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->bitField0_:I

    .line 65467
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->getVideoMailUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->videoMailUrl_:Ljava/lang/Object;

    .line 65469
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 65268
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 65268
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 65268
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;
    .locals 2

    .prologue
    .line 65293
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 65268
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 65404
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 65268
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 65268
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;
    .locals 1

    .prologue
    .line 65297
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;

    move-result-object v0

    return-object v0
.end method

.method public getVideoMailUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 65447
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->videoMailUrl_:Ljava/lang/Object;

    .line 65448
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 65449
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 65450
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->videoMailUrl_:Ljava/lang/Object;

    .line 65453
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 65401
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoMailUrl()Z
    .locals 2

    .prologue
    .line 65444
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65346
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 65358
    :goto_0
    return v0

    .line 65350
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->hasVideoMailUrl()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 65352
    goto :goto_0

    .line 65354
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 65356
    goto :goto_0

    .line 65358
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 65423
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 65425
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 65431
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->bitField0_:I

    .line 65432
    return-object p0

    .line 65428
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65268
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 65268
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65268
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65366
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 65367
    sparse-switch v0, :sswitch_data_0

    .line 65372
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 65374
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 65370
    goto :goto_1

    .line 65379
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 65380
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 65381
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 65383
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 65384
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    goto :goto_0

    .line 65388
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->bitField0_:I

    .line 65389
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->videoMailUrl_:Ljava/lang/Object;

    goto :goto_0

    .line 65367
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 65335
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 65342
    :goto_0
    return-object v0

    .line 65336
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65337
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    .line 65339
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->hasVideoMailUrl()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 65340
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->getVideoMailUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->setVideoMailUrl(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    :cond_2
    move-object v0, p0

    .line 65342
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 65417
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 65419
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->bitField0_:I

    .line 65420
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 65407
    if-nez p1, :cond_0

    .line 65408
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 65410
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 65412
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->bitField0_:I

    .line 65413
    return-object p0
.end method

.method public setVideoMailUrl(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 65457
    if-nez p1, :cond_0

    .line 65458
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 65460
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->bitField0_:I

    .line 65461
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->videoMailUrl_:Ljava/lang/Object;

    .line 65463
    return-object p0
.end method

.method setVideoMailUrl(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 65472
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->bitField0_:I

    .line 65473
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->videoMailUrl_:Ljava/lang/Object;

    .line 65475
    return-void
.end method
