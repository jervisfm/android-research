.class public final Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VideoMailDownloadURLPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final VIDEO_MAIL_URL_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private videoMailUrl_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 65481
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;

    .line 65482
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->initFields()V

    .line 65483
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 65079
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 65139
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->memoizedIsInitialized:B

    .line 65171
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->memoizedSerializedSize:I

    .line 65080
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 65074
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 65081
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 65139
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->memoizedIsInitialized:B

    .line 65171
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->memoizedSerializedSize:I

    .line 65081
    return-void
.end method

.method static synthetic access$84402(Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 65074
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$84502(Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 65074
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->videoMailUrl_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$84602(Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 65074
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;
    .locals 1

    .prologue
    .line 65085
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;

    return-object v0
.end method

.method private getVideoMailUrlBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 65124
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->videoMailUrl_:Ljava/lang/Object;

    .line 65125
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 65126
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 65128
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->videoMailUrl_:Ljava/lang/Object;

    .line 65131
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 65136
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 65137
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->videoMailUrl_:Ljava/lang/Object;

    .line 65138
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;
    .locals 1

    .prologue
    .line 65261
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->access$84200()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 65264
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65230
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    move-result-object v0

    .line 65231
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 65232
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->access$84100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;

    move-result-object v0

    .line 65234
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65241
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    move-result-object v0

    .line 65242
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 65243
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->access$84100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;

    move-result-object v0

    .line 65245
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 65197
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->access$84100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 65203
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->access$84100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65251
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->access$84100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65257
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->access$84100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65219
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->access$84100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65225
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->access$84100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 65208
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->access$84100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 65214
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;->access$84100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 65100
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 65074
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;
    .locals 1

    .prologue
    .line 65089
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 65173
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->memoizedSerializedSize:I

    .line 65174
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 65186
    :goto_0
    return v0

    .line 65176
    :cond_0
    const/4 v0, 0x0

    .line 65177
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 65178
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 65181
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 65182
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->getVideoMailUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 65185
    :cond_2
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getVideoMailUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 65110
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->videoMailUrl_:Ljava/lang/Object;

    .line 65111
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 65112
    check-cast v0, Ljava/lang/String;

    .line 65120
    :goto_0
    return-object v0

    .line 65114
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 65116
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 65117
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65118
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->videoMailUrl_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 65120
    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 65097
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoMailUrl()Z
    .locals 2

    .prologue
    .line 65107
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 65141
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->memoizedIsInitialized:B

    .line 65142
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 65157
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 65142
    goto :goto_0

    .line 65144
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 65145
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 65146
    goto :goto_0

    .line 65148
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->hasVideoMailUrl()Z

    move-result v0

    if-nez v0, :cond_3

    .line 65149
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 65150
    goto :goto_0

    .line 65152
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    .line 65153
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 65154
    goto :goto_0

    .line 65156
    :cond_4
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 65157
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 65074
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;
    .locals 1

    .prologue
    .line 65262
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 65074
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;
    .locals 1

    .prologue
    .line 65266
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 65191
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 65162
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->getSerializedSize()I

    .line 65163
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 65164
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 65166
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 65167
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->getVideoMailUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 65169
    :cond_1
    return-void
.end method
