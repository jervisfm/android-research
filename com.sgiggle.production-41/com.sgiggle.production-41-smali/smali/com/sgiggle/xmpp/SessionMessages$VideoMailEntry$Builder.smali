.class public final Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntryOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;",
        "Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntryOrBuilder;"
    }
.end annotation


# instance fields
.field private available_:Z

.field private bitField0_:I

.field private caller_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

.field private duration_:I

.field private folder_:Ljava/lang/Object;

.field private read_:Z

.field private receivers_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private size_:I

.field private timeCreated_:J

.field private timeUploaded_:J

.field private videoMailId_:Ljava/lang/Object;

.field private videoMailUrl_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 56200
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 56483
    const-string v0, "_inbox"

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->folder_:Ljava/lang/Object;

    .line 56519
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->videoMailId_:Ljava/lang/Object;

    .line 56555
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->videoMailUrl_:Ljava/lang/Object;

    .line 56591
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->caller_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 56634
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->receivers_:Ljava/util/List;

    .line 56201
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->maybeForceBuilderInitialization()V

    .line 56202
    return-void
.end method

.method static synthetic access$72100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 56195
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$72200()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1

    .prologue
    .line 56195
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 56255
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v0

    .line 56256
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 56257
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 56260
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1

    .prologue
    .line 56207
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;-><init>()V

    return-object v0
.end method

.method private ensureReceiversIsMutable()V
    .locals 2

    .prologue
    .line 56637
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    .line 56638
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->receivers_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->receivers_:Ljava/util/List;

    .line 56639
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56641
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 56205
    return-void
.end method


# virtual methods
.method public addAllReceivers(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;"
        }
    .end annotation

    .prologue
    .line 56704
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->ensureReceiversIsMutable()V

    .line 56705
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->receivers_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 56707
    return-object p0
.end method

.method public addReceivers(ILcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 56697
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->ensureReceiversIsMutable()V

    .line 56698
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->receivers_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 56700
    return-object p0
.end method

.method public addReceivers(ILcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 56680
    if-nez p2, :cond_0

    .line 56681
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56683
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->ensureReceiversIsMutable()V

    .line 56684
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->receivers_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 56686
    return-object p0
.end method

.method public addReceivers(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 2
    .parameter

    .prologue
    .line 56690
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->ensureReceiversIsMutable()V

    .line 56691
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->receivers_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56693
    return-object p0
.end method

.method public addReceivers(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 56670
    if-nez p1, :cond_0

    .line 56671
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56673
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->ensureReceiversIsMutable()V

    .line 56674
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->receivers_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56676
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 56195
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    .locals 2

    .prologue
    .line 56246
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v0

    .line 56247
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 56248
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 56250
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 56195
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    .locals 5

    .prologue
    .line 56264
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;-><init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 56265
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56266
    const/4 v2, 0x0

    .line 56267
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 56268
    or-int/lit8 v2, v2, 0x1

    .line 56270
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->folder_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->folder_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->access$72402(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56271
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 56272
    or-int/lit8 v2, v2, 0x2

    .line 56274
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->videoMailId_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->videoMailId_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->access$72502(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56275
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 56276
    or-int/lit8 v2, v2, 0x4

    .line 56278
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->videoMailUrl_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->videoMailUrl_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->access$72602(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56279
    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 56280
    or-int/lit8 v2, v2, 0x8

    .line 56282
    :cond_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->caller_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->caller_:Lcom/sgiggle/xmpp/SessionMessages$Contact;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->access$72702(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 56283
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 56284
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->receivers_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->receivers_:Ljava/util/List;

    .line 56285
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x11

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56287
    :cond_4
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->receivers_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->receivers_:Ljava/util/List;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->access$72802(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;Ljava/util/List;)Ljava/util/List;

    .line 56288
    and-int/lit8 v3, v1, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 56289
    or-int/lit8 v2, v2, 0x10

    .line 56291
    :cond_5
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->duration_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->duration_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->access$72902(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;I)I

    .line 56292
    and-int/lit8 v3, v1, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    .line 56293
    or-int/lit8 v2, v2, 0x20

    .line 56295
    :cond_6
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->size_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->size_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->access$73002(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;I)I

    .line 56296
    and-int/lit16 v3, v1, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    .line 56297
    or-int/lit8 v2, v2, 0x40

    .line 56299
    :cond_7
    iget-wide v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->timeCreated_:J

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->timeCreated_:J
    invoke-static {v0, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->access$73102(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;J)J

    .line 56300
    and-int/lit16 v3, v1, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_8

    .line 56301
    or-int/lit16 v2, v2, 0x80

    .line 56303
    :cond_8
    iget-wide v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->timeUploaded_:J

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->timeUploaded_:J
    invoke-static {v0, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->access$73202(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;J)J

    .line 56304
    and-int/lit16 v3, v1, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_9

    .line 56305
    or-int/lit16 v2, v2, 0x100

    .line 56307
    :cond_9
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->read_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->read_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->access$73302(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;Z)Z

    .line 56308
    and-int/lit16 v1, v1, 0x400

    const/16 v3, 0x400

    if-ne v1, v3, :cond_a

    .line 56309
    or-int/lit16 v1, v2, 0x200

    .line 56311
    :goto_0
    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->available_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->available_:Z
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->access$73402(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;Z)Z

    .line 56312
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->access$73502(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;I)I

    .line 56313
    return-object v0

    :cond_a
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 56195
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 56195
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 56211
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 56212
    const-string v0, "_inbox"

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->folder_:Ljava/lang/Object;

    .line 56213
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56214
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->videoMailId_:Ljava/lang/Object;

    .line 56215
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56216
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->videoMailUrl_:Ljava/lang/Object;

    .line 56217
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56218
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->caller_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 56219
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56220
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->receivers_:Ljava/util/List;

    .line 56221
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56222
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->duration_:I

    .line 56223
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56224
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->size_:I

    .line 56225
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56226
    iput-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->timeCreated_:J

    .line 56227
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56228
    iput-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->timeUploaded_:J

    .line 56229
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56230
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->read_:Z

    .line 56231
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56232
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->available_:Z

    .line 56233
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56234
    return-object p0
.end method

.method public clearAvailable()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1

    .prologue
    .line 56842
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56843
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->available_:Z

    .line 56845
    return-object p0
.end method

.method public clearCaller()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1

    .prologue
    .line 56627
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->caller_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 56629
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56630
    return-object p0
.end method

.method public clearDuration()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1

    .prologue
    .line 56737
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56738
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->duration_:I

    .line 56740
    return-object p0
.end method

.method public clearFolder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1

    .prologue
    .line 56507
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56508
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getFolder()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->folder_:Ljava/lang/Object;

    .line 56510
    return-object p0
.end method

.method public clearRead()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1

    .prologue
    .line 56821
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56822
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->read_:Z

    .line 56824
    return-object p0
.end method

.method public clearReceivers()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1

    .prologue
    .line 56710
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->receivers_:Ljava/util/List;

    .line 56711
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56713
    return-object p0
.end method

.method public clearSize()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1

    .prologue
    .line 56758
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56759
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->size_:I

    .line 56761
    return-object p0
.end method

.method public clearTimeCreated()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 2

    .prologue
    .line 56779
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56780
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->timeCreated_:J

    .line 56782
    return-object p0
.end method

.method public clearTimeUploaded()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 2

    .prologue
    .line 56800
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56801
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->timeUploaded_:J

    .line 56803
    return-object p0
.end method

.method public clearVideoMailId()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1

    .prologue
    .line 56543
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56544
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getVideoMailId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->videoMailId_:Ljava/lang/Object;

    .line 56546
    return-object p0
.end method

.method public clearVideoMailUrl()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1

    .prologue
    .line 56579
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56580
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getVideoMailUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->videoMailUrl_:Ljava/lang/Object;

    .line 56582
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 56195
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 56195
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 56195
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 2

    .prologue
    .line 56238
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 56195
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAvailable()Z
    .locals 1

    .prologue
    .line 56833
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->available_:Z

    return v0
.end method

.method public getCaller()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1

    .prologue
    .line 56596
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->caller_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 56195
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 56195
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    .locals 1

    .prologue
    .line 56242
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v0

    return-object v0
.end method

.method public getDuration()I
    .locals 1

    .prologue
    .line 56728
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->duration_:I

    return v0
.end method

.method public getFolder()Ljava/lang/String;
    .locals 2

    .prologue
    .line 56488
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->folder_:Ljava/lang/Object;

    .line 56489
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 56490
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 56491
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->folder_:Ljava/lang/Object;

    .line 56494
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getRead()Z
    .locals 1

    .prologue
    .line 56812
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->read_:Z

    return v0
.end method

.method public getReceivers(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter

    .prologue
    .line 56650
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->receivers_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getReceiversCount()I
    .locals 1

    .prologue
    .line 56647
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->receivers_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getReceiversList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56644
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->receivers_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 56749
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->size_:I

    return v0
.end method

.method public getTimeCreated()J
    .locals 2

    .prologue
    .line 56770
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->timeCreated_:J

    return-wide v0
.end method

.method public getTimeUploaded()J
    .locals 2

    .prologue
    .line 56791
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->timeUploaded_:J

    return-wide v0
.end method

.method public getVideoMailId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 56524
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->videoMailId_:Ljava/lang/Object;

    .line 56525
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 56526
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 56527
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->videoMailId_:Ljava/lang/Object;

    .line 56530
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getVideoMailUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 56560
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->videoMailUrl_:Ljava/lang/Object;

    .line 56561
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 56562
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 56563
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->videoMailUrl_:Ljava/lang/Object;

    .line 56566
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasAvailable()Z
    .locals 2

    .prologue
    .line 56830
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCaller()Z
    .locals 2

    .prologue
    .line 56593
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDuration()Z
    .locals 2

    .prologue
    .line 56725
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFolder()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 56485
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRead()Z
    .locals 2

    .prologue
    .line 56809
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSize()Z
    .locals 2

    .prologue
    .line 56746
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTimeCreated()Z
    .locals 2

    .prologue
    .line 56767
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTimeUploaded()Z
    .locals 2

    .prologue
    .line 56788
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoMailId()Z
    .locals 2

    .prologue
    .line 56521
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoMailUrl()Z
    .locals 2

    .prologue
    .line 56557
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 56362
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->hasVideoMailId()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 56396
    :goto_0
    return v0

    .line 56366
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->hasCaller()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 56368
    goto :goto_0

    .line 56370
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->hasDuration()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    .line 56372
    goto :goto_0

    .line 56374
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->hasSize()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    .line 56376
    goto :goto_0

    .line 56378
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->hasTimeCreated()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v2

    .line 56380
    goto :goto_0

    .line 56382
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->hasTimeUploaded()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v2

    .line 56384
    goto :goto_0

    .line 56386
    :cond_5
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->getCaller()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v2

    .line 56388
    goto :goto_0

    :cond_6
    move v0, v2

    .line 56390
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->getReceiversCount()I

    move-result v1

    if-ge v0, v1, :cond_8

    .line 56391
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->getReceivers(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_7

    move v0, v2

    .line 56393
    goto :goto_0

    .line 56390
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 56396
    :cond_8
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeCaller(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 2
    .parameter

    .prologue
    .line 56615
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->caller_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 56617
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->caller_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->caller_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 56623
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56624
    return-object p0

    .line 56620
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->caller_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56195
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 56195
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56195
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56404
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 56405
    sparse-switch v0, :sswitch_data_0

    .line 56410
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 56412
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 56408
    goto :goto_1

    .line 56417
    :sswitch_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56418
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->folder_:Ljava/lang/Object;

    goto :goto_0

    .line 56422
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56423
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->videoMailId_:Ljava/lang/Object;

    goto :goto_0

    .line 56427
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56428
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->videoMailUrl_:Ljava/lang/Object;

    goto :goto_0

    .line 56432
    :sswitch_4
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    .line 56433
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->hasCaller()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 56434
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->getCaller()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    .line 56436
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 56437
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->setCaller(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    goto :goto_0

    .line 56441
    :sswitch_5
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    .line 56442
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 56443
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->addReceivers(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    goto :goto_0

    .line 56447
    :sswitch_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56448
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->duration_:I

    goto :goto_0

    .line 56452
    :sswitch_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56453
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->size_:I

    goto :goto_0

    .line 56457
    :sswitch_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56458
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->timeCreated_:J

    goto/16 :goto_0

    .line 56462
    :sswitch_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56463
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->timeUploaded_:J

    goto/16 :goto_0

    .line 56467
    :sswitch_a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56468
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->read_:Z

    goto/16 :goto_0

    .line 56472
    :sswitch_b
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56473
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->available_:Z

    goto/16 :goto_0

    .line 56405
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 2
    .parameter

    .prologue
    .line 56317
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 56358
    :goto_0
    return-object v0

    .line 56318
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->hasFolder()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 56319
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getFolder()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->setFolder(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    .line 56321
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->hasVideoMailId()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 56322
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getVideoMailId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->setVideoMailId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    .line 56324
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->hasVideoMailUrl()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 56325
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getVideoMailUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->setVideoMailUrl(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    .line 56327
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->hasCaller()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 56328
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getCaller()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->mergeCaller(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    .line 56330
    :cond_4
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->receivers_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->access$72800(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 56331
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->receivers_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 56332
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->receivers_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->access$72800(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->receivers_:Ljava/util/List;

    .line 56333
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56340
    :cond_5
    :goto_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->hasDuration()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 56341
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getDuration()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->setDuration(I)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    .line 56343
    :cond_6
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->hasSize()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 56344
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getSize()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->setSize(I)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    .line 56346
    :cond_7
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->hasTimeCreated()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 56347
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getTimeCreated()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->setTimeCreated(J)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    .line 56349
    :cond_8
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->hasTimeUploaded()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 56350
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getTimeUploaded()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->setTimeUploaded(J)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    .line 56352
    :cond_9
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->hasRead()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 56353
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getRead()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->setRead(Z)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    .line 56355
    :cond_a
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->hasAvailable()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 56356
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getAvailable()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->setAvailable(Z)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    :cond_b
    move-object v0, p0

    .line 56358
    goto/16 :goto_0

    .line 56335
    :cond_c
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->ensureReceiversIsMutable()V

    .line 56336
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->receivers_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->receivers_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->access$72800(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public removeReceivers(I)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 56716
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->ensureReceiversIsMutable()V

    .line 56717
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->receivers_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 56719
    return-object p0
.end method

.method public setAvailable(Z)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 56836
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56837
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->available_:Z

    .line 56839
    return-object p0
.end method

.method public setCaller(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 56609
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->caller_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 56611
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56612
    return-object p0
.end method

.method public setCaller(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 56599
    if-nez p1, :cond_0

    .line 56600
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56602
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->caller_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 56604
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56605
    return-object p0
.end method

.method public setDuration(I)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 56731
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56732
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->duration_:I

    .line 56734
    return-object p0
.end method

.method public setFolder(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 56498
    if-nez p1, :cond_0

    .line 56499
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56501
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56502
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->folder_:Ljava/lang/Object;

    .line 56504
    return-object p0
.end method

.method setFolder(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 56513
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56514
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->folder_:Ljava/lang/Object;

    .line 56516
    return-void
.end method

.method public setRead(Z)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 56815
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56816
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->read_:Z

    .line 56818
    return-object p0
.end method

.method public setReceivers(ILcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 56664
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->ensureReceiversIsMutable()V

    .line 56665
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->receivers_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 56667
    return-object p0
.end method

.method public setReceivers(ILcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 56654
    if-nez p2, :cond_0

    .line 56655
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56657
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->ensureReceiversIsMutable()V

    .line 56658
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->receivers_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 56660
    return-object p0
.end method

.method public setSize(I)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 56752
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56753
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->size_:I

    .line 56755
    return-object p0
.end method

.method public setTimeCreated(J)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 56773
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56774
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->timeCreated_:J

    .line 56776
    return-object p0
.end method

.method public setTimeUploaded(J)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 56794
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56795
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->timeUploaded_:J

    .line 56797
    return-object p0
.end method

.method public setVideoMailId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 56534
    if-nez p1, :cond_0

    .line 56535
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56537
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56538
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->videoMailId_:Ljava/lang/Object;

    .line 56540
    return-object p0
.end method

.method setVideoMailId(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 56549
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56550
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->videoMailId_:Ljava/lang/Object;

    .line 56552
    return-void
.end method

.method public setVideoMailUrl(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 56570
    if-nez p1, :cond_0

    .line 56571
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 56573
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56574
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->videoMailUrl_:Ljava/lang/Object;

    .line 56576
    return-object p0
.end method

.method setVideoMailUrl(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 56585
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->bitField0_:I

    .line 56586
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->videoMailUrl_:Ljava/lang/Object;

    .line 56588
    return-void
.end method
