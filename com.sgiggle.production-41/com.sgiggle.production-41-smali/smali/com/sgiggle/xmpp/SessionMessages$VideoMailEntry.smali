.class public final Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntryOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VideoMailEntry"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    }
.end annotation


# static fields
.field public static final AVAILABLE_FIELD_NUMBER:I = 0xb

.field public static final CALLER_FIELD_NUMBER:I = 0x4

.field public static final DURATION_FIELD_NUMBER:I = 0x6

.field public static final FOLDER_FIELD_NUMBER:I = 0x1

.field public static final READ_FIELD_NUMBER:I = 0xa

.field public static final RECEIVERS_FIELD_NUMBER:I = 0x5

.field public static final SIZE_FIELD_NUMBER:I = 0x7

.field public static final TIME_CREATED_FIELD_NUMBER:I = 0x8

.field public static final TIME_UPLOADED_FIELD_NUMBER:I = 0x9

.field public static final VIDEO_MAIL_ID_FIELD_NUMBER:I = 0x2

.field public static final VIDEO_MAIL_URL_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;


# instance fields
.field private available_:Z

.field private bitField0_:I

.field private caller_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

.field private duration_:I

.field private folder_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private read_:Z

.field private receivers_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private size_:I

.field private timeCreated_:J

.field private timeUploaded_:J

.field private videoMailId_:Ljava/lang/Object;

.field private videoMailUrl_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 56852
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    .line 56853
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->initFields()V

    .line 56854
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 55767
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 55981
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->memoizedIsInitialized:B

    .line 56062
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->memoizedSerializedSize:I

    .line 55768
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 55762
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;-><init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 55769
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 55981
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->memoizedIsInitialized:B

    .line 56062
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->memoizedSerializedSize:I

    .line 55769
    return-void
.end method

.method static synthetic access$72402(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 55762
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->folder_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$72502(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 55762
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->videoMailId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$72602(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 55762
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->videoMailUrl_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$72702(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 55762
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->caller_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object p1
.end method

.method static synthetic access$72800(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 55762
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->receivers_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$72802(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 55762
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->receivers_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$72902(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 55762
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->duration_:I

    return p1
.end method

.method static synthetic access$73002(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 55762
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->size_:I

    return p1
.end method

.method static synthetic access$73102(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;J)J
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 55762
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->timeCreated_:J

    return-wide p1
.end method

.method static synthetic access$73202(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;J)J
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 55762
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->timeUploaded_:J

    return-wide p1
.end method

.method static synthetic access$73302(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 55762
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->read_:Z

    return p1
.end method

.method static synthetic access$73402(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 55762
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->available_:Z

    return p1
.end method

.method static synthetic access$73502(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 55762
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    .locals 1

    .prologue
    .line 55773
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    return-object v0
.end method

.method private getFolderBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 55802
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->folder_:Ljava/lang/Object;

    .line 55803
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 55804
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 55806
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->folder_:Ljava/lang/Object;

    .line 55809
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getVideoMailIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 55834
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->videoMailId_:Ljava/lang/Object;

    .line 55835
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 55836
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 55838
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->videoMailId_:Ljava/lang/Object;

    .line 55841
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getVideoMailUrlBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 55866
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->videoMailUrl_:Ljava/lang/Object;

    .line 55867
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 55868
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 55870
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->videoMailUrl_:Ljava/lang/Object;

    .line 55873
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 55969
    const-string v0, "_inbox"

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->folder_:Ljava/lang/Object;

    .line 55970
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->videoMailId_:Ljava/lang/Object;

    .line 55971
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->videoMailUrl_:Ljava/lang/Object;

    .line 55972
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->caller_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 55973
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->receivers_:Ljava/util/List;

    .line 55974
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->duration_:I

    .line 55975
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->size_:I

    .line 55976
    iput-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->timeCreated_:J

    .line 55977
    iput-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->timeUploaded_:J

    .line 55978
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->read_:Z

    .line 55979
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->available_:Z

    .line 55980
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1

    .prologue
    .line 56188
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->access$72200()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 56191
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56157
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    .line 56158
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 56159
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->access$72100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v0

    .line 56161
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56168
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    .line 56169
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 56170
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->access$72100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v0

    .line 56172
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 56124
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->access$72100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 56130
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->access$72100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56178
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->access$72100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56184
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->access$72100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56146
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->access$72100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56152
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->access$72100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 56135
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->access$72100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 56141
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->access$72100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAvailable()Z
    .locals 1

    .prologue
    .line 55965
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->available_:Z

    return v0
.end method

.method public getCaller()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1

    .prologue
    .line 55884
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->caller_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 55762
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    .locals 1

    .prologue
    .line 55777
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    return-object v0
.end method

.method public getDuration()I
    .locals 1

    .prologue
    .line 55915
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->duration_:I

    return v0
.end method

.method public getFolder()Ljava/lang/String;
    .locals 2

    .prologue
    .line 55788
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->folder_:Ljava/lang/Object;

    .line 55789
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 55790
    check-cast v0, Ljava/lang/String;

    .line 55798
    :goto_0
    return-object v0

    .line 55792
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 55794
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 55795
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55796
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->folder_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 55798
    goto :goto_0
.end method

.method public getRead()Z
    .locals 1

    .prologue
    .line 55955
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->read_:Z

    return v0
.end method

.method public getReceivers(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter

    .prologue
    .line 55901
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->receivers_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getReceiversCount()I
    .locals 1

    .prologue
    .line 55898
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->receivers_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getReceiversList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55891
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->receivers_:Ljava/util/List;

    return-object v0
.end method

.method public getReceiversOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 55905
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->receivers_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;

    return-object v0
.end method

.method public getReceiversOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55895
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->receivers_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 56064
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->memoizedSerializedSize:I

    .line 56065
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 56113
    :goto_0
    return v0

    .line 56068
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_b

    .line 56069
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getFolderBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/2addr v0, v3

    .line 56072
    :goto_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_1

    .line 56073
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getVideoMailIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 56076
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_2

    .line 56077
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getVideoMailUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 56080
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v6, :cond_3

    .line 56081
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->caller_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    move v1, v3

    move v2, v0

    .line 56084
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->receivers_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 56085
    const/4 v3, 0x5

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->receivers_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 56084
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 56088
    :cond_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_a

    .line 56089
    const/4 v0, 0x6

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->duration_:I

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v0

    add-int/2addr v0, v2

    .line 56092
    :goto_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_5

    .line 56093
    const/4 v1, 0x7

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->size_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 56096
    :cond_5
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_6

    .line 56097
    iget-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->timeCreated_:J

    invoke-static {v6, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 56100
    :cond_6
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_7

    .line 56101
    const/16 v1, 0x9

    iget-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->timeUploaded_:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 56104
    :cond_7
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_8

    .line 56105
    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->read_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 56108
    :cond_8
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_9

    .line 56109
    const/16 v1, 0xb

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->available_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 56112
    :cond_9
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->memoizedSerializedSize:I

    goto/16 :goto_0

    :cond_a
    move v0, v2

    goto :goto_3

    :cond_b
    move v0, v3

    goto/16 :goto_1
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 55925
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->size_:I

    return v0
.end method

.method public getTimeCreated()J
    .locals 2

    .prologue
    .line 55935
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->timeCreated_:J

    return-wide v0
.end method

.method public getTimeUploaded()J
    .locals 2

    .prologue
    .line 55945
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->timeUploaded_:J

    return-wide v0
.end method

.method public getVideoMailId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 55820
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->videoMailId_:Ljava/lang/Object;

    .line 55821
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 55822
    check-cast v0, Ljava/lang/String;

    .line 55830
    :goto_0
    return-object v0

    .line 55824
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 55826
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 55827
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55828
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->videoMailId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 55830
    goto :goto_0
.end method

.method public getVideoMailUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 55852
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->videoMailUrl_:Ljava/lang/Object;

    .line 55853
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 55854
    check-cast v0, Ljava/lang/String;

    .line 55862
    :goto_0
    return-object v0

    .line 55856
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 55858
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 55859
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55860
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->videoMailUrl_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 55862
    goto :goto_0
.end method

.method public hasAvailable()Z
    .locals 2

    .prologue
    .line 55962
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCaller()Z
    .locals 2

    .prologue
    .line 55881
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDuration()Z
    .locals 2

    .prologue
    .line 55912
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFolder()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 55785
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRead()Z
    .locals 2

    .prologue
    .line 55952
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSize()Z
    .locals 2

    .prologue
    .line 55922
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTimeCreated()Z
    .locals 2

    .prologue
    .line 55932
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTimeUploaded()Z
    .locals 2

    .prologue
    .line 55942
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoMailId()Z
    .locals 2

    .prologue
    .line 55817
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoMailUrl()Z
    .locals 2

    .prologue
    .line 55849
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 55983
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->memoizedIsInitialized:B

    .line 55984
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 56021
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 55984
    goto :goto_0

    .line 55986
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->hasVideoMailId()Z

    move-result v0

    if-nez v0, :cond_2

    .line 55987
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->memoizedIsInitialized:B

    move v0, v2

    .line 55988
    goto :goto_0

    .line 55990
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->hasCaller()Z

    move-result v0

    if-nez v0, :cond_3

    .line 55991
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->memoizedIsInitialized:B

    move v0, v2

    .line 55992
    goto :goto_0

    .line 55994
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->hasDuration()Z

    move-result v0

    if-nez v0, :cond_4

    .line 55995
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->memoizedIsInitialized:B

    move v0, v2

    .line 55996
    goto :goto_0

    .line 55998
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->hasSize()Z

    move-result v0

    if-nez v0, :cond_5

    .line 55999
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->memoizedIsInitialized:B

    move v0, v2

    .line 56000
    goto :goto_0

    .line 56002
    :cond_5
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->hasTimeCreated()Z

    move-result v0

    if-nez v0, :cond_6

    .line 56003
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->memoizedIsInitialized:B

    move v0, v2

    .line 56004
    goto :goto_0

    .line 56006
    :cond_6
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->hasTimeUploaded()Z

    move-result v0

    if-nez v0, :cond_7

    .line 56007
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->memoizedIsInitialized:B

    move v0, v2

    .line 56008
    goto :goto_0

    .line 56010
    :cond_7
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getCaller()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_8

    .line 56011
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->memoizedIsInitialized:B

    move v0, v2

    .line 56012
    goto :goto_0

    :cond_8
    move v0, v2

    .line 56014
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getReceiversCount()I

    move-result v1

    if-ge v0, v1, :cond_a

    .line 56015
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getReceivers(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_9

    .line 56016
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->memoizedIsInitialized:B

    move v0, v2

    .line 56017
    goto :goto_0

    .line 56014
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 56020
    :cond_a
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->memoizedIsInitialized:B

    move v0, v3

    .line 56021
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 55762
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1

    .prologue
    .line 56189
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 55762
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;
    .locals 1

    .prologue
    .line 56193
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 56118
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 56026
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getSerializedSize()I

    .line 56027
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 56028
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getFolderBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 56030
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 56031
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getVideoMailIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 56033
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 56034
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getVideoMailUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 56036
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 56037
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->caller_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 56039
    :cond_3
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->receivers_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 56040
    const/4 v2, 0x5

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->receivers_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 56039
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 56042
    :cond_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 56043
    const/4 v0, 0x6

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->duration_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 56045
    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 56046
    const/4 v0, 0x7

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->size_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 56048
    :cond_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_7

    .line 56049
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->timeCreated_:J

    invoke-virtual {p1, v4, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt64(IJ)V

    .line 56051
    :cond_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_8

    .line 56052
    const/16 v0, 0x9

    iget-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->timeUploaded_:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeUInt64(IJ)V

    .line 56054
    :cond_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_9

    .line 56055
    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->read_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 56057
    :cond_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_a

    .line 56058
    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->available_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 56060
    :cond_a
    return-void
.end method
