.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntryOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "VideoMailEntryOrBuilder"
.end annotation


# virtual methods
.method public abstract getAvailable()Z
.end method

.method public abstract getCaller()Lcom/sgiggle/xmpp/SessionMessages$Contact;
.end method

.method public abstract getDuration()I
.end method

.method public abstract getFolder()Ljava/lang/String;
.end method

.method public abstract getRead()Z
.end method

.method public abstract getReceivers(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
.end method

.method public abstract getReceiversCount()I
.end method

.method public abstract getReceiversList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getSize()I
.end method

.method public abstract getTimeCreated()J
.end method

.method public abstract getTimeUploaded()J
.end method

.method public abstract getVideoMailId()Ljava/lang/String;
.end method

.method public abstract getVideoMailUrl()Ljava/lang/String;
.end method

.method public abstract hasAvailable()Z
.end method

.method public abstract hasCaller()Z
.end method

.method public abstract hasDuration()Z
.end method

.method public abstract hasFolder()Z
.end method

.method public abstract hasRead()Z
.end method

.method public abstract hasSize()Z
.end method

.method public abstract hasTimeCreated()Z
.end method

.method public abstract hasTimeUploaded()Z
.end method

.method public abstract hasVideoMailId()Z
.end method

.method public abstract hasVideoMailUrl()Z
.end method
