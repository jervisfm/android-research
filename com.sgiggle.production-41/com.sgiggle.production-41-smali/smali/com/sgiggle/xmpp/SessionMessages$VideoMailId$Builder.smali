.class public final Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;",
        "Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private folder_:Ljava/lang/Object;

.field private videoMailId_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 55517
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 55631
    const-string v0, "_inbox"

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->folder_:Ljava/lang/Object;

    .line 55667
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->videoMailId_:Ljava/lang/Object;

    .line 55518
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->maybeForceBuilderInitialization()V

    .line 55519
    return-void
.end method

.method static synthetic access$71500(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 55512
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$71600()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;
    .locals 1

    .prologue
    .line 55512
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 55554
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    move-result-object v0

    .line 55555
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 55556
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 55559
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;
    .locals 1

    .prologue
    .line 55524
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 55522
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 55512
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
    .locals 2

    .prologue
    .line 55545
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    move-result-object v0

    .line 55546
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 55547
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 55549
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 55512
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
    .locals 5

    .prologue
    .line 55563
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;-><init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 55564
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->bitField0_:I

    .line 55565
    const/4 v2, 0x0

    .line 55566
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 55567
    or-int/lit8 v2, v2, 0x1

    .line 55569
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->folder_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->folder_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->access$71802(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55570
    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    .line 55571
    or-int/lit8 v1, v2, 0x2

    .line 55573
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->videoMailId_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->videoMailId_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->access$71902(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55574
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->access$72002(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;I)I

    .line 55575
    return-object v0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 55512
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 55512
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;
    .locals 1

    .prologue
    .line 55528
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 55529
    const-string v0, "_inbox"

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->folder_:Ljava/lang/Object;

    .line 55530
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->bitField0_:I

    .line 55531
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->videoMailId_:Ljava/lang/Object;

    .line 55532
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->bitField0_:I

    .line 55533
    return-object p0
.end method

.method public clearFolder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;
    .locals 1

    .prologue
    .line 55655
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->bitField0_:I

    .line 55656
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->getFolder()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->folder_:Ljava/lang/Object;

    .line 55658
    return-object p0
.end method

.method public clearVideoMailId()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;
    .locals 1

    .prologue
    .line 55691
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->bitField0_:I

    .line 55692
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->getVideoMailId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->videoMailId_:Ljava/lang/Object;

    .line 55694
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 55512
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 55512
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 55512
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;
    .locals 2

    .prologue
    .line 55537
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 55512
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 55512
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 55512
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
    .locals 1

    .prologue
    .line 55541
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    move-result-object v0

    return-object v0
.end method

.method public getFolder()Ljava/lang/String;
    .locals 2

    .prologue
    .line 55636
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->folder_:Ljava/lang/Object;

    .line 55637
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 55638
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 55639
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->folder_:Ljava/lang/Object;

    .line 55642
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getVideoMailId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 55672
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->videoMailId_:Ljava/lang/Object;

    .line 55673
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 55674
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 55675
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->videoMailId_:Ljava/lang/Object;

    .line 55678
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasFolder()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 55633
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoMailId()Z
    .locals 2

    .prologue
    .line 55669
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 55590
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->hasVideoMailId()Z

    move-result v0

    if-nez v0, :cond_0

    .line 55592
    const/4 v0, 0x0

    .line 55594
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55512
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 55512
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55512
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55602
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 55603
    sparse-switch v0, :sswitch_data_0

    .line 55608
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 55610
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 55606
    goto :goto_1

    .line 55615
    :sswitch_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->bitField0_:I

    .line 55616
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->folder_:Ljava/lang/Object;

    goto :goto_0

    .line 55620
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->bitField0_:I

    .line 55621
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->videoMailId_:Ljava/lang/Object;

    goto :goto_0

    .line 55603
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;
    .locals 1
    .parameter

    .prologue
    .line 55579
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 55586
    :goto_0
    return-object v0

    .line 55580
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->hasFolder()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55581
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->getFolder()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->setFolder(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    .line 55583
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->hasVideoMailId()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 55584
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->getVideoMailId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->setVideoMailId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    :cond_2
    move-object v0, p0

    .line 55586
    goto :goto_0
.end method

.method public setFolder(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;
    .locals 1
    .parameter

    .prologue
    .line 55646
    if-nez p1, :cond_0

    .line 55647
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 55649
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->bitField0_:I

    .line 55650
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->folder_:Ljava/lang/Object;

    .line 55652
    return-object p0
.end method

.method setFolder(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 55661
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->bitField0_:I

    .line 55662
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->folder_:Ljava/lang/Object;

    .line 55664
    return-void
.end method

.method public setVideoMailId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;
    .locals 1
    .parameter

    .prologue
    .line 55682
    if-nez p1, :cond_0

    .line 55683
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 55685
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->bitField0_:I

    .line 55686
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->videoMailId_:Ljava/lang/Object;

    .line 55688
    return-object p0
.end method

.method setVideoMailId(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 55697
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->bitField0_:I

    .line 55698
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->videoMailId_:Ljava/lang/Object;

    .line 55700
    return-void
.end method
