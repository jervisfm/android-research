.class public final Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VideoMailId"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;
    }
.end annotation


# static fields
.field public static final FOLDER_FIELD_NUMBER:I = 0x1

.field public static final VIDEO_MAIL_ID_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;


# instance fields
.field private bitField0_:I

.field private folder_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private videoMailId_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 55706
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    .line 55707
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->initFields()V

    .line 55708
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 55309
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 55391
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->memoizedIsInitialized:B

    .line 55415
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->memoizedSerializedSize:I

    .line 55310
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 55304
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;-><init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 55311
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 55391
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->memoizedIsInitialized:B

    .line 55415
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->memoizedSerializedSize:I

    .line 55311
    return-void
.end method

.method static synthetic access$71802(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 55304
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->folder_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$71902(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 55304
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->videoMailId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$72002(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 55304
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
    .locals 1

    .prologue
    .line 55315
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    return-object v0
.end method

.method private getFolderBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 55344
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->folder_:Ljava/lang/Object;

    .line 55345
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 55346
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 55348
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->folder_:Ljava/lang/Object;

    .line 55351
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getVideoMailIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 55376
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->videoMailId_:Ljava/lang/Object;

    .line 55377
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 55378
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 55380
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->videoMailId_:Ljava/lang/Object;

    .line 55383
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 55388
    const-string v0, "_inbox"

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->folder_:Ljava/lang/Object;

    .line 55389
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->videoMailId_:Ljava/lang/Object;

    .line 55390
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;
    .locals 1

    .prologue
    .line 55505
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->access$71600()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;
    .locals 1
    .parameter

    .prologue
    .line 55508
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55474
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    .line 55475
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 55476
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->access$71500(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    move-result-object v0

    .line 55478
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55485
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    .line 55486
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 55487
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->access$71500(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    move-result-object v0

    .line 55489
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 55441
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->access$71500(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 55447
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->access$71500(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55495
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->access$71500(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55501
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->access$71500(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55463
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->access$71500(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55469
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->access$71500(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 55452
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->access$71500(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 55458
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->access$71500(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 55304
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
    .locals 1

    .prologue
    .line 55319
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    return-object v0
.end method

.method public getFolder()Ljava/lang/String;
    .locals 2

    .prologue
    .line 55330
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->folder_:Ljava/lang/Object;

    .line 55331
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 55332
    check-cast v0, Ljava/lang/String;

    .line 55340
    :goto_0
    return-object v0

    .line 55334
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 55336
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 55337
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55338
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->folder_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 55340
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 55417
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->memoizedSerializedSize:I

    .line 55418
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 55430
    :goto_0
    return v0

    .line 55420
    :cond_0
    const/4 v0, 0x0

    .line 55421
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 55422
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->getFolderBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 55425
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 55426
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->getVideoMailIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 55429
    :cond_2
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getVideoMailId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 55362
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->videoMailId_:Ljava/lang/Object;

    .line 55363
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 55364
    check-cast v0, Ljava/lang/String;

    .line 55372
    :goto_0
    return-object v0

    .line 55366
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 55368
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 55369
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55370
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->videoMailId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 55372
    goto :goto_0
.end method

.method public hasFolder()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 55327
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoMailId()Z
    .locals 2

    .prologue
    .line 55359
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 55393
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->memoizedIsInitialized:B

    .line 55394
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v2, :cond_0

    move v0, v2

    .line 55401
    :goto_0
    return v0

    :cond_0
    move v0, v3

    .line 55394
    goto :goto_0

    .line 55396
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->hasVideoMailId()Z

    move-result v0

    if-nez v0, :cond_2

    .line 55397
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->memoizedIsInitialized:B

    move v0, v3

    .line 55398
    goto :goto_0

    .line 55400
    :cond_2
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->memoizedIsInitialized:B

    move v0, v2

    .line 55401
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 55304
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;
    .locals 1

    .prologue
    .line 55506
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 55304
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;
    .locals 1

    .prologue
    .line 55510
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 55435
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 55406
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->getSerializedSize()I

    .line 55407
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 55408
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->getFolderBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 55410
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 55411
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->getVideoMailIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 55413
    :cond_1
    return-void
.end method
