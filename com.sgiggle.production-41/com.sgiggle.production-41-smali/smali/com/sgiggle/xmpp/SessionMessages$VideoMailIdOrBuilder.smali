.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "VideoMailIdOrBuilder"
.end annotation


# virtual methods
.method public abstract getFolder()Ljava/lang/String;
.end method

.method public abstract getVideoMailId()Ljava/lang/String;
.end method

.method public abstract hasFolder()Z
.end method

.method public abstract hasVideoMailId()Z
.end method
