.class public final Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private folder_:Ljava/lang/Object;

.field private videoMailId_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 64376
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 64516
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 64559
    const-string v0, "_inbox"

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->folder_:Ljava/lang/Object;

    .line 64595
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 64377
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->maybeForceBuilderInitialization()V

    .line 64378
    return-void
.end method

.method static synthetic access$82800(Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 64371
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$82900()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;
    .locals 1

    .prologue
    .line 64371
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 64415
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;

    move-result-object v0

    .line 64416
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 64417
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 64420
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;
    .locals 1

    .prologue
    .line 64383
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 64381
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 64371
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;
    .locals 2

    .prologue
    .line 64406
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;

    move-result-object v0

    .line 64407
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 64408
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 64410
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 64371
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;
    .locals 5

    .prologue
    .line 64424
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 64425
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    .line 64426
    const/4 v2, 0x0

    .line 64427
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 64428
    or-int/lit8 v2, v2, 0x1

    .line 64430
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->access$83102(Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 64431
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 64432
    or-int/lit8 v2, v2, 0x2

    .line 64434
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->folder_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->folder_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->access$83202(Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64435
    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_2

    .line 64436
    or-int/lit8 v1, v2, 0x4

    .line 64438
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->videoMailId_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->videoMailId_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->access$83302(Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64439
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->access$83402(Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;I)I

    .line 64440
    return-object v0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 64371
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 64371
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;
    .locals 1

    .prologue
    .line 64387
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 64388
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 64389
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    .line 64390
    const-string v0, "_inbox"

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->folder_:Ljava/lang/Object;

    .line 64391
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    .line 64392
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 64393
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    .line 64394
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;
    .locals 1

    .prologue
    .line 64552
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 64554
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    .line 64555
    return-object p0
.end method

.method public clearFolder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;
    .locals 1

    .prologue
    .line 64583
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    .line 64584
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->getFolder()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->folder_:Ljava/lang/Object;

    .line 64586
    return-object p0
.end method

.method public clearVideoMailId()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;
    .locals 1

    .prologue
    .line 64619
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    .line 64620
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->getVideoMailId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 64622
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 64371
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 64371
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 64371
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;
    .locals 2

    .prologue
    .line 64398
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 64371
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 64521
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 64371
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 64371
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;
    .locals 1

    .prologue
    .line 64402
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;

    move-result-object v0

    return-object v0
.end method

.method public getFolder()Ljava/lang/String;
    .locals 2

    .prologue
    .line 64564
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->folder_:Ljava/lang/Object;

    .line 64565
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 64566
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 64567
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->folder_:Ljava/lang/Object;

    .line 64570
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getVideoMailId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 64600
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 64601
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 64602
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 64603
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 64606
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 64518
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFolder()Z
    .locals 2

    .prologue
    .line 64561
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoMailId()Z
    .locals 2

    .prologue
    .line 64597
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 64458
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 64470
    :goto_0
    return v0

    .line 64462
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->hasVideoMailId()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 64464
    goto :goto_0

    .line 64466
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 64468
    goto :goto_0

    .line 64470
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 64540
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 64542
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 64548
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    .line 64549
    return-object p0

    .line 64545
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64371
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 64371
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64371
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64478
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 64479
    sparse-switch v0, :sswitch_data_0

    .line 64484
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 64486
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 64482
    goto :goto_1

    .line 64491
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 64492
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 64493
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 64495
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 64496
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    goto :goto_0

    .line 64500
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    .line 64501
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->folder_:Ljava/lang/Object;

    goto :goto_0

    .line 64505
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    .line 64506
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->videoMailId_:Ljava/lang/Object;

    goto :goto_0

    .line 64479
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 64444
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 64454
    :goto_0
    return-object v0

    .line 64445
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64446
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    .line 64448
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->hasFolder()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 64449
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->getFolder()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->setFolder(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    .line 64451
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->hasVideoMailId()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 64452
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->getVideoMailId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->setVideoMailId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    :cond_3
    move-object v0, p0

    .line 64454
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 64534
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 64536
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    .line 64537
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 64524
    if-nez p1, :cond_0

    .line 64525
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 64527
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 64529
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    .line 64530
    return-object p0
.end method

.method public setFolder(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 64574
    if-nez p1, :cond_0

    .line 64575
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 64577
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    .line 64578
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->folder_:Ljava/lang/Object;

    .line 64580
    return-object p0
.end method

.method setFolder(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 64589
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    .line 64590
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->folder_:Ljava/lang/Object;

    .line 64592
    return-void
.end method

.method public setVideoMailId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 64610
    if-nez p1, :cond_0

    .line 64611
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 64613
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    .line 64614
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 64616
    return-object p0
.end method

.method setVideoMailId(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 64625
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->bitField0_:I

    .line 64626
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 64628
    return-void
.end method
