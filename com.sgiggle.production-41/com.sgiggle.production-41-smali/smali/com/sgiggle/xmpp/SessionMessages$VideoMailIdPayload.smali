.class public final Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VideoMailIdPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final FOLDER_FIELD_NUMBER:I = 0x2

.field public static final VIDEO_MAIL_ID_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private folder_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private videoMailId_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 64634
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;

    .line 64635
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->initFields()V

    .line 64636
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 64142
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 64235
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->memoizedIsInitialized:B

    .line 64270
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->memoizedSerializedSize:I

    .line 64143
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 64137
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 64144
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 64235
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->memoizedIsInitialized:B

    .line 64270
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->memoizedSerializedSize:I

    .line 64144
    return-void
.end method

.method static synthetic access$83102(Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 64137
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$83202(Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 64137
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->folder_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$83302(Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 64137
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->videoMailId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$83402(Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 64137
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;
    .locals 1

    .prologue
    .line 64148
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;

    return-object v0
.end method

.method private getFolderBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 64187
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->folder_:Ljava/lang/Object;

    .line 64188
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 64189
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 64191
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->folder_:Ljava/lang/Object;

    .line 64194
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getVideoMailIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 64219
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->videoMailId_:Ljava/lang/Object;

    .line 64220
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 64221
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 64223
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->videoMailId_:Ljava/lang/Object;

    .line 64226
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 64231
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 64232
    const-string v0, "_inbox"

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->folder_:Ljava/lang/Object;

    .line 64233
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->videoMailId_:Ljava/lang/Object;

    .line 64234
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;
    .locals 1

    .prologue
    .line 64364
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->access$82900()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 64367
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64333
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    move-result-object v0

    .line 64334
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 64335
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->access$82800(Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;

    move-result-object v0

    .line 64337
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64344
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    move-result-object v0

    .line 64345
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 64346
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->access$82800(Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;

    move-result-object v0

    .line 64348
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 64300
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->access$82800(Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 64306
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->access$82800(Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64354
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->access$82800(Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64360
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->access$82800(Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64322
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->access$82800(Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64328
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->access$82800(Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 64311
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->access$82800(Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 64317
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;->access$82800(Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 64163
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 64137
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;
    .locals 1

    .prologue
    .line 64152
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;

    return-object v0
.end method

.method public getFolder()Ljava/lang/String;
    .locals 2

    .prologue
    .line 64173
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->folder_:Ljava/lang/Object;

    .line 64174
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 64175
    check-cast v0, Ljava/lang/String;

    .line 64183
    :goto_0
    return-object v0

    .line 64177
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 64179
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 64180
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64181
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->folder_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 64183
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 64272
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->memoizedSerializedSize:I

    .line 64273
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 64289
    :goto_0
    return v0

    .line 64275
    :cond_0
    const/4 v0, 0x0

    .line 64276
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 64277
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64280
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 64281
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->getFolderBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64284
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 64285
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->getVideoMailIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64288
    :cond_3
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getVideoMailId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 64205
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->videoMailId_:Ljava/lang/Object;

    .line 64206
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 64207
    check-cast v0, Ljava/lang/String;

    .line 64215
    :goto_0
    return-object v0

    .line 64209
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 64211
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 64212
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64213
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->videoMailId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 64215
    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 64160
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFolder()Z
    .locals 2

    .prologue
    .line 64170
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoMailId()Z
    .locals 2

    .prologue
    .line 64202
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 64237
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->memoizedIsInitialized:B

    .line 64238
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 64253
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 64238
    goto :goto_0

    .line 64240
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 64241
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 64242
    goto :goto_0

    .line 64244
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->hasVideoMailId()Z

    move-result v0

    if-nez v0, :cond_3

    .line 64245
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 64246
    goto :goto_0

    .line 64248
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    .line 64249
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 64250
    goto :goto_0

    .line 64252
    :cond_4
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 64253
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 64137
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;
    .locals 1

    .prologue
    .line 64365
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 64137
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;
    .locals 1

    .prologue
    .line 64369
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 64294
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 64258
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->getSerializedSize()I

    .line 64259
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 64260
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 64262
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 64263
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->getFolderBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 64265
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 64266
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdPayload;->getVideoMailIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 64268
    :cond_2
    return-void
.end method
