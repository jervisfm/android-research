.class public final Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VideoMailNonTangoNotificationPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CALLEES_FIELD_NUMBER:I = 0x6

.field public static final FOLDER_FIELD_NUMBER:I = 0x2

.field public static final NON_TANGO_URL_FIELD_NUMBER:I = 0x5

.field public static final SUCCESS_FIELD_NUMBER:I = 0x7

.field public static final TYPE_FIELD_NUMBER:I = 0x4

.field public static final VIDEO_MAIL_ID_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private callees_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private folder_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private nonTangoUrl_:Ljava/lang/Object;

.field private success_:Z

.field private type_:Lcom/sgiggle/xmpp/SessionMessages$MediaType;

.field private videoMailId_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->initFields()V

    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$88002(Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$88102(Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->folder_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$88202(Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->videoMailId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$88302(Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;Lcom/sgiggle/xmpp/SessionMessages$MediaType;)Lcom/sgiggle/xmpp/SessionMessages$MediaType;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    return-object p1
.end method

.method static synthetic access$88402(Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->nonTangoUrl_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$88500(Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->callees_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$88502(Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->callees_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$88602(Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->success_:Z

    return p1
.end method

.method static synthetic access$88702(Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;

    return-object v0
.end method

.method private getFolderBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->folder_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->folder_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getNonTangoUrlBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->nonTangoUrl_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->nonTangoUrl_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getVideoMailIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->videoMailId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->videoMailId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->folder_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->videoMailId_:Ljava/lang/Object;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaType;->SMS:Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->nonTangoUrl_:Ljava/lang/Object;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->callees_:Ljava/util/List;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->success_:Z

    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;
    .locals 1

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->access$87800()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->access$87700(Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->access$87700(Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->access$87700(Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->access$87700(Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->access$87700(Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->access$87700(Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->access$87700(Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->access$87700(Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->access$87700(Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;->access$87700(Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCallees(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->callees_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getCalleesCount()I
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->callees_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCalleesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->callees_:Ljava/util/List;

    return-object v0
.end method

.method public getCalleesOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->callees_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;

    return-object v0
.end method

.method public getCalleesOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->callees_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;

    return-object v0
.end method

.method public getFolder()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->folder_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->folder_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getNonTangoUrl()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->nonTangoUrl_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->nonTangoUrl_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_7

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v3

    :goto_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_1

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->getFolderBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_2

    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->getVideoMailIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$MediaType;->getNumber()I

    move-result v1

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_4

    const/4 v1, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->getNonTangoUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    move v1, v3

    move v2, v0

    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->callees_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    const/4 v3, 0x6

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->callees_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->success_:Z

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v0

    add-int/2addr v0, v2

    :goto_3
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->memoizedSerializedSize:I

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_3

    :cond_7
    move v0, v3

    goto :goto_1
.end method

.method public getSuccess()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->success_:Z

    return v0
.end method

.method public getType()Lcom/sgiggle/xmpp/SessionMessages$MediaType;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    return-object v0
.end method

.method public getVideoMailId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->videoMailId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->videoMailId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFolder()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNonTangoUrl()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSuccess()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoMailId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->memoizedIsInitialized:B

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->hasVideoMailId()Z

    move-result v0

    if-nez v0, :cond_3

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_4
    move v0, v2

    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->getCalleesCount()I

    move-result v1

    if-ge v0, v1, :cond_6

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->getCallees(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_5

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_6
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->memoizedIsInitialized:B

    move v0, v3

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;
    .locals 1

    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->getSerializedSize()I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->getFolderBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->getVideoMailIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaType;->getNumber()I

    move-result v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->getNonTangoUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_4
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->callees_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    const/4 v2, 0x6

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->callees_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->success_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_6
    return-void
.end method
