.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$VideoMailReceiversPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "VideoMailReceiversPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getCallees(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
.end method

.method public abstract getCalleesCount()I
.end method

.method public abstract getCalleesList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getExtraInfo()Ljava/lang/String;
.end method

.method public abstract getType()Lcom/sgiggle/xmpp/SessionMessages$MediaType;
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasExtraInfo()Z
.end method

.method public abstract hasType()Z
.end method
