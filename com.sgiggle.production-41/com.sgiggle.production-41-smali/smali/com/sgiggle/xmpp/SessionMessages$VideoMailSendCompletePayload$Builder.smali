.class public final Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private callees_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private folder_:Ljava/lang/Object;

.field private nonTangoUrl_:Ljava/lang/Object;

.field private videoMailId_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 63685
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 63872
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 63915
    const-string v0, "_inbox"

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->folder_:Ljava/lang/Object;

    .line 63951
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 63987
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->callees_:Ljava/util/List;

    .line 64076
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->nonTangoUrl_:Ljava/lang/Object;

    .line 63686
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->maybeForceBuilderInitialization()V

    .line 63687
    return-void
.end method

.method static synthetic access$81900(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 63680
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$82000()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    .locals 1

    .prologue
    .line 63680
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 63728
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    move-result-object v0

    .line 63729
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 63730
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 63733
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    .locals 1

    .prologue
    .line 63692
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureCalleesIsMutable()V
    .locals 2

    .prologue
    .line 63990
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 63991
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->callees_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->callees_:Ljava/util/List;

    .line 63992
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    .line 63994
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 63690
    return-void
.end method


# virtual methods
.method public addAllCallees(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;"
        }
    .end annotation

    .prologue
    .line 64057
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->ensureCalleesIsMutable()V

    .line 64058
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->callees_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 64060
    return-object p0
.end method

.method public addCallees(ILcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 64050
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->ensureCalleesIsMutable()V

    .line 64051
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->callees_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 64053
    return-object p0
.end method

.method public addCallees(ILcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 64033
    if-nez p2, :cond_0

    .line 64034
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 64036
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->ensureCalleesIsMutable()V

    .line 64037
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 64039
    return-object p0
.end method

.method public addCallees(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 64043
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->ensureCalleesIsMutable()V

    .line 64044
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->callees_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64046
    return-object p0
.end method

.method public addCallees(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 64023
    if-nez p1, :cond_0

    .line 64024
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 64026
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->ensureCalleesIsMutable()V

    .line 64027
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64029
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 63680
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;
    .locals 2

    .prologue
    .line 63719
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    move-result-object v0

    .line 63720
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 63721
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 63723
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 63680
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;
    .locals 5

    .prologue
    .line 63737
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 63738
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    .line 63739
    const/4 v2, 0x0

    .line 63740
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 63741
    or-int/lit8 v2, v2, 0x1

    .line 63743
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->access$82202(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 63744
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 63745
    or-int/lit8 v2, v2, 0x2

    .line 63747
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->folder_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->folder_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->access$82302(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63748
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 63749
    or-int/lit8 v2, v2, 0x4

    .line 63751
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->videoMailId_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->videoMailId_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->access$82402(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63752
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 63753
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->callees_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->callees_:Ljava/util/List;

    .line 63754
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x9

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    .line 63756
    :cond_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->callees_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->callees_:Ljava/util/List;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->access$82502(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;Ljava/util/List;)Ljava/util/List;

    .line 63757
    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_4

    .line 63758
    or-int/lit8 v1, v2, 0x8

    .line 63760
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->nonTangoUrl_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->nonTangoUrl_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->access$82602(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63761
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->access$82702(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;I)I

    .line 63762
    return-object v0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 63680
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 63680
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    .locals 1

    .prologue
    .line 63696
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 63697
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 63698
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    .line 63699
    const-string v0, "_inbox"

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->folder_:Ljava/lang/Object;

    .line 63700
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    .line 63701
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 63702
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    .line 63703
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->callees_:Ljava/util/List;

    .line 63704
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    .line 63705
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->nonTangoUrl_:Ljava/lang/Object;

    .line 63706
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    .line 63707
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    .locals 1

    .prologue
    .line 63908
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 63910
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    .line 63911
    return-object p0
.end method

.method public clearCallees()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    .locals 1

    .prologue
    .line 64063
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->callees_:Ljava/util/List;

    .line 64064
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    .line 64066
    return-object p0
.end method

.method public clearFolder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    .locals 1

    .prologue
    .line 63939
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    .line 63940
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->getFolder()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->folder_:Ljava/lang/Object;

    .line 63942
    return-object p0
.end method

.method public clearNonTangoUrl()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    .locals 1

    .prologue
    .line 64100
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    .line 64101
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->getNonTangoUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->nonTangoUrl_:Ljava/lang/Object;

    .line 64103
    return-object p0
.end method

.method public clearVideoMailId()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    .locals 1

    .prologue
    .line 63975
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    .line 63976
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->getVideoMailId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 63978
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 63680
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 63680
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 63680
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    .locals 2

    .prologue
    .line 63711
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 63680
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 63877
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCallees(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter

    .prologue
    .line 64003
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getCalleesCount()I
    .locals 1

    .prologue
    .line 64000
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCalleesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63997
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->callees_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 63680
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 63680
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;
    .locals 1

    .prologue
    .line 63715
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    move-result-object v0

    return-object v0
.end method

.method public getFolder()Ljava/lang/String;
    .locals 2

    .prologue
    .line 63920
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->folder_:Ljava/lang/Object;

    .line 63921
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 63922
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 63923
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->folder_:Ljava/lang/Object;

    .line 63926
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getNonTangoUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 64081
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->nonTangoUrl_:Ljava/lang/Object;

    .line 64082
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 64083
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 64084
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->nonTangoUrl_:Ljava/lang/Object;

    .line 64087
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getVideoMailId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 63956
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 63957
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 63958
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 63959
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 63962
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 63874
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFolder()Z
    .locals 2

    .prologue
    .line 63917
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNonTangoUrl()Z
    .locals 2

    .prologue
    .line 64078
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoMailId()Z
    .locals 2

    .prologue
    .line 63953
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 63793
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 63815
    :goto_0
    return v0

    .line 63797
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->hasVideoMailId()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 63799
    goto :goto_0

    .line 63801
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->hasNonTangoUrl()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    .line 63803
    goto :goto_0

    .line 63805
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    .line 63807
    goto :goto_0

    :cond_3
    move v0, v2

    .line 63809
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->getCalleesCount()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 63810
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->getCallees(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_4

    move v0, v2

    .line 63812
    goto :goto_0

    .line 63809
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 63815
    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 63896
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 63898
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 63904
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    .line 63905
    return-object p0

    .line 63901
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63680
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 63680
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63680
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63823
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 63824
    sparse-switch v0, :sswitch_data_0

    .line 63829
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 63831
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 63827
    goto :goto_1

    .line 63836
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 63837
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 63838
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 63840
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 63841
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    goto :goto_0

    .line 63845
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    .line 63846
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->folder_:Ljava/lang/Object;

    goto :goto_0

    .line 63850
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    .line 63851
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->videoMailId_:Ljava/lang/Object;

    goto :goto_0

    .line 63855
    :sswitch_4
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    .line 63856
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 63857
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->addCallees(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    goto :goto_0

    .line 63861
    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    .line 63862
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->nonTangoUrl_:Ljava/lang/Object;

    goto :goto_0

    .line 63824
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 63766
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 63789
    :goto_0
    return-object v0

    .line 63767
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63768
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    .line 63770
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->hasFolder()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 63771
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->getFolder()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->setFolder(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    .line 63773
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->hasVideoMailId()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 63774
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->getVideoMailId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->setVideoMailId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    .line 63776
    :cond_3
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->callees_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->access$82500(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 63777
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 63778
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->callees_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->access$82500(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->callees_:Ljava/util/List;

    .line 63779
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    .line 63786
    :cond_4
    :goto_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->hasNonTangoUrl()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 63787
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->getNonTangoUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->setNonTangoUrl(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    :cond_5
    move-object v0, p0

    .line 63789
    goto :goto_0

    .line 63781
    :cond_6
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->ensureCalleesIsMutable()V

    .line 63782
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->callees_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->callees_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->access$82500(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public removeCallees(I)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 64069
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->ensureCalleesIsMutable()V

    .line 64070
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 64072
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 63890
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 63892
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    .line 63893
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 63880
    if-nez p1, :cond_0

    .line 63881
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 63883
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 63885
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    .line 63886
    return-object p0
.end method

.method public setCallees(ILcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 64017
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->ensureCalleesIsMutable()V

    .line 64018
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->callees_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 64020
    return-object p0
.end method

.method public setCallees(ILcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 64007
    if-nez p2, :cond_0

    .line 64008
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 64010
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->ensureCalleesIsMutable()V

    .line 64011
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 64013
    return-object p0
.end method

.method public setFolder(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 63930
    if-nez p1, :cond_0

    .line 63931
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 63933
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    .line 63934
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->folder_:Ljava/lang/Object;

    .line 63936
    return-object p0
.end method

.method setFolder(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 63945
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    .line 63946
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->folder_:Ljava/lang/Object;

    .line 63948
    return-void
.end method

.method public setNonTangoUrl(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 64091
    if-nez p1, :cond_0

    .line 64092
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 64094
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    .line 64095
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->nonTangoUrl_:Ljava/lang/Object;

    .line 64097
    return-object p0
.end method

.method setNonTangoUrl(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 64106
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    .line 64107
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->nonTangoUrl_:Ljava/lang/Object;

    .line 64109
    return-void
.end method

.method public setVideoMailId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 63966
    if-nez p1, :cond_0

    .line 63967
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 63969
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    .line 63970
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 63972
    return-object p0
.end method

.method setVideoMailId(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 63981
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->bitField0_:I

    .line 63982
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 63984
    return-void
.end method
