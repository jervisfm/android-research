.class public final Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VideoMailSendCompletePayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CALLEES_FIELD_NUMBER:I = 0x4

.field public static final FOLDER_FIELD_NUMBER:I = 0x2

.field public static final NON_TANGO_URL_FIELD_NUMBER:I = 0x5

.field public static final VIDEO_MAIL_ID_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private callees_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private folder_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private nonTangoUrl_:Ljava/lang/Object;

.field private videoMailId_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 64115
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    .line 64116
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->initFields()V

    .line 64117
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 63372
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 63520
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->memoizedIsInitialized:B

    .line 63571
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->memoizedSerializedSize:I

    .line 63373
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 63367
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 63374
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 63520
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->memoizedIsInitialized:B

    .line 63571
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->memoizedSerializedSize:I

    .line 63374
    return-void
.end method

.method static synthetic access$82202(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 63367
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$82302(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 63367
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->folder_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$82402(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 63367
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->videoMailId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$82500(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 63367
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->callees_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$82502(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 63367
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->callees_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$82602(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 63367
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->nonTangoUrl_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$82702(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 63367
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;
    .locals 1

    .prologue
    .line 63378
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    return-object v0
.end method

.method private getFolderBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 63417
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->folder_:Ljava/lang/Object;

    .line 63418
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 63419
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 63421
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->folder_:Ljava/lang/Object;

    .line 63424
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getNonTangoUrlBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 63502
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->nonTangoUrl_:Ljava/lang/Object;

    .line 63503
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 63504
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 63506
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->nonTangoUrl_:Ljava/lang/Object;

    .line 63509
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getVideoMailIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 63449
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->videoMailId_:Ljava/lang/Object;

    .line 63450
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 63451
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 63453
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->videoMailId_:Ljava/lang/Object;

    .line 63456
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 63514
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 63515
    const-string v0, "_inbox"

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->folder_:Ljava/lang/Object;

    .line 63516
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->videoMailId_:Ljava/lang/Object;

    .line 63517
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->callees_:Ljava/util/List;

    .line 63518
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->nonTangoUrl_:Ljava/lang/Object;

    .line 63519
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    .locals 1

    .prologue
    .line 63673
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->access$82000()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 63676
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63642
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    move-result-object v0

    .line 63643
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 63644
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->access$81900(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    move-result-object v0

    .line 63646
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63653
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    move-result-object v0

    .line 63654
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 63655
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->access$81900(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    move-result-object v0

    .line 63657
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 63609
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->access$81900(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 63615
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->access$81900(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63663
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->access$81900(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63669
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->access$81900(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63631
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->access$81900(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63637
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->access$81900(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 63620
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->access$81900(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 63626
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;->access$81900(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 63393
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCallees(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter

    .prologue
    .line 63474
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->callees_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getCalleesCount()I
    .locals 1

    .prologue
    .line 63471
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->callees_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCalleesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63464
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->callees_:Ljava/util/List;

    return-object v0
.end method

.method public getCalleesOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 63478
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->callees_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;

    return-object v0
.end method

.method public getCalleesOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63468
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->callees_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 63367
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;
    .locals 1

    .prologue
    .line 63382
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;

    return-object v0
.end method

.method public getFolder()Ljava/lang/String;
    .locals 2

    .prologue
    .line 63403
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->folder_:Ljava/lang/Object;

    .line 63404
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 63405
    check-cast v0, Ljava/lang/String;

    .line 63413
    :goto_0
    return-object v0

    .line 63407
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 63409
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 63410
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63411
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->folder_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 63413
    goto :goto_0
.end method

.method public getNonTangoUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 63488
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->nonTangoUrl_:Ljava/lang/Object;

    .line 63489
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 63490
    check-cast v0, Ljava/lang/String;

    .line 63498
    :goto_0
    return-object v0

    .line 63492
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 63494
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 63495
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63496
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->nonTangoUrl_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 63498
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 63573
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->memoizedSerializedSize:I

    .line 63574
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 63598
    :goto_0
    return v0

    .line 63577
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_5

    .line 63578
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v3

    .line 63581
    :goto_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_1

    .line 63582
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->getFolderBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63585
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_2

    .line 63586
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->getVideoMailIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    move v1, v3

    move v2, v0

    .line 63589
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->callees_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 63590
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->callees_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v5, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 63589
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 63593
    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 63594
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->getNonTangoUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/2addr v0, v2

    .line 63597
    :goto_3
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->memoizedSerializedSize:I

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    move v0, v3

    goto :goto_1
.end method

.method public getVideoMailId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 63435
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->videoMailId_:Ljava/lang/Object;

    .line 63436
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 63437
    check-cast v0, Ljava/lang/String;

    .line 63445
    :goto_0
    return-object v0

    .line 63439
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 63441
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 63442
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63443
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->videoMailId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 63445
    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 63390
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFolder()Z
    .locals 2

    .prologue
    .line 63400
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNonTangoUrl()Z
    .locals 2

    .prologue
    .line 63485
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoMailId()Z
    .locals 2

    .prologue
    .line 63432
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 63522
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->memoizedIsInitialized:B

    .line 63523
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 63548
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 63523
    goto :goto_0

    .line 63525
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 63526
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 63527
    goto :goto_0

    .line 63529
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->hasVideoMailId()Z

    move-result v0

    if-nez v0, :cond_3

    .line 63530
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 63531
    goto :goto_0

    .line 63533
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->hasNonTangoUrl()Z

    move-result v0

    if-nez v0, :cond_4

    .line 63534
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 63535
    goto :goto_0

    .line 63537
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_5

    .line 63538
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 63539
    goto :goto_0

    :cond_5
    move v0, v2

    .line 63541
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->getCalleesCount()I

    move-result v1

    if-ge v0, v1, :cond_7

    .line 63542
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->getCallees(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_6

    .line 63543
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 63544
    goto :goto_0

    .line 63541
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 63547
    :cond_7
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->memoizedIsInitialized:B

    move v0, v3

    .line 63548
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 63367
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    .locals 1

    .prologue
    .line 63674
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 63367
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;
    .locals 1

    .prologue
    .line 63678
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 63603
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 63553
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->getSerializedSize()I

    .line 63554
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 63555
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 63557
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 63558
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->getFolderBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 63560
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 63561
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->getVideoMailIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 63563
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->callees_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 63564
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->callees_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 63563
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 63566
    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 63567
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailSendCompletePayload;->getNonTangoUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 63569
    :cond_4
    return-void
.end method
