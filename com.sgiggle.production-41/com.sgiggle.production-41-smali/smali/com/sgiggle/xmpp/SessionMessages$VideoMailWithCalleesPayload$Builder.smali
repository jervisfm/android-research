.class public final Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private callees_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private folder_:Ljava/lang/Object;

.field private videoMailId_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 62959
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 63128
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 63171
    const-string v0, "_inbox"

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->folder_:Ljava/lang/Object;

    .line 63207
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 63243
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->callees_:Ljava/util/List;

    .line 62960
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->maybeForceBuilderInitialization()V

    .line 62961
    return-void
.end method

.method static synthetic access$81100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 62954
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$81200()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;
    .locals 1

    .prologue
    .line 62954
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 63000
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;

    move-result-object v0

    .line 63001
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 63002
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 63005
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;
    .locals 1

    .prologue
    .line 62966
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureCalleesIsMutable()V
    .locals 2

    .prologue
    .line 63246
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 63247
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->callees_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->callees_:Ljava/util/List;

    .line 63248
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    .line 63250
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 62964
    return-void
.end method


# virtual methods
.method public addAllCallees(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;"
        }
    .end annotation

    .prologue
    .line 63313
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->ensureCalleesIsMutable()V

    .line 63314
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->callees_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 63316
    return-object p0
.end method

.method public addCallees(ILcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 63306
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->ensureCalleesIsMutable()V

    .line 63307
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->callees_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 63309
    return-object p0
.end method

.method public addCallees(ILcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 63289
    if-nez p2, :cond_0

    .line 63290
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 63292
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->ensureCalleesIsMutable()V

    .line 63293
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 63295
    return-object p0
.end method

.method public addCallees(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 63299
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->ensureCalleesIsMutable()V

    .line 63300
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->callees_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63302
    return-object p0
.end method

.method public addCallees(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 63279
    if-nez p1, :cond_0

    .line 63280
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 63282
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->ensureCalleesIsMutable()V

    .line 63283
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63285
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 62954
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;
    .locals 2

    .prologue
    .line 62991
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;

    move-result-object v0

    .line 62992
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 62993
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 62995
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 62954
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;
    .locals 5

    .prologue
    .line 63009
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 63010
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    .line 63011
    const/4 v2, 0x0

    .line 63012
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 63013
    or-int/lit8 v2, v2, 0x1

    .line 63015
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->access$81402(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 63016
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 63017
    or-int/lit8 v2, v2, 0x2

    .line 63019
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->folder_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->folder_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->access$81502(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63020
    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_3

    .line 63021
    or-int/lit8 v1, v2, 0x4

    .line 63023
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->videoMailId_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->videoMailId_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->access$81602(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63024
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_2

    .line 63025
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->callees_:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->callees_:Ljava/util/List;

    .line 63026
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    and-int/lit8 v2, v2, -0x9

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    .line 63028
    :cond_2
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->callees_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->callees_:Ljava/util/List;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->access$81702(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;Ljava/util/List;)Ljava/util/List;

    .line 63029
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->access$81802(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;I)I

    .line 63030
    return-object v0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 62954
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 62954
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;
    .locals 1

    .prologue
    .line 62970
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 62971
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 62972
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    .line 62973
    const-string v0, "_inbox"

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->folder_:Ljava/lang/Object;

    .line 62974
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    .line 62975
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 62976
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    .line 62977
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->callees_:Ljava/util/List;

    .line 62978
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    .line 62979
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;
    .locals 1

    .prologue
    .line 63164
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 63166
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    .line 63167
    return-object p0
.end method

.method public clearCallees()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;
    .locals 1

    .prologue
    .line 63319
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->callees_:Ljava/util/List;

    .line 63320
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    .line 63322
    return-object p0
.end method

.method public clearFolder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;
    .locals 1

    .prologue
    .line 63195
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    .line 63196
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->getFolder()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->folder_:Ljava/lang/Object;

    .line 63198
    return-object p0
.end method

.method public clearVideoMailId()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;
    .locals 1

    .prologue
    .line 63231
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    .line 63232
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->getVideoMailId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 63234
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 62954
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 62954
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 62954
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;
    .locals 2

    .prologue
    .line 62983
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 62954
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 63133
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCallees(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter

    .prologue
    .line 63259
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getCalleesCount()I
    .locals 1

    .prologue
    .line 63256
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCalleesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63253
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->callees_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 62954
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 62954
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;
    .locals 1

    .prologue
    .line 62987
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;

    move-result-object v0

    return-object v0
.end method

.method public getFolder()Ljava/lang/String;
    .locals 2

    .prologue
    .line 63176
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->folder_:Ljava/lang/Object;

    .line 63177
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 63178
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 63179
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->folder_:Ljava/lang/Object;

    .line 63182
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getVideoMailId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 63212
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 63213
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 63214
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 63215
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 63218
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 63130
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFolder()Z
    .locals 2

    .prologue
    .line 63173
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoMailId()Z
    .locals 2

    .prologue
    .line 63209
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 63058
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 63076
    :goto_0
    return v0

    .line 63062
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->hasVideoMailId()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 63064
    goto :goto_0

    .line 63066
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    .line 63068
    goto :goto_0

    :cond_2
    move v0, v2

    .line 63070
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->getCalleesCount()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 63071
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->getCallees(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_3

    move v0, v2

    .line 63073
    goto :goto_0

    .line 63070
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 63076
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 63152
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 63154
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 63160
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    .line 63161
    return-object p0

    .line 63157
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62954
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 62954
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62954
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63084
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 63085
    sparse-switch v0, :sswitch_data_0

    .line 63090
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 63092
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 63088
    goto :goto_1

    .line 63097
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 63098
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 63099
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 63101
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 63102
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    goto :goto_0

    .line 63106
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    .line 63107
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->folder_:Ljava/lang/Object;

    goto :goto_0

    .line 63111
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    .line 63112
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->videoMailId_:Ljava/lang/Object;

    goto :goto_0

    .line 63116
    :sswitch_4
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    .line 63117
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 63118
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->addCallees(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    goto :goto_0

    .line 63085
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 63034
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 63054
    :goto_0
    return-object v0

    .line 63035
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63036
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    .line 63038
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->hasFolder()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 63039
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->getFolder()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->setFolder(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    .line 63041
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->hasVideoMailId()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 63042
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->getVideoMailId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->setVideoMailId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    .line 63044
    :cond_3
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->callees_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->access$81700(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 63045
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 63046
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->callees_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->access$81700(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->callees_:Ljava/util/List;

    .line 63047
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    :cond_4
    :goto_1
    move-object v0, p0

    .line 63054
    goto :goto_0

    .line 63049
    :cond_5
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->ensureCalleesIsMutable()V

    .line 63050
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->callees_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->callees_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->access$81700(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public removeCallees(I)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 63325
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->ensureCalleesIsMutable()V

    .line 63326
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 63328
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 63146
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 63148
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    .line 63149
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 63136
    if-nez p1, :cond_0

    .line 63137
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 63139
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 63141
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    .line 63142
    return-object p0
.end method

.method public setCallees(ILcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 63273
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->ensureCalleesIsMutable()V

    .line 63274
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->callees_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 63276
    return-object p0
.end method

.method public setCallees(ILcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 63263
    if-nez p2, :cond_0

    .line 63264
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 63266
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->ensureCalleesIsMutable()V

    .line 63267
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->callees_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 63269
    return-object p0
.end method

.method public setFolder(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 63186
    if-nez p1, :cond_0

    .line 63187
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 63189
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    .line 63190
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->folder_:Ljava/lang/Object;

    .line 63192
    return-object p0
.end method

.method setFolder(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 63201
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    .line 63202
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->folder_:Ljava/lang/Object;

    .line 63204
    return-void
.end method

.method public setVideoMailId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 63222
    if-nez p1, :cond_0

    .line 63223
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 63225
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    .line 63226
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 63228
    return-object p0
.end method

.method setVideoMailId(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 63237
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->bitField0_:I

    .line 63238
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->videoMailId_:Ljava/lang/Object;

    .line 63240
    return-void
.end method
