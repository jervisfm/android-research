.class public final Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VideoMailWithCalleesPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CALLEES_FIELD_NUMBER:I = 0x4

.field public static final FOLDER_FIELD_NUMBER:I = 0x2

.field public static final VIDEO_MAIL_ID_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private callees_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private folder_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private videoMailId_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 63335
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;

    .line 63336
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->initFields()V

    .line 63337
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 62690
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 62805
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->memoizedIsInitialized:B

    .line 62849
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->memoizedSerializedSize:I

    .line 62691
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 62685
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 62692
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 62805
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->memoizedIsInitialized:B

    .line 62849
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->memoizedSerializedSize:I

    .line 62692
    return-void
.end method

.method static synthetic access$81402(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 62685
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$81502(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 62685
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->folder_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$81602(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 62685
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->videoMailId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$81700(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 62685
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->callees_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$81702(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 62685
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->callees_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$81802(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 62685
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;
    .locals 1

    .prologue
    .line 62696
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;

    return-object v0
.end method

.method private getFolderBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 62735
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->folder_:Ljava/lang/Object;

    .line 62736
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 62737
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 62739
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->folder_:Ljava/lang/Object;

    .line 62742
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getVideoMailIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 62767
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->videoMailId_:Ljava/lang/Object;

    .line 62768
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 62769
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 62771
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->videoMailId_:Ljava/lang/Object;

    .line 62774
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 62800
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 62801
    const-string v0, "_inbox"

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->folder_:Ljava/lang/Object;

    .line 62802
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->videoMailId_:Ljava/lang/Object;

    .line 62803
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->callees_:Ljava/util/List;

    .line 62804
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;
    .locals 1

    .prologue
    .line 62947
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->access$81200()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 62950
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62916
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    move-result-object v0

    .line 62917
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 62918
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->access$81100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;

    move-result-object v0

    .line 62920
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62927
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    move-result-object v0

    .line 62928
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 62929
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->access$81100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;

    move-result-object v0

    .line 62931
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 62883
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->access$81100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 62889
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->access$81100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62937
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->access$81100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62943
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->access$81100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62905
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->access$81100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62911
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->access$81100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 62894
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->access$81100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 62900
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;->access$81100(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 62711
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCallees(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter

    .prologue
    .line 62792
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->callees_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getCalleesCount()I
    .locals 1

    .prologue
    .line 62789
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->callees_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCalleesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62782
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->callees_:Ljava/util/List;

    return-object v0
.end method

.method public getCalleesOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 62796
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->callees_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;

    return-object v0
.end method

.method public getCalleesOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62786
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->callees_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 62685
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;
    .locals 1

    .prologue
    .line 62700
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;

    return-object v0
.end method

.method public getFolder()Ljava/lang/String;
    .locals 2

    .prologue
    .line 62721
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->folder_:Ljava/lang/Object;

    .line 62722
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 62723
    check-cast v0, Ljava/lang/String;

    .line 62731
    :goto_0
    return-object v0

    .line 62725
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 62727
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 62728
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62729
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->folder_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 62731
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 62851
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->memoizedSerializedSize:I

    .line 62852
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 62872
    :goto_0
    return v0

    .line 62855
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_4

    .line 62856
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v3

    .line 62859
    :goto_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_1

    .line 62860
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->getFolderBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 62863
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_2

    .line 62864
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->getVideoMailIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    move v1, v3

    move v2, v0

    .line 62867
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->callees_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 62868
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->callees_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v5, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 62867
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 62871
    :cond_3
    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->memoizedSerializedSize:I

    move v0, v2

    .line 62872
    goto :goto_0

    :cond_4
    move v0, v3

    goto :goto_1
.end method

.method public getVideoMailId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 62753
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->videoMailId_:Ljava/lang/Object;

    .line 62754
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 62755
    check-cast v0, Ljava/lang/String;

    .line 62763
    :goto_0
    return-object v0

    .line 62757
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 62759
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 62760
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62761
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->videoMailId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 62763
    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 62708
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFolder()Z
    .locals 2

    .prologue
    .line 62718
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoMailId()Z
    .locals 2

    .prologue
    .line 62750
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 62807
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->memoizedIsInitialized:B

    .line 62808
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 62829
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 62808
    goto :goto_0

    .line 62810
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 62811
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 62812
    goto :goto_0

    .line 62814
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->hasVideoMailId()Z

    move-result v0

    if-nez v0, :cond_3

    .line 62815
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 62816
    goto :goto_0

    .line 62818
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    .line 62819
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 62820
    goto :goto_0

    :cond_4
    move v0, v2

    .line 62822
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->getCalleesCount()I

    move-result v1

    if-ge v0, v1, :cond_6

    .line 62823
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->getCallees(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_5

    .line 62824
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 62825
    goto :goto_0

    .line 62822
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 62828
    :cond_6
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 62829
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 62685
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;
    .locals 1

    .prologue
    .line 62948
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 62685
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;
    .locals 1

    .prologue
    .line 62952
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 62877
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 62834
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->getSerializedSize()I

    .line 62835
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 62836
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 62838
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 62839
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->getFolderBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 62841
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 62842
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->getVideoMailIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 62844
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->callees_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 62845
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailWithCalleesPayload;->callees_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 62844
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 62847
    :cond_3
    return-void
.end method
