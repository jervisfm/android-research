.class public final Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private availableSpace_:J

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private entries_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;",
            ">;"
        }
    .end annotation
.end field

.field private error_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

.field private resultType_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;

.field private upgradeable_:Z

.field private usedSpace_:J


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 57283
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 57518
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 57603
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;->CACHED:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->resultType_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;

    .line 57627
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;->OK:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->error_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    .line 57651
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->entries_:Ljava/util/List;

    .line 57740
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->upgradeable_:Z

    .line 57284
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->maybeForceBuilderInitialization()V

    .line 57285
    return-void
.end method

.method static synthetic access$73600(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 57278
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$73700()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 1

    .prologue
    .line 57278
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 57330
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;

    move-result-object v0

    .line 57331
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 57332
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 57335
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 1

    .prologue
    .line 57290
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureEntriesIsMutable()V
    .locals 2

    .prologue
    .line 57654
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    .line 57655
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->entries_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->entries_:Ljava/util/List;

    .line 57656
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57658
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 57288
    return-void
.end method


# virtual methods
.method public addAllEntries(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;"
        }
    .end annotation

    .prologue
    .line 57721
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->ensureEntriesIsMutable()V

    .line 57722
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->entries_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 57724
    return-object p0
.end method

.method public addEntries(ILcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 57714
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->ensureEntriesIsMutable()V

    .line 57715
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->entries_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 57717
    return-object p0
.end method

.method public addEntries(ILcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 57697
    if-nez p2, :cond_0

    .line 57698
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 57700
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->ensureEntriesIsMutable()V

    .line 57701
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 57703
    return-object p0
.end method

.method public addEntries(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 57707
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->ensureEntriesIsMutable()V

    .line 57708
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->entries_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57710
    return-object p0
.end method

.method public addEntries(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 57687
    if-nez p1, :cond_0

    .line 57688
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 57690
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->ensureEntriesIsMutable()V

    .line 57691
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57693
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 57278
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;
    .locals 2

    .prologue
    .line 57321
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;

    move-result-object v0

    .line 57322
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 57323
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 57325
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 57278
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;
    .locals 5

    .prologue
    .line 57339
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 57340
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57341
    const/4 v2, 0x0

    .line 57342
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 57343
    or-int/lit8 v2, v2, 0x1

    .line 57345
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->access$73902(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 57346
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 57347
    or-int/lit8 v2, v2, 0x2

    .line 57349
    :cond_1
    iget-wide v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->availableSpace_:J

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->availableSpace_:J
    invoke-static {v0, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->access$74002(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;J)J

    .line 57350
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 57351
    or-int/lit8 v2, v2, 0x4

    .line 57353
    :cond_2
    iget-wide v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->usedSpace_:J

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->usedSpace_:J
    invoke-static {v0, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->access$74102(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;J)J

    .line 57354
    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 57355
    or-int/lit8 v2, v2, 0x8

    .line 57357
    :cond_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->resultType_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->resultType_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->access$74202(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;

    .line 57358
    and-int/lit8 v3, v1, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 57359
    or-int/lit8 v2, v2, 0x10

    .line 57361
    :cond_4
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->error_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->error_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->access$74302(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    .line 57362
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 57363
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->entries_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->entries_:Ljava/util/List;

    .line 57364
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x21

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57366
    :cond_5
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->entries_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->entries_:Ljava/util/List;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->access$74402(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;Ljava/util/List;)Ljava/util/List;

    .line 57367
    and-int/lit8 v1, v1, 0x40

    const/16 v3, 0x40

    if-ne v1, v3, :cond_6

    .line 57368
    or-int/lit8 v1, v2, 0x20

    .line 57370
    :goto_0
    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->upgradeable_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->upgradeable_:Z
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->access$74502(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;Z)Z

    .line 57371
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->access$74602(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;I)I

    .line 57372
    return-object v0

    :cond_6
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 57278
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 57278
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 57294
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 57295
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 57296
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57297
    iput-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->availableSpace_:J

    .line 57298
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57299
    iput-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->usedSpace_:J

    .line 57300
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57301
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;->CACHED:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->resultType_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;

    .line 57302
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57303
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;->OK:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->error_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    .line 57304
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57305
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->entries_:Ljava/util/List;

    .line 57306
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57307
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->upgradeable_:Z

    .line 57308
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57309
    return-object p0
.end method

.method public clearAvailableSpace()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 2

    .prologue
    .line 57575
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57576
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->availableSpace_:J

    .line 57578
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 1

    .prologue
    .line 57554
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 57556
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57557
    return-object p0
.end method

.method public clearEntries()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 1

    .prologue
    .line 57727
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->entries_:Ljava/util/List;

    .line 57728
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57730
    return-object p0
.end method

.method public clearError()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 1

    .prologue
    .line 57644
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57645
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;->OK:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->error_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    .line 57647
    return-object p0
.end method

.method public clearResultType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 1

    .prologue
    .line 57620
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57621
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;->CACHED:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->resultType_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;

    .line 57623
    return-object p0
.end method

.method public clearUpgradeable()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 1

    .prologue
    .line 57754
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57755
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->upgradeable_:Z

    .line 57757
    return-object p0
.end method

.method public clearUsedSpace()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 2

    .prologue
    .line 57596
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57597
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->usedSpace_:J

    .line 57599
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 57278
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 57278
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 57278
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 2

    .prologue
    .line 57313
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 57278
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAvailableSpace()J
    .locals 2

    .prologue
    .line 57566
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->availableSpace_:J

    return-wide v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 57523
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 57278
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 57278
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;
    .locals 1

    .prologue
    .line 57317
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;

    move-result-object v0

    return-object v0
.end method

.method public getEntries(I)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    .locals 1
    .parameter

    .prologue
    .line 57667
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    return-object v0
.end method

.method public getEntriesCount()I
    .locals 1

    .prologue
    .line 57664
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getEntriesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57661
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->entries_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getError()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;
    .locals 1

    .prologue
    .line 57632
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->error_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    return-object v0
.end method

.method public getResultType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;
    .locals 1

    .prologue
    .line 57608
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->resultType_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;

    return-object v0
.end method

.method public getUpgradeable()Z
    .locals 1

    .prologue
    .line 57745
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->upgradeable_:Z

    return v0
.end method

.method public getUsedSpace()J
    .locals 2

    .prologue
    .line 57587
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->usedSpace_:J

    return-wide v0
.end method

.method public hasAvailableSpace()Z
    .locals 2

    .prologue
    .line 57563
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 57520
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasError()Z
    .locals 2

    .prologue
    .line 57629
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasResultType()Z
    .locals 2

    .prologue
    .line 57605
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUpgradeable()Z
    .locals 2

    .prologue
    .line 57742
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUsedSpace()Z
    .locals 2

    .prologue
    .line 57584
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 57409
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 57443
    :goto_0
    return v0

    .line 57413
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->hasAvailableSpace()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 57415
    goto :goto_0

    .line 57417
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->hasUsedSpace()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    .line 57419
    goto :goto_0

    .line 57421
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->hasResultType()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    .line 57423
    goto :goto_0

    .line 57425
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->hasError()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v2

    .line 57427
    goto :goto_0

    .line 57429
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->hasUpgradeable()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v2

    .line 57431
    goto :goto_0

    .line 57433
    :cond_5
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v2

    .line 57435
    goto :goto_0

    :cond_6
    move v0, v2

    .line 57437
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->getEntriesCount()I

    move-result v1

    if-ge v0, v1, :cond_8

    .line 57438
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->getEntries(I)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_7

    move v0, v2

    .line 57440
    goto :goto_0

    .line 57437
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 57443
    :cond_8
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 57542
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 57544
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 57550
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57551
    return-object p0

    .line 57547
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57278
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 57278
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57278
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57451
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 57452
    sparse-switch v0, :sswitch_data_0

    .line 57457
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 57459
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 57455
    goto :goto_1

    .line 57464
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 57465
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 57466
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 57468
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 57469
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    goto :goto_0

    .line 57473
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57474
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->availableSpace_:J

    goto :goto_0

    .line 57478
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57479
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->usedSpace_:J

    goto :goto_0

    .line 57483
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 57484
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;

    move-result-object v0

    .line 57485
    if-eqz v0, :cond_0

    .line 57486
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57487
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->resultType_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;

    goto :goto_0

    .line 57492
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 57493
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    move-result-object v0

    .line 57494
    if-eqz v0, :cond_0

    .line 57495
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57496
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->error_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    goto :goto_0

    .line 57501
    :sswitch_6
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;

    move-result-object v0

    .line 57502
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 57503
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->addEntries(Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    goto :goto_0

    .line 57507
    :sswitch_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57508
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->upgradeable_:Z

    goto/16 :goto_0

    .line 57452
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 57376
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 57405
    :goto_0
    return-object v0

    .line 57377
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57378
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    .line 57380
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->hasAvailableSpace()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 57381
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->getAvailableSpace()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->setAvailableSpace(J)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    .line 57383
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->hasUsedSpace()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 57384
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->getUsedSpace()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->setUsedSpace(J)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    .line 57386
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->hasResultType()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 57387
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->getResultType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->setResultType(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    .line 57389
    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->hasError()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 57390
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->getError()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->setError(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    .line 57392
    :cond_5
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->entries_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->access$74400(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 57393
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 57394
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->entries_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->access$74400(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->entries_:Ljava/util/List;

    .line 57395
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57402
    :cond_6
    :goto_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->hasUpgradeable()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 57403
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->getUpgradeable()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->setUpgradeable(Z)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    :cond_7
    move-object v0, p0

    .line 57405
    goto :goto_0

    .line 57397
    :cond_8
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->ensureEntriesIsMutable()V

    .line 57398
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->entries_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->entries_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->access$74400(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public removeEntries(I)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 57733
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->ensureEntriesIsMutable()V

    .line 57734
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 57736
    return-object p0
.end method

.method public setAvailableSpace(J)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 57569
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57570
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->availableSpace_:J

    .line 57572
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 57536
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 57538
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57539
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 57526
    if-nez p1, :cond_0

    .line 57527
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 57529
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 57531
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57532
    return-object p0
.end method

.method public setEntries(ILcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 57681
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->ensureEntriesIsMutable()V

    .line 57682
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->entries_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 57684
    return-object p0
.end method

.method public setEntries(ILcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 57671
    if-nez p2, :cond_0

    .line 57672
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 57674
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->ensureEntriesIsMutable()V

    .line 57675
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 57677
    return-object p0
.end method

.method public setError(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 57635
    if-nez p1, :cond_0

    .line 57636
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 57638
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57639
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->error_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    .line 57641
    return-object p0
.end method

.method public setResultType(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 57611
    if-nez p1, :cond_0

    .line 57612
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 57614
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57615
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->resultType_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;

    .line 57617
    return-object p0
.end method

.method public setUpgradeable(Z)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 57748
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57749
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->upgradeable_:Z

    .line 57751
    return-object p0
.end method

.method public setUsedSpace(J)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 57590
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->bitField0_:I

    .line 57591
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->usedSpace_:J

    .line 57593
    return-object p0
.end method
