.class public final enum Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ErrorState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState; = null

.field public static final enum ACCOUNT:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState; = null

.field public static final ACCOUNT_VALUE:I = 0x4

.field public static final enum NETWORK:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState; = null

.field public static final NETWORK_VALUE:I = 0x2

.field public static final enum OK:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState; = null

.field public static final OK_VALUE:I = 0x0

.field public static final enum SERVER_FAILURE:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState; = null

.field public static final SERVER_FAILURE_VALUE:I = 0x3

.field public static final enum TIMEOUT:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState; = null

.field public static final TIMEOUT_VALUE:I = 0x1

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 56953
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    const-string v1, "OK"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;->OK:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    .line 56954
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    const-string v1, "TIMEOUT"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;->TIMEOUT:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    .line 56955
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    const-string v1, "NETWORK"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;->NETWORK:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    .line 56956
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    const-string v1, "SERVER_FAILURE"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;->SERVER_FAILURE:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    .line 56957
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    const-string v1, "ACCOUNT"

    invoke-direct {v0, v1, v6, v6, v6}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;->ACCOUNT:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    .line 56951
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;->OK:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;->TIMEOUT:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;->NETWORK:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;->SERVER_FAILURE:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;->ACCOUNT:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    .line 56985
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 56994
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 56995
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;->value:I

    .line 56996
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56982
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;
    .locals 1
    .parameter

    .prologue
    .line 56970
    packed-switch p0, :pswitch_data_0

    .line 56976
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 56971
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;->OK:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    goto :goto_0

    .line 56972
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;->TIMEOUT:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    goto :goto_0

    .line 56973
    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;->NETWORK:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    goto :goto_0

    .line 56974
    :pswitch_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;->SERVER_FAILURE:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    goto :goto_0

    .line 56975
    :pswitch_4
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;->ACCOUNT:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    goto :goto_0

    .line 56970
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;
    .locals 1
    .parameter

    .prologue
    .line 56951
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;
    .locals 1

    .prologue
    .line 56951
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 56967
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;->value:I

    return v0
.end method
