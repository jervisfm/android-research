.class public final Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VideoMailsPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;,
        Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;,
        Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;
    }
.end annotation


# static fields
.field public static final AVAILABLE_SPACE_FIELD_NUMBER:I = 0x2

.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final ENTRIES_FIELD_NUMBER:I = 0x6

.field public static final ERROR_FIELD_NUMBER:I = 0x5

.field public static final RESULT_TYPE_FIELD_NUMBER:I = 0x4

.field public static final UPGRADEABLE_FIELD_NUMBER:I = 0x7

.field public static final USED_SPACE_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;


# instance fields
.field private availableSpace_:J

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private entries_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;",
            ">;"
        }
    .end annotation
.end field

.field private error_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private resultType_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;

.field private upgradeable_:Z

.field private usedSpace_:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 57764
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;

    .line 57765
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->initFields()V

    .line 57766
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 56897
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 57092
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->memoizedIsInitialized:B

    .line 57161
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->memoizedSerializedSize:I

    .line 56898
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 56892
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 56899
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 57092
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->memoizedIsInitialized:B

    .line 57161
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->memoizedSerializedSize:I

    .line 56899
    return-void
.end method

.method static synthetic access$73902(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 56892
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$74002(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;J)J
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 56892
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->availableSpace_:J

    return-wide p1
.end method

.method static synthetic access$74102(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;J)J
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 56892
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->usedSpace_:J

    return-wide p1
.end method

.method static synthetic access$74202(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 56892
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->resultType_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;

    return-object p1
.end method

.method static synthetic access$74302(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 56892
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->error_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    return-object p1
.end method

.method static synthetic access$74400(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 56892
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->entries_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$74402(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 56892
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->entries_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$74502(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 56892
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->upgradeable_:Z

    return p1
.end method

.method static synthetic access$74602(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 56892
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;
    .locals 1

    .prologue
    .line 56903
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 57084
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 57085
    iput-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->availableSpace_:J

    .line 57086
    iput-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->usedSpace_:J

    .line 57087
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;->CACHED:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->resultType_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;

    .line 57088
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;->OK:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->error_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    .line 57089
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->entries_:Ljava/util/List;

    .line 57090
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->upgradeable_:Z

    .line 57091
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 1

    .prologue
    .line 57271
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->access$73700()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 57274
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57240
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    move-result-object v0

    .line 57241
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57242
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->access$73600(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;

    move-result-object v0

    .line 57244
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57251
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    move-result-object v0

    .line 57252
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57253
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->access$73600(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;

    move-result-object v0

    .line 57255
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 57207
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->access$73600(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 57213
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->access$73600(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57261
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->access$73600(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57267
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->access$73600(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57229
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->access$73600(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57235
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->access$73600(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 57218
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->access$73600(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 57224
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;->access$73600(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAvailableSpace()J
    .locals 2

    .prologue
    .line 57019
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->availableSpace_:J

    return-wide v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 57009
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 56892
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;
    .locals 1

    .prologue
    .line 56907
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;

    return-object v0
.end method

.method public getEntries(I)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
    .locals 1
    .parameter

    .prologue
    .line 57066
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->entries_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    return-object v0
.end method

.method public getEntriesCount()I
    .locals 1

    .prologue
    .line 57063
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->entries_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getEntriesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57056
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->entries_:Ljava/util/List;

    return-object v0
.end method

.method public getEntriesOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntryOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 57070
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->entries_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntryOrBuilder;

    return-object v0
.end method

.method public getEntriesOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntryOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57060
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->entries_:Ljava/util/List;

    return-object v0
.end method

.method public getError()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;
    .locals 1

    .prologue
    .line 57049
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->error_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    return-object v0
.end method

.method public getResultType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;
    .locals 1

    .prologue
    .line 57039
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->resultType_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 57163
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->memoizedSerializedSize:I

    .line 57164
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 57196
    :goto_0
    return v0

    .line 57167
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_7

    .line 57168
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v4

    .line 57171
    :goto_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_1

    .line 57172
    iget-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->availableSpace_:J

    invoke-static {v3, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 57175
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_2

    .line 57176
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->usedSpace_:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 57179
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_3

    .line 57180
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->resultType_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;->getNumber()I

    move-result v1

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 57183
    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_4

    .line 57184
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->error_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    move v1, v4

    move v2, v0

    .line 57187
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->entries_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 57188
    const/4 v3, 0x6

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->entries_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 57187
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 57191
    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 57192
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->upgradeable_:Z

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v0

    add-int/2addr v0, v2

    .line 57195
    :goto_3
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->memoizedSerializedSize:I

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_3

    :cond_7
    move v0, v4

    goto :goto_1
.end method

.method public getUpgradeable()Z
    .locals 1

    .prologue
    .line 57080
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->upgradeable_:Z

    return v0
.end method

.method public getUsedSpace()J
    .locals 2

    .prologue
    .line 57029
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->usedSpace_:J

    return-wide v0
.end method

.method public hasAvailableSpace()Z
    .locals 2

    .prologue
    .line 57016
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 57006
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasError()Z
    .locals 2

    .prologue
    .line 57046
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasResultType()Z
    .locals 2

    .prologue
    .line 57036
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUpgradeable()Z
    .locals 2

    .prologue
    .line 57077
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUsedSpace()Z
    .locals 2

    .prologue
    .line 57026
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 57094
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->memoizedIsInitialized:B

    .line 57095
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 57132
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 57095
    goto :goto_0

    .line 57097
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 57098
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 57099
    goto :goto_0

    .line 57101
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->hasAvailableSpace()Z

    move-result v0

    if-nez v0, :cond_3

    .line 57102
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 57103
    goto :goto_0

    .line 57105
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->hasUsedSpace()Z

    move-result v0

    if-nez v0, :cond_4

    .line 57106
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 57107
    goto :goto_0

    .line 57109
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->hasResultType()Z

    move-result v0

    if-nez v0, :cond_5

    .line 57110
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 57111
    goto :goto_0

    .line 57113
    :cond_5
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->hasError()Z

    move-result v0

    if-nez v0, :cond_6

    .line 57114
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 57115
    goto :goto_0

    .line 57117
    :cond_6
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->hasUpgradeable()Z

    move-result v0

    if-nez v0, :cond_7

    .line 57118
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 57119
    goto :goto_0

    .line 57121
    :cond_7
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_8

    .line 57122
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 57123
    goto :goto_0

    :cond_8
    move v0, v2

    .line 57125
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->getEntriesCount()I

    move-result v1

    if-ge v0, v1, :cond_a

    .line 57126
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->getEntries(I)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_9

    .line 57127
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 57128
    goto :goto_0

    .line 57125
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 57131
    :cond_a
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 57132
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 56892
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 1

    .prologue
    .line 57272
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 56892
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;
    .locals 1

    .prologue
    .line 57276
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 57201
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 57137
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->getSerializedSize()I

    .line 57138
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 57139
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 57141
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 57142
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->availableSpace_:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 57144
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 57145
    const/4 v0, 0x3

    iget-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->usedSpace_:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 57147
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 57148
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->resultType_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;->getNumber()I

    move-result v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 57150
    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 57151
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->error_:Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 57153
    :cond_4
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->entries_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 57154
    const/4 v2, 0x6

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->entries_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 57153
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 57156
    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    .line 57157
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload;->upgradeable_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 57159
    :cond_6
    return-void
.end method
