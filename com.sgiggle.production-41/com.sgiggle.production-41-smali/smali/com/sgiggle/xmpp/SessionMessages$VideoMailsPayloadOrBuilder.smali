.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "VideoMailsPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getAvailableSpace()J
.end method

.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getEntries(I)Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;
.end method

.method public abstract getEntriesCount()I
.end method

.method public abstract getEntriesList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getError()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ErrorState;
.end method

.method public abstract getResultType()Lcom/sgiggle/xmpp/SessionMessages$VideoMailsPayload$ResultType;
.end method

.method public abstract getUpgradeable()Z
.end method

.method public abstract getUsedSpace()J
.end method

.method public abstract hasAvailableSpace()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasError()Z
.end method

.method public abstract hasResultType()Z
.end method

.method public abstract hasUpgradeable()Z
.end method

.method public abstract hasUsedSpace()Z
.end method
