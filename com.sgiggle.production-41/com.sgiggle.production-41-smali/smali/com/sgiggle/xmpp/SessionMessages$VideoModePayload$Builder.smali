.class public final Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$VideoModePayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$VideoModePayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private cameraPosition_:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 14474
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 14600
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 14643
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->CAM_NONE:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->cameraPosition_:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    .line 14475
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->maybeForceBuilderInitialization()V

    .line 14476
    return-void
.end method

.method static synthetic access$18300(Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 14469
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$18400()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;
    .locals 1

    .prologue
    .line 14469
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 14511
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;

    move-result-object v0

    .line 14512
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 14513
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 14516
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;
    .locals 1

    .prologue
    .line 14481
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 14479
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 14469
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;
    .locals 2

    .prologue
    .line 14502
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;

    move-result-object v0

    .line 14503
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 14504
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 14506
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 14469
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;
    .locals 5

    .prologue
    .line 14520
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 14521
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->bitField0_:I

    .line 14522
    const/4 v2, 0x0

    .line 14523
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 14524
    or-int/lit8 v2, v2, 0x1

    .line 14526
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->access$18602(Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 14527
    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    .line 14528
    or-int/lit8 v1, v2, 0x2

    .line 14530
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->cameraPosition_:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->cameraPosition_:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->access$18702(Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;)Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    .line 14531
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->access$18802(Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;I)I

    .line 14532
    return-object v0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 14469
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 14469
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;
    .locals 1

    .prologue
    .line 14485
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 14486
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 14487
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->bitField0_:I

    .line 14488
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->CAM_NONE:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->cameraPosition_:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    .line 14489
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->bitField0_:I

    .line 14490
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;
    .locals 1

    .prologue
    .line 14636
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 14638
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->bitField0_:I

    .line 14639
    return-object p0
.end method

.method public clearCameraPosition()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;
    .locals 1

    .prologue
    .line 14660
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->bitField0_:I

    .line 14661
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->CAM_NONE:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->cameraPosition_:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    .line 14663
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 14469
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 14469
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 14469
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;
    .locals 2

    .prologue
    .line 14494
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 14469
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 14605
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCameraPosition()Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;
    .locals 1

    .prologue
    .line 14648
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->cameraPosition_:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 14469
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 14469
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;
    .locals 1

    .prologue
    .line 14498
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;

    move-result-object v0

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 14602
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCameraPosition()Z
    .locals 2

    .prologue
    .line 14645
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 14547
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 14555
    :goto_0
    return v0

    .line 14551
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 14553
    goto :goto_0

    .line 14555
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 14624
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 14626
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 14632
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->bitField0_:I

    .line 14633
    return-object p0

    .line 14629
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14469
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 14469
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14469
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14563
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 14564
    sparse-switch v0, :sswitch_data_0

    .line 14569
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 14571
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 14567
    goto :goto_1

    .line 14576
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 14577
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 14578
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 14580
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 14581
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    goto :goto_0

    .line 14585
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 14586
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    move-result-object v0

    .line 14587
    if-eqz v0, :cond_0

    .line 14588
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->bitField0_:I

    .line 14589
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->cameraPosition_:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    goto :goto_0

    .line 14564
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 14536
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 14543
    :goto_0
    return-object v0

    .line 14537
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 14538
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    .line 14540
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->hasCameraPosition()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 14541
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->getCameraPosition()Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->setCameraPosition(Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    :cond_2
    move-object v0, p0

    .line 14543
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 14618
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 14620
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->bitField0_:I

    .line 14621
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 14608
    if-nez p1, :cond_0

    .line 14609
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 14611
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 14613
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->bitField0_:I

    .line 14614
    return-object p0
.end method

.method public setCameraPosition(Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 14651
    if-nez p1, :cond_0

    .line 14652
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 14654
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->bitField0_:I

    .line 14655
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->cameraPosition_:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    .line 14657
    return-object p0
.end method
