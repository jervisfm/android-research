.class public final Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$VideoModePayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VideoModePayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CAMERAPOSITION_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private cameraPosition_:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 14670
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;

    .line 14671
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->initFields()V

    .line 14672
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 14306
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 14344
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->memoizedIsInitialized:B

    .line 14372
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->memoizedSerializedSize:I

    .line 14307
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 14301
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 14308
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 14344
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->memoizedIsInitialized:B

    .line 14372
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->memoizedSerializedSize:I

    .line 14308
    return-void
.end method

.method static synthetic access$18602(Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 14301
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$18702(Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;)Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 14301
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->cameraPosition_:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    return-object p1
.end method

.method static synthetic access$18802(Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 14301
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;
    .locals 1

    .prologue
    .line 14312
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 14341
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 14342
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->CAM_NONE:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->cameraPosition_:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    .line 14343
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;
    .locals 1

    .prologue
    .line 14462
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->access$18400()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 14465
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14431
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    move-result-object v0

    .line 14432
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14433
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->access$18300(Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;

    move-result-object v0

    .line 14435
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14442
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    move-result-object v0

    .line 14443
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 14444
    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->access$18300(Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;

    move-result-object v0

    .line 14446
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 14398
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->access$18300(Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 14404
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->access$18300(Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14452
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->access$18300(Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14458
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->access$18300(Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14420
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->access$18300(Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14426
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->access$18300(Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 14409
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->access$18300(Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 14415
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;->access$18300(Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 14327
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCameraPosition()Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;
    .locals 1

    .prologue
    .line 14337
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->cameraPosition_:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 14301
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;
    .locals 1

    .prologue
    .line 14316
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 14374
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->memoizedSerializedSize:I

    .line 14375
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 14387
    :goto_0
    return v0

    .line 14377
    :cond_0
    const/4 v0, 0x0

    .line 14378
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 14379
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14382
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 14383
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->cameraPosition_:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->getNumber()I

    move-result v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 14386
    :cond_2
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 14324
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCameraPosition()Z
    .locals 2

    .prologue
    .line 14334
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 14346
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->memoizedIsInitialized:B

    .line 14347
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 14358
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 14347
    goto :goto_0

    .line 14349
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 14350
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 14351
    goto :goto_0

    .line 14353
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 14354
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 14355
    goto :goto_0

    .line 14357
    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->memoizedIsInitialized:B

    move v0, v3

    .line 14358
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 14301
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;
    .locals 1

    .prologue
    .line 14463
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 14301
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;
    .locals 1

    .prologue
    .line 14467
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;)Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 14392
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 14363
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->getSerializedSize()I

    .line 14364
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 14365
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 14367
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 14368
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->cameraPosition_:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->getNumber()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 14370
    :cond_1
    return-void
.end method
