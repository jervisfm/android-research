.class public final enum Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "WandLocationType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$WandLocationType; = null

.field public static final enum LOCATION_IN_CALL:Lcom/sgiggle/xmpp/SessionMessages$WandLocationType; = null

.field public static final LOCATION_IN_CALL_VALUE:I = 0x0

.field public static final enum LOCATION_IN_DEMO:Lcom/sgiggle/xmpp/SessionMessages$WandLocationType; = null

.field public static final LOCATION_IN_DEMO_VALUE:I = 0x1

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 542
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;

    const-string v1, "LOCATION_IN_CALL"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;->LOCATION_IN_CALL:Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;

    .line 543
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;

    const-string v1, "LOCATION_IN_DEMO"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;->LOCATION_IN_DEMO:Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;

    .line 540
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;->LOCATION_IN_CALL:Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;->LOCATION_IN_DEMO:Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;

    .line 565
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$WandLocationType$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$WandLocationType$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 574
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 575
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;->value:I

    .line 576
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 562
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;
    .locals 1
    .parameter

    .prologue
    .line 553
    packed-switch p0, :pswitch_data_0

    .line 556
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 554
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;->LOCATION_IN_CALL:Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;

    goto :goto_0

    .line 555
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;->LOCATION_IN_DEMO:Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;

    goto :goto_0

    .line 553
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;
    .locals 1
    .parameter

    .prologue
    .line 540
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;
    .locals 1

    .prologue
    .line 540
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 550
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;->value:I

    return v0
.end method
