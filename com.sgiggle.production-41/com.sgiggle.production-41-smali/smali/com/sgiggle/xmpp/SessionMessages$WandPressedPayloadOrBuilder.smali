.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$WandPressedPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "WandPressedPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getWandLocation()Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasWandLocation()Z
.end method
