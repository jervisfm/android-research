.class Lorg/acra/collector/Compatibility;
.super Ljava/lang/Object;
.source "Compatibility.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAPILevel()I
    .locals 5

    .prologue
    .line 40
    :try_start_0
    const-class v3, Landroid/os/Build$VERSION;

    const-string v4, "SDK_INT"

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 41
    .local v0, SDK_INT:Ljava/lang/reflect/Field;
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3

    move-result v1

    .line 52
    .end local v0           #SDK_INT:Ljava/lang/reflect/Field;
    .local v1, apiLevel:I
    :goto_0
    return v1

    .line 42
    .end local v1           #apiLevel:I
    :catch_0
    move-exception v3

    move-object v2, v3

    .line 43
    .local v2, e:Ljava/lang/SecurityException;
    sget-object v3, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 50
    .restart local v1       #apiLevel:I
    goto :goto_0

    .line 44
    .end local v1           #apiLevel:I
    .end local v2           #e:Ljava/lang/SecurityException;
    :catch_1
    move-exception v3

    move-object v2, v3

    .line 45
    .local v2, e:Ljava/lang/NoSuchFieldException;
    sget-object v3, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 50
    .restart local v1       #apiLevel:I
    goto :goto_0

    .line 46
    .end local v1           #apiLevel:I
    .end local v2           #e:Ljava/lang/NoSuchFieldException;
    :catch_2
    move-exception v3

    move-object v2, v3

    .line 47
    .local v2, e:Ljava/lang/IllegalArgumentException;
    sget-object v3, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 50
    .restart local v1       #apiLevel:I
    goto :goto_0

    .line 48
    .end local v1           #apiLevel:I
    .end local v2           #e:Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v3

    move-object v2, v3

    .line 49
    .local v2, e:Ljava/lang/IllegalAccessException;
    sget-object v3, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .restart local v1       #apiLevel:I
    goto :goto_0
.end method

.method public static getDropBoxServiceName()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NoSuchFieldException;,
            Ljava/lang/IllegalAccessException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 63
    const-class v1, Landroid/content/Context;

    const-string v2, "DROPBOX_SERVICE"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 64
    .local v0, serviceName:Ljava/lang/reflect/Field;
    if-eqz v0, :cond_0

    .line 65
    invoke-virtual {v0, v3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0           #serviceName:Ljava/lang/reflect/Field;
    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    .line 67
    :goto_0
    return-object v1

    .restart local v0       #serviceName:Ljava/lang/reflect/Field;
    :cond_0
    move-object v1, v3

    goto :goto_0
.end method
