package com.wf.wellsfargomobile;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public abstract class BaseActivity extends Activity
{
  private static final String TAG = "BaseActivity";

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    Log.d("BaseActivity", getClass().getName() + " onActivityResult - resultCode: " + paramInt2);
    switch (paramInt2)
    {
    default:
    case 13:
    }
    while (true)
    {
      ((WFApp)getApplication()).setLastActivityStoppedAt(0L);
      return;
      Log.d("BaseActivity", getClass().getName() + " onActivityResult - signing off ");
      if (!(this instanceof WFMain))
      {
        Log.d("BaseActivity", getClass().getName() + " calling finish() ");
        setResult(13, new Intent());
        finish();
      }
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Log.d("BaseActivity", getClass().getName() + " onCreate");
  }

  protected void onRestart()
  {
    super.onRestart();
    long l1 = System.currentTimeMillis();
    Log.d("BaseActivity", getClass().getName() + " onRestart - " + l1);
    WFApp localWFApp = (WFApp)getApplication();
    long l2 = l1 - localWFApp.getLastActivityStoppedAt();
    Log.d("BaseActivity", getClass().getName() + " onRestart - then: " + localWFApp.getLastActivityStoppedAt() + " now: " + l1 + " diff: " + l2 + " WFApp.MAX_STOPPED_STATE_TIME: " + 600000L);
    if ((localWFApp.getLastActivityStoppedAt() != 0L) && (l2 > 600000L))
    {
      if (!(this instanceof WFMain))
      {
        localWFApp.clearCheckDepositData();
        setResult(13, new Intent());
        Log.d("BaseActivity", getClass().getName() + " onRestart - calling finish()");
        finish();
      }
    }
    else
      return;
    Log.d("BaseActivity", getClass().getName() + " leave the user on the signon screen");
    localWFApp.setLastActivityStoppedAt(0L);
  }

  protected void onStart()
  {
    super.onStart();
    Log.d("BaseActivity", getClass().getName() + " onStart");
    ((WFApp)getApplication()).setLastActivityStoppedAt(0L);
  }

  protected void onStop()
  {
    super.onStop();
    long l = System.currentTimeMillis();
    Log.d("BaseActivity", getClass().getName() + " onStop - " + l);
    ((WFApp)getApplication()).setLastActivityStoppedAt(l);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.wf.wellsfargomobile-700220\classes_dex2jar.jar
 * Qualified Name:     com.wf.wellsfargomobile.BaseActivity
 * JD-Core Version:    0.6.2
 */