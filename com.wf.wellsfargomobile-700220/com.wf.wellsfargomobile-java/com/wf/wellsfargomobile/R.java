package com.wf.wellsfargomobile;

public final class R
{
  public static final class array
  {
    public static final int permitted_hosts = 2131230720;
  }

  public static final class attr
  {
  }

  public static final class color
  {
    public static final int background = 2130968576;
    public static final int background_title = 2130968577;
    public static final int buttonText = 2130968582;
    public static final int check_instruction = 2130968579;
    public static final int headline = 2130968580;
    public static final int link = 2130968578;
    public static final int text = 2130968581;
  }

  public static final class dimen
  {
    public static final int one_dp = 2131034112;
    public static final int one_sp = 2131034113;
  }

  public static final class drawable
  {
    public static final int andr_icn_back = 2130837504;
    public static final int andr_icn_deposit = 2130837505;
    public static final int andr_icn_info = 2130837506;
    public static final int andr_icn_locations = 2130837507;
    public static final int andr_icn_question = 2130837508;
    public static final int andr_icn_signoff = 2130837509;
    public static final int andr_icn_signon = 2130837510;
    public static final int blank_1x1 = 2130837511;
    public static final int btn_blue = 2130837512;
    public static final int btn_grey = 2130837513;
    public static final int button_blue = 2130837514;
    public static final int button_grey = 2130837515;
    public static final int camera = 2130837516;
    public static final int camera_guide_bottom_left = 2130837517;
    public static final int camera_guide_bottom_right = 2130837518;
    public static final int camera_guide_top_left = 2130837519;
    public static final int camera_guide_top_right = 2130837520;
    public static final int divider = 2130837521;
    public static final int head_atms_locs_204x21 = 2130837522;
    public static final int head_locdetails_153x17 = 2130837523;
    public static final int head_mobilebanking_146x21 = 2130837524;
    public static final int head_narrow_190x17 = 2130837525;
    public static final int head_results_138x17 = 2130837526;
    public static final int icn_sliderarrow = 2130837527;
    public static final int map_wach = 2130837528;
    public static final int map_wf = 2130837529;
    public static final int masthead = 2130837530;
    public static final int masthead_background = 2130837531;
    public static final int pt_good_bad = 2130837532;
    public static final int result_wachovia = 2130837533;
    public static final int result_wf = 2130837534;
    public static final int results_container_body_even = 2130837535;
    public static final int results_container_body_odd = 2130837536;
    public static final int results_container_bottom_even = 2130837537;
    public static final int results_container_bottom_odd = 2130837538;
    public static final int results_container_top = 2130837539;
    public static final int screen_id = 2130837540;
    public static final int security = 2130837541;
    public static final int stagecoach = 2130837542;
    public static final int tilt_backward = 2130837543;
    public static final int tilt_forward = 2130837544;
    public static final int tilt_left = 2130837545;
    public static final int tilt_right = 2130837546;
    public static final int wf_app_icon = 2130837547;
  }

  public static final class id
  {
    public static final int address = 2131099683;
    public static final int auto_focus = 2131099648;
    public static final int back = 2131099718;
    public static final int build = 2131099762;
    public static final int buttonCancel = 2131099654;
    public static final int buttonPhotoTipsClose = 2131099747;
    public static final int buttonPhotoTipsCloseDontShow = 2131099746;
    public static final int buttonRetake = 2131099751;
    public static final int buttonTakePhoto = 2131099655;
    public static final int buttonUse = 2131099752;
    public static final int cameraGuideBottomLeft = 2131099661;
    public static final int cameraGuideBottomRight = 2131099662;
    public static final int cameraGuideTopLeft = 2131099659;
    public static final int cameraGuideTopRight = 2131099660;
    public static final int cameraGuides = 2131099658;
    public static final int camerapreview = 2131099649;
    public static final int capturePhotoFooter = 2131099656;
    public static final int capturePhotoHeader = 2131099650;
    public static final int capturePhotoInstructionLabel = 2131099657;
    public static final int check_preview = 2131099749;
    public static final int city = 2131099684;
    public static final int currentLocButton = 2131099668;
    public static final int depositProgress = 2131099754;
    public static final int depositProgressBody = 2131099755;
    public static final int depositProgressDialog = 2131099753;
    public static final int detailsAddress = 2131099671;
    public static final int detailsHours = 2131099676;
    public static final int detailsIcon = 2131099672;
    public static final int detailsPhone = 2131099674;
    public static final int detailsServices = 2131099678;
    public static final int directionsButton = 2131099679;
    public static final int frameLayout1 = 2131099748;
    public static final int hoursContainer = 2131099675;
    public static final int infoDone = 2131099765;
    public static final int loading = 2131099665;
    public static final int loadingprogress = 2131099664;
    public static final int locPicker = 2131099667;
    public static final int locSearchButton = 2131099669;
    public static final int location = 2131099666;
    public static final int locationCurtain = 2131099720;
    public static final int locationForm = 2131099680;
    public static final int location_details = 2131099670;
    public static final int location_results = 2131099690;
    public static final int mapContainer = 2131099692;
    public static final int menu_account = 2131099774;
    public static final int menu_deposit = 2131099775;
    public static final int menu_info = 2131099773;
    public static final int menu_loc = 2131099778;
    public static final int menu_signOff = 2131099776;
    public static final int menu_signOn = 2131099777;
    public static final int more = 2131099719;
    public static final int name = 2131099761;
    public static final int orientationWarningMessage = 2131099663;
    public static final int password = 2131099767;
    public static final int phoneContainer = 2131099673;
    public static final int photoTips = 2131099651;
    public static final int photoTipsCheckGoodBad = 2131099741;
    public static final int photoTipsImage = 2131099652;
    public static final int photoTipsText = 2131099653;
    public static final int photoTipsText0 = 2131099742;
    public static final int photoTipsText1 = 2131099743;
    public static final int photoTipsText2 = 2131099744;
    public static final int photoTipsText3 = 2131099745;
    public static final int privacyPolicy = 2131099772;
    public static final int quickGuide = 2131099771;
    public static final int relativeLayout1 = 2131099750;
    public static final int release = 2131099763;
    public static final int result0 = 2131099698;
    public static final int result1 = 2131099702;
    public static final int result2 = 2131099706;
    public static final int result3 = 2131099710;
    public static final int result4 = 2131099714;
    public static final int result5 = 2131099721;
    public static final int result6 = 2131099725;
    public static final int result7 = 2131099729;
    public static final int result8 = 2131099733;
    public static final int result9 = 2131099737;
    public static final int resultContent0 = 2131099700;
    public static final int resultContent1 = 2131099704;
    public static final int resultContent2 = 2131099708;
    public static final int resultContent3 = 2131099712;
    public static final int resultContent4 = 2131099716;
    public static final int resultContent5 = 2131099723;
    public static final int resultContent6 = 2131099727;
    public static final int resultContent7 = 2131099731;
    public static final int resultContent8 = 2131099735;
    public static final int resultContent9 = 2131099739;
    public static final int resultIcon0 = 2131099701;
    public static final int resultIcon1 = 2131099705;
    public static final int resultIcon2 = 2131099709;
    public static final int resultIcon3 = 2131099713;
    public static final int resultIcon4 = 2131099717;
    public static final int resultIcon5 = 2131099724;
    public static final int resultIcon6 = 2131099728;
    public static final int resultIcon7 = 2131099732;
    public static final int resultIcon8 = 2131099736;
    public static final int resultIcon9 = 2131099740;
    public static final int resultLabel0 = 2131099699;
    public static final int resultLabel1 = 2131099703;
    public static final int resultLabel2 = 2131099707;
    public static final int resultLabel3 = 2131099711;
    public static final int resultLabel4 = 2131099715;
    public static final int resultLabel5 = 2131099722;
    public static final int resultLabel6 = 2131099726;
    public static final int resultLabel7 = 2131099730;
    public static final int resultLabel8 = 2131099734;
    public static final int resultLabel9 = 2131099738;
    public static final int resultMap = 2131099693;
    public static final int resultNumbers = 2131099697;
    public static final int results_scroller = 2131099691;
    public static final int searchAddress = 2131099696;
    public static final int securityGuarantee = 2131099769;
    public static final int securityText = 2131099770;
    public static final int servicesContainer = 2131099677;
    public static final int signOn = 2131099768;
    public static final int state = 2131099685;
    public static final int stateGo = 2131099686;
    public static final int tog_atm = 2131099687;
    public static final int tog_branch = 2131099688;
    public static final int tog_instore = 2131099689;
    public static final int username = 2131099766;
    public static final int version = 2131099764;
    public static final int webview = 2131099756;
    public static final int webviewCurtain = 2131099759;
    public static final int webviewProgressDialog = 2131099758;
    public static final int webviewloading = 2131099757;
    public static final int wfMain = 2131099760;
    public static final int zip = 2131099681;
    public static final int zipGo = 2131099682;
    public static final int zoomcontrols = 2131099695;
    public static final int zoomview = 2131099694;
  }

  public static final class layout
  {
    public static final int capture_photo = 2130903040;
    public static final int include_banner = 2130903041;
    public static final int include_curtain = 2130903042;
    public static final int location = 2130903043;
    public static final int location_details = 2130903044;
    public static final int location_form = 2130903045;
    public static final int location_results = 2130903046;
    public static final int photo_tips = 2130903047;
    public static final int preview_check_image = 2130903048;
    public static final int submit_deposit_progress_dialog = 2130903049;
    public static final int webview = 2130903050;
    public static final int wfinfo = 2130903051;
    public static final int wfmain = 2130903052;
  }

  public static final class menu
  {
    public static final int info_menu = 2131361792;
    public static final int loc_insession_deposit_menu = 2131361793;
    public static final int loc_insession_menu = 2131361794;
    public static final int loc_public_menu = 2131361795;
    public static final int webview_deposit_menu = 2131361796;
    public static final int webview_menu = 2131361797;
  }

  public static final class string
  {
    public static final int _continue = 2131165210;
    public static final int address = 2131165192;
    public static final int address_bad = 2131165193;
    public static final int address_or_intersection = 2131165194;
    public static final int app_name = 2131165195;
    public static final int atm = 2131165196;
    public static final int back_to_banking = 2131165197;
    public static final int branches = 2131165198;
    public static final int brokerage_url_path = 2131165186;
    public static final int build_date = 2131165199;
    public static final int camera_error_opening = 2131165204;
    public static final int camera_guide_bottom_left = 2131165200;
    public static final int camera_guide_bottom_right = 2131165201;
    public static final int camera_guide_top_left = 2131165202;
    public static final int camera_guide_top_right = 2131165203;
    public static final int cancel = 2131165205;
    public static final int cd_checkImage = 2131165206;
    public static final int city = 2131165207;
    public static final int close = 2131165208;
    public static final int company = 2131165209;
    public static final int current_location = 2131165211;
    public static final int deposit = 2131165212;
    public static final int deposit_image_rear_upload_progress_title = 2131165213;
    public static final int deposit_progress_body = 2131165215;
    public static final int deposit_progress_title = 2131165214;
    public static final int divider = 2131165216;
    public static final int done = 2131165217;
    public static final int error_communications = 2131165218;
    public static final int find_atm = 2131165219;
    public static final int gps_all_off = 2131165220;
    public static final int gps_error_message = 2131165221;
    public static final int gps_error_title = 2131165222;
    public static final int gps_off_message = 2131165223;
    public static final int gps_off_title = 2131165224;
    public static final int gps_timeout_message = 2131165225;
    public static final int gps_timeout_title = 2131165226;
    public static final int heading_atms_locs = 2131165227;
    public static final int heading_banner = 2131165228;
    public static final int heading_location_details = 2131165229;
    public static final int heading_location_search_narrow = 2131165231;
    public static final int heading_location_search_results = 2131165230;
    public static final int heading_mobile_banking = 2131165232;
    public static final int horses = 2131165234;
    public static final int hours = 2131165233;
    public static final int in_store = 2131165236;
    public static final int info = 2131165235;
    public static final int loading = 2131165237;
    public static final int loading_gps = 2131165238;
    public static final int loading_location = 2131165239;
    public static final int loading_login = 2131165240;
    public static final int loc_back = 2131165241;
    public static final int loc_details = 2131165242;
    public static final int loc_error_message = 2131165243;
    public static final int loc_error_message_zero_results = 2131165244;
    public static final int loc_error_title = 2131165245;
    public static final int loc_more = 2131165246;
    public static final int loc_or = 2131165247;
    public static final int loc_results_icon = 2131165248;
    public static final int login_error = 2131165249;
    public static final int map_and_directions = 2131165250;
    public static final int map_key = 2131165191;
    public static final int nav_away = 2131165251;
    public static final int network_error_message = 2131165252;
    public static final int network_error_title = 2131165253;
    public static final int ok = 2131165254;
    public static final int online_password = 2131165255;
    public static final int online_username = 2131165256;
    public static final int orientation_placeholder = 2131165257;
    public static final int password = 2131165258;
    public static final int password_empty = 2131165259;
    public static final int phone = 2131165260;
    public static final int photo_tips = 2131165261;
    public static final int photo_tips_best_results = 2131165264;
    public static final int photo_tips_check_alignment = 2131165262;
    public static final int photo_tips_dont_show_again = 2131165263;
    public static final int photo_tips_tip1 = 2131165265;
    public static final int photo_tips_tip2 = 2131165266;
    public static final int photo_tips_tip3 = 2131165267;
    public static final int privacy_policy_focused = 2131165268;
    public static final int qguide_unfocused = 2131165269;
    public static final int release_date = 2131165270;
    public static final int retake = 2131165271;
    public static final int review_photo_front = 2131165272;
    public static final int review_photo_rear = 2131165273;
    public static final int search = 2131165274;
    public static final int security_unfocused = 2131165275;
    public static final int services = 2131165276;
    public static final int sign_off = 2131165279;
    public static final int sign_on = 2131165277;
    public static final int sign_on_error_dialog_title = 2131165278;
    public static final int state = 2131165280;
    public static final int take_photo = 2131165281;
    public static final int take_photo_front = 2131165282;
    public static final int take_photo_rear = 2131165283;
    public static final int use = 2131165284;
    public static final int username = 2131165285;
    public static final int username_empty = 2131165286;
    public static final int username_error = 2131165287;
    public static final int version = 2131165288;
    public static final int webview_browser_link = 2131165289;
    public static final int wf_appid = 2131165187;
    public static final int wf_info_builddate = 2131165189;
    public static final int wf_info_name = 2131165290;
    public static final int wf_info_release = 2131165190;
    public static final int wf_info_version = 2131165188;
    public static final int wf_url = 2131165185;
    public static final int wf_url_initial = 2131165184;
    public static final int zip_code = 2131165291;
    public static final int zip_code_address = 2131165292;
    public static final int zip_empty = 2131165293;
  }

  public static final class style
  {
    public static final int CheckInstructions = 2131296298;
    public static final int ResultBack = 2131296295;
    public static final int ResultContainer = 2131296286;
    public static final int ResultContainerBotEven = 2131296290;
    public static final int ResultContainerBotOdd = 2131296291;
    public static final int ResultContainerEven = 2131296287;
    public static final int ResultContainerOdd = 2131296288;
    public static final int ResultContainerTop = 2131296289;
    public static final int ResultContent = 2131296293;
    public static final int ResultIcon = 2131296294;
    public static final int ResultLabel = 2131296292;
    public static final int ResultMore = 2131296296;
    public static final int Security = 2131296261;
    public static final int SecurityGuaranteeQuickGuide = 2131296271;
    public static final int WFAddressField = 2131296277;
    public static final int WFButton = 2131296281;
    public static final int WFButtonBlue = 2131296282;
    public static final int WFButtonGrey = 2131296283;
    public static final int WFDetailsContent = 2131296266;
    public static final int WFDetailsContentLabel = 2131296267;
    public static final int WFDialog = 2131296257;
    public static final int WFDivider = 2131296297;
    public static final int WFField = 2131296274;
    public static final int WFFieldLabel = 2131296279;
    public static final int WFHeader = 2131296258;
    public static final int WFHeaderBevel = 2131296259;
    public static final int WFHeadline = 2131296264;
    public static final int WFHorses = 2131296260;
    public static final int WFLink = 2131296270;
    public static final int WFLoginField = 2131296275;
    public static final int WFSearchButton = 2131296284;
    public static final int WFStateField = 2131296278;
    public static final int WFText = 2131296262;
    public static final int WFTextContent = 2131296265;
    public static final int WFTextContentLabel = 2131296268;
    public static final int WFTextFooter = 2131296269;
    public static final int WFTextTitle = 2131296263;
    public static final int WFTheme = 2131296256;
    public static final int WFTitleBackground = 2131296273;
    public static final int WFTitleImage = 2131296272;
    public static final int WFTogLabel = 2131296280;
    public static final int WFToggleButton = 2131296285;
    public static final int WFZipField = 2131296276;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.wf.wellsfargomobile-700220\classes_dex2jar.jar
 * Qualified Name:     com.wf.wellsfargomobile.R
 * JD-Core Version:    0.6.2
 */