package com.wf.wellsfargomobile;

import android.app.Application;
import android.graphics.Bitmap;
import com.wf.wellsfargomobile.util.Trinary;

public class WFApp extends Application
{
  public static final String ACCOUNT_LIST_URI = "/accounts/g.accountList.do";
  public static final int CONN_TIMEOUT_DATA_SUBMIT = 2;
  public static final int CONN_TIMEOUT_IMAGE_UPLOAD = 2;
  public static final String DEPOSIT_CANCEL_URI = "/deposit/depositCancel.action";
  public static final String DEPOSIT_CHECK_URI = "/deposit/depositCheck.action";
  public static final String DEPOSIT_DETAIL_URI = "/deposit/depositDetail.action";
  public static final String DEPOSIT_ERROR_URI = "/deposit/depositDetail.action";
  public static final String DEPOSIT_MITEK_ERROR_URI = "/deposit/recoverableMitekError.action";
  public static final String DEPOSIT_RESPONSE_FAILED = "Failed";
  public static final String DEPOSIT_RESPONSE_SUCCSSS = "Success";
  public static final int IMAGE_UPLOAD_RETRY_LOOP_MAX_SECONDS = 120;
  public static final long IMAGE_UPLOAD_RETRY_LOOP_SLEEP = 1000L;
  public static final String JAVASCRIPT_INTERFACE_NAME_CAMERA = "Camera";
  public static final String JAVASCRIPT_INTERFACE_NAME_COMMON = "Common";
  public static final String JAVASCRIPT_INTERFACE_NAME_PHOTO_TIPS = "PhotoTips";
  public static final String LOGIN_URI = "/signOn/appSignon.action";
  public static final long MAX_STOPPED_STATE_TIME = 600000L;
  public static final int MITEK_FRONT_IMAGE_COMPRESSION = 50;
  public static final int MITEK_FRONT_IMAGE_HEIGHT = 1200;
  public static final int MITEK_FRONT_IMAGE_WIDTH = 1600;
  public static final int MITEK_IMAGE_WIDTH = 1600;
  public static final int MITEK_REAR_IMAGE_COMPRESSION = 50;
  public static final int MITEK_REAR_IMAGE_HEIGHT = 1200;
  public static final int MITEK_REAR_IMAGE_WIDTH = 1600;
  public static final String MRDC_FLOW_CANCELED = "MRDC_FLOW_CANCELED";
  public static final String MRDC_FLOW_COMPLETED = "MRDC_FLOW_COMPLETED";
  public static final String MRDC_FLOW_ERRORED = "MRDC_FLOW_ERRORED";
  public static final String MRDC_FLOW_IMAGE_SIDE = "MRDC_FLOW_STATE";
  public static final String PERFORMANCE_TIMINGS_URI = "/deposit/performance.action";
  public static final String PHOTO_TIPS_DONT_SHOW = "PHOTO_TIPS_DONT_SHOW";
  public static final String PREFERENCES = "WF_PREFERENCES";
  public static final int REQUEST_CODE_ACTIVITY_LOCSEARCH = 2;
  public static final int REQUEST_CODE_CANCEL_MRDC_FLOW = 4;
  public static final int REQUEST_CODE_CAPTURE_PHOTO = 1;
  public static final int REQUEST_CODE_PREVIEW_CHECK_IMAGE = 3;
  public static final int RESULT_ACCOUNT = 2;
  public static final int RESULT_CODE_ACTIVITY_LOCSEARCH = 8;
  public static final int RESULT_CODE_CANCEL_MRDC_FLOW = 10;
  public static final int RESULT_CODE_CAPTURE_PHOTO = 7;
  public static final int RESULT_CODE_COMPLETE_MRDC_FLOW = 11;
  public static final int RESULT_CODE_PREVIEW_CHECK_IMAGE = 9;
  public static final int RESULT_CODE_RETAKE = 12;
  public static final int RESULT_CODE_SIGNOFF = 13;
  public static final int RESULT_DEPOSIT = 6;
  public static final int RESULT_ERROR = 1;
  public static final int RESULT_LOCSEARCH = 5;
  public static final int RESULT_SIGNOFF = 3;
  public static final int RESULT_SIGNON = 4;
  public static final String SAFE_JSON_PREFIX = "/*--safejson--";
  public static final String SAFE_JSON_SUFFIX = "--safejson--*/";
  public static final String SUBMIT_DEPOSIT_URI = "/deposit/submitDeposit.action";
  public static final String SUBMIT_IMAGE_BACK_URI = "/deposit/submitBackImage.action";
  public static final String SUBMIT_IMAGE_FRONT_URI = "/deposit/submitFrontImage.action";
  public static final CharSequence URL_PATH_TOKEN_BROKERAGE = "BMW";
  public static final CharSequence URL_PATH_TOKEN_MBA = "mba";
  private Trinary deviceHasCamera = Trinary.UNKNOWN;
  private boolean errorOpeningCamera = false;
  private byte[] frontCheckImage = null;
  private Bitmap frontCheckPreviewImage = null;
  private String frontImageToken;
  private boolean frontImageUploadProcessComplete = false;
  private boolean googleApiAvailable = true;
  private long lastActivityStoppedAt;
  private Trinary mrdcEnabled = Trinary.UNKNOWN;
  private String nonce = null;
  private long perfD1FrontImageStartSend;
  private long perfD2FrontImageJsonResponse;
  private long perfD3RearImageStartSend;
  private long perfD4RearImageJsonResponse;
  private long perfD5DataStartSend;
  private long perfD6DataJsonResponse;
  private byte[] rearCheckImage = null;
  private Bitmap rearCheckPreviewImage = null;
  private String rearImageToken;
  private boolean rearImageUploadProcessComplete = false;

  private void zeroByteArray(byte[] paramArrayOfByte)
  {
    if (paramArrayOfByte != null)
      for (int i = 0; i < paramArrayOfByte.length; i++)
        paramArrayOfByte[i] = 0;
  }

  public void clearCheckDepositData()
  {
    clearImageData();
    this.mrdcEnabled = Trinary.UNKNOWN;
  }

  public void clearImageData()
  {
    zeroByteArray(this.frontCheckImage);
    this.frontCheckImage = null;
    zeroByteArray(this.rearCheckImage);
    this.rearCheckImage = null;
    if (this.frontCheckPreviewImage != null)
      this.frontCheckPreviewImage.eraseColor(-16777216);
    this.frontCheckPreviewImage = null;
    if (this.rearCheckPreviewImage != null)
      this.rearCheckPreviewImage.eraseColor(-16777216);
    this.rearCheckPreviewImage = null;
    this.frontImageUploadProcessComplete = false;
    this.rearImageUploadProcessComplete = false;
  }

  public Trinary getDeviceHasCamera()
  {
    return this.deviceHasCamera;
  }

  public byte[] getFrontCheckImage()
  {
    return this.frontCheckImage;
  }

  public Bitmap getFrontCheckPreviewImage()
  {
    return this.frontCheckPreviewImage;
  }

  public String getFrontImageToken()
  {
    return this.frontImageToken;
  }

  public long getLastActivityStoppedAt()
  {
    return this.lastActivityStoppedAt;
  }

  public Trinary getMrdcEnabled()
  {
    return this.mrdcEnabled;
  }

  public String getNonce()
  {
    return this.nonce;
  }

  public long getPerfD1FrontImageStartSend()
  {
    return this.perfD1FrontImageStartSend;
  }

  public long getPerfD2FrontImageJsonResponse()
  {
    return this.perfD2FrontImageJsonResponse;
  }

  public long getPerfD3RearImageStartSend()
  {
    return this.perfD3RearImageStartSend;
  }

  public long getPerfD4RearImageJsonResponse()
  {
    return this.perfD4RearImageJsonResponse;
  }

  public long getPerfD5DataStartSend()
  {
    return this.perfD5DataStartSend;
  }

  public long getPerfD6DataJsonResponse()
  {
    return this.perfD6DataJsonResponse;
  }

  public byte[] getRearCheckImage()
  {
    return this.rearCheckImage;
  }

  public Bitmap getRearCheckPreviewImage()
  {
    return this.rearCheckPreviewImage;
  }

  public String getRearImageToken()
  {
    return this.rearImageToken;
  }

  public boolean isErrorOpeningCamera()
  {
    return this.errorOpeningCamera;
  }

  public boolean isFrontImageUploadProcessComplete()
  {
    return this.frontImageUploadProcessComplete;
  }

  public boolean isGoogleApiAvailable()
  {
    return this.googleApiAvailable;
  }

  public boolean isRearImageUploadProcessComplete()
  {
    return this.rearImageUploadProcessComplete;
  }

  public void setDeviceHasCamera(Trinary paramTrinary)
  {
    this.deviceHasCamera = paramTrinary;
  }

  public void setErrorOpeningCamera(boolean paramBoolean)
  {
    this.errorOpeningCamera = paramBoolean;
  }

  public void setFrontCheckImage(byte[] paramArrayOfByte)
  {
    this.frontCheckImage = paramArrayOfByte;
  }

  public void setFrontCheckPreviewImage(Bitmap paramBitmap)
  {
    this.frontCheckPreviewImage = paramBitmap;
  }

  public void setFrontImageToken(String paramString)
  {
    this.frontImageToken = paramString;
  }

  public void setFrontImageUploadProcessComplete(boolean paramBoolean)
  {
    this.frontImageUploadProcessComplete = paramBoolean;
  }

  public void setGoogleApiAvailable(boolean paramBoolean)
  {
    this.googleApiAvailable = paramBoolean;
  }

  public void setLastActivityStoppedAt(long paramLong)
  {
    this.lastActivityStoppedAt = paramLong;
  }

  public void setMrdcEnabled(Trinary paramTrinary)
  {
    this.mrdcEnabled = paramTrinary;
  }

  public void setNonce(String paramString)
  {
    this.nonce = paramString;
  }

  public void setPerfD1FrontImageStartSend(long paramLong)
  {
    this.perfD1FrontImageStartSend = paramLong;
  }

  public void setPerfD2FrontImageJsonResponse(long paramLong)
  {
    this.perfD2FrontImageJsonResponse = paramLong;
  }

  public void setPerfD3RearImageStartSend(long paramLong)
  {
    this.perfD3RearImageStartSend = paramLong;
  }

  public void setPerfD4RearImageJsonResponse(long paramLong)
  {
    this.perfD4RearImageJsonResponse = paramLong;
  }

  public void setPerfD5DataStartSend(long paramLong)
  {
    this.perfD5DataStartSend = paramLong;
  }

  public void setPerfD6DataJsonResponse(long paramLong)
  {
    this.perfD6DataJsonResponse = paramLong;
  }

  public void setRearCheckImage(byte[] paramArrayOfByte)
  {
    this.rearCheckImage = paramArrayOfByte;
  }

  public void setRearCheckPreviewImage(Bitmap paramBitmap)
  {
    this.rearCheckPreviewImage = paramBitmap;
  }

  public void setRearImageToken(String paramString)
  {
    this.rearImageToken = paramString;
  }

  public void setRearImageUploadProcessComplete(boolean paramBoolean)
  {
    this.rearImageUploadProcessComplete = paramBoolean;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.wf.wellsfargomobile-700220\classes_dex2jar.jar
 * Qualified Name:     com.wf.wellsfargomobile.WFApp
 * JD-Core Version:    0.6.2
 */