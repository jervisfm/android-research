package com.wf.wellsfargomobile;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class WFInfo extends BaseActivity
{
  private View.OnClickListener clickHandler = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      if (paramAnonymousView == WFInfo.this.doneButton)
        WFInfo.this.finish();
    }
  };
  private Button doneButton;

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903051);
    this.doneButton = ((Button)findViewById(2131099765));
    this.doneButton.setOnClickListener(this.clickHandler);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.wf.wellsfargomobile-700220\classes_dex2jar.jar
 * Qualified Name:     com.wf.wellsfargomobile.WFInfo
 * JD-Core Version:    0.6.2
 */