package com.wf.wellsfargomobile;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import com.wf.wellsfargomobile.loc.LocationForm;
import com.wf.wellsfargomobile.loc.LocationResults;
import com.wf.wellsfargomobile.util.NonPersistentExtraStorage;
import com.wf.wellsfargomobile.util.Trinary;
import java.util.UUID;

public class WFMain extends BaseActivity
{
  public static final int ACTIVITY_SEARCH = 1;
  public static final int ACTIVITY_SEARCHFORM = 2;
  public static final int ACTIVITY_WEBVIEW = 0;
  public static final String KEY_DEPOSIT = "deposit";
  public static final String KEY_LOGIN = "login_request";
  public static final String KEY_PASSWORD = "password";
  public static final String KEY_PRIVACY_POLICY = "privacy_policy_request";
  public static final String KEY_QGUIDE = "qguide_request";
  public static final String KEY_SECURITY = "security_request";
  public static final String KEY_USERNAME = "username";
  public static final String KEY_WEBATMLOC = "webatmloc_request";
  private static final String TAG = "WFMain";
  private View.OnClickListener clickHandler = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      if (paramAnonymousView == WFMain.this.signOnButton)
      {
        WFMain.this.signOnButton.setEnabled(false);
        WFMain.this.loginGo();
      }
      while (true)
      {
        WFMain.this.password.setText("");
        return;
        if (paramAnonymousView == WFMain.this.currentLocButton)
        {
          Intent localIntent1 = new Intent(WFMain.this, LocationResults.class);
          localIntent1.putExtra("was_gps", true);
          WFMain.this.startActivityForResult(localIntent1, 1);
        }
        else if (paramAnonymousView == WFMain.this.locSearchButton)
        {
          if (WFMain.this.wfApp.isGoogleApiAvailable())
          {
            WFMain.this.startActivityForResult(new Intent(WFMain.this, LocationForm.class), 2);
          }
          else
          {
            WFMain.this.locSearchButton.requestFocusFromTouch();
            Intent localIntent5 = new Intent(WFMain.this, WFwebview.class);
            localIntent5.putExtra("webatmloc_request", true);
            WFMain.this.startActivityForResult(localIntent5, 0);
          }
        }
        else if (paramAnonymousView == WFMain.this.security)
        {
          WFMain.this.security.requestFocusFromTouch();
          Intent localIntent4 = new Intent(WFMain.this, WFwebview.class);
          localIntent4.putExtra("security_request", true);
          WFMain.this.startActivityForResult(localIntent4, 0);
        }
        else if (paramAnonymousView == WFMain.this.quickGuide)
        {
          WFMain.this.quickGuide.requestFocusFromTouch();
          Intent localIntent3 = new Intent(WFMain.this, WFwebview.class);
          localIntent3.putExtra("qguide_request", true);
          WFMain.this.startActivityForResult(localIntent3, 0);
        }
        else if (paramAnonymousView == WFMain.this.privacyPolicy)
        {
          WFMain.this.privacyPolicy.requestFocusFromTouch();
          Intent localIntent2 = new Intent(WFMain.this, WFwebview.class);
          localIntent2.putExtra("privacy_policy_request", true);
          WFMain.this.startActivityForResult(localIntent2, 0);
        }
      }
    }
  };
  private Button currentLocButton;
  private Button locSearchButton;
  private EditText password;
  private TextView privacyPolicy;
  private TextView quickGuide;
  private LinearLayout security;
  private Button signOnButton;
  private EditText username;
  private WFApp wfApp;

  private void loginGo()
  {
    Editable localEditable1 = this.username.getText();
    Editable localEditable2 = this.password.getText();
    if (localEditable1.length() == 0)
    {
      this.signOnButton.setEnabled(true);
      showDialogBox(null, getString(2131165286));
    }
    while (true)
    {
      this.password.setText("");
      return;
      if (localEditable1.length() < 6)
      {
        this.signOnButton.setEnabled(true);
        showDialogBox(null, getString(2131165287));
      }
      else if (localEditable2.length() == 0)
      {
        this.signOnButton.setEnabled(true);
        showDialogBox(null, getString(2131165259));
      }
      else
      {
        this.wfApp.setNonce(UUID.randomUUID().toString());
        Intent localIntent = new Intent(this, WFwebview.class);
        localIntent.putExtra("login_request", true);
        NonPersistentExtraStorage.putExtras("username", localEditable1);
        NonPersistentExtraStorage.putExtras("password", localEditable2);
        this.username.setText("");
        this.password.setText("");
        this.username.requestFocus();
        Log.d("WFMain", "starting webview");
        startActivityForResult(localIntent, 0);
      }
    }
  }

  private void showDialogBox(String paramString1, String paramString2)
  {
    if (paramString1 != null)
    {
      new AlertDialog.Builder(this).setIcon(17301543).setTitle(paramString1).setMessage(paramString2).setPositiveButton("OK", new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
        }
      }).create().show();
      return;
    }
    new AlertDialog.Builder(this).setIcon(17301543).setTitle(paramString2).setPositiveButton("OK", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
      }
    }).create().show();
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    if ((paramInt1 == 2) || (paramInt2 == 4))
      this.username.requestFocus();
    do
    {
      return;
      if (paramInt2 == 5)
      {
        this.currentLocButton.requestFocus();
        return;
      }
    }
    while (((paramInt1 != 0) && (paramInt1 != 1)) || (paramInt2 != 1));
    this.signOnButton.setEnabled(true);
    Bundle localBundle = paramIntent.getExtras();
    if (localBundle.containsKey("error_title"));
    for (String str = localBundle.getString("error_title"); ; str = null)
    {
      showDialogBox(str, localBundle.getString("error_msg"));
      return;
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903052);
    this.wfApp = ((WFApp)getApplication());
    this.username = ((EditText)findViewById(2131099766));
    this.password = ((EditText)findViewById(2131099767));
    this.password.setTransformationMethod(new PasswordTransformationMethod());
    this.password.setRawInputType(129);
    this.signOnButton = ((Button)findViewById(2131099768));
    this.signOnButton.setOnClickListener(this.clickHandler);
    this.currentLocButton = ((Button)findViewById(2131099668));
    this.currentLocButton.setOnClickListener(this.clickHandler);
    this.locSearchButton = ((Button)findViewById(2131099669));
    this.locSearchButton.setOnClickListener(this.clickHandler);
    this.security = ((LinearLayout)findViewById(2131099769));
    this.security.setOnClickListener(this.clickHandler);
    this.quickGuide = ((TextView)findViewById(2131099771));
    this.quickGuide.setOnClickListener(this.clickHandler);
    this.privacyPolicy = ((TextView)findViewById(2131099772));
    this.privacyPolicy.setOnClickListener(this.clickHandler);
    this.password.setOnEditorActionListener(new TextView.OnEditorActionListener()
    {
      public boolean onEditorAction(TextView paramAnonymousTextView, int paramAnonymousInt, KeyEvent paramAnonymousKeyEvent)
      {
        if ((paramAnonymousInt == 2) || (paramAnonymousInt == 0))
        {
          WFMain.this.loginGo();
          return true;
        }
        return false;
      }
    });
    try
    {
      this.wfApp.setGoogleApiAvailable(true);
      if (!this.wfApp.isGoogleApiAvailable())
        this.currentLocButton.setVisibility(8);
      if (getPackageManager().hasSystemFeature("android.hardware.camera"))
      {
        this.wfApp.setDeviceHasCamera(Trinary.TRUE);
        return;
      }
    }
    catch (NoClassDefFoundError localNoClassDefFoundError)
    {
      while (true)
        this.wfApp.setGoogleApiAvailable(false);
      this.wfApp.setDeviceHasCamera(Trinary.FALSE);
    }
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getMenuInflater().inflate(2131361792, paramMenu);
    return true;
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
      return false;
    case 2131099773:
    }
    startActivity(new Intent(this, WFInfo.class));
    return true;
  }

  protected void onPause()
  {
    this.username.setText("");
    this.password.setText("");
    super.onPause();
  }

  protected void onResume()
  {
    this.username.setText("");
    this.password.setText("");
    this.signOnButton.setEnabled(true);
    super.onResume();
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    this.username.setText("");
    this.password.setText("");
    super.onSaveInstanceState(paramBundle);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.wf.wellsfargomobile-700220\classes_dex2jar.jar
 * Qualified Name:     com.wf.wellsfargomobile.WFMain
 * JD-Core Version:    0.6.2
 */