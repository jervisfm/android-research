package com.wf.wellsfargomobile;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebViewDatabase;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.wf.wellsfargomobile.loc.Location;
import com.wf.wellsfargomobile.mrdc.CapturePhoto;
import com.wf.wellsfargomobile.mrdc.ImageMode;
import com.wf.wellsfargomobile.mrdc.JavaScriptInterfacePhotoTips;
import com.wf.wellsfargomobile.util.NonPersistentExtraStorage;
import com.wf.wellsfargomobile.util.Trinary;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class WFwebview extends BaseActivity
{
  public static final int ACTIVITY_LOCSEARCH = 0;
  public static final String KEY_ERRORMESSAGE = "error_msg";
  public static final String KEY_ERRORTITLE = "error_title";
  private static final String TAG = "WFwebview";
  private boolean firstLoad = false;
  private Boolean inSession;
  private TextView loadStatus;
  private RelativeLayout loading;
  private boolean loginFirstLoad = false;
  private String outsideURL;
  private CharSequence password;
  private Dialog submitDepositProgressDialog;
  private Dialog submitRearImageProgressDialog;
  private CharSequence username;
  private WebView webview;
  private View webviewCurtain;
  private WFApp wfApp = null;

  private void clearAndPreventCache()
  {
    try
    {
      this.webview.clearCache(true);
      return;
    }
    catch (Throwable localThrowable)
    {
      Log.d("WFwebview", "Bad clearAndPreventCache: " + localThrowable.getMessage());
    }
  }

  private void clearWebViewDatabaseAndCookies(boolean paramBoolean)
  {
    try
    {
      WebViewDatabase localWebViewDatabase = WebViewDatabase.getInstance(this);
      localWebViewDatabase.clearFormData();
      localWebViewDatabase.clearUsernamePassword();
      localWebViewDatabase.clearHttpAuthUsernamePassword();
      if (paramBoolean)
      {
        Log.d("WFwebview", "before CookieManager.getInstance()");
        CookieSyncManager.createInstance(this);
        CookieManager.getInstance().removeAllCookie();
        CookieSyncManager.getInstance().sync();
        Log.d("WFwebview", "after CookieManager.getInstance()");
      }
      return;
    }
    catch (Throwable localThrowable)
    {
      Log.d("WFwebview", "Bad clearWebViewDatabaseAndCookies: " + localThrowable.getMessage());
    }
  }

  private void depositCheck()
  {
    Log.d("WFwebview", "depositCheck()");
    Object localObject = "/deposit/depositCheck.action?";
    String str1 = getString(2131165185);
    String str2 = this.webview.getUrl();
    Log.d("WFwebview", "depositCheck() - current URL " + str2);
    try
    {
      URL localURL = new URL(str2);
      String str3 = localURL.getPath();
      Log.d("WFwebview", "depositCheck() - current URL path: " + str3);
      if (str3.contains(WFApp.URL_PATH_TOKEN_BROKERAGE))
      {
        Log.d("WFwebview", "depositCheck() - contains " + WFApp.URL_PATH_TOKEN_BROKERAGE);
        String str4 = localURL.getProtocol();
        String str5 = localURL.getHost();
        int i = localURL.getPort();
        str1 = str4 + "://" + str5;
        if ((i != 80) && (i != 443) && (i != -1))
          str1 = str1 + ":" + i;
        String str6 = getString(2131165186) + "gotoMMRDC";
        localObject = str6;
      }
      while (true)
      {
        label259: Log.d("WFwebview", "depositCheck() - baseUrl: " + str1 + "navToUrl: " + (String)localObject);
        loadURL((String)localObject, str1);
        return;
        Log.d("WFwebview", "depositCheck() - does not contains " + WFApp.URL_PATH_TOKEN_BROKERAGE);
      }
    }
    catch (MalformedURLException localMalformedURLException)
    {
      break label259;
    }
  }

  private void goLogin()
  {
    Object localObject1 = "";
    label591: 
    while (true)
      try
      {
        boolean bool = Trinary.TRUE.equals(this.wfApp.getDeviceHasCamera());
        Camera localCamera;
        if (bool)
          localCamera = null;
        try
        {
          localCamera = Camera.open();
          if (localCamera != null)
          {
            List localList = localCamera.getParameters().getSupportedFocusModes();
            if (localList != null)
            {
              String str = localList.toString();
              localObject1 = str;
            }
          }
          if (localCamera != null)
          {
            localCamera.stopPreview();
            localCamera.release();
            break label591;
            this.loginFirstLoad = true;
            this.loadStatus.setText(2131165240);
            ArrayList localArrayList = new ArrayList();
            localArrayList.add(new BasicNameValuePair("WFAppId", getString(2131165187)));
            localArrayList.add(new BasicNameValuePair("disableBackLink", "x"));
            localArrayList.add(new BasicNameValuePair("userId", this.username.toString()));
            localArrayList.add(new BasicNameValuePair("password", this.password.toString()));
            localArrayList.add(new BasicNameValuePair("hasCamera", this.wfApp.getDeviceHasCamera().toString()));
            localArrayList.add(new BasicNameValuePair("WFAppVersion", getString(2131165188)));
            localArrayList.add(new BasicNameValuePair("nonce", this.wfApp.getNonce()));
            localArrayList.add(new BasicNameValuePair("manufacturer", Build.MANUFACTURER));
            localArrayList.add(new BasicNameValuePair("modelNumber", Build.MODEL));
            localArrayList.add(new BasicNameValuePair("modelName", Build.DEVICE));
            localArrayList.add(new BasicNameValuePair("carrier", Build.BRAND));
            localArrayList.add(new BasicNameValuePair("osVersion", Build.VERSION.RELEASE));
            localArrayList.add(new BasicNameValuePair("fingerprint", Build.FINGERPRINT));
            localArrayList.add(new BasicNameValuePair("focusModes", (String)localObject1));
            UrlEncodedFormEntity localUrlEncodedFormEntity = new UrlEncodedFormEntity(localArrayList);
            this.webview.postUrl(getString(2131165184) + "/signOn/appSignon.action", EntityUtils.toByteArray(localUrlEncodedFormEntity));
            return;
          }
        }
        catch (RuntimeException localRuntimeException)
        {
          this.wfApp.setErrorOpeningCamera(true);
          Log.d("WFwebview", "exception occured while getting camera properties");
        }
        finally
        {
          if (localCamera != null)
          {
            localCamera.stopPreview();
            localCamera.release();
          }
        }
      }
      catch (UnsupportedEncodingException localUnsupportedEncodingException)
      {
        returnWithError(getString(2131165278), getString(2131165249));
        return;
      }
      catch (IOException localIOException)
      {
        returnWithError(getString(2131165278), getString(2131165249));
        return;
      }
      catch (Exception localException)
      {
        returnWithError(getString(2131165278), getString(2131165249));
        return;
      }
  }

  private void launchLocSearch()
  {
    if (this.inSession.booleanValue())
    {
      startActivityForResult(new Intent(this, Location.class), 2);
      return;
    }
    setResult(5, null);
    finish();
  }

  private void loadInitialURL(String paramString)
  {
    loadURL(paramString, getString(2131165184));
  }

  private void loadJavaScript(String paramString)
  {
    this.webview.loadUrl("javascript:" + paramString);
  }

  private void loadURL(String paramString)
  {
    loadURL(paramString, getString(2131165185));
  }

  private void loadURL(String paramString1, String paramString2)
  {
    this.firstLoad = true;
    StringBuilder localStringBuilder = new StringBuilder(150);
    localStringBuilder.append(paramString2);
    localStringBuilder.append(paramString1);
    if (!paramString1.contains("?"))
      localStringBuilder.append("?");
    localStringBuilder.append("&WFAppId=");
    localStringBuilder.append(getString(2131165187));
    localStringBuilder.append("&disableBackLink=x");
    Log.d("WFwebview", "requesting URL: " + localStringBuilder.toString());
    this.webview.loadUrl(localStringBuilder.toString());
  }

  private void makeSureWebViewIsSecure()
  {
    WebSettings localWebSettings = this.webview.getSettings();
    localWebSettings.setSaveFormData(false);
    localWebSettings.setSavePassword(false);
    localWebSettings.setAllowFileAccess(false);
    localWebSettings.setNavDump(false);
    localWebSettings.setPluginsEnabled(false);
    localWebSettings.setSupportMultipleWindows(false);
    localWebSettings.setCacheMode(2);
    clearAndPreventCache();
    clearWebViewDatabaseAndCookies(true);
  }

  private void returnWithError(String paramString1, String paramString2)
  {
    Bundle localBundle = new Bundle();
    if (paramString1 != null)
      localBundle.putString("error_title", paramString1);
    localBundle.putString("error_msg", paramString2);
    Intent localIntent = new Intent();
    localIntent.putExtras(localBundle);
    setResult(1, localIntent);
    finish();
  }

  private void signOff()
  {
    Object localObject = getString(2131165185) + "/accounts/g.signOff.do";
    String str1 = this.webview.getUrl();
    Log.d("WFwebview", "signOff() - current URL " + str1);
    try
    {
      URL localURL = new URL(str1);
      String str2 = localURL.getPath();
      Log.d("WFwebview", "signOff() - current URL path: " + str2);
      if (str2.contains(WFApp.URL_PATH_TOKEN_BROKERAGE))
      {
        Log.d("WFwebview", "signOff() - contains BMW");
        String str3 = localURL.getProtocol();
        String str4 = localURL.getHost();
        int i = localURL.getPort();
        localObject = str3 + "://" + str4;
        if ((i != 80) && (i != 443) && (i != -1))
          localObject = (String)localObject + ":" + i;
        String str5 = (String)localObject + getString(2131165186) + "gotoMLogoff";
        localObject = str5;
      }
      while (true)
      {
        label252: Log.d("WFwebview", "signOff() - navToUrl: " + (String)localObject);
        this.inSession = Boolean.valueOf(false);
        this.wfApp.clearCheckDepositData();
        this.wfApp.setNonce(null);
        this.webview.loadUrl((String)localObject);
        return;
        Log.d("WFwebview", "signOff() - does not contains BMW");
      }
    }
    catch (MalformedURLException localMalformedURLException)
    {
      break label252;
    }
  }

  private boolean validNonce(String paramString)
  {
    Log.d("WFwebview", "validNonce(" + paramString + ") - wfApp.getNonce(): " + this.wfApp.getNonce());
    if ((paramString == null) || (this.wfApp.getNonce() == null));
    while (!paramString.equals(this.wfApp.getNonce()))
      return false;
    return true;
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    switch (paramInt2)
    {
    case 7:
    case 9:
    default:
    case 8:
      do
        return;
      while (paramInt2 != 3);
      signOff();
      return;
    case 11:
      Log.d("WFwebview", "onActivityResult - completing MRDC flow ");
      Log.d("WFwebview", "rewinding");
      runOnUiThread(new Runnable()
      {
        public void run()
        {
          WFwebview.this.submitRearImageProgressDialog.show();
        }
      });
      new SubmitRearImageThread(this).execute(new Void[0]);
      return;
    case 10:
      Log.d("WFwebview", "onActivityResult - cancelling MRDC flow  ");
      Log.d("WFwebview", "rewinding");
      this.wfApp.clearCheckDepositData();
      depositCheck();
      return;
    case 6:
    }
    Log.d("WFwebview", "onActivityResult - returning from location search  ");
    this.wfApp.clearCheckDepositData();
    depositCheck();
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.wfApp = ((WFApp)getApplication());
    setContentView(2130903050);
    this.webview = ((WebView)findViewById(2131099756));
    this.webview.setWebViewClient(new WFWebViewClient(null));
    this.webview.getSettings().setJavaScriptEnabled(true);
    this.webview.addJavascriptInterface(new CommonJavaScriptInterface(this), "Common");
    this.webview.addJavascriptInterface(new CheckDepositJavaScriptInterface(this), "Camera");
    this.webview.addJavascriptInterface(new JavaScriptInterfacePhotoTips(this, this.wfApp), "PhotoTips");
    makeSureWebViewIsSecure();
    this.wfApp.clearCheckDepositData();
    this.submitRearImageProgressDialog = new Dialog(this, 2131296257);
    this.submitRearImageProgressDialog.getWindow().setFlags(4, 4);
    this.submitRearImageProgressDialog.setTitle(getString(2131165213));
    this.submitRearImageProgressDialog.setContentView(2130903049);
    this.submitDepositProgressDialog = new Dialog(this, 2131296257);
    this.submitDepositProgressDialog.getWindow().setFlags(4, 4);
    this.submitDepositProgressDialog.setTitle(getString(2131165214));
    this.submitDepositProgressDialog.setContentView(2130903049);
    this.webviewCurtain = findViewById(2131099759);
    this.loadStatus = ((TextView)findViewById(2131099665));
    this.loading = ((RelativeLayout)findViewById(2131099757));
    this.loading.setVisibility(8);
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
    {
      if (!localBundle.containsKey("login_request"))
        break label346;
      this.username = ((CharSequence)NonPersistentExtraStorage.getExtras("username"));
      this.password = ((CharSequence)NonPersistentExtraStorage.getExtras("password"));
      this.inSession = Boolean.valueOf(true);
      goLogin();
    }
    label346: 
    do
    {
      return;
      if (localBundle.containsKey("security_request"))
      {
        this.inSession = Boolean.valueOf(false);
        loadInitialURL("/signOn/securityGuarantee.action?disableBackLink=x");
        return;
      }
      if (localBundle.containsKey("qguide_request"))
      {
        this.inSession = Boolean.valueOf(false);
        loadInitialURL("/faqs/begin.do?disableBackLink=x");
        return;
      }
      if (localBundle.containsKey("webatmloc_request"))
      {
        this.inSession = Boolean.valueOf(false);
        loadURL("/locator/g.locations.do?disableBackLink=x");
        return;
      }
      if (localBundle.containsKey("privacy_policy_request"))
      {
        this.inSession = Boolean.valueOf(false);
        loadInitialURL("/privacyPolicy/begin.do?disableBackLink=x");
        return;
      }
    }
    while (!localBundle.containsKey("deposit"));
    Log.d("WFwebview", "in onCreate to go to deposit");
    this.inSession = Boolean.valueOf(true);
    depositCheck();
  }

  protected void onDestroy()
  {
    Log.d("WFwebview", "in onDestroy");
    if (this.webview != null)
    {
      Log.d("WFwebview", "  - webview is not null");
      CookieSyncManager.createInstance(this);
      CookieManager.getInstance().removeAllCookie();
      this.webview.clearCache(true);
      this.webview.clearHistory();
      this.webview.destroy();
    }
    while (true)
    {
      super.onDestroy();
      return;
      Log.d("WFwebview", "  - webview IS null");
    }
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 4) && (this.webviewCurtain.getVisibility() == 8) && (this.webview.canGoBack()))
    {
      this.webview.goBack();
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
      return false;
    case 2131099777:
      setResult(4, null);
      finish();
      return true;
    case 2131099778:
      if (this.wfApp.isGoogleApiAvailable())
      {
        launchLocSearch();
        return true;
      }
      this.webview.loadUrl(getString(2131165185) + "/accounts/g.locations.do");
      return true;
    case 2131099776:
      signOff();
      return true;
    case 2131099775:
    }
    depositCheck();
    return true;
  }

  protected void onPause()
  {
    clearAndPreventCache();
    clearWebViewDatabaseAndCookies(false);
    super.onPause();
  }

  public boolean onPrepareOptionsMenu(Menu paramMenu)
  {
    paramMenu.clear();
    if (this.inSession.booleanValue())
      if (this.wfApp.getMrdcEnabled().equals(Trinary.TRUE))
        getMenuInflater().inflate(2131361796, paramMenu);
    while (true)
    {
      return true;
      getMenuInflater().inflate(2131361797, paramMenu);
      continue;
      getMenuInflater().inflate(2131361795, paramMenu);
    }
  }

  public class CheckDepositJavaScriptInterface
  {
    private static final String TAG = "CheckDepositJavaScriptInterface";
    private final Context mContext;

    public CheckDepositJavaScriptInterface(Context arg2)
    {
      Object localObject;
      this.mContext = localObject;
    }

    public void cancelCheckDeposit(String paramString)
    {
      Log.d("CheckDepositJavaScriptInterface", "cancelCheckDeposit() - called from JavaScript");
      if (!WFwebview.this.validNonce(paramString))
      {
        Log.d("CheckDepositJavaScriptInterface", "cancelCheckDeposit() - nonce not valid");
        return;
      }
      WFwebview.this.runOnUiThread(new Runnable()
      {
        public void run()
        {
          WFwebview.this.loadURL("/deposit/depositCancel.action?");
        }
      });
    }

    public void communicationError(String paramString)
    {
      Log.d("CheckDepositJavaScriptInterface", "in communicationError()");
      if (!WFwebview.this.validNonce(paramString))
      {
        Log.d("CheckDepositJavaScriptInterface", "communicationError() - nonce not valid");
        return;
      }
      AlertDialog.Builder localBuilder = new AlertDialog.Builder(this.mContext);
      localBuilder.setMessage(WFwebview.this.getString(2131165218)).setCancelable(false).setPositiveButton(WFwebview.this.getString(2131165254), new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          WFwebview.this.wfApp.clearImageData();
          WFwebview.this.loadURL("/deposit/depositCancel.action?");
        }
      });
      localBuilder.create();
    }

    public void enableMrdc(String paramString)
    {
      try
      {
        JSONObject localJSONObject = (JSONObject)new JSONTokener(paramString).nextValue();
        String str = localJSONObject.getString("nonce");
        if (!WFwebview.this.validNonce(str))
        {
          Log.d("CheckDepositJavaScriptInterface", "enableMrdc() - nonce not valid");
          return;
        }
        boolean bool = Boolean.parseBoolean(localJSONObject.getString("customerEnabledMrcd"));
        WFwebview.this.wfApp.setMrdcEnabled(Trinary.parseTrinary(bool));
        return;
      }
      catch (JSONException localJSONException)
      {
      }
    }

    public void enterCheckDepositFlow(String paramString)
    {
      if (!WFwebview.this.validNonce(paramString))
        Log.d("CheckDepositJavaScriptInterface", "enterCheckDepositFlow() - nonce not valid");
    }

    public void retakePhotos()
    {
      WFwebview.this.wfApp.clearImageData();
    }

    public void submitDeposit(String paramString1, String paramString2, String paramString3)
      throws IllegalCharsetNameException, UnsupportedCharsetException, ClientProtocolException, IOException
    {
      Log.d("CheckDepositJavaScriptInterface", "in submitDeposit() ");
      if (!WFwebview.this.validNonce(paramString1))
      {
        Log.d("CheckDepositJavaScriptInterface", "submitDeposit() - nonce not valid");
        return;
      }
      WFwebview.this.submitDepositProgressDialog.show();
      new WFwebview.SubmitDepositThread(WFwebview.this, this.mContext, paramString2, paramString3).execute(new Void[0]);
    }

    public void takePicture(String paramString)
    {
      if (!WFwebview.this.validNonce(paramString))
      {
        Log.d("CheckDepositJavaScriptInterface", "takePicture() - nonce not valid");
        return;
      }
      Intent localIntent = new Intent(this.mContext, CapturePhoto.class);
      localIntent.putExtra("MRDC_FLOW_STATE", ImageMode.FRONT);
      WFwebview.this.startActivityForResult(localIntent, 1);
    }
  }

  public class CommonJavaScriptInterface
  {
    private static final String TAG = "CommonJavaScriptInterface";
    private final Context mContext;

    public CommonJavaScriptInterface(Context arg2)
    {
      Object localObject;
      this.mContext = localObject;
    }

    public void signOut(String paramString)
    {
      Log.d("CommonJavaScriptInterface", "CommonJavaScriptInterface.signOut() ");
      WFwebview.access$602(WFwebview.this, Boolean.valueOf(false));
      WFwebview.this.wfApp.clearCheckDepositData();
      WFwebview.this.wfApp.setNonce(null);
      WFwebview.this.runOnUiThread(new Runnable()
      {
        public void run()
        {
          WFwebview.this.clearAndPreventCache();
          WFwebview.this.clearWebViewDatabaseAndCookies(true);
        }
      });
    }

    public void signonError(String paramString)
    {
      Log.d("CommonJavaScriptInterface", "received notification of a signonError: " + paramString);
      WFwebview.this.returnWithError(this.mContext.getString(2131165278), paramString);
    }
  }

  private class SendPerfTimingsThread extends AsyncTask<Void, Void, Void>
  {
    Context aContext;
    long d1_front_image_start_send;
    long d2_front_image_json_response;
    long d3_rear_image_start_send;
    long d4_rear_image_json_response;
    long d5_data_start_send;
    long d6_data_json_response;

    public SendPerfTimingsThread(Context paramLong1, long arg3, long arg5, long arg7, long arg9, long arg11, long arg13)
    {
      this.aContext = paramLong1;
      this.d1_front_image_start_send = ???;
      this.d2_front_image_json_response = ???;
      this.d3_rear_image_start_send = ???;
      Object localObject1;
      this.d4_rear_image_json_response = localObject1;
      Object localObject2;
      this.d5_data_start_send = localObject2;
      Object localObject3;
      this.d6_data_json_response = localObject3;
    }

    // ERROR //
    protected Void doInBackground(Void[] paramArrayOfVoid)
    {
      // Byte code:
      //   0: aconst_null
      //   1: astore_2
      //   2: ldc 56
      //   4: new 58	java/lang/StringBuilder
      //   7: dup
      //   8: invokespecial 59	java/lang/StringBuilder:<init>	()V
      //   11: ldc 61
      //   13: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   16: aload_0
      //   17: getfield 27	com/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread:d1_front_image_start_send	J
      //   20: invokevirtual 68	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
      //   23: ldc 70
      //   25: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   28: aload_0
      //   29: getfield 29	com/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread:d2_front_image_json_response	J
      //   32: invokevirtual 68	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
      //   35: ldc 72
      //   37: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   40: aload_0
      //   41: getfield 31	com/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread:d3_rear_image_start_send	J
      //   44: invokevirtual 68	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
      //   47: ldc 74
      //   49: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   52: aload_0
      //   53: getfield 33	com/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread:d4_rear_image_json_response	J
      //   56: invokevirtual 68	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
      //   59: ldc 76
      //   61: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   64: aload_0
      //   65: getfield 35	com/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread:d5_data_start_send	J
      //   68: invokevirtual 68	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
      //   71: ldc 78
      //   73: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   76: aload_0
      //   77: getfield 37	com/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread:d6_data_json_response	J
      //   80: invokevirtual 68	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
      //   83: invokevirtual 82	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   86: invokestatic 88	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
      //   89: pop
      //   90: aload_0
      //   91: getfield 25	com/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread:aContext	Landroid/content/Context;
      //   94: invokestatic 94	android/webkit/CookieSyncManager:createInstance	(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;
      //   97: pop
      //   98: invokestatic 98	android/webkit/CookieSyncManager:getInstance	()Landroid/webkit/CookieSyncManager;
      //   101: invokevirtual 101	android/webkit/CookieSyncManager:sync	()V
      //   104: invokestatic 106	android/webkit/CookieManager:getInstance	()Landroid/webkit/CookieManager;
      //   107: aload_0
      //   108: getfield 25	com/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread:aContext	Landroid/content/Context;
      //   111: ldc 107
      //   113: invokevirtual 113	android/content/Context:getString	(I)Ljava/lang/String;
      //   116: invokevirtual 117	android/webkit/CookieManager:getCookie	(Ljava/lang/String;)Ljava/lang/String;
      //   119: astore 11
      //   121: ldc 56
      //   123: new 58	java/lang/StringBuilder
      //   126: dup
      //   127: invokespecial 59	java/lang/StringBuilder:<init>	()V
      //   130: ldc 119
      //   132: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   135: aload 11
      //   137: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   140: invokevirtual 82	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   143: invokestatic 88	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
      //   146: pop
      //   147: new 121	org/apache/http/impl/client/DefaultHttpClient
      //   150: dup
      //   151: invokespecial 122	org/apache/http/impl/client/DefaultHttpClient:<init>	()V
      //   154: astore 13
      //   156: aload 13
      //   158: invokevirtual 126	org/apache/http/impl/client/DefaultHttpClient:getParams	()Lorg/apache/http/params/HttpParams;
      //   161: astore 19
      //   163: aload 19
      //   165: ldc 128
      //   167: ldc2_w 129
      //   170: invokeinterface 136 4 0
      //   175: pop2
      //   176: aload 19
      //   178: ldc 138
      //   180: ldc2_w 129
      //   183: invokeinterface 136 4 0
      //   188: pop2
      //   189: aload 13
      //   191: aload 19
      //   193: invokevirtual 142	org/apache/http/impl/client/DefaultHttpClient:setParams	(Lorg/apache/http/params/HttpParams;)V
      //   196: new 144	org/apache/http/client/methods/HttpPost
      //   199: dup
      //   200: new 58	java/lang/StringBuilder
      //   203: dup
      //   204: invokespecial 59	java/lang/StringBuilder:<init>	()V
      //   207: aload_0
      //   208: getfield 25	com/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread:aContext	Landroid/content/Context;
      //   211: ldc 107
      //   213: invokevirtual 113	android/content/Context:getString	(I)Ljava/lang/String;
      //   216: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   219: ldc 146
      //   221: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   224: invokevirtual 82	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   227: invokespecial 149	org/apache/http/client/methods/HttpPost:<init>	(Ljava/lang/String;)V
      //   230: astore 24
      //   232: aload 24
      //   234: ldc 151
      //   236: aload 11
      //   238: invokevirtual 155	org/apache/http/client/methods/HttpPost:setHeader	(Ljava/lang/String;Ljava/lang/String;)V
      //   241: new 157	java/util/ArrayList
      //   244: dup
      //   245: bipush 6
      //   247: invokespecial 160	java/util/ArrayList:<init>	(I)V
      //   250: astore 25
      //   252: aload 25
      //   254: new 162	org/apache/http/message/BasicNameValuePair
      //   257: dup
      //   258: ldc 164
      //   260: new 58	java/lang/StringBuilder
      //   263: dup
      //   264: invokespecial 59	java/lang/StringBuilder:<init>	()V
      //   267: ldc 166
      //   269: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   272: aload_0
      //   273: getfield 29	com/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread:d2_front_image_json_response	J
      //   276: aload_0
      //   277: getfield 27	com/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread:d1_front_image_start_send	J
      //   280: lsub
      //   281: invokevirtual 68	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
      //   284: invokevirtual 82	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   287: invokespecial 168	org/apache/http/message/BasicNameValuePair:<init>	(Ljava/lang/String;Ljava/lang/String;)V
      //   290: invokeinterface 174 2 0
      //   295: pop
      //   296: aload 25
      //   298: new 162	org/apache/http/message/BasicNameValuePair
      //   301: dup
      //   302: ldc 176
      //   304: new 58	java/lang/StringBuilder
      //   307: dup
      //   308: invokespecial 59	java/lang/StringBuilder:<init>	()V
      //   311: ldc 166
      //   313: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   316: aload_0
      //   317: getfield 33	com/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread:d4_rear_image_json_response	J
      //   320: aload_0
      //   321: getfield 31	com/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread:d3_rear_image_start_send	J
      //   324: lsub
      //   325: invokevirtual 68	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
      //   328: invokevirtual 82	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   331: invokespecial 168	org/apache/http/message/BasicNameValuePair:<init>	(Ljava/lang/String;Ljava/lang/String;)V
      //   334: invokeinterface 174 2 0
      //   339: pop
      //   340: aload 25
      //   342: new 162	org/apache/http/message/BasicNameValuePair
      //   345: dup
      //   346: ldc 178
      //   348: new 58	java/lang/StringBuilder
      //   351: dup
      //   352: invokespecial 59	java/lang/StringBuilder:<init>	()V
      //   355: ldc 166
      //   357: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   360: aload_0
      //   361: getfield 37	com/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread:d6_data_json_response	J
      //   364: aload_0
      //   365: getfield 35	com/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread:d5_data_start_send	J
      //   368: lsub
      //   369: invokevirtual 68	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
      //   372: invokevirtual 82	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   375: invokespecial 168	org/apache/http/message/BasicNameValuePair:<init>	(Ljava/lang/String;Ljava/lang/String;)V
      //   378: invokeinterface 174 2 0
      //   383: pop
      //   384: aload 25
      //   386: new 162	org/apache/http/message/BasicNameValuePair
      //   389: dup
      //   390: ldc 180
      //   392: new 58	java/lang/StringBuilder
      //   395: dup
      //   396: invokespecial 59	java/lang/StringBuilder:<init>	()V
      //   399: ldc 166
      //   401: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   404: aload_0
      //   405: getfield 37	com/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread:d6_data_json_response	J
      //   408: aload_0
      //   409: getfield 27	com/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread:d1_front_image_start_send	J
      //   412: lsub
      //   413: invokevirtual 68	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
      //   416: invokevirtual 82	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   419: invokespecial 168	org/apache/http/message/BasicNameValuePair:<init>	(Ljava/lang/String;Ljava/lang/String;)V
      //   422: invokeinterface 174 2 0
      //   427: pop
      //   428: aload 24
      //   430: new 182	org/apache/http/client/entity/UrlEncodedFormEntity
      //   433: dup
      //   434: aload 25
      //   436: invokespecial 185	org/apache/http/client/entity/UrlEncodedFormEntity:<init>	(Ljava/util/List;)V
      //   439: invokevirtual 189	org/apache/http/client/methods/HttpPost:setEntity	(Lorg/apache/http/HttpEntity;)V
      //   442: aload 13
      //   444: aload 24
      //   446: invokevirtual 193	org/apache/http/impl/client/DefaultHttpClient:execute	(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
      //   449: invokeinterface 199 1 0
      //   454: astore 30
      //   456: ldc 56
      //   458: new 58	java/lang/StringBuilder
      //   461: dup
      //   462: invokespecial 59	java/lang/StringBuilder:<init>	()V
      //   465: ldc 201
      //   467: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   470: aload 30
      //   472: invokestatic 206	org/apache/http/util/EntityUtils:toString	(Lorg/apache/http/HttpEntity;)Ljava/lang/String;
      //   475: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   478: invokevirtual 82	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   481: invokestatic 88	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
      //   484: pop
      //   485: aload 13
      //   487: invokevirtual 210	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   490: invokeinterface 215 1 0
      //   495: aconst_null
      //   496: areturn
      //   497: astore 8
      //   499: aload_2
      //   500: invokevirtual 210	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   503: invokeinterface 215 1 0
      //   508: goto -13 -> 495
      //   511: astore 7
      //   513: aload_2
      //   514: invokevirtual 210	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   517: invokeinterface 215 1 0
      //   522: goto -27 -> 495
      //   525: astore 6
      //   527: aload_2
      //   528: invokevirtual 210	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   531: invokeinterface 215 1 0
      //   536: goto -41 -> 495
      //   539: astore 5
      //   541: aload_2
      //   542: invokevirtual 210	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   545: invokeinterface 215 1 0
      //   550: goto -55 -> 495
      //   553: astore 4
      //   555: aload_2
      //   556: invokevirtual 210	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   559: invokeinterface 215 1 0
      //   564: goto -69 -> 495
      //   567: astore_3
      //   568: aload_2
      //   569: invokevirtual 210	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   572: invokeinterface 215 1 0
      //   577: aload_3
      //   578: athrow
      //   579: astore_3
      //   580: aload 13
      //   582: astore_2
      //   583: goto -15 -> 568
      //   586: astore 18
      //   588: aload 13
      //   590: astore_2
      //   591: goto -36 -> 555
      //   594: astore 17
      //   596: aload 13
      //   598: astore_2
      //   599: goto -58 -> 541
      //   602: astore 16
      //   604: aload 13
      //   606: astore_2
      //   607: goto -80 -> 527
      //   610: astore 15
      //   612: aload 13
      //   614: astore_2
      //   615: goto -102 -> 513
      //   618: astore 14
      //   620: aload 13
      //   622: astore_2
      //   623: goto -124 -> 499
      //
      // Exception table:
      //   from	to	target	type
      //   2	156	497	java/nio/charset/IllegalCharsetNameException
      //   2	156	511	java/nio/charset/UnsupportedCharsetException
      //   2	156	525	java/io/UnsupportedEncodingException
      //   2	156	539	org/apache/http/client/ClientProtocolException
      //   2	156	553	java/io/IOException
      //   2	156	567	finally
      //   156	485	579	finally
      //   156	485	586	java/io/IOException
      //   156	485	594	org/apache/http/client/ClientProtocolException
      //   156	485	602	java/io/UnsupportedEncodingException
      //   156	485	610	java/nio/charset/UnsupportedCharsetException
      //   156	485	618	java/nio/charset/IllegalCharsetNameException
    }
  }

  private class SubmitDepositThread extends AsyncTask<Void, Void, Void>
  {
    Context aContext;
    String amount;
    String selectedAccount;

    public SubmitDepositThread(Context paramString1, String paramString2, String arg4)
    {
      this.aContext = paramString1;
      this.amount = paramString2;
      Object localObject;
      this.selectedAccount = localObject;
    }

    // ERROR //
    protected Void doInBackground(Void[] paramArrayOfVoid)
    {
      // Byte code:
      //   0: aload_0
      //   1: getfield 16	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   4: invokestatic 50	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   7: invokestatic 56	java/lang/System:currentTimeMillis	()J
      //   10: invokevirtual 62	com/wf/wellsfargomobile/WFApp:setPerfD5DataStartSend	(J)V
      //   13: iconst_1
      //   14: istore_2
      //   15: aconst_null
      //   16: astore_3
      //   17: aload_0
      //   18: getfield 21	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:aContext	Landroid/content/Context;
      //   21: invokestatic 68	android/webkit/CookieSyncManager:createInstance	(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;
      //   24: pop
      //   25: invokestatic 72	android/webkit/CookieSyncManager:getInstance	()Landroid/webkit/CookieSyncManager;
      //   28: invokevirtual 75	android/webkit/CookieSyncManager:sync	()V
      //   31: invokestatic 80	android/webkit/CookieManager:getInstance	()Landroid/webkit/CookieManager;
      //   34: aload_0
      //   35: getfield 21	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:aContext	Landroid/content/Context;
      //   38: ldc 81
      //   40: invokevirtual 87	android/content/Context:getString	(I)Ljava/lang/String;
      //   43: invokevirtual 91	android/webkit/CookieManager:getCookie	(Ljava/lang/String;)Ljava/lang/String;
      //   46: astore 20
      //   48: ldc 93
      //   50: new 95	java/lang/StringBuilder
      //   53: dup
      //   54: invokespecial 96	java/lang/StringBuilder:<init>	()V
      //   57: ldc 98
      //   59: invokevirtual 102	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   62: aload 20
      //   64: invokevirtual 102	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   67: invokevirtual 106	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   70: invokestatic 112	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
      //   73: pop
      //   74: new 114	org/apache/http/impl/client/DefaultHttpClient
      //   77: dup
      //   78: invokespecial 115	org/apache/http/impl/client/DefaultHttpClient:<init>	()V
      //   81: astore 22
      //   83: aload 22
      //   85: invokevirtual 119	org/apache/http/impl/client/DefaultHttpClient:getParams	()Lorg/apache/http/params/HttpParams;
      //   88: astore 28
      //   90: aload 28
      //   92: ldc 121
      //   94: ldc2_w 122
      //   97: invokeinterface 129 4 0
      //   102: pop2
      //   103: aload 28
      //   105: ldc 131
      //   107: ldc2_w 122
      //   110: invokeinterface 129 4 0
      //   115: pop2
      //   116: aload 22
      //   118: aload 28
      //   120: invokevirtual 135	org/apache/http/impl/client/DefaultHttpClient:setParams	(Lorg/apache/http/params/HttpParams;)V
      //   123: new 137	org/apache/http/client/methods/HttpPost
      //   126: dup
      //   127: new 95	java/lang/StringBuilder
      //   130: dup
      //   131: invokespecial 96	java/lang/StringBuilder:<init>	()V
      //   134: aload_0
      //   135: getfield 21	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:aContext	Landroid/content/Context;
      //   138: ldc 81
      //   140: invokevirtual 87	android/content/Context:getString	(I)Ljava/lang/String;
      //   143: invokevirtual 102	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   146: ldc 139
      //   148: invokevirtual 102	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   151: invokevirtual 106	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   154: invokespecial 142	org/apache/http/client/methods/HttpPost:<init>	(Ljava/lang/String;)V
      //   157: astore 33
      //   159: aload 33
      //   161: ldc 144
      //   163: aload 20
      //   165: invokevirtual 148	org/apache/http/client/methods/HttpPost:setHeader	(Ljava/lang/String;Ljava/lang/String;)V
      //   168: new 150	org/apache/http/entity/mime/MultipartEntity
      //   171: dup
      //   172: invokespecial 151	org/apache/http/entity/mime/MultipartEntity:<init>	()V
      //   175: astore 34
      //   177: aload 34
      //   179: ldc 153
      //   181: new 155	org/apache/http/entity/mime/content/StringBody
      //   184: dup
      //   185: aload_0
      //   186: getfield 16	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   189: invokestatic 50	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   192: invokevirtual 158	com/wf/wellsfargomobile/WFApp:getFrontImageToken	()Ljava/lang/String;
      //   195: ldc 160
      //   197: invokestatic 166	java/nio/charset/Charset:forName	(Ljava/lang/String;)Ljava/nio/charset/Charset;
      //   200: invokespecial 169	org/apache/http/entity/mime/content/StringBody:<init>	(Ljava/lang/String;Ljava/nio/charset/Charset;)V
      //   203: invokevirtual 173	org/apache/http/entity/mime/MultipartEntity:addPart	(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V
      //   206: aload 34
      //   208: ldc 175
      //   210: new 155	org/apache/http/entity/mime/content/StringBody
      //   213: dup
      //   214: aload_0
      //   215: getfield 16	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   218: invokestatic 50	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   221: invokevirtual 178	com/wf/wellsfargomobile/WFApp:getRearImageToken	()Ljava/lang/String;
      //   224: ldc 160
      //   226: invokestatic 166	java/nio/charset/Charset:forName	(Ljava/lang/String;)Ljava/nio/charset/Charset;
      //   229: invokespecial 169	org/apache/http/entity/mime/content/StringBody:<init>	(Ljava/lang/String;Ljava/nio/charset/Charset;)V
      //   232: invokevirtual 173	org/apache/http/entity/mime/MultipartEntity:addPart	(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V
      //   235: aload 34
      //   237: ldc 179
      //   239: new 155	org/apache/http/entity/mime/content/StringBody
      //   242: dup
      //   243: aload_0
      //   244: getfield 23	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:amount	Ljava/lang/String;
      //   247: ldc 160
      //   249: invokestatic 166	java/nio/charset/Charset:forName	(Ljava/lang/String;)Ljava/nio/charset/Charset;
      //   252: invokespecial 169	org/apache/http/entity/mime/content/StringBody:<init>	(Ljava/lang/String;Ljava/nio/charset/Charset;)V
      //   255: invokevirtual 173	org/apache/http/entity/mime/MultipartEntity:addPart	(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V
      //   258: aload 34
      //   260: ldc 181
      //   262: new 155	org/apache/http/entity/mime/content/StringBody
      //   265: dup
      //   266: aload_0
      //   267: getfield 25	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:selectedAccount	Ljava/lang/String;
      //   270: ldc 160
      //   272: invokestatic 166	java/nio/charset/Charset:forName	(Ljava/lang/String;)Ljava/nio/charset/Charset;
      //   275: invokespecial 169	org/apache/http/entity/mime/content/StringBody:<init>	(Ljava/lang/String;Ljava/nio/charset/Charset;)V
      //   278: invokevirtual 173	org/apache/http/entity/mime/MultipartEntity:addPart	(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V
      //   281: aload 34
      //   283: ldc 183
      //   285: new 155	org/apache/http/entity/mime/content/StringBody
      //   288: dup
      //   289: aload_0
      //   290: getfield 21	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:aContext	Landroid/content/Context;
      //   293: ldc 184
      //   295: invokevirtual 87	android/content/Context:getString	(I)Ljava/lang/String;
      //   298: ldc 160
      //   300: invokestatic 166	java/nio/charset/Charset:forName	(Ljava/lang/String;)Ljava/nio/charset/Charset;
      //   303: invokespecial 169	org/apache/http/entity/mime/content/StringBody:<init>	(Ljava/lang/String;Ljava/nio/charset/Charset;)V
      //   306: invokevirtual 173	org/apache/http/entity/mime/MultipartEntity:addPart	(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V
      //   309: aload 33
      //   311: aload 34
      //   313: invokevirtual 188	org/apache/http/client/methods/HttpPost:setEntity	(Lorg/apache/http/HttpEntity;)V
      //   316: aload 22
      //   318: aload 33
      //   320: invokevirtual 192	org/apache/http/impl/client/DefaultHttpClient:execute	(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
      //   323: astore 35
      //   325: aload_0
      //   326: getfield 16	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   329: invokestatic 50	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   332: invokestatic 56	java/lang/System:currentTimeMillis	()J
      //   335: invokevirtual 195	com/wf/wellsfargomobile/WFApp:setPerfD6DataJsonResponse	(J)V
      //   338: aload 35
      //   340: invokeinterface 201 1 0
      //   345: invokestatic 206	org/apache/http/util/EntityUtils:toString	(Lorg/apache/http/HttpEntity;)Ljava/lang/String;
      //   348: astore 36
      //   350: ldc 93
      //   352: new 95	java/lang/StringBuilder
      //   355: dup
      //   356: invokespecial 96	java/lang/StringBuilder:<init>	()V
      //   359: ldc 208
      //   361: invokevirtual 102	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   364: aload 36
      //   366: invokevirtual 102	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   369: invokevirtual 106	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   372: invokestatic 112	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
      //   375: pop
      //   376: aload 36
      //   378: ldc 210
      //   380: ldc 212
      //   382: invokevirtual 218	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
      //   385: ldc 220
      //   387: ldc 212
      //   389: invokevirtual 218	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
      //   392: astore 38
      //   394: ldc 93
      //   396: new 95	java/lang/StringBuilder
      //   399: dup
      //   400: invokespecial 96	java/lang/StringBuilder:<init>	()V
      //   403: ldc 208
      //   405: invokevirtual 102	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   408: aload 38
      //   410: invokevirtual 102	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   413: invokevirtual 106	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   416: invokestatic 112	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
      //   419: pop
      //   420: ldc 222
      //   422: new 224	org/json/JSONObject
      //   425: dup
      //   426: aload 38
      //   428: invokespecial 225	org/json/JSONObject:<init>	(Ljava/lang/String;)V
      //   431: ldc 227
      //   433: invokevirtual 229	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
      //   436: invokevirtual 233	java/lang/String:equals	(Ljava/lang/Object;)Z
      //   439: istore 42
      //   441: iload 42
      //   443: ifeq +5 -> 448
      //   446: iconst_0
      //   447: istore_2
      //   448: aload 22
      //   450: invokevirtual 237	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   453: invokeinterface 242 1 0
      //   458: aload_0
      //   459: getfield 16	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   462: invokestatic 50	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   465: invokevirtual 245	com/wf/wellsfargomobile/WFApp:clearCheckDepositData	()V
      //   468: aload_0
      //   469: getfield 16	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   472: invokestatic 249	com/wf/wellsfargomobile/WFwebview:access$1700	(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;
      //   475: invokevirtual 255	android/app/Dialog:isShowing	()Z
      //   478: ifeq +456 -> 934
      //   481: aload_0
      //   482: getfield 16	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   485: invokestatic 249	com/wf/wellsfargomobile/WFwebview:access$1700	(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;
      //   488: invokevirtual 258	android/app/Dialog:dismiss	()V
      //   491: iload_2
      //   492: ifeq +368 -> 860
      //   495: aload_0
      //   496: getfield 16	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   499: astore 9
      //   501: new 260	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread$1
      //   504: dup
      //   505: aload_0
      //   506: invokespecial 263	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread$1:<init>	(Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;)V
      //   509: astore 10
      //   511: aload 9
      //   513: aload 10
      //   515: invokevirtual 267	com/wf/wellsfargomobile/WFwebview:runOnUiThread	(Ljava/lang/Runnable;)V
      //   518: aconst_null
      //   519: areturn
      //   520: astore 40
      //   522: ldc 93
      //   524: ldc_w 269
      //   527: aload 40
      //   529: invokestatic 272	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   532: pop
      //   533: iconst_0
      //   534: istore_2
      //   535: goto -87 -> 448
      //   538: astore 17
      //   540: aload_3
      //   541: invokevirtual 237	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   544: invokeinterface 242 1 0
      //   549: aload_0
      //   550: getfield 16	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   553: invokestatic 50	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   556: invokevirtual 245	com/wf/wellsfargomobile/WFApp:clearCheckDepositData	()V
      //   559: aload_0
      //   560: getfield 16	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   563: invokestatic 249	com/wf/wellsfargomobile/WFwebview:access$1700	(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;
      //   566: invokevirtual 255	android/app/Dialog:isShowing	()Z
      //   569: istore 18
      //   571: iconst_0
      //   572: istore_2
      //   573: iload 18
      //   575: ifeq -84 -> 491
      //   578: aload_0
      //   579: getfield 16	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   582: invokestatic 249	com/wf/wellsfargomobile/WFwebview:access$1700	(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;
      //   585: invokevirtual 258	android/app/Dialog:dismiss	()V
      //   588: iconst_0
      //   589: istore_2
      //   590: goto -99 -> 491
      //   593: astore 15
      //   595: aload_3
      //   596: invokevirtual 237	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   599: invokeinterface 242 1 0
      //   604: aload_0
      //   605: getfield 16	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   608: invokestatic 50	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   611: invokevirtual 245	com/wf/wellsfargomobile/WFApp:clearCheckDepositData	()V
      //   614: aload_0
      //   615: getfield 16	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   618: invokestatic 249	com/wf/wellsfargomobile/WFwebview:access$1700	(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;
      //   621: invokevirtual 255	android/app/Dialog:isShowing	()Z
      //   624: istore 16
      //   626: iconst_0
      //   627: istore_2
      //   628: iload 16
      //   630: ifeq -139 -> 491
      //   633: aload_0
      //   634: getfield 16	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   637: invokestatic 249	com/wf/wellsfargomobile/WFwebview:access$1700	(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;
      //   640: invokevirtual 258	android/app/Dialog:dismiss	()V
      //   643: iconst_0
      //   644: istore_2
      //   645: goto -154 -> 491
      //   648: astore 13
      //   650: aload_3
      //   651: invokevirtual 237	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   654: invokeinterface 242 1 0
      //   659: aload_0
      //   660: getfield 16	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   663: invokestatic 50	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   666: invokevirtual 245	com/wf/wellsfargomobile/WFApp:clearCheckDepositData	()V
      //   669: aload_0
      //   670: getfield 16	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   673: invokestatic 249	com/wf/wellsfargomobile/WFwebview:access$1700	(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;
      //   676: invokevirtual 255	android/app/Dialog:isShowing	()Z
      //   679: istore 14
      //   681: iconst_0
      //   682: istore_2
      //   683: iload 14
      //   685: ifeq -194 -> 491
      //   688: aload_0
      //   689: getfield 16	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   692: invokestatic 249	com/wf/wellsfargomobile/WFwebview:access$1700	(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;
      //   695: invokevirtual 258	android/app/Dialog:dismiss	()V
      //   698: iconst_0
      //   699: istore_2
      //   700: goto -209 -> 491
      //   703: astore 11
      //   705: aload_3
      //   706: invokevirtual 237	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   709: invokeinterface 242 1 0
      //   714: aload_0
      //   715: getfield 16	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   718: invokestatic 50	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   721: invokevirtual 245	com/wf/wellsfargomobile/WFApp:clearCheckDepositData	()V
      //   724: aload_0
      //   725: getfield 16	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   728: invokestatic 249	com/wf/wellsfargomobile/WFwebview:access$1700	(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;
      //   731: invokevirtual 255	android/app/Dialog:isShowing	()Z
      //   734: istore 12
      //   736: iconst_0
      //   737: istore_2
      //   738: iload 12
      //   740: ifeq -249 -> 491
      //   743: aload_0
      //   744: getfield 16	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   747: invokestatic 249	com/wf/wellsfargomobile/WFwebview:access$1700	(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;
      //   750: invokevirtual 258	android/app/Dialog:dismiss	()V
      //   753: iconst_0
      //   754: istore_2
      //   755: goto -264 -> 491
      //   758: astore 5
      //   760: aload_3
      //   761: invokevirtual 237	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   764: invokeinterface 242 1 0
      //   769: aload_0
      //   770: getfield 16	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   773: invokestatic 50	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   776: invokevirtual 245	com/wf/wellsfargomobile/WFApp:clearCheckDepositData	()V
      //   779: aload_0
      //   780: getfield 16	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   783: invokestatic 249	com/wf/wellsfargomobile/WFwebview:access$1700	(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;
      //   786: invokevirtual 255	android/app/Dialog:isShowing	()Z
      //   789: istore 6
      //   791: iconst_0
      //   792: istore_2
      //   793: iload 6
      //   795: ifeq -304 -> 491
      //   798: aload_0
      //   799: getfield 16	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   802: invokestatic 249	com/wf/wellsfargomobile/WFwebview:access$1700	(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;
      //   805: invokevirtual 258	android/app/Dialog:dismiss	()V
      //   808: iconst_0
      //   809: istore_2
      //   810: goto -319 -> 491
      //   813: astore 4
      //   815: aload_3
      //   816: invokevirtual 237	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   819: invokeinterface 242 1 0
      //   824: aload_0
      //   825: getfield 16	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   828: invokestatic 50	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   831: invokevirtual 245	com/wf/wellsfargomobile/WFApp:clearCheckDepositData	()V
      //   834: aload_0
      //   835: getfield 16	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   838: invokestatic 249	com/wf/wellsfargomobile/WFwebview:access$1700	(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;
      //   841: invokevirtual 255	android/app/Dialog:isShowing	()Z
      //   844: ifeq +13 -> 857
      //   847: aload_0
      //   848: getfield 16	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   851: invokestatic 249	com/wf/wellsfargomobile/WFwebview:access$1700	(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;
      //   854: invokevirtual 258	android/app/Dialog:dismiss	()V
      //   857: aload 4
      //   859: athrow
      //   860: aload_0
      //   861: getfield 16	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   864: astore 7
      //   866: new 274	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread$2
      //   869: dup
      //   870: aload_0
      //   871: invokespecial 275	com/wf/wellsfargomobile/WFwebview$SubmitDepositThread$2:<init>	(Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;)V
      //   874: astore 8
      //   876: aload 7
      //   878: aload 8
      //   880: invokevirtual 267	com/wf/wellsfargomobile/WFwebview:runOnUiThread	(Ljava/lang/Runnable;)V
      //   883: goto -365 -> 518
      //   886: astore 4
      //   888: aload 22
      //   890: astore_3
      //   891: goto -76 -> 815
      //   894: astore 27
      //   896: aload 22
      //   898: astore_3
      //   899: goto -139 -> 760
      //   902: astore 26
      //   904: aload 22
      //   906: astore_3
      //   907: goto -202 -> 705
      //   910: astore 25
      //   912: aload 22
      //   914: astore_3
      //   915: goto -265 -> 650
      //   918: astore 24
      //   920: aload 22
      //   922: astore_3
      //   923: goto -328 -> 595
      //   926: astore 23
      //   928: aload 22
      //   930: astore_3
      //   931: goto -391 -> 540
      //   934: goto -443 -> 491
      //
      // Exception table:
      //   from	to	target	type
      //   420	441	520	org/json/JSONException
      //   17	83	538	java/nio/charset/IllegalCharsetNameException
      //   17	83	593	java/nio/charset/UnsupportedCharsetException
      //   17	83	648	java/io/UnsupportedEncodingException
      //   17	83	703	org/apache/http/client/ClientProtocolException
      //   17	83	758	java/io/IOException
      //   17	83	813	finally
      //   83	420	886	finally
      //   420	441	886	finally
      //   522	533	886	finally
      //   83	420	894	java/io/IOException
      //   420	441	894	java/io/IOException
      //   522	533	894	java/io/IOException
      //   83	420	902	org/apache/http/client/ClientProtocolException
      //   420	441	902	org/apache/http/client/ClientProtocolException
      //   522	533	902	org/apache/http/client/ClientProtocolException
      //   83	420	910	java/io/UnsupportedEncodingException
      //   420	441	910	java/io/UnsupportedEncodingException
      //   522	533	910	java/io/UnsupportedEncodingException
      //   83	420	918	java/nio/charset/UnsupportedCharsetException
      //   420	441	918	java/nio/charset/UnsupportedCharsetException
      //   522	533	918	java/nio/charset/UnsupportedCharsetException
      //   83	420	926	java/nio/charset/IllegalCharsetNameException
      //   420	441	926	java/nio/charset/IllegalCharsetNameException
      //   522	533	926	java/nio/charset/IllegalCharsetNameException
    }
  }

  private class SubmitRearImageThread extends AsyncTask<Void, Void, Boolean>
  {
    private static final String TAG = "SubmitRearImageThread";
    private Context aContext;

    public SubmitRearImageThread(Context arg2)
    {
      Object localObject;
      this.aContext = localObject;
    }

    // ERROR //
    protected Boolean doInBackground(Void[] paramArrayOfVoid)
    {
      // Byte code:
      //   0: aload_0
      //   1: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   4: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   7: invokestatic 50	java/lang/System:currentTimeMillis	()J
      //   10: invokevirtual 56	com/wf/wellsfargomobile/WFApp:setPerfD3RearImageStartSend	(J)V
      //   13: iconst_1
      //   14: istore_2
      //   15: aconst_null
      //   16: astore_3
      //   17: aload_0
      //   18: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   21: invokevirtual 60	com/wf/wellsfargomobile/WFwebview:getApplication	()Landroid/app/Application;
      //   24: checkcast 52	com/wf/wellsfargomobile/WFApp
      //   27: invokevirtual 64	com/wf/wellsfargomobile/WFApp:getRearCheckImage	()[B
      //   30: astore 17
      //   32: aload 17
      //   34: ifnonnull +114 -> 148
      //   37: aload_0
      //   38: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   41: invokestatic 68	com/wf/wellsfargomobile/WFwebview:access$1400	(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;
      //   44: invokevirtual 74	android/app/Dialog:isShowing	()Z
      //   47: ifeq +13 -> 60
      //   50: aload_0
      //   51: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   54: invokestatic 68	com/wf/wellsfargomobile/WFwebview:access$1400	(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;
      //   57: invokevirtual 77	android/app/Dialog:dismiss	()V
      //   60: ldc 9
      //   62: new 79	java/lang/StringBuilder
      //   65: dup
      //   66: invokespecial 80	java/lang/StringBuilder:<init>	()V
      //   69: ldc 82
      //   71: invokevirtual 86	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   74: aload 17
      //   76: invokevirtual 89	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   79: invokevirtual 93	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   82: invokestatic 99	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
      //   85: pop
      //   86: iconst_0
      //   87: invokestatic 105	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
      //   90: astore 19
      //   92: iconst_0
      //   93: ifeq +19 -> 112
      //   96: aconst_null
      //   97: invokevirtual 111	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   100: ifnull +12 -> 112
      //   103: aconst_null
      //   104: invokevirtual 111	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   107: invokeinterface 116 1 0
      //   112: aload_0
      //   113: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   116: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   119: iconst_1
      //   120: invokevirtual 120	com/wf/wellsfargomobile/WFApp:setRearImageUploadProcessComplete	(Z)V
      //   123: aload_0
      //   124: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   127: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   130: aconst_null
      //   131: invokevirtual 124	com/wf/wellsfargomobile/WFApp:setRearCheckImage	([B)V
      //   134: aload_0
      //   135: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   138: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   141: aconst_null
      //   142: invokevirtual 128	com/wf/wellsfargomobile/WFApp:setRearCheckPreviewImage	(Landroid/graphics/Bitmap;)V
      //   145: aload 19
      //   147: areturn
      //   148: aload_0
      //   149: getfield 22	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:aContext	Landroid/content/Context;
      //   152: invokestatic 134	android/webkit/CookieSyncManager:createInstance	(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;
      //   155: pop
      //   156: invokestatic 138	android/webkit/CookieSyncManager:getInstance	()Landroid/webkit/CookieSyncManager;
      //   159: invokevirtual 141	android/webkit/CookieSyncManager:sync	()V
      //   162: invokestatic 146	android/webkit/CookieManager:getInstance	()Landroid/webkit/CookieManager;
      //   165: aload_0
      //   166: getfield 22	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:aContext	Landroid/content/Context;
      //   169: ldc 147
      //   171: invokevirtual 153	android/content/Context:getString	(I)Ljava/lang/String;
      //   174: invokevirtual 157	android/webkit/CookieManager:getCookie	(Ljava/lang/String;)Ljava/lang/String;
      //   177: astore 21
      //   179: ldc 9
      //   181: new 79	java/lang/StringBuilder
      //   184: dup
      //   185: invokespecial 80	java/lang/StringBuilder:<init>	()V
      //   188: ldc 159
      //   190: invokevirtual 86	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   193: aload 21
      //   195: invokevirtual 86	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   198: invokevirtual 93	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   201: invokestatic 99	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
      //   204: pop
      //   205: new 107	org/apache/http/impl/client/DefaultHttpClient
      //   208: dup
      //   209: invokespecial 160	org/apache/http/impl/client/DefaultHttpClient:<init>	()V
      //   212: astore 23
      //   214: aload 23
      //   216: invokevirtual 164	org/apache/http/impl/client/DefaultHttpClient:getParams	()Lorg/apache/http/params/HttpParams;
      //   219: astore 29
      //   221: aload 29
      //   223: ldc 166
      //   225: ldc2_w 167
      //   228: invokeinterface 174 4 0
      //   233: pop2
      //   234: aload 29
      //   236: ldc 176
      //   238: ldc2_w 167
      //   241: invokeinterface 174 4 0
      //   246: pop2
      //   247: aload 23
      //   249: aload 29
      //   251: invokevirtual 180	org/apache/http/impl/client/DefaultHttpClient:setParams	(Lorg/apache/http/params/HttpParams;)V
      //   254: new 182	org/apache/http/client/methods/HttpPost
      //   257: dup
      //   258: new 79	java/lang/StringBuilder
      //   261: dup
      //   262: invokespecial 80	java/lang/StringBuilder:<init>	()V
      //   265: aload_0
      //   266: getfield 22	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:aContext	Landroid/content/Context;
      //   269: ldc 147
      //   271: invokevirtual 153	android/content/Context:getString	(I)Ljava/lang/String;
      //   274: invokevirtual 86	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   277: ldc 184
      //   279: invokevirtual 86	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   282: invokevirtual 93	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   285: invokespecial 187	org/apache/http/client/methods/HttpPost:<init>	(Ljava/lang/String;)V
      //   288: astore 34
      //   290: aload 34
      //   292: ldc 189
      //   294: aload 21
      //   296: invokevirtual 193	org/apache/http/client/methods/HttpPost:setHeader	(Ljava/lang/String;Ljava/lang/String;)V
      //   299: ldc 9
      //   301: new 79	java/lang/StringBuilder
      //   304: dup
      //   305: invokespecial 80	java/lang/StringBuilder:<init>	()V
      //   308: ldc 195
      //   310: invokevirtual 86	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   313: aload 17
      //   315: arraylength
      //   316: invokevirtual 198	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
      //   319: ldc 200
      //   321: invokevirtual 86	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   324: invokevirtual 93	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   327: invokestatic 99	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
      //   330: pop
      //   331: new 202	org/apache/http/entity/mime/MultipartEntity
      //   334: dup
      //   335: invokespecial 203	org/apache/http/entity/mime/MultipartEntity:<init>	()V
      //   338: astore 36
      //   340: new 205	org/apache/http/entity/mime/content/ByteArrayBody
      //   343: dup
      //   344: aload 17
      //   346: ldc 207
      //   348: ldc 209
      //   350: invokespecial 212	org/apache/http/entity/mime/content/ByteArrayBody:<init>	([BLjava/lang/String;Ljava/lang/String;)V
      //   353: astore 37
      //   355: aload 36
      //   357: ldc 214
      //   359: aload 37
      //   361: invokevirtual 218	org/apache/http/entity/mime/MultipartEntity:addPart	(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V
      //   364: aload 36
      //   366: ldc 220
      //   368: new 222	org/apache/http/entity/mime/content/StringBody
      //   371: dup
      //   372: aload_0
      //   373: getfield 22	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:aContext	Landroid/content/Context;
      //   376: ldc 223
      //   378: invokevirtual 153	android/content/Context:getString	(I)Ljava/lang/String;
      //   381: ldc 225
      //   383: invokestatic 231	java/nio/charset/Charset:forName	(Ljava/lang/String;)Ljava/nio/charset/Charset;
      //   386: invokespecial 234	org/apache/http/entity/mime/content/StringBody:<init>	(Ljava/lang/String;Ljava/nio/charset/Charset;)V
      //   389: invokevirtual 218	org/apache/http/entity/mime/MultipartEntity:addPart	(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V
      //   392: aload 34
      //   394: aload 36
      //   396: invokevirtual 238	org/apache/http/client/methods/HttpPost:setEntity	(Lorg/apache/http/HttpEntity;)V
      //   399: aload 23
      //   401: aload 34
      //   403: invokevirtual 242	org/apache/http/impl/client/DefaultHttpClient:execute	(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
      //   406: astore 38
      //   408: aload_0
      //   409: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   412: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   415: invokestatic 50	java/lang/System:currentTimeMillis	()J
      //   418: invokevirtual 245	com/wf/wellsfargomobile/WFApp:setPerfD4RearImageJsonResponse	(J)V
      //   421: aload_0
      //   422: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   425: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   428: iconst_1
      //   429: invokevirtual 120	com/wf/wellsfargomobile/WFApp:setRearImageUploadProcessComplete	(Z)V
      //   432: aload 38
      //   434: invokeinterface 251 1 0
      //   439: invokestatic 256	org/apache/http/util/EntityUtils:toString	(Lorg/apache/http/HttpEntity;)Ljava/lang/String;
      //   442: astore 39
      //   444: ldc 9
      //   446: new 79	java/lang/StringBuilder
      //   449: dup
      //   450: invokespecial 80	java/lang/StringBuilder:<init>	()V
      //   453: ldc_w 258
      //   456: invokevirtual 86	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   459: aload 39
      //   461: invokevirtual 86	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   464: invokevirtual 93	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   467: invokestatic 99	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
      //   470: pop
      //   471: aload 39
      //   473: ldc_w 260
      //   476: ldc_w 262
      //   479: invokevirtual 268	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
      //   482: ldc_w 270
      //   485: ldc_w 262
      //   488: invokevirtual 268	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
      //   491: astore 41
      //   493: ldc 9
      //   495: new 79	java/lang/StringBuilder
      //   498: dup
      //   499: invokespecial 80	java/lang/StringBuilder:<init>	()V
      //   502: ldc_w 258
      //   505: invokevirtual 86	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   508: aload 41
      //   510: invokevirtual 86	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   513: invokevirtual 93	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   516: invokestatic 99	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
      //   519: pop
      //   520: new 272	org/json/JSONObject
      //   523: dup
      //   524: aload 41
      //   526: invokespecial 273	org/json/JSONObject:<init>	(Ljava/lang/String;)V
      //   529: astore 43
      //   531: ldc_w 275
      //   534: aload 43
      //   536: ldc_w 277
      //   539: invokevirtual 279	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
      //   542: invokevirtual 283	java/lang/String:equals	(Ljava/lang/Object;)Z
      //   545: istore 46
      //   547: iload 46
      //   549: ifeq +108 -> 657
      //   552: iconst_0
      //   553: istore_2
      //   554: aload 23
      //   556: ifnull +21 -> 577
      //   559: aload 23
      //   561: invokevirtual 111	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   564: ifnull +13 -> 577
      //   567: aload 23
      //   569: invokevirtual 111	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   572: invokeinterface 116 1 0
      //   577: aload_0
      //   578: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   581: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   584: iconst_1
      //   585: invokevirtual 120	com/wf/wellsfargomobile/WFApp:setRearImageUploadProcessComplete	(Z)V
      //   588: aload_0
      //   589: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   592: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   595: aconst_null
      //   596: invokevirtual 124	com/wf/wellsfargomobile/WFApp:setRearCheckImage	([B)V
      //   599: aload_0
      //   600: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   603: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   606: aconst_null
      //   607: invokevirtual 128	com/wf/wellsfargomobile/WFApp:setRearCheckPreviewImage	(Landroid/graphics/Bitmap;)V
      //   610: iconst_0
      //   611: istore 6
      //   613: iconst_1
      //   614: istore 7
      //   616: iconst_0
      //   617: istore 8
      //   619: iload 7
      //   621: ifeq +526 -> 1147
      //   624: iload 8
      //   626: bipush 120
      //   628: if_icmpge +519 -> 1147
      //   631: iload_2
      //   632: ifeq +426 -> 1058
      //   635: aload_0
      //   636: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   639: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   642: invokevirtual 286	com/wf/wellsfargomobile/WFApp:getFrontImageToken	()Ljava/lang/String;
      //   645: ifnull +413 -> 1058
      //   648: iconst_1
      //   649: istore 6
      //   651: iconst_0
      //   652: istore 7
      //   654: goto -35 -> 619
      //   657: aload 43
      //   659: ldc_w 288
      //   662: invokevirtual 279	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
      //   665: astore 47
      //   667: aload_0
      //   668: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   671: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   674: aload 47
      //   676: invokevirtual 291	com/wf/wellsfargomobile/WFApp:setRearImageToken	(Ljava/lang/String;)V
      //   679: goto -125 -> 554
      //   682: astore 44
      //   684: ldc 9
      //   686: ldc_w 293
      //   689: aload 44
      //   691: invokestatic 296	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   694: pop
      //   695: iconst_0
      //   696: istore_2
      //   697: goto -143 -> 554
      //   700: astore 16
      //   702: aload_3
      //   703: ifnull +19 -> 722
      //   706: aload_3
      //   707: invokevirtual 111	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   710: ifnull +12 -> 722
      //   713: aload_3
      //   714: invokevirtual 111	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   717: invokeinterface 116 1 0
      //   722: aload_0
      //   723: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   726: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   729: iconst_1
      //   730: invokevirtual 120	com/wf/wellsfargomobile/WFApp:setRearImageUploadProcessComplete	(Z)V
      //   733: aload_0
      //   734: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   737: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   740: aconst_null
      //   741: invokevirtual 124	com/wf/wellsfargomobile/WFApp:setRearCheckImage	([B)V
      //   744: aload_0
      //   745: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   748: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   751: aconst_null
      //   752: invokevirtual 128	com/wf/wellsfargomobile/WFApp:setRearCheckPreviewImage	(Landroid/graphics/Bitmap;)V
      //   755: iconst_0
      //   756: istore_2
      //   757: goto -147 -> 610
      //   760: astore 15
      //   762: aload_3
      //   763: ifnull +19 -> 782
      //   766: aload_3
      //   767: invokevirtual 111	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   770: ifnull +12 -> 782
      //   773: aload_3
      //   774: invokevirtual 111	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   777: invokeinterface 116 1 0
      //   782: aload_0
      //   783: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   786: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   789: iconst_1
      //   790: invokevirtual 120	com/wf/wellsfargomobile/WFApp:setRearImageUploadProcessComplete	(Z)V
      //   793: aload_0
      //   794: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   797: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   800: aconst_null
      //   801: invokevirtual 124	com/wf/wellsfargomobile/WFApp:setRearCheckImage	([B)V
      //   804: aload_0
      //   805: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   808: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   811: aconst_null
      //   812: invokevirtual 128	com/wf/wellsfargomobile/WFApp:setRearCheckPreviewImage	(Landroid/graphics/Bitmap;)V
      //   815: iconst_0
      //   816: istore_2
      //   817: goto -207 -> 610
      //   820: astore 14
      //   822: aload_3
      //   823: ifnull +19 -> 842
      //   826: aload_3
      //   827: invokevirtual 111	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   830: ifnull +12 -> 842
      //   833: aload_3
      //   834: invokevirtual 111	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   837: invokeinterface 116 1 0
      //   842: aload_0
      //   843: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   846: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   849: iconst_1
      //   850: invokevirtual 120	com/wf/wellsfargomobile/WFApp:setRearImageUploadProcessComplete	(Z)V
      //   853: aload_0
      //   854: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   857: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   860: aconst_null
      //   861: invokevirtual 124	com/wf/wellsfargomobile/WFApp:setRearCheckImage	([B)V
      //   864: aload_0
      //   865: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   868: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   871: aconst_null
      //   872: invokevirtual 128	com/wf/wellsfargomobile/WFApp:setRearCheckPreviewImage	(Landroid/graphics/Bitmap;)V
      //   875: iconst_0
      //   876: istore_2
      //   877: goto -267 -> 610
      //   880: astore 13
      //   882: aload_3
      //   883: ifnull +19 -> 902
      //   886: aload_3
      //   887: invokevirtual 111	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   890: ifnull +12 -> 902
      //   893: aload_3
      //   894: invokevirtual 111	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   897: invokeinterface 116 1 0
      //   902: aload_0
      //   903: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   906: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   909: iconst_1
      //   910: invokevirtual 120	com/wf/wellsfargomobile/WFApp:setRearImageUploadProcessComplete	(Z)V
      //   913: aload_0
      //   914: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   917: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   920: aconst_null
      //   921: invokevirtual 124	com/wf/wellsfargomobile/WFApp:setRearCheckImage	([B)V
      //   924: aload_0
      //   925: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   928: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   931: aconst_null
      //   932: invokevirtual 128	com/wf/wellsfargomobile/WFApp:setRearCheckPreviewImage	(Landroid/graphics/Bitmap;)V
      //   935: iconst_0
      //   936: istore_2
      //   937: goto -327 -> 610
      //   940: astore 5
      //   942: aload_3
      //   943: ifnull +19 -> 962
      //   946: aload_3
      //   947: invokevirtual 111	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   950: ifnull +12 -> 962
      //   953: aload_3
      //   954: invokevirtual 111	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   957: invokeinterface 116 1 0
      //   962: aload_0
      //   963: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   966: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   969: iconst_1
      //   970: invokevirtual 120	com/wf/wellsfargomobile/WFApp:setRearImageUploadProcessComplete	(Z)V
      //   973: aload_0
      //   974: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   977: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   980: aconst_null
      //   981: invokevirtual 124	com/wf/wellsfargomobile/WFApp:setRearCheckImage	([B)V
      //   984: aload_0
      //   985: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   988: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   991: aconst_null
      //   992: invokevirtual 128	com/wf/wellsfargomobile/WFApp:setRearCheckPreviewImage	(Landroid/graphics/Bitmap;)V
      //   995: iconst_0
      //   996: istore_2
      //   997: goto -387 -> 610
      //   1000: astore 4
      //   1002: aload_3
      //   1003: ifnull +19 -> 1022
      //   1006: aload_3
      //   1007: invokevirtual 111	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   1010: ifnull +12 -> 1022
      //   1013: aload_3
      //   1014: invokevirtual 111	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   1017: invokeinterface 116 1 0
      //   1022: aload_0
      //   1023: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   1026: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   1029: iconst_1
      //   1030: invokevirtual 120	com/wf/wellsfargomobile/WFApp:setRearImageUploadProcessComplete	(Z)V
      //   1033: aload_0
      //   1034: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   1037: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   1040: aconst_null
      //   1041: invokevirtual 124	com/wf/wellsfargomobile/WFApp:setRearCheckImage	([B)V
      //   1044: aload_0
      //   1045: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   1048: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   1051: aconst_null
      //   1052: invokevirtual 128	com/wf/wellsfargomobile/WFApp:setRearCheckPreviewImage	(Landroid/graphics/Bitmap;)V
      //   1055: aload 4
      //   1057: athrow
      //   1058: iload_2
      //   1059: ifne +21 -> 1080
      //   1062: ldc 9
      //   1064: ldc_w 298
      //   1067: invokestatic 99	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
      //   1070: pop
      //   1071: iconst_0
      //   1072: istore 7
      //   1074: iconst_0
      //   1075: istore 6
      //   1077: goto -458 -> 619
      //   1080: iinc 8 1
      //   1083: ldc 9
      //   1085: ldc_w 300
      //   1088: invokestatic 99	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
      //   1091: pop
      //   1092: aload_0
      //   1093: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   1096: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   1099: invokevirtual 303	com/wf/wellsfargomobile/WFApp:isFrontImageUploadProcessComplete	()Z
      //   1102: ifeq +22 -> 1124
      //   1105: aload_0
      //   1106: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   1109: invokestatic 44	com/wf/wellsfargomobile/WFwebview:access$400	(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
      //   1112: invokevirtual 306	com/wf/wellsfargomobile/WFApp:isRearImageUploadProcessComplete	()Z
      //   1115: ifeq +9 -> 1124
      //   1118: iconst_0
      //   1119: istore 7
      //   1121: goto -502 -> 619
      //   1124: ldc2_w 307
      //   1127: invokestatic 313	java/lang/Thread:sleep	(J)V
      //   1130: goto -511 -> 619
      //   1133: astore 10
      //   1135: ldc 9
      //   1137: ldc_w 315
      //   1140: invokestatic 99	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
      //   1143: pop
      //   1144: goto -525 -> 619
      //   1147: aload_0
      //   1148: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   1151: invokestatic 68	com/wf/wellsfargomobile/WFwebview:access$1400	(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;
      //   1154: invokevirtual 74	android/app/Dialog:isShowing	()Z
      //   1157: ifeq +13 -> 1170
      //   1160: aload_0
      //   1161: getfield 17	com/wf/wellsfargomobile/WFwebview$SubmitRearImageThread:this$0	Lcom/wf/wellsfargomobile/WFwebview;
      //   1164: invokestatic 68	com/wf/wellsfargomobile/WFwebview:access$1400	(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;
      //   1167: invokevirtual 77	android/app/Dialog:dismiss	()V
      //   1170: iload 6
      //   1172: invokestatic 105	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
      //   1175: areturn
      //   1176: astore 4
      //   1178: aload 23
      //   1180: astore_3
      //   1181: goto -179 -> 1002
      //   1184: astore 28
      //   1186: aload 23
      //   1188: astore_3
      //   1189: goto -247 -> 942
      //   1192: astore 27
      //   1194: aload 23
      //   1196: astore_3
      //   1197: goto -315 -> 882
      //   1200: astore 26
      //   1202: aload 23
      //   1204: astore_3
      //   1205: goto -383 -> 822
      //   1208: astore 25
      //   1210: aload 23
      //   1212: astore_3
      //   1213: goto -451 -> 762
      //   1216: astore 24
      //   1218: aload 23
      //   1220: astore_3
      //   1221: goto -519 -> 702
      //
      // Exception table:
      //   from	to	target	type
      //   520	547	682	org/json/JSONException
      //   657	679	682	org/json/JSONException
      //   17	32	700	java/nio/charset/IllegalCharsetNameException
      //   37	60	700	java/nio/charset/IllegalCharsetNameException
      //   60	92	700	java/nio/charset/IllegalCharsetNameException
      //   148	214	700	java/nio/charset/IllegalCharsetNameException
      //   17	32	760	java/nio/charset/UnsupportedCharsetException
      //   37	60	760	java/nio/charset/UnsupportedCharsetException
      //   60	92	760	java/nio/charset/UnsupportedCharsetException
      //   148	214	760	java/nio/charset/UnsupportedCharsetException
      //   17	32	820	java/io/UnsupportedEncodingException
      //   37	60	820	java/io/UnsupportedEncodingException
      //   60	92	820	java/io/UnsupportedEncodingException
      //   148	214	820	java/io/UnsupportedEncodingException
      //   17	32	880	org/apache/http/client/ClientProtocolException
      //   37	60	880	org/apache/http/client/ClientProtocolException
      //   60	92	880	org/apache/http/client/ClientProtocolException
      //   148	214	880	org/apache/http/client/ClientProtocolException
      //   17	32	940	java/io/IOException
      //   37	60	940	java/io/IOException
      //   60	92	940	java/io/IOException
      //   148	214	940	java/io/IOException
      //   17	32	1000	finally
      //   37	60	1000	finally
      //   60	92	1000	finally
      //   148	214	1000	finally
      //   1124	1130	1133	java/lang/InterruptedException
      //   214	520	1176	finally
      //   520	547	1176	finally
      //   657	679	1176	finally
      //   684	695	1176	finally
      //   214	520	1184	java/io/IOException
      //   520	547	1184	java/io/IOException
      //   657	679	1184	java/io/IOException
      //   684	695	1184	java/io/IOException
      //   214	520	1192	org/apache/http/client/ClientProtocolException
      //   520	547	1192	org/apache/http/client/ClientProtocolException
      //   657	679	1192	org/apache/http/client/ClientProtocolException
      //   684	695	1192	org/apache/http/client/ClientProtocolException
      //   214	520	1200	java/io/UnsupportedEncodingException
      //   520	547	1200	java/io/UnsupportedEncodingException
      //   657	679	1200	java/io/UnsupportedEncodingException
      //   684	695	1200	java/io/UnsupportedEncodingException
      //   214	520	1208	java/nio/charset/UnsupportedCharsetException
      //   520	547	1208	java/nio/charset/UnsupportedCharsetException
      //   657	679	1208	java/nio/charset/UnsupportedCharsetException
      //   684	695	1208	java/nio/charset/UnsupportedCharsetException
      //   214	520	1216	java/nio/charset/IllegalCharsetNameException
      //   520	547	1216	java/nio/charset/IllegalCharsetNameException
      //   657	679	1216	java/nio/charset/IllegalCharsetNameException
      //   684	695	1216	java/nio/charset/IllegalCharsetNameException
    }

    protected void onPostExecute(Boolean paramBoolean)
    {
      if (paramBoolean.booleanValue())
      {
        WFwebview.this.runOnUiThread(new Runnable()
        {
          public void run()
          {
            Log.d("SubmitRearImageThread", "Successful execution of rear image upload, calling JavaScript");
            WFwebview.this.loadJavaScript("WFMRDC.receiveCheckImages()");
          }
        });
        return;
      }
      Log.d("SubmitRearImageThread", "failed execution of rear image upload, opeing retake image web page");
      WFwebview.this.runOnUiThread(new Runnable()
      {
        public void run()
        {
          WFwebview.this.loadURL("/deposit/recoverableMitekError.action?");
        }
      });
    }
  }

  private class WFWebViewClient extends WebViewClient
  {
    private static final String TAG = "WFWebViewClient";

    private WFWebViewClient()
    {
    }

    public void onLoadResource(WebView paramWebView, String paramString)
    {
      WFwebview.this.clearAndPreventCache();
    }

    public void onPageFinished(WebView paramWebView, String paramString)
    {
      Log.d("WFWebViewClient", "entering onPageFinished() - url: " + paramString);
      if (paramString.indexOf("/deposit/depositDetail.action") > 1)
      {
        new WFwebview.SendPerfTimingsThread(WFwebview.this, paramWebView.getContext(), WFwebview.this.wfApp.getPerfD1FrontImageStartSend(), WFwebview.this.wfApp.getPerfD2FrontImageJsonResponse(), WFwebview.this.wfApp.getPerfD3RearImageStartSend(), WFwebview.this.wfApp.getPerfD4RearImageJsonResponse(), WFwebview.this.wfApp.getPerfD5DataStartSend(), WFwebview.this.wfApp.getPerfD6DataJsonResponse()).execute(new Void[0]);
        WFwebview.this.wfApp.setPerfD1FrontImageStartSend(0L);
        WFwebview.this.wfApp.setPerfD2FrontImageJsonResponse(0L);
        WFwebview.this.wfApp.setPerfD3RearImageStartSend(0L);
        WFwebview.this.wfApp.setPerfD4RearImageJsonResponse(0L);
        WFwebview.this.wfApp.setPerfD5DataStartSend(0L);
        WFwebview.this.wfApp.setPerfD6DataJsonResponse(0L);
      }
      if ((WFwebview.this.inSession.booleanValue() == true) && (WFwebview.this.loginFirstLoad) && (paramString.indexOf("data:text/html") == -1))
      {
        WFwebview.access$702(WFwebview.this, false);
        WFwebview.this.webview.clearCache(true);
        WFwebview.this.webview.clearHistory();
        WFwebview.this.webviewCurtain.setVisibility(8);
      }
      while (true)
      {
        WFwebview.this.loading.setVisibility(8);
        Log.d("WFWebViewClient", "exiting onPageFinished() - url: " + paramString);
        return;
        if (WFwebview.this.firstLoad)
        {
          WFwebview.access$1002(WFwebview.this, false);
          WFwebview.this.webviewCurtain.setVisibility(8);
        }
      }
    }

    public void onPageStarted(WebView paramWebView, String paramString, Bitmap paramBitmap)
    {
      WFwebview.this.loading.setVisibility(0);
    }

    public void onReceivedError(WebView paramWebView, int paramInt, String paramString1, String paramString2)
    {
      WFwebview.this.returnWithError(WFwebview.this.getString(2131165253), WFwebview.this.getString(2131165252));
    }

    public boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
    {
      Log.d("WFWebViewClient", "shouldOverrideUrlLoading - url: " + paramString);
      int i = 1;
      Log.d("WFWebViewClient", "url: " + paramString);
      try
      {
        localURL = new URL(paramString);
      }
      catch (MalformedURLException localMalformedURLException2)
      {
        try
        {
          URL localURL;
          String str = localURL.getHost();
          String[] arrayOfString = paramWebView.getContext().getResources().getStringArray(2131230720);
          int j = arrayOfString.length;
          for (int k = 0; k < j; k++)
          {
            boolean bool = str.endsWith(arrayOfString[k]);
            if (bool)
              i = 0;
          }
          while (true)
          {
            label125: if (paramString.indexOf("tel:") >= 0);
            try
            {
              WFwebview.this.startActivity(new Intent("android.intent.action.DIAL", Uri.parse(paramString)));
              while (true)
              {
                label154: return true;
                if ((paramString.startsWith("data:text/html,")) && (paramString.contains("/signOn/appSignon.action")))
                {
                  WFwebview.this.loadURL("/accounts/g.accountList.do");
                }
                else if (i != 0)
                {
                  WFwebview.access$202(WFwebview.this, paramString);
                  new AlertDialog.Builder(WFwebview.this).setIcon(17301659).setTitle(2131165289).setNegativeButton("Back", new DialogInterface.OnClickListener()
                  {
                    public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
                    {
                    }
                  }).setPositiveButton("OK", new DialogInterface.OnClickListener()
                  {
                    public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
                    {
                      WFwebview.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(WFwebview.this.outsideURL)));
                    }
                  }).create().show();
                }
                else if ((paramString.indexOf("/mba/begin.action") >= 0) || (paramString.indexOf("signOn/begin.do") >= 0) || (paramString.indexOf("g.signOn.do") >= 0))
                {
                  WFwebview.this.finish();
                }
                else if (paramString.indexOf("g.signOff.do") >= 0)
                {
                  WFwebview.this.signOff();
                }
                else
                {
                  if ((!WFwebview.this.wfApp.isGoogleApiAvailable()) || (paramString.indexOf("g.locations.do") < 0))
                    break;
                  WFwebview.this.launchLocSearch();
                }
              }
              return false;
            }
            catch (ActivityNotFoundException localActivityNotFoundException)
            {
              break label154;
            }
            localMalformedURLException2 = localMalformedURLException2;
          }
        }
        catch (MalformedURLException localMalformedURLException1)
        {
          break label125;
        }
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.wf.wellsfargomobile-700220\classes_dex2jar.jar
 * Qualified Name:     com.wf.wellsfargomobile.WFwebview
 * JD-Core Version:    0.6.2
 */