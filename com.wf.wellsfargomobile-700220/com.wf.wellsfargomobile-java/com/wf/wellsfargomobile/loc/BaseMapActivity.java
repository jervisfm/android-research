package com.wf.wellsfargomobile.loc;

import android.content.Intent;
import android.util.Log;
import com.google.android.maps.MapActivity;
import com.wf.wellsfargomobile.WFApp;

public abstract class BaseMapActivity extends MapActivity
{
  private static final String TAG = "BaseMapActivity";

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    Log.d("BaseMapActivity", getClass().getName() + " onActivityResult - resultCode: " + paramInt2);
    switch (paramInt2)
    {
    default:
    case 13:
    }
    while (true)
    {
      ((WFApp)getApplication()).setLastActivityStoppedAt(0L);
      return;
      Log.d("BaseMapActivity", getClass().getName() + " onActivityResult - signing off ");
      Log.d("BaseMapActivity", getClass().getName() + " calling finish() ");
      setResult(13, new Intent());
      finish();
    }
  }

  protected void onRestart()
  {
    super.onRestart();
    long l1 = System.currentTimeMillis();
    Log.d("BaseMapActivity", getClass().getName() + " onRestart - " + l1);
    WFApp localWFApp = (WFApp)getApplication();
    long l2 = l1 - localWFApp.getLastActivityStoppedAt();
    Log.d("BaseMapActivity", getClass().getName() + " onRestart - then: " + localWFApp.getLastActivityStoppedAt() + " now: " + l1 + " diff: " + l2 + " WFApp.MAX_STOPPED_STATE_TIME: " + 600000L);
    if ((localWFApp.getLastActivityStoppedAt() != 0L) && (l2 > 600000L))
    {
      localWFApp.clearCheckDepositData();
      setResult(13, new Intent());
      Log.d("BaseMapActivity", getClass().getName() + " onRestart - calling finish()");
      finish();
      localWFApp.setLastActivityStoppedAt(0L);
    }
  }

  protected void onStart()
  {
    super.onStart();
    Log.d("BaseMapActivity", getClass().getName() + " onStart");
    ((WFApp)getApplication()).setLastActivityStoppedAt(0L);
  }

  protected void onStop()
  {
    super.onStop();
    long l = System.currentTimeMillis();
    Log.d("BaseMapActivity", getClass().getName() + " onStop - " + l);
    ((WFApp)getApplication()).setLastActivityStoppedAt(l);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.wf.wellsfargomobile-700220\classes_dex2jar.jar
 * Qualified Name:     com.wf.wellsfargomobile.loc.BaseMapActivity
 * JD-Core Version:    0.6.2
 */