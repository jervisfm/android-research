package com.wf.wellsfargomobile.loc;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.wf.wellsfargomobile.BaseActivity;
import com.wf.wellsfargomobile.WFApp;
import com.wf.wellsfargomobile.util.Trinary;

public class Location extends BaseActivity
{
  public static final int ACTIVITY_SEARCH = 0;
  public static final int ACTIVITY_SEARCHFORM = 1;
  public static final String KEY_INSESSION = "in_session";
  public static final String KEY_WASGPS = "was_gps";
  private static final String TAG = "Location";
  private View.OnClickListener buttonHandler = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Button localButton = (Button)paramAnonymousView;
      if (localButton == Location.this.currentLocButton)
      {
        localIntent1 = new Intent(Location.this, LocationResults.class);
        localIntent1.putExtra("was_gps", true);
        localIntent1.putExtra("in_session", true);
        Location.this.startActivityForResult(localIntent1, 0);
      }
      while (localButton != Location.this.locSearchButton)
      {
        Intent localIntent1;
        return;
      }
      Intent localIntent2 = new Intent(Location.this, LocationForm.class);
      localIntent2.putExtra("in_session", true);
      Location.this.startActivityForResult(localIntent2, 1);
    }
  };
  private Button currentLocButton;
  private Button locSearchButton;
  private WFApp wfApp;

  private void showDialogBox(String paramString1, String paramString2)
  {
    if (paramString1 != null)
    {
      new AlertDialog.Builder(this).setIcon(17301543).setTitle(paramString1).setMessage(paramString2).setPositiveButton("OK", new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
        }
      }).create().show();
      return;
    }
    new AlertDialog.Builder(this).setIcon(17301543).setTitle(paramString2).setPositiveButton("OK", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
      }
    }).create().show();
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    if ((paramInt1 == 1) || (paramInt1 == 0))
    {
      if ((paramInt2 != 2) && (paramInt2 != 4))
        break label31;
      finish();
    }
    label31: 
    do
    {
      return;
      if (paramInt2 == 3)
      {
        setResult(3, null);
        finish();
        return;
      }
      if (paramInt2 == 6)
      {
        setResult(6, null);
        finish();
        return;
      }
    }
    while (paramInt2 != 1);
    Bundle localBundle = paramIntent.getExtras();
    if (localBundle.containsKey("error_title"));
    for (String str = localBundle.getString("error_title"); ; str = null)
    {
      showDialogBox(str, localBundle.getString("error_msg"));
      return;
    }
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903043);
    Log.d("Location", "onCreate");
    this.currentLocButton = ((Button)findViewById(2131099668));
    this.currentLocButton.setOnClickListener(this.buttonHandler);
    this.locSearchButton = ((Button)findViewById(2131099669));
    this.locSearchButton.setOnClickListener(this.buttonHandler);
    this.wfApp = ((WFApp)getApplication());
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    if (this.wfApp.getMrdcEnabled().equals(Trinary.TRUE))
      getMenuInflater().inflate(2131361793, paramMenu);
    while (true)
    {
      return true;
      getMenuInflater().inflate(2131361794, paramMenu);
    }
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
      return false;
    case 2131099774:
      finish();
      return true;
    case 2131099776:
      setResult(3, null);
      finish();
      return true;
    case 2131099775:
    }
    setResult(6, null);
    finish();
    return true;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.wf.wellsfargomobile-700220\classes_dex2jar.jar
 * Qualified Name:     com.wf.wellsfargomobile.loc.Location
 * JD-Core Version:    0.6.2
 */