package com.wf.wellsfargomobile.loc;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.util.Linkify;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.wf.wellsfargomobile.BaseActivity;
import com.wf.wellsfargomobile.WFApp;
import com.wf.wellsfargomobile.util.Trinary;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;

public class LocationDetails extends BaseActivity
{
  private static final String TAG = "LocationDetails";
  private TextView address;
  private final View.OnClickListener buttonHandler = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Object localObject;
      if ((Button)paramAnonymousView == LocationDetails.this.directions)
      {
        localObject = "";
        if (LocationDetails.this.myBranch.containsKey("addrLine1"))
          localObject = (String)localObject + (String)LocationDetails.this.myBranch.get("addrLine1");
        if (LocationDetails.this.myBranch.containsKey("city"))
          localObject = (String)localObject + "," + (String)LocationDetails.this.myBranch.get("city");
        if (LocationDetails.this.myBranch.containsKey("state"))
          localObject = (String)localObject + "," + (String)LocationDetails.this.myBranch.get("state");
        if (LocationDetails.this.myBranch.containsKey("postalcode"))
          localObject = (String)localObject + "," + (String)LocationDetails.this.myBranch.get("postalcode");
      }
      try
      {
        String str = URLEncoder.encode((String)localObject, "UTF-8");
        localObject = str;
        label235: Intent localIntent = new Intent("android.intent.action.VIEW", Uri.parse("http://maps.google.com/maps?daddr=" + (String)localObject));
        LocationDetails.this.startActivity(localIntent);
        return;
      }
      catch (UnsupportedEncodingException localUnsupportedEncodingException)
      {
        break label235;
      }
    }
  };
  private Button directions;
  private TextView hours;
  private LinearLayout hoursContainer;
  private HashMap<String, String> myBranch;
  private TextView phone;
  private LinearLayout phoneContainer;
  private TextView services;
  private LinearLayout servicesContainer;
  private WFApp wfApp;

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903044);
    Log.d("LocationDetails", "onCreate");
    this.address = ((TextView)findViewById(2131099671));
    this.phone = ((TextView)findViewById(2131099674));
    this.hours = ((TextView)findViewById(2131099676));
    this.services = ((TextView)findViewById(2131099678));
    this.phoneContainer = ((LinearLayout)findViewById(2131099673));
    this.hoursContainer = ((LinearLayout)findViewById(2131099675));
    this.servicesContainer = ((LinearLayout)findViewById(2131099677));
    this.directions = ((Button)findViewById(2131099679));
    this.directions.setOnClickListener(this.buttonHandler);
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
    {
      this.myBranch = ((HashMap)localBundle.get("branch_object"));
      String str1 = "";
      if (this.myBranch.containsKey("siteName"))
        str1 = str1 + (String)this.myBranch.get("siteName");
      if (this.myBranch.containsKey("addrLine1"))
        str1 = str1 + "\n" + (String)this.myBranch.get("addrLine1");
      if (this.myBranch.containsKey("city"))
        str1 = str1 + "\n" + (String)this.myBranch.get("city");
      if (this.myBranch.containsKey("state"))
        str1 = str1 + ", " + (String)this.myBranch.get("state");
      if (this.myBranch.containsKey("postalcode"))
        str1 = str1 + " " + (String)this.myBranch.get("postalcode");
      this.address.setText(str1);
      if (!this.myBranch.containsKey("phone"))
        break label709;
      String str3 = (String)this.myBranch.get("phone");
      this.phone.setText(str3.substring(0, 3) + "-" + str3.substring(3, 6) + "-" + str3.substring(6, 10));
      Linkify.addLinks(this.phone, 4);
      this.phoneContainer.setVisibility(0);
      label515: if (!this.myBranch.containsKey("hours"))
        break label721;
      this.hours.setText(((String)this.myBranch.get("hours")).replace('|', '\n'));
      this.hoursContainer.setVisibility(0);
      label561: String str2 = "";
      if (this.myBranch.containsKey("serviceType"))
        str2 = str2 + (String)this.myBranch.get("serviceType") + " ";
      if (this.myBranch.containsKey("atmCount"))
        str2 = str2 + (String)this.myBranch.get("atmCount") + " ATM(s)";
      if ("".equals(str2))
        break label733;
      this.services.setText(str2);
      this.servicesContainer.setVisibility(0);
    }
    while (true)
    {
      this.wfApp = ((WFApp)getApplication());
      return;
      finish();
      break;
      label709: this.phoneContainer.setVisibility(8);
      break label515;
      label721: this.hoursContainer.setVisibility(8);
      break label561;
      label733: this.servicesContainer.setVisibility(8);
    }
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    Bundle localBundle = getIntent().getExtras();
    if ((localBundle != null) && (localBundle.containsKey("in_session")) && (localBundle.getBoolean("in_session", false)))
      if (this.wfApp.getMrdcEnabled().equals(Trinary.TRUE))
        getMenuInflater().inflate(2131361793, paramMenu);
    while (true)
    {
      return true;
      getMenuInflater().inflate(2131361794, paramMenu);
      continue;
      getMenuInflater().inflate(2131361795, paramMenu);
    }
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
      return false;
    case 2131099777:
      setResult(4, null);
      finish();
      return true;
    case 2131099774:
      setResult(2, null);
      finish();
      return true;
    case 2131099776:
      setResult(3, null);
      finish();
      return true;
    case 2131099775:
    }
    setResult(6, null);
    finish();
    return true;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.wf.wellsfargomobile-700220\classes_dex2jar.jar
 * Qualified Name:     com.wf.wellsfargomobile.loc.LocationDetails
 * JD-Core Version:    0.6.2
 */