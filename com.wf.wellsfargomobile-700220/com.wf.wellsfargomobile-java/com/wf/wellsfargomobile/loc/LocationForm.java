package com.wf.wellsfargomobile.loc;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.ToggleButton;
import com.wf.wellsfargomobile.BaseActivity;
import com.wf.wellsfargomobile.WFApp;
import com.wf.wellsfargomobile.util.Trinary;

public class LocationForm extends BaseActivity
{
  public static final int ACTIVITY_SEARCH = 0;
  public static final String KEY_ADDRESS = "address_string";
  public static final String KEY_CITY = "city_string";
  public static final String KEY_STATE = "state_string";
  public static final String KEY_TOGATM = "toggle_atm";
  public static final String KEY_TOGBRANCH = "toggle_branch";
  public static final String KEY_TOGINSTORE = "toggle_instore";
  public static final String KEY_ZIP = "zip_string";
  private static final String TAG = "LocationForm";
  private TextView.OnEditorActionListener actionHandler = new TextView.OnEditorActionListener()
  {
    public boolean onEditorAction(TextView paramAnonymousTextView, int paramAnonymousInt, KeyEvent paramAnonymousKeyEvent)
    {
      if ((paramAnonymousInt == 2) || (paramAnonymousInt == 0))
      {
        EditText localEditText = (EditText)paramAnonymousTextView;
        if (localEditText == LocationForm.this.zip)
          LocationForm.this.checkZip();
        while (true)
        {
          return true;
          if (localEditText == LocationForm.this.state)
            LocationForm.this.checkState();
        }
      }
      return false;
    }
  };
  private EditText address;
  private View.OnClickListener buttonHandler = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Button localButton = (Button)paramAnonymousView;
      if (localButton == LocationForm.this.zipGo)
        LocationForm.this.checkZip();
      while (localButton != LocationForm.this.stateGo)
        return;
      LocationForm.this.checkState();
    }
  };
  private EditText city;
  private Boolean inSession;
  private EditText state;
  private Button stateGo;
  private ToggleButton togAtm;
  private Boolean togAtmValue = Boolean.valueOf(false);
  private ToggleButton togBranch;
  private Boolean togBranchValue = Boolean.valueOf(false);
  private ToggleButton togInStore;
  private Boolean togInStoreValue = Boolean.valueOf(false);
  private CompoundButton.OnCheckedChangeListener toggleHandler = new CompoundButton.OnCheckedChangeListener()
  {
    public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean)
    {
      ToggleButton localToggleButton = (ToggleButton)paramAnonymousCompoundButton;
      if (localToggleButton == LocationForm.this.togAtm)
        LocationForm.access$102(LocationForm.this, Boolean.valueOf(paramAnonymousBoolean));
      do
      {
        return;
        if (localToggleButton == LocationForm.this.togBranch)
        {
          LocationForm.access$302(LocationForm.this, Boolean.valueOf(paramAnonymousBoolean));
          return;
        }
      }
      while (localToggleButton != LocationForm.this.togInStore);
      LocationForm.access$502(LocationForm.this, Boolean.valueOf(paramAnonymousBoolean));
    }
  };
  private WFApp wfApp;
  private EditText zip;
  private Button zipGo;

  private void checkState()
  {
    if ((this.city.length() > 0) && (this.state.length() > 0))
    {
      launchSearchWithAddress(Boolean.valueOf(true));
      return;
    }
    showDialogBox(null, getString(2131165193));
  }

  private void checkZip()
  {
    if (this.zip.length() == 5)
    {
      launchSearchWithZip(Boolean.valueOf(true));
      return;
    }
    showDialogBox(null, getString(2131165293));
  }

  private void launchSearchWithAddress(Boolean paramBoolean)
  {
    Intent localIntent = new Intent(this, LocationResults.class);
    localIntent.putExtra("was_gps", false);
    if (this.address.length() > 0)
      localIntent.putExtra("address_string", this.address.getText().toString().trim());
    localIntent.putExtra("city_string", this.city.getText().toString().trim());
    localIntent.putExtra("state_string", this.state.getText().toString().trim().toUpperCase());
    localIntent.putExtra("zip_string", "");
    if (paramBoolean.booleanValue())
    {
      localIntent.putExtra("toggle_atm", this.togAtmValue);
      localIntent.putExtra("toggle_branch", this.togBranchValue);
      localIntent.putExtra("toggle_instore", this.togInStoreValue);
    }
    localIntent.putExtra("in_session", this.inSession);
    startActivityForResult(localIntent, 0);
  }

  private void launchSearchWithZip(Boolean paramBoolean)
  {
    Intent localIntent = new Intent(this, LocationResults.class);
    localIntent.putExtra("was_gps", false);
    localIntent.putExtra("zip_string", this.zip.getText().toString().trim());
    if (paramBoolean.booleanValue())
    {
      localIntent.putExtra("toggle_atm", this.togAtmValue);
      localIntent.putExtra("toggle_branch", this.togBranchValue);
      localIntent.putExtra("toggle_instore", this.togInStoreValue);
    }
    localIntent.putExtra("in_session", this.inSession);
    startActivityForResult(localIntent, 0);
  }

  private void showDialogBox(String paramString1, String paramString2)
  {
    if (paramString1 != null)
    {
      new AlertDialog.Builder(this).setIcon(17301543).setTitle(paramString1).setMessage(paramString2).setPositiveButton("OK", new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
        }
      }).create().show();
      return;
    }
    new AlertDialog.Builder(this).setIcon(17301543).setTitle(paramString2).setPositiveButton("OK", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
      }
    }).create().show();
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    if (paramInt1 == 0)
    {
      if (paramInt2 != 2)
        break label27;
      setResult(2, null);
      finish();
    }
    label27: 
    do
    {
      return;
      if (paramInt2 == 4)
      {
        setResult(4, null);
        finish();
        return;
      }
      if (paramInt2 == 3)
      {
        setResult(3, null);
        finish();
        return;
      }
      if (paramInt2 == 6)
      {
        setResult(6, null);
        finish();
        return;
      }
    }
    while (paramInt2 != 1);
    Bundle localBundle = paramIntent.getExtras();
    if (localBundle.containsKey("error_title"));
    for (String str = localBundle.getString("error_title"); ; str = null)
    {
      showDialogBox(str, localBundle.getString("error_msg"));
      return;
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903045);
    Log.d("LocationForm", "onCreate");
    this.zipGo = ((Button)findViewById(2131099682));
    this.zipGo.setOnClickListener(this.buttonHandler);
    this.stateGo = ((Button)findViewById(2131099686));
    this.stateGo.setOnClickListener(this.buttonHandler);
    this.zip = ((EditText)findViewById(2131099681));
    this.address = ((EditText)findViewById(2131099683));
    this.city = ((EditText)findViewById(2131099684));
    this.state = ((EditText)findViewById(2131099685));
    this.togAtm = ((ToggleButton)findViewById(2131099687));
    this.togBranch = ((ToggleButton)findViewById(2131099688));
    this.togInStore = ((ToggleButton)findViewById(2131099689));
    this.togAtm.setOnCheckedChangeListener(this.toggleHandler);
    this.togBranch.setOnCheckedChangeListener(this.toggleHandler);
    this.togInStore.setOnCheckedChangeListener(this.toggleHandler);
    this.zip.setOnEditorActionListener(this.actionHandler);
    this.state.setOnEditorActionListener(this.actionHandler);
    Bundle localBundle = getIntent().getExtras();
    if ((localBundle != null) && (localBundle.containsKey("in_session")) && (localBundle.getBoolean("in_session", false)));
    for (this.inSession = Boolean.valueOf(true); ; this.inSession = Boolean.valueOf(false))
    {
      this.wfApp = ((WFApp)getApplication());
      return;
    }
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    Bundle localBundle = getIntent().getExtras();
    if ((localBundle != null) && (localBundle.containsKey("in_session")) && (localBundle.getBoolean("in_session", false)))
      if (this.wfApp.getMrdcEnabled().equals(Trinary.TRUE))
        getMenuInflater().inflate(2131361793, paramMenu);
    while (true)
    {
      return true;
      getMenuInflater().inflate(2131361794, paramMenu);
      continue;
      getMenuInflater().inflate(2131361795, paramMenu);
    }
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
      return false;
    case 2131099777:
      finish();
      return true;
    case 2131099774:
      setResult(2, null);
      finish();
      return true;
    case 2131099776:
      setResult(3, null);
      finish();
      return true;
    case 2131099775:
    }
    setResult(6, null);
    finish();
    return true;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.wf.wellsfargomobile-700220\classes_dex2jar.jar
 * Qualified Name:     com.wf.wellsfargomobile.loc.LocationForm
 * JD-Core Version:    0.6.2
 */