package com.wf.wellsfargomobile.loc;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Application;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextPaint;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ZoomControls;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;
import com.google.android.maps.Projection;
import com.wf.wellsfargomobile.WFApp;
import com.wf.wellsfargomobile.util.Trinary;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

public class LocationResults extends BaseMapActivity
{
  public static final int ACTIVITY_DETAILS = 0;
  public static final String KEY_BRANCH = "branch_object";
  public static final String KEY_ERRORMESSAGE = "error_msg";
  public static final String KEY_ERRORTITLE = "error_title";
  private static final String TAG = "LocationResults";
  final String ENCODING = "UTF-8";
  private Location bestLoc;
  private final View.OnClickListener buttonHandler = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      Button localButton = (Button)paramAnonymousView;
      if (localButton == LocationResults.this.resultBack)
      {
        LocationResults.access$1120(LocationResults.this, LocationResults.this.resultsPerPage);
        LocationResults.this.updateResults();
      }
      while (localButton != LocationResults.this.resultMore)
        return;
      LocationResults.access$1112(LocationResults.this, LocationResults.this.resultsPerPage);
      LocationResults.this.updateResults();
    }
  };
  private int currentBranch = 0;
  private int currentZoom = 13;
  private int desiredAccuracy;
  private Boolean gpsSearching;
  private Handler gpsTimeout;
  private Boolean inSession;
  private TextView loadStatus;
  private View locCurtain;
  private MapController mapController;
  private List<Overlay> mapOverlays;
  private ArrayList<HashMap<String, String>> myBranches;
  private LocationListener myLL;
  private LocationManager myLM;
  private double myLat;
  private double myLong;
  private ScrollView myScroller;
  private LocationMapOverlay overlays;
  private Button resultBack;
  private TextView[] resultContent;
  private TextView[] resultLabels;
  private MapView resultMap;
  private Button resultMore;
  private TextView resultNumbers;
  private LinearLayout[] results;
  private final View.OnClickListener resultsHandler = new View.OnClickListener()
  {
    public void onClick(View paramAnonymousView)
    {
      LinearLayout localLinearLayout = (LinearLayout)paramAnonymousView;
      for (int i = 0; i < LocationResults.this.resultsPerPage; i++)
        if (LocationResults.this.results[i] == localLinearLayout)
        {
          LocationResults.this.results[i].requestFocusFromTouch();
          LocationResults.this.launchDetails(i);
        }
    }
  };
  private int resultsPerPage = 5;
  private TextView searchAddress;
  private Boolean wasGPS;
  private WFApp wfApp;
  private ZoomControls zoomControls;

  private void GPSSearch()
  {
    this.myLM.removeUpdates(this.myLL);
    this.myLat = this.bestLoc.getLatitude();
    this.myLong = this.bestLoc.getLongitude();
    this.loadStatus.setText(2131165239);
    String str = getString(2131165185) + "/locator/searchForLocationsByGeocode.do?lg=" + this.myLong + "&lt=" + this.myLat + "&WFAppId=" + getString(2131165187) + "&format=xml";
    this.searchAddress.setText("For Address: Current Location");
    new XMLParseTask(null).execute(new String[] { str });
  }

private void checkLocationProviders()
  {
    this.myLM = ((LocationManager)getSystemService("location"));
    this.myLL = new MyLocationListener(null);
    Boolean gpsEnabled = Boolean.valueOf(this.myLM.isProviderEnabled("gps"));
    Boolean cellNetwork = Boolean.valueOf(this.myLM.isProviderEnabled("network"));
    if ((!cellNetwork.booleanValue()) && (!gpsEnabled.booleanValue())) // Both GPS and Network Disabled
    { // Show Message that you must enable GPS to Continue. 
      this.gpsSearching = Boolean.valueOf(false);
      new AlertDialog.Builder(this).setIcon(17301543).setTitle(2131165220).setPositiveButton("Back", new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          LocationResults.this.finish();
        }
      }).setNegativeButton("Settings", new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          LocationResults.this.startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
          LocationResults.this.finish();
        }
      }).create().show();
      return;
    }
    if ((cellNetwork.booleanValue()) && (!gpsEnabled.booleanValue()))
    {  // Inform User it's more accurate to have GPS enabled but can use network provider for estimation
      this.gpsSearching = Boolean.valueOf(false);
      new AlertDialog.Builder(this).setIcon(17301543).setTitle(2131165224).setMessage(2131165223).setPositiveButton("Continue", new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
        }
      }).setNegativeButton("Settings", new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          LocationResults.this.startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
          LocationResults.this.finish();
        }
      }).create().show();
    }
    findCurrLocation(); // Actually find the current location. 
  }

  private void findCurrLocation()
  {
    this.myLM.requestLocationUpdates("gps", 0L, 0.0F, this.myLL);
    this.myLM.requestLocationUpdates("network", 0L, 0.0F, this.myLL);
    this.desiredAccuracy = 200;
    this.gpsSearching = Boolean.valueOf(true);
    this.bestLoc = null;
    this.gpsTimeout = new Handler();
    Runnable local7 = new Runnable()
    {
      public void run()
      {
        if (LocationResults.this.gpsSearching.booleanValue())
        {
          LocationResults.access$302(LocationResults.this, Boolean.valueOf(false));
          if (LocationResults.this.bestLoc == null)
            LocationResults.this.returnWithError(LocationResults.this.getString(2131165222), LocationResults.this.getString(2131165221));
        }
        else
        {
          return;
        }
        new AlertDialog.Builder(LocationResults.this).setIcon(17301543).setTitle(2131165226).setMessage(2131165225).setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
          public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int)
          {
          }
        }).create().show();
        LocationResults.this.GPSSearch();
      }
    };
    this.gpsTimeout.postDelayed(local7, 20000L);
  }

  private void launchDetails(int paramInt)
  {
    Intent localIntent = new Intent(this, LocationDetails.class);
    localIntent.putExtra("branch_object", (Serializable)this.myBranches.get(paramInt + this.currentBranch));
    localIntent.putExtra("in_session", this.inSession);
    startActivityForResult(localIntent, 0);
  }

  private void nonGPSSearch(Bundle paramBundle)
  {
    Object localObject = getString(2131165185) + "/locator/search.do?WFAppId=" + getString(2131165187);
    String str1 = "For Address: ";
    try
    {
      if (paramBundle.containsKey("address_string"))
      {
        str1 = str1 + paramBundle.getString("address_string") + ", ";
        localObject = (String)localObject + "&address=" + URLEncoder.encode(paramBundle.getString("address_string"), "UTF-8");
      }
      if (paramBundle.containsKey("city_string"))
      {
        str1 = str1 + paramBundle.getString("city_string") + " ";
        localObject = (String)localObject + "&city=" + URLEncoder.encode(paramBundle.getString("city_string"), "UTF-8");
      }
      if (paramBundle.containsKey("state_string"))
      {
        str1 = str1 + paramBundle.get("state_string") + " ";
        localObject = (String)localObject + "&state=" + URLEncoder.encode(paramBundle.getString("state_string"), "UTF-8");
      }
      if (paramBundle.containsKey("zip_string"))
      {
        str1 = str1 + paramBundle.get("zip_string");
        String str3 = (String)localObject + "&zip=" + URLEncoder.encode(paramBundle.getString("zip_string"), "UTF-8");
        localObject = str3;
      }
      label345: if ((paramBundle.containsKey("toggle_atm")) && (paramBundle.getBoolean("toggle_atm")))
        localObject = (String)localObject + "&wlw-checkbox_key:{actionForm.searchAtms}";
      if ((paramBundle.containsKey("toggle_branch")) && (paramBundle.getBoolean("toggle_branch")))
        localObject = (String)localObject + "&wlw-checkbox_key:{actionForm.searchBranches}";
      if ((paramBundle.containsKey("toggle_instore")) && (paramBundle.getBoolean("toggle_instore")))
        localObject = (String)localObject + "&wlw-checkbox_key:{actionForm.searchInStoreBranches}";
      String str2 = (String)localObject + "&format=xml";
      this.searchAddress.setText(str1);
      new XMLParseTask(null).execute(new String[] { str2 });
      return;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      break label345;
    }
  }

  private void returnWithError(String paramString1, String paramString2)
  {
    Bundle localBundle = new Bundle();
    if (paramString1 != null)
      localBundle.putString("error_title", paramString1);
    localBundle.putString("error_msg", paramString2);
    Intent localIntent = new Intent();
    localIntent.putExtras(localBundle);
    setResult(1, localIntent);
    finish();
  }

  private void updateMap()
  {
    this.mapController.animateTo(new GeoPoint((int)(1000000.0D * this.myLat), (int)(1000000.0D * this.myLong)));
    updateZoom();
    this.overlays.clearOverlays();
    for (int i = 0; i < this.resultsPerPage; i++)
      if (i + this.currentBranch < this.myBranches.size())
      {
        HashMap localHashMap = (HashMap)this.myBranches.get(i + this.currentBranch);
        OverlayItem localOverlayItem = new OverlayItem(new GeoPoint((int)(1000000.0D * Double.parseDouble((String)localHashMap.get("latitude"))), (int)(1000000.0D * Double.parseDouble((String)localHashMap.get("longitude")))), "" + (1 + (i + this.currentBranch)), (String)((HashMap)this.myBranches.get(i + this.currentBranch)).get("bankType"));
        this.overlays.addOverlay(localOverlayItem);
      }
    this.overlays.populateOverlays();
    this.mapOverlays.add(this.overlays);
  }

  private void updateResults()
  {
    updateMap();
    int i = 0;
    if (i < this.resultsPerPage)
    {
      if (i + this.currentBranch < this.myBranches.size())
      {
        HashMap localHashMap = (HashMap)this.myBranches.get(i + this.currentBranch);
        this.resultLabels[i].setText(1 + (i + this.currentBranch) + ".");
        this.resultContent[i].setText((String)localHashMap.get("siteName") + "\n(" + (String)localHashMap.get("distFromStart") + " miles)\n" + (String)localHashMap.get("addrLine1"));
        this.results[i].setVisibility(0);
      }
      while (true)
      {
        i++;
        break;
        this.results[i].setVisibility(8);
      }
    }
    if (this.currentBranch == 0)
    {
      this.resultBack.setVisibility(4);
      if (this.currentBranch + this.resultsPerPage < this.myBranches.size())
        break label304;
      this.resultNumbers.setText(1 + this.currentBranch + "-" + this.myBranches.size() + " of " + this.myBranches.size());
      this.resultMore.setVisibility(4);
    }
    while (true)
    {
      this.myScroller.scrollTo(0, 0);
      return;
      this.resultBack.setVisibility(0);
      break;
      label304: this.resultNumbers.setText(1 + this.currentBranch + "-" + (this.currentBranch + this.resultsPerPage) + " of " + this.myBranches.size());
      this.resultMore.setVisibility(0);
    }
  }

  private void updateZoom()
  {
    HashMap localHashMap;
    Double localDouble;
    int i;
    if (4 + this.currentBranch < this.myBranches.size())
    {
      localHashMap = (HashMap)this.myBranches.get(4 + this.currentBranch);
      localDouble = Double.valueOf(Double.parseDouble((String)localHashMap.get("distFromStart")));
      if (localDouble.doubleValue() >= 0.1D)
        break label124;
      i = 17;
    }
    while (true)
    {
      if (this.currentZoom == i)
        return;
      if (this.currentZoom < i)
      {
        this.currentZoom = (1 + this.currentZoom);
        this.mapController.zoomIn();
        continue;
        localHashMap = (HashMap)this.myBranches.get(-1 + this.myBranches.size());
        break;
        label124: if (localDouble.doubleValue() < 0.2D)
        {
          i = 16;
          continue;
        }
        if (localDouble.doubleValue() < 0.4D)
        {
          i = 15;
          continue;
        }
        if (localDouble.doubleValue() < 0.8D)
        {
          i = 14;
          continue;
        }
        if (localDouble.doubleValue() < 1.6D)
        {
          i = 13;
          continue;
        }
        if (localDouble.doubleValue() < 3.2D)
        {
          i = 12;
          continue;
        }
        if (localDouble.doubleValue() < 6.4D)
        {
          i = 11;
          continue;
        }
        if (localDouble.doubleValue() < 12.800000000000001D)
        {
          i = 10;
          continue;
        }
        i = 9;
        continue;
      }
      if (this.currentZoom > i)
      {
        this.currentZoom = (-1 + this.currentZoom);
        this.mapController.zoomOut();
      }
    }
  }

  protected boolean isRouteDisplayed()
  {
    return false;
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    if (paramInt1 == 0)
    {
      if (paramInt2 != 2)
        break label27;
      setResult(2, null);
      finish();
    }
    label27: 
    do
    {
      return;
      if (paramInt2 == 4)
      {
        setResult(4, null);
        finish();
        return;
      }
      if (paramInt2 == 3)
      {
        setResult(3, null);
        finish();
        return;
      }
    }
    while (paramInt2 != 6);
    setResult(6, null);
    finish();
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903046);
    this.locCurtain = findViewById(2131099720);
    this.loadStatus = ((TextView)findViewById(2131099665));
    this.searchAddress = ((TextView)findViewById(2131099696));
    this.resultNumbers = ((TextView)findViewById(2131099697));
    if ((0xF & getResources().getConfiguration().screenLayout) == 4)
    {
      this.resultsPerPage = 10;
      this.results = new LinearLayout[this.resultsPerPage];
      this.resultLabels = new TextView[this.resultsPerPage];
      this.resultContent = new TextView[this.resultsPerPage];
      this.results[0] = ((LinearLayout)findViewById(2131099698));
      this.resultLabels[0] = ((TextView)findViewById(2131099699));
      this.resultContent[0] = ((TextView)findViewById(2131099700));
      this.results[1] = ((LinearLayout)findViewById(2131099702));
      this.resultLabels[1] = ((TextView)findViewById(2131099703));
      this.resultContent[1] = ((TextView)findViewById(2131099704));
      this.results[2] = ((LinearLayout)findViewById(2131099706));
      this.resultLabels[2] = ((TextView)findViewById(2131099707));
      this.resultContent[2] = ((TextView)findViewById(2131099708));
      this.results[3] = ((LinearLayout)findViewById(2131099710));
      this.resultLabels[3] = ((TextView)findViewById(2131099711));
      this.resultContent[3] = ((TextView)findViewById(2131099712));
      this.results[4] = ((LinearLayout)findViewById(2131099714));
      this.resultLabels[4] = ((TextView)findViewById(2131099715));
      this.resultContent[4] = ((TextView)findViewById(2131099716));
      this.results[5] = ((LinearLayout)findViewById(2131099721));
      this.resultLabels[5] = ((TextView)findViewById(2131099722));
      this.resultContent[5] = ((TextView)findViewById(2131099723));
      this.results[6] = ((LinearLayout)findViewById(2131099725));
      this.resultLabels[6] = ((TextView)findViewById(2131099726));
      this.resultContent[6] = ((TextView)findViewById(2131099727));
      this.results[7] = ((LinearLayout)findViewById(2131099729));
      this.resultLabels[7] = ((TextView)findViewById(2131099730));
      this.resultContent[7] = ((TextView)findViewById(2131099731));
      this.results[8] = ((LinearLayout)findViewById(2131099733));
      this.resultLabels[8] = ((TextView)findViewById(2131099734));
      this.resultContent[8] = ((TextView)findViewById(2131099735));
      this.results[9] = ((LinearLayout)findViewById(2131099737));
      this.resultLabels[9] = ((TextView)findViewById(2131099738));
      this.resultContent[9] = ((TextView)findViewById(2131099739));
    }
    while (true)
    {
      this.resultBack = ((Button)findViewById(2131099718));
      this.resultBack.setOnClickListener(this.buttonHandler);
      this.resultMore = ((Button)findViewById(2131099719));
      this.resultMore.setOnClickListener(this.buttonHandler);
      this.zoomControls = ((ZoomControls)findViewById(2131099695));
      this.zoomControls.setOnZoomInClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          if (LocationResults.this.mapController.zoomIn())
            LocationResults.access$108(LocationResults.this);
        }
      });
      this.zoomControls.setOnZoomOutClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          if (LocationResults.this.mapController.zoomOut())
            LocationResults.access$110(LocationResults.this);
        }
      });
      for (int i = 0; i < this.resultsPerPage; i++)
        this.results[i].setOnClickListener(this.resultsHandler);
      this.resultsPerPage = 5;
      this.results = new LinearLayout[this.resultsPerPage];
      this.resultLabels = new TextView[this.resultsPerPage];
      this.resultContent = new TextView[this.resultsPerPage];
      this.results[0] = ((LinearLayout)findViewById(2131099698));
      this.resultLabels[0] = ((TextView)findViewById(2131099699));
      this.resultContent[0] = ((TextView)findViewById(2131099700));
      this.results[1] = ((LinearLayout)findViewById(2131099702));
      this.resultLabels[1] = ((TextView)findViewById(2131099703));
      this.resultContent[1] = ((TextView)findViewById(2131099704));
      this.results[2] = ((LinearLayout)findViewById(2131099706));
      this.resultLabels[2] = ((TextView)findViewById(2131099707));
      this.resultContent[2] = ((TextView)findViewById(2131099708));
      this.results[3] = ((LinearLayout)findViewById(2131099710));
      this.resultLabels[3] = ((TextView)findViewById(2131099711));
      this.resultContent[3] = ((TextView)findViewById(2131099712));
      this.results[4] = ((LinearLayout)findViewById(2131099714));
      this.resultLabels[4] = ((TextView)findViewById(2131099715));
      this.resultContent[4] = ((TextView)findViewById(2131099716));
    }
    this.myScroller = ((ScrollView)findViewById(2131099691));
    this.resultMap = ((MapView)findViewById(2131099693));
    this.mapController = this.resultMap.getController();
    this.mapOverlays = this.resultMap.getOverlays();
    this.overlays = new LocationMapOverlay(getResources().getDrawable(2130837529));
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
    {
      this.wasGPS = Boolean.valueOf(localBundle.getBoolean("was_gps", true));
      if (this.wasGPS.booleanValue())
      {
        this.loadStatus.setText(2131165238);
        checkLocationProviders();
      }
    }
    else
    {
      if ((localBundle == null) || (!localBundle.containsKey("in_session")) || (!localBundle.getBoolean("in_session", false)))
        break label1202;
    }
    label1202: for (this.inSession = Boolean.valueOf(true); ; this.inSession = Boolean.valueOf(false))
    {
      this.wfApp = ((WFApp)getApplication());
      return;
      this.loadStatus.setText(2131165239);
      nonGPSSearch(localBundle);
      break;
    }
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    Bundle localBundle = getIntent().getExtras();
    if ((localBundle != null) && (localBundle.containsKey("in_session")) && (localBundle.getBoolean("in_session", false)))
      if (this.wfApp.getMrdcEnabled().equals(Trinary.TRUE))
        getMenuInflater().inflate(2131361793, paramMenu);
    while (true)
    {
      return true;
      getMenuInflater().inflate(2131361794, paramMenu);
      continue;
      getMenuInflater().inflate(2131361795, paramMenu);
    }
  }

  protected void onDestroy()
  {
    if (this.wasGPS.booleanValue())
      this.myLM.removeUpdates(this.myLL);
    this.gpsSearching = Boolean.valueOf(false);
    super.onDestroy();
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 4) && (this.currentBranch > 0))
    {
      this.currentBranch -= this.resultsPerPage;
      updateResults();
      return false;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
      return false;
    case 2131099777:
      setResult(4, null);
      finish();
      return true;
    case 2131099774:
      setResult(2, null);
      finish();
      return true;
    case 2131099776:
      setResult(3, null);
      finish();
      return true;
    case 2131099775:
    }
    setResult(6, null);
    finish();
    return true;
  }

  public class LocationMapOverlay extends ItemizedOverlay<OverlayItem>
  {
    private String addr;
    private String bankType;
    private int clicked = 0;
    private final float dp;
    private final float font_height;
    private final Bitmap iconWF;
    private final Bitmap iconWach;
    private final Bitmap markerWF;
    private final Bitmap markerWach;
    private final ArrayList<OverlayItem> overlays = new ArrayList();
    private Boolean popup = Boolean.valueOf(false);
    private String siteName;
    private final float sp;
    private final float tail_height;
    private final float tail_width;
    private final TextPaint textPaint;
    private final Paint windowBorder;
    private final Paint windowInner;
    private final float window_height;
    private final float window_padding;
    private float window_width;

    public LocationMapOverlay(Drawable arg2)
    {
      super();
      Resources localResources = LocationResults.this.getApplication().getResources();
      this.markerWF = BitmapFactory.decodeResource(localResources, 2130837529);
      this.markerWach = BitmapFactory.decodeResource(localResources, 2130837528);
      this.iconWF = BitmapFactory.decodeResource(localResources, 2130837534);
      this.iconWach = BitmapFactory.decodeResource(localResources, 2130837533);
      this.sp = localResources.getDimension(2131034113);
      this.dp = localResources.getDimension(2131034112);
      this.window_width = (200.0F * this.dp);
      this.window_height = (50.0F * this.dp);
      this.window_padding = (5.0F * this.dp);
      this.tail_height = (10.0F * this.dp);
      this.tail_width = (6.0F * this.dp);
      this.textPaint = new TextPaint();
      this.textPaint.setFakeBoldText(true);
      this.textPaint.setAntiAlias(true);
      this.textPaint.setTextSize(12.0F * this.sp);
      this.font_height = this.textPaint.getFontMetrics(null);
      this.windowInner = new Paint();
      this.windowInner.setColor(LocationResults.this.getResources().getColor(2130968576));
      this.windowInner.setAntiAlias(true);
      this.windowInner.setStyle(Paint.Style.FILL);
      this.windowBorder = new Paint();
      this.windowBorder.setAntiAlias(true);
      this.windowBorder.setStyle(Paint.Style.STROKE);
      setBorderDark();
    }

    private Boolean getHitMapLocation(GeoPoint paramGeoPoint, MapView paramMapView)
    {
      Point localPoint = new Point();
      RectF localRectF = new RectF();
      if (this.popup.booleanValue())
      {
        paramMapView.getProjection().toPixels(getItem(this.clicked).getPoint(), localPoint);
        localRectF.set(0.0F, 0.0F, this.window_width, this.window_height);
        localRectF.offset(localPoint.x - this.window_width / 2.0F, localPoint.y - this.window_height - this.markerWF.getHeight() - this.tail_height);
        paramMapView.getProjection().toPixels(paramGeoPoint, localPoint);
        if (localRectF.contains(localPoint.x, localPoint.y))
        {
          LocationResults.this.launchDetails(this.clicked);
          return Boolean.valueOf(false);
        }
      }
      for (int i = 0; i < size(); i++)
      {
        paramMapView.getProjection().toPixels(getItem(i).getPoint(), localPoint);
        localRectF.set(-this.markerWF.getWidth() / 2, -this.markerWF.getHeight(), this.markerWF.getWidth() / 2, 0.0F);
        localRectF.offset(localPoint.x, localPoint.y);
        paramMapView.getProjection().toPixels(paramGeoPoint, localPoint);
        if (localRectF.contains(localPoint.x, localPoint.y))
        {
          HashMap localHashMap = (HashMap)LocationResults.this.myBranches.get(i + LocationResults.this.currentBranch);
          this.siteName = ((String)localHashMap.get("siteName"));
          this.addr = ((String)localHashMap.get("addrLine1"));
          this.bankType = ((String)localHashMap.get("bankType"));
          float f = this.textPaint.measureText(this.siteName);
          if (f < this.textPaint.measureText(this.addr))
            f = this.textPaint.measureText(this.addr);
          if (f < 150.0F * this.dp)
            f = 150.0F * this.dp;
          this.window_width = (f + 2.0F * this.window_padding + this.iconWF.getWidth() + 10.0F * this.dp);
          LocationResults.this.mapController.animateTo(paramMapView.getProjection().fromPixels(localPoint.x, localPoint.y - this.markerWF.getHeight() / 2));
          this.clicked = i;
          this.popup = Boolean.valueOf(true);
          return Boolean.valueOf(true);
        }
      }
      this.popup = Boolean.valueOf(false);
      return Boolean.valueOf(true);
    }

    private void setBorderBackground()
    {
      this.windowBorder.setColor(LocationResults.this.getResources().getColor(2130968576));
    }

    private void setBorderDark()
    {
      this.windowBorder.setARGB(255, 130, 130, 130);
    }

    private void setLinkColor()
    {
      this.textPaint.setColor(LocationResults.this.getResources().getColor(2130968578));
    }

    private void setTextColor()
    {
      this.textPaint.setARGB(255, 30, 30, 30);
    }

    public void addOverlay(OverlayItem paramOverlayItem)
    {
      this.overlays.add(paramOverlayItem);
    }

    public void clearOverlays()
    {
      this.popup = Boolean.valueOf(false);
      this.overlays.clear();
    }

    protected OverlayItem createItem(int paramInt)
    {
      return (OverlayItem)this.overlays.get(paramInt);
    }

    public void draw(Canvas paramCanvas, MapView paramMapView, boolean paramBoolean)
    {
      Projection localProjection = paramMapView.getProjection();
      int i = -1 + size();
      if (i >= 0)
      {
        OverlayItem localOverlayItem = getItem(i);
        String str1 = localOverlayItem.getTitle();
        String str2 = localOverlayItem.getSnippet();
        Point localPoint2 = localProjection.toPixels(localOverlayItem.getPoint(), null);
        if (str2.equals("Wells Fargo"))
        {
          paramCanvas.drawBitmap(this.markerWF, localPoint2.x - this.markerWF.getWidth() / 2, localPoint2.y - this.markerWF.getHeight(), null);
          this.textPaint.setARGB(255, 30, 30, 30);
        }
        while (true)
        {
          paramCanvas.drawText(str1, localPoint2.x - 3 * str1.length() * this.dp - (-1 + str1.length()) * this.dp, localPoint2.y - 20.0F * this.dp, this.textPaint);
          i--;
          break;
          if (str2.equals("Wachovia"))
          {
            paramCanvas.drawBitmap(this.markerWach, localPoint2.x - this.markerWach.getWidth() / 2, localPoint2.y - this.markerWach.getHeight(), null);
            this.textPaint.setARGB(255, 200, 200, 200);
          }
        }
      }
      Point localPoint1;
      float f1;
      float f2;
      if (this.popup.booleanValue())
      {
        localPoint1 = localProjection.toPixels(getItem(this.clicked).getPoint(), null);
        RectF localRectF3 = new RectF(0.0F, 0.0F, this.window_width, this.window_height);
        f1 = localPoint1.x - this.window_width / 2.0F;
        f2 = localPoint1.y - this.window_height - this.markerWF.getHeight() - this.tail_height;
        localRectF3.offset(f1, f2);
        this.windowBorder.setStrokeWidth(1.0F * this.dp);
        paramCanvas.drawRoundRect(localRectF3, 5.0F * this.dp, 5.0F * this.dp, this.windowInner);
        paramCanvas.drawRoundRect(localRectF3, 5.0F * this.dp, 5.0F * this.dp, this.windowBorder);
        setTextColor();
        paramCanvas.drawText(this.siteName, f1 + this.window_padding, f2 + this.font_height, this.textPaint);
        this.textPaint.setFakeBoldText(false);
        paramCanvas.drawText(this.addr, f1 + this.window_padding, f2 + 2.0F * this.font_height, this.textPaint);
        this.textPaint.setFakeBoldText(true);
        setLinkColor();
        paramCanvas.drawText("Click for more details", f1 + this.window_padding, f2 + 3.0F * this.font_height, this.textPaint);
        if (!this.bankType.equals("Wells Fargo"))
          break label941;
        paramCanvas.drawBitmap(this.iconWF, f1 + this.window_width - this.window_padding - this.iconWF.getWidth(), f2 + this.window_padding, null);
      }
      while (true)
      {
        float f3 = localPoint1.x;
        float f4 = localPoint1.y - this.markerWF.getHeight() - 1.0F * this.dp;
        Path localPath = new Path();
        localPath.moveTo(f3, f4);
        localPath.lineTo(f3 + this.tail_width / 2.0F, f4 - this.tail_height);
        localPath.lineTo(f3 - this.tail_width / 2.0F, f4 - this.tail_height);
        paramCanvas.drawPath(localPath, this.windowInner);
        paramCanvas.drawLine(f3 - this.tail_width / 2.0F, f4 - this.tail_height, f3, f4, this.windowBorder);
        paramCanvas.drawLine(f3, f4, f3 + this.tail_width / 2.0F, f4 - this.tail_height, this.windowBorder);
        RectF localRectF1 = new RectF(0.0F, 0.0F, LocationResults.this.resultMap.getWidth(), LocationResults.this.resultMap.getHeight());
        this.windowBorder.setStrokeWidth(2.0F * this.dp);
        setBorderBackground();
        paramCanvas.drawRect(localRectF1, this.windowBorder);
        setBorderDark();
        this.windowBorder.setStrokeWidth(1.0F * this.dp);
        for (int j = 0; j < 5; j++)
        {
          RectF localRectF2 = new RectF(j * this.dp, j * this.dp, LocationResults.this.resultMap.getWidth() - j * this.dp, LocationResults.this.resultMap.getHeight() - j * this.dp);
          int k = this.windowBorder.getAlpha();
          this.windowBorder.setAlpha(k / (j + 1));
          paramCanvas.drawRoundRect(localRectF2, 7.0F * this.dp, 7.0F * this.dp, this.windowBorder);
        }
        label941: paramCanvas.drawBitmap(this.iconWach, f1 + this.window_width - this.window_padding - this.iconWF.getWidth(), f2 + this.window_padding, null);
      }
      setBorderDark();
    }

    public boolean onTap(GeoPoint paramGeoPoint, MapView paramMapView)
    {
      if (getHitMapLocation(paramGeoPoint, paramMapView).booleanValue())
        LocationResults.this.resultMap.invalidate();
      return true;
    }

    public void populateOverlays()
    {
      populate();
    }

    public int size()
    {
      return this.overlays.size();
    }
  }

  private class MyLocationListener
    implements LocationListener
  {
    private MyLocationListener()
    {
    }

    public void onLocationChanged(Location paramLocation)
    {
      if ((LocationResults.this.bestLoc == null) || (paramLocation.getAccuracy() < LocationResults.this.bestLoc.getAccuracy()))
        LocationResults.access$402(LocationResults.this, paramLocation);
      if (LocationResults.this.bestLoc.getAccuracy() <= LocationResults.this.desiredAccuracy)
      {
        LocationResults.access$302(LocationResults.this, Boolean.valueOf(false));
        LocationResults.this.GPSSearch();
      }
    }

    public void onProviderDisabled(String paramString)
    {
    }

    public void onProviderEnabled(String paramString)
    {
    }

    public void onStatusChanged(String paramString, int paramInt, Bundle paramBundle)
    {
    }
  }

  private class XMLParseTask extends AsyncTask<String, Integer, Object>
  {
    private XMLParseTask()
    {
    }

    protected Object doInBackground(String[] paramArrayOfString)
    {
      try
      {
        URL localURL = new URL(paramArrayOfString[0]);
        XMLReader localXMLReader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
        LocationXMLHandler localLocationXMLHandler = new LocationXMLHandler();
        localXMLReader.setContentHandler(localLocationXMLHandler);
        localXMLReader.parse(new InputSource(localURL.openStream()));
        LocationResults.access$802(LocationResults.this, localLocationXMLHandler.getParsedLocations());
        return null;
      }
      catch (Exception localException)
      {
        while (true)
        {
          Log.d("LocationResults", "Parsing Error: ", localException);
          LocationResults.this.returnWithError(LocationResults.this.getString(2131165245), LocationResults.this.getString(2131165243));
        }
      }
    }

    protected void onPostExecute(Object paramObject)
    {
      if (LocationResults.this.myBranches == null)
      {
        LocationResults.this.returnWithError(LocationResults.this.getString(2131165245), LocationResults.this.getString(2131165243));
        return;
      }
      if (LocationResults.this.myBranches.size() == 0)
      {
        LocationResults.this.returnWithError(LocationResults.this.getString(2131165245), LocationResults.this.getString(2131165244));
        return;
      }
      if (((HashMap)LocationResults.this.myBranches.get(0)).containsKey("error"))
      {
        LocationResults.this.returnWithError(null, (String)((HashMap)LocationResults.this.myBranches.get(0)).get("error"));
        return;
      }
      if (((String)((HashMap)LocationResults.this.myBranches.get(0)).get("id")).matches("-1"))
      {
        HashMap localHashMap = (HashMap)LocationResults.this.myBranches.remove(0);
        LocationResults.access$902(LocationResults.this, Double.parseDouble((String)localHashMap.get("latitude")));
        LocationResults.access$1002(LocationResults.this, Double.parseDouble((String)localHashMap.get("longitude")));
      }
      LocationResults.access$1102(LocationResults.this, 0);
      LocationResults.this.mapController.setCenter(new GeoPoint((int)(1000000.0D * LocationResults.this.myLat), (int)(1000000.0D * LocationResults.this.myLong)));
      LocationResults.this.mapController.setZoom(LocationResults.this.currentZoom);
      LocationResults.this.updateResults();
      LocationResults.this.locCurtain.setVisibility(8);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.wf.wellsfargomobile-700220\classes_dex2jar.jar
 * Qualified Name:     com.wf.wellsfargomobile.loc.LocationResults
 * JD-Core Version:    0.6.2
 */