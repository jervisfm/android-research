package com.wf.wellsfargomobile.loc;

import java.util.ArrayList;
import java.util.HashMap;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class LocationXMLHandler extends DefaultHandler
{
  private HashMap<String, String> currentBranch;
  private String currentElementData;
  private String currentKey;
  private boolean isKey;
  private boolean isValue;
  private ArrayList<HashMap<String, String>> myLocations;

  public void characters(char[] paramArrayOfChar, int paramInt1, int paramInt2)
    throws SAXException
  {
    if ((this.isKey) || (this.isValue))
      this.currentElementData += new String(paramArrayOfChar, paramInt1, paramInt2);
  }

  public void endDocument()
    throws SAXException
  {
  }

  public void endElement(String paramString1, String paramString2, String paramString3)
    throws SAXException
  {
    if (paramString2.equals("dict"))
      this.myLocations.add(this.currentBranch);
    while (true)
    {
      this.isKey = false;
      this.isValue = false;
      return;
      if (this.isKey)
      {
        this.currentElementData = this.currentElementData.replaceAll("(\\r|\\n)", "").trim();
        this.currentKey = this.currentElementData;
      }
      else if (this.isValue)
      {
        this.currentElementData = this.currentElementData.replaceAll("(\\r|\\n)", "").trim();
        this.currentBranch.put(this.currentKey, this.currentElementData);
      }
    }
  }

  public ArrayList<HashMap<String, String>> getParsedLocations()
  {
    return this.myLocations;
  }

  public void startDocument()
    throws SAXException
  {
    this.myLocations = new ArrayList();
    this.isKey = false;
    this.isValue = false;
  }

  public void startElement(String paramString1, String paramString2, String paramString3, Attributes paramAttributes)
    throws SAXException
  {
    if (paramString2.equals("dict"))
      this.currentBranch = new HashMap();
    do
    {
      return;
      if (paramString2.equals("key"))
      {
        this.isKey = true;
        this.currentElementData = "";
        return;
      }
    }
    while ((!paramString2.equals("string")) && (!paramString2.equals("integer")));
    this.isValue = true;
    this.currentElementData = "";
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.wf.wellsfargomobile-700220\classes_dex2jar.jar
 * Qualified Name:     com.wf.wellsfargomobile.loc.LocationXMLHandler
 * JD-Core Version:    0.6.2
 */