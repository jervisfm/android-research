package com.wf.wellsfargomobile.mrdc;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.hardware.Camera.Size;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.wf.wellsfargomobile.BaseActivity;
import com.wf.wellsfargomobile.WFApp;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class CapturePhoto extends BaseActivity
  implements SurfaceHolder.Callback, SensorEventListener
{
  private static final int MAX_PREVIEW_PIXELS = 691200;
  private static final int MIN_PREVIEW_PIXELS = 150400;
  private static final String TAG = "CapturePhoto";
  private AutoFocusCallBack autoFocusCallBack = new AutoFocusCallBack(null);
  Camera camera;
  private ImageMode imageMode = null;
  TextView instruction = null;
  private boolean isPictureTaking;
  JavaScriptInterfacePhotoTips jsPhotoTips = null;
  Camera.PictureCallback myPictureCallback_JPG = new Camera.PictureCallback()
  {
    public void onPictureTaken(byte[] paramAnonymousArrayOfByte, Camera paramAnonymousCamera)
    {
      Bitmap localBitmap = BitmapFactory.decodeByteArray(paramAnonymousArrayOfByte, 0, paramAnonymousArrayOfByte.length);
      ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
      localBitmap.compress(Bitmap.CompressFormat.JPEG, 50, localByteArrayOutputStream);
      byte[] arrayOfByte = localByteArrayOutputStream.toByteArray();
      if (CapturePhoto.this.previewSize != null)
        localBitmap = ImageUtilities.resizeImageForWidth(localBitmap, CapturePhoto.this.previewSize.width);
      switch (CapturePhoto.7.$SwitchMap$com$wf$wellsfargomobile$mrdc$ImageMode[CapturePhoto.this.imageMode.ordinal()])
      {
      default:
        Log.d("CapturePhoto", "unrecognized image mode: " + CapturePhoto.this.imageMode);
      case 1:
      case 2:
      }
      while (true)
      {
        Intent localIntent = new Intent(CapturePhoto.this.getBaseContext(), PreviewCheckImage.class);
        localIntent.putExtra("MRDC_FLOW_STATE", CapturePhoto.this.imageMode);
        CapturePhoto.this.startActivityForResult(localIntent, 3);
        CapturePhoto.access$402(CapturePhoto.this, false);
        return;
        CapturePhoto.this.wfApp.setFrontCheckImage(arrayOfByte);
        CapturePhoto.this.wfApp.setFrontCheckPreviewImage(localBitmap);
        continue;
        CapturePhoto.this.wfApp.setRearCheckImage(arrayOfByte);
        CapturePhoto.this.wfApp.setRearCheckPreviewImage(localBitmap);
      }
    }
  };
  Camera.PictureCallback myPictureCallback_RAW = new Camera.PictureCallback()
  {
    public void onPictureTaken(byte[] paramAnonymousArrayOfByte, Camera paramAnonymousCamera)
    {
    }
  };
  Camera.ShutterCallback myShutterCallback = new Camera.ShutterCallback()
  {
    public void onShutter()
    {
    }
  };
  ImageView orientationWarning = null;
  private Camera.Size previewSize;
  boolean previewing = false;
  private SensorManager sensorManager = null;
  SurfaceHolder surfaceHolder;
  SurfaceView surfaceView;
  private ImageButton takePhotoButton;
  private WFApp wfApp;

  private void doSetTorch(Camera.Parameters paramParameters, boolean paramBoolean)
  {
    if (paramBoolean);
    for (String str = findSettableValue(paramParameters.getSupportedFlashModes(), new String[] { "torch", "on" }); ; str = findSettableValue(paramParameters.getSupportedFlashModes(), new String[] { "off" }))
    {
      if (str != null)
        paramParameters.setFlashMode(str);
      return;
    }
  }

  private String findSettableValue(Collection<String> paramCollection, String[] paramArrayOfString)
  {
    Object localObject = null;
    int i;
    if (paramCollection != null)
      i = paramArrayOfString.length;
    for (int j = 0; ; j++)
    {
      localObject = null;
      if (j < i)
      {
        String str = paramArrayOfString[j];
        if (paramCollection.contains(str))
          localObject = str;
      }
      else
      {
        return localObject;
      }
    }
  }

  private Camera.Size getBestSupportedSize(int paramInt, Camera.Parameters paramParameters)
  {
    List localList = paramParameters.getSupportedPictureSizes();
    Object localObject = null;
    if (localList != null)
    {
      Iterator localIterator = localList.iterator();
      while (localIterator.hasNext())
      {
        Camera.Size localSize = (Camera.Size)localIterator.next();
        if (localSize.width <= paramInt)
          if (localObject == null)
          {
            localObject = localSize;
          }
          else
          {
            int i = ((Camera.Size)localObject).width * ((Camera.Size)localObject).height;
            if (localSize.width * localSize.height >= i)
              localObject = localSize;
          }
      }
    }
    if (localObject == null)
    {
      Camera localCamera = this.camera;
      localCamera.getClass();
      localObject = new Camera.Size(localCamera, 1600, 1200);
    }
    return localObject;
  }

  protected static Camera.Size getOptimumSupportedPreviewSize(int paramInt1, int paramInt2, int paramInt3, int paramInt4, List<Camera.Size> paramList, Camera.Size paramSize)
  {
    if (paramList == null)
    {
      localObject = null;
      return localObject;
    }
    Collections.sort(paramList, new Comparator()
    {
      public int compare(Camera.Size paramAnonymousSize1, Camera.Size paramAnonymousSize2)
      {
        int i = paramAnonymousSize1.height * paramAnonymousSize1.width;
        int j = paramAnonymousSize2.height * paramAnonymousSize2.width;
        if (j < i)
          return -1;
        if (j > i)
          return 1;
        return 0;
      }
    });
    Object localObject = null;
    float f1 = paramInt2 / paramInt1;
    long l = paramInt2 * paramInt1;
    float f2 = (1.0F / 1.0F);
    Iterator localIterator = paramList.iterator();
    while (true)
    {
      Camera.Size localSize;
      if (localIterator.hasNext())
      {
        localSize = (Camera.Size)localIterator.next();
        if ((localSize.height == paramInt1) && (localSize.width == paramInt2))
          localObject = localSize;
      }
      else
      {
        if (localObject != null)
          break;
        return paramSize;
      }
      if (localSize.width * localSize.height <= l)
      {
        float f3 = Math.abs(localSize.width / localSize.height - f1);
        if (f3 < f2)
        {
          localObject = localSize;
          f2 = f3;
        }
      }
    }
  }

  private void setTorch(Camera paramCamera, boolean paramBoolean)
  {
    Camera.Parameters localParameters = paramCamera.getParameters();
    doSetTorch(localParameters, paramBoolean);
    paramCamera.setParameters(localParameters);
  }

  private void setTorch(boolean paramBoolean)
  {
    if (this.camera != null)
    {
      if ("auto".equals(this.camera.getParameters().getFocusMode()))
        this.camera.cancelAutoFocus();
      setTorch(this.camera, paramBoolean);
      if ("auto".equals(this.camera.getParameters().getFocusMode()))
        this.camera.autoFocus(this.autoFocusCallBack);
    }
  }

  private void takePhoto()
  {
    this.takePhotoButton.setEnabled(false);
    if (!this.isPictureTaking)
    {
      this.isPictureTaking = true;
      if ("auto".equals(this.camera.getParameters().getFocusMode()))
        this.camera.autoFocus(this.autoFocusCallBack);
    }
    else
    {
      return;
    }
    this.camera.takePicture(this.myShutterCallback, this.myPictureCallback_RAW, this.myPictureCallback_JPG);
  }

  public void onAccuracyChanged(Sensor paramSensor, int paramInt)
  {
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    switch (paramInt2)
    {
    default:
      return;
    case 10:
      setResult(10, new Intent());
      finish();
      return;
    case 11:
    }
    setResult(11, new Intent());
    finish();
  }

  public void onCancel(View paramView)
  {
    setResult(10, new Intent());
    finish();
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903040);
    setRequestedOrientation(0);
    getWindow().setFormat(0);
    this.surfaceView = ((SurfaceView)findViewById(2131099649));
    this.surfaceHolder = this.surfaceView.getHolder();
    this.surfaceHolder.addCallback(this);
    this.surfaceHolder.setType(3);
    this.sensorManager = ((SensorManager)getSystemService("sensor"));
    this.orientationWarning = ((ImageView)findViewById(2131099663));
    this.instruction = ((TextView)findViewById(2131099657));
    Serializable localSerializable = getIntent().getSerializableExtra("MRDC_FLOW_STATE");
    if (localSerializable == null)
    {
      this.imageMode = ImageMode.FRONT;
      this.wfApp = ((WFApp)getApplication());
      switch (7.$SwitchMap$com$wf$wellsfargomobile$mrdc$ImageMode[this.imageMode.ordinal()])
      {
      default:
      case 1:
      case 2:
      }
    }
    while (true)
    {
      if (this.wfApp.isErrorOpeningCamera())
        runOnUiThread(new Runnable()
        {
          public void run()
          {
            AlertDialog.Builder localBuilder = new AlertDialog.Builder(CapturePhoto.this);
            localBuilder.setMessage(CapturePhoto.this.getString(2131165204)).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener()
            {
              public void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int)
              {
                CapturePhoto.this.finish();
              }
            });
            localBuilder.create().show();
          }
        });
      this.takePhotoButton = ((ImageButton)findViewById(2131099655));
      return;
      this.imageMode = ((ImageMode)localSerializable);
      break;
      if (!this.wfApp.isErrorOpeningCamera())
      {
        runOnUiThread(new Runnable()
        {
          public void run()
          {
            if (!CapturePhoto.this.getSharedPreferences("WF_PREFERENCES", 0).getBoolean("PHOTO_TIPS_DONT_SHOW", false))
            {
              if (CapturePhoto.this.jsPhotoTips == null)
                CapturePhoto.this.jsPhotoTips = new JavaScriptInterfacePhotoTips(CapturePhoto.this, CapturePhoto.this.wfApp);
              CapturePhoto.this.jsPhotoTips.openPhotoTips(CapturePhoto.this.wfApp.getNonce(), false);
            }
          }
        });
        continue;
        this.instruction.setText(getString(2131165283));
      }
    }
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    switch (paramInt)
    {
    case 26:
    default:
      return super.onKeyDown(paramInt, paramKeyEvent);
    case 27:
      takePhoto();
      return true;
    case 25:
      setTorch(false);
      return true;
    case 24:
    }
    setTorch(true);
    return true;
  }

  protected void onResume()
  {
    super.onResume();
    this.takePhotoButton.setEnabled(true);
    this.sensorManager.registerListener(this, this.sensorManager.getDefaultSensor(3), 3);
  }

  public void onSensorChanged(SensorEvent paramSensorEvent)
  {
    Float localFloat1 = Float.valueOf(0.0F);
    Float localFloat2 = Float.valueOf(0.0F);
    if (paramSensorEvent.sensor.getType() == 3)
    {
      localFloat1 = Float.valueOf(paramSensorEvent.values[1]);
      localFloat2 = Float.valueOf(paramSensorEvent.values[2]);
    }
    if (this.orientationWarning != null);
    try
    {
      if (paramSensorEvent.sensor.getType() == 3)
      {
        localFloat1 = Float.valueOf(paramSensorEvent.values[1]);
        localFloat2 = Float.valueOf(paramSensorEvent.values[2]);
      }
      if (localFloat2.floatValue() <= -10.0D)
      {
        this.orientationWarning.setImageResource(2130837543);
        return;
      }
    }
    finally
    {
    }
    if (localFloat2.floatValue() >= 10.0D)
    {
      this.orientationWarning.setImageResource(2130837544);
      return;
    }
    if (localFloat1.floatValue() <= -10.0D)
    {
      this.orientationWarning.setImageResource(2130837546);
      return;
    }
    if (localFloat1.floatValue() >= 10.0D)
    {
      this.orientationWarning.setImageResource(2130837545);
      return;
    }
    this.orientationWarning.setImageResource(2130837511);
  }

  protected void onStart()
  {
    super.onStart();
  }

  protected void onStop()
  {
    this.sensorManager.unregisterListener(this);
    super.onStop();
  }

  public void onTakePhoto(View paramView)
  {
    takePhoto();
  }

  public void openPhotoTips(View paramView)
  {
    new JavaScriptInterfacePhotoTips(paramView.getContext(), this.wfApp).openPhotoTips(this.wfApp.getNonce());
  }

  public void surfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3)
  {
    if (this.previewing)
    {
      this.camera.stopPreview();
      this.previewing = false;
    }
    if (this.camera != null);
    try
    {
      Camera.Parameters localParameters = this.camera.getParameters();
      Display localDisplay = getWindowManager().getDefaultDisplay();
      int i = Math.min(localDisplay.getHeight(), localDisplay.getWidth());
      int j = Math.max(localDisplay.getHeight(), localDisplay.getWidth());
      Camera.Size localSize = getBestSupportedSize(1600, localParameters);
      if (localSize != null)
        localParameters.setPictureSize(localSize.width, localSize.height);
      if (localSize != null)
        this.previewSize = getOptimumSupportedPreviewSize(i, j, localSize.height, localSize.width, localParameters.getSupportedPreviewSizes(), localParameters.getPreviewSize());
      if (this.previewSize != null)
        localParameters.setPreviewSize(this.previewSize.width, this.previewSize.height);
      localParameters.setJpegQuality(100);
      localParameters.setPictureFormat(256);
      String str = findSettableValue(localParameters.getSupportedFocusModes(), new String[] { "continuous-picture", "auto", "macro" });
      if (str != null)
        localParameters.setFocusMode(str);
      this.camera.setParameters(localParameters);
      this.camera.setPreviewDisplay(this.surfaceHolder);
      this.camera.startPreview();
      this.previewing = true;
      return;
    }
    catch (IOException localIOException)
    {
    }
  }

  public void surfaceClick(View paramView)
  {
    this.isPictureTaking = false;
    this.camera.autoFocus(this.autoFocusCallBack);
  }

  public void surfaceCreated(SurfaceHolder paramSurfaceHolder)
  {
    try
    {
      this.camera = Camera.open();
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      this.wfApp.setErrorOpeningCamera(true);
    }
  }

  public void surfaceDestroyed(SurfaceHolder paramSurfaceHolder)
  {
    if (this.camera != null)
    {
      this.camera.stopPreview();
      this.camera.setPreviewCallback(null);
      this.camera.release();
    }
    this.camera = null;
    this.previewing = false;
  }

  private class AutoFocusCallBack
    implements Camera.AutoFocusCallback
  {
    private AutoFocusCallBack()
    {
    }

    public void onAutoFocus(boolean paramBoolean, Camera paramCamera)
    {
      if (CapturePhoto.this.isPictureTaking)
      {
        paramCamera.takePicture(CapturePhoto.this.myShutterCallback, CapturePhoto.this.myPictureCallback_RAW, CapturePhoto.this.myPictureCallback_JPG);
        CapturePhoto.access$402(CapturePhoto.this, false);
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.wf.wellsfargomobile-700220\classes_dex2jar.jar
 * Qualified Name:     com.wf.wellsfargomobile.mrdc.CapturePhoto
 * JD-Core Version:    0.6.2
 */