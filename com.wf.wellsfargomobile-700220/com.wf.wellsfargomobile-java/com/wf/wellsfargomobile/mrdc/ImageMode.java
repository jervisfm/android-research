package com.wf.wellsfargomobile.mrdc;

public enum ImageMode
{
  static
  {
    ImageMode[] arrayOfImageMode = new ImageMode[2];
    arrayOfImageMode[0] = FRONT;
    arrayOfImageMode[1] = REAR;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.wf.wellsfargomobile-700220\classes_dex2jar.jar
 * Qualified Name:     com.wf.wellsfargomobile.mrdc.ImageMode
 * JD-Core Version:    0.6.2
 */