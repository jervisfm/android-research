package com.wf.wellsfargomobile.mrdc;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Matrix;
import android.util.Log;
import java.io.ByteArrayOutputStream;

public class ImageUtilities
{
  private static final String TAG = "ImageUtilities";

  public static byte[] bitmapToByteArray(Bitmap paramBitmap, int paramInt)
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    paramBitmap.compress(Bitmap.CompressFormat.JPEG, paramInt, localByteArrayOutputStream);
    return localByteArrayOutputStream.toByteArray();
  }

  public static byte[] resizeImage(Bitmap paramBitmap, int paramInt1, int paramInt2)
  {
    int i = paramBitmap.getWidth();
    int j = paramBitmap.getHeight();
    if (i > j);
    for (int k = i; ; k = j)
    {
      float f = k / paramInt1;
      Matrix localMatrix = new Matrix();
      localMatrix.postScale(f, f);
      return bitmapToByteArray(Bitmap.createBitmap(paramBitmap, 0, 0, i, j, localMatrix, true), paramInt2);
    }
  }

  public static byte[] resizeImage(Bitmap paramBitmap, boolean paramBoolean)
  {
    if (paramBoolean)
      return resizeImage(paramBitmap, 1600, 50);
    return resizeImage(paramBitmap, 1600, 50);
  }

  public static Bitmap resizeImageForHeight(Bitmap paramBitmap, int paramInt)
  {
    int i;
    if (paramBitmap.getHeight() != paramInt)
    {
      double d = paramInt / paramBitmap.getHeight();
      i = (int)Math.floor(d * paramBitmap.getWidth());
      Log.d("ImageUtilities", "scale to size - scale: " + d + " height: " + paramInt + " width: " + i);
    }
    try
    {
      Bitmap localBitmap = Bitmap.createScaledBitmap(paramBitmap, i, paramInt, false);
      paramBitmap = localBitmap;
      return paramBitmap;
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
      Log.d("ImageUtilities", "OutOfMemoryError occured while resizing image");
    }
    return paramBitmap;
  }

  public static Bitmap resizeImageForWidth(Bitmap paramBitmap, int paramInt)
  {
    int i;
    if (paramBitmap.getWidth() != paramInt)
    {
      double d = paramInt / paramBitmap.getWidth();
      i = (int)Math.floor(d * paramBitmap.getHeight());
      Log.d("ImageUtilities", "scale to size - scale: " + d + " height: " + i + " width: " + paramInt);
    }
    try
    {
      Bitmap localBitmap = Bitmap.createScaledBitmap(paramBitmap, paramInt, i, false);
      paramBitmap = localBitmap;
      return paramBitmap;
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
      Log.d("ImageUtilities", "OutOfMemoryError occured while resizing image");
    }
    return paramBitmap;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.wf.wellsfargomobile-700220\classes_dex2jar.jar
 * Qualified Name:     com.wf.wellsfargomobile.mrdc.ImageUtilities
 * JD-Core Version:    0.6.2
 */