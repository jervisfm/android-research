package com.wf.wellsfargomobile.mrdc;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import com.wf.wellsfargomobile.WFApp;

public class JavaScriptInterfacePhotoTips
{
  private static final String TAG = "JavaScriptInterfacePhotoTips";
  private Dialog d;
  private final Context mContext;
  private WFApp wfApp;

  public JavaScriptInterfacePhotoTips(Context paramContext, WFApp paramWFApp)
  {
    this.mContext = paramContext;
    this.wfApp = paramWFApp;
  }

  private boolean validNonce(String paramString)
  {
    if ((paramString == null) || (this.wfApp.getNonce() == null));
    while (!paramString.equals(this.wfApp.getNonce()))
      return false;
    return true;
  }

  public void closeDialog(View paramView)
  {
    closeDialog(this.wfApp.getNonce());
  }

  public void closeDialog(String paramString)
  {
    if (!validNonce(paramString))
    {
      Log.d("JavaScriptInterfacePhotoTips", "closeDialog() - nonce not valid: " + paramString);
      return;
    }
    this.d.dismiss();
  }

  public void closeDialogDontShow(View paramView)
  {
    closeDialogDontShow(this.wfApp.getNonce());
  }

  public void closeDialogDontShow(String paramString)
  {
    if (!validNonce(paramString))
    {
      Log.d("JavaScriptInterfacePhotoTips", "closeDialogDontShow() - nonce not valid: " + paramString);
      return;
    }
    SharedPreferences.Editor localEditor = this.mContext.getSharedPreferences("WF_PREFERENCES", 0).edit();
    localEditor.putBoolean("PHOTO_TIPS_DONT_SHOW", true);
    localEditor.commit();
    this.d.dismiss();
  }

  public void openPhotoTips(String paramString)
  {
    openPhotoTips(paramString, true);
  }

  public void openPhotoTips(String paramString, boolean paramBoolean)
  {
    if (!validNonce(paramString))
    {
      Log.d("JavaScriptInterfacePhotoTips", "openPhotoTips() - nonce not valid");
      return;
    }
    this.d = new Dialog(this.mContext, 2131296257);
    this.d.getWindow().setFlags(4, 4);
    this.d.setTitle(this.mContext.getString(2131165261));
    this.d.setContentView(2130903047);
    ((Button)this.d.findViewById(2131099747)).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        JavaScriptInterfacePhotoTips.this.closeDialog(JavaScriptInterfacePhotoTips.this.wfApp.getNonce());
      }
    });
    Button localButton = (Button)this.d.findViewById(2131099746);
    if (paramBoolean)
      localButton.setVisibility(8);
    while (true)
    {
      this.d.show();
      return;
      localButton.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          JavaScriptInterfacePhotoTips.this.closeDialogDontShow(JavaScriptInterfacePhotoTips.this.wfApp.getNonce());
        }
      });
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.wf.wellsfargomobile-700220\classes_dex2jar.jar
 * Qualified Name:     com.wf.wellsfargomobile.mrdc.JavaScriptInterfacePhotoTips
 * JD-Core Version:    0.6.2
 */