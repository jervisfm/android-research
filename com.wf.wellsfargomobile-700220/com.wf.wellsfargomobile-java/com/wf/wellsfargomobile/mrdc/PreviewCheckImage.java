package com.wf.wellsfargomobile.mrdc;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.wf.wellsfargomobile.BaseActivity;
import com.wf.wellsfargomobile.WFApp;
import java.io.Serializable;

public class PreviewCheckImage extends BaseActivity
{
  private final String TAG = "PreviewCheckImage";
  private ImageMode imageMode = null;
  private Button useButton;
  private WFApp wfApp;

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    Log.d("PreviewCheckImage", "onActivityResult - imageMode: " + this.imageMode + " result code: " + paramInt2);
    Log.d("PreviewCheckImage", "PreviewCheckImage.onActivityResult - imageMode: " + this.imageMode + " result code: " + paramInt2);
    switch (paramInt2)
    {
    default:
    case 10:
    case 11:
    }
    while (true)
    {
      Log.d("PreviewCheckImage", "onActivityResult - unrecognized result code: " + paramInt2);
      Log.d("PreviewCheckImage", "PreviewCheckImage.onActivityResult - unrecognized result code: " + paramInt2);
      return;
      Log.d("PreviewCheckImage", "onActivityResult - cancelling MRDC flow - imageMode: " + this.imageMode);
      Log.d("PreviewCheckImage", "PreviewCheckImage.onActivityResult - cancelling MRDC flow - imageMode: " + this.imageMode);
      setResult(10, new Intent());
      finish();
      Log.d("PreviewCheckImage", "onActivityResult - completing MRDC flow - imageMode: " + this.imageMode);
      Log.d("PreviewCheckImage", "PreviewCheckImage.onActivityResult - completing MRDC flow - imageMode: " + this.imageMode);
      setResult(11, new Intent());
      finish();
    }
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Log.d("PreviewCheckImage", "PreviewCheckImage.onCreate");
    setContentView(2130903048);
    setRequestedOrientation(0);
    this.wfApp = ((WFApp)getApplication());
    Serializable localSerializable = getIntent().getSerializableExtra("MRDC_FLOW_STATE");
    ImageView localImageView;
    if (localSerializable == null)
    {
      this.imageMode = ImageMode.FRONT;
      localImageView = (ImageView)findViewById(2131099749);
      switch (1.$SwitchMap$com$wf$wellsfargomobile$mrdc$ImageMode[this.imageMode.ordinal()])
      {
      default:
      case 1:
      case 2:
      }
    }
    while (true)
    {
      this.useButton = ((Button)findViewById(2131099752));
      this.useButton.setEnabled(true);
      return;
      this.imageMode = ((ImageMode)localSerializable);
      break;
      Log.d("PreviewCheckImage", "getting FRONT image for display");
      Bitmap localBitmap2 = this.wfApp.getFrontCheckPreviewImage();
      if (localBitmap2 != null)
      {
        localImageView.setImageBitmap(localBitmap2);
        continue;
        Log.d("PreviewCheckImage", "getting REAR image for display");
        Bitmap localBitmap1 = this.wfApp.getRearCheckPreviewImage();
        if (localBitmap1 != null)
          localImageView.setImageBitmap(localBitmap1);
        ((TextView)findViewById(2131099657)).setText(getString(2131165273));
      }
    }
  }

  public void onRetakePicture(View paramView)
  {
    Log.d("PreviewCheckImage", "onRetakePicture - imageMode: " + this.imageMode);
    setResult(12);
    finish();
  }

  public void onUsePicture(View paramView)
  {
    this.useButton.setEnabled(false);
    switch (1.$SwitchMap$com$wf$wellsfargomobile$mrdc$ImageMode[this.imageMode.ordinal()])
    {
    default:
      Log.d("PreviewCheckImage", "PreviewCheckImage.onUsePicture unrecognized image mode: " + this.imageMode);
      Log.d("PreviewCheckImage", "unrecognized image mode: " + this.imageMode);
      return;
    case 1:
      Log.d("PreviewCheckImage", "PreviewCheckImage.onUsePicture launching CapturePhoto image mode: " + this.imageMode);
      new SubmitFrontImageThread(this).execute(new Void[0]);
      Intent localIntent = new Intent(getBaseContext(), CapturePhoto.class);
      localIntent.putExtra("MRDC_FLOW_STATE", ImageMode.REAR);
      startActivityForResult(localIntent, 1);
      return;
    case 2:
    }
    Log.d("PreviewCheckImage", "PreviewCheckImage.onUsePicture sending flow complete image mode: " + this.imageMode);
    setResult(11, new Intent());
    finish();
  }

  public void openPhotoTips(View paramView)
  {
    new JavaScriptInterfacePhotoTips(paramView.getContext(), this.wfApp).openPhotoTips(this.wfApp.getNonce());
  }

  private class SubmitFrontImageThread extends AsyncTask<Void, Void, Void>
  {
    private String TAG = "SubmitFrontImageThread";
    private Context aContext;

    public SubmitFrontImageThread(Context arg2)
    {
      Object localObject;
      this.aContext = localObject;
    }

    // ERROR //
    protected Void doInBackground(Void[] paramArrayOfVoid)
    {
      // Byte code:
      //   0: aload_0
      //   1: getfield 15	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:this$0	Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
      //   4: invokestatic 49	com/wf/wellsfargomobile/mrdc/PreviewCheckImage:access$000	(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
      //   7: invokestatic 55	java/lang/System:currentTimeMillis	()J
      //   10: invokevirtual 61	com/wf/wellsfargomobile/WFApp:setPerfD1FrontImageStartSend	(J)V
      //   13: iconst_1
      //   14: istore_2
      //   15: aconst_null
      //   16: astore_3
      //   17: aload_0
      //   18: getfield 15	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:this$0	Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
      //   21: invokestatic 49	com/wf/wellsfargomobile/mrdc/PreviewCheckImage:access$000	(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
      //   24: invokevirtual 65	com/wf/wellsfargomobile/WFApp:getFrontCheckImage	()[B
      //   27: astore 11
      //   29: aload 11
      //   31: ifnonnull +97 -> 128
      //   34: aload_0
      //   35: getfield 22	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:TAG	Ljava/lang/String;
      //   38: new 67	java/lang/StringBuilder
      //   41: dup
      //   42: invokespecial 68	java/lang/StringBuilder:<init>	()V
      //   45: ldc 70
      //   47: invokevirtual 74	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   50: aload 11
      //   52: invokevirtual 77	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   55: invokevirtual 81	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   58: invokestatic 87	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
      //   61: pop
      //   62: aload_0
      //   63: getfield 15	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:this$0	Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
      //   66: invokestatic 49	com/wf/wellsfargomobile/mrdc/PreviewCheckImage:access$000	(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
      //   69: aconst_null
      //   70: invokevirtual 91	com/wf/wellsfargomobile/WFApp:setFrontImageToken	(Ljava/lang/String;)V
      //   73: iconst_0
      //   74: ifeq +19 -> 93
      //   77: aconst_null
      //   78: invokevirtual 97	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   81: ifnull +12 -> 93
      //   84: aconst_null
      //   85: invokevirtual 97	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   88: invokeinterface 102 1 0
      //   93: aload_0
      //   94: getfield 15	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:this$0	Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
      //   97: invokestatic 49	com/wf/wellsfargomobile/mrdc/PreviewCheckImage:access$000	(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
      //   100: iconst_1
      //   101: invokevirtual 106	com/wf/wellsfargomobile/WFApp:setFrontImageUploadProcessComplete	(Z)V
      //   104: aload_0
      //   105: getfield 15	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:this$0	Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
      //   108: invokestatic 49	com/wf/wellsfargomobile/mrdc/PreviewCheckImage:access$000	(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
      //   111: aconst_null
      //   112: invokevirtual 110	com/wf/wellsfargomobile/WFApp:setFrontCheckImage	([B)V
      //   115: aload_0
      //   116: getfield 15	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:this$0	Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
      //   119: invokestatic 49	com/wf/wellsfargomobile/mrdc/PreviewCheckImage:access$000	(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
      //   122: aconst_null
      //   123: invokevirtual 114	com/wf/wellsfargomobile/WFApp:setFrontCheckPreviewImage	(Landroid/graphics/Bitmap;)V
      //   126: aconst_null
      //   127: areturn
      //   128: aload_0
      //   129: getfield 24	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:aContext	Landroid/content/Context;
      //   132: invokestatic 120	android/webkit/CookieSyncManager:createInstance	(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;
      //   135: pop
      //   136: invokestatic 124	android/webkit/CookieSyncManager:getInstance	()Landroid/webkit/CookieSyncManager;
      //   139: invokevirtual 127	android/webkit/CookieSyncManager:sync	()V
      //   142: invokestatic 132	android/webkit/CookieManager:getInstance	()Landroid/webkit/CookieManager;
      //   145: aload_0
      //   146: getfield 24	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:aContext	Landroid/content/Context;
      //   149: ldc 133
      //   151: invokevirtual 139	android/content/Context:getString	(I)Ljava/lang/String;
      //   154: invokevirtual 143	android/webkit/CookieManager:getCookie	(Ljava/lang/String;)Ljava/lang/String;
      //   157: astore 14
      //   159: aload_0
      //   160: getfield 22	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:TAG	Ljava/lang/String;
      //   163: new 67	java/lang/StringBuilder
      //   166: dup
      //   167: invokespecial 68	java/lang/StringBuilder:<init>	()V
      //   170: ldc 145
      //   172: invokevirtual 74	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   175: aload 14
      //   177: invokevirtual 74	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   180: invokevirtual 81	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   183: invokestatic 87	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
      //   186: pop
      //   187: new 93	org/apache/http/impl/client/DefaultHttpClient
      //   190: dup
      //   191: invokespecial 146	org/apache/http/impl/client/DefaultHttpClient:<init>	()V
      //   194: astore 16
      //   196: aload 16
      //   198: invokevirtual 150	org/apache/http/impl/client/DefaultHttpClient:getParams	()Lorg/apache/http/params/HttpParams;
      //   201: astore 22
      //   203: aload 22
      //   205: ldc 152
      //   207: ldc2_w 153
      //   210: invokeinterface 160 4 0
      //   215: pop2
      //   216: aload 22
      //   218: ldc 162
      //   220: ldc2_w 153
      //   223: invokeinterface 160 4 0
      //   228: pop2
      //   229: aload 16
      //   231: aload 22
      //   233: invokevirtual 166	org/apache/http/impl/client/DefaultHttpClient:setParams	(Lorg/apache/http/params/HttpParams;)V
      //   236: new 168	org/apache/http/client/methods/HttpPost
      //   239: dup
      //   240: new 67	java/lang/StringBuilder
      //   243: dup
      //   244: invokespecial 68	java/lang/StringBuilder:<init>	()V
      //   247: aload_0
      //   248: getfield 24	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:aContext	Landroid/content/Context;
      //   251: ldc 133
      //   253: invokevirtual 139	android/content/Context:getString	(I)Ljava/lang/String;
      //   256: invokevirtual 74	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   259: ldc 170
      //   261: invokevirtual 74	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   264: invokevirtual 81	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   267: invokespecial 172	org/apache/http/client/methods/HttpPost:<init>	(Ljava/lang/String;)V
      //   270: astore 27
      //   272: aload 27
      //   274: ldc 174
      //   276: aload 14
      //   278: invokevirtual 178	org/apache/http/client/methods/HttpPost:setHeader	(Ljava/lang/String;Ljava/lang/String;)V
      //   281: aload_0
      //   282: getfield 22	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:TAG	Ljava/lang/String;
      //   285: new 67	java/lang/StringBuilder
      //   288: dup
      //   289: invokespecial 68	java/lang/StringBuilder:<init>	()V
      //   292: ldc 180
      //   294: invokevirtual 74	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   297: aload 11
      //   299: arraylength
      //   300: invokevirtual 183	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
      //   303: ldc 185
      //   305: invokevirtual 74	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   308: invokevirtual 81	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   311: invokestatic 87	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
      //   314: pop
      //   315: new 187	org/apache/http/entity/mime/MultipartEntity
      //   318: dup
      //   319: invokespecial 188	org/apache/http/entity/mime/MultipartEntity:<init>	()V
      //   322: astore 29
      //   324: new 190	org/apache/http/entity/mime/content/ByteArrayBody
      //   327: dup
      //   328: aload 11
      //   330: ldc 192
      //   332: ldc 194
      //   334: invokespecial 197	org/apache/http/entity/mime/content/ByteArrayBody:<init>	([BLjava/lang/String;Ljava/lang/String;)V
      //   337: astore 30
      //   339: aload 29
      //   341: ldc 199
      //   343: aload 30
      //   345: invokevirtual 203	org/apache/http/entity/mime/MultipartEntity:addPart	(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V
      //   348: aload 29
      //   350: ldc 205
      //   352: new 207	org/apache/http/entity/mime/content/StringBody
      //   355: dup
      //   356: aload_0
      //   357: getfield 24	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:aContext	Landroid/content/Context;
      //   360: ldc 208
      //   362: invokevirtual 139	android/content/Context:getString	(I)Ljava/lang/String;
      //   365: ldc 210
      //   367: invokestatic 216	java/nio/charset/Charset:forName	(Ljava/lang/String;)Ljava/nio/charset/Charset;
      //   370: invokespecial 219	org/apache/http/entity/mime/content/StringBody:<init>	(Ljava/lang/String;Ljava/nio/charset/Charset;)V
      //   373: invokevirtual 203	org/apache/http/entity/mime/MultipartEntity:addPart	(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V
      //   376: aload 27
      //   378: aload 29
      //   380: invokevirtual 223	org/apache/http/client/methods/HttpPost:setEntity	(Lorg/apache/http/HttpEntity;)V
      //   383: aload 16
      //   385: aload 27
      //   387: invokevirtual 227	org/apache/http/impl/client/DefaultHttpClient:execute	(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
      //   390: astore 31
      //   392: aload_0
      //   393: getfield 15	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:this$0	Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
      //   396: invokestatic 49	com/wf/wellsfargomobile/mrdc/PreviewCheckImage:access$000	(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
      //   399: invokestatic 55	java/lang/System:currentTimeMillis	()J
      //   402: invokevirtual 230	com/wf/wellsfargomobile/WFApp:setPerfD2FrontImageJsonResponse	(J)V
      //   405: aload_0
      //   406: getfield 15	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:this$0	Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
      //   409: invokestatic 49	com/wf/wellsfargomobile/mrdc/PreviewCheckImage:access$000	(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
      //   412: iconst_1
      //   413: invokevirtual 106	com/wf/wellsfargomobile/WFApp:setFrontImageUploadProcessComplete	(Z)V
      //   416: aload 31
      //   418: invokeinterface 236 1 0
      //   423: invokestatic 241	org/apache/http/util/EntityUtils:toString	(Lorg/apache/http/HttpEntity;)Ljava/lang/String;
      //   426: astore 32
      //   428: aload_0
      //   429: getfield 22	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:TAG	Ljava/lang/String;
      //   432: new 67	java/lang/StringBuilder
      //   435: dup
      //   436: invokespecial 68	java/lang/StringBuilder:<init>	()V
      //   439: ldc 243
      //   441: invokevirtual 74	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   444: aload 32
      //   446: invokevirtual 74	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   449: invokevirtual 81	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   452: invokestatic 87	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
      //   455: pop
      //   456: aload 32
      //   458: ldc 245
      //   460: ldc 247
      //   462: invokevirtual 253	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
      //   465: ldc 255
      //   467: ldc 247
      //   469: invokevirtual 253	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
      //   472: astore 34
      //   474: aload_0
      //   475: getfield 22	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:TAG	Ljava/lang/String;
      //   478: new 67	java/lang/StringBuilder
      //   481: dup
      //   482: invokespecial 68	java/lang/StringBuilder:<init>	()V
      //   485: ldc 243
      //   487: invokevirtual 74	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   490: aload 34
      //   492: invokevirtual 74	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   495: invokevirtual 81	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   498: invokestatic 87	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
      //   501: pop
      //   502: new 257	org/json/JSONObject
      //   505: dup
      //   506: aload 34
      //   508: invokespecial 258	org/json/JSONObject:<init>	(Ljava/lang/String;)V
      //   511: astore 36
      //   513: ldc_w 260
      //   516: aload 36
      //   518: ldc_w 262
      //   521: invokevirtual 264	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
      //   524: invokevirtual 268	java/lang/String:equals	(Ljava/lang/Object;)Z
      //   527: istore 39
      //   529: iload 39
      //   531: ifeq +78 -> 609
      //   534: iconst_0
      //   535: istore_2
      //   536: aload 16
      //   538: ifnull +21 -> 559
      //   541: aload 16
      //   543: invokevirtual 97	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   546: ifnull +13 -> 559
      //   549: aload 16
      //   551: invokevirtual 97	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   554: invokeinterface 102 1 0
      //   559: aload_0
      //   560: getfield 15	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:this$0	Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
      //   563: invokestatic 49	com/wf/wellsfargomobile/mrdc/PreviewCheckImage:access$000	(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
      //   566: iconst_1
      //   567: invokevirtual 106	com/wf/wellsfargomobile/WFApp:setFrontImageUploadProcessComplete	(Z)V
      //   570: aload_0
      //   571: getfield 15	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:this$0	Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
      //   574: invokestatic 49	com/wf/wellsfargomobile/mrdc/PreviewCheckImage:access$000	(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
      //   577: aconst_null
      //   578: invokevirtual 110	com/wf/wellsfargomobile/WFApp:setFrontCheckImage	([B)V
      //   581: aload_0
      //   582: getfield 15	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:this$0	Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
      //   585: invokestatic 49	com/wf/wellsfargomobile/mrdc/PreviewCheckImage:access$000	(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
      //   588: aconst_null
      //   589: invokevirtual 114	com/wf/wellsfargomobile/WFApp:setFrontCheckPreviewImage	(Landroid/graphics/Bitmap;)V
      //   592: iload_2
      //   593: ifne +14 -> 607
      //   596: aload_0
      //   597: getfield 22	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:TAG	Ljava/lang/String;
      //   600: ldc_w 270
      //   603: invokestatic 273	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
      //   606: pop
      //   607: aconst_null
      //   608: areturn
      //   609: aload 36
      //   611: ldc_w 275
      //   614: invokevirtual 264	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
      //   617: astore 40
      //   619: aload_0
      //   620: getfield 15	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:this$0	Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
      //   623: invokestatic 49	com/wf/wellsfargomobile/mrdc/PreviewCheckImage:access$000	(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
      //   626: aload 40
      //   628: invokevirtual 91	com/wf/wellsfargomobile/WFApp:setFrontImageToken	(Ljava/lang/String;)V
      //   631: goto -95 -> 536
      //   634: astore 37
      //   636: aload_0
      //   637: getfield 22	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:TAG	Ljava/lang/String;
      //   640: ldc_w 277
      //   643: aload 37
      //   645: invokestatic 280	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   648: pop
      //   649: iconst_0
      //   650: istore_2
      //   651: goto -115 -> 536
      //   654: astore 10
      //   656: aload_3
      //   657: ifnull +19 -> 676
      //   660: aload_3
      //   661: invokevirtual 97	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   664: ifnull +12 -> 676
      //   667: aload_3
      //   668: invokevirtual 97	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   671: invokeinterface 102 1 0
      //   676: aload_0
      //   677: getfield 15	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:this$0	Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
      //   680: invokestatic 49	com/wf/wellsfargomobile/mrdc/PreviewCheckImage:access$000	(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
      //   683: iconst_1
      //   684: invokevirtual 106	com/wf/wellsfargomobile/WFApp:setFrontImageUploadProcessComplete	(Z)V
      //   687: aload_0
      //   688: getfield 15	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:this$0	Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
      //   691: invokestatic 49	com/wf/wellsfargomobile/mrdc/PreviewCheckImage:access$000	(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
      //   694: aconst_null
      //   695: invokevirtual 110	com/wf/wellsfargomobile/WFApp:setFrontCheckImage	([B)V
      //   698: aload_0
      //   699: getfield 15	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:this$0	Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
      //   702: invokestatic 49	com/wf/wellsfargomobile/mrdc/PreviewCheckImage:access$000	(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
      //   705: aconst_null
      //   706: invokevirtual 114	com/wf/wellsfargomobile/WFApp:setFrontCheckPreviewImage	(Landroid/graphics/Bitmap;)V
      //   709: iconst_0
      //   710: istore_2
      //   711: goto -119 -> 592
      //   714: astore 9
      //   716: aload_3
      //   717: ifnull +19 -> 736
      //   720: aload_3
      //   721: invokevirtual 97	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   724: ifnull +12 -> 736
      //   727: aload_3
      //   728: invokevirtual 97	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   731: invokeinterface 102 1 0
      //   736: aload_0
      //   737: getfield 15	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:this$0	Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
      //   740: invokestatic 49	com/wf/wellsfargomobile/mrdc/PreviewCheckImage:access$000	(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
      //   743: iconst_1
      //   744: invokevirtual 106	com/wf/wellsfargomobile/WFApp:setFrontImageUploadProcessComplete	(Z)V
      //   747: aload_0
      //   748: getfield 15	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:this$0	Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
      //   751: invokestatic 49	com/wf/wellsfargomobile/mrdc/PreviewCheckImage:access$000	(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
      //   754: aconst_null
      //   755: invokevirtual 110	com/wf/wellsfargomobile/WFApp:setFrontCheckImage	([B)V
      //   758: aload_0
      //   759: getfield 15	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:this$0	Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
      //   762: invokestatic 49	com/wf/wellsfargomobile/mrdc/PreviewCheckImage:access$000	(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
      //   765: aconst_null
      //   766: invokevirtual 114	com/wf/wellsfargomobile/WFApp:setFrontCheckPreviewImage	(Landroid/graphics/Bitmap;)V
      //   769: iconst_0
      //   770: istore_2
      //   771: goto -179 -> 592
      //   774: astore 8
      //   776: aload_3
      //   777: ifnull +19 -> 796
      //   780: aload_3
      //   781: invokevirtual 97	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   784: ifnull +12 -> 796
      //   787: aload_3
      //   788: invokevirtual 97	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   791: invokeinterface 102 1 0
      //   796: aload_0
      //   797: getfield 15	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:this$0	Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
      //   800: invokestatic 49	com/wf/wellsfargomobile/mrdc/PreviewCheckImage:access$000	(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
      //   803: iconst_1
      //   804: invokevirtual 106	com/wf/wellsfargomobile/WFApp:setFrontImageUploadProcessComplete	(Z)V
      //   807: aload_0
      //   808: getfield 15	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:this$0	Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
      //   811: invokestatic 49	com/wf/wellsfargomobile/mrdc/PreviewCheckImage:access$000	(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
      //   814: aconst_null
      //   815: invokevirtual 110	com/wf/wellsfargomobile/WFApp:setFrontCheckImage	([B)V
      //   818: aload_0
      //   819: getfield 15	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:this$0	Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
      //   822: invokestatic 49	com/wf/wellsfargomobile/mrdc/PreviewCheckImage:access$000	(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
      //   825: aconst_null
      //   826: invokevirtual 114	com/wf/wellsfargomobile/WFApp:setFrontCheckPreviewImage	(Landroid/graphics/Bitmap;)V
      //   829: iconst_0
      //   830: istore_2
      //   831: goto -239 -> 592
      //   834: astore 7
      //   836: aload_3
      //   837: ifnull +19 -> 856
      //   840: aload_3
      //   841: invokevirtual 97	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   844: ifnull +12 -> 856
      //   847: aload_3
      //   848: invokevirtual 97	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   851: invokeinterface 102 1 0
      //   856: aload_0
      //   857: getfield 15	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:this$0	Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
      //   860: invokestatic 49	com/wf/wellsfargomobile/mrdc/PreviewCheckImage:access$000	(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
      //   863: iconst_1
      //   864: invokevirtual 106	com/wf/wellsfargomobile/WFApp:setFrontImageUploadProcessComplete	(Z)V
      //   867: aload_0
      //   868: getfield 15	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:this$0	Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
      //   871: invokestatic 49	com/wf/wellsfargomobile/mrdc/PreviewCheckImage:access$000	(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
      //   874: aconst_null
      //   875: invokevirtual 110	com/wf/wellsfargomobile/WFApp:setFrontCheckImage	([B)V
      //   878: aload_0
      //   879: getfield 15	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:this$0	Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
      //   882: invokestatic 49	com/wf/wellsfargomobile/mrdc/PreviewCheckImage:access$000	(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
      //   885: aconst_null
      //   886: invokevirtual 114	com/wf/wellsfargomobile/WFApp:setFrontCheckPreviewImage	(Landroid/graphics/Bitmap;)V
      //   889: iconst_0
      //   890: istore_2
      //   891: goto -299 -> 592
      //   894: astore 5
      //   896: aload_3
      //   897: ifnull +19 -> 916
      //   900: aload_3
      //   901: invokevirtual 97	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   904: ifnull +12 -> 916
      //   907: aload_3
      //   908: invokevirtual 97	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   911: invokeinterface 102 1 0
      //   916: aload_0
      //   917: getfield 15	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:this$0	Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
      //   920: invokestatic 49	com/wf/wellsfargomobile/mrdc/PreviewCheckImage:access$000	(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
      //   923: iconst_1
      //   924: invokevirtual 106	com/wf/wellsfargomobile/WFApp:setFrontImageUploadProcessComplete	(Z)V
      //   927: aload_0
      //   928: getfield 15	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:this$0	Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
      //   931: invokestatic 49	com/wf/wellsfargomobile/mrdc/PreviewCheckImage:access$000	(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
      //   934: aconst_null
      //   935: invokevirtual 110	com/wf/wellsfargomobile/WFApp:setFrontCheckImage	([B)V
      //   938: aload_0
      //   939: getfield 15	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:this$0	Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
      //   942: invokestatic 49	com/wf/wellsfargomobile/mrdc/PreviewCheckImage:access$000	(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
      //   945: aconst_null
      //   946: invokevirtual 114	com/wf/wellsfargomobile/WFApp:setFrontCheckPreviewImage	(Landroid/graphics/Bitmap;)V
      //   949: iconst_0
      //   950: istore_2
      //   951: goto -359 -> 592
      //   954: astore 4
      //   956: aload_3
      //   957: ifnull +19 -> 976
      //   960: aload_3
      //   961: invokevirtual 97	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   964: ifnull +12 -> 976
      //   967: aload_3
      //   968: invokevirtual 97	org/apache/http/impl/client/DefaultHttpClient:getConnectionManager	()Lorg/apache/http/conn/ClientConnectionManager;
      //   971: invokeinterface 102 1 0
      //   976: aload_0
      //   977: getfield 15	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:this$0	Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
      //   980: invokestatic 49	com/wf/wellsfargomobile/mrdc/PreviewCheckImage:access$000	(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
      //   983: iconst_1
      //   984: invokevirtual 106	com/wf/wellsfargomobile/WFApp:setFrontImageUploadProcessComplete	(Z)V
      //   987: aload_0
      //   988: getfield 15	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:this$0	Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
      //   991: invokestatic 49	com/wf/wellsfargomobile/mrdc/PreviewCheckImage:access$000	(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
      //   994: aconst_null
      //   995: invokevirtual 110	com/wf/wellsfargomobile/WFApp:setFrontCheckImage	([B)V
      //   998: aload_0
      //   999: getfield 15	com/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread:this$0	Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
      //   1002: invokestatic 49	com/wf/wellsfargomobile/mrdc/PreviewCheckImage:access$000	(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
      //   1005: aconst_null
      //   1006: invokevirtual 114	com/wf/wellsfargomobile/WFApp:setFrontCheckPreviewImage	(Landroid/graphics/Bitmap;)V
      //   1009: aload 4
      //   1011: athrow
      //   1012: astore 4
      //   1014: aload 16
      //   1016: astore_3
      //   1017: goto -61 -> 956
      //   1020: astore 21
      //   1022: aload 16
      //   1024: astore_3
      //   1025: goto -129 -> 896
      //   1028: astore 20
      //   1030: aload 16
      //   1032: astore_3
      //   1033: goto -197 -> 836
      //   1036: astore 19
      //   1038: aload 16
      //   1040: astore_3
      //   1041: goto -265 -> 776
      //   1044: astore 18
      //   1046: aload 16
      //   1048: astore_3
      //   1049: goto -333 -> 716
      //   1052: astore 17
      //   1054: aload 16
      //   1056: astore_3
      //   1057: goto -401 -> 656
      //
      // Exception table:
      //   from	to	target	type
      //   502	529	634	org/json/JSONException
      //   609	631	634	org/json/JSONException
      //   17	29	654	java/nio/charset/IllegalCharsetNameException
      //   34	73	654	java/nio/charset/IllegalCharsetNameException
      //   128	196	654	java/nio/charset/IllegalCharsetNameException
      //   17	29	714	java/nio/charset/UnsupportedCharsetException
      //   34	73	714	java/nio/charset/UnsupportedCharsetException
      //   128	196	714	java/nio/charset/UnsupportedCharsetException
      //   17	29	774	java/io/UnsupportedEncodingException
      //   34	73	774	java/io/UnsupportedEncodingException
      //   128	196	774	java/io/UnsupportedEncodingException
      //   17	29	834	org/apache/http/client/ClientProtocolException
      //   34	73	834	org/apache/http/client/ClientProtocolException
      //   128	196	834	org/apache/http/client/ClientProtocolException
      //   17	29	894	java/io/IOException
      //   34	73	894	java/io/IOException
      //   128	196	894	java/io/IOException
      //   17	29	954	finally
      //   34	73	954	finally
      //   128	196	954	finally
      //   196	502	1012	finally
      //   502	529	1012	finally
      //   609	631	1012	finally
      //   636	649	1012	finally
      //   196	502	1020	java/io/IOException
      //   502	529	1020	java/io/IOException
      //   609	631	1020	java/io/IOException
      //   636	649	1020	java/io/IOException
      //   196	502	1028	org/apache/http/client/ClientProtocolException
      //   502	529	1028	org/apache/http/client/ClientProtocolException
      //   609	631	1028	org/apache/http/client/ClientProtocolException
      //   636	649	1028	org/apache/http/client/ClientProtocolException
      //   196	502	1036	java/io/UnsupportedEncodingException
      //   502	529	1036	java/io/UnsupportedEncodingException
      //   609	631	1036	java/io/UnsupportedEncodingException
      //   636	649	1036	java/io/UnsupportedEncodingException
      //   196	502	1044	java/nio/charset/UnsupportedCharsetException
      //   502	529	1044	java/nio/charset/UnsupportedCharsetException
      //   609	631	1044	java/nio/charset/UnsupportedCharsetException
      //   636	649	1044	java/nio/charset/UnsupportedCharsetException
      //   196	502	1052	java/nio/charset/IllegalCharsetNameException
      //   502	529	1052	java/nio/charset/IllegalCharsetNameException
      //   609	631	1052	java/nio/charset/IllegalCharsetNameException
      //   636	649	1052	java/nio/charset/IllegalCharsetNameException
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.wf.wellsfargomobile-700220\classes_dex2jar.jar
 * Qualified Name:     com.wf.wellsfargomobile.mrdc.PreviewCheckImage
 * JD-Core Version:    0.6.2
 */