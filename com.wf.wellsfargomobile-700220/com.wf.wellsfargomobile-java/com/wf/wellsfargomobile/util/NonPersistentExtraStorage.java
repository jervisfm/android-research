package com.wf.wellsfargomobile.util;

import java.util.HashMap;

public class NonPersistentExtraStorage
{
  private static HashMap<String, Object> storage = new HashMap();

  public static Object getExtras(String paramString)
  {
    return storage.get(paramString);
  }

  public static void putExtras(String paramString, Object paramObject)
  {
    storage.put(paramString, paramObject);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.wf.wellsfargomobile-700220\classes_dex2jar.jar
 * Qualified Name:     com.wf.wellsfargomobile.util.NonPersistentExtraStorage
 * JD-Core Version:    0.6.2
 */