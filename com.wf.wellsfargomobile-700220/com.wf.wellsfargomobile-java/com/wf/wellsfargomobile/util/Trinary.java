package com.wf.wellsfargomobile.util;

public enum Trinary
{
  static
  {
    FALSE = new Trinary("FALSE", 1);
    UNKNOWN = new Trinary("UNKNOWN", 2);
    Trinary[] arrayOfTrinary = new Trinary[3];
    arrayOfTrinary[0] = TRUE;
    arrayOfTrinary[1] = FALSE;
    arrayOfTrinary[2] = UNKNOWN;
  }

  public static Trinary parseTrinary(boolean paramBoolean)
  {
    if (paramBoolean)
      return TRUE;
    return FALSE;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.wf.wellsfargomobile-700220\classes_dex2jar.jar
 * Qualified Name:     com.wf.wellsfargomobile.util.Trinary
 * JD-Core Version:    0.6.2
 */