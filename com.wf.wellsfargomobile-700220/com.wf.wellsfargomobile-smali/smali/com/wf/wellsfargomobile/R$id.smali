.class public final Lcom/wf/wellsfargomobile/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wf/wellsfargomobile/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final address:I = 0x7f060023

.field public static final auto_focus:I = 0x7f060000

.field public static final back:I = 0x7f060046

.field public static final build:I = 0x7f060072

.field public static final buttonCancel:I = 0x7f060006

.field public static final buttonPhotoTipsClose:I = 0x7f060063

.field public static final buttonPhotoTipsCloseDontShow:I = 0x7f060062

.field public static final buttonRetake:I = 0x7f060067

.field public static final buttonTakePhoto:I = 0x7f060007

.field public static final buttonUse:I = 0x7f060068

.field public static final cameraGuideBottomLeft:I = 0x7f06000d

.field public static final cameraGuideBottomRight:I = 0x7f06000e

.field public static final cameraGuideTopLeft:I = 0x7f06000b

.field public static final cameraGuideTopRight:I = 0x7f06000c

.field public static final cameraGuides:I = 0x7f06000a

.field public static final camerapreview:I = 0x7f060001

.field public static final capturePhotoFooter:I = 0x7f060008

.field public static final capturePhotoHeader:I = 0x7f060002

.field public static final capturePhotoInstructionLabel:I = 0x7f060009

.field public static final check_preview:I = 0x7f060065

.field public static final city:I = 0x7f060024

.field public static final currentLocButton:I = 0x7f060014

.field public static final depositProgress:I = 0x7f06006a

.field public static final depositProgressBody:I = 0x7f06006b

.field public static final depositProgressDialog:I = 0x7f060069

.field public static final detailsAddress:I = 0x7f060017

.field public static final detailsHours:I = 0x7f06001c

.field public static final detailsIcon:I = 0x7f060018

.field public static final detailsPhone:I = 0x7f06001a

.field public static final detailsServices:I = 0x7f06001e

.field public static final directionsButton:I = 0x7f06001f

.field public static final frameLayout1:I = 0x7f060064

.field public static final hoursContainer:I = 0x7f06001b

.field public static final infoDone:I = 0x7f060075

.field public static final loading:I = 0x7f060011

.field public static final loadingprogress:I = 0x7f060010

.field public static final locPicker:I = 0x7f060013

.field public static final locSearchButton:I = 0x7f060015

.field public static final location:I = 0x7f060012

.field public static final locationCurtain:I = 0x7f060048

.field public static final locationForm:I = 0x7f060020

.field public static final location_details:I = 0x7f060016

.field public static final location_results:I = 0x7f06002a

.field public static final mapContainer:I = 0x7f06002c

.field public static final menu_account:I = 0x7f06007e

.field public static final menu_deposit:I = 0x7f06007f

.field public static final menu_info:I = 0x7f06007d

.field public static final menu_loc:I = 0x7f060082

.field public static final menu_signOff:I = 0x7f060080

.field public static final menu_signOn:I = 0x7f060081

.field public static final more:I = 0x7f060047

.field public static final name:I = 0x7f060071

.field public static final orientationWarningMessage:I = 0x7f06000f

.field public static final password:I = 0x7f060077

.field public static final phoneContainer:I = 0x7f060019

.field public static final photoTips:I = 0x7f060003

.field public static final photoTipsCheckGoodBad:I = 0x7f06005d

.field public static final photoTipsImage:I = 0x7f060004

.field public static final photoTipsText:I = 0x7f060005

.field public static final photoTipsText0:I = 0x7f06005e

.field public static final photoTipsText1:I = 0x7f06005f

.field public static final photoTipsText2:I = 0x7f060060

.field public static final photoTipsText3:I = 0x7f060061

.field public static final privacyPolicy:I = 0x7f06007c

.field public static final quickGuide:I = 0x7f06007b

.field public static final relativeLayout1:I = 0x7f060066

.field public static final release:I = 0x7f060073

.field public static final result0:I = 0x7f060032

.field public static final result1:I = 0x7f060036

.field public static final result2:I = 0x7f06003a

.field public static final result3:I = 0x7f06003e

.field public static final result4:I = 0x7f060042

.field public static final result5:I = 0x7f060049

.field public static final result6:I = 0x7f06004d

.field public static final result7:I = 0x7f060051

.field public static final result8:I = 0x7f060055

.field public static final result9:I = 0x7f060059

.field public static final resultContent0:I = 0x7f060034

.field public static final resultContent1:I = 0x7f060038

.field public static final resultContent2:I = 0x7f06003c

.field public static final resultContent3:I = 0x7f060040

.field public static final resultContent4:I = 0x7f060044

.field public static final resultContent5:I = 0x7f06004b

.field public static final resultContent6:I = 0x7f06004f

.field public static final resultContent7:I = 0x7f060053

.field public static final resultContent8:I = 0x7f060057

.field public static final resultContent9:I = 0x7f06005b

.field public static final resultIcon0:I = 0x7f060035

.field public static final resultIcon1:I = 0x7f060039

.field public static final resultIcon2:I = 0x7f06003d

.field public static final resultIcon3:I = 0x7f060041

.field public static final resultIcon4:I = 0x7f060045

.field public static final resultIcon5:I = 0x7f06004c

.field public static final resultIcon6:I = 0x7f060050

.field public static final resultIcon7:I = 0x7f060054

.field public static final resultIcon8:I = 0x7f060058

.field public static final resultIcon9:I = 0x7f06005c

.field public static final resultLabel0:I = 0x7f060033

.field public static final resultLabel1:I = 0x7f060037

.field public static final resultLabel2:I = 0x7f06003b

.field public static final resultLabel3:I = 0x7f06003f

.field public static final resultLabel4:I = 0x7f060043

.field public static final resultLabel5:I = 0x7f06004a

.field public static final resultLabel6:I = 0x7f06004e

.field public static final resultLabel7:I = 0x7f060052

.field public static final resultLabel8:I = 0x7f060056

.field public static final resultLabel9:I = 0x7f06005a

.field public static final resultMap:I = 0x7f06002d

.field public static final resultNumbers:I = 0x7f060031

.field public static final results_scroller:I = 0x7f06002b

.field public static final searchAddress:I = 0x7f060030

.field public static final securityGuarantee:I = 0x7f060079

.field public static final securityText:I = 0x7f06007a

.field public static final servicesContainer:I = 0x7f06001d

.field public static final signOn:I = 0x7f060078

.field public static final state:I = 0x7f060025

.field public static final stateGo:I = 0x7f060026

.field public static final tog_atm:I = 0x7f060027

.field public static final tog_branch:I = 0x7f060028

.field public static final tog_instore:I = 0x7f060029

.field public static final username:I = 0x7f060076

.field public static final version:I = 0x7f060074

.field public static final webview:I = 0x7f06006c

.field public static final webviewCurtain:I = 0x7f06006f

.field public static final webviewProgressDialog:I = 0x7f06006e

.field public static final webviewloading:I = 0x7f06006d

.field public static final wfMain:I = 0x7f060070

.field public static final zip:I = 0x7f060021

.field public static final zipGo:I = 0x7f060022

.field public static final zoomcontrols:I = 0x7f06002f

.field public static final zoomview:I = 0x7f06002e


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
