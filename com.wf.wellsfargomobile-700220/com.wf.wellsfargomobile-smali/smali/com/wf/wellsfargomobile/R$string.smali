.class public final Lcom/wf/wellsfargomobile/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wf/wellsfargomobile/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final _continue:I = 0x7f07001a

.field public static final address:I = 0x7f070008

.field public static final address_bad:I = 0x7f070009

.field public static final address_or_intersection:I = 0x7f07000a

.field public static final app_name:I = 0x7f07000b

.field public static final atm:I = 0x7f07000c

.field public static final back_to_banking:I = 0x7f07000d

.field public static final branches:I = 0x7f07000e

.field public static final brokerage_url_path:I = 0x7f070002

.field public static final build_date:I = 0x7f07000f

.field public static final camera_error_opening:I = 0x7f070014

.field public static final camera_guide_bottom_left:I = 0x7f070010

.field public static final camera_guide_bottom_right:I = 0x7f070011

.field public static final camera_guide_top_left:I = 0x7f070012

.field public static final camera_guide_top_right:I = 0x7f070013

.field public static final cancel:I = 0x7f070015

.field public static final cd_checkImage:I = 0x7f070016

.field public static final city:I = 0x7f070017

.field public static final close:I = 0x7f070018

.field public static final company:I = 0x7f070019

.field public static final current_location:I = 0x7f07001b

.field public static final deposit:I = 0x7f07001c

.field public static final deposit_image_rear_upload_progress_title:I = 0x7f07001d

.field public static final deposit_progress_body:I = 0x7f07001f

.field public static final deposit_progress_title:I = 0x7f07001e

.field public static final divider:I = 0x7f070020

.field public static final done:I = 0x7f070021

.field public static final error_communications:I = 0x7f070022

.field public static final find_atm:I = 0x7f070023

.field public static final gps_all_off:I = 0x7f070024

.field public static final gps_error_message:I = 0x7f070025

.field public static final gps_error_title:I = 0x7f070026

.field public static final gps_off_message:I = 0x7f070027

.field public static final gps_off_title:I = 0x7f070028

.field public static final gps_timeout_message:I = 0x7f070029

.field public static final gps_timeout_title:I = 0x7f07002a

.field public static final heading_atms_locs:I = 0x7f07002b

.field public static final heading_banner:I = 0x7f07002c

.field public static final heading_location_details:I = 0x7f07002d

.field public static final heading_location_search_narrow:I = 0x7f07002f

.field public static final heading_location_search_results:I = 0x7f07002e

.field public static final heading_mobile_banking:I = 0x7f070030

.field public static final horses:I = 0x7f070032

.field public static final hours:I = 0x7f070031

.field public static final in_store:I = 0x7f070034

.field public static final info:I = 0x7f070033

.field public static final loading:I = 0x7f070035

.field public static final loading_gps:I = 0x7f070036

.field public static final loading_location:I = 0x7f070037

.field public static final loading_login:I = 0x7f070038

.field public static final loc_back:I = 0x7f070039

.field public static final loc_details:I = 0x7f07003a

.field public static final loc_error_message:I = 0x7f07003b

.field public static final loc_error_message_zero_results:I = 0x7f07003c

.field public static final loc_error_title:I = 0x7f07003d

.field public static final loc_more:I = 0x7f07003e

.field public static final loc_or:I = 0x7f07003f

.field public static final loc_results_icon:I = 0x7f070040

.field public static final login_error:I = 0x7f070041

.field public static final map_and_directions:I = 0x7f070042

.field public static final map_key:I = 0x7f070007

.field public static final nav_away:I = 0x7f070043

.field public static final network_error_message:I = 0x7f070044

.field public static final network_error_title:I = 0x7f070045

.field public static final ok:I = 0x7f070046

.field public static final online_password:I = 0x7f070047

.field public static final online_username:I = 0x7f070048

.field public static final orientation_placeholder:I = 0x7f070049

.field public static final password:I = 0x7f07004a

.field public static final password_empty:I = 0x7f07004b

.field public static final phone:I = 0x7f07004c

.field public static final photo_tips:I = 0x7f07004d

.field public static final photo_tips_best_results:I = 0x7f070050

.field public static final photo_tips_check_alignment:I = 0x7f07004e

.field public static final photo_tips_dont_show_again:I = 0x7f07004f

.field public static final photo_tips_tip1:I = 0x7f070051

.field public static final photo_tips_tip2:I = 0x7f070052

.field public static final photo_tips_tip3:I = 0x7f070053

.field public static final privacy_policy_focused:I = 0x7f070054

.field public static final qguide_unfocused:I = 0x7f070055

.field public static final release_date:I = 0x7f070056

.field public static final retake:I = 0x7f070057

.field public static final review_photo_front:I = 0x7f070058

.field public static final review_photo_rear:I = 0x7f070059

.field public static final search:I = 0x7f07005a

.field public static final security_unfocused:I = 0x7f07005b

.field public static final services:I = 0x7f07005c

.field public static final sign_off:I = 0x7f07005f

.field public static final sign_on:I = 0x7f07005d

.field public static final sign_on_error_dialog_title:I = 0x7f07005e

.field public static final state:I = 0x7f070060

.field public static final take_photo:I = 0x7f070061

.field public static final take_photo_front:I = 0x7f070062

.field public static final take_photo_rear:I = 0x7f070063

.field public static final use:I = 0x7f070064

.field public static final username:I = 0x7f070065

.field public static final username_empty:I = 0x7f070066

.field public static final username_error:I = 0x7f070067

.field public static final version:I = 0x7f070068

.field public static final webview_browser_link:I = 0x7f070069

.field public static final wf_appid:I = 0x7f070003

.field public static final wf_info_builddate:I = 0x7f070005

.field public static final wf_info_name:I = 0x7f07006a

.field public static final wf_info_release:I = 0x7f070006

.field public static final wf_info_version:I = 0x7f070004

.field public static final wf_url:I = 0x7f070001

.field public static final wf_url_initial:I = 0x7f070000

.field public static final zip_code:I = 0x7f07006b

.field public static final zip_code_address:I = 0x7f07006c

.field public static final zip_empty:I = 0x7f07006d


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
