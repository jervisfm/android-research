.class public final Lcom/wf/wellsfargomobile/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wf/wellsfargomobile/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final CheckInstructions:I = 0x7f09002a

.field public static final ResultBack:I = 0x7f090027

.field public static final ResultContainer:I = 0x7f09001e

.field public static final ResultContainerBotEven:I = 0x7f090022

.field public static final ResultContainerBotOdd:I = 0x7f090023

.field public static final ResultContainerEven:I = 0x7f09001f

.field public static final ResultContainerOdd:I = 0x7f090020

.field public static final ResultContainerTop:I = 0x7f090021

.field public static final ResultContent:I = 0x7f090025

.field public static final ResultIcon:I = 0x7f090026

.field public static final ResultLabel:I = 0x7f090024

.field public static final ResultMore:I = 0x7f090028

.field public static final Security:I = 0x7f090005

.field public static final SecurityGuaranteeQuickGuide:I = 0x7f09000f

.field public static final WFAddressField:I = 0x7f090015

.field public static final WFButton:I = 0x7f090019

.field public static final WFButtonBlue:I = 0x7f09001a

.field public static final WFButtonGrey:I = 0x7f09001b

.field public static final WFDetailsContent:I = 0x7f09000a

.field public static final WFDetailsContentLabel:I = 0x7f09000b

.field public static final WFDialog:I = 0x7f090001

.field public static final WFDivider:I = 0x7f090029

.field public static final WFField:I = 0x7f090012

.field public static final WFFieldLabel:I = 0x7f090017

.field public static final WFHeader:I = 0x7f090002

.field public static final WFHeaderBevel:I = 0x7f090003

.field public static final WFHeadline:I = 0x7f090008

.field public static final WFHorses:I = 0x7f090004

.field public static final WFLink:I = 0x7f09000e

.field public static final WFLoginField:I = 0x7f090013

.field public static final WFSearchButton:I = 0x7f09001c

.field public static final WFStateField:I = 0x7f090016

.field public static final WFText:I = 0x7f090006

.field public static final WFTextContent:I = 0x7f090009

.field public static final WFTextContentLabel:I = 0x7f09000c

.field public static final WFTextFooter:I = 0x7f09000d

.field public static final WFTextTitle:I = 0x7f090007

.field public static final WFTheme:I = 0x7f090000

.field public static final WFTitleBackground:I = 0x7f090011

.field public static final WFTitleImage:I = 0x7f090010

.field public static final WFTogLabel:I = 0x7f090018

.field public static final WFToggleButton:I = 0x7f09001d

.field public static final WFZipField:I = 0x7f090014


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 357
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
