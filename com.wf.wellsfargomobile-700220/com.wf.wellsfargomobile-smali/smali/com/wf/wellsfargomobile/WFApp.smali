.class public Lcom/wf/wellsfargomobile/WFApp;
.super Landroid/app/Application;
.source "WFApp.java"


# static fields
.field public static final ACCOUNT_LIST_URI:Ljava/lang/String; = "/accounts/g.accountList.do"

.field public static final CONN_TIMEOUT_DATA_SUBMIT:I = 0x2

.field public static final CONN_TIMEOUT_IMAGE_UPLOAD:I = 0x2

.field public static final DEPOSIT_CANCEL_URI:Ljava/lang/String; = "/deposit/depositCancel.action"

.field public static final DEPOSIT_CHECK_URI:Ljava/lang/String; = "/deposit/depositCheck.action"

.field public static final DEPOSIT_DETAIL_URI:Ljava/lang/String; = "/deposit/depositDetail.action"

.field public static final DEPOSIT_ERROR_URI:Ljava/lang/String; = "/deposit/depositDetail.action"

.field public static final DEPOSIT_MITEK_ERROR_URI:Ljava/lang/String; = "/deposit/recoverableMitekError.action"

.field public static final DEPOSIT_RESPONSE_FAILED:Ljava/lang/String; = "Failed"

.field public static final DEPOSIT_RESPONSE_SUCCSSS:Ljava/lang/String; = "Success"

.field public static final IMAGE_UPLOAD_RETRY_LOOP_MAX_SECONDS:I = 0x78

.field public static final IMAGE_UPLOAD_RETRY_LOOP_SLEEP:J = 0x3e8L

.field public static final JAVASCRIPT_INTERFACE_NAME_CAMERA:Ljava/lang/String; = "Camera"

.field public static final JAVASCRIPT_INTERFACE_NAME_COMMON:Ljava/lang/String; = "Common"

.field public static final JAVASCRIPT_INTERFACE_NAME_PHOTO_TIPS:Ljava/lang/String; = "PhotoTips"

.field public static final LOGIN_URI:Ljava/lang/String; = "/signOn/appSignon.action"

.field public static final MAX_STOPPED_STATE_TIME:J = 0x927c0L

.field public static final MITEK_FRONT_IMAGE_COMPRESSION:I = 0x32

.field public static final MITEK_FRONT_IMAGE_HEIGHT:I = 0x4b0

.field public static final MITEK_FRONT_IMAGE_WIDTH:I = 0x640

.field public static final MITEK_IMAGE_WIDTH:I = 0x640

.field public static final MITEK_REAR_IMAGE_COMPRESSION:I = 0x32

.field public static final MITEK_REAR_IMAGE_HEIGHT:I = 0x4b0

.field public static final MITEK_REAR_IMAGE_WIDTH:I = 0x640

.field public static final MRDC_FLOW_CANCELED:Ljava/lang/String; = "MRDC_FLOW_CANCELED"

.field public static final MRDC_FLOW_COMPLETED:Ljava/lang/String; = "MRDC_FLOW_COMPLETED"

.field public static final MRDC_FLOW_ERRORED:Ljava/lang/String; = "MRDC_FLOW_ERRORED"

.field public static final MRDC_FLOW_IMAGE_SIDE:Ljava/lang/String; = "MRDC_FLOW_STATE"

.field public static final PERFORMANCE_TIMINGS_URI:Ljava/lang/String; = "/deposit/performance.action"

.field public static final PHOTO_TIPS_DONT_SHOW:Ljava/lang/String; = "PHOTO_TIPS_DONT_SHOW"

.field public static final PREFERENCES:Ljava/lang/String; = "WF_PREFERENCES"

.field public static final REQUEST_CODE_ACTIVITY_LOCSEARCH:I = 0x2

.field public static final REQUEST_CODE_CANCEL_MRDC_FLOW:I = 0x4

.field public static final REQUEST_CODE_CAPTURE_PHOTO:I = 0x1

.field public static final REQUEST_CODE_PREVIEW_CHECK_IMAGE:I = 0x3

.field public static final RESULT_ACCOUNT:I = 0x2

.field public static final RESULT_CODE_ACTIVITY_LOCSEARCH:I = 0x8

.field public static final RESULT_CODE_CANCEL_MRDC_FLOW:I = 0xa

.field public static final RESULT_CODE_CAPTURE_PHOTO:I = 0x7

.field public static final RESULT_CODE_COMPLETE_MRDC_FLOW:I = 0xb

.field public static final RESULT_CODE_PREVIEW_CHECK_IMAGE:I = 0x9

.field public static final RESULT_CODE_RETAKE:I = 0xc

.field public static final RESULT_CODE_SIGNOFF:I = 0xd

.field public static final RESULT_DEPOSIT:I = 0x6

.field public static final RESULT_ERROR:I = 0x1

.field public static final RESULT_LOCSEARCH:I = 0x5

.field public static final RESULT_SIGNOFF:I = 0x3

.field public static final RESULT_SIGNON:I = 0x4

.field public static final SAFE_JSON_PREFIX:Ljava/lang/String; = "/*--safejson--"

.field public static final SAFE_JSON_SUFFIX:Ljava/lang/String; = "--safejson--*/"

.field public static final SUBMIT_DEPOSIT_URI:Ljava/lang/String; = "/deposit/submitDeposit.action"

.field public static final SUBMIT_IMAGE_BACK_URI:Ljava/lang/String; = "/deposit/submitBackImage.action"

.field public static final SUBMIT_IMAGE_FRONT_URI:Ljava/lang/String; = "/deposit/submitFrontImage.action"

.field public static final URL_PATH_TOKEN_BROKERAGE:Ljava/lang/CharSequence;

.field public static final URL_PATH_TOKEN_MBA:Ljava/lang/CharSequence;


# instance fields
.field private deviceHasCamera:Lcom/wf/wellsfargomobile/util/Trinary;

.field private errorOpeningCamera:Z

.field private frontCheckImage:[B

.field private frontCheckPreviewImage:Landroid/graphics/Bitmap;

.field private frontImageToken:Ljava/lang/String;

.field private frontImageUploadProcessComplete:Z

.field private googleApiAvailable:Z

.field private lastActivityStoppedAt:J

.field private mrdcEnabled:Lcom/wf/wellsfargomobile/util/Trinary;

.field private nonce:Ljava/lang/String;

.field private perfD1FrontImageStartSend:J

.field private perfD2FrontImageJsonResponse:J

.field private perfD3RearImageStartSend:J

.field private perfD4RearImageJsonResponse:J

.field private perfD5DataStartSend:J

.field private perfD6DataJsonResponse:J

.field private rearCheckImage:[B

.field private rearCheckPreviewImage:Landroid/graphics/Bitmap;

.field private rearImageToken:Ljava/lang/String;

.field private rearImageUploadProcessComplete:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 110
    const-string v0, "mba"

    sput-object v0, Lcom/wf/wellsfargomobile/WFApp;->URL_PATH_TOKEN_MBA:Ljava/lang/CharSequence;

    .line 111
    const-string v0, "BMW"

    sput-object v0, Lcom/wf/wellsfargomobile/WFApp;->URL_PATH_TOKEN_BROKERAGE:Ljava/lang/CharSequence;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 79
    iput-object v1, p0, Lcom/wf/wellsfargomobile/WFApp;->frontCheckImage:[B

    .line 80
    iput-object v1, p0, Lcom/wf/wellsfargomobile/WFApp;->rearCheckImage:[B

    .line 81
    iput-object v1, p0, Lcom/wf/wellsfargomobile/WFApp;->frontCheckPreviewImage:Landroid/graphics/Bitmap;

    .line 82
    iput-object v1, p0, Lcom/wf/wellsfargomobile/WFApp;->rearCheckPreviewImage:Landroid/graphics/Bitmap;

    .line 83
    sget-object v0, Lcom/wf/wellsfargomobile/util/Trinary;->UNKNOWN:Lcom/wf/wellsfargomobile/util/Trinary;

    iput-object v0, p0, Lcom/wf/wellsfargomobile/WFApp;->mrdcEnabled:Lcom/wf/wellsfargomobile/util/Trinary;

    .line 84
    iput-object v1, p0, Lcom/wf/wellsfargomobile/WFApp;->nonce:Ljava/lang/String;

    .line 87
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/wf/wellsfargomobile/WFApp;->googleApiAvailable:Z

    .line 93
    sget-object v0, Lcom/wf/wellsfargomobile/util/Trinary;->UNKNOWN:Lcom/wf/wellsfargomobile/util/Trinary;

    iput-object v0, p0, Lcom/wf/wellsfargomobile/WFApp;->deviceHasCamera:Lcom/wf/wellsfargomobile/util/Trinary;

    .line 95
    iput-boolean v2, p0, Lcom/wf/wellsfargomobile/WFApp;->frontImageUploadProcessComplete:Z

    .line 97
    iput-boolean v2, p0, Lcom/wf/wellsfargomobile/WFApp;->rearImageUploadProcessComplete:Z

    .line 107
    iput-boolean v2, p0, Lcom/wf/wellsfargomobile/WFApp;->errorOpeningCamera:Z

    return-void
.end method

.method private zeroByteArray([B)V
    .locals 2
    .parameter "byteArray"

    .prologue
    .line 134
    if-eqz p1, :cond_0

    .line 135
    const/4 v0, 0x0

    .local v0, ii:I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 136
    const/4 v1, 0x0

    aput-byte v1, p1, v0

    .line 135
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 139
    .end local v0           #ii:I
    :cond_0
    return-void
.end method


# virtual methods
.method public clearCheckDepositData()V
    .locals 1

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/WFApp;->clearImageData()V

    .line 143
    sget-object v0, Lcom/wf/wellsfargomobile/util/Trinary;->UNKNOWN:Lcom/wf/wellsfargomobile/util/Trinary;

    iput-object v0, p0, Lcom/wf/wellsfargomobile/WFApp;->mrdcEnabled:Lcom/wf/wellsfargomobile/util/Trinary;

    .line 144
    return-void
.end method

.method public clearImageData()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/high16 v2, -0x100

    const/4 v1, 0x0

    .line 117
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFApp;->frontCheckImage:[B

    invoke-direct {p0, v0}, Lcom/wf/wellsfargomobile/WFApp;->zeroByteArray([B)V

    .line 118
    iput-object v1, p0, Lcom/wf/wellsfargomobile/WFApp;->frontCheckImage:[B

    .line 119
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFApp;->rearCheckImage:[B

    invoke-direct {p0, v0}, Lcom/wf/wellsfargomobile/WFApp;->zeroByteArray([B)V

    .line 120
    iput-object v1, p0, Lcom/wf/wellsfargomobile/WFApp;->rearCheckImage:[B

    .line 121
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFApp;->frontCheckPreviewImage:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFApp;->frontCheckPreviewImage:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 124
    :cond_0
    iput-object v1, p0, Lcom/wf/wellsfargomobile/WFApp;->frontCheckPreviewImage:Landroid/graphics/Bitmap;

    .line 125
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFApp;->rearCheckPreviewImage:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 126
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFApp;->rearCheckPreviewImage:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 128
    :cond_1
    iput-object v1, p0, Lcom/wf/wellsfargomobile/WFApp;->rearCheckPreviewImage:Landroid/graphics/Bitmap;

    .line 129
    iput-boolean v3, p0, Lcom/wf/wellsfargomobile/WFApp;->frontImageUploadProcessComplete:Z

    .line 130
    iput-boolean v3, p0, Lcom/wf/wellsfargomobile/WFApp;->rearImageUploadProcessComplete:Z

    .line 131
    return-void
.end method

.method public getDeviceHasCamera()Lcom/wf/wellsfargomobile/util/Trinary;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFApp;->deviceHasCamera:Lcom/wf/wellsfargomobile/util/Trinary;

    return-object v0
.end method

.method public getFrontCheckImage()[B
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFApp;->frontCheckImage:[B

    return-object v0
.end method

.method public getFrontCheckPreviewImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFApp;->frontCheckPreviewImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getFrontImageToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFApp;->frontImageToken:Ljava/lang/String;

    return-object v0
.end method

.method public getLastActivityStoppedAt()J
    .locals 2

    .prologue
    .line 295
    iget-wide v0, p0, Lcom/wf/wellsfargomobile/WFApp;->lastActivityStoppedAt:J

    return-wide v0
.end method

.method public getMrdcEnabled()Lcom/wf/wellsfargomobile/util/Trinary;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFApp;->mrdcEnabled:Lcom/wf/wellsfargomobile/util/Trinary;

    return-object v0
.end method

.method public getNonce()Ljava/lang/String;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFApp;->nonce:Ljava/lang/String;

    return-object v0
.end method

.method public getPerfD1FrontImageStartSend()J
    .locals 2

    .prologue
    .line 243
    iget-wide v0, p0, Lcom/wf/wellsfargomobile/WFApp;->perfD1FrontImageStartSend:J

    return-wide v0
.end method

.method public getPerfD2FrontImageJsonResponse()J
    .locals 2

    .prologue
    .line 251
    iget-wide v0, p0, Lcom/wf/wellsfargomobile/WFApp;->perfD2FrontImageJsonResponse:J

    return-wide v0
.end method

.method public getPerfD3RearImageStartSend()J
    .locals 2

    .prologue
    .line 259
    iget-wide v0, p0, Lcom/wf/wellsfargomobile/WFApp;->perfD3RearImageStartSend:J

    return-wide v0
.end method

.method public getPerfD4RearImageJsonResponse()J
    .locals 2

    .prologue
    .line 267
    iget-wide v0, p0, Lcom/wf/wellsfargomobile/WFApp;->perfD4RearImageJsonResponse:J

    return-wide v0
.end method

.method public getPerfD5DataStartSend()J
    .locals 2

    .prologue
    .line 275
    iget-wide v0, p0, Lcom/wf/wellsfargomobile/WFApp;->perfD5DataStartSend:J

    return-wide v0
.end method

.method public getPerfD6DataJsonResponse()J
    .locals 2

    .prologue
    .line 283
    iget-wide v0, p0, Lcom/wf/wellsfargomobile/WFApp;->perfD6DataJsonResponse:J

    return-wide v0
.end method

.method public getRearCheckImage()[B
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFApp;->rearCheckImage:[B

    return-object v0
.end method

.method public getRearCheckPreviewImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFApp;->rearCheckPreviewImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getRearImageToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFApp;->rearImageToken:Ljava/lang/String;

    return-object v0
.end method

.method public isErrorOpeningCamera()Z
    .locals 1

    .prologue
    .line 303
    iget-boolean v0, p0, Lcom/wf/wellsfargomobile/WFApp;->errorOpeningCamera:Z

    return v0
.end method

.method public isFrontImageUploadProcessComplete()Z
    .locals 1

    .prologue
    .line 211
    iget-boolean v0, p0, Lcom/wf/wellsfargomobile/WFApp;->frontImageUploadProcessComplete:Z

    return v0
.end method

.method public isGoogleApiAvailable()Z
    .locals 1

    .prologue
    .line 191
    iget-boolean v0, p0, Lcom/wf/wellsfargomobile/WFApp;->googleApiAvailable:Z

    return v0
.end method

.method public isRearImageUploadProcessComplete()Z
    .locals 1

    .prologue
    .line 223
    iget-boolean v0, p0, Lcom/wf/wellsfargomobile/WFApp;->rearImageUploadProcessComplete:Z

    return v0
.end method

.method public setDeviceHasCamera(Lcom/wf/wellsfargomobile/util/Trinary;)V
    .locals 0
    .parameter "deviceHasCamera"

    .prologue
    .line 171
    iput-object p1, p0, Lcom/wf/wellsfargomobile/WFApp;->deviceHasCamera:Lcom/wf/wellsfargomobile/util/Trinary;

    .line 172
    return-void
.end method

.method public setErrorOpeningCamera(Z)V
    .locals 0
    .parameter "errorOpeningCamera"

    .prologue
    .line 299
    iput-boolean p1, p0, Lcom/wf/wellsfargomobile/WFApp;->errorOpeningCamera:Z

    .line 300
    return-void
.end method

.method public setFrontCheckImage([B)V
    .locals 0
    .parameter "frontCheckImage"

    .prologue
    .line 151
    iput-object p1, p0, Lcom/wf/wellsfargomobile/WFApp;->frontCheckImage:[B

    .line 152
    return-void
.end method

.method public setFrontCheckPreviewImage(Landroid/graphics/Bitmap;)V
    .locals 0
    .parameter "frontCheckPreviewImage"

    .prologue
    .line 231
    iput-object p1, p0, Lcom/wf/wellsfargomobile/WFApp;->frontCheckPreviewImage:Landroid/graphics/Bitmap;

    .line 232
    return-void
.end method

.method public setFrontImageToken(Ljava/lang/String;)V
    .locals 0
    .parameter "frontImageToken"

    .prologue
    .line 195
    iput-object p1, p0, Lcom/wf/wellsfargomobile/WFApp;->frontImageToken:Ljava/lang/String;

    .line 196
    return-void
.end method

.method public setFrontImageUploadProcessComplete(Z)V
    .locals 0
    .parameter "frontImageUploadProcessComplete"

    .prologue
    .line 215
    iput-boolean p1, p0, Lcom/wf/wellsfargomobile/WFApp;->frontImageUploadProcessComplete:Z

    .line 216
    return-void
.end method

.method public setGoogleApiAvailable(Z)V
    .locals 0
    .parameter "googleApiAvailable"

    .prologue
    .line 187
    iput-boolean p1, p0, Lcom/wf/wellsfargomobile/WFApp;->googleApiAvailable:Z

    .line 188
    return-void
.end method

.method public setLastActivityStoppedAt(J)V
    .locals 0
    .parameter "timeMillis"

    .prologue
    .line 291
    iput-wide p1, p0, Lcom/wf/wellsfargomobile/WFApp;->lastActivityStoppedAt:J

    .line 292
    return-void
.end method

.method public setMrdcEnabled(Lcom/wf/wellsfargomobile/util/Trinary;)V
    .locals 0
    .parameter "mrdcEnabled"

    .prologue
    .line 163
    iput-object p1, p0, Lcom/wf/wellsfargomobile/WFApp;->mrdcEnabled:Lcom/wf/wellsfargomobile/util/Trinary;

    .line 164
    return-void
.end method

.method public setNonce(Ljava/lang/String;)V
    .locals 0
    .parameter "nonce"

    .prologue
    .line 179
    iput-object p1, p0, Lcom/wf/wellsfargomobile/WFApp;->nonce:Ljava/lang/String;

    .line 180
    return-void
.end method

.method public setPerfD1FrontImageStartSend(J)V
    .locals 0
    .parameter "perfD1FrontImageStartSend"

    .prologue
    .line 247
    iput-wide p1, p0, Lcom/wf/wellsfargomobile/WFApp;->perfD1FrontImageStartSend:J

    .line 248
    return-void
.end method

.method public setPerfD2FrontImageJsonResponse(J)V
    .locals 0
    .parameter "perfD2FrontImageJsonResponse"

    .prologue
    .line 255
    iput-wide p1, p0, Lcom/wf/wellsfargomobile/WFApp;->perfD2FrontImageJsonResponse:J

    .line 256
    return-void
.end method

.method public setPerfD3RearImageStartSend(J)V
    .locals 0
    .parameter "perfD3RearImageStartSend"

    .prologue
    .line 263
    iput-wide p1, p0, Lcom/wf/wellsfargomobile/WFApp;->perfD3RearImageStartSend:J

    .line 264
    return-void
.end method

.method public setPerfD4RearImageJsonResponse(J)V
    .locals 0
    .parameter "perfD4RearImageJsonResponse"

    .prologue
    .line 271
    iput-wide p1, p0, Lcom/wf/wellsfargomobile/WFApp;->perfD4RearImageJsonResponse:J

    .line 272
    return-void
.end method

.method public setPerfD5DataStartSend(J)V
    .locals 0
    .parameter "perfD5DataStartSend"

    .prologue
    .line 279
    iput-wide p1, p0, Lcom/wf/wellsfargomobile/WFApp;->perfD5DataStartSend:J

    .line 280
    return-void
.end method

.method public setPerfD6DataJsonResponse(J)V
    .locals 0
    .parameter "perfD6DataJsonResponse"

    .prologue
    .line 287
    iput-wide p1, p0, Lcom/wf/wellsfargomobile/WFApp;->perfD6DataJsonResponse:J

    .line 288
    return-void
.end method

.method public setRearCheckImage([B)V
    .locals 0
    .parameter "rearCheckImage"

    .prologue
    .line 159
    iput-object p1, p0, Lcom/wf/wellsfargomobile/WFApp;->rearCheckImage:[B

    .line 160
    return-void
.end method

.method public setRearCheckPreviewImage(Landroid/graphics/Bitmap;)V
    .locals 0
    .parameter "rearCheckPreviewImage"

    .prologue
    .line 239
    iput-object p1, p0, Lcom/wf/wellsfargomobile/WFApp;->rearCheckPreviewImage:Landroid/graphics/Bitmap;

    .line 240
    return-void
.end method

.method public setRearImageToken(Ljava/lang/String;)V
    .locals 0
    .parameter "rearImageToken"

    .prologue
    .line 203
    iput-object p1, p0, Lcom/wf/wellsfargomobile/WFApp;->rearImageToken:Ljava/lang/String;

    .line 204
    return-void
.end method

.method public setRearImageUploadProcessComplete(Z)V
    .locals 0
    .parameter "rearImageUploadProcessComplete"

    .prologue
    .line 219
    iput-boolean p1, p0, Lcom/wf/wellsfargomobile/WFApp;->rearImageUploadProcessComplete:Z

    .line 220
    return-void
.end method
