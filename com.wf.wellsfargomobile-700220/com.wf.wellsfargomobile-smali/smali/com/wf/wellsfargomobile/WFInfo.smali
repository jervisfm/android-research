.class public Lcom/wf/wellsfargomobile/WFInfo;
.super Lcom/wf/wellsfargomobile/BaseActivity;
.source "WFInfo.java"


# instance fields
.field private clickHandler:Landroid/view/View$OnClickListener;

.field private doneButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/BaseActivity;-><init>()V

    .line 24
    new-instance v0, Lcom/wf/wellsfargomobile/WFInfo$1;

    invoke-direct {v0, p0}, Lcom/wf/wellsfargomobile/WFInfo$1;-><init>(Lcom/wf/wellsfargomobile/WFInfo;)V

    iput-object v0, p0, Lcom/wf/wellsfargomobile/WFInfo;->clickHandler:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/wf/wellsfargomobile/WFInfo;)Landroid/widget/Button;
    .locals 1
    .parameter "x0"

    .prologue
    .line 12
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFInfo;->doneButton:Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedInstanceState"

    .prologue
    .line 17
    invoke-super {p0, p1}, Lcom/wf/wellsfargomobile/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 18
    const v0, 0x7f03000b

    invoke-virtual {p0, v0}, Lcom/wf/wellsfargomobile/WFInfo;->setContentView(I)V

    .line 20
    const v0, 0x7f060075

    invoke-virtual {p0, v0}, Lcom/wf/wellsfargomobile/WFInfo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/wf/wellsfargomobile/WFInfo;->doneButton:Landroid/widget/Button;

    .line 21
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFInfo;->doneButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/wf/wellsfargomobile/WFInfo;->clickHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 22
    return-void
.end method
