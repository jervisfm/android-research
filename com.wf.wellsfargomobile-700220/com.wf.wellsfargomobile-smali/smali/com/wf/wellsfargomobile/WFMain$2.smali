.class Lcom/wf/wellsfargomobile/WFMain$2;
.super Ljava/lang/Object;
.source "WFMain.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wf/wellsfargomobile/WFMain;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/wf/wellsfargomobile/WFMain;


# direct methods
.method constructor <init>(Lcom/wf/wellsfargomobile/WFMain;)V
    .locals 0
    .parameter

    .prologue
    .line 175
    iput-object p1, p0, Lcom/wf/wellsfargomobile/WFMain$2;->this$0:Lcom/wf/wellsfargomobile/WFMain;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .parameter "v"

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 178
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain$2;->this$0:Lcom/wf/wellsfargomobile/WFMain;

    #getter for: Lcom/wf/wellsfargomobile/WFMain;->signOnButton:Landroid/widget/Button;
    invoke-static {v2}, Lcom/wf/wellsfargomobile/WFMain;->access$100(Lcom/wf/wellsfargomobile/WFMain;)Landroid/widget/Button;

    move-result-object v2

    if-ne p1, v2, :cond_1

    .line 179
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain$2;->this$0:Lcom/wf/wellsfargomobile/WFMain;

    #getter for: Lcom/wf/wellsfargomobile/WFMain;->signOnButton:Landroid/widget/Button;
    invoke-static {v2}, Lcom/wf/wellsfargomobile/WFMain;->access$100(Lcom/wf/wellsfargomobile/WFMain;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 181
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain$2;->this$0:Lcom/wf/wellsfargomobile/WFMain;

    #calls: Lcom/wf/wellsfargomobile/WFMain;->loginGo()V
    invoke-static {v2}, Lcom/wf/wellsfargomobile/WFMain;->access$000(Lcom/wf/wellsfargomobile/WFMain;)V

    .line 214
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain$2;->this$0:Lcom/wf/wellsfargomobile/WFMain;

    #getter for: Lcom/wf/wellsfargomobile/WFMain;->password:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/wf/wellsfargomobile/WFMain;->access$800(Lcom/wf/wellsfargomobile/WFMain;)Landroid/widget/EditText;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 215
    return-void

    .line 182
    :cond_1
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain$2;->this$0:Lcom/wf/wellsfargomobile/WFMain;

    #getter for: Lcom/wf/wellsfargomobile/WFMain;->currentLocButton:Landroid/widget/Button;
    invoke-static {v2}, Lcom/wf/wellsfargomobile/WFMain;->access$200(Lcom/wf/wellsfargomobile/WFMain;)Landroid/widget/Button;

    move-result-object v2

    if-ne p1, v2, :cond_2

    .line 183
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain$2;->this$0:Lcom/wf/wellsfargomobile/WFMain;

    const-class v3, Lcom/wf/wellsfargomobile/loc/LocationResults;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 184
    .local v0, locSearchIntent:Landroid/content/Intent;
    const-string v2, "was_gps"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 185
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain$2;->this$0:Lcom/wf/wellsfargomobile/WFMain;

    invoke-virtual {v2, v0, v4}, Lcom/wf/wellsfargomobile/WFMain;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 186
    .end local v0           #locSearchIntent:Landroid/content/Intent;
    :cond_2
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain$2;->this$0:Lcom/wf/wellsfargomobile/WFMain;

    #getter for: Lcom/wf/wellsfargomobile/WFMain;->locSearchButton:Landroid/widget/Button;
    invoke-static {v2}, Lcom/wf/wellsfargomobile/WFMain;->access$300(Lcom/wf/wellsfargomobile/WFMain;)Landroid/widget/Button;

    move-result-object v2

    if-ne p1, v2, :cond_4

    .line 187
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain$2;->this$0:Lcom/wf/wellsfargomobile/WFMain;

    #getter for: Lcom/wf/wellsfargomobile/WFMain;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static {v2}, Lcom/wf/wellsfargomobile/WFMain;->access$400(Lcom/wf/wellsfargomobile/WFMain;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/wf/wellsfargomobile/WFApp;->isGoogleApiAvailable()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 188
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain$2;->this$0:Lcom/wf/wellsfargomobile/WFMain;

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/wf/wellsfargomobile/WFMain$2;->this$0:Lcom/wf/wellsfargomobile/WFMain;

    const-class v5, Lcom/wf/wellsfargomobile/loc/LocationForm;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v4}, Lcom/wf/wellsfargomobile/WFMain;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 193
    :cond_3
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain$2;->this$0:Lcom/wf/wellsfargomobile/WFMain;

    #getter for: Lcom/wf/wellsfargomobile/WFMain;->locSearchButton:Landroid/widget/Button;
    invoke-static {v2}, Lcom/wf/wellsfargomobile/WFMain;->access$300(Lcom/wf/wellsfargomobile/WFMain;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Button;->requestFocusFromTouch()Z

    .line 194
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain$2;->this$0:Lcom/wf/wellsfargomobile/WFMain;

    const-class v3, Lcom/wf/wellsfargomobile/WFwebview;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 195
    .local v1, webviewIntent:Landroid/content/Intent;
    const-string v2, "webatmloc_request"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 196
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain$2;->this$0:Lcom/wf/wellsfargomobile/WFMain;

    invoke-virtual {v2, v1, v5}, Lcom/wf/wellsfargomobile/WFMain;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 198
    .end local v1           #webviewIntent:Landroid/content/Intent;
    :cond_4
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain$2;->this$0:Lcom/wf/wellsfargomobile/WFMain;

    #getter for: Lcom/wf/wellsfargomobile/WFMain;->security:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/wf/wellsfargomobile/WFMain;->access$500(Lcom/wf/wellsfargomobile/WFMain;)Landroid/widget/LinearLayout;

    move-result-object v2

    if-ne p1, v2, :cond_5

    .line 199
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain$2;->this$0:Lcom/wf/wellsfargomobile/WFMain;

    #getter for: Lcom/wf/wellsfargomobile/WFMain;->security:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/wf/wellsfargomobile/WFMain;->access$500(Lcom/wf/wellsfargomobile/WFMain;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->requestFocusFromTouch()Z

    .line 200
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain$2;->this$0:Lcom/wf/wellsfargomobile/WFMain;

    const-class v3, Lcom/wf/wellsfargomobile/WFwebview;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 201
    .restart local v1       #webviewIntent:Landroid/content/Intent;
    const-string v2, "security_request"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 202
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain$2;->this$0:Lcom/wf/wellsfargomobile/WFMain;

    invoke-virtual {v2, v1, v5}, Lcom/wf/wellsfargomobile/WFMain;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 203
    .end local v1           #webviewIntent:Landroid/content/Intent;
    :cond_5
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain$2;->this$0:Lcom/wf/wellsfargomobile/WFMain;

    #getter for: Lcom/wf/wellsfargomobile/WFMain;->quickGuide:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/wf/wellsfargomobile/WFMain;->access$600(Lcom/wf/wellsfargomobile/WFMain;)Landroid/widget/TextView;

    move-result-object v2

    if-ne p1, v2, :cond_6

    .line 204
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain$2;->this$0:Lcom/wf/wellsfargomobile/WFMain;

    #getter for: Lcom/wf/wellsfargomobile/WFMain;->quickGuide:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/wf/wellsfargomobile/WFMain;->access$600(Lcom/wf/wellsfargomobile/WFMain;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->requestFocusFromTouch()Z

    .line 205
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain$2;->this$0:Lcom/wf/wellsfargomobile/WFMain;

    const-class v3, Lcom/wf/wellsfargomobile/WFwebview;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 206
    .restart local v1       #webviewIntent:Landroid/content/Intent;
    const-string v2, "qguide_request"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 207
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain$2;->this$0:Lcom/wf/wellsfargomobile/WFMain;

    invoke-virtual {v2, v1, v5}, Lcom/wf/wellsfargomobile/WFMain;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 208
    .end local v1           #webviewIntent:Landroid/content/Intent;
    :cond_6
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain$2;->this$0:Lcom/wf/wellsfargomobile/WFMain;

    #getter for: Lcom/wf/wellsfargomobile/WFMain;->privacyPolicy:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/wf/wellsfargomobile/WFMain;->access$700(Lcom/wf/wellsfargomobile/WFMain;)Landroid/widget/TextView;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 209
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain$2;->this$0:Lcom/wf/wellsfargomobile/WFMain;

    #getter for: Lcom/wf/wellsfargomobile/WFMain;->privacyPolicy:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/wf/wellsfargomobile/WFMain;->access$700(Lcom/wf/wellsfargomobile/WFMain;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->requestFocusFromTouch()Z

    .line 210
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain$2;->this$0:Lcom/wf/wellsfargomobile/WFMain;

    const-class v3, Lcom/wf/wellsfargomobile/WFwebview;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 211
    .restart local v1       #webviewIntent:Landroid/content/Intent;
    const-string v2, "privacy_policy_request"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 212
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain$2;->this$0:Lcom/wf/wellsfargomobile/WFMain;

    invoke-virtual {v2, v1, v5}, Lcom/wf/wellsfargomobile/WFMain;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0
.end method
