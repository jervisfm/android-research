.class public Lcom/wf/wellsfargomobile/WFMain;
.super Lcom/wf/wellsfargomobile/BaseActivity;
.source "WFMain.java"


# static fields
.field public static final ACTIVITY_SEARCH:I = 0x1

.field public static final ACTIVITY_SEARCHFORM:I = 0x2

.field public static final ACTIVITY_WEBVIEW:I = 0x0

.field public static final KEY_DEPOSIT:Ljava/lang/String; = "deposit"

.field public static final KEY_LOGIN:Ljava/lang/String; = "login_request"

.field public static final KEY_PASSWORD:Ljava/lang/String; = "password"

.field public static final KEY_PRIVACY_POLICY:Ljava/lang/String; = "privacy_policy_request"

.field public static final KEY_QGUIDE:Ljava/lang/String; = "qguide_request"

.field public static final KEY_SECURITY:Ljava/lang/String; = "security_request"

.field public static final KEY_USERNAME:Ljava/lang/String; = "username"

.field public static final KEY_WEBATMLOC:Ljava/lang/String; = "webatmloc_request"

.field private static final TAG:Ljava/lang/String; = "WFMain"


# instance fields
.field private clickHandler:Landroid/view/View$OnClickListener;

.field private currentLocButton:Landroid/widget/Button;

.field private locSearchButton:Landroid/widget/Button;

.field private password:Landroid/widget/EditText;

.field private privacyPolicy:Landroid/widget/TextView;

.field private quickGuide:Landroid/widget/TextView;

.field private security:Landroid/widget/LinearLayout;

.field private signOnButton:Landroid/widget/Button;

.field private username:Landroid/widget/EditText;

.field private wfApp:Lcom/wf/wellsfargomobile/WFApp;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/BaseActivity;-><init>()V

    .line 175
    new-instance v0, Lcom/wf/wellsfargomobile/WFMain$2;

    invoke-direct {v0, p0}, Lcom/wf/wellsfargomobile/WFMain$2;-><init>(Lcom/wf/wellsfargomobile/WFMain;)V

    iput-object v0, p0, Lcom/wf/wellsfargomobile/WFMain;->clickHandler:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/wf/wellsfargomobile/WFMain;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/WFMain;->loginGo()V

    return-void
.end method

.method static synthetic access$100(Lcom/wf/wellsfargomobile/WFMain;)Landroid/widget/Button;
    .locals 1
    .parameter "x0"

    .prologue
    .line 36
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFMain;->signOnButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$200(Lcom/wf/wellsfargomobile/WFMain;)Landroid/widget/Button;
    .locals 1
    .parameter "x0"

    .prologue
    .line 36
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFMain;->currentLocButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$300(Lcom/wf/wellsfargomobile/WFMain;)Landroid/widget/Button;
    .locals 1
    .parameter "x0"

    .prologue
    .line 36
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFMain;->locSearchButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$400(Lcom/wf/wellsfargomobile/WFMain;)Lcom/wf/wellsfargomobile/WFApp;
    .locals 1
    .parameter "x0"

    .prologue
    .line 36
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFMain;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    return-object v0
.end method

.method static synthetic access$500(Lcom/wf/wellsfargomobile/WFMain;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter "x0"

    .prologue
    .line 36
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFMain;->security:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$600(Lcom/wf/wellsfargomobile/WFMain;)Landroid/widget/TextView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 36
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFMain;->quickGuide:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/wf/wellsfargomobile/WFMain;)Landroid/widget/TextView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 36
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFMain;->privacyPolicy:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/wf/wellsfargomobile/WFMain;)Landroid/widget/EditText;
    .locals 1
    .parameter "x0"

    .prologue
    .line 36
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFMain;->password:Landroid/widget/EditText;

    return-object v0
.end method

.method private loginGo()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 139
    iget-object v3, p0, Lcom/wf/wellsfargomobile/WFMain;->username:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 140
    .local v1, myUsername:Landroid/text/Editable;
    iget-object v3, p0, Lcom/wf/wellsfargomobile/WFMain;->password:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 142
    .local v0, myPassword:Landroid/text/Editable;
    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v3

    if-nez v3, :cond_0

    .line 143
    iget-object v3, p0, Lcom/wf/wellsfargomobile/WFMain;->signOnButton:Landroid/widget/Button;

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 144
    const v3, 0x7f070066

    invoke-virtual {p0, v3}, Lcom/wf/wellsfargomobile/WFMain;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v6, v3}, Lcom/wf/wellsfargomobile/WFMain;->showDialogBox(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    :goto_0
    iget-object v3, p0, Lcom/wf/wellsfargomobile/WFMain;->password:Landroid/widget/EditText;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 173
    return-void

    .line 145
    :cond_0
    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v3

    const/4 v4, 0x6

    if-ge v3, v4, :cond_1

    .line 146
    iget-object v3, p0, Lcom/wf/wellsfargomobile/WFMain;->signOnButton:Landroid/widget/Button;

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 147
    const v3, 0x7f070067

    invoke-virtual {p0, v3}, Lcom/wf/wellsfargomobile/WFMain;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v6, v3}, Lcom/wf/wellsfargomobile/WFMain;->showDialogBox(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 148
    :cond_1
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v3

    if-nez v3, :cond_2

    .line 149
    iget-object v3, p0, Lcom/wf/wellsfargomobile/WFMain;->signOnButton:Landroid/widget/Button;

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 150
    const v3, 0x7f07004b

    invoke-virtual {p0, v3}, Lcom/wf/wellsfargomobile/WFMain;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v6, v3}, Lcom/wf/wellsfargomobile/WFMain;->showDialogBox(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 153
    :cond_2
    iget-object v3, p0, Lcom/wf/wellsfargomobile/WFMain;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/wf/wellsfargomobile/WFApp;->setNonce(Ljava/lang/String;)V

    .line 155
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/wf/wellsfargomobile/WFwebview;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 156
    .local v2, webviewIntent:Landroid/content/Intent;
    const-string v3, "login_request"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 159
    const-string v3, "username"

    invoke-static {v3, v1}, Lcom/wf/wellsfargomobile/util/NonPersistentExtraStorage;->putExtras(Ljava/lang/String;Ljava/lang/Object;)V

    .line 160
    const-string v3, "password"

    invoke-static {v3, v0}, Lcom/wf/wellsfargomobile/util/NonPersistentExtraStorage;->putExtras(Ljava/lang/String;Ljava/lang/Object;)V

    .line 163
    iget-object v3, p0, Lcom/wf/wellsfargomobile/WFMain;->username:Landroid/widget/EditText;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v3, p0, Lcom/wf/wellsfargomobile/WFMain;->password:Landroid/widget/EditText;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 166
    iget-object v3, p0, Lcom/wf/wellsfargomobile/WFMain;->username:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->requestFocus()Z

    .line 168
    const-string v3, "WFMain"

    const-string v4, "starting webview"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/wf/wellsfargomobile/WFMain;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private showDialogBox(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .parameter "title"
    .parameter "message"

    .prologue
    const v1, 0x1080027

    .line 242
    if-eqz p1, :cond_0

    .line 243
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "OK"

    new-instance v2, Lcom/wf/wellsfargomobile/WFMain$3;

    invoke-direct {v2, p0}, Lcom/wf/wellsfargomobile/WFMain$3;-><init>(Lcom/wf/wellsfargomobile/WFMain;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 268
    :goto_0
    return-void

    .line 256
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "OK"

    new-instance v2, Lcom/wf/wellsfargomobile/WFMain$4;

    invoke-direct {v2, p0}, Lcom/wf/wellsfargomobile/WFMain$4;-><init>(Lcom/wf/wellsfargomobile/WFMain;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "intent"

    .prologue
    const/4 v4, 0x1

    .line 220
    invoke-super {p0, p1, p2, p3}, Lcom/wf/wellsfargomobile/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 221
    const/4 v3, 0x2

    if-eq p1, v3, :cond_0

    const/4 v3, 0x4

    if-ne p2, v3, :cond_2

    .line 222
    :cond_0
    iget-object v3, p0, Lcom/wf/wellsfargomobile/WFMain;->username:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->requestFocus()Z

    .line 239
    :cond_1
    :goto_0
    return-void

    .line 223
    :cond_2
    const/4 v3, 0x5

    if-ne p2, v3, :cond_3

    .line 224
    iget-object v3, p0, Lcom/wf/wellsfargomobile/WFMain;->currentLocButton:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    .line 225
    :cond_3
    if-eqz p1, :cond_4

    if-ne p1, v4, :cond_1

    :cond_4
    if-ne p2, v4, :cond_1

    .line 227
    iget-object v3, p0, Lcom/wf/wellsfargomobile/WFMain;->signOnButton:Landroid/widget/Button;

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 229
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 231
    .local v0, extras:Landroid/os/Bundle;
    const-string v3, "error_title"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 232
    const-string v3, "error_title"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 236
    .local v2, title:Ljava/lang/String;
    :goto_1
    const-string v3, "error_msg"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 237
    .local v1, message:Ljava/lang/String;
    invoke-direct {p0, v2, v1}, Lcom/wf/wellsfargomobile/WFMain;->showDialogBox(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 234
    .end local v1           #message:Ljava/lang/String;
    .end local v2           #title:Ljava/lang/String;
    :cond_5
    const/4 v2, 0x0

    .restart local v2       #title:Ljava/lang/String;
    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter "savedInstanceState"

    .prologue
    .line 65
    invoke-super {p0, p1}, Lcom/wf/wellsfargomobile/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 66
    const v2, 0x7f03000c

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/WFMain;->setContentView(I)V

    .line 68
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/WFMain;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/wf/wellsfargomobile/WFApp;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/WFMain;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    .line 70
    const v2, 0x7f060076

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/WFMain;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/WFMain;->username:Landroid/widget/EditText;

    .line 75
    const v2, 0x7f060077

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/WFMain;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/WFMain;->password:Landroid/widget/EditText;

    .line 76
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain;->password:Landroid/widget/EditText;

    new-instance v3, Landroid/text/method/PasswordTransformationMethod;

    invoke-direct {v3}, Landroid/text/method/PasswordTransformationMethod;-><init>()V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 77
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain;->password:Landroid/widget/EditText;

    const/16 v3, 0x81

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setRawInputType(I)V

    .line 79
    const v2, 0x7f060078

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/WFMain;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/WFMain;->signOnButton:Landroid/widget/Button;

    .line 80
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain;->signOnButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/wf/wellsfargomobile/WFMain;->clickHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    const v2, 0x7f060014

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/WFMain;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/WFMain;->currentLocButton:Landroid/widget/Button;

    .line 83
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain;->currentLocButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/wf/wellsfargomobile/WFMain;->clickHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    const v2, 0x7f060015

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/WFMain;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/WFMain;->locSearchButton:Landroid/widget/Button;

    .line 86
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain;->locSearchButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/wf/wellsfargomobile/WFMain;->clickHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    const v2, 0x7f060079

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/WFMain;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/WFMain;->security:Landroid/widget/LinearLayout;

    .line 89
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain;->security:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/wf/wellsfargomobile/WFMain;->clickHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    const v2, 0x7f06007b

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/WFMain;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/WFMain;->quickGuide:Landroid/widget/TextView;

    .line 92
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain;->quickGuide:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/wf/wellsfargomobile/WFMain;->clickHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    const v2, 0x7f06007c

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/WFMain;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/WFMain;->privacyPolicy:Landroid/widget/TextView;

    .line 95
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain;->privacyPolicy:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/wf/wellsfargomobile/WFMain;->clickHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain;->password:Landroid/widget/EditText;

    new-instance v3, Lcom/wf/wellsfargomobile/WFMain$1;

    invoke-direct {v3, p0}, Lcom/wf/wellsfargomobile/WFMain$1;-><init>(Lcom/wf/wellsfargomobile/WFMain;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 112
    :try_start_0
    const-class v1, Lcom/google/android/maps/MapView;

    .line 114
    .local v1, mapViewClass:Ljava/lang/Class;
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/wf/wellsfargomobile/WFApp;->setGoogleApiAvailable(Z)V
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    .end local v1           #mapViewClass:Ljava/lang/Class;
    :goto_0
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-virtual {v2}, Lcom/wf/wellsfargomobile/WFApp;->isGoogleApiAvailable()Z

    move-result v2

    if-nez v2, :cond_0

    .line 122
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain;->currentLocButton:Landroid/widget/Button;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 129
    :cond_0
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/WFMain;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "android.hardware.camera"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 131
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    sget-object v3, Lcom/wf/wellsfargomobile/util/Trinary;->TRUE:Lcom/wf/wellsfargomobile/util/Trinary;

    invoke-virtual {v2, v3}, Lcom/wf/wellsfargomobile/WFApp;->setDeviceHasCamera(Lcom/wf/wellsfargomobile/util/Trinary;)V

    .line 136
    :goto_1
    return-void

    .line 115
    :catch_0
    move-exception v0

    .line 117
    .local v0, e:Ljava/lang/NoClassDefFoundError;
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/wf/wellsfargomobile/WFApp;->setGoogleApiAvailable(Z)V

    goto :goto_0

    .line 134
    .end local v0           #e:Ljava/lang/NoClassDefFoundError;
    :cond_1
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFMain;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    sget-object v3, Lcom/wf/wellsfargomobile/util/Trinary;->FALSE:Lcom/wf/wellsfargomobile/util/Trinary;

    invoke-virtual {v2, v3}, Lcom/wf/wellsfargomobile/WFApp;->setDeviceHasCamera(Lcom/wf/wellsfargomobile/util/Trinary;)V

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .parameter "menu"

    .prologue
    .line 272
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/WFMain;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f0a

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 273
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .parameter "item"

    .prologue
    .line 278
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 283
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 280
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/wf/wellsfargomobile/WFInfo;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/wf/wellsfargomobile/WFMain;->startActivity(Landroid/content/Intent;)V

    .line 281
    const/4 v0, 0x1

    goto :goto_0

    .line 278
    nop

    :pswitch_data_0
    .packed-switch 0x7f06007d
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFMain;->username:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 297
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFMain;->password:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 298
    invoke-super {p0}, Lcom/wf/wellsfargomobile/BaseActivity;->onPause()V

    .line 299
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 288
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFMain;->username:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 289
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFMain;->password:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 290
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFMain;->signOnButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 291
    invoke-super {p0}, Lcom/wf/wellsfargomobile/BaseActivity;->onResume()V

    .line 292
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 303
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFMain;->username:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 304
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFMain;->password:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 305
    invoke-super {p0, p1}, Lcom/wf/wellsfargomobile/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 306
    return-void
.end method
