.class public Lcom/wf/wellsfargomobile/WFwebview$CheckDepositJavaScriptInterface;
.super Ljava/lang/Object;
.source "WFwebview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wf/wellsfargomobile/WFwebview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CheckDepositJavaScriptInterface"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "CheckDepositJavaScriptInterface"


# instance fields
.field private final mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/wf/wellsfargomobile/WFwebview;


# direct methods
.method public constructor <init>(Lcom/wf/wellsfargomobile/WFwebview;Landroid/content/Context;)V
    .locals 0
    .parameter
    .parameter "c"

    .prologue
    .line 794
    iput-object p1, p0, Lcom/wf/wellsfargomobile/WFwebview$CheckDepositJavaScriptInterface;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 795
    iput-object p2, p0, Lcom/wf/wellsfargomobile/WFwebview$CheckDepositJavaScriptInterface;->mContext:Landroid/content/Context;

    .line 796
    return-void
.end method


# virtual methods
.method public cancelCheckDeposit(Ljava/lang/String;)V
    .locals 2
    .parameter "nonce"

    .prologue
    .line 892
    const-string v0, "CheckDepositJavaScriptInterface"

    const-string v1, "cancelCheckDeposit() - called from JavaScript"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 893
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview$CheckDepositJavaScriptInterface;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #calls: Lcom/wf/wellsfargomobile/WFwebview;->validNonce(Ljava/lang/String;)Z
    invoke-static {v0, p1}, Lcom/wf/wellsfargomobile/WFwebview;->access$1600(Lcom/wf/wellsfargomobile/WFwebview;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 894
    const-string v0, "CheckDepositJavaScriptInterface"

    const-string v1, "cancelCheckDeposit() - nonce not valid"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 904
    :goto_0
    return-void

    .line 898
    :cond_0
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview$CheckDepositJavaScriptInterface;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    new-instance v1, Lcom/wf/wellsfargomobile/WFwebview$CheckDepositJavaScriptInterface$2;

    invoke-direct {v1, p0}, Lcom/wf/wellsfargomobile/WFwebview$CheckDepositJavaScriptInterface$2;-><init>(Lcom/wf/wellsfargomobile/WFwebview$CheckDepositJavaScriptInterface;)V

    invoke-virtual {v0, v1}, Lcom/wf/wellsfargomobile/WFwebview;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public communicationError(Ljava/lang/String;)V
    .locals 4
    .parameter "nonce"

    .prologue
    .line 851
    const-string v1, "CheckDepositJavaScriptInterface"

    const-string v2, "in communicationError()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 852
    iget-object v1, p0, Lcom/wf/wellsfargomobile/WFwebview$CheckDepositJavaScriptInterface;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #calls: Lcom/wf/wellsfargomobile/WFwebview;->validNonce(Ljava/lang/String;)Z
    invoke-static {v1, p1}, Lcom/wf/wellsfargomobile/WFwebview;->access$1600(Lcom/wf/wellsfargomobile/WFwebview;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 853
    const-string v1, "CheckDepositJavaScriptInterface"

    const-string v2, "communicationError() - nonce not valid"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 869
    :goto_0
    return-void

    .line 857
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/wf/wellsfargomobile/WFwebview$CheckDepositJavaScriptInterface;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 858
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    iget-object v1, p0, Lcom/wf/wellsfargomobile/WFwebview$CheckDepositJavaScriptInterface;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    const v2, 0x7f070022

    invoke-virtual {v1, v2}, Lcom/wf/wellsfargomobile/WFwebview;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview$CheckDepositJavaScriptInterface;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    const v3, 0x7f070046

    invoke-virtual {v2, v3}, Lcom/wf/wellsfargomobile/WFwebview;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/wf/wellsfargomobile/WFwebview$CheckDepositJavaScriptInterface$1;

    invoke-direct {v3, p0}, Lcom/wf/wellsfargomobile/WFwebview$CheckDepositJavaScriptInterface$1;-><init>(Lcom/wf/wellsfargomobile/WFwebview$CheckDepositJavaScriptInterface;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 868
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    goto :goto_0
.end method

.method public enableMrdc(Ljava/lang/String;)V
    .locals 6
    .parameter "json"

    .prologue
    .line 816
    :try_start_0
    new-instance v4, Lorg/json/JSONTokener;

    invoke-direct {v4, p1}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/json/JSONObject;

    .line 818
    .local v3, object:Lorg/json/JSONObject;
    const-string v4, "nonce"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 820
    .local v2, nonce:Ljava/lang/String;
    iget-object v4, p0, Lcom/wf/wellsfargomobile/WFwebview$CheckDepositJavaScriptInterface;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #calls: Lcom/wf/wellsfargomobile/WFwebview;->validNonce(Ljava/lang/String;)Z
    invoke-static {v4, v2}, Lcom/wf/wellsfargomobile/WFwebview;->access$1600(Lcom/wf/wellsfargomobile/WFwebview;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 821
    const-string v4, "CheckDepositJavaScriptInterface"

    const-string v5, "enableMrdc() - nonce not valid"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 832
    .end local v2           #nonce:Ljava/lang/String;
    .end local v3           #object:Lorg/json/JSONObject;
    :goto_0
    return-void

    .line 825
    .restart local v2       #nonce:Ljava/lang/String;
    .restart local v3       #object:Lorg/json/JSONObject;
    :cond_0
    const-string v4, "customerEnabledMrcd"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 826
    .local v1, customerEnabledMrcdStr:Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 827
    .local v0, customerEnabledMrcd:Z
    iget-object v4, p0, Lcom/wf/wellsfargomobile/WFwebview$CheckDepositJavaScriptInterface;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static {v4}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v4

    invoke-static {v0}, Lcom/wf/wellsfargomobile/util/Trinary;->parseTrinary(Z)Lcom/wf/wellsfargomobile/util/Trinary;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/wf/wellsfargomobile/WFApp;->setMrdcEnabled(Lcom/wf/wellsfargomobile/util/Trinary;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 829
    .end local v0           #customerEnabledMrcd:Z
    .end local v1           #customerEnabledMrcdStr:Ljava/lang/String;
    .end local v2           #nonce:Ljava/lang/String;
    .end local v3           #object:Lorg/json/JSONObject;
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method public enterCheckDepositFlow(Ljava/lang/String;)V
    .locals 2
    .parameter "nonce"

    .prologue
    .line 876
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview$CheckDepositJavaScriptInterface;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #calls: Lcom/wf/wellsfargomobile/WFwebview;->validNonce(Ljava/lang/String;)Z
    invoke-static {v0, p1}, Lcom/wf/wellsfargomobile/WFwebview;->access$1600(Lcom/wf/wellsfargomobile/WFwebview;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 877
    const-string v0, "CheckDepositJavaScriptInterface"

    const-string v1, "enterCheckDepositFlow() - nonce not valid"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 880
    :cond_0
    return-void
.end method

.method public retakePhotos()V
    .locals 1

    .prologue
    .line 883
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview$CheckDepositJavaScriptInterface;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static {v0}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/wf/wellsfargomobile/WFApp;->clearImageData()V

    .line 884
    return-void
.end method

.method public submitDeposit(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .parameter "nonce"
    .parameter "amount"
    .parameter "selectedAccount"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/charset/IllegalCharsetNameException;,
            Ljava/nio/charset/UnsupportedCharsetException;,
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 838
    const-string v0, "CheckDepositJavaScriptInterface"

    const-string v1, "in submitDeposit() "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 839
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview$CheckDepositJavaScriptInterface;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #calls: Lcom/wf/wellsfargomobile/WFwebview;->validNonce(Ljava/lang/String;)Z
    invoke-static {v0, p1}, Lcom/wf/wellsfargomobile/WFwebview;->access$1600(Lcom/wf/wellsfargomobile/WFwebview;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 840
    const-string v0, "CheckDepositJavaScriptInterface"

    const-string v1, "submitDeposit() - nonce not valid"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 848
    :goto_0
    return-void

    .line 845
    :cond_0
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview$CheckDepositJavaScriptInterface;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->submitDepositProgressDialog:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/wf/wellsfargomobile/WFwebview;->access$1700(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 847
    new-instance v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;

    iget-object v1, p0, Lcom/wf/wellsfargomobile/WFwebview$CheckDepositJavaScriptInterface;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview$CheckDepositJavaScriptInterface;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2, p2, p3}, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;-><init>(Lcom/wf/wellsfargomobile/WFwebview;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public takePicture(Ljava/lang/String;)V
    .locals 3
    .parameter "nonce"

    .prologue
    .line 803
    iget-object v1, p0, Lcom/wf/wellsfargomobile/WFwebview$CheckDepositJavaScriptInterface;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #calls: Lcom/wf/wellsfargomobile/WFwebview;->validNonce(Ljava/lang/String;)Z
    invoke-static {v1, p1}, Lcom/wf/wellsfargomobile/WFwebview;->access$1600(Lcom/wf/wellsfargomobile/WFwebview;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 804
    const-string v1, "CheckDepositJavaScriptInterface"

    const-string v2, "takePicture() - nonce not valid"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 811
    :goto_0
    return-void

    .line 808
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/wf/wellsfargomobile/WFwebview$CheckDepositJavaScriptInterface;->mContext:Landroid/content/Context;

    const-class v2, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 809
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "MRDC_FLOW_STATE"

    sget-object v2, Lcom/wf/wellsfargomobile/mrdc/ImageMode;->FRONT:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 810
    iget-object v1, p0, Lcom/wf/wellsfargomobile/WFwebview$CheckDepositJavaScriptInterface;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/wf/wellsfargomobile/WFwebview;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method
