.class public Lcom/wf/wellsfargomobile/WFwebview$CommonJavaScriptInterface;
.super Ljava/lang/Object;
.source "WFwebview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wf/wellsfargomobile/WFwebview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CommonJavaScriptInterface"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "CommonJavaScriptInterface"


# instance fields
.field private final mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/wf/wellsfargomobile/WFwebview;


# direct methods
.method public constructor <init>(Lcom/wf/wellsfargomobile/WFwebview;Landroid/content/Context;)V
    .locals 0
    .parameter
    .parameter "c"

    .prologue
    .line 750
    iput-object p1, p0, Lcom/wf/wellsfargomobile/WFwebview$CommonJavaScriptInterface;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 751
    iput-object p2, p0, Lcom/wf/wellsfargomobile/WFwebview$CommonJavaScriptInterface;->mContext:Landroid/content/Context;

    .line 752
    return-void
.end method


# virtual methods
.method public signOut(Ljava/lang/String;)V
    .locals 2
    .parameter "nonce"

    .prologue
    .line 760
    const-string v0, "CommonJavaScriptInterface"

    const-string v1, "CommonJavaScriptInterface.signOut() "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 762
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview$CommonJavaScriptInterface;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    #setter for: Lcom/wf/wellsfargomobile/WFwebview;->inSession:Ljava/lang/Boolean;
    invoke-static {v0, v1}, Lcom/wf/wellsfargomobile/WFwebview;->access$602(Lcom/wf/wellsfargomobile/WFwebview;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 763
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview$CommonJavaScriptInterface;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static {v0}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/wf/wellsfargomobile/WFApp;->clearCheckDepositData()V

    .line 764
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview$CommonJavaScriptInterface;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static {v0}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/wf/wellsfargomobile/WFApp;->setNonce(Ljava/lang/String;)V

    .line 767
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview$CommonJavaScriptInterface;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    new-instance v1, Lcom/wf/wellsfargomobile/WFwebview$CommonJavaScriptInterface$1;

    invoke-direct {v1, p0}, Lcom/wf/wellsfargomobile/WFwebview$CommonJavaScriptInterface$1;-><init>(Lcom/wf/wellsfargomobile/WFwebview$CommonJavaScriptInterface;)V

    invoke-virtual {v0, v1}, Lcom/wf/wellsfargomobile/WFwebview;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 775
    return-void
.end method

.method public signonError(Ljava/lang/String;)V
    .locals 3
    .parameter "errorMessage"

    .prologue
    .line 778
    const-string v0, "CommonJavaScriptInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "received notification of a signonError: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 781
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview$CommonJavaScriptInterface;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    iget-object v1, p0, Lcom/wf/wellsfargomobile/WFwebview$CommonJavaScriptInterface;->mContext:Landroid/content/Context;

    const v2, 0x7f07005e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    #calls: Lcom/wf/wellsfargomobile/WFwebview;->returnWithError(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, p1}, Lcom/wf/wellsfargomobile/WFwebview;->access$1300(Lcom/wf/wellsfargomobile/WFwebview;Ljava/lang/String;Ljava/lang/String;)V

    .line 782
    return-void
.end method
