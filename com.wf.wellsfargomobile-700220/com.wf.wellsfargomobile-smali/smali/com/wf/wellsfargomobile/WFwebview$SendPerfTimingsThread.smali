.class Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;
.super Landroid/os/AsyncTask;
.source "WFwebview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wf/wellsfargomobile/WFwebview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SendPerfTimingsThread"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field aContext:Landroid/content/Context;

.field d1_front_image_start_send:J

.field d2_front_image_json_response:J

.field d3_rear_image_start_send:J

.field d4_rear_image_json_response:J

.field d5_data_start_send:J

.field d6_data_json_response:J

.field final synthetic this$0:Lcom/wf/wellsfargomobile/WFwebview;


# direct methods
.method public constructor <init>(Lcom/wf/wellsfargomobile/WFwebview;Landroid/content/Context;JJJJJJ)V
    .locals 0
    .parameter
    .parameter "aContext"
    .parameter "d1_front_image_start_send"
    .parameter "d2_front_image_json_response"
    .parameter "d3_rear_image_start_send"
    .parameter "d4_rear_image_json_response"
    .parameter "d5_data_start_send"
    .parameter "d6_data_json_response"

    .prologue
    .line 1272
    iput-object p1, p0, Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    .line 1273
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1274
    iput-object p2, p0, Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;->aContext:Landroid/content/Context;

    .line 1275
    iput-wide p3, p0, Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;->d1_front_image_start_send:J

    .line 1276
    iput-wide p5, p0, Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;->d2_front_image_json_response:J

    .line 1277
    iput-wide p7, p0, Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;->d3_rear_image_start_send:J

    .line 1278
    iput-wide p9, p0, Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;->d4_rear_image_json_response:J

    .line 1279
    iput-wide p11, p0, Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;->d5_data_start_send:J

    .line 1280
    iput-wide p13, p0, Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;->d6_data_json_response:J

    .line 1281
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 1257
    check-cast p1, [Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 19
    .parameter "arg0"

    .prologue
    .line 1286
    const/4 v2, 0x0

    .line 1289
    .local v2, client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :try_start_0
    const-string v12, "WFwebview"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "SendPerfTimingsThread - d1: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;->d1_front_image_start_send:J

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " d2: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;->d2_front_image_json_response:J

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " d3: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;->d3_rear_image_start_send:J

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " d4: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;->d4_rear_image_json_response:J

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " d5: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;->d5_data_start_send:J

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " d6: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;->d6_data_json_response:J

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1295
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;->aContext:Landroid/content/Context;

    invoke-static {v12}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 1296
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v5

    .line 1297
    .local v5, cookieSyncManager:Landroid/webkit/CookieSyncManager;
    invoke-virtual {v5}, Landroid/webkit/CookieSyncManager;->sync()V

    .line 1299
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v4

    .line 1300
    .local v4, cookieManager:Landroid/webkit/CookieManager;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;->aContext:Landroid/content/Context;

    const v13, 0x7f070001

    invoke-virtual {v12, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v4, v12}, Landroid/webkit/CookieManager;->getCookie(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1302
    .local v6, cookies:Ljava/lang/String;
    const-string v12, "WFwebview"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "cookies: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1306
    new-instance v3, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4

    .line 1307
    .end local v2           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .local v3, client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :try_start_1
    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v7

    .line 1308
    .local v7, httpParams:Lorg/apache/http/params/HttpParams;
    const-string v12, "http.socket.timeout"

    const-wide/32 v13, 0x493e0

    invoke-interface {v7, v12, v13, v14}, Lorg/apache/http/params/HttpParams;->getLongParameter(Ljava/lang/String;J)J

    .line 1309
    const-string v12, "http.connection.timeout"

    const-wide/32 v13, 0x493e0

    invoke-interface {v7, v12, v13, v14}, Lorg/apache/http/params/HttpParams;->getLongParameter(Ljava/lang/String;J)J

    .line 1310
    invoke-virtual {v3, v7}, Lorg/apache/http/impl/client/DefaultHttpClient;->setParams(Lorg/apache/http/params/HttpParams;)V

    .line 1312
    new-instance v9, Lorg/apache/http/client/methods/HttpPost;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;->aContext:Landroid/content/Context;

    const v14, 0x7f070001

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/deposit/performance.action"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v9, v12}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 1316
    .local v9, request:Lorg/apache/http/client/methods/HttpPost;
    const-string v12, "Cookie"

    invoke-virtual {v9, v12, v6}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1318
    new-instance v8, Ljava/util/ArrayList;

    const/4 v12, 0x6

    invoke-direct {v8, v12}, Ljava/util/ArrayList;-><init>(I)V

    .line 1326
    .local v8, nameValuePairs:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v12, Lorg/apache/http/message/BasicNameValuePair;

    const-string v13, "D2D1"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, ""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-wide v15, v0, Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;->d2_front_image_json_response:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;->d1_front_image_start_send:J

    move-wide/from16 v17, v0

    sub-long v15, v15, v17

    invoke-virtual/range {v14 .. v16}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v12, v13, v14}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1328
    new-instance v12, Lorg/apache/http/message/BasicNameValuePair;

    const-string v13, "D4D3"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, ""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-wide v15, v0, Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;->d4_rear_image_json_response:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;->d3_rear_image_start_send:J

    move-wide/from16 v17, v0

    sub-long v15, v15, v17

    invoke-virtual/range {v14 .. v16}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v12, v13, v14}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1330
    new-instance v12, Lorg/apache/http/message/BasicNameValuePair;

    const-string v13, "D6D5"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, ""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-wide v15, v0, Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;->d6_data_json_response:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;->d5_data_start_send:J

    move-wide/from16 v17, v0

    sub-long v15, v15, v17

    invoke-virtual/range {v14 .. v16}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v12, v13, v14}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1332
    new-instance v12, Lorg/apache/http/message/BasicNameValuePair;

    const-string v13, "D6D1"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, ""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-wide v15, v0, Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;->d6_data_json_response:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;->d1_front_image_start_send:J

    move-wide/from16 v17, v0

    sub-long v15, v15, v17

    invoke-virtual/range {v14 .. v16}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v12, v13, v14}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1335
    new-instance v12, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    invoke-direct {v12, v8}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;)V

    invoke-virtual {v9, v12}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 1337
    invoke-virtual {v3, v9}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v10

    .line 1339
    .local v10, response:Lorg/apache/http/HttpResponse;
    invoke-interface {v10}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v11

    .line 1341
    .local v11, responseEntity:Lorg/apache/http/HttpEntity;
    const-string v12, "WFwebview"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "responseEntity: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {v11}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    .line 1355
    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v12

    invoke-interface {v12}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    move-object v2, v3

    .line 1358
    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .end local v4           #cookieManager:Landroid/webkit/CookieManager;
    .end local v5           #cookieSyncManager:Landroid/webkit/CookieSyncManager;
    .end local v6           #cookies:Ljava/lang/String;
    .end local v7           #httpParams:Lorg/apache/http/params/HttpParams;
    .end local v8           #nameValuePairs:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .end local v9           #request:Lorg/apache/http/client/methods/HttpPost;
    .end local v10           #response:Lorg/apache/http/HttpResponse;
    .end local v11           #responseEntity:Lorg/apache/http/HttpEntity;
    .restart local v2       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :goto_0
    const/4 v12, 0x0

    return-object v12

    .line 1343
    :catch_0
    move-exception v12

    .line 1355
    :goto_1
    invoke-virtual {v2}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v12

    invoke-interface {v12}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    goto :goto_0

    .line 1345
    :catch_1
    move-exception v12

    .line 1355
    :goto_2
    invoke-virtual {v2}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v12

    invoke-interface {v12}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    goto :goto_0

    .line 1347
    :catch_2
    move-exception v12

    .line 1355
    :goto_3
    invoke-virtual {v2}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v12

    invoke-interface {v12}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    goto :goto_0

    .line 1349
    :catch_3
    move-exception v12

    .line 1355
    :goto_4
    invoke-virtual {v2}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v12

    invoke-interface {v12}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    goto :goto_0

    .line 1351
    :catch_4
    move-exception v12

    .line 1355
    :goto_5
    invoke-virtual {v2}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v12

    invoke-interface {v12}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    goto :goto_0

    :catchall_0
    move-exception v12

    :goto_6
    invoke-virtual {v2}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v13

    invoke-interface {v13}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    throw v12

    .end local v2           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v4       #cookieManager:Landroid/webkit/CookieManager;
    .restart local v5       #cookieSyncManager:Landroid/webkit/CookieSyncManager;
    .restart local v6       #cookies:Ljava/lang/String;
    :catchall_1
    move-exception v12

    move-object v2, v3

    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v2       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    goto :goto_6

    .line 1351
    .end local v2           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :catch_5
    move-exception v12

    move-object v2, v3

    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v2       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    goto :goto_5

    .line 1349
    .end local v2           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :catch_6
    move-exception v12

    move-object v2, v3

    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v2       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    goto :goto_4

    .line 1347
    .end local v2           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :catch_7
    move-exception v12

    move-object v2, v3

    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v2       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    goto :goto_3

    .line 1345
    .end local v2           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :catch_8
    move-exception v12

    move-object v2, v3

    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v2       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    goto :goto_2

    .line 1343
    .end local v2           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :catch_9
    move-exception v12

    move-object v2, v3

    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v2       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    goto :goto_1
.end method
