.class Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;
.super Landroid/os/AsyncTask;
.source "WFwebview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wf/wellsfargomobile/WFwebview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SubmitDepositThread"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field aContext:Landroid/content/Context;

.field amount:Ljava/lang/String;

.field selectedAccount:Ljava/lang/String;

.field final synthetic this$0:Lcom/wf/wellsfargomobile/WFwebview;


# direct methods
.method public constructor <init>(Lcom/wf/wellsfargomobile/WFwebview;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter "aContext"
    .parameter "amount"
    .parameter "selectedAccount"

    .prologue
    .line 917
    iput-object p1, p0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 918
    iput-object p2, p0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->aContext:Landroid/content/Context;

    .line 919
    iput-object p3, p0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->amount:Ljava/lang/String;

    .line 920
    iput-object p4, p0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->selectedAccount:Ljava/lang/String;

    .line 921
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 912
    check-cast p1, [Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 22
    .parameter "asyncParams"

    .prologue
    .line 927
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v18, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v18 .. v18}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v18

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v19

    invoke-virtual/range {v18 .. v20}, Lcom/wf/wellsfargomobile/WFApp;->setPerfD5DataStartSend(J)V

    .line 929
    const/16 v17, 0x1

    .line 930
    .local v17, uploadSucceded:Z
    const/4 v3, 0x0

    .line 934
    .local v3, client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->aContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 935
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v6

    .line 936
    .local v6, cookieSyncManager:Landroid/webkit/CookieSyncManager;
    invoke-virtual {v6}, Landroid/webkit/CookieSyncManager;->sync()V

    .line 938
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v5

    .line 939
    .local v5, cookieManager:Landroid/webkit/CookieManager;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->aContext:Landroid/content/Context;

    move-object/from16 v18, v0

    const v19, 0x7f070001

    invoke-virtual/range {v18 .. v19}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Landroid/webkit/CookieManager;->getCookie(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 941
    .local v7, cookies:Ljava/lang/String;
    const-string v18, "WFwebview"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "cookies: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 945
    new-instance v4, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v4}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5

    .line 946
    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .local v4, client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :try_start_1
    invoke-virtual {v4}, Lorg/apache/http/impl/client/DefaultHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v9

    .line 947
    .local v9, httpParams:Lorg/apache/http/params/HttpParams;
    const-string v18, "http.socket.timeout"

    const-wide/32 v19, 0x1d4c0

    move-object/from16 v0, v18

    move-wide/from16 v1, v19

    invoke-interface {v9, v0, v1, v2}, Lorg/apache/http/params/HttpParams;->getLongParameter(Ljava/lang/String;J)J

    .line 949
    const-string v18, "http.connection.timeout"

    const-wide/32 v19, 0x1d4c0

    move-object/from16 v0, v18

    move-wide/from16 v1, v19

    invoke-interface {v9, v0, v1, v2}, Lorg/apache/http/params/HttpParams;->getLongParameter(Ljava/lang/String;J)J

    .line 951
    invoke-virtual {v4, v9}, Lorg/apache/http/impl/client/DefaultHttpClient;->setParams(Lorg/apache/http/params/HttpParams;)V

    .line 953
    new-instance v12, Lorg/apache/http/client/methods/HttpPost;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->aContext:Landroid/content/Context;

    move-object/from16 v19, v0

    const v20, 0x7f070001

    invoke-virtual/range {v19 .. v20}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/deposit/submitDeposit.action"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v12, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 957
    .local v12, request:Lorg/apache/http/client/methods/HttpPost;
    const-string v18, "Cookie"

    move-object/from16 v0, v18

    invoke-virtual {v12, v0, v7}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 959
    new-instance v11, Lorg/apache/http/entity/mime/MultipartEntity;

    invoke-direct {v11}, Lorg/apache/http/entity/mime/MultipartEntity;-><init>()V

    .line 960
    .local v11, mp:Lorg/apache/http/entity/mime/MultipartEntity;
    const-string v18, "frontImageId"

    new-instance v19, Lorg/apache/http/entity/mime/content/StringBody;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v20, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v20 .. v20}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/wf/wellsfargomobile/WFApp;->getFrontImageToken()Ljava/lang/String;

    move-result-object v20

    const-string v21, "UTF-8"

    invoke-static/range {v21 .. v21}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v21

    invoke-direct/range {v19 .. v21}, Lorg/apache/http/entity/mime/content/StringBody;-><init>(Ljava/lang/String;Ljava/nio/charset/Charset;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v11, v0, v1}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 962
    const-string v18, "backImageId"

    new-instance v19, Lorg/apache/http/entity/mime/content/StringBody;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v20, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v20 .. v20}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/wf/wellsfargomobile/WFApp;->getRearImageToken()Ljava/lang/String;

    move-result-object v20

    const-string v21, "UTF-8"

    invoke-static/range {v21 .. v21}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v21

    invoke-direct/range {v19 .. v21}, Lorg/apache/http/entity/mime/content/StringBody;-><init>(Ljava/lang/String;Ljava/nio/charset/Charset;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v11, v0, v1}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 964
    const-string v18, "amount"

    new-instance v19, Lorg/apache/http/entity/mime/content/StringBody;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->amount:Ljava/lang/String;

    move-object/from16 v20, v0

    const-string v21, "UTF-8"

    invoke-static/range {v21 .. v21}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v21

    invoke-direct/range {v19 .. v21}, Lorg/apache/http/entity/mime/content/StringBody;-><init>(Ljava/lang/String;Ljava/nio/charset/Charset;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v11, v0, v1}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 965
    const-string v18, "account"

    new-instance v19, Lorg/apache/http/entity/mime/content/StringBody;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->selectedAccount:Ljava/lang/String;

    move-object/from16 v20, v0

    const-string v21, "UTF-8"

    invoke-static/range {v21 .. v21}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v21

    invoke-direct/range {v19 .. v21}, Lorg/apache/http/entity/mime/content/StringBody;-><init>(Ljava/lang/String;Ljava/nio/charset/Charset;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v11, v0, v1}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 966
    const-string v18, "WFAppId"

    new-instance v19, Lorg/apache/http/entity/mime/content/StringBody;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->aContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x7f070003

    invoke-virtual/range {v20 .. v21}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v20

    const-string v21, "UTF-8"

    invoke-static/range {v21 .. v21}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v21

    invoke-direct/range {v19 .. v21}, Lorg/apache/http/entity/mime/content/StringBody;-><init>(Ljava/lang/String;Ljava/nio/charset/Charset;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v11, v0, v1}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 970
    invoke-virtual {v12, v11}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 972
    invoke-virtual {v4, v12}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v13

    .line 974
    .local v13, response:Lorg/apache/http/HttpResponse;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v18, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v18 .. v18}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v18

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v19

    invoke-virtual/range {v18 .. v20}, Lcom/wf/wellsfargomobile/WFApp;->setPerfD6DataJsonResponse(J)V

    .line 976
    invoke-interface {v13}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v14

    .line 978
    .local v14, responseEntity:Lorg/apache/http/HttpEntity;
    invoke-static {v14}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v15

    .line 980
    .local v15, responseEntityStr:Ljava/lang/String;
    const-string v18, "WFwebview"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "responseEntity: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 983
    const-string v18, "/*--safejson--"

    const-string v19, ""

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v15, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v15

    .line 984
    const-string v18, "--safejson--*/"

    const-string v19, ""

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v15, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v15

    .line 985
    const-string v18, "WFwebview"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "responseEntity: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6

    .line 988
    :try_start_2
    new-instance v10, Lorg/json/JSONObject;

    invoke-direct {v10, v15}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 990
    .local v10, jsonResponse:Lorg/json/JSONObject;
    const-string v18, "status"

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 991
    .local v16, status:Ljava/lang/String;
    const-string v18, "Failed"

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_2 .. :try_end_2} :catch_a
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_8
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6

    move-result v18

    if-eqz v18, :cond_0

    .line 992
    const/16 v17, 0x0

    .line 1017
    .end local v10           #jsonResponse:Lorg/json/JSONObject;
    .end local v16           #status:Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-virtual {v4}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 1018
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v18, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v18 .. v18}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/wf/wellsfargomobile/WFApp;->clearCheckDepositData()V

    .line 1021
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v18, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->submitDepositProgressDialog:Landroid/app/Dialog;
    invoke-static/range {v18 .. v18}, Lcom/wf/wellsfargomobile/WFwebview;->access$1700(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/app/Dialog;->isShowing()Z

    move-result v18

    if-eqz v18, :cond_4

    .line 1022
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v18, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->submitDepositProgressDialog:Landroid/app/Dialog;
    invoke-static/range {v18 .. v18}, Lcom/wf/wellsfargomobile/WFwebview;->access$1700(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/app/Dialog;->dismiss()V

    move-object v3, v4

    .line 1027
    .end local v4           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .end local v5           #cookieManager:Landroid/webkit/CookieManager;
    .end local v6           #cookieSyncManager:Landroid/webkit/CookieSyncManager;
    .end local v7           #cookies:Ljava/lang/String;
    .end local v9           #httpParams:Lorg/apache/http/params/HttpParams;
    .end local v11           #mp:Lorg/apache/http/entity/mime/MultipartEntity;
    .end local v12           #request:Lorg/apache/http/client/methods/HttpPost;
    .end local v13           #response:Lorg/apache/http/HttpResponse;
    .end local v14           #responseEntity:Lorg/apache/http/HttpEntity;
    .end local v15           #responseEntityStr:Ljava/lang/String;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :cond_1
    :goto_1
    if-eqz v17, :cond_3

    .line 1029
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v18, v0

    new-instance v19, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread$1;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread$1;-><init>(Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;)V

    invoke-virtual/range {v18 .. v19}, Lcom/wf/wellsfargomobile/WFwebview;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1052
    :goto_2
    const/16 v18, 0x0

    return-object v18

    .line 995
    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v4       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v5       #cookieManager:Landroid/webkit/CookieManager;
    .restart local v6       #cookieSyncManager:Landroid/webkit/CookieSyncManager;
    .restart local v7       #cookies:Ljava/lang/String;
    .restart local v9       #httpParams:Lorg/apache/http/params/HttpParams;
    .restart local v11       #mp:Lorg/apache/http/entity/mime/MultipartEntity;
    .restart local v12       #request:Lorg/apache/http/client/methods/HttpPost;
    .restart local v13       #response:Lorg/apache/http/HttpResponse;
    .restart local v14       #responseEntity:Lorg/apache/http/HttpEntity;
    .restart local v15       #responseEntityStr:Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 996
    .local v8, e:Lorg/json/JSONException;
    :try_start_3
    const-string v18, "WFwebview"

    const-string v19, "JSON parsing problem"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v0, v1, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_3 .. :try_end_3} :catch_a
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_3 .. :try_end_3} :catch_9
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6

    .line 997
    const/16 v17, 0x0

    goto :goto_0

    .line 1000
    .end local v4           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .end local v5           #cookieManager:Landroid/webkit/CookieManager;
    .end local v6           #cookieSyncManager:Landroid/webkit/CookieSyncManager;
    .end local v7           #cookies:Ljava/lang/String;
    .end local v8           #e:Lorg/json/JSONException;
    .end local v9           #httpParams:Lorg/apache/http/params/HttpParams;
    .end local v11           #mp:Lorg/apache/http/entity/mime/MultipartEntity;
    .end local v12           #request:Lorg/apache/http/client/methods/HttpPost;
    .end local v13           #response:Lorg/apache/http/HttpResponse;
    .end local v14           #responseEntity:Lorg/apache/http/HttpEntity;
    .end local v15           #responseEntityStr:Ljava/lang/String;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :catch_1
    move-exception v8

    .line 1002
    .local v8, e:Ljava/nio/charset/IllegalCharsetNameException;
    :goto_3
    const/16 v17, 0x0

    .line 1017
    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 1018
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v18, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v18 .. v18}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/wf/wellsfargomobile/WFApp;->clearCheckDepositData()V

    .line 1021
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v18, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->submitDepositProgressDialog:Landroid/app/Dialog;
    invoke-static/range {v18 .. v18}, Lcom/wf/wellsfargomobile/WFwebview;->access$1700(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/app/Dialog;->isShowing()Z

    move-result v18

    if-eqz v18, :cond_1

    .line 1022
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v18, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->submitDepositProgressDialog:Landroid/app/Dialog;
    invoke-static/range {v18 .. v18}, Lcom/wf/wellsfargomobile/WFwebview;->access$1700(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/app/Dialog;->dismiss()V

    goto :goto_1

    .line 1003
    .end local v8           #e:Ljava/nio/charset/IllegalCharsetNameException;
    :catch_2
    move-exception v8

    .line 1005
    .local v8, e:Ljava/nio/charset/UnsupportedCharsetException;
    :goto_4
    const/16 v17, 0x0

    .line 1017
    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 1018
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v18, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v18 .. v18}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/wf/wellsfargomobile/WFApp;->clearCheckDepositData()V

    .line 1021
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v18, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->submitDepositProgressDialog:Landroid/app/Dialog;
    invoke-static/range {v18 .. v18}, Lcom/wf/wellsfargomobile/WFwebview;->access$1700(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/app/Dialog;->isShowing()Z

    move-result v18

    if-eqz v18, :cond_1

    .line 1022
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v18, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->submitDepositProgressDialog:Landroid/app/Dialog;
    invoke-static/range {v18 .. v18}, Lcom/wf/wellsfargomobile/WFwebview;->access$1700(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/app/Dialog;->dismiss()V

    goto/16 :goto_1

    .line 1006
    .end local v8           #e:Ljava/nio/charset/UnsupportedCharsetException;
    :catch_3
    move-exception v8

    .line 1008
    .local v8, e:Ljava/io/UnsupportedEncodingException;
    :goto_5
    const/16 v17, 0x0

    .line 1017
    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 1018
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v18, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v18 .. v18}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/wf/wellsfargomobile/WFApp;->clearCheckDepositData()V

    .line 1021
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v18, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->submitDepositProgressDialog:Landroid/app/Dialog;
    invoke-static/range {v18 .. v18}, Lcom/wf/wellsfargomobile/WFwebview;->access$1700(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/app/Dialog;->isShowing()Z

    move-result v18

    if-eqz v18, :cond_1

    .line 1022
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v18, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->submitDepositProgressDialog:Landroid/app/Dialog;
    invoke-static/range {v18 .. v18}, Lcom/wf/wellsfargomobile/WFwebview;->access$1700(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/app/Dialog;->dismiss()V

    goto/16 :goto_1

    .line 1009
    .end local v8           #e:Ljava/io/UnsupportedEncodingException;
    :catch_4
    move-exception v8

    .line 1011
    .local v8, e:Lorg/apache/http/client/ClientProtocolException;
    :goto_6
    const/16 v17, 0x0

    .line 1017
    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 1018
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v18, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v18 .. v18}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/wf/wellsfargomobile/WFApp;->clearCheckDepositData()V

    .line 1021
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v18, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->submitDepositProgressDialog:Landroid/app/Dialog;
    invoke-static/range {v18 .. v18}, Lcom/wf/wellsfargomobile/WFwebview;->access$1700(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/app/Dialog;->isShowing()Z

    move-result v18

    if-eqz v18, :cond_1

    .line 1022
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v18, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->submitDepositProgressDialog:Landroid/app/Dialog;
    invoke-static/range {v18 .. v18}, Lcom/wf/wellsfargomobile/WFwebview;->access$1700(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/app/Dialog;->dismiss()V

    goto/16 :goto_1

    .line 1012
    .end local v8           #e:Lorg/apache/http/client/ClientProtocolException;
    :catch_5
    move-exception v8

    .line 1014
    .local v8, e:Ljava/io/IOException;
    :goto_7
    const/16 v17, 0x0

    .line 1017
    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 1018
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v18, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v18 .. v18}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/wf/wellsfargomobile/WFApp;->clearCheckDepositData()V

    .line 1021
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v18, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->submitDepositProgressDialog:Landroid/app/Dialog;
    invoke-static/range {v18 .. v18}, Lcom/wf/wellsfargomobile/WFwebview;->access$1700(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/app/Dialog;->isShowing()Z

    move-result v18

    if-eqz v18, :cond_1

    .line 1022
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v18, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->submitDepositProgressDialog:Landroid/app/Dialog;
    invoke-static/range {v18 .. v18}, Lcom/wf/wellsfargomobile/WFwebview;->access$1700(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/app/Dialog;->dismiss()V

    goto/16 :goto_1

    .line 1017
    .end local v8           #e:Ljava/io/IOException;
    :catchall_0
    move-exception v18

    :goto_8
    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 1018
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v19, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v19 .. v19}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/wf/wellsfargomobile/WFApp;->clearCheckDepositData()V

    .line 1021
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v19, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->submitDepositProgressDialog:Landroid/app/Dialog;
    invoke-static/range {v19 .. v19}, Lcom/wf/wellsfargomobile/WFwebview;->access$1700(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/app/Dialog;->isShowing()Z

    move-result v19

    if-eqz v19, :cond_2

    .line 1022
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v19, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->submitDepositProgressDialog:Landroid/app/Dialog;
    invoke-static/range {v19 .. v19}, Lcom/wf/wellsfargomobile/WFwebview;->access$1700(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/app/Dialog;->dismiss()V

    :cond_2
    throw v18

    .line 1041
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v18, v0

    new-instance v19, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread$2;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread$2;-><init>(Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;)V

    invoke-virtual/range {v18 .. v19}, Lcom/wf/wellsfargomobile/WFwebview;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_2

    .line 1017
    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v4       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v5       #cookieManager:Landroid/webkit/CookieManager;
    .restart local v6       #cookieSyncManager:Landroid/webkit/CookieSyncManager;
    .restart local v7       #cookies:Ljava/lang/String;
    :catchall_1
    move-exception v18

    move-object v3, v4

    .end local v4           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    goto :goto_8

    .line 1012
    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v4       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :catch_6
    move-exception v8

    move-object v3, v4

    .end local v4           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    goto/16 :goto_7

    .line 1009
    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v4       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :catch_7
    move-exception v8

    move-object v3, v4

    .end local v4           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    goto/16 :goto_6

    .line 1006
    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v4       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :catch_8
    move-exception v8

    move-object v3, v4

    .end local v4           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    goto/16 :goto_5

    .line 1003
    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v4       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :catch_9
    move-exception v8

    move-object v3, v4

    .end local v4           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    goto/16 :goto_4

    .line 1000
    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v4       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :catch_a
    move-exception v8

    move-object v3, v4

    .end local v4           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    goto/16 :goto_3

    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v4       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v9       #httpParams:Lorg/apache/http/params/HttpParams;
    .restart local v11       #mp:Lorg/apache/http/entity/mime/MultipartEntity;
    .restart local v12       #request:Lorg/apache/http/client/methods/HttpPost;
    .restart local v13       #response:Lorg/apache/http/HttpResponse;
    .restart local v14       #responseEntity:Lorg/apache/http/HttpEntity;
    .restart local v15       #responseEntityStr:Ljava/lang/String;
    :cond_4
    move-object v3, v4

    .end local v4           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    goto/16 :goto_1
.end method
