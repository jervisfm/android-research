.class Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;
.super Landroid/os/AsyncTask;
.source "WFwebview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wf/wellsfargomobile/WFwebview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SubmitRearImageThread"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SubmitRearImageThread"


# instance fields
.field private aContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/wf/wellsfargomobile/WFwebview;


# direct methods
.method public constructor <init>(Lcom/wf/wellsfargomobile/WFwebview;Landroid/content/Context;)V
    .locals 0
    .parameter
    .parameter "aContext"

    .prologue
    .line 1066
    iput-object p1, p0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1067
    iput-object p2, p0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->aContext:Landroid/content/Context;

    .line 1068
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 27
    .parameter "asyncParams"

    .prologue
    .line 1074
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v23, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v23

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v24

    invoke-virtual/range {v23 .. v25}, Lcom/wf/wellsfargomobile/WFApp;->setPerfD3RearImageStartSend(J)V

    .line 1076
    const/16 v22, 0x1

    .line 1077
    .local v22, uploadSucceded:Z
    const/4 v3, 0x0

    .line 1080
    .local v3, client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFwebview;->getApplication()Landroid/app/Application;

    move-result-object v23

    check-cast v23, Lcom/wf/wellsfargomobile/WFApp;

    invoke-virtual/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFApp;->getRearCheckImage()[B

    move-result-object v14

    .line 1083
    .local v14, rearImage:[B
    if-nez v14, :cond_2

    .line 1084
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v23, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->submitRearImageProgressDialog:Landroid/app/Dialog;
    invoke-static/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFwebview;->access$1400(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/app/Dialog;->isShowing()Z

    move-result v23

    if-eqz v23, :cond_0

    .line 1085
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v23, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->submitRearImageProgressDialog:Landroid/app/Dialog;
    invoke-static/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFwebview;->access$1400(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/app/Dialog;->dismiss()V

    .line 1088
    :cond_0
    const-string v23, "SubmitRearImageThread"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "image(s) were null - rearImage: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1090
    const/16 v23, 0x0

    invoke-static/range {v23 .. v23}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5

    move-result-object v23

    .line 1178
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v24

    if-eqz v24, :cond_1

    .line 1179
    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 1182
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v24, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v24 .. v24}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v24

    const/16 v25, 0x1

    invoke-virtual/range {v24 .. v25}, Lcom/wf/wellsfargomobile/WFApp;->setRearImageUploadProcessComplete(Z)V

    .line 1184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v24, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v24 .. v24}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v24

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Lcom/wf/wellsfargomobile/WFApp;->setRearCheckImage([B)V

    .line 1185
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v24, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v24 .. v24}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v24

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Lcom/wf/wellsfargomobile/WFApp;->setRearCheckPreviewImage(Landroid/graphics/Bitmap;)V

    .line 1226
    .end local v14           #rearImage:[B
    :goto_0
    return-object v23

    .line 1094
    .restart local v14       #rearImage:[B
    :cond_2
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->aContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 1095
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v6

    .line 1096
    .local v6, cookieSyncManager:Landroid/webkit/CookieSyncManager;
    invoke-virtual {v6}, Landroid/webkit/CookieSyncManager;->sync()V

    .line 1098
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v5

    .line 1099
    .local v5, cookieManager:Landroid/webkit/CookieManager;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->aContext:Landroid/content/Context;

    move-object/from16 v23, v0

    const v24, 0x7f070001

    invoke-virtual/range {v23 .. v24}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Landroid/webkit/CookieManager;->getCookie(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1101
    .local v7, cookies:Ljava/lang/String;
    const-string v23, "SubmitRearImageThread"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "cookies: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1104
    new-instance v4, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v4}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    .line 1107
    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .local v4, client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :try_start_2
    invoke-virtual {v4}, Lorg/apache/http/impl/client/DefaultHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v9

    .line 1108
    .local v9, httpParams:Lorg/apache/http/params/HttpParams;
    const-string v23, "http.socket.timeout"

    const-wide/32 v24, 0x493e0

    move-object/from16 v0, v23

    move-wide/from16 v1, v24

    invoke-interface {v9, v0, v1, v2}, Lorg/apache/http/params/HttpParams;->getLongParameter(Ljava/lang/String;J)J

    .line 1109
    const-string v23, "http.connection.timeout"

    const-wide/32 v24, 0x493e0

    move-object/from16 v0, v23

    move-wide/from16 v1, v24

    invoke-interface {v9, v0, v1, v2}, Lorg/apache/http/params/HttpParams;->getLongParameter(Ljava/lang/String;J)J

    .line 1110
    invoke-virtual {v4, v9}, Lorg/apache/http/impl/client/DefaultHttpClient;->setParams(Lorg/apache/http/params/HttpParams;)V

    .line 1112
    new-instance v16, Lorg/apache/http/client/methods/HttpPost;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->aContext:Landroid/content/Context;

    move-object/from16 v24, v0

    const v25, 0x7f070001

    invoke-virtual/range {v24 .. v25}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "/deposit/submitBackImage.action"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v16

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 1116
    .local v16, request:Lorg/apache/http/client/methods/HttpPost;
    const-string v23, "Cookie"

    move-object/from16 v0, v16

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v7}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1118
    const-string v23, "SubmitRearImageThread"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "rearImage size: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    array-length v0, v14

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " bytes"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1120
    new-instance v13, Lorg/apache/http/entity/mime/MultipartEntity;

    invoke-direct {v13}, Lorg/apache/http/entity/mime/MultipartEntity;-><init>()V

    .line 1121
    .local v13, mp:Lorg/apache/http/entity/mime/MultipartEntity;
    const-string v23, "backImage"

    new-instance v24, Lorg/apache/http/entity/mime/content/ByteArrayBody;

    const-string v25, "image/jpeg"

    const-string v26, "back.jpg"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    invoke-direct {v0, v14, v1, v2}, Lorg/apache/http/entity/mime/content/ByteArrayBody;-><init>([BLjava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v13, v0, v1}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 1122
    const-string v23, "WFAppId"

    new-instance v24, Lorg/apache/http/entity/mime/content/StringBody;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->aContext:Landroid/content/Context;

    move-object/from16 v25, v0

    const v26, 0x7f070003

    invoke-virtual/range {v25 .. v26}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v25

    const-string v26, "UTF-8"

    invoke-static/range {v26 .. v26}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v26

    invoke-direct/range {v24 .. v26}, Lorg/apache/http/entity/mime/content/StringBody;-><init>(Ljava/lang/String;Ljava/nio/charset/Charset;)V

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v13, v0, v1}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 1126
    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 1128
    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v17

    .line 1130
    .local v17, response:Lorg/apache/http/HttpResponse;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v23, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v23

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v24

    invoke-virtual/range {v23 .. v25}, Lcom/wf/wellsfargomobile/WFApp;->setPerfD4RearImageJsonResponse(J)V

    .line 1132
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v23, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v23

    const/16 v24, 0x1

    invoke-virtual/range {v23 .. v24}, Lcom/wf/wellsfargomobile/WFApp;->setRearImageUploadProcessComplete(Z)V

    .line 1134
    invoke-interface/range {v17 .. v17}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v18

    .line 1136
    .local v18, responseEntity:Lorg/apache/http/HttpEntity;
    invoke-static/range {v18 .. v18}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v19

    .line 1138
    .local v19, responseEntityStr:Ljava/lang/String;
    const-string v23, "SubmitRearImageThread"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "responseEntity: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1141
    const-string v23, "/*--safejson--"

    const-string v24, ""

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v19

    .line 1142
    const-string v23, "--safejson--*/"

    const-string v24, ""

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v19

    .line 1143
    const-string v23, "SubmitRearImageThread"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "responseEntity: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_2 .. :try_end_2} :catch_b
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_2 .. :try_end_2} :catch_a
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_2 .. :try_end_2} :catch_8
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7

    .line 1146
    :try_start_3
    new-instance v10, Lorg/json/JSONObject;

    move-object/from16 v0, v19

    invoke-direct {v10, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1148
    .local v10, jsonResponse:Lorg/json/JSONObject;
    const-string v23, "status"

    move-object/from16 v0, v23

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 1149
    .local v20, status:Ljava/lang/String;
    const-string v23, "Failed"

    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_3 .. :try_end_3} :catch_b
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_3 .. :try_end_3} :catch_a
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_3 .. :try_end_3} :catch_9
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_7

    move-result v23

    if-eqz v23, :cond_4

    .line 1150
    const/16 v22, 0x0

    .line 1178
    .end local v10           #jsonResponse:Lorg/json/JSONObject;
    .end local v20           #status:Ljava/lang/String;
    :goto_1
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v23

    if-eqz v23, :cond_3

    .line 1179
    invoke-virtual {v4}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 1182
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v23, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v23

    const/16 v24, 0x1

    invoke-virtual/range {v23 .. v24}, Lcom/wf/wellsfargomobile/WFApp;->setRearImageUploadProcessComplete(Z)V

    .line 1184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v23, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v23

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Lcom/wf/wellsfargomobile/WFApp;->setRearCheckImage([B)V

    .line 1185
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v23, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v23

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Lcom/wf/wellsfargomobile/WFApp;->setRearCheckPreviewImage(Landroid/graphics/Bitmap;)V

    move-object v3, v4

    .line 1189
    .end local v4           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .end local v5           #cookieManager:Landroid/webkit/CookieManager;
    .end local v6           #cookieSyncManager:Landroid/webkit/CookieSyncManager;
    .end local v7           #cookies:Ljava/lang/String;
    .end local v9           #httpParams:Lorg/apache/http/params/HttpParams;
    .end local v13           #mp:Lorg/apache/http/entity/mime/MultipartEntity;
    .end local v14           #rearImage:[B
    .end local v16           #request:Lorg/apache/http/client/methods/HttpPost;
    .end local v17           #response:Lorg/apache/http/HttpResponse;
    .end local v18           #responseEntity:Lorg/apache/http/HttpEntity;
    .end local v19           #responseEntityStr:Ljava/lang/String;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :goto_2
    const/16 v21, 0x0

    .line 1190
    .local v21, success:Z
    const/4 v11, 0x1

    .line 1191
    .local v11, keepLookingForFrontImageToken:Z
    const/4 v12, 0x0

    .line 1194
    .local v12, loopCount:I
    :goto_3
    if-eqz v11, :cond_e

    const/16 v23, 0x78

    move/from16 v0, v23

    if-ge v12, v0, :cond_e

    .line 1195
    if-eqz v22, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v23, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFApp;->getFrontImageToken()Ljava/lang/String;

    move-result-object v23

    if-eqz v23, :cond_b

    .line 1196
    const/16 v21, 0x1

    .line 1197
    const/4 v11, 0x0

    goto :goto_3

    .line 1152
    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .end local v11           #keepLookingForFrontImageToken:Z
    .end local v12           #loopCount:I
    .end local v21           #success:Z
    .restart local v4       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v5       #cookieManager:Landroid/webkit/CookieManager;
    .restart local v6       #cookieSyncManager:Landroid/webkit/CookieSyncManager;
    .restart local v7       #cookies:Ljava/lang/String;
    .restart local v9       #httpParams:Lorg/apache/http/params/HttpParams;
    .restart local v10       #jsonResponse:Lorg/json/JSONObject;
    .restart local v13       #mp:Lorg/apache/http/entity/mime/MultipartEntity;
    .restart local v14       #rearImage:[B
    .restart local v16       #request:Lorg/apache/http/client/methods/HttpPost;
    .restart local v17       #response:Lorg/apache/http/HttpResponse;
    .restart local v18       #responseEntity:Lorg/apache/http/HttpEntity;
    .restart local v19       #responseEntityStr:Ljava/lang/String;
    .restart local v20       #status:Ljava/lang/String;
    :cond_4
    :try_start_4
    const-string v23, "backImageId"

    move-object/from16 v0, v23

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 1153
    .local v15, rearImageId:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v23, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v15}, Lcom/wf/wellsfargomobile/WFApp;->setRearImageToken(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_4 .. :try_end_4} :catch_b
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_4 .. :try_end_4} :catch_a
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_4 .. :try_end_4} :catch_9
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_4 .. :try_end_4} :catch_8
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_7

    goto :goto_1

    .line 1156
    .end local v10           #jsonResponse:Lorg/json/JSONObject;
    .end local v15           #rearImageId:Ljava/lang/String;
    .end local v20           #status:Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 1157
    .local v8, e:Lorg/json/JSONException;
    :try_start_5
    const-string v23, "SubmitRearImageThread"

    const-string v24, "JSON parsing problem"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-static {v0, v1, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_5 .. :try_end_5} :catch_b
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_5 .. :try_end_5} :catch_a
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_5 .. :try_end_5} :catch_9
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_5 .. :try_end_5} :catch_8
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_7

    .line 1158
    const/16 v22, 0x0

    goto/16 :goto_1

    .line 1161
    .end local v4           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .end local v5           #cookieManager:Landroid/webkit/CookieManager;
    .end local v6           #cookieSyncManager:Landroid/webkit/CookieSyncManager;
    .end local v7           #cookies:Ljava/lang/String;
    .end local v8           #e:Lorg/json/JSONException;
    .end local v9           #httpParams:Lorg/apache/http/params/HttpParams;
    .end local v13           #mp:Lorg/apache/http/entity/mime/MultipartEntity;
    .end local v14           #rearImage:[B
    .end local v16           #request:Lorg/apache/http/client/methods/HttpPost;
    .end local v17           #response:Lorg/apache/http/HttpResponse;
    .end local v18           #responseEntity:Lorg/apache/http/HttpEntity;
    .end local v19           #responseEntityStr:Ljava/lang/String;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :catch_1
    move-exception v8

    .line 1162
    .local v8, e:Ljava/nio/charset/IllegalCharsetNameException;
    :goto_4
    const/16 v22, 0x0

    .line 1178
    if-eqz v3, :cond_5

    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v23

    if-eqz v23, :cond_5

    .line 1179
    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 1182
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v23, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v23

    const/16 v24, 0x1

    invoke-virtual/range {v23 .. v24}, Lcom/wf/wellsfargomobile/WFApp;->setRearImageUploadProcessComplete(Z)V

    .line 1184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v23, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v23

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Lcom/wf/wellsfargomobile/WFApp;->setRearCheckImage([B)V

    .line 1185
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v23, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v23

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Lcom/wf/wellsfargomobile/WFApp;->setRearCheckPreviewImage(Landroid/graphics/Bitmap;)V

    goto/16 :goto_2

    .line 1164
    .end local v8           #e:Ljava/nio/charset/IllegalCharsetNameException;
    :catch_2
    move-exception v8

    .line 1165
    .local v8, e:Ljava/nio/charset/UnsupportedCharsetException;
    :goto_5
    const/16 v22, 0x0

    .line 1178
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v23

    if-eqz v23, :cond_6

    .line 1179
    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 1182
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v23, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v23

    const/16 v24, 0x1

    invoke-virtual/range {v23 .. v24}, Lcom/wf/wellsfargomobile/WFApp;->setRearImageUploadProcessComplete(Z)V

    .line 1184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v23, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v23

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Lcom/wf/wellsfargomobile/WFApp;->setRearCheckImage([B)V

    .line 1185
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v23, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v23

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Lcom/wf/wellsfargomobile/WFApp;->setRearCheckPreviewImage(Landroid/graphics/Bitmap;)V

    goto/16 :goto_2

    .line 1167
    .end local v8           #e:Ljava/nio/charset/UnsupportedCharsetException;
    :catch_3
    move-exception v8

    .line 1168
    .local v8, e:Ljava/io/UnsupportedEncodingException;
    :goto_6
    const/16 v22, 0x0

    .line 1178
    if-eqz v3, :cond_7

    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v23

    if-eqz v23, :cond_7

    .line 1179
    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 1182
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v23, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v23

    const/16 v24, 0x1

    invoke-virtual/range {v23 .. v24}, Lcom/wf/wellsfargomobile/WFApp;->setRearImageUploadProcessComplete(Z)V

    .line 1184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v23, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v23

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Lcom/wf/wellsfargomobile/WFApp;->setRearCheckImage([B)V

    .line 1185
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v23, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v23

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Lcom/wf/wellsfargomobile/WFApp;->setRearCheckPreviewImage(Landroid/graphics/Bitmap;)V

    goto/16 :goto_2

    .line 1170
    .end local v8           #e:Ljava/io/UnsupportedEncodingException;
    :catch_4
    move-exception v8

    .line 1171
    .local v8, e:Lorg/apache/http/client/ClientProtocolException;
    :goto_7
    const/16 v22, 0x0

    .line 1178
    if-eqz v3, :cond_8

    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v23

    if-eqz v23, :cond_8

    .line 1179
    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 1182
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v23, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v23

    const/16 v24, 0x1

    invoke-virtual/range {v23 .. v24}, Lcom/wf/wellsfargomobile/WFApp;->setRearImageUploadProcessComplete(Z)V

    .line 1184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v23, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v23

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Lcom/wf/wellsfargomobile/WFApp;->setRearCheckImage([B)V

    .line 1185
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v23, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v23

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Lcom/wf/wellsfargomobile/WFApp;->setRearCheckPreviewImage(Landroid/graphics/Bitmap;)V

    goto/16 :goto_2

    .line 1173
    .end local v8           #e:Lorg/apache/http/client/ClientProtocolException;
    :catch_5
    move-exception v8

    .line 1174
    .local v8, e:Ljava/io/IOException;
    :goto_8
    const/16 v22, 0x0

    .line 1178
    if-eqz v3, :cond_9

    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v23

    if-eqz v23, :cond_9

    .line 1179
    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 1182
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v23, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v23

    const/16 v24, 0x1

    invoke-virtual/range {v23 .. v24}, Lcom/wf/wellsfargomobile/WFApp;->setRearImageUploadProcessComplete(Z)V

    .line 1184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v23, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v23

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Lcom/wf/wellsfargomobile/WFApp;->setRearCheckImage([B)V

    .line 1185
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v23, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v23

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Lcom/wf/wellsfargomobile/WFApp;->setRearCheckPreviewImage(Landroid/graphics/Bitmap;)V

    goto/16 :goto_2

    .line 1178
    .end local v8           #e:Ljava/io/IOException;
    :catchall_0
    move-exception v23

    :goto_9
    if-eqz v3, :cond_a

    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v24

    if-eqz v24, :cond_a

    .line 1179
    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 1182
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v24, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v24 .. v24}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v24

    const/16 v25, 0x1

    invoke-virtual/range {v24 .. v25}, Lcom/wf/wellsfargomobile/WFApp;->setRearImageUploadProcessComplete(Z)V

    .line 1184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v24, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v24 .. v24}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v24

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Lcom/wf/wellsfargomobile/WFApp;->setRearCheckImage([B)V

    .line 1185
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v24, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v24 .. v24}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v24

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Lcom/wf/wellsfargomobile/WFApp;->setRearCheckPreviewImage(Landroid/graphics/Bitmap;)V

    throw v23

    .line 1198
    .restart local v11       #keepLookingForFrontImageToken:Z
    .restart local v12       #loopCount:I
    .restart local v21       #success:Z
    :cond_b
    if-nez v22, :cond_c

    .line 1199
    const-string v23, "SubmitRearImageThread"

    const-string v24, "rear image did not upload don\'t even look for the status of the front image"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1201
    const/16 v21, 0x0

    .line 1202
    const/4 v11, 0x0

    goto/16 :goto_3

    .line 1204
    :cond_c
    add-int/lit8 v12, v12, 0x1

    .line 1205
    const-string v23, "SubmitRearImageThread"

    const-string v24, "looping to wait for front image upload"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1208
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v23, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFApp;->isFrontImageUploadProcessComplete()Z

    move-result v23

    if-eqz v23, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v23, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFApp;->isRearImageUploadProcessComplete()Z

    move-result v23

    if-eqz v23, :cond_d

    .line 1210
    const/4 v11, 0x0

    goto/16 :goto_3

    .line 1213
    :cond_d
    const-wide/16 v23, 0x3e8

    :try_start_6
    invoke-static/range {v23 .. v24}, Ljava/lang/Thread;->sleep(J)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_6

    goto/16 :goto_3

    .line 1214
    :catch_6
    move-exception v8

    .line 1215
    .local v8, e:Ljava/lang/InterruptedException;
    const-string v23, "SubmitRearImageThread"

    const-string v24, "Front image upload wait loop interrupted."

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 1222
    .end local v8           #e:Ljava/lang/InterruptedException;
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v23, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->submitRearImageProgressDialog:Landroid/app/Dialog;
    invoke-static/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFwebview;->access$1400(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/app/Dialog;->isShowing()Z

    move-result v23

    if-eqz v23, :cond_f

    .line 1223
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    move-object/from16 v23, v0

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->submitRearImageProgressDialog:Landroid/app/Dialog;
    invoke-static/range {v23 .. v23}, Lcom/wf/wellsfargomobile/WFwebview;->access$1400(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/app/Dialog;->dismiss()V

    .line 1226
    :cond_f
    invoke-static/range {v21 .. v21}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v23

    goto/16 :goto_0

    .line 1178
    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .end local v11           #keepLookingForFrontImageToken:Z
    .end local v12           #loopCount:I
    .end local v21           #success:Z
    .restart local v4       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v5       #cookieManager:Landroid/webkit/CookieManager;
    .restart local v6       #cookieSyncManager:Landroid/webkit/CookieSyncManager;
    .restart local v7       #cookies:Ljava/lang/String;
    .restart local v14       #rearImage:[B
    :catchall_1
    move-exception v23

    move-object v3, v4

    .end local v4           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    goto/16 :goto_9

    .line 1173
    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v4       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :catch_7
    move-exception v8

    move-object v3, v4

    .end local v4           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    goto/16 :goto_8

    .line 1170
    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v4       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :catch_8
    move-exception v8

    move-object v3, v4

    .end local v4           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    goto/16 :goto_7

    .line 1167
    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v4       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :catch_9
    move-exception v8

    move-object v3, v4

    .end local v4           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    goto/16 :goto_6

    .line 1164
    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v4       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :catch_a
    move-exception v8

    move-object v3, v4

    .end local v4           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    goto/16 :goto_5

    .line 1161
    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v4       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :catch_b
    move-exception v8

    move-object v3, v4

    .end local v4           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    goto/16 :goto_4
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 1061
    check-cast p1, [Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 2
    .parameter "result"

    .prologue
    .line 1231
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1232
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    new-instance v1, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread$1;

    invoke-direct {v1, p0}, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread$1;-><init>(Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;)V

    invoke-virtual {v0, v1}, Lcom/wf/wellsfargomobile/WFwebview;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1254
    :goto_0
    return-void

    .line 1243
    :cond_0
    const-string v0, "SubmitRearImageThread"

    const-string v1, "failed execution of rear image upload, opeing retake image web page"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1244
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    new-instance v1, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread$2;

    invoke-direct {v1, p0}, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread$2;-><init>(Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;)V

    invoke-virtual {v0, v1}, Lcom/wf/wellsfargomobile/WFwebview;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 1061
    check-cast p1, Ljava/lang/Boolean;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
