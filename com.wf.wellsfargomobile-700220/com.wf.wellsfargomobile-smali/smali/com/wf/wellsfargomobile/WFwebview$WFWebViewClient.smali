.class Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "WFwebview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wf/wellsfargomobile/WFwebview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WFWebViewClient"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "WFWebViewClient"


# instance fields
.field final synthetic this$0:Lcom/wf/wellsfargomobile/WFwebview;


# direct methods
.method private constructor <init>(Lcom/wf/wellsfargomobile/WFwebview;)V
    .locals 0
    .parameter

    .prologue
    .line 287
    iput-object p1, p0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/wf/wellsfargomobile/WFwebview;Lcom/wf/wellsfargomobile/WFwebview$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 287
    invoke-direct {p0, p1}, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;-><init>(Lcom/wf/wellsfargomobile/WFwebview;)V

    return-void
.end method


# virtual methods
.method public onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1
    .parameter "view"
    .parameter "url"

    .prologue
    .line 411
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #calls: Lcom/wf/wellsfargomobile/WFwebview;->clearAndPreventCache()V
    invoke-static {v0}, Lcom/wf/wellsfargomobile/WFwebview;->access$1200(Lcom/wf/wellsfargomobile/WFwebview;)V

    .line 412
    return-void
.end method

.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 16
    .parameter "view"
    .parameter "url"

    .prologue
    .line 372
    const-string v1, "WFWebViewClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "entering onPageFinished() - url: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    const-string v1, "/deposit/depositDetail.action"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 378
    new-instance v1, Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    invoke-virtual/range {p1 .. p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static {v4}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/wf/wellsfargomobile/WFApp;->getPerfD1FrontImageStartSend()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static {v6}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/wf/wellsfargomobile/WFApp;->getPerfD2FrontImageJsonResponse()J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static {v8}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v8

    invoke-virtual {v8}, Lcom/wf/wellsfargomobile/WFApp;->getPerfD3RearImageStartSend()J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static {v10}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v10

    invoke-virtual {v10}, Lcom/wf/wellsfargomobile/WFApp;->getPerfD4RearImageJsonResponse()J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static {v12}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v12

    invoke-virtual {v12}, Lcom/wf/wellsfargomobile/WFApp;->getPerfD5DataStartSend()J

    move-result-wide v12

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static {v14}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v14

    invoke-virtual {v14}, Lcom/wf/wellsfargomobile/WFApp;->getPerfD6DataJsonResponse()J

    move-result-wide v14

    invoke-direct/range {v1 .. v15}, Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;-><init>(Lcom/wf/wellsfargomobile/WFwebview;Landroid/content/Context;JJJJJJ)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 387
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static {v1}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/wf/wellsfargomobile/WFApp;->setPerfD1FrontImageStartSend(J)V

    .line 388
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static {v1}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/wf/wellsfargomobile/WFApp;->setPerfD2FrontImageJsonResponse(J)V

    .line 389
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static {v1}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/wf/wellsfargomobile/WFApp;->setPerfD3RearImageStartSend(J)V

    .line 390
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static {v1}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/wf/wellsfargomobile/WFApp;->setPerfD4RearImageJsonResponse(J)V

    .line 391
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static {v1}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/wf/wellsfargomobile/WFApp;->setPerfD5DataStartSend(J)V

    .line 392
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static {v1}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/wf/wellsfargomobile/WFApp;->setPerfD6DataJsonResponse(J)V

    .line 395
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->inSession:Ljava/lang/Boolean;
    invoke-static {v1}, Lcom/wf/wellsfargomobile/WFwebview;->access$600(Lcom/wf/wellsfargomobile/WFwebview;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->loginFirstLoad:Z
    invoke-static {v1}, Lcom/wf/wellsfargomobile/WFwebview;->access$700(Lcom/wf/wellsfargomobile/WFwebview;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "data:text/html"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 396
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    const/4 v2, 0x0

    #setter for: Lcom/wf/wellsfargomobile/WFwebview;->loginFirstLoad:Z
    invoke-static {v1, v2}, Lcom/wf/wellsfargomobile/WFwebview;->access$702(Lcom/wf/wellsfargomobile/WFwebview;Z)Z

    .line 397
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->webview:Landroid/webkit/WebView;
    invoke-static {v1}, Lcom/wf/wellsfargomobile/WFwebview;->access$800(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/webkit/WebView;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->clearCache(Z)V

    .line 398
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->webview:Landroid/webkit/WebView;
    invoke-static {v1}, Lcom/wf/wellsfargomobile/WFwebview;->access$800(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/WebView;->clearHistory()V

    .line 399
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->webviewCurtain:Landroid/view/View;
    invoke-static {v1}, Lcom/wf/wellsfargomobile/WFwebview;->access$900(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 404
    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->loading:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/wf/wellsfargomobile/WFwebview;->access$1100(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/widget/RelativeLayout;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 405
    const-string v1, "WFWebViewClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exiting onPageFinished() - url: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 407
    return-void

    .line 400
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->firstLoad:Z
    invoke-static {v1}, Lcom/wf/wellsfargomobile/WFwebview;->access$1000(Lcom/wf/wellsfargomobile/WFwebview;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 401
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    const/4 v2, 0x0

    #setter for: Lcom/wf/wellsfargomobile/WFwebview;->firstLoad:Z
    invoke-static {v1, v2}, Lcom/wf/wellsfargomobile/WFwebview;->access$1002(Lcom/wf/wellsfargomobile/WFwebview;Z)Z

    .line 402
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->webviewCurtain:Landroid/view/View;
    invoke-static {v1}, Lcom/wf/wellsfargomobile/WFwebview;->access$900(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2
    .parameter "view"
    .parameter "url"
    .parameter "favicon"

    .prologue
    .line 416
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->loading:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/wf/wellsfargomobile/WFwebview;->access$1100(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 417
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .parameter "view"
    .parameter "errorCode"
    .parameter "description"
    .parameter "failingUrl"

    .prologue
    .line 424
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    iget-object v1, p0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    const v2, 0x7f070045

    invoke-virtual {v1, v2}, Lcom/wf/wellsfargomobile/WFwebview;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    const v3, 0x7f070044

    invoke-virtual {v2, v3}, Lcom/wf/wellsfargomobile/WFwebview;->getString(I)Ljava/lang/String;

    move-result-object v2

    #calls: Lcom/wf/wellsfargomobile/WFwebview;->returnWithError(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/wf/wellsfargomobile/WFwebview;->access$1300(Lcom/wf/wellsfargomobile/WFwebview;Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 13
    .parameter "view"
    .parameter "url"

    .prologue
    .line 292
    const-string v9, "WFWebViewClient"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "shouldOverrideUrlLoading - url: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    const/4 v0, 0x0

    .line 294
    .local v0, aUrl:Ljava/net/URL;
    const/4 v5, 0x0

    .line 295
    .local v5, host:Ljava/lang/String;
    const/4 v3, 0x1

    .line 296
    .local v3, externalUrl:Z
    const-string v9, "WFWebViewClient"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "url: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    :try_start_0
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, p2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_1

    .line 299
    .end local v0           #aUrl:Ljava/net/URL;
    .local v1, aUrl:Ljava/net/URL;
    :try_start_1
    invoke-virtual {v1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v5

    .line 301
    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const/high16 v10, 0x7f08

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v8

    .line 305
    .local v8, permittedHosts:[Ljava/lang/String;
    move-object v2, v8

    .local v2, arr$:[Ljava/lang/String;
    array-length v7, v2

    .local v7, len$:I
    const/4 v6, 0x0

    .local v6, i$:I
    :goto_0
    if-ge v6, v7, :cond_1

    aget-object v4, v2, v6

    .line 306
    .local v4, h:Ljava/lang/String;
    invoke-virtual {v5, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_2

    move-result v9

    if-eqz v9, :cond_0

    .line 307
    const/4 v3, 0x0

    .line 305
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .end local v4           #h:Ljava/lang/String;
    :cond_1
    move-object v0, v1

    .line 314
    .end local v1           #aUrl:Ljava/net/URL;
    .end local v2           #arr$:[Ljava/lang/String;
    .end local v6           #i$:I
    .end local v7           #len$:I
    .end local v8           #permittedHosts:[Ljava/lang/String;
    .restart local v0       #aUrl:Ljava/net/URL;
    :goto_1
    const-string v9, "tel:"

    invoke-virtual {p2, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    if-ltz v9, :cond_2

    .line 316
    :try_start_2
    iget-object v9, p0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    new-instance v10, Landroid/content/Intent;

    const-string v11, "android.intent.action.DIAL"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    invoke-direct {v10, v11, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v9, v10}, Lcom/wf/wellsfargomobile/WFwebview;->startActivity(Landroid/content/Intent;)V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    .line 367
    :goto_2
    const/4 v9, 0x1

    :goto_3
    return v9

    .line 321
    :cond_2
    const-string v9, "data:text/html,"

    invoke-virtual {p2, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    const-string v9, "/signOn/appSignon.action"

    invoke-virtual {p2, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 328
    iget-object v9, p0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    const-string v10, "/accounts/g.accountList.do"

    #calls: Lcom/wf/wellsfargomobile/WFwebview;->loadURL(Ljava/lang/String;)V
    invoke-static {v9, v10}, Lcom/wf/wellsfargomobile/WFwebview;->access$100(Lcom/wf/wellsfargomobile/WFwebview;Ljava/lang/String;)V

    goto :goto_2

    .line 329
    :cond_3
    if-eqz v3, :cond_4

    .line 334
    iget-object v9, p0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #setter for: Lcom/wf/wellsfargomobile/WFwebview;->outsideURL:Ljava/lang/String;
    invoke-static {v9, p2}, Lcom/wf/wellsfargomobile/WFwebview;->access$202(Lcom/wf/wellsfargomobile/WFwebview;Ljava/lang/String;)Ljava/lang/String;

    .line 335
    new-instance v9, Landroid/app/AlertDialog$Builder;

    iget-object v10, p0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    invoke-direct {v9, v10}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v10, 0x108009b

    invoke-virtual {v9, v10}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    const v10, 0x7f070069

    invoke-virtual {v9, v10}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    const-string v10, "Back"

    new-instance v11, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient$2;

    invoke-direct {v11, p0}, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient$2;-><init>(Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;)V

    invoke-virtual {v9, v10, v11}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    const-string v10, "OK"

    new-instance v11, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient$1;

    invoke-direct {v11, p0}, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient$1;-><init>(Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;)V

    invoke-virtual {v9, v10, v11}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/AlertDialog;->show()V

    goto :goto_2

    .line 355
    :cond_4
    const-string v9, "/mba/begin.action"

    invoke-virtual {p2, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    if-gez v9, :cond_5

    const-string v9, "signOn/begin.do"

    invoke-virtual {p2, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    if-gez v9, :cond_5

    const-string v9, "g.signOn.do"

    invoke-virtual {p2, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    if-ltz v9, :cond_6

    .line 359
    :cond_5
    iget-object v9, p0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    invoke-virtual {v9}, Lcom/wf/wellsfargomobile/WFwebview;->finish()V

    goto :goto_2

    .line 360
    :cond_6
    const-string v9, "g.signOff.do"

    invoke-virtual {p2, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    if-ltz v9, :cond_7

    .line 361
    iget-object v9, p0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #calls: Lcom/wf/wellsfargomobile/WFwebview;->signOff()V
    invoke-static {v9}, Lcom/wf/wellsfargomobile/WFwebview;->access$300(Lcom/wf/wellsfargomobile/WFwebview;)V

    goto :goto_2

    .line 362
    :cond_7
    iget-object v9, p0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #getter for: Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static {v9}, Lcom/wf/wellsfargomobile/WFwebview;->access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v9

    invoke-virtual {v9}, Lcom/wf/wellsfargomobile/WFApp;->isGoogleApiAvailable()Z

    move-result v9

    if-eqz v9, :cond_8

    const-string v9, "g.locations.do"

    invoke-virtual {p2, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    if-ltz v9, :cond_8

    .line 363
    iget-object v9, p0, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;->this$0:Lcom/wf/wellsfargomobile/WFwebview;

    #calls: Lcom/wf/wellsfargomobile/WFwebview;->launchLocSearch()V
    invoke-static {v9}, Lcom/wf/wellsfargomobile/WFwebview;->access$500(Lcom/wf/wellsfargomobile/WFwebview;)V

    goto/16 :goto_2

    .line 365
    :cond_8
    const/4 v9, 0x0

    goto/16 :goto_3

    .line 317
    :catch_0
    move-exception v9

    goto/16 :goto_2

    .line 310
    :catch_1
    move-exception v9

    goto/16 :goto_1

    .end local v0           #aUrl:Ljava/net/URL;
    .restart local v1       #aUrl:Ljava/net/URL;
    :catch_2
    move-exception v9

    move-object v0, v1

    .end local v1           #aUrl:Ljava/net/URL;
    .restart local v0       #aUrl:Ljava/net/URL;
    goto/16 :goto_1
.end method
