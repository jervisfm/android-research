.class public Lcom/wf/wellsfargomobile/WFwebview;
.super Lcom/wf/wellsfargomobile/BaseActivity;
.source "WFwebview.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/wf/wellsfargomobile/WFwebview$SendPerfTimingsThread;,
        Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;,
        Lcom/wf/wellsfargomobile/WFwebview$SubmitDepositThread;,
        Lcom/wf/wellsfargomobile/WFwebview$CheckDepositJavaScriptInterface;,
        Lcom/wf/wellsfargomobile/WFwebview$CommonJavaScriptInterface;,
        Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;
    }
.end annotation


# static fields
.field public static final ACTIVITY_LOCSEARCH:I = 0x0

.field public static final KEY_ERRORMESSAGE:Ljava/lang/String; = "error_msg"

.field public static final KEY_ERRORTITLE:Ljava/lang/String; = "error_title"

.field private static final TAG:Ljava/lang/String; = "WFwebview"


# instance fields
.field private firstLoad:Z

.field private inSession:Ljava/lang/Boolean;

.field private loadStatus:Landroid/widget/TextView;

.field private loading:Landroid/widget/RelativeLayout;

.field private loginFirstLoad:Z

.field private outsideURL:Ljava/lang/String;

.field private password:Ljava/lang/CharSequence;

.field private submitDepositProgressDialog:Landroid/app/Dialog;

.field private submitRearImageProgressDialog:Landroid/app/Dialog;

.field private username:Ljava/lang/CharSequence;

.field private webview:Landroid/webkit/WebView;

.field private webviewCurtain:Landroid/view/View;

.field private wfApp:Lcom/wf/wellsfargomobile/WFApp;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 71
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/BaseActivity;-><init>()V

    .line 84
    iput-boolean v0, p0, Lcom/wf/wellsfargomobile/WFwebview;->loginFirstLoad:Z

    .line 85
    iput-boolean v0, p0, Lcom/wf/wellsfargomobile/WFwebview;->firstLoad:Z

    .line 91
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    .line 1257
    return-void
.end method

.method static synthetic access$100(Lcom/wf/wellsfargomobile/WFwebview;Ljava/lang/String;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/wf/wellsfargomobile/WFwebview;->loadURL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/wf/wellsfargomobile/WFwebview;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/wf/wellsfargomobile/WFwebview;->firstLoad:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/wf/wellsfargomobile/WFwebview;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/wf/wellsfargomobile/WFwebview;->firstLoad:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/widget/RelativeLayout;
    .locals 1
    .parameter "x0"

    .prologue
    .line 71
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview;->loading:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/wf/wellsfargomobile/WFwebview;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/WFwebview;->clearAndPreventCache()V

    return-void
.end method

.method static synthetic access$1300(Lcom/wf/wellsfargomobile/WFwebview;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Lcom/wf/wellsfargomobile/WFwebview;->returnWithError(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;
    .locals 1
    .parameter "x0"

    .prologue
    .line 71
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview;->submitRearImageProgressDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/wf/wellsfargomobile/WFwebview;Z)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/wf/wellsfargomobile/WFwebview;->clearWebViewDatabaseAndCookies(Z)V

    return-void
.end method

.method static synthetic access$1600(Lcom/wf/wellsfargomobile/WFwebview;Ljava/lang/String;)Z
    .locals 1
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/wf/wellsfargomobile/WFwebview;->validNonce(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1700(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/app/Dialog;
    .locals 1
    .parameter "x0"

    .prologue
    .line 71
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview;->submitDepositProgressDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/wf/wellsfargomobile/WFwebview;Ljava/lang/String;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/wf/wellsfargomobile/WFwebview;->loadJavaScript(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/wf/wellsfargomobile/WFwebview;)Ljava/lang/String;
    .locals 1
    .parameter "x0"

    .prologue
    .line 71
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview;->outsideURL:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/wf/wellsfargomobile/WFwebview;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 71
    iput-object p1, p0, Lcom/wf/wellsfargomobile/WFwebview;->outsideURL:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/wf/wellsfargomobile/WFwebview;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/WFwebview;->signOff()V

    return-void
.end method

.method static synthetic access$400(Lcom/wf/wellsfargomobile/WFwebview;)Lcom/wf/wellsfargomobile/WFApp;
    .locals 1
    .parameter "x0"

    .prologue
    .line 71
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    return-object v0
.end method

.method static synthetic access$500(Lcom/wf/wellsfargomobile/WFwebview;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/WFwebview;->launchLocSearch()V

    return-void
.end method

.method static synthetic access$600(Lcom/wf/wellsfargomobile/WFwebview;)Ljava/lang/Boolean;
    .locals 1
    .parameter "x0"

    .prologue
    .line 71
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview;->inSession:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$602(Lcom/wf/wellsfargomobile/WFwebview;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 71
    iput-object p1, p0, Lcom/wf/wellsfargomobile/WFwebview;->inSession:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$700(Lcom/wf/wellsfargomobile/WFwebview;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/wf/wellsfargomobile/WFwebview;->loginFirstLoad:Z

    return v0
.end method

.method static synthetic access$702(Lcom/wf/wellsfargomobile/WFwebview;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/wf/wellsfargomobile/WFwebview;->loginFirstLoad:Z

    return p1
.end method

.method static synthetic access$800(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/webkit/WebView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 71
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview;->webview:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/wf/wellsfargomobile/WFwebview;)Landroid/view/View;
    .locals 1
    .parameter "x0"

    .prologue
    .line 71
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview;->webviewCurtain:Landroid/view/View;

    return-object v0
.end method

.method private clearAndPreventCache()V
    .locals 4

    .prologue
    .line 712
    :try_start_0
    iget-object v1, p0, Lcom/wf/wellsfargomobile/WFwebview;->webview:Landroid/webkit/WebView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->clearCache(Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 717
    :goto_0
    return-void

    .line 714
    :catch_0
    move-exception v0

    .line 715
    .local v0, ex:Ljava/lang/Throwable;
    const-string v1, "WFwebview"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Bad clearAndPreventCache: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private clearWebViewDatabaseAndCookies(Z)V
    .locals 6
    .parameter "clearCookies"

    .prologue
    .line 722
    :try_start_0
    invoke-static {p0}, Landroid/webkit/WebViewDatabase;->getInstance(Landroid/content/Context;)Landroid/webkit/WebViewDatabase;

    move-result-object v1

    .line 723
    .local v1, db:Landroid/webkit/WebViewDatabase;
    invoke-virtual {v1}, Landroid/webkit/WebViewDatabase;->clearFormData()V

    .line 724
    invoke-virtual {v1}, Landroid/webkit/WebViewDatabase;->clearUsernamePassword()V

    .line 725
    invoke-virtual {v1}, Landroid/webkit/WebViewDatabase;->clearHttpAuthUsernamePassword()V

    .line 728
    if-eqz p1, :cond_0

    .line 729
    const-string v3, "WFwebview"

    const-string v4, "before CookieManager.getInstance()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 730
    invoke-static {p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 731
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    .line 732
    .local v0, cookies:Landroid/webkit/CookieManager;
    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    .line 733
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/webkit/CookieSyncManager;->sync()V

    .line 734
    const-string v3, "WFwebview"

    const-string v4, "after CookieManager.getInstance()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 739
    .end local v0           #cookies:Landroid/webkit/CookieManager;
    .end local v1           #db:Landroid/webkit/WebViewDatabase;
    :cond_0
    :goto_0
    return-void

    .line 736
    :catch_0
    move-exception v2

    .line 737
    .local v2, ex:Ljava/lang/Throwable;
    const-string v3, "WFwebview"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Bad clearWebViewDatabaseAndCookies: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private depositCheck()V
    .locals 11

    .prologue
    .line 595
    const-string v8, "WFwebview"

    const-string v9, "depositCheck()"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    const-string v6, "/deposit/depositCheck.action?"

    .line 599
    .local v6, navToPartialUrl:Ljava/lang/String;
    const v8, 0x7f070001

    invoke-virtual {p0, v8}, Lcom/wf/wellsfargomobile/WFwebview;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 601
    .local v0, baseUrl:Ljava/lang/String;
    iget-object v8, p0, Lcom/wf/wellsfargomobile/WFwebview;->webview:Landroid/webkit/WebView;

    invoke-virtual {v8}, Landroid/webkit/WebView;->getUrl()Ljava/lang/String;

    move-result-object v5

    .line 602
    .local v5, currentUrlStr:Ljava/lang/String;
    const-string v8, "WFwebview"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "depositCheck() - current URL "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 605
    :try_start_0
    new-instance v4, Ljava/net/URL;

    invoke-direct {v4, v5}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 607
    .local v4, currentUrl:Ljava/net/URL;
    invoke-virtual {v4}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v7

    .line 608
    .local v7, path:Ljava/lang/String;
    const-string v8, "WFwebview"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "depositCheck() - current URL path: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 609
    sget-object v8, Lcom/wf/wellsfargomobile/WFApp;->URL_PATH_TOKEN_BROKERAGE:Ljava/lang/CharSequence;

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 610
    const-string v8, "WFwebview"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "depositCheck() - contains "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Lcom/wf/wellsfargomobile/WFApp;->URL_PATH_TOKEN_BROKERAGE:Ljava/lang/CharSequence;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 612
    invoke-virtual {v4}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v3

    .line 613
    .local v3, currentProtocol:Ljava/lang/String;
    invoke-virtual {v4}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 614
    .local v1, currentHost:Ljava/lang/String;
    invoke-virtual {v4}, Ljava/net/URL;->getPort()I

    move-result v2

    .line 616
    .local v2, currentPort:I
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "://"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 619
    const/16 v8, 0x50

    if-eq v2, v8, :cond_0

    const/16 v8, 0x1bb

    if-eq v2, v8, :cond_0

    const/4 v8, -0x1

    if-eq v2, v8, :cond_0

    .line 620
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 623
    :cond_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const v9, 0x7f070002

    invoke-virtual {p0, v9}, Lcom/wf/wellsfargomobile/WFwebview;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "gotoMMRDC"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 633
    .end local v1           #currentHost:Ljava/lang/String;
    .end local v2           #currentPort:I
    .end local v3           #currentProtocol:Ljava/lang/String;
    .end local v4           #currentUrl:Ljava/net/URL;
    .end local v7           #path:Ljava/lang/String;
    :goto_0
    const-string v8, "WFwebview"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "depositCheck() - baseUrl: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "navToUrl: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 635
    invoke-direct {p0, v6, v0}, Lcom/wf/wellsfargomobile/WFwebview;->loadURL(Ljava/lang/String;Ljava/lang/String;)V

    .line 636
    return-void

    .line 626
    .restart local v4       #currentUrl:Ljava/net/URL;
    .restart local v7       #path:Ljava/lang/String;
    :cond_1
    :try_start_1
    const-string v8, "WFwebview"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "depositCheck() - does not contains "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Lcom/wf/wellsfargomobile/WFApp;->URL_PATH_TOKEN_BROKERAGE:Ljava/lang/CharSequence;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 628
    .end local v4           #currentUrl:Ljava/net/URL;
    .end local v7           #path:Ljava/lang/String;
    :catch_0
    move-exception v8

    goto :goto_0
.end method

.method private goLogin()V
    .locals 14

    .prologue
    const v13, 0x7f07005e

    const v12, 0x7f070041

    .line 177
    :try_start_0
    const-string v4, ""

    .line 179
    .local v4, modes:Ljava/lang/String;
    sget-object v9, Lcom/wf/wellsfargomobile/util/Trinary;->TRUE:Lcom/wf/wellsfargomobile/util/Trinary;

    iget-object v10, p0, Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-virtual {v10}, Lcom/wf/wellsfargomobile/WFApp;->getDeviceHasCamera()Lcom/wf/wellsfargomobile/util/Trinary;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/wf/wellsfargomobile/util/Trinary;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    move-result v9

    if-eqz v9, :cond_2

    .line 180
    const/4 v0, 0x0

    .line 183
    .local v0, camera:Landroid/hardware/Camera;
    :try_start_1
    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;

    move-result-object v0

    .line 184
    if-eqz v0, :cond_0

    .line 185
    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v5

    .line 186
    .local v5, parameters:Landroid/hardware/Camera$Parameters;
    invoke-virtual {v5}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v3

    .line 187
    .local v3, focusModes:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v3, :cond_0

    .line 188
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v4

    .line 195
    .end local v3           #focusModes:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v5           #parameters:Landroid/hardware/Camera$Parameters;
    :cond_0
    if-eqz v0, :cond_1

    .line 196
    :try_start_2
    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 197
    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 199
    :cond_1
    const/4 v0, 0x0

    .line 200
    const/4 v3, 0x0

    .line 204
    .end local v0           #camera:Landroid/hardware/Camera;
    :cond_2
    :goto_0
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/wf/wellsfargomobile/WFwebview;->loginFirstLoad:Z

    .line 205
    iget-object v9, p0, Lcom/wf/wellsfargomobile/WFwebview;->loadStatus:Landroid/widget/TextView;

    const v10, 0x7f070038

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(I)V

    .line 207
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 208
    .local v6, postParams:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "WFAppId"

    const v11, 0x7f070003

    invoke-virtual {p0, v11}, Lcom/wf/wellsfargomobile/WFwebview;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 209
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "disableBackLink"

    const-string v11, "x"

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 210
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "userId"

    iget-object v11, p0, Lcom/wf/wellsfargomobile/WFwebview;->username:Ljava/lang/CharSequence;

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "password"

    iget-object v11, p0, Lcom/wf/wellsfargomobile/WFwebview;->password:Ljava/lang/CharSequence;

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "hasCamera"

    iget-object v11, p0, Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-virtual {v11}, Lcom/wf/wellsfargomobile/WFApp;->getDeviceHasCamera()Lcom/wf/wellsfargomobile/util/Trinary;

    move-result-object v11

    invoke-virtual {v11}, Lcom/wf/wellsfargomobile/util/Trinary;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 214
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "WFAppVersion"

    const v11, 0x7f070004

    invoke-virtual {p0, v11}, Lcom/wf/wellsfargomobile/WFwebview;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 216
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "nonce"

    iget-object v11, p0, Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-virtual {v11}, Lcom/wf/wellsfargomobile/WFApp;->getNonce()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 217
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "manufacturer"

    sget-object v11, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "modelNumber"

    sget-object v11, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 219
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "modelName"

    sget-object v11, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 220
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "carrier"

    sget-object v11, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "osVersion"

    sget-object v11, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 222
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "fingerprint"

    sget-object v11, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 223
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "focusModes"

    invoke-direct {v9, v10, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 225
    new-instance v2, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    invoke-direct {v2, v6}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;)V

    .line 227
    .local v2, entity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    iget-object v9, p0, Lcom/wf/wellsfargomobile/WFwebview;->webview:Landroid/webkit/WebView;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const/high16 v11, 0x7f07

    invoke-virtual {p0, v11}, Lcom/wf/wellsfargomobile/WFwebview;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/signOn/appSignon.action"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v2}, Lorg/apache/http/util/EntityUtils;->toByteArray(Lorg/apache/http/HttpEntity;)[B

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/webkit/WebView;->postUrl(Ljava/lang/String;[B)V
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    .line 251
    .end local v2           #entity:Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .end local v4           #modes:Ljava/lang/String;
    .end local v6           #postParams:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    :goto_1
    return-void

    .line 191
    .restart local v0       #camera:Landroid/hardware/Camera;
    .restart local v4       #modes:Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 192
    .local v7, re:Ljava/lang/RuntimeException;
    :try_start_3
    iget-object v9, p0, Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/wf/wellsfargomobile/WFApp;->setErrorOpeningCamera(Z)V

    .line 193
    const-string v9, "WFwebview"

    const-string v10, "exception occured while getting camera properties"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 195
    if-eqz v0, :cond_3

    .line 196
    :try_start_4
    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 197
    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 199
    :cond_3
    const/4 v0, 0x0

    .line 200
    const/4 v3, 0x0

    .line 201
    .restart local v3       #focusModes:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    goto/16 :goto_0

    .line 195
    .end local v3           #focusModes:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v7           #re:Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v9

    if-eqz v0, :cond_4

    .line 196
    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 197
    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 199
    :cond_4
    const/4 v0, 0x0

    .line 200
    const/4 v3, 0x0

    .restart local v3       #focusModes:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    throw v9
    :try_end_4
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 230
    .end local v0           #camera:Landroid/hardware/Camera;
    .end local v3           #focusModes:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v4           #modes:Ljava/lang/String;
    :catch_1
    move-exception v8

    .line 235
    .local v8, uee:Ljava/io/UnsupportedEncodingException;
    invoke-virtual {p0, v13}, Lcom/wf/wellsfargomobile/WFwebview;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v12}, Lcom/wf/wellsfargomobile/WFwebview;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v9, v10}, Lcom/wf/wellsfargomobile/WFwebview;->returnWithError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 237
    .end local v8           #uee:Ljava/io/UnsupportedEncodingException;
    :catch_2
    move-exception v1

    .line 242
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {p0, v13}, Lcom/wf/wellsfargomobile/WFwebview;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v12}, Lcom/wf/wellsfargomobile/WFwebview;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v9, v10}, Lcom/wf/wellsfargomobile/WFwebview;->returnWithError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 244
    .end local v1           #e:Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 248
    .local v1, e:Ljava/lang/Exception;
    invoke-virtual {p0, v13}, Lcom/wf/wellsfargomobile/WFwebview;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v12}, Lcom/wf/wellsfargomobile/WFwebview;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v9, v10}, Lcom/wf/wellsfargomobile/WFwebview;->returnWithError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private launchLocSearch()V
    .locals 2

    .prologue
    .line 449
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview;->inSession:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 450
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/wf/wellsfargomobile/loc/Location;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/wf/wellsfargomobile/WFwebview;->startActivityForResult(Landroid/content/Intent;I)V

    .line 456
    :goto_0
    return-void

    .line 453
    :cond_0
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/wf/wellsfargomobile/WFwebview;->setResult(ILandroid/content/Intent;)V

    .line 454
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/WFwebview;->finish()V

    goto :goto_0
.end method

.method private loadInitialURL(Ljava/lang/String;)V
    .locals 1
    .parameter "partialURL"

    .prologue
    .line 284
    const/high16 v0, 0x7f07

    invoke-virtual {p0, v0}, Lcom/wf/wellsfargomobile/WFwebview;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/wf/wellsfargomobile/WFwebview;->loadURL(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    return-void
.end method

.method private loadJavaScript(Ljava/lang/String;)V
    .locals 3
    .parameter "partialUrl"

    .prologue
    .line 254
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview;->webview:Landroid/webkit/WebView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "javascript:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 255
    return-void
.end method

.method private loadURL(Ljava/lang/String;)V
    .locals 1
    .parameter "partialURL"

    .prologue
    .line 275
    const v0, 0x7f070001

    invoke-virtual {p0, v0}, Lcom/wf/wellsfargomobile/WFwebview;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/wf/wellsfargomobile/WFwebview;->loadURL(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    return-void
.end method

.method private loadURL(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .parameter "partialURL"
    .parameter "baseUrl"

    .prologue
    .line 258
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/wf/wellsfargomobile/WFwebview;->firstLoad:Z

    .line 259
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x96

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 260
    .local v0, buff:Ljava/lang/StringBuilder;
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    const-string v1, "?"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 263
    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    :cond_0
    const-string v1, "&WFAppId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    const v1, 0x7f070003

    invoke-virtual {p0, v1}, Lcom/wf/wellsfargomobile/WFwebview;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    const-string v1, "&disableBackLink=x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    const-string v1, "WFwebview"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "requesting URL: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    iget-object v1, p0, Lcom/wf/wellsfargomobile/WFwebview;->webview:Landroid/webkit/WebView;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 272
    return-void
.end method

.method private makeSureWebViewIsSecure()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 683
    iget-object v1, p0, Lcom/wf/wellsfargomobile/WFwebview;->webview:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 684
    .local v0, settings:Landroid/webkit/WebSettings;
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    .line 685
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    .line 686
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 687
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setNavDump(Z)V

    .line 688
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setPluginsEnabled(Z)V

    .line 689
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setSupportMultipleWindows(Z)V

    .line 690
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    .line 705
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/WFwebview;->clearAndPreventCache()V

    .line 706
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/wf/wellsfargomobile/WFwebview;->clearWebViewDatabaseAndCookies(Z)V

    .line 707
    return-void
.end method

.method private returnWithError(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .parameter "title"
    .parameter "message"

    .prologue
    .line 437
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 438
    .local v0, bundle:Landroid/os/Bundle;
    if-eqz p1, :cond_0

    .line 439
    const-string v2, "error_title"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    :cond_0
    const-string v2, "error_msg"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 443
    .local v1, errorIntent:Landroid/content/Intent;
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 444
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v1}, Lcom/wf/wellsfargomobile/WFwebview;->setResult(ILandroid/content/Intent;)V

    .line 445
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/WFwebview;->finish()V

    .line 446
    return-void
.end method

.method private signOff()V
    .locals 10

    .prologue
    .line 459
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const v8, 0x7f070001

    invoke-virtual {p0, v8}, Lcom/wf/wellsfargomobile/WFwebview;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/accounts/g.signOff.do"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 461
    .local v5, navToUrl:Ljava/lang/String;
    iget-object v7, p0, Lcom/wf/wellsfargomobile/WFwebview;->webview:Landroid/webkit/WebView;

    invoke-virtual {v7}, Landroid/webkit/WebView;->getUrl()Ljava/lang/String;

    move-result-object v4

    .line 462
    .local v4, currentUrlStr:Ljava/lang/String;
    const-string v7, "WFwebview"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "signOff() - current URL "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    :try_start_0
    new-instance v3, Ljava/net/URL;

    invoke-direct {v3, v4}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 467
    .local v3, currentUrl:Ljava/net/URL;
    invoke-virtual {v3}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v6

    .line 468
    .local v6, path:Ljava/lang/String;
    const-string v7, "WFwebview"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "signOff() - current URL path: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 469
    sget-object v7, Lcom/wf/wellsfargomobile/WFApp;->URL_PATH_TOKEN_BROKERAGE:Ljava/lang/CharSequence;

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 470
    const-string v7, "WFwebview"

    const-string v8, "signOff() - contains BMW"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    invoke-virtual {v3}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v2

    .line 473
    .local v2, currentProtocol:Ljava/lang/String;
    invoke-virtual {v3}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 474
    .local v0, currentHost:Ljava/lang/String;
    invoke-virtual {v3}, Ljava/net/URL;->getPort()I

    move-result v1

    .line 476
    .local v1, currentPort:I
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "://"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 479
    const/16 v7, 0x50

    if-eq v1, v7, :cond_0

    const/16 v7, 0x1bb

    if-eq v1, v7, :cond_0

    const/4 v7, -0x1

    if-eq v1, v7, :cond_0

    .line 480
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 483
    :cond_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const v8, 0x7f070002

    invoke-virtual {p0, v8}, Lcom/wf/wellsfargomobile/WFwebview;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "gotoMLogoff"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 493
    .end local v0           #currentHost:Ljava/lang/String;
    .end local v1           #currentPort:I
    .end local v2           #currentProtocol:Ljava/lang/String;
    .end local v3           #currentUrl:Ljava/net/URL;
    .end local v6           #path:Ljava/lang/String;
    :goto_0
    const-string v7, "WFwebview"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "signOff() - navToUrl: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 495
    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    iput-object v7, p0, Lcom/wf/wellsfargomobile/WFwebview;->inSession:Ljava/lang/Boolean;

    .line 496
    iget-object v7, p0, Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-virtual {v7}, Lcom/wf/wellsfargomobile/WFApp;->clearCheckDepositData()V

    .line 497
    iget-object v7, p0, Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/wf/wellsfargomobile/WFApp;->setNonce(Ljava/lang/String;)V

    .line 498
    iget-object v7, p0, Lcom/wf/wellsfargomobile/WFwebview;->webview:Landroid/webkit/WebView;

    invoke-virtual {v7, v5}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 499
    return-void

    .line 485
    .restart local v3       #currentUrl:Ljava/net/URL;
    .restart local v6       #path:Ljava/lang/String;
    :cond_1
    :try_start_1
    const-string v7, "WFwebview"

    const-string v8, "signOff() - does not contains BMW"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 488
    .end local v3           #currentUrl:Ljava/net/URL;
    .end local v6           #path:Ljava/lang/String;
    :catch_0
    move-exception v7

    goto :goto_0
.end method

.method private validNonce(Ljava/lang/String;)Z
    .locals 4
    .parameter "nonce"

    .prologue
    const/4 v0, 0x0

    .line 1363
    const-string v1, "WFwebview"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "validNonce("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") - wfApp.getNonce(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-virtual {v3}, Lcom/wf/wellsfargomobile/WFApp;->getNonce()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1364
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-virtual {v1}, Lcom/wf/wellsfargomobile/WFApp;->getNonce()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1370
    :cond_0
    :goto_0
    return v0

    .line 1367
    :cond_1
    iget-object v1, p0, Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-virtual {v1}, Lcom/wf/wellsfargomobile/WFApp;->getNonce()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1368
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "intent"

    .prologue
    .line 503
    invoke-super {p0, p1, p2, p3}, Lcom/wf/wellsfargomobile/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 505
    packed-switch p2, :pswitch_data_0

    .line 551
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 507
    :pswitch_1
    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    .line 508
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/WFwebview;->signOff()V

    goto :goto_0

    .line 513
    :pswitch_2
    const-string v0, "WFwebview"

    const-string v1, "onActivityResult - completing MRDC flow "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    const-string v0, "WFwebview"

    const-string v1, "rewinding"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 517
    new-instance v0, Lcom/wf/wellsfargomobile/WFwebview$1;

    invoke-direct {v0, p0}, Lcom/wf/wellsfargomobile/WFwebview$1;-><init>(Lcom/wf/wellsfargomobile/WFwebview;)V

    invoke-virtual {p0, v0}, Lcom/wf/wellsfargomobile/WFwebview;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 524
    new-instance v0, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;

    invoke-direct {v0, p0, p0}, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;-><init>(Lcom/wf/wellsfargomobile/WFwebview;Landroid/content/Context;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/wf/wellsfargomobile/WFwebview$SubmitRearImageThread;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 529
    :pswitch_3
    const-string v0, "WFwebview"

    const-string v1, "onActivityResult - cancelling MRDC flow  "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 530
    const-string v0, "WFwebview"

    const-string v1, "rewinding"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 532
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-virtual {v0}, Lcom/wf/wellsfargomobile/WFApp;->clearCheckDepositData()V

    .line 534
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/WFwebview;->depositCheck()V

    goto :goto_0

    .line 538
    :pswitch_4
    const-string v0, "WFwebview"

    const-string v1, "onActivityResult - returning from location search  "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-virtual {v0}, Lcom/wf/wellsfargomobile/WFApp;->clearCheckDepositData()V

    .line 542
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/WFwebview;->depositCheck()V

    goto :goto_0

    .line 505
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .parameter "newConfig"

    .prologue
    .line 671
    invoke-super {p0, p1}, Lcom/wf/wellsfargomobile/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 672
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .parameter "savedInstanceState"

    .prologue
    const v9, 0x7f090001

    const v8, 0x7f030009

    const/4 v7, 0x1

    const/4 v6, 0x4

    const/4 v5, 0x0

    .line 95
    invoke-super {p0, p1}, Lcom/wf/wellsfargomobile/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 97
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/WFwebview;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/wf/wellsfargomobile/WFApp;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    .line 100
    const v2, 0x7f03000a

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/WFwebview;->setContentView(I)V

    .line 102
    const v2, 0x7f06006c

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/WFwebview;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/webkit/WebView;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview;->webview:Landroid/webkit/WebView;

    .line 103
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview;->webview:Landroid/webkit/WebView;

    new-instance v3, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/wf/wellsfargomobile/WFwebview$WFWebViewClient;-><init>(Lcom/wf/wellsfargomobile/WFwebview;Lcom/wf/wellsfargomobile/WFwebview$1;)V

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 104
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview;->webview:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 105
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview;->webview:Landroid/webkit/WebView;

    new-instance v3, Lcom/wf/wellsfargomobile/WFwebview$CommonJavaScriptInterface;

    invoke-direct {v3, p0, p0}, Lcom/wf/wellsfargomobile/WFwebview$CommonJavaScriptInterface;-><init>(Lcom/wf/wellsfargomobile/WFwebview;Landroid/content/Context;)V

    const-string v4, "Common"

    invoke-virtual {v2, v3, v4}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview;->webview:Landroid/webkit/WebView;

    new-instance v3, Lcom/wf/wellsfargomobile/WFwebview$CheckDepositJavaScriptInterface;

    invoke-direct {v3, p0, p0}, Lcom/wf/wellsfargomobile/WFwebview$CheckDepositJavaScriptInterface;-><init>(Lcom/wf/wellsfargomobile/WFwebview;Landroid/content/Context;)V

    const-string v4, "Camera"

    invoke-virtual {v2, v3, v4}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview;->webview:Landroid/webkit/WebView;

    new-instance v3, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;

    iget-object v4, p0, Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-direct {v3, p0, v4}, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;-><init>(Landroid/content/Context;Lcom/wf/wellsfargomobile/WFApp;)V

    const-string v4, "PhotoTips"

    invoke-virtual {v2, v3, v4}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/WFwebview;->makeSureWebViewIsSecure()V

    .line 114
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-virtual {v2}, Lcom/wf/wellsfargomobile/WFApp;->clearCheckDepositData()V

    .line 117
    new-instance v2, Landroid/app/Dialog;

    invoke-direct {v2, p0, v9}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview;->submitRearImageProgressDialog:Landroid/app/Dialog;

    .line 120
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview;->submitRearImageProgressDialog:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 121
    .local v1, window:Landroid/view/Window;
    invoke-virtual {v1, v6, v6}, Landroid/view/Window;->setFlags(II)V

    .line 124
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview;->submitRearImageProgressDialog:Landroid/app/Dialog;

    const v3, 0x7f07001d

    invoke-virtual {p0, v3}, Lcom/wf/wellsfargomobile/WFwebview;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 126
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview;->submitRearImageProgressDialog:Landroid/app/Dialog;

    invoke-virtual {v2, v8}, Landroid/app/Dialog;->setContentView(I)V

    .line 129
    new-instance v2, Landroid/app/Dialog;

    invoke-direct {v2, p0, v9}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview;->submitDepositProgressDialog:Landroid/app/Dialog;

    .line 132
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview;->submitDepositProgressDialog:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 133
    invoke-virtual {v1, v6, v6}, Landroid/view/Window;->setFlags(II)V

    .line 136
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview;->submitDepositProgressDialog:Landroid/app/Dialog;

    const v3, 0x7f07001e

    invoke-virtual {p0, v3}, Lcom/wf/wellsfargomobile/WFwebview;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 138
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview;->submitDepositProgressDialog:Landroid/app/Dialog;

    invoke-virtual {v2, v8}, Landroid/app/Dialog;->setContentView(I)V

    .line 140
    const v2, 0x7f06006f

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/WFwebview;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview;->webviewCurtain:Landroid/view/View;

    .line 141
    const v2, 0x7f060011

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/WFwebview;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview;->loadStatus:Landroid/widget/TextView;

    .line 143
    const v2, 0x7f06006d

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/WFwebview;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview;->loading:Landroid/widget/RelativeLayout;

    .line 144
    iget-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview;->loading:Landroid/widget/RelativeLayout;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 146
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/WFwebview;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 147
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 148
    const-string v2, "login_request"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 149
    const-string v2, "username"

    invoke-static {v2}, Lcom/wf/wellsfargomobile/util/NonPersistentExtraStorage;->getExtras(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview;->username:Ljava/lang/CharSequence;

    .line 150
    const-string v2, "password"

    invoke-static {v2}, Lcom/wf/wellsfargomobile/util/NonPersistentExtraStorage;->getExtras(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview;->password:Ljava/lang/CharSequence;

    .line 152
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview;->inSession:Ljava/lang/Boolean;

    .line 153
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/WFwebview;->goLogin()V

    .line 172
    :cond_0
    :goto_0
    return-void

    .line 154
    :cond_1
    const-string v2, "security_request"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 155
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview;->inSession:Ljava/lang/Boolean;

    .line 156
    const-string v2, "/signOn/securityGuarantee.action?disableBackLink=x"

    invoke-direct {p0, v2}, Lcom/wf/wellsfargomobile/WFwebview;->loadInitialURL(Ljava/lang/String;)V

    goto :goto_0

    .line 157
    :cond_2
    const-string v2, "qguide_request"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 158
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview;->inSession:Ljava/lang/Boolean;

    .line 159
    const-string v2, "/faqs/begin.do?disableBackLink=x"

    invoke-direct {p0, v2}, Lcom/wf/wellsfargomobile/WFwebview;->loadInitialURL(Ljava/lang/String;)V

    goto :goto_0

    .line 160
    :cond_3
    const-string v2, "webatmloc_request"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 161
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview;->inSession:Ljava/lang/Boolean;

    .line 162
    const-string v2, "/locator/g.locations.do?disableBackLink=x"

    invoke-direct {p0, v2}, Lcom/wf/wellsfargomobile/WFwebview;->loadURL(Ljava/lang/String;)V

    goto :goto_0

    .line 163
    :cond_4
    const-string v2, "privacy_policy_request"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 164
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview;->inSession:Ljava/lang/Boolean;

    .line 165
    const-string v2, "/privacyPolicy/begin.do?disableBackLink=x"

    invoke-direct {p0, v2}, Lcom/wf/wellsfargomobile/WFwebview;->loadInitialURL(Ljava/lang/String;)V

    goto :goto_0

    .line 166
    :cond_5
    const-string v2, "deposit"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 167
    const-string v2, "WFwebview"

    const-string v3, "in onCreate to go to deposit"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/wf/wellsfargomobile/WFwebview;->inSession:Ljava/lang/Boolean;

    .line 169
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/WFwebview;->depositCheck()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 654
    const-string v0, "WFwebview"

    const-string v1, "in onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 656
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview;->webview:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 657
    const-string v0, "WFwebview"

    const-string v1, "  - webview is not null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 658
    invoke-static {p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 659
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    .line 660
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview;->webview:Landroid/webkit/WebView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->clearCache(Z)V

    .line 661
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview;->webview:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearHistory()V

    .line 662
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview;->webview:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    .line 666
    :goto_0
    invoke-super {p0}, Lcom/wf/wellsfargomobile/BaseActivity;->onDestroy()V

    .line 667
    return-void

    .line 664
    :cond_0
    const-string v0, "WFwebview"

    const-string v1, "  - webview IS null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 640
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 644
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview;->webviewCurtain:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview;->webview:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 645
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview;->webview:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 646
    const/4 v0, 0x1

    .line 649
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/wf/wellsfargomobile/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .parameter "item"

    .prologue
    const/4 v0, 0x1

    .line 571
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 591
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 573
    :pswitch_0
    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/wf/wellsfargomobile/WFwebview;->setResult(ILandroid/content/Intent;)V

    .line 574
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/WFwebview;->finish()V

    goto :goto_0

    .line 577
    :pswitch_1
    iget-object v1, p0, Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-virtual {v1}, Lcom/wf/wellsfargomobile/WFApp;->isGoogleApiAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 578
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/WFwebview;->launchLocSearch()V

    goto :goto_0

    .line 581
    :cond_0
    iget-object v1, p0, Lcom/wf/wellsfargomobile/WFwebview;->webview:Landroid/webkit/WebView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f070001

    invoke-virtual {p0, v3}, Lcom/wf/wellsfargomobile/WFwebview;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/accounts/g.locations.do"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    .line 585
    :pswitch_2
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/WFwebview;->signOff()V

    goto :goto_0

    .line 588
    :pswitch_3
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/WFwebview;->depositCheck()V

    goto :goto_0

    .line 571
    nop

    :pswitch_data_0
    .packed-switch 0x7f06007f
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 676
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/WFwebview;->clearAndPreventCache()V

    .line 677
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/wf/wellsfargomobile/WFwebview;->clearWebViewDatabaseAndCookies(Z)V

    .line 678
    invoke-super {p0}, Lcom/wf/wellsfargomobile/BaseActivity;->onPause()V

    .line 679
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .parameter "menu"

    .prologue
    .line 556
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 557
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview;->inSession:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 558
    iget-object v0, p0, Lcom/wf/wellsfargomobile/WFwebview;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-virtual {v0}, Lcom/wf/wellsfargomobile/WFApp;->getMrdcEnabled()Lcom/wf/wellsfargomobile/util/Trinary;

    move-result-object v0

    sget-object v1, Lcom/wf/wellsfargomobile/util/Trinary;->TRUE:Lcom/wf/wellsfargomobile/util/Trinary;

    invoke-virtual {v0, v1}, Lcom/wf/wellsfargomobile/util/Trinary;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 559
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/WFwebview;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0a0004

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 566
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 561
    :cond_0
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/WFwebview;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0a0005

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0

    .line 564
    :cond_1
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/WFwebview;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0a0003

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0
.end method
