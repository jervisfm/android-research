.class Lcom/wf/wellsfargomobile/loc/Location$1;
.super Ljava/lang/Object;
.source "Location.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wf/wellsfargomobile/loc/Location;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/wf/wellsfargomobile/loc/Location;


# direct methods
.method constructor <init>(Lcom/wf/wellsfargomobile/loc/Location;)V
    .locals 0
    .parameter

    .prologue
    .line 50
    iput-object p1, p0, Lcom/wf/wellsfargomobile/loc/Location$1;->this$0:Lcom/wf/wellsfargomobile/loc/Location;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .parameter "v"

    .prologue
    const/4 v4, 0x1

    .line 53
    move-object v0, p1

    check-cast v0, Landroid/widget/Button;

    .line 54
    .local v0, clicked:Landroid/widget/Button;
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/Location$1;->this$0:Lcom/wf/wellsfargomobile/loc/Location;

    #getter for: Lcom/wf/wellsfargomobile/loc/Location;->currentLocButton:Landroid/widget/Button;
    invoke-static {v2}, Lcom/wf/wellsfargomobile/loc/Location;->access$000(Lcom/wf/wellsfargomobile/loc/Location;)Landroid/widget/Button;

    move-result-object v2

    if-ne v0, v2, :cond_1

    .line 55
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/Location$1;->this$0:Lcom/wf/wellsfargomobile/loc/Location;

    const-class v3, Lcom/wf/wellsfargomobile/loc/LocationResults;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 56
    .local v1, locSearchIntent:Landroid/content/Intent;
    const-string v2, "was_gps"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 57
    const-string v2, "in_session"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 58
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/Location$1;->this$0:Lcom/wf/wellsfargomobile/loc/Location;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Lcom/wf/wellsfargomobile/loc/Location;->startActivityForResult(Landroid/content/Intent;I)V

    .line 64
    .end local v1           #locSearchIntent:Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 59
    :cond_1
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/Location$1;->this$0:Lcom/wf/wellsfargomobile/loc/Location;

    #getter for: Lcom/wf/wellsfargomobile/loc/Location;->locSearchButton:Landroid/widget/Button;
    invoke-static {v2}, Lcom/wf/wellsfargomobile/loc/Location;->access$100(Lcom/wf/wellsfargomobile/loc/Location;)Landroid/widget/Button;

    move-result-object v2

    if-ne v0, v2, :cond_0

    .line 60
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/Location$1;->this$0:Lcom/wf/wellsfargomobile/loc/Location;

    const-class v3, Lcom/wf/wellsfargomobile/loc/LocationForm;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 61
    .restart local v1       #locSearchIntent:Landroid/content/Intent;
    const-string v2, "in_session"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 62
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/Location$1;->this$0:Lcom/wf/wellsfargomobile/loc/Location;

    invoke-virtual {v2, v1, v4}, Lcom/wf/wellsfargomobile/loc/Location;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method
