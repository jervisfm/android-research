.class public Lcom/wf/wellsfargomobile/loc/Location;
.super Lcom/wf/wellsfargomobile/BaseActivity;
.source "Location.java"


# static fields
.field public static final ACTIVITY_SEARCH:I = 0x0

.field public static final ACTIVITY_SEARCHFORM:I = 0x1

.field public static final KEY_INSESSION:Ljava/lang/String; = "in_session"

.field public static final KEY_WASGPS:Ljava/lang/String; = "was_gps"

.field private static final TAG:Ljava/lang/String; = "Location"


# instance fields
.field private buttonHandler:Landroid/view/View$OnClickListener;

.field private currentLocButton:Landroid/widget/Button;

.field private locSearchButton:Landroid/widget/Button;

.field private wfApp:Lcom/wf/wellsfargomobile/WFApp;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/BaseActivity;-><init>()V

    .line 50
    new-instance v0, Lcom/wf/wellsfargomobile/loc/Location$1;

    invoke-direct {v0, p0}, Lcom/wf/wellsfargomobile/loc/Location$1;-><init>(Lcom/wf/wellsfargomobile/loc/Location;)V

    iput-object v0, p0, Lcom/wf/wellsfargomobile/loc/Location;->buttonHandler:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/wf/wellsfargomobile/loc/Location;)Landroid/widget/Button;
    .locals 1
    .parameter "x0"

    .prologue
    .line 23
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/Location;->currentLocButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$100(Lcom/wf/wellsfargomobile/loc/Location;)Landroid/widget/Button;
    .locals 1
    .parameter "x0"

    .prologue
    .line 23
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/Location;->locSearchButton:Landroid/widget/Button;

    return-object v0
.end method

.method private showDialogBox(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .parameter "title"
    .parameter "message"

    .prologue
    const v1, 0x1080027

    .line 94
    if-eqz p1, :cond_0

    .line 95
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "OK"

    new-instance v2, Lcom/wf/wellsfargomobile/loc/Location$2;

    invoke-direct {v2, p0}, Lcom/wf/wellsfargomobile/loc/Location$2;-><init>(Lcom/wf/wellsfargomobile/loc/Location;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 120
    :goto_0
    return-void

    .line 108
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "OK"

    new-instance v2, Lcom/wf/wellsfargomobile/loc/Location$3;

    invoke-direct {v2, p0}, Lcom/wf/wellsfargomobile/loc/Location$3;-><init>(Lcom/wf/wellsfargomobile/loc/Location;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 8
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "intent"

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x6

    const/4 v5, 0x3

    const/4 v4, 0x1

    .line 69
    invoke-super {p0, p1, p2, p3}, Lcom/wf/wellsfargomobile/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 70
    if-eq p1, v4, :cond_0

    if-nez p1, :cond_2

    .line 71
    :cond_0
    const/4 v3, 0x2

    if-eq p2, v3, :cond_1

    const/4 v3, 0x4

    if-ne p2, v3, :cond_3

    .line 72
    :cond_1
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/Location;->finish()V

    .line 91
    :cond_2
    :goto_0
    return-void

    .line 73
    :cond_3
    if-ne p2, v5, :cond_4

    .line 74
    invoke-virtual {p0, v5, v7}, Lcom/wf/wellsfargomobile/loc/Location;->setResult(ILandroid/content/Intent;)V

    .line 75
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/Location;->finish()V

    goto :goto_0

    .line 76
    :cond_4
    if-ne p2, v6, :cond_5

    .line 77
    invoke-virtual {p0, v6, v7}, Lcom/wf/wellsfargomobile/loc/Location;->setResult(ILandroid/content/Intent;)V

    .line 78
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/Location;->finish()V

    goto :goto_0

    .line 79
    :cond_5
    if-ne p2, v4, :cond_2

    .line 80
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 82
    .local v0, extras:Landroid/os/Bundle;
    const-string v3, "error_title"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 83
    const-string v3, "error_title"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 87
    .local v2, title:Ljava/lang/String;
    :goto_1
    const-string v3, "error_msg"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 88
    .local v1, message:Ljava/lang/String;
    invoke-direct {p0, v2, v1}, Lcom/wf/wellsfargomobile/loc/Location;->showDialogBox(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 85
    .end local v1           #message:Ljava/lang/String;
    .end local v2           #title:Ljava/lang/String;
    :cond_6
    const/4 v2, 0x0

    .restart local v2       #title:Ljava/lang/String;
    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedInstanceState"

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/wf/wellsfargomobile/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    const v0, 0x7f030003

    invoke-virtual {p0, v0}, Lcom/wf/wellsfargomobile/loc/Location;->setContentView(I)V

    .line 39
    const-string v0, "Location"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    const v0, 0x7f060014

    invoke-virtual {p0, v0}, Lcom/wf/wellsfargomobile/loc/Location;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/wf/wellsfargomobile/loc/Location;->currentLocButton:Landroid/widget/Button;

    .line 42
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/Location;->currentLocButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/Location;->buttonHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 44
    const v0, 0x7f060015

    invoke-virtual {p0, v0}, Lcom/wf/wellsfargomobile/loc/Location;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/wf/wellsfargomobile/loc/Location;->locSearchButton:Landroid/widget/Button;

    .line 45
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/Location;->locSearchButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/Location;->buttonHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/Location;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/wf/wellsfargomobile/WFApp;

    iput-object v0, p0, Lcom/wf/wellsfargomobile/loc/Location;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    .line 48
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .parameter "menu"

    .prologue
    .line 124
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/Location;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-virtual {v0}, Lcom/wf/wellsfargomobile/WFApp;->getMrdcEnabled()Lcom/wf/wellsfargomobile/util/Trinary;

    move-result-object v0

    sget-object v1, Lcom/wf/wellsfargomobile/util/Trinary;->TRUE:Lcom/wf/wellsfargomobile/util/Trinary;

    invoke-virtual {v0, v1}, Lcom/wf/wellsfargomobile/util/Trinary;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/Location;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0a0001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 129
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 127
    :cond_0
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/Location;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0a0002

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .parameter "item"

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 134
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 147
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 136
    :pswitch_0
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/Location;->finish()V

    goto :goto_0

    .line 139
    :pswitch_1
    const/4 v1, 0x3

    invoke-virtual {p0, v1, v2}, Lcom/wf/wellsfargomobile/loc/Location;->setResult(ILandroid/content/Intent;)V

    .line 140
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/Location;->finish()V

    goto :goto_0

    .line 143
    :pswitch_2
    const/4 v1, 0x6

    invoke-virtual {p0, v1, v2}, Lcom/wf/wellsfargomobile/loc/Location;->setResult(ILandroid/content/Intent;)V

    .line 144
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/Location;->finish()V

    goto :goto_0

    .line 134
    nop

    :pswitch_data_0
    .packed-switch 0x7f06007e
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
