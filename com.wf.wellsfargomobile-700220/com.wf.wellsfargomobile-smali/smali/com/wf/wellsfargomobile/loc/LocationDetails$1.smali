.class Lcom/wf/wellsfargomobile/loc/LocationDetails$1;
.super Ljava/lang/Object;
.source "LocationDetails.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wf/wellsfargomobile/loc/LocationDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/wf/wellsfargomobile/loc/LocationDetails;


# direct methods
.method constructor <init>(Lcom/wf/wellsfargomobile/loc/LocationDetails;)V
    .locals 0
    .parameter

    .prologue
    .line 118
    iput-object p1, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails$1;->this$0:Lcom/wf/wellsfargomobile/loc/LocationDetails;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .parameter "v"

    .prologue
    .line 121
    move-object v1, p1

    check-cast v1, Landroid/widget/Button;

    .line 122
    .local v1, clicked:Landroid/widget/Button;
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails$1;->this$0:Lcom/wf/wellsfargomobile/loc/LocationDetails;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationDetails;->directions:Landroid/widget/Button;
    invoke-static {v3}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->access$000(Lcom/wf/wellsfargomobile/loc/LocationDetails;)Landroid/widget/Button;

    move-result-object v3

    if-ne v1, v3, :cond_4

    .line 123
    const-string v0, ""

    .line 124
    .local v0, addressText:Ljava/lang/String;
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails$1;->this$0:Lcom/wf/wellsfargomobile/loc/LocationDetails;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationDetails;->myBranch:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->access$100(Lcom/wf/wellsfargomobile/loc/LocationDetails;)Ljava/util/HashMap;

    move-result-object v3

    const-string v4, "addrLine1"

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 125
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails$1;->this$0:Lcom/wf/wellsfargomobile/loc/LocationDetails;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationDetails;->myBranch:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->access$100(Lcom/wf/wellsfargomobile/loc/LocationDetails;)Ljava/util/HashMap;

    move-result-object v3

    const-string v5, "addrLine1"

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 127
    :cond_0
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails$1;->this$0:Lcom/wf/wellsfargomobile/loc/LocationDetails;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationDetails;->myBranch:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->access$100(Lcom/wf/wellsfargomobile/loc/LocationDetails;)Ljava/util/HashMap;

    move-result-object v3

    const-string v4, "city"

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 128
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails$1;->this$0:Lcom/wf/wellsfargomobile/loc/LocationDetails;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationDetails;->myBranch:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->access$100(Lcom/wf/wellsfargomobile/loc/LocationDetails;)Ljava/util/HashMap;

    move-result-object v3

    const-string v5, "city"

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 130
    :cond_1
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails$1;->this$0:Lcom/wf/wellsfargomobile/loc/LocationDetails;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationDetails;->myBranch:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->access$100(Lcom/wf/wellsfargomobile/loc/LocationDetails;)Ljava/util/HashMap;

    move-result-object v3

    const-string v4, "state"

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 131
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails$1;->this$0:Lcom/wf/wellsfargomobile/loc/LocationDetails;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationDetails;->myBranch:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->access$100(Lcom/wf/wellsfargomobile/loc/LocationDetails;)Ljava/util/HashMap;

    move-result-object v3

    const-string v5, "state"

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 133
    :cond_2
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails$1;->this$0:Lcom/wf/wellsfargomobile/loc/LocationDetails;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationDetails;->myBranch:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->access$100(Lcom/wf/wellsfargomobile/loc/LocationDetails;)Ljava/util/HashMap;

    move-result-object v3

    const-string v4, "postalcode"

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 134
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails$1;->this$0:Lcom/wf/wellsfargomobile/loc/LocationDetails;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationDetails;->myBranch:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->access$100(Lcom/wf/wellsfargomobile/loc/LocationDetails;)Ljava/util/HashMap;

    move-result-object v3

    const-string v5, "postalcode"

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 137
    :cond_3
    :try_start_0
    const-string v3, "UTF-8"

    invoke-static {v0, v3}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 140
    :goto_0
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "http://maps.google.com/maps?daddr="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 143
    .local v2, navigation:Landroid/content/Intent;
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails$1;->this$0:Lcom/wf/wellsfargomobile/loc/LocationDetails;

    invoke-virtual {v3, v2}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->startActivity(Landroid/content/Intent;)V

    .line 145
    .end local v0           #addressText:Ljava/lang/String;
    .end local v2           #navigation:Landroid/content/Intent;
    :cond_4
    return-void

    .line 138
    .restart local v0       #addressText:Ljava/lang/String;
    :catch_0
    move-exception v3

    goto :goto_0
.end method
