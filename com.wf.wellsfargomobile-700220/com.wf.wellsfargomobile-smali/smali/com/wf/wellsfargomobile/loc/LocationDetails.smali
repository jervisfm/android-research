.class public Lcom/wf/wellsfargomobile/loc/LocationDetails;
.super Lcom/wf/wellsfargomobile/BaseActivity;
.source "LocationDetails.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "LocationDetails"


# instance fields
.field private address:Landroid/widget/TextView;

.field private final buttonHandler:Landroid/view/View$OnClickListener;

.field private directions:Landroid/widget/Button;

.field private hours:Landroid/widget/TextView;

.field private hoursContainer:Landroid/widget/LinearLayout;

.field private myBranch:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private phone:Landroid/widget/TextView;

.field private phoneContainer:Landroid/widget/LinearLayout;

.field private services:Landroid/widget/TextView;

.field private servicesContainer:Landroid/widget/LinearLayout;

.field private wfApp:Lcom/wf/wellsfargomobile/WFApp;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/BaseActivity;-><init>()V

    .line 118
    new-instance v0, Lcom/wf/wellsfargomobile/loc/LocationDetails$1;

    invoke-direct {v0, p0}, Lcom/wf/wellsfargomobile/loc/LocationDetails$1;-><init>(Lcom/wf/wellsfargomobile/loc/LocationDetails;)V

    iput-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->buttonHandler:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/wf/wellsfargomobile/loc/LocationDetails;)Landroid/widget/Button;
    .locals 1
    .parameter "x0"

    .prologue
    .line 29
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->directions:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$100(Lcom/wf/wellsfargomobile/loc/LocationDetails;)Ljava/util/HashMap;
    .locals 1
    .parameter "x0"

    .prologue
    .line 29
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->myBranch:Ljava/util/HashMap;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .parameter "savedInstanceState"

    .prologue
    const/16 v11, 0xa

    const/4 v10, 0x6

    const/4 v9, 0x3

    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 43
    invoke-super {p0, p1}, Lcom/wf/wellsfargomobile/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    const v4, 0x7f030004

    invoke-virtual {p0, v4}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->setContentView(I)V

    .line 46
    const-string v4, "LocationDetails"

    const-string v5, "onCreate"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    const v4, 0x7f060017

    invoke-virtual {p0, v4}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->address:Landroid/widget/TextView;

    .line 49
    const v4, 0x7f06001a

    invoke-virtual {p0, v4}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->phone:Landroid/widget/TextView;

    .line 50
    const v4, 0x7f06001c

    invoke-virtual {p0, v4}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->hours:Landroid/widget/TextView;

    .line 51
    const v4, 0x7f06001e

    invoke-virtual {p0, v4}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->services:Landroid/widget/TextView;

    .line 53
    const v4, 0x7f060019

    invoke-virtual {p0, v4}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->phoneContainer:Landroid/widget/LinearLayout;

    .line 54
    const v4, 0x7f06001b

    invoke-virtual {p0, v4}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->hoursContainer:Landroid/widget/LinearLayout;

    .line 55
    const v4, 0x7f06001d

    invoke-virtual {p0, v4}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->servicesContainer:Landroid/widget/LinearLayout;

    .line 57
    const v4, 0x7f06001f

    invoke-virtual {p0, v4}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->directions:Landroid/widget/Button;

    .line 58
    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->directions:Landroid/widget/Button;

    iget-object v5, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->buttonHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 61
    .local v2, extras:Landroid/os/Bundle;
    if-eqz v2, :cond_7

    .line 62
    const-string v4, "branch_object"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/HashMap;

    iput-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->myBranch:Ljava/util/HashMap;

    .line 67
    :goto_0
    const-string v0, ""

    .line 68
    .local v0, addressText:Ljava/lang/String;
    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->myBranch:Ljava/util/HashMap;

    const-string v5, "siteName"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 69
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->myBranch:Ljava/util/HashMap;

    const-string v6, "siteName"

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 71
    :cond_0
    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->myBranch:Ljava/util/HashMap;

    const-string v5, "addrLine1"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 72
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->myBranch:Ljava/util/HashMap;

    const-string v6, "addrLine1"

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 74
    :cond_1
    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->myBranch:Ljava/util/HashMap;

    const-string v5, "city"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 75
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->myBranch:Ljava/util/HashMap;

    const-string v6, "city"

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 77
    :cond_2
    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->myBranch:Ljava/util/HashMap;

    const-string v5, "state"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 78
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->myBranch:Ljava/util/HashMap;

    const-string v6, "state"

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 80
    :cond_3
    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->myBranch:Ljava/util/HashMap;

    const-string v5, "postalcode"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 81
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->myBranch:Ljava/util/HashMap;

    const-string v6, "postalcode"

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 83
    :cond_4
    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->address:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->myBranch:Ljava/util/HashMap;

    const-string v5, "phone"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 86
    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->myBranch:Ljava/util/HashMap;

    const-string v5, "phone"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 87
    .local v3, ph:Ljava/lang/String;
    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->phone:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v7, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->phone:Landroid/widget/TextView;

    const/4 v5, 0x4

    invoke-static {v4, v5}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    .line 89
    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->phoneContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 94
    .end local v3           #ph:Ljava/lang/String;
    :goto_1
    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->myBranch:Ljava/util/HashMap;

    const-string v5, "hours"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 95
    iget-object v5, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->hours:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->myBranch:Ljava/util/HashMap;

    const-string v6, "hours"

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const/16 v6, 0x7c

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->hoursContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 101
    :goto_2
    const-string v1, ""

    .line 102
    .local v1, branchServices:Ljava/lang/String;
    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->myBranch:Ljava/util/HashMap;

    const-string v5, "serviceType"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 103
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->myBranch:Ljava/util/HashMap;

    const-string v6, "serviceType"

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 105
    :cond_5
    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->myBranch:Ljava/util/HashMap;

    const-string v5, "atmCount"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 106
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->myBranch:Ljava/util/HashMap;

    const-string v6, "atmCount"

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ATM(s)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 108
    :cond_6
    const-string v4, ""

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_a

    .line 109
    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->services:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->servicesContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 115
    :goto_3
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->getApplication()Landroid/app/Application;

    move-result-object v4

    check-cast v4, Lcom/wf/wellsfargomobile/WFApp;

    iput-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    .line 116
    return-void

    .line 64
    .end local v0           #addressText:Ljava/lang/String;
    .end local v1           #branchServices:Ljava/lang/String;
    :cond_7
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->finish()V

    goto/16 :goto_0

    .line 91
    .restart local v0       #addressText:Ljava/lang/String;
    :cond_8
    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->phoneContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 98
    :cond_9
    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->hoursContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_2

    .line 112
    .restart local v1       #branchServices:Ljava/lang/String;
    :cond_a
    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->servicesContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_3
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .parameter "menu"

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 151
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_1

    const-string v1, "in_session"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "in_session"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 154
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationDetails;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-virtual {v1}, Lcom/wf/wellsfargomobile/WFApp;->getMrdcEnabled()Lcom/wf/wellsfargomobile/util/Trinary;

    move-result-object v1

    sget-object v2, Lcom/wf/wellsfargomobile/util/Trinary;->TRUE:Lcom/wf/wellsfargomobile/util/Trinary;

    invoke-virtual {v1, v2}, Lcom/wf/wellsfargomobile/util/Trinary;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 155
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const v2, 0x7f0a0001

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 163
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 157
    :cond_0
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const v2, 0x7f0a0002

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0

    .line 161
    :cond_1
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const v2, 0x7f0a0003

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .parameter "item"

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 168
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 186
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 170
    :pswitch_0
    const/4 v1, 0x4

    invoke-virtual {p0, v1, v2}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->setResult(ILandroid/content/Intent;)V

    .line 171
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->finish()V

    goto :goto_0

    .line 174
    :pswitch_1
    const/4 v1, 0x2

    invoke-virtual {p0, v1, v2}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->setResult(ILandroid/content/Intent;)V

    .line 175
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->finish()V

    goto :goto_0

    .line 178
    :pswitch_2
    const/4 v1, 0x3

    invoke-virtual {p0, v1, v2}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->setResult(ILandroid/content/Intent;)V

    .line 179
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->finish()V

    goto :goto_0

    .line 182
    :pswitch_3
    const/4 v1, 0x6

    invoke-virtual {p0, v1, v2}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->setResult(ILandroid/content/Intent;)V

    .line 183
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationDetails;->finish()V

    goto :goto_0

    .line 168
    nop

    :pswitch_data_0
    .packed-switch 0x7f06007e
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
