.class Lcom/wf/wellsfargomobile/loc/LocationForm$1;
.super Ljava/lang/Object;
.source "LocationForm.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wf/wellsfargomobile/loc/LocationForm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/wf/wellsfargomobile/loc/LocationForm;


# direct methods
.method constructor <init>(Lcom/wf/wellsfargomobile/loc/LocationForm;)V
    .locals 0
    .parameter

    .prologue
    .line 126
    iput-object p1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm$1;->this$0:Lcom/wf/wellsfargomobile/loc/LocationForm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .parameter "buttonView"
    .parameter "isChecked"

    .prologue
    .line 129
    move-object v0, p1

    check-cast v0, Landroid/widget/ToggleButton;

    .line 130
    .local v0, clicked:Landroid/widget/ToggleButton;
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm$1;->this$0:Lcom/wf/wellsfargomobile/loc/LocationForm;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationForm;->togAtm:Landroid/widget/ToggleButton;
    invoke-static {v1}, Lcom/wf/wellsfargomobile/loc/LocationForm;->access$000(Lcom/wf/wellsfargomobile/loc/LocationForm;)Landroid/widget/ToggleButton;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 131
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm$1;->this$0:Lcom/wf/wellsfargomobile/loc/LocationForm;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    #setter for: Lcom/wf/wellsfargomobile/loc/LocationForm;->togAtmValue:Ljava/lang/Boolean;
    invoke-static {v1, v2}, Lcom/wf/wellsfargomobile/loc/LocationForm;->access$102(Lcom/wf/wellsfargomobile/loc/LocationForm;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 132
    :cond_1
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm$1;->this$0:Lcom/wf/wellsfargomobile/loc/LocationForm;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationForm;->togBranch:Landroid/widget/ToggleButton;
    invoke-static {v1}, Lcom/wf/wellsfargomobile/loc/LocationForm;->access$200(Lcom/wf/wellsfargomobile/loc/LocationForm;)Landroid/widget/ToggleButton;

    move-result-object v1

    if-ne v0, v1, :cond_2

    .line 133
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm$1;->this$0:Lcom/wf/wellsfargomobile/loc/LocationForm;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    #setter for: Lcom/wf/wellsfargomobile/loc/LocationForm;->togBranchValue:Ljava/lang/Boolean;
    invoke-static {v1, v2}, Lcom/wf/wellsfargomobile/loc/LocationForm;->access$302(Lcom/wf/wellsfargomobile/loc/LocationForm;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    goto :goto_0

    .line 134
    :cond_2
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm$1;->this$0:Lcom/wf/wellsfargomobile/loc/LocationForm;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationForm;->togInStore:Landroid/widget/ToggleButton;
    invoke-static {v1}, Lcom/wf/wellsfargomobile/loc/LocationForm;->access$400(Lcom/wf/wellsfargomobile/loc/LocationForm;)Landroid/widget/ToggleButton;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 135
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm$1;->this$0:Lcom/wf/wellsfargomobile/loc/LocationForm;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    #setter for: Lcom/wf/wellsfargomobile/loc/LocationForm;->togInStoreValue:Ljava/lang/Boolean;
    invoke-static {v1, v2}, Lcom/wf/wellsfargomobile/loc/LocationForm;->access$502(Lcom/wf/wellsfargomobile/loc/LocationForm;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    goto :goto_0
.end method
