.class Lcom/wf/wellsfargomobile/loc/LocationForm$3;
.super Ljava/lang/Object;
.source "LocationForm.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wf/wellsfargomobile/loc/LocationForm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/wf/wellsfargomobile/loc/LocationForm;


# direct methods
.method constructor <init>(Lcom/wf/wellsfargomobile/loc/LocationForm;)V
    .locals 0
    .parameter

    .prologue
    .line 168
    iput-object p1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm$3;->this$0:Lcom/wf/wellsfargomobile/loc/LocationForm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 2
    .parameter "v"
    .parameter "actionId"
    .parameter "event"

    .prologue
    .line 171
    const/4 v1, 0x2

    if-eq p2, v1, :cond_0

    if-nez p2, :cond_3

    :cond_0
    move-object v0, p1

    .line 173
    check-cast v0, Landroid/widget/EditText;

    .line 174
    .local v0, focused:Landroid/widget/EditText;
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm$3;->this$0:Lcom/wf/wellsfargomobile/loc/LocationForm;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationForm;->zip:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/wf/wellsfargomobile/loc/LocationForm;->access$1000(Lcom/wf/wellsfargomobile/loc/LocationForm;)Landroid/widget/EditText;

    move-result-object v1

    if-ne v0, v1, :cond_2

    .line 175
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm$3;->this$0:Lcom/wf/wellsfargomobile/loc/LocationForm;

    #calls: Lcom/wf/wellsfargomobile/loc/LocationForm;->checkZip()V
    invoke-static {v1}, Lcom/wf/wellsfargomobile/loc/LocationForm;->access$700(Lcom/wf/wellsfargomobile/loc/LocationForm;)V

    .line 179
    :cond_1
    :goto_0
    const/4 v1, 0x1

    .line 181
    .end local v0           #focused:Landroid/widget/EditText;
    :goto_1
    return v1

    .line 176
    .restart local v0       #focused:Landroid/widget/EditText;
    :cond_2
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm$3;->this$0:Lcom/wf/wellsfargomobile/loc/LocationForm;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationForm;->state:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/wf/wellsfargomobile/loc/LocationForm;->access$1100(Lcom/wf/wellsfargomobile/loc/LocationForm;)Landroid/widget/EditText;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 177
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm$3;->this$0:Lcom/wf/wellsfargomobile/loc/LocationForm;

    #calls: Lcom/wf/wellsfargomobile/loc/LocationForm;->checkState()V
    invoke-static {v1}, Lcom/wf/wellsfargomobile/loc/LocationForm;->access$900(Lcom/wf/wellsfargomobile/loc/LocationForm;)V

    goto :goto_0

    .line 181
    .end local v0           #focused:Landroid/widget/EditText;
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method
