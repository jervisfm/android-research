.class public Lcom/wf/wellsfargomobile/loc/LocationForm;
.super Lcom/wf/wellsfargomobile/BaseActivity;
.source "LocationForm.java"


# static fields
.field public static final ACTIVITY_SEARCH:I = 0x0

.field public static final KEY_ADDRESS:Ljava/lang/String; = "address_string"

.field public static final KEY_CITY:Ljava/lang/String; = "city_string"

.field public static final KEY_STATE:Ljava/lang/String; = "state_string"

.field public static final KEY_TOGATM:Ljava/lang/String; = "toggle_atm"

.field public static final KEY_TOGBRANCH:Ljava/lang/String; = "toggle_branch"

.field public static final KEY_TOGINSTORE:Ljava/lang/String; = "toggle_instore"

.field public static final KEY_ZIP:Ljava/lang/String; = "zip_string"

.field private static final TAG:Ljava/lang/String; = "LocationForm"


# instance fields
.field private actionHandler:Landroid/widget/TextView$OnEditorActionListener;

.field private address:Landroid/widget/EditText;

.field private buttonHandler:Landroid/view/View$OnClickListener;

.field private city:Landroid/widget/EditText;

.field private inSession:Ljava/lang/Boolean;

.field private state:Landroid/widget/EditText;

.field private stateGo:Landroid/widget/Button;

.field private togAtm:Landroid/widget/ToggleButton;

.field private togAtmValue:Ljava/lang/Boolean;

.field private togBranch:Landroid/widget/ToggleButton;

.field private togBranchValue:Ljava/lang/Boolean;

.field private togInStore:Landroid/widget/ToggleButton;

.field private togInStoreValue:Ljava/lang/Boolean;

.field private toggleHandler:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private wfApp:Lcom/wf/wellsfargomobile/WFApp;

.field private zip:Landroid/widget/EditText;

.field private zipGo:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 30
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/BaseActivity;-><init>()V

    .line 48
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->togAtmValue:Ljava/lang/Boolean;

    .line 49
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->togBranchValue:Ljava/lang/Boolean;

    .line 50
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->togInStoreValue:Ljava/lang/Boolean;

    .line 126
    new-instance v0, Lcom/wf/wellsfargomobile/loc/LocationForm$1;

    invoke-direct {v0, p0}, Lcom/wf/wellsfargomobile/loc/LocationForm$1;-><init>(Lcom/wf/wellsfargomobile/loc/LocationForm;)V

    iput-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->toggleHandler:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 156
    new-instance v0, Lcom/wf/wellsfargomobile/loc/LocationForm$2;

    invoke-direct {v0, p0}, Lcom/wf/wellsfargomobile/loc/LocationForm$2;-><init>(Lcom/wf/wellsfargomobile/loc/LocationForm;)V

    iput-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->buttonHandler:Landroid/view/View$OnClickListener;

    .line 168
    new-instance v0, Lcom/wf/wellsfargomobile/loc/LocationForm$3;

    invoke-direct {v0, p0}, Lcom/wf/wellsfargomobile/loc/LocationForm$3;-><init>(Lcom/wf/wellsfargomobile/loc/LocationForm;)V

    iput-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->actionHandler:Landroid/widget/TextView$OnEditorActionListener;

    return-void
.end method

.method static synthetic access$000(Lcom/wf/wellsfargomobile/loc/LocationForm;)Landroid/widget/ToggleButton;
    .locals 1
    .parameter "x0"

    .prologue
    .line 30
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->togAtm:Landroid/widget/ToggleButton;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/wf/wellsfargomobile/loc/LocationForm;)Landroid/widget/EditText;
    .locals 1
    .parameter "x0"

    .prologue
    .line 30
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->zip:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$102(Lcom/wf/wellsfargomobile/loc/LocationForm;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 30
    iput-object p1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->togAtmValue:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/wf/wellsfargomobile/loc/LocationForm;)Landroid/widget/EditText;
    .locals 1
    .parameter "x0"

    .prologue
    .line 30
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->state:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$200(Lcom/wf/wellsfargomobile/loc/LocationForm;)Landroid/widget/ToggleButton;
    .locals 1
    .parameter "x0"

    .prologue
    .line 30
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->togBranch:Landroid/widget/ToggleButton;

    return-object v0
.end method

.method static synthetic access$302(Lcom/wf/wellsfargomobile/loc/LocationForm;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 30
    iput-object p1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->togBranchValue:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$400(Lcom/wf/wellsfargomobile/loc/LocationForm;)Landroid/widget/ToggleButton;
    .locals 1
    .parameter "x0"

    .prologue
    .line 30
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->togInStore:Landroid/widget/ToggleButton;

    return-object v0
.end method

.method static synthetic access$502(Lcom/wf/wellsfargomobile/loc/LocationForm;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 30
    iput-object p1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->togInStoreValue:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$600(Lcom/wf/wellsfargomobile/loc/LocationForm;)Landroid/widget/Button;
    .locals 1
    .parameter "x0"

    .prologue
    .line 30
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->zipGo:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$700(Lcom/wf/wellsfargomobile/loc/LocationForm;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/loc/LocationForm;->checkZip()V

    return-void
.end method

.method static synthetic access$800(Lcom/wf/wellsfargomobile/loc/LocationForm;)Landroid/widget/Button;
    .locals 1
    .parameter "x0"

    .prologue
    .line 30
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->stateGo:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$900(Lcom/wf/wellsfargomobile/loc/LocationForm;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/loc/LocationForm;->checkState()V

    return-void
.end method

.method private checkState()V
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->city:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->state:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 150
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/wf/wellsfargomobile/loc/LocationForm;->launchSearchWithAddress(Ljava/lang/Boolean;)V

    .line 154
    :goto_0
    return-void

    .line 152
    :cond_0
    const/4 v0, 0x0

    const v1, 0x7f070009

    invoke-virtual {p0, v1}, Lcom/wf/wellsfargomobile/loc/LocationForm;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/wf/wellsfargomobile/loc/LocationForm;->showDialogBox(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private checkZip()V
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->zip:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 142
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/wf/wellsfargomobile/loc/LocationForm;->launchSearchWithZip(Ljava/lang/Boolean;)V

    .line 146
    :goto_0
    return-void

    .line 144
    :cond_0
    const/4 v0, 0x0

    const v1, 0x7f07006d

    invoke-virtual {p0, v1}, Lcom/wf/wellsfargomobile/loc/LocationForm;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/wf/wellsfargomobile/loc/LocationForm;->showDialogBox(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private launchSearchWithAddress(Ljava/lang/Boolean;)V
    .locals 4
    .parameter "options"

    .prologue
    const/4 v3, 0x0

    .line 106
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/wf/wellsfargomobile/loc/LocationResults;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 107
    .local v0, locSearchIntent:Landroid/content/Intent;
    const-string v1, "was_gps"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 108
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->address:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 109
    const-string v1, "address_string"

    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->address:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    :cond_0
    const-string v1, "city_string"

    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->city:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 112
    const-string v1, "state_string"

    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->state:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 116
    const-string v1, "zip_string"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 117
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 118
    const-string v1, "toggle_atm"

    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->togAtmValue:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 119
    const-string v1, "toggle_branch"

    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->togBranchValue:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 120
    const-string v1, "toggle_instore"

    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->togInStoreValue:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 122
    :cond_1
    const-string v1, "in_session"

    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->inSession:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 123
    invoke-virtual {p0, v0, v3}, Lcom/wf/wellsfargomobile/loc/LocationForm;->startActivityForResult(Landroid/content/Intent;I)V

    .line 124
    return-void
.end method

.method private launchSearchWithZip(Ljava/lang/Boolean;)V
    .locals 4
    .parameter "options"

    .prologue
    const/4 v3, 0x0

    .line 93
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/wf/wellsfargomobile/loc/LocationResults;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 94
    .local v0, locSearchIntent:Landroid/content/Intent;
    const-string v1, "was_gps"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 95
    const-string v1, "zip_string"

    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->zip:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 96
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 97
    const-string v1, "toggle_atm"

    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->togAtmValue:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 98
    const-string v1, "toggle_branch"

    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->togBranchValue:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 99
    const-string v1, "toggle_instore"

    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->togInStoreValue:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 101
    :cond_0
    const-string v1, "in_session"

    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->inSession:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 102
    invoke-virtual {p0, v0, v3}, Lcom/wf/wellsfargomobile/loc/LocationForm;->startActivityForResult(Landroid/content/Intent;I)V

    .line 103
    return-void
.end method

.method private showDialogBox(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .parameter "title"
    .parameter "message"

    .prologue
    const v1, 0x1080027

    .line 216
    if-eqz p1, :cond_0

    .line 217
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "OK"

    new-instance v2, Lcom/wf/wellsfargomobile/loc/LocationForm$4;

    invoke-direct {v2, p0}, Lcom/wf/wellsfargomobile/loc/LocationForm$4;-><init>(Lcom/wf/wellsfargomobile/loc/LocationForm;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 242
    :goto_0
    return-void

    .line 230
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "OK"

    new-instance v2, Lcom/wf/wellsfargomobile/loc/LocationForm$5;

    invoke-direct {v2, p0}, Lcom/wf/wellsfargomobile/loc/LocationForm$5;-><init>(Lcom/wf/wellsfargomobile/loc/LocationForm;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 8
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "intent"

    .prologue
    const/4 v7, 0x6

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 187
    invoke-super {p0, p1, p2, p3}, Lcom/wf/wellsfargomobile/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 188
    if-nez p1, :cond_0

    .line 189
    if-ne p2, v4, :cond_1

    .line 190
    invoke-virtual {p0, v4, v3}, Lcom/wf/wellsfargomobile/loc/LocationForm;->setResult(ILandroid/content/Intent;)V

    .line 191
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationForm;->finish()V

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 192
    :cond_1
    if-ne p2, v6, :cond_2

    .line 193
    invoke-virtual {p0, v6, v3}, Lcom/wf/wellsfargomobile/loc/LocationForm;->setResult(ILandroid/content/Intent;)V

    .line 194
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationForm;->finish()V

    goto :goto_0

    .line 195
    :cond_2
    if-ne p2, v5, :cond_3

    .line 196
    invoke-virtual {p0, v5, v3}, Lcom/wf/wellsfargomobile/loc/LocationForm;->setResult(ILandroid/content/Intent;)V

    .line 197
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationForm;->finish()V

    goto :goto_0

    .line 198
    :cond_3
    if-ne p2, v7, :cond_4

    .line 199
    invoke-virtual {p0, v7, v3}, Lcom/wf/wellsfargomobile/loc/LocationForm;->setResult(ILandroid/content/Intent;)V

    .line 200
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationForm;->finish()V

    goto :goto_0

    .line 201
    :cond_4
    const/4 v3, 0x1

    if-ne p2, v3, :cond_0

    .line 202
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 204
    .local v0, extras:Landroid/os/Bundle;
    const-string v3, "error_title"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 205
    const-string v3, "error_title"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 209
    .local v2, title:Ljava/lang/String;
    :goto_1
    const-string v3, "error_msg"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 210
    .local v1, message:Ljava/lang/String;
    invoke-direct {p0, v2, v1}, Lcom/wf/wellsfargomobile/loc/LocationForm;->showDialogBox(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 207
    .end local v1           #message:Ljava/lang/String;
    .end local v2           #title:Ljava/lang/String;
    :cond_5
    const/4 v2, 0x0

    .restart local v2       #title:Ljava/lang/String;
    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter "savedInstanceState"

    .prologue
    const/4 v3, 0x0

    .line 55
    invoke-super {p0, p1}, Lcom/wf/wellsfargomobile/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 56
    const v1, 0x7f030005

    invoke-virtual {p0, v1}, Lcom/wf/wellsfargomobile/loc/LocationForm;->setContentView(I)V

    .line 58
    const-string v1, "LocationForm"

    const-string v2, "onCreate"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    const v1, 0x7f060022

    invoke-virtual {p0, v1}, Lcom/wf/wellsfargomobile/loc/LocationForm;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->zipGo:Landroid/widget/Button;

    .line 61
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->zipGo:Landroid/widget/Button;

    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->buttonHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    const v1, 0x7f060026

    invoke-virtual {p0, v1}, Lcom/wf/wellsfargomobile/loc/LocationForm;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->stateGo:Landroid/widget/Button;

    .line 64
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->stateGo:Landroid/widget/Button;

    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->buttonHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    const v1, 0x7f060021

    invoke-virtual {p0, v1}, Lcom/wf/wellsfargomobile/loc/LocationForm;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->zip:Landroid/widget/EditText;

    .line 67
    const v1, 0x7f060023

    invoke-virtual {p0, v1}, Lcom/wf/wellsfargomobile/loc/LocationForm;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->address:Landroid/widget/EditText;

    .line 68
    const v1, 0x7f060024

    invoke-virtual {p0, v1}, Lcom/wf/wellsfargomobile/loc/LocationForm;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->city:Landroid/widget/EditText;

    .line 69
    const v1, 0x7f060025

    invoke-virtual {p0, v1}, Lcom/wf/wellsfargomobile/loc/LocationForm;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->state:Landroid/widget/EditText;

    .line 71
    const v1, 0x7f060027

    invoke-virtual {p0, v1}, Lcom/wf/wellsfargomobile/loc/LocationForm;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ToggleButton;

    iput-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->togAtm:Landroid/widget/ToggleButton;

    .line 72
    const v1, 0x7f060028

    invoke-virtual {p0, v1}, Lcom/wf/wellsfargomobile/loc/LocationForm;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ToggleButton;

    iput-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->togBranch:Landroid/widget/ToggleButton;

    .line 73
    const v1, 0x7f060029

    invoke-virtual {p0, v1}, Lcom/wf/wellsfargomobile/loc/LocationForm;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ToggleButton;

    iput-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->togInStore:Landroid/widget/ToggleButton;

    .line 74
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->togAtm:Landroid/widget/ToggleButton;

    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->toggleHandler:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/ToggleButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 75
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->togBranch:Landroid/widget/ToggleButton;

    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->toggleHandler:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/ToggleButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 76
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->togInStore:Landroid/widget/ToggleButton;

    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->toggleHandler:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/ToggleButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 78
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->zip:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->actionHandler:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 79
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->state:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->actionHandler:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 81
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationForm;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 82
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_0

    const-string v1, "in_session"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "in_session"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 84
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->inSession:Ljava/lang/Boolean;

    .line 89
    :goto_0
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationForm;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/wf/wellsfargomobile/WFApp;

    iput-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    .line 90
    return-void

    .line 86
    :cond_0
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->inSession:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .parameter "menu"

    .prologue
    .line 246
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationForm;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 247
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_1

    const-string v1, "in_session"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "in_session"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 250
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationForm;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-virtual {v1}, Lcom/wf/wellsfargomobile/WFApp;->getMrdcEnabled()Lcom/wf/wellsfargomobile/util/Trinary;

    move-result-object v1

    sget-object v2, Lcom/wf/wellsfargomobile/util/Trinary;->TRUE:Lcom/wf/wellsfargomobile/util/Trinary;

    invoke-virtual {v1, v2}, Lcom/wf/wellsfargomobile/util/Trinary;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 251
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationForm;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const v2, 0x7f0a0001

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 259
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 253
    :cond_0
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationForm;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const v2, 0x7f0a0002

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0

    .line 257
    :cond_1
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationForm;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const v2, 0x7f0a0003

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .parameter "item"

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 264
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 281
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 266
    :pswitch_0
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationForm;->finish()V

    goto :goto_0

    .line 269
    :pswitch_1
    const/4 v1, 0x2

    invoke-virtual {p0, v1, v2}, Lcom/wf/wellsfargomobile/loc/LocationForm;->setResult(ILandroid/content/Intent;)V

    .line 270
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationForm;->finish()V

    goto :goto_0

    .line 273
    :pswitch_2
    const/4 v1, 0x3

    invoke-virtual {p0, v1, v2}, Lcom/wf/wellsfargomobile/loc/LocationForm;->setResult(ILandroid/content/Intent;)V

    .line 274
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationForm;->finish()V

    goto :goto_0

    .line 277
    :pswitch_3
    const/4 v1, 0x6

    invoke-virtual {p0, v1, v2}, Lcom/wf/wellsfargomobile/loc/LocationForm;->setResult(ILandroid/content/Intent;)V

    .line 278
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationForm;->finish()V

    goto :goto_0

    .line 264
    nop

    :pswitch_data_0
    .packed-switch 0x7f06007e
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
