.class Lcom/wf/wellsfargomobile/loc/LocationResults$7;
.super Ljava/lang/Object;
.source "LocationResults.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/wf/wellsfargomobile/loc/LocationResults;->findCurrLocation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;


# direct methods
.method constructor <init>(Lcom/wf/wellsfargomobile/loc/LocationResults;)V
    .locals 0
    .parameter

    .prologue
    .line 311
    iput-object p1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$7;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 314
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$7;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->gpsSearching:Ljava/lang/Boolean;
    invoke-static {v0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$300(Lcom/wf/wellsfargomobile/loc/LocationResults;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 315
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$7;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    #setter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->gpsSearching:Ljava/lang/Boolean;
    invoke-static {v0, v1}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$302(Lcom/wf/wellsfargomobile/loc/LocationResults;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 316
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$7;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->bestLoc:Landroid/location/Location;
    invoke-static {v0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$400(Lcom/wf/wellsfargomobile/loc/LocationResults;)Landroid/location/Location;

    move-result-object v0

    if-nez v0, :cond_1

    .line 317
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$7;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$7;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    const v2, 0x7f070026

    invoke-virtual {v1, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$7;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    const v3, 0x7f070025

    invoke-virtual {v2, v3}, Lcom/wf/wellsfargomobile/loc/LocationResults;->getString(I)Ljava/lang/String;

    move-result-object v2

    #calls: Lcom/wf/wellsfargomobile/loc/LocationResults;->returnWithError(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$500(Lcom/wf/wellsfargomobile/loc/LocationResults;Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    :cond_0
    :goto_0
    return-void

    .line 320
    :cond_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$7;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f07002a

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070029

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "OK"

    new-instance v2, Lcom/wf/wellsfargomobile/loc/LocationResults$7$1;

    invoke-direct {v2, p0}, Lcom/wf/wellsfargomobile/loc/LocationResults$7$1;-><init>(Lcom/wf/wellsfargomobile/loc/LocationResults$7;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 332
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$7;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #calls: Lcom/wf/wellsfargomobile/loc/LocationResults;->GPSSearch()V
    invoke-static {v0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$600(Lcom/wf/wellsfargomobile/loc/LocationResults;)V

    goto :goto_0
.end method
