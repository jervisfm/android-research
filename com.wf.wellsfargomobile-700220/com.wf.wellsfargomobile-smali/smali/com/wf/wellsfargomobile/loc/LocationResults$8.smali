.class Lcom/wf/wellsfargomobile/loc/LocationResults$8;
.super Ljava/lang/Object;
.source "LocationResults.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wf/wellsfargomobile/loc/LocationResults;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;


# direct methods
.method constructor <init>(Lcom/wf/wellsfargomobile/loc/LocationResults;)V
    .locals 0
    .parameter

    .prologue
    .line 558
    iput-object p1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$8;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .parameter "v"

    .prologue
    .line 561
    move-object v0, p1

    check-cast v0, Landroid/widget/Button;

    .line 562
    .local v0, clicked:Landroid/widget/Button;
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$8;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->resultBack:Landroid/widget/Button;
    invoke-static {v1}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$1400(Lcom/wf/wellsfargomobile/loc/LocationResults;)Landroid/widget/Button;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 563
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$8;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$8;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->resultsPerPage:I
    invoke-static {v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$1500(Lcom/wf/wellsfargomobile/loc/LocationResults;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$1120(Lcom/wf/wellsfargomobile/loc/LocationResults;I)I

    .line 564
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$8;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #calls: Lcom/wf/wellsfargomobile/loc/LocationResults;->updateResults()V
    invoke-static {v1}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$1200(Lcom/wf/wellsfargomobile/loc/LocationResults;)V

    .line 569
    :cond_0
    :goto_0
    return-void

    .line 565
    :cond_1
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$8;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->resultMore:Landroid/widget/Button;
    invoke-static {v1}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$1600(Lcom/wf/wellsfargomobile/loc/LocationResults;)Landroid/widget/Button;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 566
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$8;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$8;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->resultsPerPage:I
    invoke-static {v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$1500(Lcom/wf/wellsfargomobile/loc/LocationResults;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$1112(Lcom/wf/wellsfargomobile/loc/LocationResults;I)I

    .line 567
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$8;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #calls: Lcom/wf/wellsfargomobile/loc/LocationResults;->updateResults()V
    invoke-static {v1}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$1200(Lcom/wf/wellsfargomobile/loc/LocationResults;)V

    goto :goto_0
.end method
