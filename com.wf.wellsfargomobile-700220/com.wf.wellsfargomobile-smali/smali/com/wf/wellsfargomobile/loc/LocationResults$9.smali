.class Lcom/wf/wellsfargomobile/loc/LocationResults$9;
.super Ljava/lang/Object;
.source "LocationResults.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wf/wellsfargomobile/loc/LocationResults;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;


# direct methods
.method constructor <init>(Lcom/wf/wellsfargomobile/loc/LocationResults;)V
    .locals 0
    .parameter

    .prologue
    .line 572
    iput-object p1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$9;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .parameter "v"

    .prologue
    .line 575
    move-object v0, p1

    check-cast v0, Landroid/widget/LinearLayout;

    .line 576
    .local v0, clicked:Landroid/widget/LinearLayout;
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$9;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->resultsPerPage:I
    invoke-static {v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$1500(Lcom/wf/wellsfargomobile/loc/LocationResults;)I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 577
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$9;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->results:[Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$1700(Lcom/wf/wellsfargomobile/loc/LocationResults;)[Landroid/widget/LinearLayout;

    move-result-object v2

    aget-object v2, v2, v1

    if-ne v2, v0, :cond_0

    .line 578
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$9;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->results:[Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$1700(Lcom/wf/wellsfargomobile/loc/LocationResults;)[Landroid/widget/LinearLayout;

    move-result-object v2

    aget-object v2, v2, v1

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->requestFocusFromTouch()Z

    .line 579
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$9;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #calls: Lcom/wf/wellsfargomobile/loc/LocationResults;->launchDetails(I)V
    invoke-static {v2, v1}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$1800(Lcom/wf/wellsfargomobile/loc/LocationResults;I)V

    .line 576
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 582
    :cond_1
    return-void
.end method
