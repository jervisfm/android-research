.class public Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;
.super Lcom/google/android/maps/ItemizedOverlay;
.source "LocationResults.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wf/wellsfargomobile/loc/LocationResults;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LocationMapOverlay"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/maps/ItemizedOverlay",
        "<",
        "Lcom/google/android/maps/OverlayItem;",
        ">;"
    }
.end annotation


# instance fields
.field private addr:Ljava/lang/String;

.field private bankType:Ljava/lang/String;

.field private clicked:I

.field private final dp:F

.field private final font_height:F

.field private final iconWF:Landroid/graphics/Bitmap;

.field private final iconWach:Landroid/graphics/Bitmap;

.field private final markerWF:Landroid/graphics/Bitmap;

.field private final markerWach:Landroid/graphics/Bitmap;

.field private final overlays:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/maps/OverlayItem;",
            ">;"
        }
    .end annotation
.end field

.field private popup:Ljava/lang/Boolean;

.field private siteName:Ljava/lang/String;

.field private final sp:F

.field private final tail_height:F

.field private final tail_width:F

.field private final textPaint:Landroid/text/TextPaint;

.field final synthetic this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

.field private final windowBorder:Landroid/graphics/Paint;

.field private final windowInner:Landroid/graphics/Paint;

.field private final window_height:F

.field private final window_padding:F

.field private window_width:F


# direct methods
.method public constructor <init>(Lcom/wf/wellsfargomobile/loc/LocationResults;Landroid/graphics/drawable/Drawable;)V
    .locals 5
    .parameter
    .parameter "defaultMarker"

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 630
    iput-object p1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    .line 631
    invoke-static {p2}, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->boundCenterBottom(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/maps/ItemizedOverlay;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 619
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->overlays:Ljava/util/ArrayList;

    .line 623
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->popup:Ljava/lang/Boolean;

    .line 624
    iput v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->clicked:I

    .line 632
    invoke-virtual {p1}, Lcom/wf/wellsfargomobile/loc/LocationResults;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 633
    .local v0, res:Landroid/content/res/Resources;
    const v1, 0x7f020019

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->markerWF:Landroid/graphics/Bitmap;

    .line 634
    const v1, 0x7f020018

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->markerWach:Landroid/graphics/Bitmap;

    .line 635
    const v1, 0x7f02001e

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->iconWF:Landroid/graphics/Bitmap;

    .line 636
    const v1, 0x7f02001d

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->iconWach:Landroid/graphics/Bitmap;

    .line 638
    const v1, 0x7f050001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->sp:F

    .line 639
    const/high16 v1, 0x7f05

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->dp:F

    .line 640
    const/high16 v1, 0x4348

    iget v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->dp:F

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->window_width:F

    .line 641
    const/high16 v1, 0x4248

    iget v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->dp:F

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->window_height:F

    .line 642
    const/high16 v1, 0x40a0

    iget v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->dp:F

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->window_padding:F

    .line 643
    const/high16 v1, 0x4120

    iget v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->dp:F

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->tail_height:F

    .line 644
    const/high16 v1, 0x40c0

    iget v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->dp:F

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->tail_width:F

    .line 646
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    iput-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->textPaint:Landroid/text/TextPaint;

    .line 647
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v4}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 648
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v4}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 649
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->textPaint:Landroid/text/TextPaint;

    iget v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->sp:F

    const/high16 v3, 0x4140

    mul-float/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 650
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->textPaint:Landroid/text/TextPaint;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->getFontMetrics(Landroid/graphics/Paint$FontMetrics;)F

    move-result v1

    iput v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->font_height:F

    .line 652
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->windowInner:Landroid/graphics/Paint;

    .line 653
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->windowInner:Landroid/graphics/Paint;

    invoke-virtual {p1}, Lcom/wf/wellsfargomobile/loc/LocationResults;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f04

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 654
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->windowInner:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 655
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->windowInner:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 657
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->windowBorder:Landroid/graphics/Paint;

    .line 658
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->windowBorder:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 659
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->windowBorder:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 660
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->setBorderDark()V

    .line 661
    return-void
.end method

.method private getHitMapLocation(Lcom/google/android/maps/GeoPoint;Lcom/google/android/maps/MapView;)Ljava/lang/Boolean;
    .locals 13
    .parameter "tapPoint"
    .parameter "mapView"

    .prologue
    const/4 v12, 0x0

    const/high16 v11, 0x4316

    const/high16 v9, 0x4000

    const/4 v10, 0x1

    const/4 v8, 0x0

    .line 801
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    .line 802
    .local v3, screenCoords:Landroid/graphics/Point;
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 803
    .local v1, hitTestRecr:Landroid/graphics/RectF;
    iget-object v5, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->popup:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 804
    invoke-virtual {p2}, Lcom/google/android/maps/MapView;->getProjection()Lcom/google/android/maps/Projection;

    move-result-object v5

    iget v6, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->clicked:I

    invoke-virtual {p0, v6}, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->getItem(I)Lcom/google/android/maps/OverlayItem;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/maps/OverlayItem;->getPoint()Lcom/google/android/maps/GeoPoint;

    move-result-object v6

    invoke-interface {v5, v6, v3}, Lcom/google/android/maps/Projection;->toPixels(Lcom/google/android/maps/GeoPoint;Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 805
    iget v5, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->window_width:F

    iget v6, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->window_height:F

    invoke-virtual {v1, v8, v8, v5, v6}, Landroid/graphics/RectF;->set(FFFF)V

    .line 806
    iget v5, v3, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    iget v6, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->window_width:F

    div-float/2addr v6, v9

    sub-float/2addr v5, v6

    iget v6, v3, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    iget v7, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->window_height:F

    sub-float/2addr v6, v7

    iget-object v7, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->markerWF:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v7, v7

    sub-float/2addr v6, v7

    iget v7, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->tail_height:F

    sub-float/2addr v6, v7

    invoke-virtual {v1, v5, v6}, Landroid/graphics/RectF;->offset(FF)V

    .line 808
    invoke-virtual {p2}, Lcom/google/android/maps/MapView;->getProjection()Lcom/google/android/maps/Projection;

    move-result-object v5

    invoke-interface {v5, p1, v3}, Lcom/google/android/maps/Projection;->toPixels(Lcom/google/android/maps/GeoPoint;Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 809
    iget v5, v3, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    iget v6, v3, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    invoke-virtual {v1, v5, v6}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 810
    iget-object v5, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    iget v6, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->clicked:I

    #calls: Lcom/wf/wellsfargomobile/loc/LocationResults;->launchDetails(I)V
    invoke-static {v5, v6}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$1800(Lcom/wf/wellsfargomobile/loc/LocationResults;I)V

    .line 811
    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 848
    :goto_0
    return-object v5

    .line 814
    :cond_0
    const/4 v2, 0x0

    .local v2, i:I
    :goto_1
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->size()I

    move-result v5

    if-ge v2, v5, :cond_4

    .line 815
    invoke-virtual {p2}, Lcom/google/android/maps/MapView;->getProjection()Lcom/google/android/maps/Projection;

    move-result-object v5

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->getItem(I)Lcom/google/android/maps/OverlayItem;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/maps/OverlayItem;->getPoint()Lcom/google/android/maps/GeoPoint;

    move-result-object v6

    invoke-interface {v5, v6, v3}, Lcom/google/android/maps/Projection;->toPixels(Lcom/google/android/maps/GeoPoint;Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 816
    iget-object v5, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->markerWF:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    neg-int v5, v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    iget-object v6, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->markerWF:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    neg-int v6, v6

    int-to-float v6, v6

    iget-object v7, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->markerWF:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    invoke-virtual {v1, v5, v6, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 820
    iget v5, v3, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    iget v6, v3, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    invoke-virtual {v1, v5, v6}, Landroid/graphics/RectF;->offset(FF)V

    .line 822
    invoke-virtual {p2}, Lcom/google/android/maps/MapView;->getProjection()Lcom/google/android/maps/Projection;

    move-result-object v5

    invoke-interface {v5, p1, v3}, Lcom/google/android/maps/Projection;->toPixels(Lcom/google/android/maps/GeoPoint;Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 823
    iget v5, v3, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    iget v6, v3, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    invoke-virtual {v1, v5, v6}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 824
    iget-object v5, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->myBranches:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$800(Lcom/wf/wellsfargomobile/loc/LocationResults;)Ljava/util/ArrayList;

    move-result-object v5

    iget-object v6, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->currentBranch:I
    invoke-static {v6}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$1100(Lcom/wf/wellsfargomobile/loc/LocationResults;)I

    move-result v6

    add-int/2addr v6, v2

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 825
    .local v0, branch:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v5, "siteName"

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iput-object v5, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->siteName:Ljava/lang/String;

    .line 826
    const-string v5, "addrLine1"

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iput-object v5, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->addr:Ljava/lang/String;

    .line 827
    const-string v5, "bankType"

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iput-object v5, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->bankType:Ljava/lang/String;

    .line 829
    iget-object v5, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->textPaint:Landroid/text/TextPaint;

    iget-object v6, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->siteName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v4

    .line 830
    .local v4, textWidth:F
    iget-object v5, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->textPaint:Landroid/text/TextPaint;

    iget-object v6, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->addr:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v5

    cmpg-float v5, v4, v5

    if-gez v5, :cond_1

    .line 831
    iget-object v5, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->textPaint:Landroid/text/TextPaint;

    iget-object v6, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->addr:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v4

    .line 833
    :cond_1
    iget v5, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->dp:F

    mul-float/2addr v5, v11

    cmpg-float v5, v4, v5

    if-gez v5, :cond_2

    .line 834
    iget v5, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->dp:F

    mul-float v4, v11, v5

    .line 836
    :cond_2
    iget v5, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->window_padding:F

    mul-float/2addr v5, v9

    add-float/2addr v5, v4

    iget-object v6, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->iconWF:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    const/high16 v6, 0x4120

    iget v7, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->dp:F

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    iput v5, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->window_width:F

    .line 838
    iget-object v5, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->mapController:Lcom/google/android/maps/MapController;
    invoke-static {v5}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$000(Lcom/wf/wellsfargomobile/loc/LocationResults;)Lcom/google/android/maps/MapController;

    move-result-object v5

    invoke-virtual {p2}, Lcom/google/android/maps/MapView;->getProjection()Lcom/google/android/maps/Projection;

    move-result-object v6

    iget v7, v3, Landroid/graphics/Point;->x:I

    iget v8, v3, Landroid/graphics/Point;->y:I

    iget-object v9, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->markerWF:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    sub-int/2addr v8, v9

    invoke-interface {v6, v7, v8}, Lcom/google/android/maps/Projection;->fromPixels(II)Lcom/google/android/maps/GeoPoint;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/maps/MapController;->animateTo(Lcom/google/android/maps/GeoPoint;)V

    .line 842
    iput v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->clicked:I

    .line 843
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->popup:Ljava/lang/Boolean;

    .line 844
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    goto/16 :goto_0

    .line 814
    .end local v0           #branch:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4           #textWidth:F
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 847
    :cond_4
    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->popup:Ljava/lang/Boolean;

    .line 848
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    goto/16 :goto_0
.end method

.method private setBorderBackground()V
    .locals 3

    .prologue
    .line 685
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->windowBorder:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    invoke-virtual {v1}, Lcom/wf/wellsfargomobile/loc/LocationResults;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f04

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 686
    return-void
.end method

.method private setBorderDark()V
    .locals 3

    .prologue
    const/16 v2, 0x82

    .line 689
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->windowBorder:Landroid/graphics/Paint;

    const/16 v1, 0xff

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 690
    return-void
.end method

.method private setLinkColor()V
    .locals 3

    .prologue
    .line 681
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->textPaint:Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    invoke-virtual {v1}, Lcom/wf/wellsfargomobile/loc/LocationResults;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f040002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 682
    return-void
.end method

.method private setTextColor()V
    .locals 3

    .prologue
    const/16 v2, 0x1e

    .line 677
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->textPaint:Landroid/text/TextPaint;

    const/16 v1, 0xff

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/text/TextPaint;->setARGB(IIII)V

    .line 678
    return-void
.end method


# virtual methods
.method public addOverlay(Lcom/google/android/maps/OverlayItem;)V
    .locals 1
    .parameter "overlay"

    .prologue
    .line 664
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->overlays:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 665
    return-void
.end method

.method public clearOverlays()V
    .locals 1

    .prologue
    .line 668
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->popup:Ljava/lang/Boolean;

    .line 669
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->overlays:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 670
    return-void
.end method

.method protected createItem(I)Lcom/google/android/maps/OverlayItem;
    .locals 1
    .parameter "i"

    .prologue
    .line 784
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->overlays:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/OverlayItem;

    return-object v0
.end method

.method public draw(Landroid/graphics/Canvas;Lcom/google/android/maps/MapView;Z)V
    .locals 22
    .parameter "canvas"
    .parameter "mapView"
    .parameter "shadow"

    .prologue
    .line 694
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/maps/MapView;->getProjection()Lcom/google/android/maps/Projection;

    move-result-object v19

    .line 695
    .local v19, projection:Lcom/google/android/maps/Projection;
    invoke-virtual/range {p0 .. p0}, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->size()I

    move-result v2

    add-int/lit8 v11, v2, -0x1

    .local v11, i:I
    :goto_0
    if-ltz v11, :cond_2

    .line 696
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->getItem(I)Lcom/google/android/maps/OverlayItem;

    move-result-object v13

    .line 697
    .local v13, item:Lcom/google/android/maps/OverlayItem;
    invoke-virtual {v13}, Lcom/google/android/maps/OverlayItem;->getTitle()Ljava/lang/String;

    move-result-object v21

    .line 698
    .local v21, title:Ljava/lang/String;
    invoke-virtual {v13}, Lcom/google/android/maps/OverlayItem;->getSnippet()Ljava/lang/String;

    move-result-object v20

    .line 699
    .local v20, snippet:Ljava/lang/String;
    invoke-virtual {v13}, Lcom/google/android/maps/OverlayItem;->getPoint()Lcom/google/android/maps/GeoPoint;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, v19

    invoke-interface {v0, v2, v3}, Lcom/google/android/maps/Projection;->toPixels(Lcom/google/android/maps/GeoPoint;Landroid/graphics/Point;)Landroid/graphics/Point;

    move-result-object v18

    .line 701
    .local v18, point:Landroid/graphics/Point;
    const-string v2, "Wells Fargo"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 702
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->markerWF:Landroid/graphics/Bitmap;

    move-object/from16 v0, v18

    iget v3, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->markerWF:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    int-to-float v3, v3

    move-object/from16 v0, v18

    iget v4, v0, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->markerWF:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    sub-int/2addr v4, v7

    int-to-float v4, v4

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 704
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->textPaint:Landroid/text/TextPaint;

    const/16 v3, 0xff

    const/16 v4, 0x1e

    const/16 v7, 0x1e

    const/16 v8, 0x1e

    invoke-virtual {v2, v3, v4, v7, v8}, Landroid/text/TextPaint;->setARGB(IIII)V

    .line 710
    :cond_0
    :goto_1
    move-object/from16 v0, v18

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v3

    mul-int/lit8 v3, v3, 0x3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->dp:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->dp:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    move-object/from16 v0, v18

    iget v3, v0, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    const/high16 v4, 0x41a0

    move-object/from16 v0, p0

    iget v7, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->dp:F

    mul-float/2addr v4, v7

    sub-float/2addr v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->textPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 695
    add-int/lit8 v11, v11, -0x1

    goto/16 :goto_0

    .line 705
    :cond_1
    const-string v2, "Wachovia"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 706
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->markerWach:Landroid/graphics/Bitmap;

    move-object/from16 v0, v18

    iget v3, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->markerWach:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    int-to-float v3, v3

    move-object/from16 v0, v18

    iget v4, v0, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->markerWach:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    sub-int/2addr v4, v7

    int-to-float v4, v4

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 708
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->textPaint:Landroid/text/TextPaint;

    const/16 v3, 0xff

    const/16 v4, 0xc8

    const/16 v7, 0xc8

    const/16 v8, 0xc8

    invoke-virtual {v2, v3, v4, v7, v8}, Landroid/text/TextPaint;->setARGB(IIII)V

    goto :goto_1

    .line 714
    .end local v13           #item:Lcom/google/android/maps/OverlayItem;
    .end local v18           #point:Landroid/graphics/Point;
    .end local v20           #snippet:Ljava/lang/String;
    .end local v21           #title:Ljava/lang/String;
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->popup:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 715
    move-object/from16 v0, p0

    iget v2, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->clicked:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->getItem(I)Lcom/google/android/maps/OverlayItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/OverlayItem;->getPoint()Lcom/google/android/maps/GeoPoint;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, v19

    invoke-interface {v0, v2, v3}, Lcom/google/android/maps/Projection;->toPixels(Lcom/google/android/maps/GeoPoint;Landroid/graphics/Point;)Landroid/graphics/Point;

    move-result-object v18

    .line 716
    .restart local v18       #point:Landroid/graphics/Point;
    new-instance v12, Landroid/graphics/RectF;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->window_width:F

    move-object/from16 v0, p0

    iget v7, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->window_height:F

    invoke-direct {v12, v2, v3, v4, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 718
    .local v12, infoWindowRect:Landroid/graphics/RectF;
    move-object/from16 v0, v18

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->window_width:F

    const/high16 v4, 0x4000

    div-float/2addr v3, v4

    sub-float v15, v2, v3

    .line 719
    .local v15, offx:F
    move-object/from16 v0, v18

    iget v2, v0, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->window_height:F

    sub-float/2addr v2, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->markerWF:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->tail_height:F

    sub-float v16, v2, v3

    .line 720
    .local v16, offy:F
    move/from16 v0, v16

    invoke-virtual {v12, v15, v0}, Landroid/graphics/RectF;->offset(FF)V

    .line 721
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->windowBorder:Landroid/graphics/Paint;

    const/high16 v3, 0x3f80

    move-object/from16 v0, p0

    iget v4, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->dp:F

    mul-float/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 722
    const/high16 v2, 0x40a0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->dp:F

    mul-float/2addr v2, v3

    const/high16 v3, 0x40a0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->dp:F

    mul-float/2addr v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->windowInner:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v2, v3, v4}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 723
    const/high16 v2, 0x40a0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->dp:F

    mul-float/2addr v2, v3

    const/high16 v3, 0x40a0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->dp:F

    mul-float/2addr v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->windowBorder:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v2, v3, v4}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 725
    invoke-direct/range {p0 .. p0}, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->setTextColor()V

    .line 726
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->siteName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->window_padding:F

    add-float/2addr v3, v15

    move-object/from16 v0, p0

    iget v4, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->font_height:F

    add-float v4, v4, v16

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->textPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 727
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->textPaint:Landroid/text/TextPaint;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 728
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->addr:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->window_padding:F

    add-float/2addr v3, v15

    move-object/from16 v0, p0

    iget v4, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->font_height:F

    const/high16 v7, 0x4000

    mul-float/2addr v4, v7

    add-float v4, v4, v16

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->textPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 729
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->textPaint:Landroid/text/TextPaint;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 730
    invoke-direct/range {p0 .. p0}, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->setLinkColor()V

    .line 731
    const-string v2, "Click for more details"

    move-object/from16 v0, p0

    iget v3, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->window_padding:F

    add-float/2addr v3, v15

    move-object/from16 v0, p0

    iget v4, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->font_height:F

    const/high16 v7, 0x4040

    mul-float/2addr v4, v7

    add-float v4, v4, v16

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->textPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 733
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->bankType:Ljava/lang/String;

    const-string v3, "Wells Fargo"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 734
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->iconWF:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->window_width:F

    add-float/2addr v3, v15

    move-object/from16 v0, p0

    iget v4, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->window_padding:F

    sub-float/2addr v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->iconWF:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->window_padding:F

    add-float v4, v4, v16

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 745
    :goto_2
    move-object/from16 v0, v18

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v5, v2

    .line 746
    .local v5, pathx:F
    move-object/from16 v0, v18

    iget v2, v0, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->markerWF:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    const/high16 v3, 0x3f80

    move-object/from16 v0, p0

    iget v4, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->dp:F

    mul-float/2addr v3, v4

    sub-float v6, v2, v3

    .line 747
    .local v6, pathy:F
    new-instance v17, Landroid/graphics/Path;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Path;-><init>()V

    .line 748
    .local v17, path:Landroid/graphics/Path;
    move-object/from16 v0, v17

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 749
    move-object/from16 v0, p0

    iget v2, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->tail_width:F

    const/high16 v3, 0x4000

    div-float/2addr v2, v3

    add-float/2addr v2, v5

    move-object/from16 v0, p0

    iget v3, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->tail_height:F

    sub-float v3, v6, v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 750
    move-object/from16 v0, p0

    iget v2, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->tail_width:F

    const/high16 v3, 0x4000

    div-float/2addr v2, v3

    sub-float v2, v5, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->tail_height:F

    sub-float v3, v6, v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 751
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->windowInner:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 752
    move-object/from16 v0, p0

    iget v2, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->tail_width:F

    const/high16 v3, 0x4000

    div-float/2addr v2, v3

    sub-float v3, v5, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->tail_height:F

    sub-float v4, v6, v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->windowBorder:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 757
    move-object/from16 v0, p0

    iget v2, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->tail_width:F

    const/high16 v3, 0x4000

    div-float/2addr v2, v3

    add-float v7, v5, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->tail_height:F

    sub-float v8, v6, v2

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->windowBorder:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 764
    .end local v5           #pathx:F
    .end local v6           #pathy:F
    .end local v12           #infoWindowRect:Landroid/graphics/RectF;
    .end local v15           #offx:F
    .end local v16           #offy:F
    .end local v17           #path:Landroid/graphics/Path;
    .end local v18           #point:Landroid/graphics/Point;
    :cond_3
    new-instance v14, Landroid/graphics/RectF;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->resultMap:Lcom/google/android/maps/MapView;
    invoke-static {v4}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$2000(Lcom/wf/wellsfargomobile/loc/LocationResults;)Lcom/google/android/maps/MapView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/MapView;->getWidth()I

    move-result v4

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->resultMap:Lcom/google/android/maps/MapView;
    invoke-static {v7}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$2000(Lcom/wf/wellsfargomobile/loc/LocationResults;)Lcom/google/android/maps/MapView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/maps/MapView;->getHeight()I

    move-result v7

    int-to-float v7, v7

    invoke-direct {v14, v2, v3, v4, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 765
    .local v14, mapBorder:Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->windowBorder:Landroid/graphics/Paint;

    const/high16 v3, 0x4000

    move-object/from16 v0, p0

    iget v4, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->dp:F

    mul-float/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 766
    invoke-direct/range {p0 .. p0}, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->setBorderBackground()V

    .line 767
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->windowBorder:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 768
    invoke-direct/range {p0 .. p0}, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->setBorderDark()V

    .line 769
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->windowBorder:Landroid/graphics/Paint;

    const/high16 v3, 0x3f80

    move-object/from16 v0, p0

    iget v4, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->dp:F

    mul-float/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 770
    const/4 v11, 0x0

    :goto_3
    const/4 v2, 0x5

    if-ge v11, v2, :cond_5

    .line 771
    new-instance v14, Landroid/graphics/RectF;

    .end local v14           #mapBorder:Landroid/graphics/RectF;
    int-to-float v2, v11

    move-object/from16 v0, p0

    iget v3, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->dp:F

    mul-float/2addr v2, v3

    int-to-float v3, v11

    move-object/from16 v0, p0

    iget v4, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->dp:F

    mul-float/2addr v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->resultMap:Lcom/google/android/maps/MapView;
    invoke-static {v4}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$2000(Lcom/wf/wellsfargomobile/loc/LocationResults;)Lcom/google/android/maps/MapView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/MapView;->getWidth()I

    move-result v4

    int-to-float v4, v4

    int-to-float v7, v11

    move-object/from16 v0, p0

    iget v8, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->dp:F

    mul-float/2addr v7, v8

    sub-float/2addr v4, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->resultMap:Lcom/google/android/maps/MapView;
    invoke-static {v7}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$2000(Lcom/wf/wellsfargomobile/loc/LocationResults;)Lcom/google/android/maps/MapView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/maps/MapView;->getHeight()I

    move-result v7

    int-to-float v7, v7

    int-to-float v8, v11

    move-object/from16 v0, p0

    iget v9, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->dp:F

    mul-float/2addr v8, v9

    sub-float/2addr v7, v8

    invoke-direct {v14, v2, v3, v4, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 775
    .restart local v14       #mapBorder:Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->windowBorder:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getAlpha()I

    move-result v10

    .line 776
    .local v10, a:I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->windowBorder:Landroid/graphics/Paint;

    add-int/lit8 v3, v11, 0x1

    div-int v3, v10, v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 777
    const/high16 v2, 0x40e0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->dp:F

    mul-float/2addr v2, v3

    const/high16 v3, 0x40e0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->dp:F

    mul-float/2addr v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->windowBorder:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v2, v3, v4}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 770
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 739
    .end local v10           #a:I
    .end local v14           #mapBorder:Landroid/graphics/RectF;
    .restart local v12       #infoWindowRect:Landroid/graphics/RectF;
    .restart local v15       #offx:F
    .restart local v16       #offy:F
    .restart local v18       #point:Landroid/graphics/Point;
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->iconWach:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->window_width:F

    add-float/2addr v3, v15

    move-object/from16 v0, p0

    iget v4, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->window_padding:F

    sub-float/2addr v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->iconWF:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->window_padding:F

    add-float v4, v4, v16

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 779
    .end local v12           #infoWindowRect:Landroid/graphics/RectF;
    .end local v15           #offx:F
    .end local v16           #offy:F
    .end local v18           #point:Landroid/graphics/Point;
    .restart local v14       #mapBorder:Landroid/graphics/RectF;
    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->setBorderDark()V

    .line 780
    return-void
.end method

.method public onTap(Lcom/google/android/maps/GeoPoint;Lcom/google/android/maps/MapView;)Z
    .locals 1
    .parameter "p"
    .parameter "mapView"

    .prologue
    .line 794
    invoke-direct {p0, p1, p2}, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->getHitMapLocation(Lcom/google/android/maps/GeoPoint;Lcom/google/android/maps/MapView;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 795
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->resultMap:Lcom/google/android/maps/MapView;
    invoke-static {v0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$2000(Lcom/wf/wellsfargomobile/loc/LocationResults;)Lcom/google/android/maps/MapView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/MapView;->invalidate()V

    .line 797
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public populateOverlays()V
    .locals 0

    .prologue
    .line 673
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->populate()V

    .line 674
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 789
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->overlays:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
