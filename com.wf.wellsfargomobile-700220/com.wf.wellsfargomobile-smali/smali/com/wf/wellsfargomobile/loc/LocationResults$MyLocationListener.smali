.class Lcom/wf/wellsfargomobile/loc/LocationResults$MyLocationListener;
.super Ljava/lang/Object;
.source "LocationResults.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wf/wellsfargomobile/loc/LocationResults;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyLocationListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;


# direct methods
.method private constructor <init>(Lcom/wf/wellsfargomobile/loc/LocationResults;)V
    .locals 0
    .parameter

    .prologue
    .line 592
    iput-object p1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$MyLocationListener;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/wf/wellsfargomobile/loc/LocationResults;Lcom/wf/wellsfargomobile/loc/LocationResults$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 592
    invoke-direct {p0, p1}, Lcom/wf/wellsfargomobile/loc/LocationResults$MyLocationListener;-><init>(Lcom/wf/wellsfargomobile/loc/LocationResults;)V

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2
    .parameter "loc"

    .prologue
    .line 595
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$MyLocationListener;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->bestLoc:Landroid/location/Location;
    invoke-static {v0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$400(Lcom/wf/wellsfargomobile/loc/LocationResults;)Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$MyLocationListener;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->bestLoc:Landroid/location/Location;
    invoke-static {v1}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$400(Lcom/wf/wellsfargomobile/loc/LocationResults;)Landroid/location/Location;

    move-result-object v1

    invoke-virtual {v1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 596
    :cond_0
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$MyLocationListener;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #setter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->bestLoc:Landroid/location/Location;
    invoke-static {v0, p1}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$402(Lcom/wf/wellsfargomobile/loc/LocationResults;Landroid/location/Location;)Landroid/location/Location;

    .line 599
    :cond_1
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$MyLocationListener;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->bestLoc:Landroid/location/Location;
    invoke-static {v0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$400(Lcom/wf/wellsfargomobile/loc/LocationResults;)Landroid/location/Location;

    move-result-object v0

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$MyLocationListener;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->desiredAccuracy:I
    invoke-static {v1}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$1900(Lcom/wf/wellsfargomobile/loc/LocationResults;)I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_2

    .line 600
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$MyLocationListener;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    #setter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->gpsSearching:Ljava/lang/Boolean;
    invoke-static {v0, v1}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$302(Lcom/wf/wellsfargomobile/loc/LocationResults;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 601
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$MyLocationListener;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #calls: Lcom/wf/wellsfargomobile/loc/LocationResults;->GPSSearch()V
    invoke-static {v0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$600(Lcom/wf/wellsfargomobile/loc/LocationResults;)V

    .line 603
    :cond_2
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0
    .parameter "provider"

    .prologue
    .line 607
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .parameter "provider"

    .prologue
    .line 611
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .parameter "provider"
    .parameter "status"
    .parameter "extras"

    .prologue
    .line 615
    return-void
.end method
