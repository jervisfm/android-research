.class Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;
.super Landroid/os/AsyncTask;
.source "LocationResults.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wf/wellsfargomobile/loc/LocationResults;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "XMLParseTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;


# direct methods
.method private constructor <init>(Lcom/wf/wellsfargomobile/loc/LocationResults;)V
    .locals 0
    .parameter

    .prologue
    .line 496
    iput-object p1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/wf/wellsfargomobile/loc/LocationResults;Lcom/wf/wellsfargomobile/loc/LocationResults$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 496
    invoke-direct {p0, p1}, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;-><init>(Lcom/wf/wellsfargomobile/loc/LocationResults;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 496
    check-cast p1, [Ljava/lang/String;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Object;
    .locals 10
    .parameter "urls"

    .prologue
    .line 500
    :try_start_0
    new-instance v4, Ljava/net/URL;

    const/4 v6, 0x0

    aget-object v6, p1, v6

    invoke-direct {v4, v6}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 502
    .local v4, url:Ljava/net/URL;
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v3

    .line 504
    .local v3, spf:Ljavax/xml/parsers/SAXParserFactory;
    invoke-virtual {v3}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v2

    .line 506
    .local v2, sp:Ljavax/xml/parsers/SAXParser;
    invoke-virtual {v2}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v5

    .line 507
    .local v5, xr:Lorg/xml/sax/XMLReader;
    new-instance v1, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;

    invoke-direct {v1}, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;-><init>()V

    .line 508
    .local v1, locHandler:Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;
    invoke-interface {v5, v1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 510
    new-instance v6, Lorg/xml/sax/InputSource;

    invoke-virtual {v4}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v7

    invoke-direct {v6, v7}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/InputStream;)V

    invoke-interface {v5, v6}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 511
    iget-object v6, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    invoke-virtual {v1}, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;->getParsedLocations()Ljava/util/ArrayList;

    move-result-object v7

    #setter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->myBranches:Ljava/util/ArrayList;
    invoke-static {v6, v7}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$802(Lcom/wf/wellsfargomobile/loc/LocationResults;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 517
    .end local v1           #locHandler:Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;
    .end local v2           #sp:Ljavax/xml/parsers/SAXParser;
    .end local v3           #spf:Ljavax/xml/parsers/SAXParserFactory;
    .end local v4           #url:Ljava/net/URL;
    .end local v5           #xr:Lorg/xml/sax/XMLReader;
    :goto_0
    const/4 v6, 0x0

    return-object v6

    .line 512
    :catch_0
    move-exception v0

    .line 513
    .local v0, e:Ljava/lang/Exception;
    const-string v6, "LocationResults"

    const-string v7, "Parsing Error: "

    invoke-static {v6, v7, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 514
    iget-object v6, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    iget-object v7, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    const v8, 0x7f07003d

    invoke-virtual {v7, v8}, Lcom/wf/wellsfargomobile/loc/LocationResults;->getString(I)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    const v9, 0x7f07003b

    invoke-virtual {v8, v9}, Lcom/wf/wellsfargomobile/loc/LocationResults;->getString(I)Ljava/lang/String;

    move-result-object v8

    #calls: Lcom/wf/wellsfargomobile/loc/LocationResults;->returnWithError(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v6, v7, v8}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$500(Lcom/wf/wellsfargomobile/loc/LocationResults;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 8
    .parameter "result"

    .prologue
    const v3, 0x7f07003d

    const-wide v6, 0x412e848000000000L

    const/4 v5, 0x0

    .line 522
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->myBranches:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$800(Lcom/wf/wellsfargomobile/loc/LocationResults;)Ljava/util/ArrayList;

    move-result-object v1

    if-nez v1, :cond_0

    .line 523
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    invoke-virtual {v2, v3}, Lcom/wf/wellsfargomobile/loc/LocationResults;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    const v4, 0x7f07003b

    invoke-virtual {v3, v4}, Lcom/wf/wellsfargomobile/loc/LocationResults;->getString(I)Ljava/lang/String;

    move-result-object v3

    #calls: Lcom/wf/wellsfargomobile/loc/LocationResults;->returnWithError(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v1, v2, v3}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$500(Lcom/wf/wellsfargomobile/loc/LocationResults;Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    :goto_0
    return-void

    .line 525
    :cond_0
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->myBranches:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$800(Lcom/wf/wellsfargomobile/loc/LocationResults;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 526
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    invoke-virtual {v2, v3}, Lcom/wf/wellsfargomobile/loc/LocationResults;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    const v4, 0x7f07003c

    invoke-virtual {v3, v4}, Lcom/wf/wellsfargomobile/loc/LocationResults;->getString(I)Ljava/lang/String;

    move-result-object v3

    #calls: Lcom/wf/wellsfargomobile/loc/LocationResults;->returnWithError(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v1, v2, v3}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$500(Lcom/wf/wellsfargomobile/loc/LocationResults;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 528
    :cond_1
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->myBranches:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$800(Lcom/wf/wellsfargomobile/loc/LocationResults;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    const-string v2, "error"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 529
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->myBranches:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$800(Lcom/wf/wellsfargomobile/loc/LocationResults;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    const-string v4, "error"

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    #calls: Lcom/wf/wellsfargomobile/loc/LocationResults;->returnWithError(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v2, v3, v1}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$500(Lcom/wf/wellsfargomobile/loc/LocationResults;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 531
    :cond_2
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->myBranches:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$800(Lcom/wf/wellsfargomobile/loc/LocationResults;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    const-string v2, "id"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, "-1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 532
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->myBranches:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$800(Lcom/wf/wellsfargomobile/loc/LocationResults;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 533
    .local v0, searchCoords:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v3

    #setter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->myLat:D
    invoke-static {v2, v3, v4}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$902(Lcom/wf/wellsfargomobile/loc/LocationResults;D)D

    .line 534
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    const-string v1, "longitude"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v3

    #setter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->myLong:D
    invoke-static {v2, v3, v4}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$1002(Lcom/wf/wellsfargomobile/loc/LocationResults;D)D

    .line 536
    .end local v0           #searchCoords:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_3
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #setter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->currentBranch:I
    invoke-static {v1, v5}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$1102(Lcom/wf/wellsfargomobile/loc/LocationResults;I)I

    .line 537
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->mapController:Lcom/google/android/maps/MapController;
    invoke-static {v1}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$000(Lcom/wf/wellsfargomobile/loc/LocationResults;)Lcom/google/android/maps/MapController;

    move-result-object v1

    new-instance v2, Lcom/google/android/maps/GeoPoint;

    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->myLat:D
    invoke-static {v3}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$900(Lcom/wf/wellsfargomobile/loc/LocationResults;)D

    move-result-wide v3

    mul-double/2addr v3, v6

    double-to-int v3, v3

    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->myLong:D
    invoke-static {v4}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$1000(Lcom/wf/wellsfargomobile/loc/LocationResults;)D

    move-result-wide v4

    mul-double/2addr v4, v6

    double-to-int v4, v4

    invoke-direct {v2, v3, v4}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    invoke-virtual {v1, v2}, Lcom/google/android/maps/MapController;->setCenter(Lcom/google/android/maps/GeoPoint;)V

    .line 539
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->mapController:Lcom/google/android/maps/MapController;
    invoke-static {v1}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$000(Lcom/wf/wellsfargomobile/loc/LocationResults;)Lcom/google/android/maps/MapController;

    move-result-object v1

    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->currentZoom:I
    invoke-static {v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$100(Lcom/wf/wellsfargomobile/loc/LocationResults;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/maps/MapController;->setZoom(I)I

    .line 540
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #calls: Lcom/wf/wellsfargomobile/loc/LocationResults;->updateResults()V
    invoke-static {v1}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$1200(Lcom/wf/wellsfargomobile/loc/LocationResults;)V

    .line 541
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->this$0:Lcom/wf/wellsfargomobile/loc/LocationResults;

    #getter for: Lcom/wf/wellsfargomobile/loc/LocationResults;->locCurtain:Landroid/view/View;
    invoke-static {v1}, Lcom/wf/wellsfargomobile/loc/LocationResults;->access$1300(Lcom/wf/wellsfargomobile/loc/LocationResults;)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0
.end method
