.class public Lcom/wf/wellsfargomobile/loc/LocationResults;
.super Lcom/wf/wellsfargomobile/loc/BaseMapActivity;
.source "LocationResults.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;,
        Lcom/wf/wellsfargomobile/loc/LocationResults$MyLocationListener;,
        Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;
    }
.end annotation


# static fields
.field public static final ACTIVITY_DETAILS:I = 0x0

.field public static final KEY_BRANCH:Ljava/lang/String; = "branch_object"

.field public static final KEY_ERRORMESSAGE:Ljava/lang/String; = "error_msg"

.field public static final KEY_ERRORTITLE:Ljava/lang/String; = "error_title"

.field private static final TAG:Ljava/lang/String; = "LocationResults"


# instance fields
.field final ENCODING:Ljava/lang/String;

.field private bestLoc:Landroid/location/Location;

.field private final buttonHandler:Landroid/view/View$OnClickListener;

.field private currentBranch:I

.field private currentZoom:I

.field private desiredAccuracy:I

.field private gpsSearching:Ljava/lang/Boolean;

.field private gpsTimeout:Landroid/os/Handler;

.field private inSession:Ljava/lang/Boolean;

.field private loadStatus:Landroid/widget/TextView;

.field private locCurtain:Landroid/view/View;

.field private mapController:Lcom/google/android/maps/MapController;

.field private mapOverlays:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/maps/Overlay;",
            ">;"
        }
    .end annotation
.end field

.field private myBranches:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private myLL:Landroid/location/LocationListener;

.field private myLM:Landroid/location/LocationManager;

.field private myLat:D

.field private myLong:D

.field private myScroller:Landroid/widget/ScrollView;

.field private overlays:Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;

.field private resultBack:Landroid/widget/Button;

.field private resultContent:[Landroid/widget/TextView;

.field private resultLabels:[Landroid/widget/TextView;

.field private resultMap:Lcom/google/android/maps/MapView;

.field private resultMore:Landroid/widget/Button;

.field private resultNumbers:Landroid/widget/TextView;

.field private results:[Landroid/widget/LinearLayout;

.field private final resultsHandler:Landroid/view/View$OnClickListener;

.field private resultsPerPage:I

.field private searchAddress:Landroid/widget/TextView;

.field private wasGPS:Ljava/lang/Boolean;

.field private wfApp:Lcom/wf/wellsfargomobile/WFApp;

.field private zoomControls:Landroid/widget/ZoomControls;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/loc/BaseMapActivity;-><init>()V

    .line 93
    const/4 v0, 0x0

    iput v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentBranch:I

    .line 94
    const/16 v0, 0xd

    iput v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentZoom:I

    .line 96
    const/4 v0, 0x5

    iput v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultsPerPage:I

    .line 98
    const-string v0, "UTF-8"

    iput-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->ENCODING:Ljava/lang/String;

    .line 558
    new-instance v0, Lcom/wf/wellsfargomobile/loc/LocationResults$8;

    invoke-direct {v0, p0}, Lcom/wf/wellsfargomobile/loc/LocationResults$8;-><init>(Lcom/wf/wellsfargomobile/loc/LocationResults;)V

    iput-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->buttonHandler:Landroid/view/View$OnClickListener;

    .line 572
    new-instance v0, Lcom/wf/wellsfargomobile/loc/LocationResults$9;

    invoke-direct {v0, p0}, Lcom/wf/wellsfargomobile/loc/LocationResults$9;-><init>(Lcom/wf/wellsfargomobile/loc/LocationResults;)V

    iput-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultsHandler:Landroid/view/View$OnClickListener;

    .line 618
    return-void
.end method

.method private GPSSearch()V
    .locals 4

    .prologue
    .line 392
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myLM:Landroid/location/LocationManager;

    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myLL:Landroid/location/LocationListener;

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 393
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->bestLoc:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    iput-wide v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myLat:D

    .line 394
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->bestLoc:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v1

    iput-wide v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myLong:D

    .line 396
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->loadStatus:Landroid/widget/TextView;

    const v2, 0x7f070037

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 398
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f070001

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/locator/searchForLocationsByGeocode.do?lg="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myLong:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&lt="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myLat:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&WFAppId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f070003

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&format=xml"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 401
    .local v0, gpsUrl:Ljava/lang/String;
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->searchAddress:Landroid/widget/TextView;

    const-string v2, "For Address: Current Location"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 402
    new-instance v1, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;-><init>(Lcom/wf/wellsfargomobile/loc/LocationResults;Lcom/wf/wellsfargomobile/loc/LocationResults$1;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 403
    return-void
.end method

.method static synthetic access$000(Lcom/wf/wellsfargomobile/loc/LocationResults;)Lcom/google/android/maps/MapController;
    .locals 1
    .parameter "x0"

    .prologue
    .line 64
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->mapController:Lcom/google/android/maps/MapController;

    return-object v0
.end method

.method static synthetic access$100(Lcom/wf/wellsfargomobile/loc/LocationResults;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 64
    iget v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentZoom:I

    return v0
.end method

.method static synthetic access$1000(Lcom/wf/wellsfargomobile/loc/LocationResults;)D
    .locals 2
    .parameter "x0"

    .prologue
    .line 64
    iget-wide v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myLong:D

    return-wide v0
.end method

.method static synthetic access$1002(Lcom/wf/wellsfargomobile/loc/LocationResults;D)D
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 64
    iput-wide p1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myLong:D

    return-wide p1
.end method

.method static synthetic access$108(Lcom/wf/wellsfargomobile/loc/LocationResults;)I
    .locals 2
    .parameter "x0"

    .prologue
    .line 64
    iget v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentZoom:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentZoom:I

    return v0
.end method

.method static synthetic access$110(Lcom/wf/wellsfargomobile/loc/LocationResults;)I
    .locals 2
    .parameter "x0"

    .prologue
    .line 64
    iget v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentZoom:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentZoom:I

    return v0
.end method

.method static synthetic access$1100(Lcom/wf/wellsfargomobile/loc/LocationResults;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 64
    iget v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentBranch:I

    return v0
.end method

.method static synthetic access$1102(Lcom/wf/wellsfargomobile/loc/LocationResults;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 64
    iput p1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentBranch:I

    return p1
.end method

.method static synthetic access$1112(Lcom/wf/wellsfargomobile/loc/LocationResults;I)I
    .locals 1
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 64
    iget v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentBranch:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentBranch:I

    return v0
.end method

.method static synthetic access$1120(Lcom/wf/wellsfargomobile/loc/LocationResults;I)I
    .locals 1
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 64
    iget v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentBranch:I

    sub-int/2addr v0, p1

    iput v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentBranch:I

    return v0
.end method

.method static synthetic access$1200(Lcom/wf/wellsfargomobile/loc/LocationResults;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->updateResults()V

    return-void
.end method

.method static synthetic access$1300(Lcom/wf/wellsfargomobile/loc/LocationResults;)Landroid/view/View;
    .locals 1
    .parameter "x0"

    .prologue
    .line 64
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->locCurtain:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/wf/wellsfargomobile/loc/LocationResults;)Landroid/widget/Button;
    .locals 1
    .parameter "x0"

    .prologue
    .line 64
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultBack:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/wf/wellsfargomobile/loc/LocationResults;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 64
    iget v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultsPerPage:I

    return v0
.end method

.method static synthetic access$1600(Lcom/wf/wellsfargomobile/loc/LocationResults;)Landroid/widget/Button;
    .locals 1
    .parameter "x0"

    .prologue
    .line 64
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultMore:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/wf/wellsfargomobile/loc/LocationResults;)[Landroid/widget/LinearLayout;
    .locals 1
    .parameter "x0"

    .prologue
    .line 64
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->results:[Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/wf/wellsfargomobile/loc/LocationResults;I)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/wf/wellsfargomobile/loc/LocationResults;->launchDetails(I)V

    return-void
.end method

.method static synthetic access$1900(Lcom/wf/wellsfargomobile/loc/LocationResults;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 64
    iget v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->desiredAccuracy:I

    return v0
.end method

.method static synthetic access$2000(Lcom/wf/wellsfargomobile/loc/LocationResults;)Lcom/google/android/maps/MapView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 64
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultMap:Lcom/google/android/maps/MapView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/wf/wellsfargomobile/loc/LocationResults;)Ljava/lang/Boolean;
    .locals 1
    .parameter "x0"

    .prologue
    .line 64
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->gpsSearching:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$302(Lcom/wf/wellsfargomobile/loc/LocationResults;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 64
    iput-object p1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->gpsSearching:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$400(Lcom/wf/wellsfargomobile/loc/LocationResults;)Landroid/location/Location;
    .locals 1
    .parameter "x0"

    .prologue
    .line 64
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->bestLoc:Landroid/location/Location;

    return-object v0
.end method

.method static synthetic access$402(Lcom/wf/wellsfargomobile/loc/LocationResults;Landroid/location/Location;)Landroid/location/Location;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 64
    iput-object p1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->bestLoc:Landroid/location/Location;

    return-object p1
.end method

.method static synthetic access$500(Lcom/wf/wellsfargomobile/loc/LocationResults;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->returnWithError(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/wf/wellsfargomobile/loc/LocationResults;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->GPSSearch()V

    return-void
.end method

.method static synthetic access$800(Lcom/wf/wellsfargomobile/loc/LocationResults;)Ljava/util/ArrayList;
    .locals 1
    .parameter "x0"

    .prologue
    .line 64
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myBranches:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$802(Lcom/wf/wellsfargomobile/loc/LocationResults;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 64
    iput-object p1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myBranches:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$900(Lcom/wf/wellsfargomobile/loc/LocationResults;)D
    .locals 2
    .parameter "x0"

    .prologue
    .line 64
    iget-wide v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myLat:D

    return-wide v0
.end method

.method static synthetic access$902(Lcom/wf/wellsfargomobile/loc/LocationResults;D)D
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 64
    iput-wide p1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myLat:D

    return-wide p1
.end method

.method private checkLocationProviders()V
    .locals 6

    .prologue
    const v5, 0x1080027

    const/4 v4, 0x0

    .line 245
    const-string v2, "location"

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/LocationManager;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myLM:Landroid/location/LocationManager;

    .line 246
    new-instance v2, Lcom/wf/wellsfargomobile/loc/LocationResults$MyLocationListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/wf/wellsfargomobile/loc/LocationResults$MyLocationListener;-><init>(Lcom/wf/wellsfargomobile/loc/LocationResults;Lcom/wf/wellsfargomobile/loc/LocationResults$1;)V

    iput-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myLL:Landroid/location/LocationListener;

    .line 248
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myLM:Landroid/location/LocationManager;

    const-string v3, "gps"

    invoke-virtual {v2, v3}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 249
    .local v0, gpsOn:Ljava/lang/Boolean;
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myLM:Landroid/location/LocationManager;

    const-string v3, "network"

    invoke-virtual {v2, v3}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 251
    .local v1, networkLocOn:Ljava/lang/Boolean;
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_0

    .line 252
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->gpsSearching:Ljava/lang/Boolean;

    .line 253
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f070024

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const-string v3, "Back"

    new-instance v4, Lcom/wf/wellsfargomobile/loc/LocationResults$4;

    invoke-direct {v4, p0}, Lcom/wf/wellsfargomobile/loc/LocationResults$4;-><init>(Lcom/wf/wellsfargomobile/loc/LocationResults;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const-string v3, "Settings"

    new-instance v4, Lcom/wf/wellsfargomobile/loc/LocationResults$3;

    invoke-direct {v4, p0}, Lcom/wf/wellsfargomobile/loc/LocationResults$3;-><init>(Lcom/wf/wellsfargomobile/loc/LocationResults;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 301
    :goto_0
    return-void

    .line 275
    :cond_0
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_1

    .line 276
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->gpsSearching:Ljava/lang/Boolean;

    .line 277
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f070028

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f070027

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const-string v3, "Continue"

    new-instance v4, Lcom/wf/wellsfargomobile/loc/LocationResults$6;

    invoke-direct {v4, p0}, Lcom/wf/wellsfargomobile/loc/LocationResults$6;-><init>(Lcom/wf/wellsfargomobile/loc/LocationResults;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const-string v3, "Settings"

    new-instance v4, Lcom/wf/wellsfargomobile/loc/LocationResults$5;

    invoke-direct {v4, p0}, Lcom/wf/wellsfargomobile/loc/LocationResults$5;-><init>(Lcom/wf/wellsfargomobile/loc/LocationResults;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 299
    :cond_1
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findCurrLocation()V

    goto :goto_0
.end method

.method private findCurrLocation()V
    .locals 7

    .prologue
    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    .line 304
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myLM:Landroid/location/LocationManager;

    const-string v1, "gps"

    iget-object v5, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myLL:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 305
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myLM:Landroid/location/LocationManager;

    const-string v1, "network"

    iget-object v5, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myLL:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 306
    const/16 v0, 0xc8

    iput v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->desiredAccuracy:I

    .line 307
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->gpsSearching:Ljava/lang/Boolean;

    .line 308
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->bestLoc:Landroid/location/Location;

    .line 310
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->gpsTimeout:Landroid/os/Handler;

    .line 311
    new-instance v6, Lcom/wf/wellsfargomobile/loc/LocationResults$7;

    invoke-direct {v6, p0}, Lcom/wf/wellsfargomobile/loc/LocationResults$7;-><init>(Lcom/wf/wellsfargomobile/loc/LocationResults;)V

    .line 337
    .local v6, r:Ljava/lang/Runnable;
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->gpsTimeout:Landroid/os/Handler;

    const-wide/16 v1, 0x4e20

    invoke-virtual {v0, v6, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 338
    return-void
.end method

.method private launchDetails(I)V
    .locals 4
    .parameter "i"

    .prologue
    .line 586
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/wf/wellsfargomobile/loc/LocationDetails;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 587
    .local v0, resultDetails:Landroid/content/Intent;
    const-string v2, "branch_object"

    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myBranches:Ljava/util/ArrayList;

    iget v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentBranch:I

    add-int/2addr v3, p1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/Serializable;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 588
    const-string v1, "in_session"

    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->inSession:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 589
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/wf/wellsfargomobile/loc/LocationResults;->startActivityForResult(Landroid/content/Intent;I)V

    .line 590
    return-void
.end method

.method private nonGPSSearch(Landroid/os/Bundle;)V
    .locals 5
    .parameter "extras"

    .prologue
    .line 341
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f070001

    invoke-virtual {p0, v3}, Lcom/wf/wellsfargomobile/loc/LocationResults;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/locator/search.do?WFAppId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f070003

    invoke-virtual {p0, v3}, Lcom/wf/wellsfargomobile/loc/LocationResults;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 343
    .local v1, searchUrl:Ljava/lang/String;
    const-string v0, "For Address: "

    .line 345
    .local v0, searchAddressText:Ljava/lang/String;
    :try_start_0
    const-string v2, "address_string"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 346
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "address_string"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 347
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&address="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "address_string"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "UTF-8"

    invoke-static {v3, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 350
    :cond_0
    const-string v2, "city_string"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 351
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "city_string"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 352
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&city="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "city_string"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "UTF-8"

    invoke-static {v3, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 355
    :cond_1
    const-string v2, "state_string"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 356
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "state_string"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 357
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&state="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "state_string"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "UTF-8"

    invoke-static {v3, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 360
    :cond_2
    const-string v2, "zip_string"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 361
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "zip_string"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 362
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&zip="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "zip_string"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "UTF-8"

    invoke-static {v3, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 368
    :cond_3
    :goto_0
    const-string v2, "toggle_atm"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 369
    const-string v2, "toggle_atm"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 370
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&wlw-checkbox_key:{actionForm.searchAtms}"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 373
    :cond_4
    const-string v2, "toggle_branch"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 374
    const-string v2, "toggle_branch"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 375
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&wlw-checkbox_key:{actionForm.searchBranches}"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 378
    :cond_5
    const-string v2, "toggle_instore"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 379
    const-string v2, "toggle_instore"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 380
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&wlw-checkbox_key:{actionForm.searchInStoreBranches}"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 384
    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&format=xml"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 387
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->searchAddress:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 388
    new-instance v2, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;-><init>(Lcom/wf/wellsfargomobile/loc/LocationResults;Lcom/wf/wellsfargomobile/loc/LocationResults$1;)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {v2, v3}, Lcom/wf/wellsfargomobile/loc/LocationResults$XMLParseTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 389
    return-void

    .line 365
    :catch_0
    move-exception v2

    goto/16 :goto_0
.end method

.method private returnWithError(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .parameter "title"
    .parameter "message"

    .prologue
    .line 547
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 548
    .local v0, bundle:Landroid/os/Bundle;
    if-eqz p1, :cond_0

    .line 549
    const-string v2, "error_title"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    :cond_0
    const-string v2, "error_msg"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 552
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 553
    .local v1, errorIntent:Landroid/content/Intent;
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 554
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v1}, Lcom/wf/wellsfargomobile/loc/LocationResults;->setResult(ILandroid/content/Intent;)V

    .line 555
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->finish()V

    .line 556
    return-void
.end method

.method private updateMap()V
    .locals 11

    .prologue
    const-wide v9, 0x412e848000000000L

    .line 437
    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->mapController:Lcom/google/android/maps/MapController;

    new-instance v5, Lcom/google/android/maps/GeoPoint;

    iget-wide v6, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myLat:D

    mul-double/2addr v6, v9

    double-to-int v6, v6

    iget-wide v7, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myLong:D

    mul-double/2addr v7, v9

    double-to-int v7, v7

    invoke-direct {v5, v6, v7}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    invoke-virtual {v4, v5}, Lcom/google/android/maps/MapController;->animateTo(Lcom/google/android/maps/GeoPoint;)V

    .line 438
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->updateZoom()V

    .line 439
    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->overlays:Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;

    invoke-virtual {v4}, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->clearOverlays()V

    .line 440
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    iget v4, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultsPerPage:I

    if-ge v1, v4, :cond_1

    .line 441
    iget v4, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentBranch:I

    add-int/2addr v4, v1

    iget-object v5, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myBranches:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v4, v5, :cond_0

    .line 442
    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myBranches:Ljava/util/ArrayList;

    iget v5, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentBranch:I

    add-int/2addr v5, v1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 443
    .local v0, branch:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v3, Lcom/google/android/maps/GeoPoint;

    const-string v4, "latitude"

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    mul-double/2addr v4, v9

    double-to-int v5, v4

    const-string v4, "longitude"

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    mul-double/2addr v6, v9

    double-to-int v4, v6

    invoke-direct {v3, v5, v4}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    .line 445
    .local v3, point:Lcom/google/android/maps/GeoPoint;
    new-instance v2, Lcom/google/android/maps/OverlayItem;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentBranch:I

    add-int/2addr v5, v1

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myBranches:Ljava/util/ArrayList;

    iget v6, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentBranch:I

    add-int/2addr v6, v1

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/HashMap;

    const-string v6, "bankType"

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-direct {v2, v3, v5, v4}, Lcom/google/android/maps/OverlayItem;-><init>(Lcom/google/android/maps/GeoPoint;Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    .local v2, overlayitem:Lcom/google/android/maps/OverlayItem;
    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->overlays:Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;

    invoke-virtual {v4, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->addOverlay(Lcom/google/android/maps/OverlayItem;)V

    .line 440
    .end local v0           #branch:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2           #overlayitem:Lcom/google/android/maps/OverlayItem;
    .end local v3           #point:Lcom/google/android/maps/GeoPoint;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 452
    :cond_1
    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->overlays:Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;

    invoke-virtual {v4}, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;->populateOverlays()V

    .line 453
    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->mapOverlays:Ljava/util/List;

    iget-object v5, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->overlays:Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 454
    return-void
.end method

.method private updateResults()V
    .locals 7

    .prologue
    const/4 v5, 0x4

    const/4 v6, 0x0

    .line 406
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->updateMap()V

    .line 407
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    iget v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultsPerPage:I

    if-ge v1, v2, :cond_1

    .line 408
    iget v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentBranch:I

    add-int/2addr v2, v1

    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myBranches:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 409
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myBranches:Ljava/util/ArrayList;

    iget v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentBranch:I

    add-int/2addr v3, v1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 410
    .local v0, branch:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultLabels:[Landroid/widget/TextView;

    aget-object v2, v2, v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentBranch:I

    add-int/2addr v4, v1

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 411
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultContent:[Landroid/widget/TextView;

    aget-object v3, v2, v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "siteName"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\n("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v2, "distFromStart"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " miles)\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v2, "addrLine1"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 413
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->results:[Landroid/widget/LinearLayout;

    aget-object v2, v2, v1

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 407
    .end local v0           #branch:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 415
    :cond_0
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->results:[Landroid/widget/LinearLayout;

    aget-object v2, v2, v1

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1

    .line 419
    :cond_1
    iget v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentBranch:I

    if-nez v2, :cond_2

    .line 420
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultBack:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 424
    :goto_2
    iget v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentBranch:I

    iget v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultsPerPage:I

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myBranches:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v2, v3, :cond_3

    .line 425
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultNumbers:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentBranch:I

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myBranches:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myBranches:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 427
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultMore:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 433
    :goto_3
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myScroller:Landroid/widget/ScrollView;

    invoke-virtual {v2, v6, v6}, Landroid/widget/ScrollView;->scrollTo(II)V

    .line 434
    return-void

    .line 422
    :cond_2
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultBack:Landroid/widget/Button;

    invoke-virtual {v2, v6}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_2

    .line 429
    :cond_3
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultNumbers:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentBranch:I

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentBranch:I

    iget v5, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultsPerPage:I

    add-int/2addr v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myBranches:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 431
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultMore:Landroid/widget/Button;

    invoke-virtual {v2, v6}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_3
.end method

.method private updateZoom()V
    .locals 7

    .prologue
    .line 458
    iget v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentBranch:I

    add-int/lit8 v3, v3, 0x4

    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myBranches:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 459
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myBranches:Ljava/util/ArrayList;

    iget v4, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentBranch:I

    add-int/lit8 v4, v4, 0x4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 463
    .local v0, branch:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :goto_0
    const-string v3, "distFromStart"

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    .line 464
    .local v1, dist:Ljava/lang/Double;
    const/16 v2, 0xd

    .line 465
    .local v2, newZoom:I
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    const-wide v5, 0x3fb999999999999aL

    cmpg-double v3, v3, v5

    if-gez v3, :cond_2

    .line 466
    const/16 v2, 0x11

    .line 485
    :cond_0
    :goto_1
    iget v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentZoom:I

    if-eq v3, v2, :cond_b

    .line 486
    iget v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentZoom:I

    if-ge v3, v2, :cond_a

    .line 487
    iget v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentZoom:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentZoom:I

    .line 488
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->mapController:Lcom/google/android/maps/MapController;

    invoke-virtual {v3}, Lcom/google/android/maps/MapController;->zoomIn()Z

    goto :goto_1

    .line 461
    .end local v0           #branch:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1           #dist:Ljava/lang/Double;
    .end local v2           #newZoom:I
    :cond_1
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myBranches:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myBranches:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .restart local v0       #branch:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    goto :goto_0

    .line 467
    .restart local v1       #dist:Ljava/lang/Double;
    .restart local v2       #newZoom:I
    :cond_2
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    const-wide v5, 0x3fc999999999999aL

    cmpg-double v3, v3, v5

    if-gez v3, :cond_3

    .line 468
    const/16 v2, 0x10

    goto :goto_1

    .line 469
    :cond_3
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    const-wide v5, 0x3fd999999999999aL

    cmpg-double v3, v3, v5

    if-gez v3, :cond_4

    .line 470
    const/16 v2, 0xf

    goto :goto_1

    .line 471
    :cond_4
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    const-wide v5, 0x3fe999999999999aL

    cmpg-double v3, v3, v5

    if-gez v3, :cond_5

    .line 472
    const/16 v2, 0xe

    goto :goto_1

    .line 473
    :cond_5
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    const-wide v5, 0x3ff999999999999aL

    cmpg-double v3, v3, v5

    if-gez v3, :cond_6

    .line 474
    const/16 v2, 0xd

    goto :goto_1

    .line 475
    :cond_6
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    const-wide v5, 0x400999999999999aL

    cmpg-double v3, v3, v5

    if-gez v3, :cond_7

    .line 476
    const/16 v2, 0xc

    goto :goto_1

    .line 477
    :cond_7
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    const-wide v5, 0x401999999999999aL

    cmpg-double v3, v3, v5

    if-gez v3, :cond_8

    .line 478
    const/16 v2, 0xb

    goto/16 :goto_1

    .line 479
    :cond_8
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    const-wide v5, 0x402999999999999aL

    cmpg-double v3, v3, v5

    if-gez v3, :cond_9

    .line 480
    const/16 v2, 0xa

    goto/16 :goto_1

    .line 482
    :cond_9
    const/16 v2, 0x9

    goto/16 :goto_1

    .line 489
    :cond_a
    iget v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentZoom:I

    if-le v3, v2, :cond_0

    .line 490
    iget v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentZoom:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentZoom:I

    .line 491
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->mapController:Lcom/google/android/maps/MapController;

    invoke-virtual {v3}, Lcom/google/android/maps/MapController;->zoomOut()Z

    goto/16 :goto_1

    .line 494
    :cond_b
    return-void
.end method


# virtual methods
.method protected isRouteDisplayed()Z
    .locals 1

    .prologue
    .line 927
    const/4 v0, 0x0

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "intent"

    .prologue
    const/4 v4, 0x6

    const/4 v3, 0x4

    const/4 v2, 0x3

    const/4 v1, 0x2

    const/4 v0, 0x0

    .line 854
    invoke-super {p0, p1, p2, p3}, Lcom/wf/wellsfargomobile/loc/BaseMapActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 855
    if-nez p1, :cond_0

    .line 856
    if-ne p2, v1, :cond_1

    .line 857
    invoke-virtual {p0, v1, v0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->setResult(ILandroid/content/Intent;)V

    .line 858
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->finish()V

    .line 870
    :cond_0
    :goto_0
    return-void

    .line 859
    :cond_1
    if-ne p2, v3, :cond_2

    .line 860
    invoke-virtual {p0, v3, v0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->setResult(ILandroid/content/Intent;)V

    .line 861
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->finish()V

    goto :goto_0

    .line 862
    :cond_2
    if-ne p2, v2, :cond_3

    .line 863
    invoke-virtual {p0, v2, v0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->setResult(ILandroid/content/Intent;)V

    .line 864
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->finish()V

    goto :goto_0

    .line 865
    :cond_3
    if-ne p2, v4, :cond_0

    .line 866
    invoke-virtual {p0, v4, v0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->setResult(ILandroid/content/Intent;)V

    .line 867
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->finish()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .parameter "newConfig"

    .prologue
    .line 932
    invoke-super {p0, p1}, Lcom/wf/wellsfargomobile/loc/BaseMapActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 933
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .parameter "savedInstanceState"

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v4, 0x4

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 102
    invoke-super {p0, p1}, Lcom/wf/wellsfargomobile/loc/BaseMapActivity;->onCreate(Landroid/os/Bundle;)V

    .line 103
    const v2, 0x7f030006

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->setContentView(I)V

    .line 105
    const v2, 0x7f060048

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->locCurtain:Landroid/view/View;

    .line 106
    const v2, 0x7f060011

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->loadStatus:Landroid/widget/TextView;

    .line 107
    const v2, 0x7f060030

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->searchAddress:Landroid/widget/TextView;

    .line 108
    const v2, 0x7f060031

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultNumbers:Landroid/widget/TextView;

    .line 113
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v2, v2, 0xf

    if-ne v2, v4, :cond_0

    .line 115
    const/16 v2, 0xa

    iput v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultsPerPage:I

    .line 117
    iget v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultsPerPage:I

    new-array v2, v2, [Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->results:[Landroid/widget/LinearLayout;

    .line 118
    iget v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultsPerPage:I

    new-array v2, v2, [Landroid/widget/TextView;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultLabels:[Landroid/widget/TextView;

    .line 119
    iget v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultsPerPage:I

    new-array v2, v2, [Landroid/widget/TextView;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultContent:[Landroid/widget/TextView;

    .line 121
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->results:[Landroid/widget/LinearLayout;

    const v2, 0x7f060032

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    aput-object v2, v3, v5

    .line 122
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultLabels:[Landroid/widget/TextView;

    const v2, 0x7f060033

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v5

    .line 123
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultContent:[Landroid/widget/TextView;

    const v2, 0x7f060034

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v5

    .line 125
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->results:[Landroid/widget/LinearLayout;

    const v2, 0x7f060036

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    aput-object v2, v3, v6

    .line 126
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultLabels:[Landroid/widget/TextView;

    const v2, 0x7f060037

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v6

    .line 127
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultContent:[Landroid/widget/TextView;

    const v2, 0x7f060038

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v6

    .line 129
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->results:[Landroid/widget/LinearLayout;

    const v2, 0x7f06003a

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    aput-object v2, v3, v7

    .line 130
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultLabels:[Landroid/widget/TextView;

    const v2, 0x7f06003b

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v7

    .line 131
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultContent:[Landroid/widget/TextView;

    const v2, 0x7f06003c

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v7

    .line 133
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->results:[Landroid/widget/LinearLayout;

    const v2, 0x7f06003e

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    aput-object v2, v3, v8

    .line 134
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultLabels:[Landroid/widget/TextView;

    const v2, 0x7f06003f

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v8

    .line 135
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultContent:[Landroid/widget/TextView;

    const v2, 0x7f060040

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v8

    .line 137
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->results:[Landroid/widget/LinearLayout;

    const v2, 0x7f060042

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    aput-object v2, v3, v4

    .line 138
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultLabels:[Landroid/widget/TextView;

    const v2, 0x7f060043

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    .line 139
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultContent:[Landroid/widget/TextView;

    const v2, 0x7f060044

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    .line 141
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->results:[Landroid/widget/LinearLayout;

    const/4 v4, 0x5

    const v2, 0x7f060049

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    aput-object v2, v3, v4

    .line 142
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultLabels:[Landroid/widget/TextView;

    const/4 v4, 0x5

    const v2, 0x7f06004a

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    .line 143
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultContent:[Landroid/widget/TextView;

    const/4 v4, 0x5

    const v2, 0x7f06004b

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    .line 145
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->results:[Landroid/widget/LinearLayout;

    const/4 v4, 0x6

    const v2, 0x7f06004d

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    aput-object v2, v3, v4

    .line 146
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultLabels:[Landroid/widget/TextView;

    const/4 v4, 0x6

    const v2, 0x7f06004e

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    .line 147
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultContent:[Landroid/widget/TextView;

    const/4 v4, 0x6

    const v2, 0x7f06004f

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    .line 149
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->results:[Landroid/widget/LinearLayout;

    const/4 v4, 0x7

    const v2, 0x7f060051

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    aput-object v2, v3, v4

    .line 150
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultLabels:[Landroid/widget/TextView;

    const/4 v4, 0x7

    const v2, 0x7f060052

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    .line 151
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultContent:[Landroid/widget/TextView;

    const/4 v4, 0x7

    const v2, 0x7f060053

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    .line 153
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->results:[Landroid/widget/LinearLayout;

    const/16 v4, 0x8

    const v2, 0x7f060055

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    aput-object v2, v3, v4

    .line 154
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultLabels:[Landroid/widget/TextView;

    const/16 v4, 0x8

    const v2, 0x7f060056

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    .line 155
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultContent:[Landroid/widget/TextView;

    const/16 v4, 0x8

    const v2, 0x7f060057

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    .line 157
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->results:[Landroid/widget/LinearLayout;

    const/16 v4, 0x9

    const v2, 0x7f060059

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    aput-object v2, v3, v4

    .line 158
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultLabels:[Landroid/widget/TextView;

    const/16 v4, 0x9

    const v2, 0x7f06005a

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    .line 159
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultContent:[Landroid/widget/TextView;

    const/16 v4, 0x9

    const v2, 0x7f06005b

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    .line 188
    :goto_0
    const v2, 0x7f060046

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultBack:Landroid/widget/Button;

    .line 189
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultBack:Landroid/widget/Button;

    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->buttonHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 191
    const v2, 0x7f060047

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultMore:Landroid/widget/Button;

    .line 192
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultMore:Landroid/widget/Button;

    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->buttonHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 194
    const v2, 0x7f06002f

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ZoomControls;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->zoomControls:Landroid/widget/ZoomControls;

    .line 195
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->zoomControls:Landroid/widget/ZoomControls;

    new-instance v3, Lcom/wf/wellsfargomobile/loc/LocationResults$1;

    invoke-direct {v3, p0}, Lcom/wf/wellsfargomobile/loc/LocationResults$1;-><init>(Lcom/wf/wellsfargomobile/loc/LocationResults;)V

    invoke-virtual {v2, v3}, Landroid/widget/ZoomControls;->setOnZoomInClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->zoomControls:Landroid/widget/ZoomControls;

    new-instance v3, Lcom/wf/wellsfargomobile/loc/LocationResults$2;

    invoke-direct {v3, p0}, Lcom/wf/wellsfargomobile/loc/LocationResults$2;-><init>(Lcom/wf/wellsfargomobile/loc/LocationResults;)V

    invoke-virtual {v2, v3}, Landroid/widget/ZoomControls;->setOnZoomOutClickListener(Landroid/view/View$OnClickListener;)V

    .line 212
    const/4 v1, 0x0

    .local v1, i:I
    :goto_1
    iget v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultsPerPage:I

    if-ge v1, v2, :cond_1

    .line 213
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->results:[Landroid/widget/LinearLayout;

    aget-object v2, v2, v1

    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultsHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 212
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 161
    .end local v1           #i:I
    :cond_0
    const/4 v2, 0x5

    iput v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultsPerPage:I

    .line 163
    iget v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultsPerPage:I

    new-array v2, v2, [Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->results:[Landroid/widget/LinearLayout;

    .line 164
    iget v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultsPerPage:I

    new-array v2, v2, [Landroid/widget/TextView;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultLabels:[Landroid/widget/TextView;

    .line 165
    iget v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultsPerPage:I

    new-array v2, v2, [Landroid/widget/TextView;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultContent:[Landroid/widget/TextView;

    .line 167
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->results:[Landroid/widget/LinearLayout;

    const v2, 0x7f060032

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    aput-object v2, v3, v5

    .line 168
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultLabels:[Landroid/widget/TextView;

    const v2, 0x7f060033

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v5

    .line 169
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultContent:[Landroid/widget/TextView;

    const v2, 0x7f060034

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v5

    .line 171
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->results:[Landroid/widget/LinearLayout;

    const v2, 0x7f060036

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    aput-object v2, v3, v6

    .line 172
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultLabels:[Landroid/widget/TextView;

    const v2, 0x7f060037

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v6

    .line 173
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultContent:[Landroid/widget/TextView;

    const v2, 0x7f060038

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v6

    .line 175
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->results:[Landroid/widget/LinearLayout;

    const v2, 0x7f06003a

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    aput-object v2, v3, v7

    .line 176
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultLabels:[Landroid/widget/TextView;

    const v2, 0x7f06003b

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v7

    .line 177
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultContent:[Landroid/widget/TextView;

    const v2, 0x7f06003c

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v7

    .line 179
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->results:[Landroid/widget/LinearLayout;

    const v2, 0x7f06003e

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    aput-object v2, v3, v8

    .line 180
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultLabels:[Landroid/widget/TextView;

    const v2, 0x7f06003f

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v8

    .line 181
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultContent:[Landroid/widget/TextView;

    const v2, 0x7f060040

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v8

    .line 183
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->results:[Landroid/widget/LinearLayout;

    const v2, 0x7f060042

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    aput-object v2, v3, v4

    .line 184
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultLabels:[Landroid/widget/TextView;

    const v2, 0x7f060043

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    .line 185
    iget-object v3, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultContent:[Landroid/widget/TextView;

    const v2, 0x7f060044

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    goto/16 :goto_0

    .line 216
    .restart local v1       #i:I
    :cond_1
    const v2, 0x7f06002b

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ScrollView;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myScroller:Landroid/widget/ScrollView;

    .line 217
    const v2, 0x7f06002d

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/maps/MapView;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultMap:Lcom/google/android/maps/MapView;

    .line 218
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultMap:Lcom/google/android/maps/MapView;

    invoke-virtual {v2}, Lcom/google/android/maps/MapView;->getController()Lcom/google/android/maps/MapController;

    move-result-object v2

    iput-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->mapController:Lcom/google/android/maps/MapController;

    .line 219
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultMap:Lcom/google/android/maps/MapView;

    invoke-virtual {v2}, Lcom/google/android/maps/MapView;->getOverlays()Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->mapOverlays:Ljava/util/List;

    .line 220
    new-instance v2, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;

    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020019

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;-><init>(Lcom/wf/wellsfargomobile/loc/LocationResults;Landroid/graphics/drawable/Drawable;)V

    iput-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->overlays:Lcom/wf/wellsfargomobile/loc/LocationResults$LocationMapOverlay;

    .line 222
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 223
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_2

    .line 224
    const-string v2, "was_gps"

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->wasGPS:Ljava/lang/Boolean;

    .line 226
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->wasGPS:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 227
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->loadStatus:Landroid/widget/TextView;

    const v3, 0x7f070036

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 228
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->checkLocationProviders()V

    .line 234
    :cond_2
    :goto_2
    if-eqz v0, :cond_4

    const-string v2, "in_session"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "in_session"

    invoke-virtual {v0, v2, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 236
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->inSession:Ljava/lang/Boolean;

    .line 241
    :goto_3
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/wf/wellsfargomobile/WFApp;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    .line 242
    return-void

    .line 230
    :cond_3
    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->loadStatus:Landroid/widget/TextView;

    const v3, 0x7f070037

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 231
    invoke-direct {p0, v0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->nonGPSSearch(Landroid/os/Bundle;)V

    goto :goto_2

    .line 238
    :cond_4
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->inSession:Ljava/lang/Boolean;

    goto :goto_3
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .parameter "menu"

    .prologue
    .line 874
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 875
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_1

    const-string v1, "in_session"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "in_session"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 878
    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-virtual {v1}, Lcom/wf/wellsfargomobile/WFApp;->getMrdcEnabled()Lcom/wf/wellsfargomobile/util/Trinary;

    move-result-object v1

    sget-object v2, Lcom/wf/wellsfargomobile/util/Trinary;->TRUE:Lcom/wf/wellsfargomobile/util/Trinary;

    invoke-virtual {v1, v2}, Lcom/wf/wellsfargomobile/util/Trinary;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 879
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const v2, 0x7f0a0001

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 887
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 881
    :cond_0
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const v2, 0x7f0a0002

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0

    .line 885
    :cond_1
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const v2, 0x7f0a0003

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 937
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->wasGPS:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 938
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myLM:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->myLL:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 940
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->gpsSearching:Ljava/lang/Boolean;

    .line 941
    invoke-super {p0}, Lcom/wf/wellsfargomobile/loc/BaseMapActivity;->onDestroy()V

    .line 942
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 915
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 916
    iget v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentBranch:I

    if-lez v0, :cond_0

    .line 917
    iget v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentBranch:I

    iget v1, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->resultsPerPage:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/wf/wellsfargomobile/loc/LocationResults;->currentBranch:I

    .line 918
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->updateResults()V

    .line 919
    const/4 v0, 0x0

    .line 922
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/wf/wellsfargomobile/loc/BaseMapActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .parameter "item"

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 892
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 910
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 894
    :pswitch_0
    const/4 v1, 0x4

    invoke-virtual {p0, v1, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->setResult(ILandroid/content/Intent;)V

    .line 895
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->finish()V

    goto :goto_0

    .line 898
    :pswitch_1
    const/4 v1, 0x2

    invoke-virtual {p0, v1, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->setResult(ILandroid/content/Intent;)V

    .line 899
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->finish()V

    goto :goto_0

    .line 902
    :pswitch_2
    const/4 v1, 0x3

    invoke-virtual {p0, v1, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->setResult(ILandroid/content/Intent;)V

    .line 903
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->finish()V

    goto :goto_0

    .line 906
    :pswitch_3
    const/4 v1, 0x6

    invoke-virtual {p0, v1, v2}, Lcom/wf/wellsfargomobile/loc/LocationResults;->setResult(ILandroid/content/Intent;)V

    .line 907
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/loc/LocationResults;->finish()V

    goto :goto_0

    .line 892
    nop

    :pswitch_data_0
    .packed-switch 0x7f06007e
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
