.class public Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "LocationXMLHandler.java"


# instance fields
.field private currentBranch:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private currentElementData:Ljava/lang/String;

.field private currentKey:Ljava/lang/String;

.field private isKey:Z

.field private isValue:Z

.field private myLocations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public characters([CII)V
    .locals 2
    .parameter "ch"
    .parameter "start"
    .parameter "length"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;->isKey:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;->isValue:Z

    if-eqz v0, :cond_1

    .line 48
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;->currentElementData:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;->currentElementData:Ljava/lang/String;

    .line 50
    :cond_1
    return-void
.end method

.method public endDocument()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 70
    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .parameter "uri"
    .parameter "localName"
    .parameter "qName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 54
    const-string v0, "dict"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;->myLocations:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;->currentBranch:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 64
    :cond_0
    :goto_0
    iput-boolean v3, p0, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;->isKey:Z

    .line 65
    iput-boolean v3, p0, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;->isValue:Z

    .line 66
    return-void

    .line 56
    :cond_1
    iget-boolean v0, p0, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;->isKey:Z

    if-eqz v0, :cond_2

    .line 57
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;->currentElementData:Ljava/lang/String;

    const-string v1, "(\\r|\\n)"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;->currentElementData:Ljava/lang/String;

    .line 58
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;->currentElementData:Ljava/lang/String;

    iput-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;->currentKey:Ljava/lang/String;

    goto :goto_0

    .line 59
    :cond_2
    iget-boolean v0, p0, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;->isValue:Z

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;->currentElementData:Ljava/lang/String;

    const-string v1, "(\\r|\\n)"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;->currentElementData:Ljava/lang/String;

    .line 61
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;->currentBranch:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;->currentKey:Ljava/lang/String;

    iget-object v2, p0, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;->currentElementData:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public getParsedLocations()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 22
    iget-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;->myLocations:Ljava/util/ArrayList;

    return-object v0
.end method

.method public startDocument()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;->myLocations:Ljava/util/ArrayList;

    .line 28
    iput-boolean v1, p0, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;->isKey:Z

    .line 29
    iput-boolean v1, p0, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;->isValue:Z

    .line 30
    return-void
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .parameter "uri"
    .parameter "localName"
    .parameter "qName"
    .parameter "attributes"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 34
    const-string v0, "dict"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 35
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;->currentBranch:Ljava/util/HashMap;

    .line 43
    :cond_0
    :goto_0
    return-void

    .line 36
    :cond_1
    const-string v0, "key"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 37
    iput-boolean v1, p0, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;->isKey:Z

    .line 38
    const-string v0, ""

    iput-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;->currentElementData:Ljava/lang/String;

    goto :goto_0

    .line 39
    :cond_2
    const-string v0, "string"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "integer"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    :cond_3
    iput-boolean v1, p0, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;->isValue:Z

    .line 41
    const-string v0, ""

    iput-object v0, p0, Lcom/wf/wellsfargomobile/loc/LocationXMLHandler;->currentElementData:Ljava/lang/String;

    goto :goto_0
.end method
