.class Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$1;
.super Ljava/lang/Object;
.source "CapturePhoto.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;


# direct methods
.method constructor <init>(Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;)V
    .locals 0
    .parameter

    .prologue
    .line 119
    iput-object p1, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$1;->this$0:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 124
    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$1;->this$0:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;

    const-string v3, "WF_PREFERENCES"

    invoke-virtual {v2, v3, v6}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 126
    .local v1, sp:Landroid/content/SharedPreferences;
    const-string v2, "PHOTO_TIPS_DONT_SHOW"

    invoke-interface {v1, v2, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 127
    .local v0, dontShowPhotoTips:Z
    if-nez v0, :cond_1

    .line 128
    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$1;->this$0:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;

    iget-object v2, v2, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->jsPhotoTips:Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;

    if-nez v2, :cond_0

    .line 129
    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$1;->this$0:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;

    new-instance v3, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;

    iget-object v4, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$1;->this$0:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;

    iget-object v5, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$1;->this$0:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;

    #getter for: Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static {v5}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->access$100(Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;-><init>(Landroid/content/Context;Lcom/wf/wellsfargomobile/WFApp;)V

    iput-object v3, v2, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->jsPhotoTips:Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;

    .line 132
    :cond_0
    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$1;->this$0:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;

    iget-object v2, v2, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->jsPhotoTips:Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;

    iget-object v3, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$1;->this$0:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;

    #getter for: Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static {v3}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->access$100(Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/wf/wellsfargomobile/WFApp;->getNonce()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v6}, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;->openPhotoTips(Ljava/lang/String;Z)V

    .line 134
    :cond_1
    return-void
.end method
