.class Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$5;
.super Ljava/lang/Object;
.source "CapturePhoto.java"

# interfaces
.implements Landroid/hardware/Camera$PictureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;


# direct methods
.method constructor <init>(Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;)V
    .locals 0
    .parameter

    .prologue
    .line 182
    iput-object p1, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$5;->this$0:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPictureTaken([BLandroid/hardware/Camera;)V
    .locals 7
    .parameter "picture"
    .parameter "cam"

    .prologue
    const/4 v6, 0x0

    .line 185
    array-length v3, p1

    invoke-static {p1, v6, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 199
    .local v1, bitmapPicture:Landroid/graphics/Bitmap;
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 200
    .local v0, baos:Ljava/io/ByteArrayOutputStream;
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x32

    invoke-virtual {v1, v3, v4, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 203
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1

    .line 206
    iget-object v3, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$5;->this$0:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;

    #getter for: Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->previewSize:Landroid/hardware/Camera$Size;
    invoke-static {v3}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->access$200(Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;)Landroid/hardware/Camera$Size;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 207
    iget-object v3, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$5;->this$0:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;

    #getter for: Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->previewSize:Landroid/hardware/Camera$Size;
    invoke-static {v3}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->access$200(Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;)Landroid/hardware/Camera$Size;

    move-result-object v3

    iget v3, v3, Landroid/hardware/Camera$Size;->width:I

    invoke-static {v1, v3}, Lcom/wf/wellsfargomobile/mrdc/ImageUtilities;->resizeImageForWidth(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 224
    :cond_0
    sget-object v3, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$7;->$SwitchMap$com$wf$wellsfargomobile$mrdc$ImageMode:[I

    iget-object v4, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$5;->this$0:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;

    #getter for: Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->imageMode:Lcom/wf/wellsfargomobile/mrdc/ImageMode;
    invoke-static {v4}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->access$300(Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;)Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    move-result-object v4

    invoke-virtual {v4}, Lcom/wf/wellsfargomobile/mrdc/ImageMode;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 237
    const-string v3, "CapturePhoto"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "unrecognized image mode: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$5;->this$0:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;

    #getter for: Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->imageMode:Lcom/wf/wellsfargomobile/mrdc/ImageMode;
    invoke-static {v5}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->access$300(Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;)Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    :goto_0
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$5;->this$0:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;

    invoke-virtual {v3}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 243
    .local v2, intent:Landroid/content/Intent;
    const-string v3, "MRDC_FLOW_STATE"

    iget-object v4, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$5;->this$0:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;

    #getter for: Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->imageMode:Lcom/wf/wellsfargomobile/mrdc/ImageMode;
    invoke-static {v4}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->access$300(Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;)Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 247
    iget-object v3, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$5;->this$0:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;

    const/4 v4, 0x3

    invoke-virtual {v3, v2, v4}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->startActivityForResult(Landroid/content/Intent;I)V

    .line 248
    iget-object v3, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$5;->this$0:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;

    #setter for: Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->isPictureTaking:Z
    invoke-static {v3, v6}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->access$402(Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;Z)Z

    .line 249
    return-void

    .line 226
    .end local v2           #intent:Landroid/content/Intent;
    :pswitch_0
    iget-object v3, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$5;->this$0:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;

    #getter for: Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static {v3}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->access$100(Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/wf/wellsfargomobile/WFApp;->setFrontCheckImage([B)V

    .line 227
    iget-object v3, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$5;->this$0:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;

    #getter for: Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static {v3}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->access$100(Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/wf/wellsfargomobile/WFApp;->setFrontCheckPreviewImage(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 231
    :pswitch_1
    iget-object v3, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$5;->this$0:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;

    #getter for: Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static {v3}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->access$100(Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/wf/wellsfargomobile/WFApp;->setRearCheckImage([B)V

    .line 232
    iget-object v3, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$5;->this$0:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;

    #getter for: Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static {v3}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->access$100(Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/wf/wellsfargomobile/WFApp;->setRearCheckPreviewImage(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 224
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
