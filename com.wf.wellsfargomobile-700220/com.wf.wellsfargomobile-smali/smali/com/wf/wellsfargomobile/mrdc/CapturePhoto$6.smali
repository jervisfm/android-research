.class final Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$6;
.super Ljava/lang/Object;
.source "CapturePhoto.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->getOptimumSupportedPreviewSize(IIIILjava/util/List;Landroid/hardware/Camera$Size;)Landroid/hardware/Camera$Size;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/hardware/Camera$Size;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 509
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Landroid/hardware/Camera$Size;Landroid/hardware/Camera$Size;)I
    .locals 4
    .parameter "a"
    .parameter "b"

    .prologue
    .line 512
    iget v2, p1, Landroid/hardware/Camera$Size;->height:I

    iget v3, p1, Landroid/hardware/Camera$Size;->width:I

    mul-int v0, v2, v3

    .line 513
    .local v0, aPixels:I
    iget v2, p2, Landroid/hardware/Camera$Size;->height:I

    iget v3, p2, Landroid/hardware/Camera$Size;->width:I

    mul-int v1, v2, v3

    .line 514
    .local v1, bPixels:I
    if-ge v1, v0, :cond_0

    .line 515
    const/4 v2, -0x1

    .line 520
    :goto_0
    return v2

    .line 517
    :cond_0
    if-le v1, v0, :cond_1

    .line 518
    const/4 v2, 0x1

    goto :goto_0

    .line 520
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 509
    check-cast p1, Landroid/hardware/Camera$Size;

    .end local p1
    check-cast p2, Landroid/hardware/Camera$Size;

    .end local p2
    invoke-virtual {p0, p1, p2}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$6;->compare(Landroid/hardware/Camera$Size;Landroid/hardware/Camera$Size;)I

    move-result v0

    return v0
.end method
