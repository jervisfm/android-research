.class Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$AutoFocusCallBack;
.super Ljava/lang/Object;
.source "CapturePhoto.java"

# interfaces
.implements Landroid/hardware/Camera$AutoFocusCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AutoFocusCallBack"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;


# direct methods
.method private constructor <init>(Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;)V
    .locals 0
    .parameter

    .prologue
    .line 416
    iput-object p1, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$AutoFocusCallBack;->this$0:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$1;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 416
    invoke-direct {p0, p1}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$AutoFocusCallBack;-><init>(Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;)V

    return-void
.end method


# virtual methods
.method public onAutoFocus(ZLandroid/hardware/Camera;)V
    .locals 3
    .parameter "success"
    .parameter "camera"

    .prologue
    .line 423
    iget-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$AutoFocusCallBack;->this$0:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;

    #getter for: Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->isPictureTaking:Z
    invoke-static {v0}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->access$400(Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 425
    iget-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$AutoFocusCallBack;->this$0:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->myShutterCallback:Landroid/hardware/Camera$ShutterCallback;

    iget-object v1, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$AutoFocusCallBack;->this$0:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;

    iget-object v1, v1, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->myPictureCallback_RAW:Landroid/hardware/Camera$PictureCallback;

    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$AutoFocusCallBack;->this$0:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;

    iget-object v2, v2, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->myPictureCallback_JPG:Landroid/hardware/Camera$PictureCallback;

    invoke-virtual {p2, v0, v1, v2}, Landroid/hardware/Camera;->takePicture(Landroid/hardware/Camera$ShutterCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;)V

    .line 426
    iget-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$AutoFocusCallBack;->this$0:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;

    const/4 v1, 0x0

    #setter for: Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->isPictureTaking:Z
    invoke-static {v0, v1}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->access$402(Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;Z)Z

    .line 428
    :cond_0
    return-void
.end method
