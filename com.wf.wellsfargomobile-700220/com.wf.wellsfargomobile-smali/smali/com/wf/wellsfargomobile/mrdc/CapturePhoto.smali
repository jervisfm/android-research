.class public Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;
.super Lcom/wf/wellsfargomobile/BaseActivity;
.source "CapturePhoto.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$7;,
        Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$AutoFocusCallBack;
    }
.end annotation


# static fields
.field private static final MAX_PREVIEW_PIXELS:I = 0xa8c00

.field private static final MIN_PREVIEW_PIXELS:I = 0x24b80

.field private static final TAG:Ljava/lang/String; = "CapturePhoto"


# instance fields
.field private autoFocusCallBack:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$AutoFocusCallBack;

.field camera:Landroid/hardware/Camera;

.field private imageMode:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

.field instruction:Landroid/widget/TextView;

.field private isPictureTaking:Z

.field jsPhotoTips:Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;

.field myPictureCallback_JPG:Landroid/hardware/Camera$PictureCallback;

.field myPictureCallback_RAW:Landroid/hardware/Camera$PictureCallback;

.field myShutterCallback:Landroid/hardware/Camera$ShutterCallback;

.field orientationWarning:Landroid/widget/ImageView;

.field private previewSize:Landroid/hardware/Camera$Size;

.field previewing:Z

.field private sensorManager:Landroid/hardware/SensorManager;

.field surfaceHolder:Landroid/view/SurfaceHolder;

.field surfaceView:Landroid/view/SurfaceView;

.field private takePhotoButton:Landroid/widget/ImageButton;

.field private wfApp:Lcom/wf/wellsfargomobile/WFApp;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/BaseActivity;-><init>()V

    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->previewing:Z

    .line 67
    iput-object v1, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->imageMode:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    .line 68
    iput-object v1, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->orientationWarning:Landroid/widget/ImageView;

    .line 69
    iput-object v1, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->instruction:Landroid/widget/TextView;

    .line 70
    iput-object v1, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->jsPhotoTips:Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;

    .line 71
    new-instance v0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$AutoFocusCallBack;

    invoke-direct {v0, p0, v1}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$AutoFocusCallBack;-><init>(Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$1;)V

    iput-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->autoFocusCallBack:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$AutoFocusCallBack;

    .line 73
    iput-object v1, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->sensorManager:Landroid/hardware/SensorManager;

    .line 170
    new-instance v0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$3;

    invoke-direct {v0, p0}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$3;-><init>(Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;)V

    iput-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->myShutterCallback:Landroid/hardware/Camera$ShutterCallback;

    .line 176
    new-instance v0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$4;

    invoke-direct {v0, p0}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$4;-><init>(Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;)V

    iput-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->myPictureCallback_RAW:Landroid/hardware/Camera$PictureCallback;

    .line 182
    new-instance v0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$5;

    invoke-direct {v0, p0}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$5;-><init>(Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;)V

    iput-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->myPictureCallback_JPG:Landroid/hardware/Camera$PictureCallback;

    .line 416
    return-void
.end method

.method static synthetic access$100(Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;)Lcom/wf/wellsfargomobile/WFApp;
    .locals 1
    .parameter "x0"

    .prologue
    .line 53
    iget-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    return-object v0
.end method

.method static synthetic access$200(Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;)Landroid/hardware/Camera$Size;
    .locals 1
    .parameter "x0"

    .prologue
    .line 53
    iget-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->previewSize:Landroid/hardware/Camera$Size;

    return-object v0
.end method

.method static synthetic access$300(Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;)Lcom/wf/wellsfargomobile/mrdc/ImageMode;
    .locals 1
    .parameter "x0"

    .prologue
    .line 53
    iget-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->imageMode:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    return-object v0
.end method

.method static synthetic access$400(Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->isPictureTaking:Z

    return v0
.end method

.method static synthetic access$402(Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->isPictureTaking:Z

    return p1
.end method

.method private doSetTorch(Landroid/hardware/Camera$Parameters;Z)V
    .locals 6
    .parameter "parameters"
    .parameter "newSetting"

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 403
    if-eqz p2, :cond_1

    .line 404
    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedFlashModes()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "torch"

    aput-object v3, v2, v4

    const-string v3, "on"

    aput-object v3, v2, v5

    invoke-direct {p0, v1, v2}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->findSettableValue(Ljava/util/Collection;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 411
    .local v0, flashMode:Ljava/lang/String;
    :goto_0
    if-eqz v0, :cond_0

    .line 412
    invoke-virtual {p1, v0}, Landroid/hardware/Camera$Parameters;->setFlashMode(Ljava/lang/String;)V

    .line 414
    :cond_0
    return-void

    .line 408
    .end local v0           #flashMode:Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedFlashModes()Ljava/util/List;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "off"

    aput-object v3, v2, v4

    invoke-direct {p0, v1, v2}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->findSettableValue(Ljava/util/Collection;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .restart local v0       #flashMode:Ljava/lang/String;
    goto :goto_0
.end method

.method private varargs findSettableValue(Ljava/util/Collection;[Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .parameter
    .parameter "desiredValues"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;[",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 618
    .local p1, supportedValues:Ljava/util/Collection;,"Ljava/util/Collection<Ljava/lang/String;>;"
    const/4 v4, 0x0

    .line 619
    .local v4, result:Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 620
    move-object v0, p2

    .local v0, arr$:[Ljava/lang/String;
    array-length v3, v0

    .local v3, len$:I
    const/4 v2, 0x0

    .local v2, i$:I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 621
    .local v1, desiredValue:Ljava/lang/String;
    invoke-interface {p1, v1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 622
    move-object v4, v1

    .line 630
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #desiredValue:Ljava/lang/String;
    .end local v2           #i$:I
    .end local v3           #len$:I
    :cond_0
    return-object v4

    .line 620
    .restart local v0       #arr$:[Ljava/lang/String;
    .restart local v1       #desiredValue:Ljava/lang/String;
    .restart local v2       #i$:I
    .restart local v3       #len$:I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private getBestSupportedSize(ILandroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Size;
    .locals 9
    .parameter "targetImageWidth"
    .parameter "parameters"

    .prologue
    .line 439
    const/4 v3, 0x0

    .line 441
    .local v3, retVal:Landroid/hardware/Camera$Size;
    invoke-virtual {p2}, Landroid/hardware/Camera$Parameters;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v5

    .line 449
    .local v5, supportedSizes:Ljava/util/List;,"Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    if-eqz v5, :cond_2

    .line 450
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/hardware/Camera$Size;

    .line 456
    .local v4, s:Landroid/hardware/Camera$Size;
    iget v6, v4, Landroid/hardware/Camera$Size;->width:I

    if-gt v6, p1, :cond_0

    .line 457
    if-nez v3, :cond_1

    .line 458
    move-object v3, v4

    goto :goto_0

    .line 460
    :cond_1
    iget v6, v3, Landroid/hardware/Camera$Size;->width:I

    iget v7, v3, Landroid/hardware/Camera$Size;->height:I

    mul-int v2, v6, v7

    .line 461
    .local v2, resultArea:I
    iget v6, v4, Landroid/hardware/Camera$Size;->width:I

    iget v7, v4, Landroid/hardware/Camera$Size;->height:I

    mul-int v1, v6, v7

    .line 463
    .local v1, newArea:I
    if-lt v1, v2, :cond_0

    .line 464
    move-object v3, v4

    goto :goto_0

    .line 471
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #newArea:I
    .end local v2           #resultArea:I
    .end local v4           #s:Landroid/hardware/Camera$Size;
    :cond_2
    if-nez v3, :cond_3

    .line 472
    new-instance v3, Landroid/hardware/Camera$Size;

    .end local v3           #retVal:Landroid/hardware/Camera$Size;
    iget-object v6, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->camera:Landroid/hardware/Camera;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v7, 0x640

    const/16 v8, 0x4b0

    invoke-direct {v3, v6, v7, v8}, Landroid/hardware/Camera$Size;-><init>(Landroid/hardware/Camera;II)V

    .line 485
    .restart local v3       #retVal:Landroid/hardware/Camera$Size;
    :cond_3
    return-object v3
.end method

.method protected static getOptimumSupportedPreviewSize(IIIILjava/util/List;Landroid/hardware/Camera$Size;)Landroid/hardware/Camera$Size;
    .locals 14
    .parameter "targetHeight"
    .parameter "targetWidth"
    .parameter "pictureHeight"
    .parameter "pictureWidth"
    .parameter
    .parameter "defaultPreviewSize"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIII",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;",
            "Landroid/hardware/Camera$Size;",
            ")",
            "Landroid/hardware/Camera$Size;"
        }
    .end annotation

    .prologue
    .line 504
    .local p4, sizes:Ljava/util/List;,"Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    if-nez p4, :cond_1

    .line 505
    const/4 v5, 0x0

    .line 604
    :cond_0
    :goto_0
    return-object v5

    .line 509
    :cond_1
    new-instance v12, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$6;

    invoke-direct {v12}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$6;-><init>()V

    move-object/from16 v0, p4

    invoke-static {v0, v12}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 526
    const/4 v5, 0x0

    .line 535
    .local v5, optimalSize:Landroid/hardware/Camera$Size;
    int-to-float v12, p1

    int-to-float v13, p0

    div-float v11, v12, v13

    .line 536
    .local v11, targetRatio:F
    mul-int v12, p1, p0

    int-to-long v3, v12

    .line 550
    .local v3, maxPixels:J
    const/high16 v1, 0x7f80

    .line 552
    .local v1, diff:F
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/hardware/Camera$Size;

    .line 558
    .local v10, size:Landroid/hardware/Camera$Size;
    iget v12, v10, Landroid/hardware/Camera$Size;->height:I

    if-ne v12, p0, :cond_4

    iget v12, v10, Landroid/hardware/Camera$Size;->width:I

    if-ne v12, p1, :cond_4

    .line 562
    move-object v5, v10

    .line 590
    .end local v10           #size:Landroid/hardware/Camera$Size;
    :cond_3
    if-nez v5, :cond_0

    .line 597
    move-object/from16 v5, p5

    goto :goto_0

    .line 567
    .restart local v10       #size:Landroid/hardware/Camera$Size;
    :cond_4
    iget v12, v10, Landroid/hardware/Camera$Size;->width:I

    iget v13, v10, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v12, v13

    int-to-long v6, v12

    .line 568
    .local v6, pixels:J
    cmp-long v12, v6, v3

    if-gtz v12, :cond_2

    .line 572
    iget v12, v10, Landroid/hardware/Camera$Size;->width:I

    int-to-float v12, v12

    iget v13, v10, Landroid/hardware/Camera$Size;->height:I

    int-to-float v13, v13

    div-float v8, v12, v13

    .line 573
    .local v8, ratio:F
    sub-float v12, v8, v11

    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v9

    .line 579
    .local v9, ratioDiff:F
    cmpg-float v12, v9, v1

    if-gez v12, :cond_2

    .line 580
    move-object v5, v10

    .line 585
    move v1, v9

    goto :goto_1
.end method

.method private setTorch(Landroid/hardware/Camera;Z)V
    .locals 1
    .parameter "camera"
    .parameter "newSetting"

    .prologue
    .line 396
    invoke-virtual {p1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    .line 397
    .local v0, parameters:Landroid/hardware/Camera$Parameters;
    invoke-direct {p0, v0, p2}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->doSetTorch(Landroid/hardware/Camera$Parameters;Z)V

    .line 398
    invoke-virtual {p1, v0}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 399
    return-void
.end method

.method private setTorch(Z)V
    .locals 2
    .parameter "setting"

    .prologue
    .line 382
    iget-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->camera:Landroid/hardware/Camera;

    if-eqz v0, :cond_1

    .line 383
    const-string v0, "auto"

    iget-object v1, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->camera:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getFocusMode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 385
    iget-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->cancelAutoFocus()V

    .line 387
    :cond_0
    iget-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->camera:Landroid/hardware/Camera;

    invoke-direct {p0, v0, p1}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->setTorch(Landroid/hardware/Camera;Z)V

    .line 388
    const-string v0, "auto"

    iget-object v1, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->camera:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getFocusMode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 390
    iget-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->camera:Landroid/hardware/Camera;

    iget-object v1, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->autoFocusCallBack:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$AutoFocusCallBack;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    .line 393
    :cond_1
    return-void
.end method

.method private takePhoto()V
    .locals 5

    .prologue
    .line 683
    iget-object v1, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->takePhotoButton:Landroid/widget/ImageButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 688
    iget-boolean v1, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->isPictureTaking:Z

    if-nez v1, :cond_0

    .line 689
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->isPictureTaking:Z

    .line 691
    iget-object v1, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->camera:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    .line 693
    .local v0, params:Landroid/hardware/Camera$Parameters;
    const-string v1, "auto"

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getFocusMode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 699
    iget-object v1, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->camera:Landroid/hardware/Camera;

    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->autoFocusCallBack:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$AutoFocusCallBack;

    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    .line 709
    .end local v0           #params:Landroid/hardware/Camera$Parameters;
    :cond_0
    :goto_0
    return-void

    .line 706
    .restart local v0       #params:Landroid/hardware/Camera$Parameters;
    :cond_1
    iget-object v1, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->camera:Landroid/hardware/Camera;

    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->myShutterCallback:Landroid/hardware/Camera$ShutterCallback;

    iget-object v3, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->myPictureCallback_RAW:Landroid/hardware/Camera$PictureCallback;

    iget-object v4, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->myPictureCallback_JPG:Landroid/hardware/Camera$PictureCallback;

    invoke-virtual {v1, v2, v3, v4}, Landroid/hardware/Camera;->takePicture(Landroid/hardware/Camera$ShutterCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;)V

    goto :goto_0
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .parameter "arg0"
    .parameter "arg1"

    .prologue
    .line 721
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "intent"

    .prologue
    .line 254
    invoke-super {p0, p1, p2, p3}, Lcom/wf/wellsfargomobile/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 259
    packed-switch p2, :pswitch_data_0

    .line 296
    :goto_0
    return-void

    .line 266
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 267
    .local v0, in:Landroid/content/Intent;
    const/16 v1, 0xa

    invoke-virtual {p0, v1, v0}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->setResult(ILandroid/content/Intent;)V

    .line 268
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->finish()V

    goto :goto_0

    .line 277
    .end local v0           #in:Landroid/content/Intent;
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 278
    .restart local v0       #in:Landroid/content/Intent;
    const/16 v1, 0xb

    invoke-virtual {p0, v1, v0}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->setResult(ILandroid/content/Intent;)V

    .line 279
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->finish()V

    goto :goto_0

    .line 259
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCancel(Landroid/view/View;)V
    .locals 2
    .parameter "v"

    .prologue
    .line 677
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 678
    .local v0, in:Landroid/content/Intent;
    const/16 v1, 0xa

    invoke-virtual {p0, v1, v0}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->setResult(ILandroid/content/Intent;)V

    .line 679
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->finish()V

    .line 680
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter "savedInstanceState"

    .prologue
    const/4 v3, 0x0

    .line 83
    invoke-super {p0, p1}, Lcom/wf/wellsfargomobile/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 88
    const/high16 v2, 0x7f03

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->setContentView(I)V

    .line 89
    invoke-virtual {p0, v3}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->setRequestedOrientation(I)V

    .line 91
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/Window;->setFormat(I)V

    .line 92
    const v2, 0x7f060001

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/SurfaceView;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->surfaceView:Landroid/view/SurfaceView;

    .line 93
    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v2}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v2

    iput-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->surfaceHolder:Landroid/view/SurfaceHolder;

    .line 94
    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->surfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v2, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 95
    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->surfaceHolder:Landroid/view/SurfaceHolder;

    const/4 v3, 0x3

    invoke-interface {v2, v3}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 99
    const-string v2, "sensor"

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/SensorManager;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->sensorManager:Landroid/hardware/SensorManager;

    .line 100
    const v2, 0x7f06000f

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->orientationWarning:Landroid/widget/ImageView;

    .line 102
    const v2, 0x7f060009

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->instruction:Landroid/widget/TextView;

    .line 105
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 106
    .local v1, intent:Landroid/content/Intent;
    const-string v2, "MRDC_FLOW_STATE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    .line 107
    .local v0, flag:Ljava/io/Serializable;
    if-nez v0, :cond_2

    .line 108
    sget-object v2, Lcom/wf/wellsfargomobile/mrdc/ImageMode;->FRONT:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->imageMode:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    .line 114
    .end local v0           #flag:Ljava/io/Serializable;
    :goto_0
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/wf/wellsfargomobile/WFApp;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    .line 116
    sget-object v2, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$7;->$SwitchMap$com$wf$wellsfargomobile$mrdc$ImageMode:[I

    iget-object v3, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->imageMode:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    invoke-virtual {v3}, Lcom/wf/wellsfargomobile/mrdc/ImageMode;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 149
    :cond_0
    :goto_1
    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-virtual {v2}, Lcom/wf/wellsfargomobile/WFApp;->isErrorOpeningCamera()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 150
    new-instance v2, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$2;

    invoke-direct {v2, p0}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$2;-><init>(Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;)V

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 167
    :cond_1
    const v2, 0x7f060007

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->takePhotoButton:Landroid/widget/ImageButton;

    .line 168
    return-void

    .line 110
    .restart local v0       #flag:Ljava/io/Serializable;
    :cond_2
    check-cast v0, Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    .end local v0           #flag:Ljava/io/Serializable;
    iput-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->imageMode:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    goto :goto_0

    .line 118
    :pswitch_0
    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-virtual {v2}, Lcom/wf/wellsfargomobile/WFApp;->isErrorOpeningCamera()Z

    move-result v2

    if-nez v2, :cond_0

    .line 119
    new-instance v2, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$1;

    invoke-direct {v2, p0}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$1;-><init>(Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;)V

    invoke-virtual {p0, v2}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 140
    :pswitch_1
    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->instruction:Landroid/widget/TextView;

    const v3, 0x7f070063

    invoke-virtual {p0, v3}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 116
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .parameter "keyCode"
    .parameter "event"

    .prologue
    const/4 v0, 0x1

    .line 795
    packed-switch p1, :pswitch_data_0

    .line 807
    :pswitch_0
    invoke-super {p0, p1, p2}, Lcom/wf/wellsfargomobile/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 797
    :pswitch_1
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->takePhoto()V

    goto :goto_0

    .line 801
    :pswitch_2
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->setTorch(Z)V

    goto :goto_0

    .line 804
    :pswitch_3
    invoke-direct {p0, v0}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->setTorch(Z)V

    goto :goto_0

    .line 795
    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 776
    invoke-super {p0}, Lcom/wf/wellsfargomobile/BaseActivity;->onResume()V

    .line 778
    iget-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->takePhotoButton:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 781
    iget-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->sensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 784
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 10
    .parameter "sensorEvent"

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v2, 0x0

    const-wide/high16 v6, 0x4024

    const-wide/high16 v4, -0x3fdc

    .line 726
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    .line 727
    .local v0, pitch:Ljava/lang/Float;
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    .line 729
    .local v1, roll:Ljava/lang/Float;
    iget-object v2, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v2}, Landroid/hardware/Sensor;->getType()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    .line 731
    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v2, v2, v8

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    .line 733
    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v2, v2, v9

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    .line 742
    :cond_0
    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->orientationWarning:Landroid/widget/ImageView;

    if-eqz v2, :cond_2

    .line 743
    monitor-enter p0

    .line 744
    :try_start_0
    iget-object v2, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v2}, Landroid/hardware/Sensor;->getType()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    .line 746
    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    .line 748
    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    .line 751
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 752
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v2

    float-to-double v2, v2

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_3

    .line 753
    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->orientationWarning:Landroid/widget/ImageView;

    const v3, 0x7f020027

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 769
    :cond_2
    :goto_0
    return-void

    .line 751
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 755
    :cond_3
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v2

    float-to-double v2, v2

    cmpl-double v2, v2, v6

    if-ltz v2, :cond_4

    .line 756
    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->orientationWarning:Landroid/widget/ImageView;

    const v3, 0x7f020028

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 758
    :cond_4
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v2

    float-to-double v2, v2

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_5

    .line 759
    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->orientationWarning:Landroid/widget/ImageView;

    const v3, 0x7f02002a

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 761
    :cond_5
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v2

    float-to-double v2, v2

    cmpl-double v2, v2, v6

    if-ltz v2, :cond_6

    .line 762
    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->orientationWarning:Landroid/widget/ImageView;

    const v3, 0x7f020029

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 765
    :cond_6
    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->orientationWarning:Landroid/widget/ImageView;

    const v3, 0x7f020007

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 817
    invoke-super {p0}, Lcom/wf/wellsfargomobile/BaseActivity;->onStart()V

    .line 822
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 789
    iget-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->sensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 790
    invoke-super {p0}, Lcom/wf/wellsfargomobile/BaseActivity;->onStop()V

    .line 791
    return-void
.end method

.method public onTakePhoto(Landroid/view/View;)V
    .locals 0
    .parameter "v"

    .prologue
    .line 713
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->takePhoto()V

    .line 714
    return-void
.end method

.method public openPhotoTips(Landroid/view/View;)V
    .locals 3
    .parameter "v"

    .prologue
    .line 669
    new-instance v0, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-direct {v0, v1, v2}, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;-><init>(Landroid/content/Context;Lcom/wf/wellsfargomobile/WFApp;)V

    iget-object v1, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-virtual {v1}, Lcom/wf/wellsfargomobile/WFApp;->getNonce()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;->openPhotoTips(Ljava/lang/String;)V

    .line 670
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 10
    .parameter "arg0"
    .parameter "format"
    .parameter "width"
    .parameter "height"

    .prologue
    const/4 v3, 0x0

    .line 303
    iget-boolean v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->previewing:Z

    if-eqz v2, :cond_0

    .line 304
    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->camera:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->stopPreview()V

    .line 305
    iput-boolean v3, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->previewing:Z

    .line 308
    :cond_0
    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->camera:Landroid/hardware/Camera;

    if-eqz v2, :cond_5

    .line 310
    :try_start_0
    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->camera:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v9

    .line 318
    .local v9, parameters:Landroid/hardware/Camera$Parameters;
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    .line 319
    .local v7, display:Landroid/view/Display;
    invoke-virtual {v7}, Landroid/view/Display;->getHeight()I

    move-result v2

    invoke-virtual {v7}, Landroid/view/Display;->getWidth()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 320
    .local v0, targetHeight:I
    invoke-virtual {v7}, Landroid/view/Display;->getHeight()I

    move-result v2

    invoke-virtual {v7}, Landroid/view/Display;->getWidth()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 332
    .local v1, targetWidth:I
    const/16 v2, 0x640

    invoke-direct {p0, v2, v9}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->getBestSupportedSize(ILandroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Size;

    move-result-object v6

    .line 333
    .local v6, bestSupportedSize:Landroid/hardware/Camera$Size;
    if-eqz v6, :cond_1

    .line 334
    iget v2, v6, Landroid/hardware/Camera$Size;->width:I

    iget v3, v6, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v9, v2, v3}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    .line 339
    :cond_1
    if-eqz v6, :cond_2

    .line 340
    iget v2, v6, Landroid/hardware/Camera$Size;->height:I

    iget v3, v6, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v9}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v9}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->getOptimumSupportedPreviewSize(IIIILjava/util/List;Landroid/hardware/Camera$Size;)Landroid/hardware/Camera$Size;

    move-result-object v2

    iput-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->previewSize:Landroid/hardware/Camera$Size;

    .line 348
    :cond_2
    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->previewSize:Landroid/hardware/Camera$Size;

    if-eqz v2, :cond_3

    .line 349
    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->previewSize:Landroid/hardware/Camera$Size;

    iget v2, v2, Landroid/hardware/Camera$Size;->width:I

    iget-object v3, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->previewSize:Landroid/hardware/Camera$Size;

    iget v3, v3, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v9, v2, v3}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 352
    :cond_3
    const/16 v2, 0x64

    invoke-virtual {v9, v2}, Landroid/hardware/Camera$Parameters;->setJpegQuality(I)V

    .line 354
    const/16 v2, 0x100

    invoke-virtual {v9, v2}, Landroid/hardware/Camera$Parameters;->setPictureFormat(I)V

    .line 357
    invoke-virtual {v9}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "continuous-picture"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "auto"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "macro"

    aput-object v5, v3, v4

    invoke-direct {p0, v2, v3}, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->findSettableValue(Ljava/util/Collection;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 361
    .local v8, focusMode:Ljava/lang/String;
    if-eqz v8, :cond_4

    .line 362
    invoke-virtual {v9, v8}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    .line 368
    :cond_4
    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->camera:Landroid/hardware/Camera;

    invoke-virtual {v2, v9}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 370
    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->camera:Landroid/hardware/Camera;

    iget-object v3, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->surfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v2, v3}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    .line 372
    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->camera:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->startPreview()V

    .line 373
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->previewing:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 379
    .end local v0           #targetHeight:I
    .end local v1           #targetWidth:I
    .end local v6           #bestSupportedSize:Landroid/hardware/Camera$Size;
    .end local v7           #display:Landroid/view/Display;
    .end local v8           #focusMode:Ljava/lang/String;
    .end local v9           #parameters:Landroid/hardware/Camera$Parameters;
    :cond_5
    :goto_0
    return-void

    .line 375
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public surfaceClick(Landroid/view/View;)V
    .locals 2
    .parameter "v"

    .prologue
    .line 811
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->isPictureTaking:Z

    .line 812
    iget-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->camera:Landroid/hardware/Camera;

    iget-object v1, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->autoFocusCallBack:Lcom/wf/wellsfargomobile/mrdc/CapturePhoto$AutoFocusCallBack;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    .line 813
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 3
    .parameter "holder"

    .prologue
    .line 639
    :try_start_0
    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;

    move-result-object v1

    iput-object v1, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->camera:Landroid/hardware/Camera;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 647
    :goto_0
    return-void

    .line 640
    :catch_0
    move-exception v0

    .line 644
    .local v0, re:Ljava/lang/RuntimeException;
    iget-object v1, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/wf/wellsfargomobile/WFApp;->setErrorOpeningCamera(Z)V

    goto :goto_0
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .parameter "holder"

    .prologue
    const/4 v1, 0x0

    .line 654
    iget-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->camera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 655
    iget-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 656
    iget-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 657
    iget-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 659
    :cond_0
    iput-object v1, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->camera:Landroid/hardware/Camera;

    .line 660
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;->previewing:Z

    .line 661
    return-void
.end method
