.class public final enum Lcom/wf/wellsfargomobile/mrdc/ImageMode;
.super Ljava/lang/Enum;
.source "ImageMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/wf/wellsfargomobile/mrdc/ImageMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/wf/wellsfargomobile/mrdc/ImageMode;

.field public static final enum FRONT:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

.field public static final enum REAR:Lcom/wf/wellsfargomobile/mrdc/ImageMode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4
    new-instance v0, Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    const-string v1, "FRONT"

    invoke-direct {v0, v1, v2}, Lcom/wf/wellsfargomobile/mrdc/ImageMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/wf/wellsfargomobile/mrdc/ImageMode;->FRONT:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    .line 5
    new-instance v0, Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    const-string v1, "REAR"

    invoke-direct {v0, v1, v3}, Lcom/wf/wellsfargomobile/mrdc/ImageMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/wf/wellsfargomobile/mrdc/ImageMode;->REAR:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    .line 3
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    sget-object v1, Lcom/wf/wellsfargomobile/mrdc/ImageMode;->FRONT:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/wf/wellsfargomobile/mrdc/ImageMode;->REAR:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/wf/wellsfargomobile/mrdc/ImageMode;->$VALUES:[Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/wf/wellsfargomobile/mrdc/ImageMode;
    .locals 1
    .parameter "name"

    .prologue
    .line 3
    const-class v0, Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    return-object v0
.end method

.method public static values()[Lcom/wf/wellsfargomobile/mrdc/ImageMode;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/wf/wellsfargomobile/mrdc/ImageMode;->$VALUES:[Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    invoke-virtual {v0}, [Lcom/wf/wellsfargomobile/mrdc/ImageMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    return-object v0
.end method
