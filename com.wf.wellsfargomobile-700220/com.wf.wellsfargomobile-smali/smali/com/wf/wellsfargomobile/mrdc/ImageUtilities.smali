.class public Lcom/wf/wellsfargomobile/mrdc/ImageUtilities;
.super Ljava/lang/Object;
.source "ImageUtilities.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ImageUtilities"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static bitmapToByteArray(Landroid/graphics/Bitmap;I)[B
    .locals 3
    .parameter "bitmap"
    .parameter "compression"

    .prologue
    .line 85
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 86
    .local v0, baos:Ljava/io/ByteArrayOutputStream;
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {p0, v2, p1, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 87
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 88
    .local v1, byteArrayImage:[B
    return-object v1
.end method

.method public static resizeImage(Landroid/graphics/Bitmap;II)[B
    .locals 11
    .parameter "eI"
    .parameter "targetLength"
    .parameter "compression"

    .prologue
    const/4 v1, 0x0

    .line 59
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 60
    .local v3, oldWidth:I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 61
    .local v4, oldHeight:I
    if-le v3, v4, :cond_0

    move v7, v3

    .line 62
    .local v7, longLength:I
    :goto_0
    int-to-float v0, v7

    int-to-float v2, p1

    div-float v10, v0, v2

    .line 64
    .local v10, scaleFactor:F
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 65
    .local v5, matrix:Landroid/graphics/Matrix;
    invoke-virtual {v5, v10, v10}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 67
    const/4 v6, 0x1

    move-object v0, p0

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 70
    .local v8, resizedImage:Landroid/graphics/Bitmap;
    invoke-static {v8, p2}, Lcom/wf/wellsfargomobile/mrdc/ImageUtilities;->bitmapToByteArray(Landroid/graphics/Bitmap;I)[B

    move-result-object v9

    .line 72
    .local v9, retv:[B
    return-object v9

    .end local v5           #matrix:Landroid/graphics/Matrix;
    .end local v7           #longLength:I
    .end local v8           #resizedImage:Landroid/graphics/Bitmap;
    .end local v9           #retv:[B
    .end local v10           #scaleFactor:F
    :cond_0
    move v7, v4

    .line 61
    goto :goto_0
.end method

.method public static resizeImage(Landroid/graphics/Bitmap;Z)[B
    .locals 3
    .parameter "imageData"
    .parameter "front"

    .prologue
    const/16 v2, 0x640

    const/16 v1, 0x32

    .line 35
    const/4 v0, 0x0

    .line 36
    .local v0, compressedImage:[B
    if-eqz p1, :cond_0

    .line 37
    invoke-static {p0, v2, v1}, Lcom/wf/wellsfargomobile/mrdc/ImageUtilities;->resizeImage(Landroid/graphics/Bitmap;II)[B

    move-result-object v0

    .line 45
    :goto_0
    return-object v0

    .line 41
    :cond_0
    invoke-static {p0, v2, v1}, Lcom/wf/wellsfargomobile/mrdc/ImageUtilities;->resizeImage(Landroid/graphics/Bitmap;II)[B

    move-result-object v0

    goto :goto_0
.end method

.method public static resizeImageForHeight(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 8
    .parameter "bitmapPicture"
    .parameter "desiredHeight"

    .prologue
    .line 127
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    if-eq v4, p1, :cond_0

    .line 130
    int-to-double v4, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-double v6, v6

    div-double v2, v4, v6

    .line 131
    .local v2, scale:D
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-double v4, v4

    mul-double/2addr v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v0, v4

    .line 133
    .local v0, desiredWidth:I
    const-string v4, "ImageUtilities"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "scale to size - scale: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " height: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " width: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    const/4 v4, 0x0

    :try_start_0
    invoke-static {p0, v0, p1, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    .line 146
    .end local v0           #desiredWidth:I
    .end local v2           #scale:D
    :cond_0
    :goto_0
    return-object p0

    .line 141
    .restart local v0       #desiredWidth:I
    .restart local v2       #scale:D
    :catch_0
    move-exception v1

    .line 143
    .local v1, oome:Ljava/lang/OutOfMemoryError;
    const-string v4, "ImageUtilities"

    const-string v5, "OutOfMemoryError occured while resizing image"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static resizeImageForWidth(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 8
    .parameter "bitmapPicture"
    .parameter "desiredWidth"

    .prologue
    .line 98
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    if-eq v4, p1, :cond_0

    .line 101
    int-to-double v4, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-double v6, v6

    div-double v2, v4, v6

    .line 102
    .local v2, scale:D
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-double v4, v4

    mul-double/2addr v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v0, v4

    .line 104
    .local v0, desiredHeight:I
    const-string v4, "ImageUtilities"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "scale to size - scale: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " height: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " width: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    const/4 v4, 0x0

    :try_start_0
    invoke-static {p0, p1, v0, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    .line 117
    .end local v0           #desiredHeight:I
    .end local v2           #scale:D
    :cond_0
    :goto_0
    return-object p0

    .line 112
    .restart local v0       #desiredHeight:I
    .restart local v2       #scale:D
    :catch_0
    move-exception v1

    .line 114
    .local v1, oome:Ljava/lang/OutOfMemoryError;
    const-string v4, "ImageUtilities"

    const-string v5, "OutOfMemoryError occured while resizing image"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
