.class public Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;
.super Ljava/lang/Object;
.source "JavaScriptInterfacePhotoTips.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "JavaScriptInterfacePhotoTips"


# instance fields
.field private d:Landroid/app/Dialog;

.field private final mContext:Landroid/content/Context;

.field private wfApp:Lcom/wf/wellsfargomobile/WFApp;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/wf/wellsfargomobile/WFApp;)V
    .locals 0
    .parameter "c"
    .parameter "wfApp"

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;->mContext:Landroid/content/Context;

    .line 36
    iput-object p2, p0, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    .line 37
    return-void
.end method

.method static synthetic access$000(Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;)Lcom/wf/wellsfargomobile/WFApp;
    .locals 1
    .parameter "x0"

    .prologue
    .line 23
    iget-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    return-object v0
.end method

.method private validNonce(Ljava/lang/String;)Z
    .locals 2
    .parameter "nonce"

    .prologue
    const/4 v0, 0x0

    .line 120
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-virtual {v1}, Lcom/wf/wellsfargomobile/WFApp;->getNonce()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 126
    :cond_0
    :goto_0
    return v0

    .line 123
    :cond_1
    iget-object v1, p0, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-virtual {v1}, Lcom/wf/wellsfargomobile/WFApp;->getNonce()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 124
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public closeDialog(Landroid/view/View;)V
    .locals 1
    .parameter "v"

    .prologue
    .line 85
    iget-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-virtual {v0}, Lcom/wf/wellsfargomobile/WFApp;->getNonce()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;->closeDialog(Ljava/lang/String;)V

    .line 86
    return-void
.end method

.method public closeDialog(Ljava/lang/String;)V
    .locals 3
    .parameter "nonce"

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;->validNonce(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 91
    const-string v0, "JavaScriptInterfacePhotoTips"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "closeDialog() - nonce not valid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    :goto_0
    return-void

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;->d:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    goto :goto_0
.end method

.method public closeDialogDontShow(Landroid/view/View;)V
    .locals 1
    .parameter "V"

    .prologue
    .line 99
    iget-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-virtual {v0}, Lcom/wf/wellsfargomobile/WFApp;->getNonce()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;->closeDialogDontShow(Ljava/lang/String;)V

    .line 100
    return-void
.end method

.method public closeDialogDontShow(Ljava/lang/String;)V
    .locals 5
    .parameter "nonce"

    .prologue
    .line 103
    invoke-direct {p0, p1}, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;->validNonce(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 104
    const-string v2, "JavaScriptInterfacePhotoTips"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "closeDialogDontShow() - nonce not valid: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    :goto_0
    return-void

    .line 109
    :cond_0
    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;->mContext:Landroid/content/Context;

    const-string v3, "WF_PREFERENCES"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 111
    .local v1, sp:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 113
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "PHOTO_TIPS_DONT_SHOW"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 114
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 116
    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;->d:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->dismiss()V

    goto :goto_0
.end method

.method public openPhotoTips(Ljava/lang/String;)V
    .locals 1
    .parameter "nonce"

    .prologue
    .line 40
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;->openPhotoTips(Ljava/lang/String;Z)V

    .line 41
    return-void
.end method

.method public openPhotoTips(Ljava/lang/String;Z)V
    .locals 7
    .parameter "nonce"
    .parameter "suppressButton"

    .prologue
    const/4 v6, 0x4

    .line 44
    invoke-direct {p0, p1}, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;->validNonce(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 45
    const-string v3, "JavaScriptInterfacePhotoTips"

    const-string v4, "openPhotoTips() - nonce not valid"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    :goto_0
    return-void

    .line 49
    :cond_0
    new-instance v3, Landroid/app/Dialog;

    iget-object v4, p0, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;->mContext:Landroid/content/Context;

    const v5, 0x7f090001

    invoke-direct {v3, v4, v5}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iput-object v3, p0, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;->d:Landroid/app/Dialog;

    .line 52
    iget-object v3, p0, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;->d:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 53
    .local v2, window:Landroid/view/Window;
    invoke-virtual {v2, v6, v6}, Landroid/view/Window;->setFlags(II)V

    .line 57
    iget-object v3, p0, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;->d:Landroid/app/Dialog;

    iget-object v4, p0, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;->mContext:Landroid/content/Context;

    const v5, 0x7f07004d

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 59
    iget-object v3, p0, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;->d:Landroid/app/Dialog;

    const v4, 0x7f030007

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->setContentView(I)V

    .line 61
    iget-object v3, p0, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;->d:Landroid/app/Dialog;

    const v4, 0x7f060063

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 62
    .local v1, closeButton:Landroid/widget/Button;
    new-instance v3, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips$1;

    invoke-direct {v3, p0}, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips$1;-><init>(Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    iget-object v3, p0, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;->d:Landroid/app/Dialog;

    const v4, 0x7f060062

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 70
    .local v0, b:Landroid/widget/Button;
    if-eqz p2, :cond_1

    .line 71
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 81
    :goto_1
    iget-object v3, p0, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;->d:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->show()V

    goto :goto_0

    .line 73
    :cond_1
    new-instance v3, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips$2;

    invoke-direct {v3, p0}, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips$2;-><init>(Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method
