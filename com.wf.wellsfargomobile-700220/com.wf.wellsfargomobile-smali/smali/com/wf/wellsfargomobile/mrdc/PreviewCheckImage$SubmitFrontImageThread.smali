.class Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;
.super Landroid/os/AsyncTask;
.source "PreviewCheckImage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SubmitFrontImageThread"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private TAG:Ljava/lang/String;

.field private aContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;


# direct methods
.method public constructor <init>(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;Landroid/content/Context;)V
    .locals 1
    .parameter
    .parameter "aContext"

    .prologue
    .line 206
    iput-object p1, p0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 203
    const-string v0, "SubmitFrontImageThread"

    iput-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->TAG:Ljava/lang/String;

    .line 207
    iput-object p2, p0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->aContext:Landroid/content/Context;

    .line 208
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 202
    check-cast p1, [Ljava/lang/Void;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 24
    .parameter "asyncParams"

    .prologue
    .line 214
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    move-object/from16 v20, v0

    #getter for: Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v20 .. v20}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v20

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v21

    invoke-virtual/range {v20 .. v22}, Lcom/wf/wellsfargomobile/WFApp;->setPerfD1FrontImageStartSend(J)V

    .line 216
    const/16 v19, 0x1

    .line 217
    .local v19, uploadSucceded:Z
    const/4 v3, 0x0

    .line 219
    .local v3, client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    move-object/from16 v20, v0

    #getter for: Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v20 .. v20}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/wf/wellsfargomobile/WFApp;->getFrontCheckImage()[B

    move-result-object v11

    .line 222
    .local v11, image:[B
    if-nez v11, :cond_1

    .line 224
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->TAG:Ljava/lang/String;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "front image was null - image: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    const/16 v19, 0x0

    .line 227
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    move-object/from16 v20, v0

    #getter for: Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v20 .. v20}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v20

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Lcom/wf/wellsfargomobile/WFApp;->setFrontImageToken(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5

    .line 229
    const/16 v20, 0x0

    .line 317
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v21

    if-eqz v21, :cond_0

    .line 318
    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 321
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    move-object/from16 v21, v0

    #getter for: Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v21 .. v21}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v21

    const/16 v22, 0x1

    invoke-virtual/range {v21 .. v22}, Lcom/wf/wellsfargomobile/WFApp;->setFrontImageUploadProcessComplete(Z)V

    .line 323
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    move-object/from16 v21, v0

    #getter for: Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v21 .. v21}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v21

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Lcom/wf/wellsfargomobile/WFApp;->setFrontCheckImage([B)V

    .line 324
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    move-object/from16 v21, v0

    #getter for: Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v21 .. v21}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v21

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Lcom/wf/wellsfargomobile/WFApp;->setFrontCheckPreviewImage(Landroid/graphics/Bitmap;)V

    .line 331
    .end local v11           #image:[B
    :goto_0
    return-object v20

    .line 233
    .restart local v11       #image:[B
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->aContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 234
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v6

    .line 235
    .local v6, cookieSyncManager:Landroid/webkit/CookieSyncManager;
    invoke-virtual {v6}, Landroid/webkit/CookieSyncManager;->sync()V

    .line 237
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v5

    .line 238
    .local v5, cookieManager:Landroid/webkit/CookieManager;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->aContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x7f070001

    invoke-virtual/range {v20 .. v21}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/webkit/CookieManager;->getCookie(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 240
    .local v7, cookies:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->TAG:Ljava/lang/String;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "cookies: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    new-instance v4, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v4}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    .line 244
    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .local v4, client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :try_start_2
    invoke-virtual {v4}, Lorg/apache/http/impl/client/DefaultHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v10

    .line 245
    .local v10, httpParams:Lorg/apache/http/params/HttpParams;
    const-string v20, "http.socket.timeout"

    const-wide/32 v21, 0x493e0

    move-object/from16 v0, v20

    move-wide/from16 v1, v21

    invoke-interface {v10, v0, v1, v2}, Lorg/apache/http/params/HttpParams;->getLongParameter(Ljava/lang/String;J)J

    .line 246
    const-string v20, "http.connection.timeout"

    const-wide/32 v21, 0x493e0

    move-object/from16 v0, v20

    move-wide/from16 v1, v21

    invoke-interface {v10, v0, v1, v2}, Lorg/apache/http/params/HttpParams;->getLongParameter(Ljava/lang/String;J)J

    .line 247
    invoke-virtual {v4, v10}, Lorg/apache/http/impl/client/DefaultHttpClient;->setParams(Lorg/apache/http/params/HttpParams;)V

    .line 249
    new-instance v14, Lorg/apache/http/client/methods/HttpPost;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->aContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f070001

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/deposit/submitFrontImage.action"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v14, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 253
    .local v14, request:Lorg/apache/http/client/methods/HttpPost;
    const-string v20, "Cookie"

    move-object/from16 v0, v20

    invoke-virtual {v14, v0, v7}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->TAG:Ljava/lang/String;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "image size: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    array-length v0, v11

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " bytes"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    new-instance v13, Lorg/apache/http/entity/mime/MultipartEntity;

    invoke-direct {v13}, Lorg/apache/http/entity/mime/MultipartEntity;-><init>()V

    .line 258
    .local v13, mp:Lorg/apache/http/entity/mime/MultipartEntity;
    const-string v20, "frontImage"

    new-instance v21, Lorg/apache/http/entity/mime/content/ByteArrayBody;

    const-string v22, "image/jpeg"

    const-string v23, "front.jpg"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-direct {v0, v11, v1, v2}, Lorg/apache/http/entity/mime/content/ByteArrayBody;-><init>([BLjava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v13, v0, v1}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 259
    const-string v20, "WFAppId"

    new-instance v21, Lorg/apache/http/entity/mime/content/StringBody;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->aContext:Landroid/content/Context;

    move-object/from16 v22, v0

    const v23, 0x7f070003

    invoke-virtual/range {v22 .. v23}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v22

    const-string v23, "UTF-8"

    invoke-static/range {v23 .. v23}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v23

    invoke-direct/range {v21 .. v23}, Lorg/apache/http/entity/mime/content/StringBody;-><init>(Ljava/lang/String;Ljava/nio/charset/Charset;)V

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v13, v0, v1}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 263
    invoke-virtual {v14, v13}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 265
    invoke-virtual {v4, v14}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v15

    .line 267
    .local v15, response:Lorg/apache/http/HttpResponse;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    move-object/from16 v20, v0

    #getter for: Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v20 .. v20}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v20

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v21

    invoke-virtual/range {v20 .. v22}, Lcom/wf/wellsfargomobile/WFApp;->setPerfD2FrontImageJsonResponse(J)V

    .line 269
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    move-object/from16 v20, v0

    #getter for: Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v20 .. v20}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v20

    const/16 v21, 0x1

    invoke-virtual/range {v20 .. v21}, Lcom/wf/wellsfargomobile/WFApp;->setFrontImageUploadProcessComplete(Z)V

    .line 271
    invoke-interface {v15}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v16

    .line 273
    .local v16, responseEntity:Lorg/apache/http/HttpEntity;
    invoke-static/range {v16 .. v16}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v17

    .line 275
    .local v17, responseEntityStr:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->TAG:Ljava/lang/String;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "responseEntity: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    const-string v20, "/*--safejson--"

    const-string v21, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v17

    .line 279
    const-string v20, "--safejson--*/"

    const-string v21, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v17

    .line 280
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->TAG:Ljava/lang/String;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "responseEntity: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_2 .. :try_end_2} :catch_a
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_8
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6

    .line 283
    :try_start_3
    new-instance v12, Lorg/json/JSONObject;

    move-object/from16 v0, v17

    invoke-direct {v12, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 285
    .local v12, jsonResponse:Lorg/json/JSONObject;
    const-string v20, "status"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 286
    .local v18, status:Ljava/lang/String;
    const-string v20, "Failed"

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_3 .. :try_end_3} :catch_a
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_3 .. :try_end_3} :catch_9
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6

    move-result v20

    if-eqz v20, :cond_4

    .line 287
    const/16 v19, 0x0

    .line 317
    .end local v12           #jsonResponse:Lorg/json/JSONObject;
    .end local v18           #status:Ljava/lang/String;
    :goto_1
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v20

    if-eqz v20, :cond_2

    .line 318
    invoke-virtual {v4}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 321
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    move-object/from16 v20, v0

    #getter for: Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v20 .. v20}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v20

    const/16 v21, 0x1

    invoke-virtual/range {v20 .. v21}, Lcom/wf/wellsfargomobile/WFApp;->setFrontImageUploadProcessComplete(Z)V

    .line 323
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    move-object/from16 v20, v0

    #getter for: Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v20 .. v20}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v20

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Lcom/wf/wellsfargomobile/WFApp;->setFrontCheckImage([B)V

    .line 324
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    move-object/from16 v20, v0

    #getter for: Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v20 .. v20}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v20

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Lcom/wf/wellsfargomobile/WFApp;->setFrontCheckPreviewImage(Landroid/graphics/Bitmap;)V

    move-object v3, v4

    .line 327
    .end local v4           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .end local v5           #cookieManager:Landroid/webkit/CookieManager;
    .end local v6           #cookieSyncManager:Landroid/webkit/CookieSyncManager;
    .end local v7           #cookies:Ljava/lang/String;
    .end local v10           #httpParams:Lorg/apache/http/params/HttpParams;
    .end local v11           #image:[B
    .end local v13           #mp:Lorg/apache/http/entity/mime/MultipartEntity;
    .end local v14           #request:Lorg/apache/http/client/methods/HttpPost;
    .end local v15           #response:Lorg/apache/http/HttpResponse;
    .end local v16           #responseEntity:Lorg/apache/http/HttpEntity;
    .end local v17           #responseEntityStr:Ljava/lang/String;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :goto_2
    if-nez v19, :cond_3

    .line 328
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->TAG:Ljava/lang/String;

    move-object/from16 v20, v0

    const-string v21, "Front image failed to upload."

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    :cond_3
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 289
    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v4       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v5       #cookieManager:Landroid/webkit/CookieManager;
    .restart local v6       #cookieSyncManager:Landroid/webkit/CookieSyncManager;
    .restart local v7       #cookies:Ljava/lang/String;
    .restart local v10       #httpParams:Lorg/apache/http/params/HttpParams;
    .restart local v11       #image:[B
    .restart local v12       #jsonResponse:Lorg/json/JSONObject;
    .restart local v13       #mp:Lorg/apache/http/entity/mime/MultipartEntity;
    .restart local v14       #request:Lorg/apache/http/client/methods/HttpPost;
    .restart local v15       #response:Lorg/apache/http/HttpResponse;
    .restart local v16       #responseEntity:Lorg/apache/http/HttpEntity;
    .restart local v17       #responseEntityStr:Ljava/lang/String;
    .restart local v18       #status:Ljava/lang/String;
    :cond_4
    :try_start_4
    const-string v20, "frontImageId"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 290
    .local v9, frontImageId:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    move-object/from16 v20, v0

    #getter for: Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v20 .. v20}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Lcom/wf/wellsfargomobile/WFApp;->setFrontImageToken(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_4 .. :try_end_4} :catch_a
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_4 .. :try_end_4} :catch_9
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_4 .. :try_end_4} :catch_8
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_4 .. :try_end_4} :catch_7
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6

    goto :goto_1

    .line 292
    .end local v9           #frontImageId:Ljava/lang/String;
    .end local v12           #jsonResponse:Lorg/json/JSONObject;
    .end local v18           #status:Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 293
    .local v8, e:Lorg/json/JSONException;
    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->TAG:Ljava/lang/String;

    move-object/from16 v20, v0

    const-string v21, "JSON parsing problem"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v0, v1, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1
    .catch Ljava/nio/charset/IllegalCharsetNameException; {:try_start_5 .. :try_end_5} :catch_a
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_5 .. :try_end_5} :catch_9
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_5 .. :try_end_5} :catch_8
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_5 .. :try_end_5} :catch_7
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6

    .line 294
    const/16 v19, 0x0

    goto :goto_1

    .line 300
    .end local v4           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .end local v5           #cookieManager:Landroid/webkit/CookieManager;
    .end local v6           #cookieSyncManager:Landroid/webkit/CookieSyncManager;
    .end local v7           #cookies:Ljava/lang/String;
    .end local v8           #e:Lorg/json/JSONException;
    .end local v10           #httpParams:Lorg/apache/http/params/HttpParams;
    .end local v11           #image:[B
    .end local v13           #mp:Lorg/apache/http/entity/mime/MultipartEntity;
    .end local v14           #request:Lorg/apache/http/client/methods/HttpPost;
    .end local v15           #response:Lorg/apache/http/HttpResponse;
    .end local v16           #responseEntity:Lorg/apache/http/HttpEntity;
    .end local v17           #responseEntityStr:Ljava/lang/String;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :catch_1
    move-exception v8

    .line 301
    .local v8, e:Ljava/nio/charset/IllegalCharsetNameException;
    :goto_3
    const/16 v19, 0x0

    .line 317
    if-eqz v3, :cond_5

    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v20

    if-eqz v20, :cond_5

    .line 318
    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 321
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    move-object/from16 v20, v0

    #getter for: Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v20 .. v20}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v20

    const/16 v21, 0x1

    invoke-virtual/range {v20 .. v21}, Lcom/wf/wellsfargomobile/WFApp;->setFrontImageUploadProcessComplete(Z)V

    .line 323
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    move-object/from16 v20, v0

    #getter for: Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v20 .. v20}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v20

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Lcom/wf/wellsfargomobile/WFApp;->setFrontCheckImage([B)V

    .line 324
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    move-object/from16 v20, v0

    #getter for: Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v20 .. v20}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v20

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Lcom/wf/wellsfargomobile/WFApp;->setFrontCheckPreviewImage(Landroid/graphics/Bitmap;)V

    goto :goto_2

    .line 303
    .end local v8           #e:Ljava/nio/charset/IllegalCharsetNameException;
    :catch_2
    move-exception v8

    .line 304
    .local v8, e:Ljava/nio/charset/UnsupportedCharsetException;
    :goto_4
    const/16 v19, 0x0

    .line 317
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v20

    if-eqz v20, :cond_6

    .line 318
    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 321
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    move-object/from16 v20, v0

    #getter for: Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v20 .. v20}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v20

    const/16 v21, 0x1

    invoke-virtual/range {v20 .. v21}, Lcom/wf/wellsfargomobile/WFApp;->setFrontImageUploadProcessComplete(Z)V

    .line 323
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    move-object/from16 v20, v0

    #getter for: Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v20 .. v20}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v20

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Lcom/wf/wellsfargomobile/WFApp;->setFrontCheckImage([B)V

    .line 324
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    move-object/from16 v20, v0

    #getter for: Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v20 .. v20}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v20

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Lcom/wf/wellsfargomobile/WFApp;->setFrontCheckPreviewImage(Landroid/graphics/Bitmap;)V

    goto/16 :goto_2

    .line 306
    .end local v8           #e:Ljava/nio/charset/UnsupportedCharsetException;
    :catch_3
    move-exception v8

    .line 307
    .local v8, e:Ljava/io/UnsupportedEncodingException;
    :goto_5
    const/16 v19, 0x0

    .line 317
    if-eqz v3, :cond_7

    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v20

    if-eqz v20, :cond_7

    .line 318
    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 321
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    move-object/from16 v20, v0

    #getter for: Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v20 .. v20}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v20

    const/16 v21, 0x1

    invoke-virtual/range {v20 .. v21}, Lcom/wf/wellsfargomobile/WFApp;->setFrontImageUploadProcessComplete(Z)V

    .line 323
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    move-object/from16 v20, v0

    #getter for: Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v20 .. v20}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v20

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Lcom/wf/wellsfargomobile/WFApp;->setFrontCheckImage([B)V

    .line 324
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    move-object/from16 v20, v0

    #getter for: Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v20 .. v20}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v20

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Lcom/wf/wellsfargomobile/WFApp;->setFrontCheckPreviewImage(Landroid/graphics/Bitmap;)V

    goto/16 :goto_2

    .line 309
    .end local v8           #e:Ljava/io/UnsupportedEncodingException;
    :catch_4
    move-exception v8

    .line 310
    .local v8, e:Lorg/apache/http/client/ClientProtocolException;
    :goto_6
    const/16 v19, 0x0

    .line 317
    if-eqz v3, :cond_8

    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v20

    if-eqz v20, :cond_8

    .line 318
    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 321
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    move-object/from16 v20, v0

    #getter for: Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v20 .. v20}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v20

    const/16 v21, 0x1

    invoke-virtual/range {v20 .. v21}, Lcom/wf/wellsfargomobile/WFApp;->setFrontImageUploadProcessComplete(Z)V

    .line 323
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    move-object/from16 v20, v0

    #getter for: Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v20 .. v20}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v20

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Lcom/wf/wellsfargomobile/WFApp;->setFrontCheckImage([B)V

    .line 324
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    move-object/from16 v20, v0

    #getter for: Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v20 .. v20}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v20

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Lcom/wf/wellsfargomobile/WFApp;->setFrontCheckPreviewImage(Landroid/graphics/Bitmap;)V

    goto/16 :goto_2

    .line 312
    .end local v8           #e:Lorg/apache/http/client/ClientProtocolException;
    :catch_5
    move-exception v8

    .line 313
    .local v8, e:Ljava/io/IOException;
    :goto_7
    const/16 v19, 0x0

    .line 317
    if-eqz v3, :cond_9

    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v20

    if-eqz v20, :cond_9

    .line 318
    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 321
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    move-object/from16 v20, v0

    #getter for: Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v20 .. v20}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v20

    const/16 v21, 0x1

    invoke-virtual/range {v20 .. v21}, Lcom/wf/wellsfargomobile/WFApp;->setFrontImageUploadProcessComplete(Z)V

    .line 323
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    move-object/from16 v20, v0

    #getter for: Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v20 .. v20}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v20

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Lcom/wf/wellsfargomobile/WFApp;->setFrontCheckImage([B)V

    .line 324
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    move-object/from16 v20, v0

    #getter for: Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v20 .. v20}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v20

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Lcom/wf/wellsfargomobile/WFApp;->setFrontCheckPreviewImage(Landroid/graphics/Bitmap;)V

    goto/16 :goto_2

    .line 317
    .end local v8           #e:Ljava/io/IOException;
    :catchall_0
    move-exception v20

    :goto_8
    if-eqz v3, :cond_a

    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v21

    if-eqz v21, :cond_a

    .line 318
    invoke-virtual {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 321
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    move-object/from16 v21, v0

    #getter for: Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v21 .. v21}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v21

    const/16 v22, 0x1

    invoke-virtual/range {v21 .. v22}, Lcom/wf/wellsfargomobile/WFApp;->setFrontImageUploadProcessComplete(Z)V

    .line 323
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    move-object/from16 v21, v0

    #getter for: Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v21 .. v21}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v21

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Lcom/wf/wellsfargomobile/WFApp;->setFrontCheckImage([B)V

    .line 324
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->this$0:Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;

    move-object/from16 v21, v0

    #getter for: Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;
    invoke-static/range {v21 .. v21}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;

    move-result-object v21

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Lcom/wf/wellsfargomobile/WFApp;->setFrontCheckPreviewImage(Landroid/graphics/Bitmap;)V

    throw v20

    .line 317
    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v4       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v5       #cookieManager:Landroid/webkit/CookieManager;
    .restart local v6       #cookieSyncManager:Landroid/webkit/CookieSyncManager;
    .restart local v7       #cookies:Ljava/lang/String;
    .restart local v11       #image:[B
    :catchall_1
    move-exception v20

    move-object v3, v4

    .end local v4           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    goto :goto_8

    .line 312
    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v4       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :catch_6
    move-exception v8

    move-object v3, v4

    .end local v4           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    goto/16 :goto_7

    .line 309
    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v4       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :catch_7
    move-exception v8

    move-object v3, v4

    .end local v4           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    goto/16 :goto_6

    .line 306
    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v4       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :catch_8
    move-exception v8

    move-object v3, v4

    .end local v4           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    goto/16 :goto_5

    .line 303
    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v4       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :catch_9
    move-exception v8

    move-object v3, v4

    .end local v4           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    goto/16 :goto_4

    .line 300
    .end local v3           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v4       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    :catch_a
    move-exception v8

    move-object v3, v4

    .end local v4           #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    .restart local v3       #client:Lorg/apache/http/impl/client/DefaultHttpClient;
    goto/16 :goto_3
.end method
