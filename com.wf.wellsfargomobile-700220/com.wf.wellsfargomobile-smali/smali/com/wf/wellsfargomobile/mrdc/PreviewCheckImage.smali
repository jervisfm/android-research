.class public Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;
.super Lcom/wf/wellsfargomobile/BaseActivity;
.source "PreviewCheckImage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$1;,
        Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;
    }
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;

.field private imageMode:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

.field private useButton:Landroid/widget/Button;

.field private wfApp:Lcom/wf/wellsfargomobile/WFApp;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/wf/wellsfargomobile/BaseActivity;-><init>()V

    .line 50
    const-string v0, "PreviewCheckImage"

    iput-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->TAG:Ljava/lang/String;

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->imageMode:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    .line 202
    return-void
.end method

.method static synthetic access$000(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;)Lcom/wf/wellsfargomobile/WFApp;
    .locals 1
    .parameter "x0"

    .prologue
    .line 49
    iget-object v0, p0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    return-object v0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "intent"

    .prologue
    .line 158
    invoke-super {p0, p1, p2, p3}, Lcom/wf/wellsfargomobile/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 159
    const-string v1, "PreviewCheckImage"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onActivityResult - imageMode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->imageMode:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " result code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    const-string v1, "PreviewCheckImage"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PreviewCheckImage.onActivityResult - imageMode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->imageMode:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " result code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    packed-switch p2, :pswitch_data_0

    .line 181
    :goto_0
    const-string v1, "PreviewCheckImage"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onActivityResult - unrecognized result code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    const-string v1, "PreviewCheckImage"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PreviewCheckImage.onActivityResult - unrecognized result code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    return-void

    .line 165
    :pswitch_0
    const-string v1, "PreviewCheckImage"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onActivityResult - cancelling MRDC flow - imageMode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->imageMode:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    const-string v1, "PreviewCheckImage"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PreviewCheckImage.onActivityResult - cancelling MRDC flow - imageMode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->imageMode:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 169
    .local v0, in:Landroid/content/Intent;
    const/16 v1, 0xa

    invoke-virtual {p0, v1, v0}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->setResult(ILandroid/content/Intent;)V

    .line 170
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->finish()V

    .line 173
    .end local v0           #in:Landroid/content/Intent;
    :pswitch_1
    const-string v1, "PreviewCheckImage"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onActivityResult - completing MRDC flow - imageMode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->imageMode:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    const-string v1, "PreviewCheckImage"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PreviewCheckImage.onActivityResult - completing MRDC flow - imageMode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->imageMode:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 177
    .restart local v0       #in:Landroid/content/Intent;
    const/16 v1, 0xb

    invoke-virtual {p0, v1, v0}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->setResult(ILandroid/content/Intent;)V

    .line 178
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->finish()V

    goto/16 :goto_0

    .line 163
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .parameter "savedInstanceState"

    .prologue
    .line 57
    invoke-super {p0, p1}, Lcom/wf/wellsfargomobile/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 59
    const-string v6, "PreviewCheckImage"

    const-string v7, "PreviewCheckImage.onCreate"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    const v6, 0x7f030008

    invoke-virtual {p0, v6}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->setContentView(I)V

    .line 62
    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->setRequestedOrientation(I)V

    .line 64
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->getApplication()Landroid/app/Application;

    move-result-object v6

    check-cast v6, Lcom/wf/wellsfargomobile/WFApp;

    iput-object v6, p0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    .line 67
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 68
    .local v4, intent:Landroid/content/Intent;
    const-string v6, "MRDC_FLOW_STATE"

    invoke-virtual {v4, v6}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    .line 69
    .local v1, flag:Ljava/io/Serializable;
    if-nez v1, :cond_1

    .line 70
    sget-object v6, Lcom/wf/wellsfargomobile/mrdc/ImageMode;->FRONT:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    iput-object v6, p0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->imageMode:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    .line 76
    .end local v1           #flag:Ljava/io/Serializable;
    :goto_0
    const v6, 0x7f060065

    invoke-virtual {p0, v6}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 77
    .local v0, checkPreview:Landroid/widget/ImageView;
    sget-object v6, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$1;->$SwitchMap$com$wf$wellsfargomobile$mrdc$ImageMode:[I

    iget-object v7, p0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->imageMode:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    invoke-virtual {v7}, Lcom/wf/wellsfargomobile/mrdc/ImageMode;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 102
    :cond_0
    :goto_1
    const v6, 0x7f060068

    invoke-virtual {p0, v6}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    iput-object v6, p0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->useButton:Landroid/widget/Button;

    .line 103
    iget-object v6, p0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->useButton:Landroid/widget/Button;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 104
    return-void

    .line 72
    .end local v0           #checkPreview:Landroid/widget/ImageView;
    .restart local v1       #flag:Ljava/io/Serializable;
    :cond_1
    check-cast v1, Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    .end local v1           #flag:Ljava/io/Serializable;
    iput-object v1, p0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->imageMode:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    goto :goto_0

    .line 79
    .restart local v0       #checkPreview:Landroid/widget/ImageView;
    :pswitch_0
    const-string v6, "PreviewCheckImage"

    const-string v7, "getting FRONT image for display"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    iget-object v6, p0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-virtual {v6}, Lcom/wf/wellsfargomobile/WFApp;->getFrontCheckPreviewImage()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 81
    .local v2, frontImage:Landroid/graphics/Bitmap;
    if-eqz v2, :cond_0

    .line 82
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 87
    .end local v2           #frontImage:Landroid/graphics/Bitmap;
    :pswitch_1
    const-string v6, "PreviewCheckImage"

    const-string v7, "getting REAR image for display"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    iget-object v6, p0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-virtual {v6}, Lcom/wf/wellsfargomobile/WFApp;->getRearCheckPreviewImage()Landroid/graphics/Bitmap;

    move-result-object v5

    .line 89
    .local v5, rearImage:Landroid/graphics/Bitmap;
    if-eqz v5, :cond_2

    .line 90
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 92
    :cond_2
    const v6, 0x7f060009

    invoke-virtual {p0, v6}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 93
    .local v3, instruction:Landroid/widget/TextView;
    const v6, 0x7f070059

    invoke-virtual {p0, v6}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 77
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onRetakePicture(Landroid/view/View;)V
    .locals 3
    .parameter "v"

    .prologue
    .line 151
    const-string v0, "PreviewCheckImage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onRetakePicture - imageMode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->imageMode:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->setResult(I)V

    .line 153
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->finish()V

    .line 154
    return-void
.end method

.method public onUsePicture(Landroid/view/View;)V
    .locals 6
    .parameter "v"

    .prologue
    const/4 v5, 0x0

    .line 117
    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->useButton:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 119
    sget-object v2, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$1;->$SwitchMap$com$wf$wellsfargomobile$mrdc$ImageMode:[I

    iget-object v3, p0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->imageMode:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    invoke-virtual {v3}, Lcom/wf/wellsfargomobile/mrdc/ImageMode;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 144
    const-string v2, "PreviewCheckImage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PreviewCheckImage.onUsePicture unrecognized image mode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->imageMode:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    const-string v2, "PreviewCheckImage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "unrecognized image mode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->imageMode:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    :goto_0
    return-void

    .line 121
    :pswitch_0
    const-string v2, "PreviewCheckImage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PreviewCheckImage.onUsePicture launching CapturePhoto image mode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->imageMode:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    new-instance v2, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;

    invoke-direct {v2, p0, p0}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;-><init>(Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;Landroid/content/Context;)V

    new-array v3, v5, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage$SubmitFrontImageThread;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 128
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/wf/wellsfargomobile/mrdc/CapturePhoto;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 129
    .local v1, intent:Landroid/content/Intent;
    const-string v2, "MRDC_FLOW_STATE"

    sget-object v3, Lcom/wf/wellsfargomobile/mrdc/ImageMode;->REAR:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 130
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 134
    .end local v1           #intent:Landroid/content/Intent;
    :pswitch_1
    const-string v2, "PreviewCheckImage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PreviewCheckImage.onUsePicture sending flow complete image mode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->imageMode:Lcom/wf/wellsfargomobile/mrdc/ImageMode;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 138
    .local v0, in:Landroid/content/Intent;
    const/16 v2, 0xb

    invoke-virtual {p0, v2, v0}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->setResult(ILandroid/content/Intent;)V

    .line 139
    invoke-virtual {p0}, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->finish()V

    goto :goto_0

    .line 119
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public openPhotoTips(Landroid/view/View;)V
    .locals 3
    .parameter "v"

    .prologue
    .line 194
    new-instance v0, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-direct {v0, v1, v2}, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;-><init>(Landroid/content/Context;Lcom/wf/wellsfargomobile/WFApp;)V

    iget-object v1, p0, Lcom/wf/wellsfargomobile/mrdc/PreviewCheckImage;->wfApp:Lcom/wf/wellsfargomobile/WFApp;

    invoke-virtual {v1}, Lcom/wf/wellsfargomobile/WFApp;->getNonce()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/wf/wellsfargomobile/mrdc/JavaScriptInterfacePhotoTips;->openPhotoTips(Ljava/lang/String;)V

    .line 195
    return-void
.end method
