package com.amazon.device.ads;

class AdBridge$1
  implements AdListener
{
  AdBridge$1(AdBridge paramAdBridge)
  {
  }

  public void onAdCollapsed(AdLayout paramAdLayout)
  {
    Log.d("AdBridge", "Default ad listener called - Ad Collapsed.");
  }

  public void onAdExpanded(AdLayout paramAdLayout)
  {
    Log.d("AdBridge", "Default ad listener called - Ad Will Expand.");
  }

  public void onAdFailedToLoad(AdLayout paramAdLayout, AdError paramAdError)
  {
    Log.d("AdBridge", "Default ad listener called - Ad Failed to Load. Error code: " + paramAdError.code_ + ", Error Message: " + paramAdError.getResponseMessage());
  }

  public void onAdLoaded(AdLayout paramAdLayout, AdProperties paramAdProperties)
  {
    Log.d("AdBridge", "Default ad listener called - AdLoaded.");
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.AdBridge.1
 * JD-Core Version:    0.6.2
 */