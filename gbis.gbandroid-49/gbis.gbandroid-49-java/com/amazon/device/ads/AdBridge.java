package com.amazon.device.ads;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.net.Uri;
import android.os.Handler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

class AdBridge
{
  private static final String LOG_TAG = "AdBridge";
  protected AdLayout adLayout_;
  protected AdListener adListener_ = null;
  protected AdLayout.AdSize adSize_;
  protected int adWindowHeight_ = 0;
  protected int adWindowWidth_ = 0;
  protected Context context_;
  protected AdRenderer currentAdRenderer_ = null;
  protected AdLoader.AdRequestTask currentAdRequestTask_ = null;
  protected boolean invalidated_;
  protected boolean isLoading_;
  protected LocationAwareness locationAwareness_;
  protected int locationPrecision_;
  protected Location location_;
  protected float scalingDensity_ = 1.0F;
  protected int timeout_;
  protected String userAgent_;

  AdBridge(AdLayout paramAdLayout, AdLayout.AdSize paramAdSize)
  {
    this.context_ = paramAdLayout.getContext();
    this.adLayout_ = paramAdLayout;
    initializeAdListener();
    this.location_ = getLastKnownLocation();
    this.locationAwareness_ = LocationAwareness.LOCATION_AWARENESS_NORMAL;
    this.locationPrecision_ = 6;
    this.isLoading_ = false;
    this.timeout_ = 20000;
    this.adSize_ = paramAdSize;
    this.scalingDensity_ = Float.parseFloat(getAdRegistration().deviceNativeData_.sf);
    this.userAgent_ = new WebView(this.context_.getApplicationContext()).getSettings().getUserAgentString();
  }

  protected void adClosedExpansion()
  {
    Log.d("AdBridge", "adClosedExpansion");
    new Handler(this.context_.getMainLooper()).post(new AdBridge.5(this));
  }

  protected void adExpanded()
  {
    Log.d("AdBridge", "adExpanded");
    new Handler(this.context_.getMainLooper()).post(new AdBridge.4(this));
  }

  protected void adFailed(AdError paramAdError)
  {
    Log.d("AdBridge", "adFailed");
    new Handler(this.context_.getMainLooper()).post(new AdBridge.3(this, paramAdError));
  }

  protected void adLoaded(AdProperties paramAdProperties)
  {
    Log.d("AdBridge", "adLoaded");
    new Handler(this.context_.getMainLooper()).post(new AdBridge.2(this, paramAdProperties));
  }

  protected void destroy()
  {
    if (this.currentAdRequestTask_ != null)
      this.currentAdRequestTask_.releaseResources();
    if (this.currentAdRenderer_ != null)
    {
      this.currentAdRenderer_.removeView();
      this.currentAdRenderer_.destroy();
      this.currentAdRenderer_ = null;
    }
  }

  protected Activity getActivity()
  {
    return (Activity)this.context_;
  }

  protected AdLayout getAdLayout()
  {
    return this.adLayout_;
  }

  protected InternalAdRegistration getAdRegistration()
  {
    return this.adLayout_.getAdRegistration();
  }

  protected AdLayout.AdSize getAdSize()
  {
    return this.adSize_;
  }

  protected Context getContext()
  {
    return this.context_;
  }

  // ERROR //
  protected Location getLastKnownLocation()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 78	com/amazon/device/ads/AdBridge:locationAwareness_	Lcom/amazon/device/ads/AdBridge$LocationAwareness;
    //   4: getstatic 205	com/amazon/device/ads/AdBridge$LocationAwareness:LOCATION_AWARENESS_DISABLED	Lcom/amazon/device/ads/AdBridge$LocationAwareness;
    //   7: if_acmpne +5 -> 12
    //   10: aconst_null
    //   11: areturn
    //   12: aload_0
    //   13: getfield 60	com/amazon/device/ads/AdBridge:context_	Landroid/content/Context;
    //   16: ldc 207
    //   18: invokevirtual 211	android/content/Context:getSystemService	(Ljava/lang/String;)Ljava/lang/Object;
    //   21: checkcast 213	android/location/LocationManager
    //   24: astore_1
    //   25: aload_1
    //   26: ldc 215
    //   28: invokevirtual 218	android/location/LocationManager:getLastKnownLocation	(Ljava/lang/String;)Landroid/location/Location;
    //   31: astore 9
    //   33: aload 9
    //   35: astore_3
    //   36: aload_1
    //   37: ldc 220
    //   39: invokevirtual 218	android/location/LocationManager:getLastKnownLocation	(Ljava/lang/String;)Landroid/location/Location;
    //   42: astore 7
    //   44: aload 7
    //   46: astore 5
    //   48: aload_3
    //   49: ifnonnull +8 -> 57
    //   52: aload 5
    //   54: ifnull -44 -> 10
    //   57: aload_3
    //   58: ifnull +159 -> 217
    //   61: aload 5
    //   63: ifnull +154 -> 217
    //   66: aload_3
    //   67: invokevirtual 226	android/location/Location:getTime	()J
    //   70: aload 5
    //   72: invokevirtual 226	android/location/Location:getTime	()J
    //   75: lcmp
    //   76: ifle +127 -> 203
    //   79: ldc 8
    //   81: ldc 228
    //   83: invokestatic 137	com/amazon/device/ads/Log:d	(Ljava/lang/String;Ljava/lang/String;)V
    //   86: aload_0
    //   87: getfield 78	com/amazon/device/ads/AdBridge:locationAwareness_	Lcom/amazon/device/ads/AdBridge$LocationAwareness;
    //   90: getstatic 231	com/amazon/device/ads/AdBridge$LocationAwareness:LOCATION_AWARENESS_TRUNCATED	Lcom/amazon/device/ads/AdBridge$LocationAwareness;
    //   93: if_acmpne +47 -> 140
    //   96: aload_3
    //   97: aload_3
    //   98: invokevirtual 235	android/location/Location:getLatitude	()D
    //   101: invokestatic 241	java/math/BigDecimal:valueOf	(D)Ljava/math/BigDecimal;
    //   104: aload_0
    //   105: getfield 80	com/amazon/device/ads/AdBridge:locationPrecision_	I
    //   108: iconst_5
    //   109: invokevirtual 245	java/math/BigDecimal:setScale	(II)Ljava/math/BigDecimal;
    //   112: invokevirtual 248	java/math/BigDecimal:doubleValue	()D
    //   115: invokevirtual 252	android/location/Location:setLatitude	(D)V
    //   118: aload_3
    //   119: aload_3
    //   120: invokevirtual 255	android/location/Location:getLongitude	()D
    //   123: invokestatic 241	java/math/BigDecimal:valueOf	(D)Ljava/math/BigDecimal;
    //   126: aload_0
    //   127: getfield 80	com/amazon/device/ads/AdBridge:locationPrecision_	I
    //   130: iconst_5
    //   131: invokevirtual 245	java/math/BigDecimal:setScale	(II)Ljava/math/BigDecimal;
    //   134: invokevirtual 248	java/math/BigDecimal:doubleValue	()D
    //   137: invokevirtual 258	android/location/Location:setLongitude	(D)V
    //   140: aload_3
    //   141: areturn
    //   142: astore 8
    //   144: ldc 8
    //   146: ldc_w 260
    //   149: invokestatic 137	com/amazon/device/ads/Log:d	(Ljava/lang/String;Ljava/lang/String;)V
    //   152: aconst_null
    //   153: astore_3
    //   154: goto -118 -> 36
    //   157: astore_2
    //   158: ldc 8
    //   160: ldc_w 262
    //   163: invokestatic 137	com/amazon/device/ads/Log:d	(Ljava/lang/String;Ljava/lang/String;)V
    //   166: aconst_null
    //   167: astore_3
    //   168: goto -132 -> 36
    //   171: astore 6
    //   173: ldc 8
    //   175: ldc_w 264
    //   178: invokestatic 137	com/amazon/device/ads/Log:d	(Ljava/lang/String;Ljava/lang/String;)V
    //   181: aconst_null
    //   182: astore 5
    //   184: goto -136 -> 48
    //   187: astore 4
    //   189: ldc 8
    //   191: ldc_w 266
    //   194: invokestatic 137	com/amazon/device/ads/Log:d	(Ljava/lang/String;Ljava/lang/String;)V
    //   197: aconst_null
    //   198: astore 5
    //   200: goto -152 -> 48
    //   203: ldc 8
    //   205: ldc_w 268
    //   208: invokestatic 137	com/amazon/device/ads/Log:d	(Ljava/lang/String;Ljava/lang/String;)V
    //   211: aload 5
    //   213: astore_3
    //   214: goto -128 -> 86
    //   217: aload_3
    //   218: ifnull +14 -> 232
    //   221: ldc 8
    //   223: ldc_w 270
    //   226: invokestatic 137	com/amazon/device/ads/Log:d	(Ljava/lang/String;Ljava/lang/String;)V
    //   229: goto -143 -> 86
    //   232: ldc 8
    //   234: ldc_w 272
    //   237: invokestatic 137	com/amazon/device/ads/Log:d	(Ljava/lang/String;Ljava/lang/String;)V
    //   240: aload 5
    //   242: astore_3
    //   243: goto -157 -> 86
    //
    // Exception table:
    //   from	to	target	type
    //   25	33	142	java/lang/SecurityException
    //   25	33	157	java/lang/IllegalArgumentException
    //   36	44	171	java/lang/SecurityException
    //   36	44	187	java/lang/IllegalArgumentException
  }

  protected Location getLocation()
  {
    if (this.location_ == null)
      this.location_ = getLastKnownLocation();
    return this.location_;
  }

  protected float getScalingDensity()
  {
    return this.scalingDensity_;
  }

  protected int getTimeout()
  {
    return this.timeout_;
  }

  protected int getWindowHeight()
  {
    return this.adWindowHeight_;
  }

  protected int getWindowWidth()
  {
    return this.adWindowWidth_;
  }

  protected void handleApplicationDefinedSpecialURL(String paramString)
  {
    Log.i("AdBridge", "Special url clicked, but was not handled by SDK. Url: " + paramString);
  }

  protected void handleResponse(AdResponse paramAdResponse)
  {
    if (this.currentAdRenderer_ != null)
      this.currentAdRenderer_.removeView();
    Iterator localIterator = paramAdResponse.getCreativeTypes().iterator();
    do
    {
      if (!localIterator.hasNext())
        break;
      AdResponse.AAXCreative localAAXCreative = (AdResponse.AAXCreative)localIterator.next();
      if (!AdRendererFactory.shouldCreateNewRenderer(localAAXCreative, this.currentAdRenderer_))
        break label123;
      Log.d("AdBridge", "Creating new renderer");
      if (this.currentAdRenderer_ != null)
        this.currentAdRenderer_.destroy();
      this.currentAdRenderer_ = AdRendererFactory.getAdRenderer(localAAXCreative, paramAdResponse, this);
    }
    while (this.currentAdRenderer_ == null);
    while (this.currentAdRenderer_ == null)
    {
      Log.d("AdBridge", "No renderer returned, not loading an ad");
      adFailed(new AdError(-2, "No renderer returned, not loading an ad"));
      return;
      label123: Log.d("AdBridge", "Re-using renderer");
      this.currentAdRenderer_.setAdResponse(paramAdResponse);
    }
    this.currentAdRenderer_.render();
  }

  protected void initializeAdListener()
  {
    this.adListener_ = new AdBridge.1(this);
  }

  protected boolean isAdLoading()
  {
    return this.isLoading_;
  }

  protected boolean isInvalidated()
  {
    return this.invalidated_;
  }

  protected void loadAd(AdTargetingOptions paramAdTargetingOptions)
  {
    AdRequest localAdRequest = new AdRequest(this, paramAdTargetingOptions, this.adSize_, this.adWindowWidth_, this.adWindowHeight_, this.userAgent_, this.timeout_);
    this.isLoading_ = true;
    this.currentAdRequestTask_ = new AdLoader.AdRequestTask();
    this.currentAdRequestTask_.execute(new AdRequest[] { localAdRequest });
  }

  protected void prepareToGoAway()
  {
    if (this.currentAdRenderer_ != null)
      this.currentAdRenderer_.prepareToGoAway();
  }

  protected boolean sendCommand(String paramString, HashMap<String, String> paramHashMap)
  {
    if (this.currentAdRenderer_ != null)
      return this.currentAdRenderer_.sendCommand(paramString, paramHashMap);
    return false;
  }

  protected void setAdSize(AdLayout.AdSize paramAdSize)
  {
    this.adSize_ = paramAdSize;
  }

  protected void setIsLoading(boolean paramBoolean)
  {
    this.isLoading_ = paramBoolean;
  }

  protected void setListener(AdListener paramAdListener)
  {
    if (paramAdListener != null)
      this.adListener_ = paramAdListener;
  }

  protected void setTimeout(int paramInt)
  {
    this.timeout_ = paramInt;
  }

  protected void setWindowDimensions(int paramInt1, int paramInt2)
  {
    this.adWindowHeight_ = paramInt1;
    this.adWindowWidth_ = paramInt2;
  }

  protected void specialUrlClicked(String paramString)
  {
    if (AmazonDeviceLauncher.isWindowshopPresent(this.context_))
    {
      Uri localUri = Uri.parse(paramString);
      String str1;
      if (localUri.getHost().equals("shopping"))
      {
        str1 = localUri.getQueryParameter("app-action");
        if ((str1 != null) && (str1.length() != 0))
          break label48;
      }
      label48: 
      do
      {
        String str2;
        do
        {
          String str3;
          do
          {
            return;
            if (!str1.equals("detail"))
              break;
            str3 = localUri.getQueryParameter("asin");
          }
          while ((str3 == null) || (str3.length() == 0));
          AmazonDeviceLauncher.launchWindowshopDetailPage(this.context_, str3);
          return;
          if (!str1.equals("search"))
            break;
          str2 = localUri.getQueryParameter("keyword");
        }
        while ((str2 == null) || (str2.length() == 0));
        AmazonDeviceLauncher.luanchWindowshopSearchPage(this.context_, str2);
        return;
      }
      while (!str1.equals("webview"));
      handleApplicationDefinedSpecialURL(paramString);
      return;
    }
    handleApplicationDefinedSpecialURL(paramString);
  }

  protected static enum LocationAwareness
  {
    static
    {
      LOCATION_AWARENESS_DISABLED = new LocationAwareness("LOCATION_AWARENESS_DISABLED", 2);
      LocationAwareness[] arrayOfLocationAwareness = new LocationAwareness[3];
      arrayOfLocationAwareness[0] = LOCATION_AWARENESS_NORMAL;
      arrayOfLocationAwareness[1] = LOCATION_AWARENESS_TRUNCATED;
      arrayOfLocationAwareness[2] = LOCATION_AWARENESS_DISABLED;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.AdBridge
 * JD-Core Version:    0.6.2
 */