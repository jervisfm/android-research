package com.amazon.device.ads;

public final class AdError
{
  protected int code_;
  protected String message_;

  AdError(int paramInt, String paramString)
  {
    this.code_ = paramInt;
    this.message_ = paramString;
  }

  public final int getResponseCode()
  {
    return this.code_;
  }

  public final String getResponseMessage()
  {
    return this.message_;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.AdError
 * JD-Core Version:    0.6.2
 */