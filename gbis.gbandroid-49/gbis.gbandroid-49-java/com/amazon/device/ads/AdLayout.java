package com.amazon.device.ads;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.webkit.WebViewDatabase;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.TextView;

public class AdLayout extends FrameLayout
{
  private static final String LOG_TAG = "AmazonAdLayout";
  private InternalAdRegistration amznAdregistration_;
  private boolean attached_ = false;
  private AdBridge bridge_;
  private Context context_;
  private boolean hasRegisterBroadcastReciever_ = false;
  private boolean hasSetAdUnitId_ = false;
  private boolean isInForeground_;
  private int lastVisibility_ = 8;
  private BroadcastReceiver screenStateReceiver_;

  public AdLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    try
    {
      ResourceLookup localResourceLookup1 = new ResourceLookup("adsdk", paramContext);
      str = null;
      localResourceLookup2 = localResourceLookup1;
      if (localResourceLookup2 != null)
      {
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, localResourceLookup2.getStyleableArray("Amazon"));
        str = localTypedArray.getString(localResourceLookup2.getStyleableId("Amazon", "adSize"));
        localTypedArray.recycle();
      }
      initialize(paramContext, AdSize.fromString(str));
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      while (true)
      {
        String str = "custom";
        ResourceLookup localResourceLookup2 = null;
      }
    }
  }

  public AdLayout(Context paramContext, AdSize paramAdSize)
  {
    super(paramContext);
    if (paramAdSize == null)
      throw new IllegalArgumentException();
    initialize(paramContext, paramAdSize);
  }

  private void initialize(Context paramContext, AdSize paramAdSize)
  {
    this.context_ = paramContext;
    if (isInEditMode())
    {
      TextView localTextView = new TextView(paramContext);
      localTextView.setText("AdLayout");
      localTextView.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
      localTextView.setGravity(17);
      addView(localTextView);
      return;
    }
    this.amznAdregistration_ = InternalAdRegistration.getInstance(paramContext);
    if (getVisibility() == 0);
    for (boolean bool = true; ; bool = false)
    {
      this.isInForeground_ = bool;
      setHorizontalScrollBarEnabled(false);
      setVerticalScrollBarEnabled(false);
      if (WebViewDatabase.getInstance(paramContext) != null)
        break;
      Log.e("AmazonAdLayout", "Disabling ads. Local cache file is inaccessible so ads will fail if we try to create a WebView. Details of this Android bug found at:http://code.google.com/p/android/issues/detail?id=10789");
      return;
    }
    this.bridge_ = createAdBridge(paramAdSize);
  }

  private void registerScreenStateBroadcastReceiver()
  {
    if (this.hasRegisterBroadcastReciever_ == true)
      return;
    this.hasRegisterBroadcastReciever_ = true;
    this.screenStateReceiver_ = new AdLayout.1(this);
    IntentFilter localIntentFilter = new IntentFilter("android.intent.action.SCREEN_OFF");
    localIntentFilter.addAction("android.intent.action.USER_PRESENT");
    this.context_.registerReceiver(this.screenStateReceiver_, localIntentFilter);
  }

  private void unregisterScreenStateBroadcastReceiver()
  {
    if (this.hasRegisterBroadcastReciever_ == true)
    {
      this.hasRegisterBroadcastReciever_ = false;
      this.context_.unregisterReceiver(this.screenStateReceiver_);
    }
  }

  public boolean collapseAd()
  {
    return this.bridge_.sendCommand("close", null);
  }

  protected AdBridge createAdBridge(AdSize paramAdSize)
  {
    return new AdBridge(this, paramAdSize);
  }

  public void destroy()
  {
    unregisterScreenStateBroadcastReceiver();
    if (this.bridge_ != null)
      this.bridge_.destroy();
  }

  protected InternalAdRegistration getAdRegistration()
  {
    return this.amznAdregistration_;
  }

  public AdSize getAdSize()
  {
    return this.bridge_.getAdSize();
  }

  public int getTimeout()
  {
    return this.bridge_.getTimeout();
  }

  public boolean isAdLoading()
  {
    return this.bridge_.isAdLoading();
  }

  public boolean loadAd(AdTargetingOptions paramAdTargetingOptions)
  {
    if (this.bridge_.isAdLoading())
    {
      Log.e("AmazonAdLayout", "Can't load an ad because ad loading is already in progress");
      return false;
    }
    if (!this.hasSetAdUnitId_)
    {
      if (this.amznAdregistration_.getAppId() == null)
      {
        Log.e("AmazonAdLayout", "Can't load an ad because App GUID has not been set. Did you forget to call AdRegistration.setAppGUID( ... )?");
        this.bridge_.adFailed(new AdError(400, "Can't load an ad because App GUID has not been set. Did you forget to call AdRegistration.setAppGUID( ... )?"));
        return true;
      }
      this.hasSetAdUnitId_ = true;
    }
    if (this.bridge_.getAdSize() == null)
    {
      Log.e("AmazonAdLayout", "Ad size is not defined.");
      this.bridge_.adFailed(new AdError(400, "Ad size is not defined."));
      return true;
    }
    if ((this.bridge_.getAdSize() == AdSize.AD_SIZE_CUSTOM) && (!paramAdTargetingOptions.containsAdvancedOption("sz")))
    {
      Log.e("AmazonAdLayout", "Can't load an ad because a custom ad size is specified, but ad size dimensions are not configured using AdTargetingOptions.setAdvancedOption( ... ) with key 'sz'.");
      this.bridge_.adFailed(new AdError(400, "Can't load an ad because a custom ad size is specified, but ad size dimensions are not configured using AdTargetingOptions.setAdvancedOption( ... ) with key 'sz'."));
      return true;
    }
    this.amznAdregistration_.registerIfNeeded();
    this.bridge_.loadAd(paramAdTargetingOptions);
    return true;
  }

  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    if (isInEditMode())
      return;
    this.attached_ = true;
    registerScreenStateBroadcastReceiver();
  }

  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    this.attached_ = false;
    unregisterScreenStateBroadcastReceiver();
    this.bridge_.prepareToGoAway();
  }

  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int i = paramInt3 - paramInt1;
    int j = paramInt4 - paramInt2;
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    if (isInEditMode())
      return;
    this.bridge_.setWindowDimensions(j, i);
  }

  protected void onWindowVisibilityChanged(int paramInt)
  {
    if ((this.attached_) && (this.lastVisibility_ != paramInt))
    {
      if (paramInt == 0)
        break label29;
      this.isInForeground_ = false;
      unregisterScreenStateBroadcastReceiver();
    }
    label29: 
    while (paramInt != 0)
      return;
    this.isInForeground_ = true;
  }

  public void setListener(AdListener paramAdListener)
  {
    this.bridge_.setListener(paramAdListener);
  }

  public void setTimeout(int paramInt)
  {
    this.bridge_.setTimeout(paramInt);
  }

  public static enum AdSize
  {
    public final int height;
    public final String size;
    public final int width;

    static
    {
      AD_SIZE_300x250 = new AdSize("AD_SIZE_300x250", 2, "300x250", 300, 250);
      AD_SIZE_600x90 = new AdSize("AD_SIZE_600x90", 3, "600x90", 600, 90);
      AD_SIZE_728x90 = new AdSize("AD_SIZE_728x90", 4, "728x90", 728, 90);
      AD_SIZE_1024x50 = new AdSize("AD_SIZE_1024x50", 5, "1024x50", 1024, 50);
      AD_SIZE_CUSTOM = new AdSize("AD_SIZE_CUSTOM", 6, "CUSTOM", -1, -1);
      AdSize[] arrayOfAdSize = new AdSize[7];
      arrayOfAdSize[0] = AD_SIZE_300x50;
      arrayOfAdSize[1] = AD_SIZE_320x50;
      arrayOfAdSize[2] = AD_SIZE_300x250;
      arrayOfAdSize[3] = AD_SIZE_600x90;
      arrayOfAdSize[4] = AD_SIZE_728x90;
      arrayOfAdSize[5] = AD_SIZE_1024x50;
      arrayOfAdSize[6] = AD_SIZE_CUSTOM;
    }

    private AdSize(String paramString, int paramInt1, int paramInt2)
    {
      this.size = paramString;
      this.width = paramInt1;
      this.height = paramInt2;
    }

    public static AdSize fromString(String paramString)
    {
      if ((paramString != null) && (paramString.length() != 0))
      {
        if (paramString.equalsIgnoreCase("300x50"))
          return AD_SIZE_300x50;
        if (paramString.equalsIgnoreCase("320x50"))
          return AD_SIZE_320x50;
        if (paramString.equalsIgnoreCase("300x250"))
          return AD_SIZE_300x250;
        if (paramString.equalsIgnoreCase("600x90"))
          return AD_SIZE_600x90;
        if (paramString.equalsIgnoreCase("728x90"))
          return AD_SIZE_728x90;
        if (paramString.equalsIgnoreCase("1024x50"))
          return AD_SIZE_1024x50;
        if (paramString.equalsIgnoreCase("custom"))
          return AD_SIZE_CUSTOM;
        throw new IllegalArgumentException("Invalid ad size: '" + paramString + "', see documentation for available ad sizes.");
      }
      throw new IllegalArgumentException("Invalid ad size string. See documentation for available ad sizes");
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.AdLayout
 * JD-Core Version:    0.6.2
 */