package com.amazon.device.ads;

public abstract interface AdListener
{
  public abstract void onAdCollapsed(AdLayout paramAdLayout);

  public abstract void onAdExpanded(AdLayout paramAdLayout);

  public abstract void onAdFailedToLoad(AdLayout paramAdLayout, AdError paramAdError);

  public abstract void onAdLoaded(AdLayout paramAdLayout, AdProperties paramAdProperties);
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.AdListener
 * JD-Core Version:    0.6.2
 */