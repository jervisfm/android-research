package com.amazon.device.ads;

import android.os.AsyncTask;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

class AdLoader
{
  protected static String LOG_TAG = "AdLoader";

  protected static class AdRequestTask extends AsyncTask<AdRequest, Void, AdResponse>
  {
    protected AdBridge bridge_ = null;
    protected HttpClient client_ = null;
    protected AdError error_;
    protected Exception exception_;
    protected HttpParams httpParams_ = new BasicHttpParams();

    protected AdResponse doInBackground(AdRequest[] paramArrayOfAdRequest)
    {
      this.bridge_ = paramArrayOfAdRequest[0].bridge_;
      try
      {
        AdResponse localAdResponse = fetchAd(paramArrayOfAdRequest[0]);
        return localAdResponse;
      }
      catch (Exception localException)
      {
        this.exception_ = localException;
      }
      return null;
    }

    // ERROR //
    protected AdResponse fetchAd(AdRequest paramAdRequest)
    {
      // Byte code:
      //   0: aload_0
      //   1: getfield 28	com/amazon/device/ads/AdLoader$AdRequestTask:bridge_	Lcom/amazon/device/ads/AdBridge;
      //   4: invokevirtual 56	com/amazon/device/ads/AdBridge:getAdRegistration	()Lcom/amazon/device/ads/InternalAdRegistration;
      //   7: invokevirtual 62	com/amazon/device/ads/InternalAdRegistration:usingMSDK	()Z
      //   10: ifne +13 -> 23
      //   13: new 32	java/lang/Exception
      //   16: dup
      //   17: ldc 64
      //   19: invokespecial 67	java/lang/Exception:<init>	(Ljava/lang/String;)V
      //   22: athrow
      //   23: aload_0
      //   24: getfield 24	com/amazon/device/ads/AdLoader$AdRequestTask:httpParams_	Lorg/apache/http/params/HttpParams;
      //   27: aload_1
      //   28: getfield 71	com/amazon/device/ads/AdRequest:timeout_	I
      //   31: invokestatic 77	org/apache/http/params/HttpConnectionParams:setConnectionTimeout	(Lorg/apache/http/params/HttpParams;I)V
      //   34: aload_0
      //   35: getfield 24	com/amazon/device/ads/AdLoader$AdRequestTask:httpParams_	Lorg/apache/http/params/HttpParams;
      //   38: aload_1
      //   39: getfield 71	com/amazon/device/ads/AdRequest:timeout_	I
      //   42: invokestatic 80	org/apache/http/params/HttpConnectionParams:setSoTimeout	(Lorg/apache/http/params/HttpParams;I)V
      //   45: aload_0
      //   46: getfield 24	com/amazon/device/ads/AdLoader$AdRequestTask:httpParams_	Lorg/apache/http/params/HttpParams;
      //   49: sipush 8192
      //   52: invokestatic 83	org/apache/http/params/HttpConnectionParams:setSocketBufferSize	(Lorg/apache/http/params/HttpParams;I)V
      //   55: aload_0
      //   56: new 85	org/apache/http/impl/client/DefaultHttpClient
      //   59: dup
      //   60: aload_0
      //   61: getfield 24	com/amazon/device/ads/AdLoader$AdRequestTask:httpParams_	Lorg/apache/http/params/HttpParams;
      //   64: invokespecial 88	org/apache/http/impl/client/DefaultHttpClient:<init>	(Lorg/apache/http/params/HttpParams;)V
      //   67: putfield 26	com/amazon/device/ads/AdLoader$AdRequestTask:client_	Lorg/apache/http/client/HttpClient;
      //   70: aload_1
      //   71: getfield 92	com/amazon/device/ads/AdRequest:url_	Ljava/lang/StringBuilder;
      //   74: invokevirtual 98	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   77: astore_2
      //   78: new 100	org/apache/http/client/methods/HttpPost
      //   81: dup
      //   82: aload_2
      //   83: invokespecial 101	org/apache/http/client/methods/HttpPost:<init>	(Ljava/lang/String;)V
      //   86: astore_3
      //   87: aload_0
      //   88: getfield 26	com/amazon/device/ads/AdLoader$AdRequestTask:client_	Lorg/apache/http/client/HttpClient;
      //   91: aload_3
      //   92: invokeinterface 107 2 0
      //   97: astore 4
      //   99: aload 4
      //   101: invokeinterface 113 1 0
      //   106: astore 5
      //   108: aload 4
      //   110: invokeinterface 117 1 0
      //   115: invokeinterface 123 1 0
      //   120: sipush 200
      //   123: if_icmpne +20 -> 143
      //   126: aload 5
      //   128: ifnull +15 -> 143
      //   131: aload 5
      //   133: invokeinterface 129 1 0
      //   138: lconst_0
      //   139: lcmp
      //   140: ifne +29 -> 169
      //   143: aload_0
      //   144: new 131	com/amazon/device/ads/AdError
      //   147: dup
      //   148: sipush 503
      //   151: ldc 133
      //   153: invokespecial 136	com/amazon/device/ads/AdError:<init>	(ILjava/lang/String;)V
      //   156: putfield 138	com/amazon/device/ads/AdLoader$AdRequestTask:error_	Lcom/amazon/device/ads/AdError;
      //   159: new 32	java/lang/Exception
      //   162: dup
      //   163: ldc 133
      //   165: invokespecial 67	java/lang/Exception:<init>	(Ljava/lang/String;)V
      //   168: athrow
      //   169: aload 5
      //   171: invokeinterface 142 1 0
      //   176: astore 6
      //   178: new 144	java/lang/StringBuffer
      //   181: dup
      //   182: invokespecial 145	java/lang/StringBuffer:<init>	()V
      //   185: astore 7
      //   187: sipush 4096
      //   190: newarray byte
      //   192: astore 8
      //   194: aload 6
      //   196: aload 8
      //   198: invokevirtual 151	java/io/InputStream:read	([B)I
      //   201: istore 9
      //   203: iload 9
      //   205: iconst_m1
      //   206: if_icmpeq +24 -> 230
      //   209: aload 7
      //   211: new 153	java/lang/String
      //   214: dup
      //   215: aload 8
      //   217: iconst_0
      //   218: iload 9
      //   220: invokespecial 156	java/lang/String:<init>	([BII)V
      //   223: invokevirtual 160	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
      //   226: pop
      //   227: goto -33 -> 194
      //   230: new 162	org/json/JSONTokener
      //   233: dup
      //   234: aload 7
      //   236: invokevirtual 163	java/lang/StringBuffer:toString	()Ljava/lang/String;
      //   239: invokespecial 164	org/json/JSONTokener:<init>	(Ljava/lang/String;)V
      //   242: invokevirtual 168	org/json/JSONTokener:nextValue	()Ljava/lang/Object;
      //   245: checkcast 170	org/json/JSONObject
      //   248: astore 13
      //   250: aload 13
      //   252: ldc 172
      //   254: invokevirtual 176	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
      //   257: astore 14
      //   259: aload 14
      //   261: ifnull +21 -> 282
      //   264: aload 14
      //   266: invokevirtual 179	java/lang/String:length	()I
      //   269: ifeq +13 -> 282
      //   272: aload 14
      //   274: ldc 181
      //   276: invokevirtual 185	java/lang/String:equals	(Ljava/lang/Object;)Z
      //   279: ifeq +259 -> 538
      //   282: aload 13
      //   284: ldc 187
      //   286: invokevirtual 176	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
      //   289: ldc 189
      //   291: ldc 191
      //   293: invokevirtual 195	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
      //   296: ldc 197
      //   298: ldc 191
      //   300: invokevirtual 195	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
      //   303: ldc 199
      //   305: ldc 191
      //   307: invokevirtual 195	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
      //   310: invokevirtual 202	java/lang/String:trim	()Ljava/lang/String;
      //   313: astore 15
      //   315: getstatic 208	com/amazon/device/ads/AdLoader:LOG_TAG	Ljava/lang/String;
      //   318: new 94	java/lang/StringBuilder
      //   321: dup
      //   322: ldc 210
      //   324: invokespecial 211	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
      //   327: aload 15
      //   329: invokevirtual 214	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   332: invokevirtual 98	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   335: invokestatic 220	com/amazon/device/ads/Log:d	(Ljava/lang/String;Ljava/lang/String;)V
      //   338: aload 13
      //   340: ldc 222
      //   342: invokevirtual 226	org/json/JSONObject:getJSONArray	(Ljava/lang/String;)Lorg/json/JSONArray;
      //   345: astore 16
      //   347: new 228	com/amazon/device/ads/AdProperties
      //   350: dup
      //   351: aload 16
      //   353: invokespecial 231	com/amazon/device/ads/AdProperties:<init>	(Lorg/json/JSONArray;)V
      //   356: astore 17
      //   358: new 233	java/util/HashSet
      //   361: dup
      //   362: invokespecial 234	java/util/HashSet:<init>	()V
      //   365: astore 18
      //   367: iconst_0
      //   368: istore 19
      //   370: aload 16
      //   372: invokevirtual 237	org/json/JSONArray:length	()I
      //   375: istore 20
      //   377: iload 19
      //   379: iload 20
      //   381: if_icmpge +25 -> 406
      //   384: aload 18
      //   386: aload 16
      //   388: iload 19
      //   390: invokevirtual 241	org/json/JSONArray:getInt	(I)I
      //   393: invokestatic 247	com/amazon/device/ads/AdResponse$AAXCreative:getCreative	(I)Lcom/amazon/device/ads/AdResponse$AAXCreative;
      //   396: invokevirtual 250	java/util/HashSet:add	(Ljava/lang/Object;)Z
      //   399: pop
      //   400: iinc 19 1
      //   403: goto -33 -> 370
      //   406: aload 18
      //   408: invokevirtual 253	java/util/HashSet:isEmpty	()Z
      //   411: ifne +56 -> 467
      //   414: aload 18
      //   416: invokestatic 257	com/amazon/device/ads/AdResponse$AAXCreative:determineTypeOrder	(Ljava/util/HashSet;)Ljava/util/ArrayList;
      //   419: astore 21
      //   421: aload_2
      //   422: invokestatic 263	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
      //   425: ldc_w 265
      //   428: invokevirtual 268	android/net/Uri:getQueryParameter	(Ljava/lang/String;)Ljava/lang/String;
      //   431: ldc_w 270
      //   434: invokevirtual 274	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
      //   437: astore 22
      //   439: new 276	com/amazon/device/ads/AdResponse
      //   442: dup
      //   443: aload 17
      //   445: aload 15
      //   447: aload 21
      //   449: aload 22
      //   451: iconst_0
      //   452: aaload
      //   453: invokestatic 282	java/lang/Integer:parseInt	(Ljava/lang/String;)I
      //   456: aload 22
      //   458: iconst_1
      //   459: aaload
      //   460: invokestatic 282	java/lang/Integer:parseInt	(Ljava/lang/String;)I
      //   463: invokespecial 285	com/amazon/device/ads/AdResponse:<init>	(Lcom/amazon/device/ads/AdProperties;Ljava/lang/String;Ljava/util/ArrayList;II)V
      //   466: areturn
      //   467: aload_0
      //   468: new 131	com/amazon/device/ads/AdError
      //   471: dup
      //   472: sipush 500
      //   475: ldc_w 287
      //   478: invokespecial 136	com/amazon/device/ads/AdError:<init>	(ILjava/lang/String;)V
      //   481: putfield 138	com/amazon/device/ads/AdLoader$AdRequestTask:error_	Lcom/amazon/device/ads/AdError;
      //   484: getstatic 208	com/amazon/device/ads/AdLoader:LOG_TAG	Ljava/lang/String;
      //   487: ldc_w 287
      //   490: invokestatic 290	com/amazon/device/ads/Log:w	(Ljava/lang/String;Ljava/lang/String;)V
      //   493: aconst_null
      //   494: astore 21
      //   496: goto -75 -> 421
      //   499: astore 12
      //   501: aload_0
      //   502: new 131	com/amazon/device/ads/AdError
      //   505: dup
      //   506: sipush 500
      //   509: ldc_w 292
      //   512: invokespecial 136	com/amazon/device/ads/AdError:<init>	(ILjava/lang/String;)V
      //   515: putfield 138	com/amazon/device/ads/AdLoader$AdRequestTask:error_	Lcom/amazon/device/ads/AdError;
      //   518: getstatic 208	com/amazon/device/ads/AdLoader:LOG_TAG	Ljava/lang/String;
      //   521: ldc_w 292
      //   524: invokestatic 290	com/amazon/device/ads/Log:w	(Ljava/lang/String;Ljava/lang/String;)V
      //   527: new 32	java/lang/Exception
      //   530: dup
      //   531: ldc_w 294
      //   534: invokespecial 67	java/lang/Exception:<init>	(Ljava/lang/String;)V
      //   537: athrow
      //   538: aload 13
      //   540: ldc_w 296
      //   543: invokevirtual 176	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
      //   546: astore 25
      //   548: aload 13
      //   550: ldc_w 298
      //   553: invokevirtual 176	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
      //   556: astore 26
      //   558: new 94	java/lang/StringBuilder
      //   561: dup
      //   562: ldc_w 300
      //   565: invokespecial 211	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
      //   568: aload 25
      //   570: invokevirtual 214	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   573: invokevirtual 98	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   576: astore 27
      //   578: aload_0
      //   579: new 131	com/amazon/device/ads/AdError
      //   582: dup
      //   583: sipush 500
      //   586: aload 27
      //   588: invokespecial 136	com/amazon/device/ads/AdError:<init>	(ILjava/lang/String;)V
      //   591: putfield 138	com/amazon/device/ads/AdLoader$AdRequestTask:error_	Lcom/amazon/device/ads/AdError;
      //   594: getstatic 208	com/amazon/device/ads/AdLoader:LOG_TAG	Ljava/lang/String;
      //   597: new 94	java/lang/StringBuilder
      //   600: dup
      //   601: invokespecial 301	java/lang/StringBuilder:<init>	()V
      //   604: aload 27
      //   606: invokevirtual 214	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   609: ldc_w 303
      //   612: invokevirtual 214	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   615: aload 26
      //   617: invokevirtual 214	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   620: invokevirtual 98	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   623: invokestatic 290	com/amazon/device/ads/Log:w	(Ljava/lang/String;Ljava/lang/String;)V
      //   626: goto -99 -> 527
      //   629: astore 10
      //   631: aload 10
      //   633: invokevirtual 306	java/lang/UnsupportedOperationException:getMessage	()Ljava/lang/String;
      //   636: astore 11
      //   638: aload_0
      //   639: new 131	com/amazon/device/ads/AdError
      //   642: dup
      //   643: sipush 500
      //   646: aload 11
      //   648: invokespecial 136	com/amazon/device/ads/AdError:<init>	(ILjava/lang/String;)V
      //   651: putfield 138	com/amazon/device/ads/AdLoader$AdRequestTask:error_	Lcom/amazon/device/ads/AdError;
      //   654: getstatic 208	com/amazon/device/ads/AdLoader:LOG_TAG	Ljava/lang/String;
      //   657: aload 11
      //   659: invokestatic 290	com/amazon/device/ads/Log:w	(Ljava/lang/String;Ljava/lang/String;)V
      //   662: goto -135 -> 527
      //   665: astore 23
      //   667: goto -267 -> 400
      //
      // Exception table:
      //   from	to	target	type
      //   230	259	499	org/json/JSONException
      //   264	282	499	org/json/JSONException
      //   282	367	499	org/json/JSONException
      //   370	377	499	org/json/JSONException
      //   384	400	499	org/json/JSONException
      //   406	421	499	org/json/JSONException
      //   421	467	499	org/json/JSONException
      //   467	493	499	org/json/JSONException
      //   538	626	499	org/json/JSONException
      //   230	259	629	java/lang/UnsupportedOperationException
      //   264	282	629	java/lang/UnsupportedOperationException
      //   282	367	629	java/lang/UnsupportedOperationException
      //   370	377	629	java/lang/UnsupportedOperationException
      //   406	421	629	java/lang/UnsupportedOperationException
      //   421	467	629	java/lang/UnsupportedOperationException
      //   467	493	629	java/lang/UnsupportedOperationException
      //   538	626	629	java/lang/UnsupportedOperationException
      //   384	400	665	java/lang/UnsupportedOperationException
    }

    protected void onPostExecute(AdResponse paramAdResponse)
    {
      if (paramAdResponse == null)
      {
        this.bridge_.setIsLoading(false);
        if (this.exception_ != null)
        {
          String str = "Exception caught while loading ad: " + this.exception_;
          Log.d(AdLoader.LOG_TAG, str);
          this.exception_.printStackTrace();
          if (this.error_ == null)
            this.error_ = new AdError(-1, str);
        }
        if (this.error_ != null)
        {
          if (this.bridge_ == null)
            break label111;
          if (this.error_ != null)
            this.bridge_.adFailed(this.error_);
        }
      }
      while (true)
      {
        releaseResources();
        return;
        label111: this.bridge_.adFailed(new AdError(-1, "Unknown error ocurred."));
        continue;
        this.bridge_.handleResponse(paramAdResponse);
      }
    }

    public void releaseResources()
    {
      try
      {
        if (this.client_ != null)
        {
          ClientConnectionManager localClientConnectionManager = this.client_.getConnectionManager();
          if (localClientConnectionManager != null)
            localClientConnectionManager.shutdown();
          this.client_ = null;
        }
        this.exception_ = null;
        return;
      }
      finally
      {
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.AdLoader
 * JD-Core Version:    0.6.2
 */