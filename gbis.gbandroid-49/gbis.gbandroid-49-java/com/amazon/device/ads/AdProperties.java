package com.amazon.device.ads;

import org.json.JSONArray;
import org.json.JSONException;

public class AdProperties
{
  public static final String LOG_TAG = "AdProperties";
  private AdType adType_;
  private boolean canExpand_ = false;
  private boolean canPlayAudio_ = false;
  private boolean canPlayVideo_ = false;

  AdProperties(AdType paramAdType)
  {
    this.adType_ = paramAdType;
  }

  AdProperties(JSONArray paramJSONArray)
  {
    int i = 0;
    if (paramJSONArray != null);
    while (true)
    {
      if (i < paramJSONArray.length())
      {
        try
        {
          switch (paramJSONArray.getInt(i))
          {
          case 1001:
          case 1002:
            this.canPlayAudio_ = true;
          case 1003:
          case 1004:
          case 1007:
          case 1014:
          case 1016:
          case 1017:
          case 1005:
          case 1006:
          case 1008:
          case 1009:
          case 1010:
          case 1011:
          case 1012:
          case 1013:
          case 1015:
          }
        }
        catch (JSONException localJSONException)
        {
          Log.w("AdProperties", "Unable to parse creative type: " + localJSONException.getMessage());
        }
        this.canExpand_ = true;
        break label203;
        this.adType_ = AdType.IMAGE_BANNER;
        break label203;
        this.canPlayVideo_ = true;
        break label203;
        this.adType_ = AdType.MRAID_1;
        break label203;
        this.adType_ = AdType.MRAID_2;
      }
      else
      {
        return;
      }
      label203: i++;
    }
  }

  public boolean canExpand()
  {
    return this.canExpand_;
  }

  public boolean canPlayAudio()
  {
    return this.canPlayAudio_;
  }

  public boolean canPlayVideo()
  {
    return this.canPlayVideo_;
  }

  public AdType getAdType()
  {
    return this.adType_;
  }

  void setAdType(AdType paramAdType)
  {
    this.adType_ = paramAdType;
  }

  void setCanExpand(boolean paramBoolean)
  {
    this.canExpand_ = paramBoolean;
  }

  void setCanPlayAudio(boolean paramBoolean)
  {
    this.canPlayAudio_ = paramBoolean;
  }

  void setCanPlayVideo(boolean paramBoolean)
  {
    this.canPlayVideo_ = paramBoolean;
  }

  public static enum AdType
  {
    private String type_;

    static
    {
      AdType[] arrayOfAdType = new AdType[3];
      arrayOfAdType[0] = IMAGE_BANNER;
      arrayOfAdType[1] = MRAID_1;
      arrayOfAdType[2] = MRAID_2;
    }

    private AdType(String paramString)
    {
      this.type_ = paramString;
    }

    public final String toString()
    {
      return this.type_;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.AdProperties
 * JD-Core Version:    0.6.2
 */