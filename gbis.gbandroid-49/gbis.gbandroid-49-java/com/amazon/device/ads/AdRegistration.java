package com.amazon.device.ads;

import android.content.Context;

public final class AdRegistration
{
  public static final void enableLogging(Context paramContext, boolean paramBoolean)
  {
    InternalAdRegistration.getInstance(paramContext).setLoggingEnabled(paramBoolean);
  }

  public static final void enableTesting(Context paramContext, boolean paramBoolean)
  {
    InternalAdRegistration.getInstance(paramContext).setTestMode(paramBoolean);
  }

  public static final String getVersion(Context paramContext)
  {
    return InternalAdRegistration.getInstance(paramContext).getSDKVersionID();
  }

  public static final void registerApp(Context paramContext)
  {
    InternalAdRegistration.getInstance(paramContext).registerIfNeeded();
  }

  public static final void setAppGUID(Context paramContext, String paramString)
  {
    InternalAdRegistration.getInstance(paramContext).setApplicationId(paramString);
  }

  public static final void setAppUniqueId(Context paramContext, String paramString)
  {
    InternalAdRegistration.getInstance(paramContext).setApplicationId(paramString);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.AdRegistration
 * JD-Core Version:    0.6.2
 */