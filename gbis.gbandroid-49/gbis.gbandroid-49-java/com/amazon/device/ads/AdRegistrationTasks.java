package com.amazon.device.ads;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

final class AdRegistrationTasks
{
  public static int HTTP_TIMEOUT = 0;
  public static final String LOG_TAG = "AmazonAdRegistrationTasks";

  protected static String buildDeviceInfoParams(A9Request paramA9Request, boolean paramBoolean)
  {
    StringBuilder localStringBuilder = new StringBuilder("http://");
    localStringBuilder.append(paramA9Request.endpoint_.adSystemEndpoint);
    localStringBuilder.append(paramA9Request.endpoint_.adSystemHandler);
    if (paramBoolean)
    {
      localStringBuilder.append("/update_dev_info");
      localStringBuilder.append("?adId=");
      if (paramA9Request.amazonDeviceId_ == null)
        throw new IllegalArgumentException("AmazonDeviceId is null");
      localStringBuilder.append(paramA9Request.amazonDeviceId_);
      localStringBuilder.append("&dt=");
    }
    while (true)
    {
      localStringBuilder.append(paramA9Request.deviceInfo_.dt);
      localStringBuilder.append("&app=");
      localStringBuilder.append(paramA9Request.deviceInfo_.app);
      localStringBuilder.append("&aud=");
      localStringBuilder.append(paramA9Request.deviceInfo_.aud);
      localStringBuilder.append("&appId=");
      localStringBuilder.append(paramA9Request.deviceInfo_.appId);
      if (paramA9Request.deviceInfo_.sha1_mac != null)
      {
        localStringBuilder.append("&sha1_mac=");
        localStringBuilder.append(paramA9Request.deviceInfo_.sha1_mac);
      }
      if (paramA9Request.deviceInfo_.sha1_udid != null)
      {
        localStringBuilder.append("&sha1_udid=");
        localStringBuilder.append(paramA9Request.deviceInfo_.sha1_udid);
      }
      if (paramA9Request.deviceInfo_.sha1_tel != null)
      {
        localStringBuilder.append("&sha1_tel=");
        localStringBuilder.append(paramA9Request.deviceInfo_.sha1_tel);
      }
      if (paramA9Request.deviceInfo_.sha1_serial != null)
      {
        localStringBuilder.append("&sha1_serial=");
        localStringBuilder.append(paramA9Request.deviceInfo_.sha1_serial);
      }
      if (paramA9Request.deviceNativeData_.json != null)
      {
        localStringBuilder.append("&dinfo=");
        localStringBuilder.append(paramA9Request.deviceNativeData_.urlEncodedJson);
      }
      if (paramA9Request.deviceInfo_.ua != null)
      {
        localStringBuilder.append("&ua=");
        localStringBuilder.append(paramA9Request.deviceInfo_.ua);
      }
      if (paramA9Request.deviceInfo_.bad_mac)
        localStringBuilder.append("&badMac=true");
      if (paramA9Request.deviceInfo_.bad_serial)
        localStringBuilder.append("&badSerial=true");
      if (paramA9Request.deviceInfo_.bad_tel)
        localStringBuilder.append("&badTel=true");
      if (paramA9Request.deviceInfo_.bad_udid)
        localStringBuilder.append("&badUdid=true");
      return localStringBuilder.toString();
      localStringBuilder.append("/generate_did");
      localStringBuilder.append("?dt=");
    }
  }

  protected static int getIntegerFromJSON(JSONObject paramJSONObject, String paramString, int paramInt)
  {
    try
    {
      int i = paramJSONObject.getInt(paramString);
      return i;
    }
    catch (JSONException localJSONException)
    {
    }
    return paramInt;
  }

  protected static String getStringFromJSON(JSONObject paramJSONObject, String paramString1, String paramString2)
  {
    try
    {
      String str = paramJSONObject.getString(paramString1);
      return str;
    }
    catch (JSONException localJSONException)
    {
    }
    return paramString2;
  }

  protected static class A9Request
  {
    protected String amazonDeviceId_;
    protected Context context_;
    protected InternalAdRegistration.DeviceInfo deviceInfo_;
    protected InternalAdRegistration.DeviceNativeData deviceNativeData_;
    protected InternalAdRegistration.AmazonAdEndpoint endpoint_;
    protected String requestTag_;

    public A9Request(Context paramContext, String paramString1, InternalAdRegistration.AmazonAdEndpoint paramAmazonAdEndpoint, InternalAdRegistration.DeviceInfo paramDeviceInfo, InternalAdRegistration.DeviceNativeData paramDeviceNativeData, String paramString2)
    {
      this.context_ = paramContext;
      this.requestTag_ = paramString1;
      this.endpoint_ = paramAmazonAdEndpoint;
      this.deviceInfo_ = paramDeviceInfo;
      this.deviceNativeData_ = paramDeviceNativeData;
      this.amazonDeviceId_ = paramString2;
    }
  }

  protected static abstract class A9RequestTask extends AsyncTask<AdRegistrationTasks.A9Request, Void, AdRegistrationTasks.A9TaskResult>
  {
    protected Exception exception_;
    protected HttpClient httpClient_;
    protected String requestTag_ = "unknown";

    public A9RequestTask()
    {
      BasicHttpParams localBasicHttpParams = new BasicHttpParams();
      HttpConnectionParams.setConnectionTimeout(localBasicHttpParams, AdRegistrationTasks.HTTP_TIMEOUT);
      HttpConnectionParams.setSoTimeout(localBasicHttpParams, AdRegistrationTasks.HTTP_TIMEOUT);
      HttpConnectionParams.setSocketBufferSize(localBasicHttpParams, 8192);
      this.httpClient_ = new DefaultHttpClient(localBasicHttpParams);
    }

    public AdRegistrationTasks.A9TaskResult doInBackground(AdRegistrationTasks.A9Request[] paramArrayOfA9Request)
    {
      try
      {
        this.requestTag_ = paramArrayOfA9Request[0].requestTag_;
        AdRegistrationTasks.A9TaskResult localA9TaskResult = executeA9Request(paramArrayOfA9Request[0]);
        return localA9TaskResult;
      }
      catch (Exception localException)
      {
        this.exception_ = localException;
      }
      return null;
    }

    protected abstract AdRegistrationTasks.A9TaskResult executeA9Request(AdRegistrationTasks.A9Request paramA9Request);

    protected void onPostExecute(AdRegistrationTasks.A9TaskResult paramA9TaskResult)
    {
      if (this.exception_ != null)
        Log.d("AmazonAdRegistrationTasks", "Exception caught while performing " + this.requestTag_ + " request to A9: " + this.exception_);
      while (paramA9TaskResult == null)
        return;
      paramA9TaskResult.execute();
    }
  }

  protected static abstract class A9TaskResult
  {
    protected Context context_;

    public A9TaskResult(Context paramContext)
    {
      this.context_ = paramContext;
    }

    public abstract void execute();
  }

  protected static class NoOpA9TaskResult extends AdRegistrationTasks.A9TaskResult
  {
    public NoOpA9TaskResult(Context paramContext)
    {
      super();
    }

    public void execute()
    {
    }
  }

  protected static class PingTask extends AdRegistrationTasks.A9RequestTask
  {
    protected AdRegistrationTasks.A9TaskResult executeA9Request(AdRegistrationTasks.A9Request paramA9Request)
    {
      StringBuilder localStringBuilder = new StringBuilder("http://");
      localStringBuilder.append(paramA9Request.endpoint_.adSystemEndpoint);
      localStringBuilder.append(paramA9Request.endpoint_.adSystemHandler);
      localStringBuilder.append("/ping");
      localStringBuilder.append("?adId=");
      if (paramA9Request.amazonDeviceId_ == null)
        throw new IllegalArgumentException("AmazonDeviceId is null");
      localStringBuilder.append(paramA9Request.amazonDeviceId_);
      localStringBuilder.append("&appId=");
      localStringBuilder.append(paramA9Request.deviceInfo_.appId);
      HttpPost localHttpPost = new HttpPost(localStringBuilder.toString());
      Log.d("AmazonAdRegistrationTasks", "Sending: " + localStringBuilder.toString());
      HttpResponse localHttpResponse = this.httpClient_.execute(localHttpPost);
      HttpEntity localHttpEntity = localHttpResponse.getEntity();
      if ((localHttpResponse.getStatusLine().getStatusCode() != 200) || (localHttpEntity == null) || (localHttpEntity.getContentLength() == 0L))
      {
        String str1 = "SIS ping call failed: Status code: " + localHttpResponse.getStatusLine().getStatusCode();
        Log.e("AmazonAdRegistrationTasks", str1);
        throw new Exception(str1);
      }
      StringBuffer localStringBuffer = Utils.extractHttpResponse(localHttpEntity.getContent());
      try
      {
        JSONObject localJSONObject = (JSONObject)new JSONTokener(localStringBuffer.toString()).nextValue();
        int i = AdRegistrationTasks.getIntegerFromJSON(localJSONObject, "rcode", 0);
        String str3 = AdRegistrationTasks.getStringFromJSON(localJSONObject, "msg", "");
        if (i == 1)
          return new AdRegistrationTasks.NoOpA9TaskResult(paramA9Request.context_);
        String str4 = "SIS ping failed -- code: " + i + ", msg: " + str3;
        Log.e("AmazonAdRegistrationTasks", str4);
        throw new Exception(str4);
      }
      catch (JSONException localJSONException)
      {
        String str2 = "JSON error parsing return from SIS: " + localJSONException.getMessage();
        Log.e("AmazonAdRegistrationTasks", str2);
        throw new Exception(str2);
      }
    }
  }

  protected static class RegisterDeviceTask extends AdRegistrationTasks.A9RequestTask
  {
    protected AdRegistrationTasks.A9TaskResult executeA9Request(AdRegistrationTasks.A9Request paramA9Request)
    {
      String str1 = AdRegistrationTasks.buildDeviceInfoParams(paramA9Request, false);
      HttpPost localHttpPost = new HttpPost(str1);
      Log.d("AmazonAdRegistrationTasks", "Sending: " + str1);
      HttpResponse localHttpResponse = this.httpClient_.execute(localHttpPost);
      HttpEntity localHttpEntity = localHttpResponse.getEntity();
      if ((localHttpResponse.getStatusLine().getStatusCode() != 200) || (localHttpEntity == null) || (localHttpEntity.getContentLength() == 0L))
      {
        String str2 = "SIS registration call failed: Status code: " + localHttpResponse.getStatusLine().getStatusCode();
        Log.e("AmazonAdRegistrationTasks", str2);
        throw new Exception(str2);
      }
      StringBuffer localStringBuffer = Utils.extractHttpResponse(localHttpEntity.getContent());
      try
      {
        JSONObject localJSONObject = (JSONObject)new JSONTokener(localStringBuffer.toString()).nextValue();
        int i = AdRegistrationTasks.getIntegerFromJSON(localJSONObject, "rcode", 0);
        String str4 = AdRegistrationTasks.getStringFromJSON(localJSONObject, "msg", "");
        String str5 = AdRegistrationTasks.getStringFromJSON(localJSONObject, "adId", "");
        if (i == 1)
          return new AdRegistrationTasks.RegisterTaskResult(paramA9Request.context_, str5);
        String str6 = "SIS failed registering device -- code: " + i + ", msg: " + str4;
        Log.e("AmazonAdRegistrationTasks", str6);
        throw new Exception(str6);
      }
      catch (JSONException localJSONException)
      {
        String str3 = "JSON error parsing return from SIS: " + localJSONException.getMessage();
        Log.e("AmazonAdRegistrationTasks", str3);
        throw new Exception(str3);
      }
    }
  }

  protected static class RegisterTaskResult extends AdRegistrationTasks.A9TaskResult
  {
    protected String adid_;

    public RegisterTaskResult(Context paramContext, String paramString)
    {
      super();
      this.adid_ = paramString;
      Log.d("AmazonAdRegistrationTasks", "Register Device returned adid: " + paramString);
    }

    public void execute()
    {
      String str = InternalAdRegistration.getInstance().amznDeviceId_;
      if ((str == null) || (!str.equals(this.adid_)))
      {
        SharedPreferences.Editor localEditor = this.context_.getSharedPreferences("AmazonMobileAds", 0).edit();
        localEditor.putString("amzn-ad-id", this.adid_);
        localEditor.commit();
        InternalAdRegistration.getInstance().amznDeviceId_ = this.adid_;
      }
    }
  }

  protected static class UpdateDeviceInfoTask extends AdRegistrationTasks.A9RequestTask
  {
    protected AdRegistrationTasks.A9TaskResult executeA9Request(AdRegistrationTasks.A9Request paramA9Request)
    {
      String str1 = AdRegistrationTasks.buildDeviceInfoParams(paramA9Request, true);
      HttpPost localHttpPost = new HttpPost(str1);
      Log.d("AmazonAdRegistrationTasks", "Sending: " + str1);
      HttpResponse localHttpResponse = this.httpClient_.execute(localHttpPost);
      HttpEntity localHttpEntity = localHttpResponse.getEntity();
      if ((localHttpResponse.getStatusLine().getStatusCode() != 200) || (localHttpEntity == null) || (localHttpEntity.getContentLength() == 0L))
      {
        String str2 = "SIS update_device_info call failed: Status code: " + localHttpResponse.getStatusLine().getStatusCode();
        Log.e("AmazonAdRegistrationTasks", str2);
        throw new Exception(str2);
      }
      StringBuffer localStringBuffer = Utils.extractHttpResponse(localHttpEntity.getContent());
      int i;
      String str4;
      try
      {
        JSONObject localJSONObject = (JSONObject)new JSONTokener(localStringBuffer.toString()).nextValue();
        i = AdRegistrationTasks.getIntegerFromJSON(localJSONObject, "rcode", 0);
        str4 = AdRegistrationTasks.getStringFromJSON(localJSONObject, "msg", "");
        String str5 = AdRegistrationTasks.getStringFromJSON(localJSONObject, "adId", "");
        String str6 = AdRegistrationTasks.getStringFromJSON(localJSONObject, "idChanged", "");
        if (i != 1)
          break label328;
        if ((str6.length() > 0) && (str6.equals("true")))
        {
          if (str5.length() == 0)
            return new AdRegistrationTasks.NoOpA9TaskResult(paramA9Request.context_);
          AdRegistrationTasks.RegisterTaskResult localRegisterTaskResult = new AdRegistrationTasks.RegisterTaskResult(paramA9Request.context_, str5);
          return localRegisterTaskResult;
        }
      }
      catch (JSONException localJSONException)
      {
        String str3 = "JSON error parsing return from SIS: " + localJSONException.getMessage();
        Log.e("AmazonAdRegistrationTasks", str3);
        throw new Exception(str3);
      }
      return new AdRegistrationTasks.NoOpA9TaskResult(paramA9Request.context_);
      label328: String str7 = "SIS failed updating device info -- code: " + i + ", msg: " + str4;
      Log.e("AmazonAdRegistrationTasks", str7);
      throw new Exception(str7);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.AdRegistrationTasks
 * JD-Core Version:    0.6.2
 */