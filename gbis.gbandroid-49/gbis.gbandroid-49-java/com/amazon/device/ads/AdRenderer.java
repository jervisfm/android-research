package com.amazon.device.ads;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

abstract class AdRenderer
{
  protected static final String AAX_REDIRECT_BETA = "aax-beta.integ.amazon.com";
  protected static final String AAX_REDIRECT_GAMMA = "aax-us-east.amazon-adsystem.com";
  protected static final String AAX_REDIRECT_PROD = "aax-us-east.amazon-adsystem.com";
  protected static final String CORNERSTONE_BEST_ENDPOINT_BETA = "d16g-cornerstone-bes.integ.amazon.com";
  protected static final String CORNERSTONE_BEST_ENDPOINT_PROD = "pda-bes.amazon.com";
  private static final String LOG_TAG = "AdRenderer";
  protected AdResponse ad_ = null;
  protected AdBridge bridge_ = null;
  protected boolean isDestroyed_ = false;
  protected Set<String> redirectHosts_ = null;
  protected double scalingFactor_ = 1.0D;
  protected boolean usingDownscalingLogic_ = false;
  protected boolean viewRemoved_ = false;

  protected AdRenderer(AdResponse paramAdResponse, AdBridge paramAdBridge)
  {
    this.ad_ = paramAdResponse;
    this.bridge_ = paramAdBridge;
    this.redirectHosts_ = new HashSet();
    this.redirectHosts_.add("aax-us-east.amazon-adsystem.com");
    this.redirectHosts_.add("aax-us-east.amazon-adsystem.com");
    this.redirectHosts_.add("aax-beta.integ.amazon.com");
    this.redirectHosts_.add("pda-bes.amazon.com");
    this.redirectHosts_.add("d16g-cornerstone-bes.integ.amazon.com");
    determineScalingFactor();
  }

  protected void adLoaded(AdProperties paramAdProperties)
  {
    this.bridge_.setIsLoading(false);
    this.bridge_.adLoaded(paramAdProperties);
  }

  protected abstract void destroy();

  protected void determineScalingFactor()
  {
    int i = (int)(this.ad_.adSizeWidth_ * this.bridge_.getScalingDensity());
    int j = (int)(this.ad_.adSizeHeight_ * this.bridge_.getScalingDensity());
    double d1 = this.bridge_.getWindowWidth() / i;
    double d2 = this.bridge_.getWindowHeight() / j;
    if (d2 < d1);
    for (this.scalingFactor_ = d2; ; this.scalingFactor_ = d1)
    {
      if (this.scalingFactor_ < 1.0D)
      {
        this.scalingFactor_ *= this.bridge_.getScalingDensity();
        this.usingDownscalingLogic_ = true;
      }
      Log.d("AdRenderer", "SCALING params: scalingDensity: " + this.bridge_.getScalingDensity() + ", windowWidth: " + this.bridge_.getWindowWidth() + ", windowHeight: " + this.bridge_.getWindowHeight() + ", adWidth: " + i + ", adHeight: " + j + ", scalingFactor: " + this.scalingFactor_);
      return;
    }
  }

  protected boolean isAdViewDestroyed()
  {
    return this.isDestroyed_;
  }

  protected boolean isAdViewRemoved()
  {
    return this.viewRemoved_;
  }

  protected void launchExternalBrowserForLink(String paramString)
  {
    if (isAdViewDestroyed())
      return;
    if ((paramString == null) || (paramString.equals("")))
      paramString = "about:blank";
    Log.d("AdRenderer", "Final URI to show in browser: " + paramString);
    Intent localIntent = new Intent("android.intent.action.VIEW", Uri.parse(paramString));
    localIntent.setFlags(268435456);
    try
    {
      this.bridge_.getContext().startActivity(localIntent);
      return;
    }
    catch (ActivityNotFoundException localActivityNotFoundException)
    {
      String str = localIntent.getAction();
      if (str.startsWith("market://"))
      {
        Log.w("AdRenderer", "Could not handle market action: " + str);
        return;
      }
      Log.w("AdRenderer", "Could not handle intent action: " + str);
    }
  }

  protected abstract void prepareToGoAway();

  protected abstract void removeView();

  protected abstract void render();

  protected abstract boolean sendCommand(String paramString, Map<String, String> paramMap);

  protected void setAdResponse(AdResponse paramAdResponse)
  {
    this.ad_ = paramAdResponse;
  }

  protected abstract boolean shouldReuse();
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.AdRenderer
 * JD-Core Version:    0.6.2
 */