package com.amazon.device.ads;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

class AdRendererFactory
{
  public static final String LOG_TAG = "AdRendererFactory";

  static AdRenderer getAdRenderer(AdResponse.AAXCreative paramAAXCreative, AdResponse paramAdResponse, AdBridge paramAdBridge)
  {
    if (paramAAXCreative == null)
    {
      Log.e("AdRendererFactory", "NULL passed to getAdRendere()");
      return null;
    }
    try
    {
      Class localClass = Class.forName(paramAAXCreative.getClassName());
      if (localClass == null)
      {
        Log.e("AdRendererFactory", "Unable to create " + paramAAXCreative.getClassName() + " class, Class.forName() returned null");
        return null;
      }
      AdRenderer localAdRenderer = (AdRenderer)localClass.getDeclaredConstructor(new Class[] { AdResponse.class, AdBridge.class }).newInstance(new Object[] { paramAdResponse, paramAdBridge });
      return localAdRenderer;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      Log.e("AdRendererFactory", "Couldn't find " + paramAAXCreative.getClassName() + " ad renderer class");
      return null;
    }
    catch (SecurityException localSecurityException)
    {
      Log.e("AdRendererFactory", "Security exception " + localSecurityException.getLocalizedMessage() + " instantiating " + paramAAXCreative.getClassName() + " ad renderer class");
      return null;
    }
    catch (NoSuchMethodException localNoSuchMethodException)
    {
      Log.e("AdRendererFactory", "No valid constructor found: " + localNoSuchMethodException.getLocalizedMessage() + " instantiating " + paramAAXCreative.getClassName() + " ad renderer class");
      return null;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      Log.e("AdRendererFactory", "Illegal argument exception: " + localIllegalArgumentException.getLocalizedMessage() + " instantiating " + paramAAXCreative.getClassName() + " ad renderer class");
      return null;
    }
    catch (InstantiationException localInstantiationException)
    {
      Log.e("AdRendererFactory", "Instantiation exception: " + localInstantiationException.getLocalizedMessage() + " instantiating " + paramAAXCreative.getClassName() + " ad renderer class");
      return null;
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      Log.e("AdRendererFactory", "Illegal access exception: " + localIllegalAccessException.getLocalizedMessage() + " instantiating " + paramAAXCreative.getClassName() + " ad renderer class");
      return null;
    }
    catch (InvocationTargetException localInvocationTargetException)
    {
      Log.e("AdRendererFactory", "Invocation Target exception: " + localInvocationTargetException.getLocalizedMessage() + " instantiating " + paramAAXCreative.getClassName() + " ad renderer class");
    }
    return null;
  }

  static boolean shouldCreateNewRenderer(AdResponse.AAXCreative paramAAXCreative, AdRenderer paramAdRenderer)
  {
    return (paramAdRenderer == null) || (!paramAdRenderer.shouldReuse()) || (!paramAAXCreative.getClassName().equals(paramAdRenderer.getClass().getName()));
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.AdRendererFactory
 * JD-Core Version:    0.6.2
 */