package com.amazon.device.ads;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

class AdRequest
{
  protected static String LOG_TAG = "AdRequest";
  protected AdBridge bridge_;
  protected AdTargetingOptions opt_;
  protected AdLayout.AdSize size_;
  protected int timeout_;
  protected StringBuilder url_ = new StringBuilder("http://");
  protected String userAgent_;
  protected int windowHeight_;
  protected int windowWidth_;

  public AdRequest(AdBridge paramAdBridge, AdTargetingOptions paramAdTargetingOptions, AdLayout.AdSize paramAdSize, int paramInt1, int paramInt2, String paramString, int paramInt3)
  {
    this.bridge_ = paramAdBridge;
    this.opt_ = paramAdTargetingOptions;
    this.size_ = paramAdSize;
    this.userAgent_ = paramString;
    this.timeout_ = paramInt3;
    this.windowHeight_ = paramInt2;
    this.windowWidth_ = paramInt1;
    initialize();
  }

  // ERROR //
  private String getAAXParam(AAXParameters paramAAXParameters, HashMap<String, String> paramHashMap)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual 65	com/amazon/device/ads/AdRequest$AAXParameters:getDefaultValue	()Ljava/lang/String;
    //   4: astore_3
    //   5: aload_2
    //   6: aload_1
    //   7: getfield 68	com/amazon/device/ads/AdRequest$AAXParameters:name_	Ljava/lang/String;
    //   10: invokevirtual 74	java/util/HashMap:containsKey	(Ljava/lang/Object;)Z
    //   13: ifeq +28 -> 41
    //   16: aload_2
    //   17: aload_1
    //   18: getfield 68	com/amazon/device/ads/AdRequest$AAXParameters:name_	Ljava/lang/String;
    //   21: invokevirtual 78	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   24: checkcast 80	java/lang/String
    //   27: astore 18
    //   29: aload_2
    //   30: aload_1
    //   31: getfield 68	com/amazon/device/ads/AdRequest$AAXParameters:name_	Ljava/lang/String;
    //   34: invokevirtual 83	java/util/HashMap:remove	(Ljava/lang/Object;)Ljava/lang/Object;
    //   37: pop
    //   38: aload 18
    //   40: astore_3
    //   41: aload_3
    //   42: ifnonnull +524 -> 566
    //   45: aload_1
    //   46: getstatic 87	com/amazon/device/ads/AdRequest$AAXParameters:SIZE	Lcom/amazon/device/ads/AdRequest$AAXParameters;
    //   49: if_acmpne +15 -> 64
    //   52: aload_0
    //   53: getfield 44	com/amazon/device/ads/AdRequest:size_	Lcom/amazon/device/ads/AdLayout$AdSize;
    //   56: getfield 92	com/amazon/device/ads/AdLayout$AdSize:size	Ljava/lang/String;
    //   59: astore 13
    //   61: aload 13
    //   63: areturn
    //   64: aload_1
    //   65: getstatic 95	com/amazon/device/ads/AdRequest$AAXParameters:APPID	Lcom/amazon/device/ads/AdRequest$AAXParameters;
    //   68: if_acmpne +14 -> 82
    //   71: aload_0
    //   72: getfield 40	com/amazon/device/ads/AdRequest:bridge_	Lcom/amazon/device/ads/AdBridge;
    //   75: invokevirtual 101	com/amazon/device/ads/AdBridge:getAdRegistration	()Lcom/amazon/device/ads/InternalAdRegistration;
    //   78: invokevirtual 106	com/amazon/device/ads/InternalAdRegistration:getAppId	()Ljava/lang/String;
    //   81: areturn
    //   82: aload_1
    //   83: getstatic 109	com/amazon/device/ads/AdRequest$AAXParameters:ADID	Lcom/amazon/device/ads/AdRequest$AAXParameters;
    //   86: if_acmpne +14 -> 100
    //   89: aload_0
    //   90: getfield 40	com/amazon/device/ads/AdRequest:bridge_	Lcom/amazon/device/ads/AdBridge;
    //   93: invokevirtual 101	com/amazon/device/ads/AdBridge:getAdRegistration	()Lcom/amazon/device/ads/InternalAdRegistration;
    //   96: invokevirtual 112	com/amazon/device/ads/InternalAdRegistration:getAmazonDeviceId	()Ljava/lang/String;
    //   99: areturn
    //   100: aload_1
    //   101: getstatic 115	com/amazon/device/ads/AdRequest$AAXParameters:USER_AGENT	Lcom/amazon/device/ads/AdRequest$AAXParameters;
    //   104: if_acmpne +11 -> 115
    //   107: aload_0
    //   108: getfield 46	com/amazon/device/ads/AdRequest:userAgent_	Ljava/lang/String;
    //   111: invokestatic 121	com/amazon/device/ads/Utils:getURLEncodedString	(Ljava/lang/String;)Ljava/lang/String;
    //   114: areturn
    //   115: aload_1
    //   116: getstatic 124	com/amazon/device/ads/AdRequest$AAXParameters:DEVICE_INFO	Lcom/amazon/device/ads/AdRequest$AAXParameters;
    //   119: if_acmpne +79 -> 198
    //   122: aload_0
    //   123: getfield 40	com/amazon/device/ads/AdRequest:bridge_	Lcom/amazon/device/ads/AdBridge;
    //   126: invokevirtual 101	com/amazon/device/ads/AdBridge:getAdRegistration	()Lcom/amazon/device/ads/InternalAdRegistration;
    //   129: invokevirtual 128	com/amazon/device/ads/InternalAdRegistration:getDeviceNativeData	()Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;
    //   132: getfield 134	com/amazon/device/ads/InternalAdRegistration$DeviceNativeData:json	Lorg/json/JSONObject;
    //   135: ifnull +63 -> 198
    //   138: aload_0
    //   139: getfield 40	com/amazon/device/ads/AdRequest:bridge_	Lcom/amazon/device/ads/AdBridge;
    //   142: invokevirtual 138	com/amazon/device/ads/AdBridge:getActivity	()Landroid/app/Activity;
    //   145: invokestatic 142	com/amazon/device/ads/Utils:isPortrait	(Landroid/app/Activity;)Z
    //   148: ifeq +32 -> 180
    //   151: aload_0
    //   152: getfield 40	com/amazon/device/ads/AdRequest:bridge_	Lcom/amazon/device/ads/AdBridge;
    //   155: invokevirtual 101	com/amazon/device/ads/AdBridge:getAdRegistration	()Lcom/amazon/device/ads/InternalAdRegistration;
    //   158: getfield 146	com/amazon/device/ads/InternalAdRegistration:deviceNativeData_	Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;
    //   161: ldc 148
    //   163: putfield 151	com/amazon/device/ads/InternalAdRegistration$DeviceNativeData:orientation	Ljava/lang/String;
    //   166: aload_0
    //   167: getfield 40	com/amazon/device/ads/AdRequest:bridge_	Lcom/amazon/device/ads/AdBridge;
    //   170: invokevirtual 101	com/amazon/device/ads/AdBridge:getAdRegistration	()Lcom/amazon/device/ads/InternalAdRegistration;
    //   173: invokevirtual 128	com/amazon/device/ads/InternalAdRegistration:getDeviceNativeData	()Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;
    //   176: invokevirtual 154	com/amazon/device/ads/InternalAdRegistration$DeviceNativeData:getJsonEncodedWithOrientation	()Ljava/lang/String;
    //   179: areturn
    //   180: aload_0
    //   181: getfield 40	com/amazon/device/ads/AdRequest:bridge_	Lcom/amazon/device/ads/AdBridge;
    //   184: invokevirtual 101	com/amazon/device/ads/AdBridge:getAdRegistration	()Lcom/amazon/device/ads/InternalAdRegistration;
    //   187: getfield 146	com/amazon/device/ads/InternalAdRegistration:deviceNativeData_	Lcom/amazon/device/ads/InternalAdRegistration$DeviceNativeData;
    //   190: ldc 156
    //   192: putfield 151	com/amazon/device/ads/InternalAdRegistration$DeviceNativeData:orientation	Ljava/lang/String;
    //   195: goto -29 -> 166
    //   198: aload_1
    //   199: getstatic 159	com/amazon/device/ads/AdRequest$AAXParameters:USER_INFO	Lcom/amazon/device/ads/AdRequest$AAXParameters;
    //   202: if_acmpne +131 -> 333
    //   205: new 161	org/json/JSONObject
    //   208: dup
    //   209: invokespecial 162	org/json/JSONObject:<init>	()V
    //   212: astore 4
    //   214: aload_0
    //   215: getfield 42	com/amazon/device/ads/AdRequest:opt_	Lcom/amazon/device/ads/AdTargetingOptions;
    //   218: invokevirtual 168	com/amazon/device/ads/AdTargetingOptions:getAge	()I
    //   221: iconst_m1
    //   222: if_icmpeq +352 -> 574
    //   225: aload 4
    //   227: ldc 170
    //   229: aload_0
    //   230: getfield 42	com/amazon/device/ads/AdRequest:opt_	Lcom/amazon/device/ads/AdTargetingOptions;
    //   233: invokevirtual 168	com/amazon/device/ads/AdTargetingOptions:getAge	()I
    //   236: invokestatic 174	java/lang/String:valueOf	(I)Ljava/lang/String;
    //   239: invokevirtual 178	org/json/JSONObject:put	(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    //   242: pop
    //   243: iconst_1
    //   244: istore 7
    //   246: aload_0
    //   247: getfield 42	com/amazon/device/ads/AdRequest:opt_	Lcom/amazon/device/ads/AdTargetingOptions;
    //   250: invokevirtual 182	com/amazon/device/ads/AdTargetingOptions:getGender	()Lcom/amazon/device/ads/AdTargetingOptions$Gender;
    //   253: getstatic 188	com/amazon/device/ads/AdTargetingOptions$Gender:UNKNOWN	Lcom/amazon/device/ads/AdTargetingOptions$Gender;
    //   256: if_acmpeq +24 -> 280
    //   259: aload 4
    //   261: ldc 190
    //   263: aload_0
    //   264: getfield 42	com/amazon/device/ads/AdRequest:opt_	Lcom/amazon/device/ads/AdTargetingOptions;
    //   267: invokevirtual 182	com/amazon/device/ads/AdTargetingOptions:getGender	()Lcom/amazon/device/ads/AdTargetingOptions$Gender;
    //   270: getfield 192	com/amazon/device/ads/AdTargetingOptions$Gender:gender	Ljava/lang/String;
    //   273: invokevirtual 178	org/json/JSONObject:put	(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    //   276: pop
    //   277: iconst_1
    //   278: istore 7
    //   280: iload 7
    //   282: ifeq +286 -> 568
    //   285: aload 4
    //   287: invokevirtual 195	org/json/JSONObject:toString	()Ljava/lang/String;
    //   290: invokestatic 121	com/amazon/device/ads/Utils:getURLEncodedString	(Ljava/lang/String;)Ljava/lang/String;
    //   293: astore 8
    //   295: aload 8
    //   297: areturn
    //   298: astore 5
    //   300: iconst_0
    //   301: istore 6
    //   303: getstatic 25	com/amazon/device/ads/AdRequest:LOG_TAG	Ljava/lang/String;
    //   306: new 31	java/lang/StringBuilder
    //   309: dup
    //   310: ldc 197
    //   312: invokespecial 36	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   315: aload 5
    //   317: invokevirtual 201	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   320: invokevirtual 202	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   323: invokestatic 208	com/amazon/device/ads/Log:w	(Ljava/lang/String;Ljava/lang/String;)V
    //   326: iload 6
    //   328: istore 7
    //   330: goto -50 -> 280
    //   333: aload_1
    //   334: getstatic 211	com/amazon/device/ads/AdRequest$AAXParameters:TEST	Lcom/amazon/device/ads/AdRequest$AAXParameters;
    //   337: if_acmpne +29 -> 366
    //   340: aload_0
    //   341: getfield 40	com/amazon/device/ads/AdRequest:bridge_	Lcom/amazon/device/ads/AdBridge;
    //   344: invokevirtual 101	com/amazon/device/ads/AdBridge:getAdRegistration	()Lcom/amazon/device/ads/InternalAdRegistration;
    //   347: invokevirtual 215	com/amazon/device/ads/InternalAdRegistration:getTestMode	()Z
    //   350: ifeq +10 -> 360
    //   353: ldc 217
    //   355: astore 17
    //   357: aload 17
    //   359: areturn
    //   360: aconst_null
    //   361: astore 17
    //   363: goto -6 -> 357
    //   366: aload_1
    //   367: getstatic 220	com/amazon/device/ads/AdRequest$AAXParameters:GEOLOCATION	Lcom/amazon/device/ads/AdRequest$AAXParameters;
    //   370: if_acmpne +69 -> 439
    //   373: aload_0
    //   374: getfield 42	com/amazon/device/ads/AdRequest:opt_	Lcom/amazon/device/ads/AdTargetingOptions;
    //   377: invokevirtual 223	com/amazon/device/ads/AdTargetingOptions:isGeoLocationEnabled	()Z
    //   380: istore 15
    //   382: aconst_null
    //   383: astore 13
    //   385: iload 15
    //   387: ifeq -326 -> 61
    //   390: aload_0
    //   391: getfield 40	com/amazon/device/ads/AdRequest:bridge_	Lcom/amazon/device/ads/AdBridge;
    //   394: invokevirtual 227	com/amazon/device/ads/AdBridge:getLocation	()Landroid/location/Location;
    //   397: astore 16
    //   399: aconst_null
    //   400: astore 13
    //   402: aload 16
    //   404: ifnull -343 -> 61
    //   407: new 31	java/lang/StringBuilder
    //   410: dup
    //   411: invokespecial 228	java/lang/StringBuilder:<init>	()V
    //   414: aload 16
    //   416: invokevirtual 234	android/location/Location:getLatitude	()D
    //   419: invokevirtual 237	java/lang/StringBuilder:append	(D)Ljava/lang/StringBuilder;
    //   422: ldc 239
    //   424: invokevirtual 242	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   427: aload 16
    //   429: invokevirtual 245	android/location/Location:getLongitude	()D
    //   432: invokevirtual 237	java/lang/StringBuilder:append	(D)Ljava/lang/StringBuilder;
    //   435: invokevirtual 202	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   438: areturn
    //   439: aload_1
    //   440: getstatic 248	com/amazon/device/ads/AdRequest$AAXParameters:SHA1_UDID	Lcom/amazon/device/ads/AdRequest$AAXParameters;
    //   443: if_acmpne +40 -> 483
    //   446: aload_0
    //   447: getfield 40	com/amazon/device/ads/AdRequest:bridge_	Lcom/amazon/device/ads/AdBridge;
    //   450: invokevirtual 101	com/amazon/device/ads/AdBridge:getAdRegistration	()Lcom/amazon/device/ads/InternalAdRegistration;
    //   453: getfield 252	com/amazon/device/ads/InternalAdRegistration:deviceInfo_	Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;
    //   456: getfield 257	com/amazon/device/ads/InternalAdRegistration$DeviceInfo:sha1_udid	Ljava/lang/String;
    //   459: astore 14
    //   461: aconst_null
    //   462: astore 13
    //   464: aload 14
    //   466: ifnull -405 -> 61
    //   469: aload_0
    //   470: getfield 40	com/amazon/device/ads/AdRequest:bridge_	Lcom/amazon/device/ads/AdBridge;
    //   473: invokevirtual 101	com/amazon/device/ads/AdBridge:getAdRegistration	()Lcom/amazon/device/ads/InternalAdRegistration;
    //   476: getfield 252	com/amazon/device/ads/InternalAdRegistration:deviceInfo_	Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;
    //   479: getfield 257	com/amazon/device/ads/InternalAdRegistration$DeviceInfo:sha1_udid	Ljava/lang/String;
    //   482: areturn
    //   483: aload_1
    //   484: getstatic 260	com/amazon/device/ads/AdRequest$AAXParameters:MD5_UDID	Lcom/amazon/device/ads/AdRequest$AAXParameters;
    //   487: if_acmpne +40 -> 527
    //   490: aload_0
    //   491: getfield 40	com/amazon/device/ads/AdRequest:bridge_	Lcom/amazon/device/ads/AdBridge;
    //   494: invokevirtual 101	com/amazon/device/ads/AdBridge:getAdRegistration	()Lcom/amazon/device/ads/InternalAdRegistration;
    //   497: getfield 252	com/amazon/device/ads/InternalAdRegistration:deviceInfo_	Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;
    //   500: getfield 263	com/amazon/device/ads/InternalAdRegistration$DeviceInfo:md5_udid	Ljava/lang/String;
    //   503: astore 12
    //   505: aconst_null
    //   506: astore 13
    //   508: aload 12
    //   510: ifnull -449 -> 61
    //   513: aload_0
    //   514: getfield 40	com/amazon/device/ads/AdRequest:bridge_	Lcom/amazon/device/ads/AdBridge;
    //   517: invokevirtual 101	com/amazon/device/ads/AdBridge:getAdRegistration	()Lcom/amazon/device/ads/InternalAdRegistration;
    //   520: getfield 252	com/amazon/device/ads/InternalAdRegistration:deviceInfo_	Lcom/amazon/device/ads/InternalAdRegistration$DeviceInfo;
    //   523: getfield 263	com/amazon/device/ads/InternalAdRegistration$DeviceInfo:md5_udid	Ljava/lang/String;
    //   526: areturn
    //   527: aload_1
    //   528: getstatic 266	com/amazon/device/ads/AdRequest$AAXParameters:SLOT	Lcom/amazon/device/ads/AdRequest$AAXParameters;
    //   531: if_acmpne +35 -> 566
    //   534: aload_0
    //   535: getfield 40	com/amazon/device/ads/AdRequest:bridge_	Lcom/amazon/device/ads/AdBridge;
    //   538: invokevirtual 138	com/amazon/device/ads/AdBridge:getActivity	()Landroid/app/Activity;
    //   541: invokestatic 142	com/amazon/device/ads/Utils:isPortrait	(Landroid/app/Activity;)Z
    //   544: ifeq +6 -> 550
    //   547: ldc 148
    //   549: areturn
    //   550: ldc 156
    //   552: areturn
    //   553: astore 9
    //   555: iload 7
    //   557: istore 6
    //   559: aload 9
    //   561: astore 5
    //   563: goto -260 -> 303
    //   566: aload_3
    //   567: areturn
    //   568: aload_3
    //   569: astore 8
    //   571: goto -276 -> 295
    //   574: iconst_0
    //   575: istore 7
    //   577: goto -331 -> 246
    //
    // Exception table:
    //   from	to	target	type
    //   214	243	298	org/json/JSONException
    //   246	277	553	org/json/JSONException
  }

  public void initialize()
  {
    this.url_.append(this.bridge_.getAdRegistration().getEndpoint().aaxEndpoint);
    this.url_.append(this.bridge_.getAdRegistration().getEndpoint().aaxHandler);
    int i = 1;
    HashMap localHashMap = this.opt_.getCopyOfAdvancedOptions();
    AAXParameters[] arrayOfAAXParameters = AAXParameters.values();
    int j = arrayOfAAXParameters.length;
    int k = 0;
    while (true)
    {
      AAXParameters localAAXParameters;
      if (k < j)
        localAAXParameters = arrayOfAAXParameters[k];
      try
      {
        String str = getAAXParam(localAAXParameters, localHashMap);
        StringBuilder localStringBuilder1;
        StringBuilder localStringBuilder2;
        if (str != null)
        {
          localStringBuilder1 = this.url_;
          localStringBuilder2 = new StringBuilder();
          if (i == 0)
            break label152;
        }
        label146: label152: for (char c = '?'; ; c = '&')
        {
          localStringBuilder1.append(localAAXParameters.getUrlComponent(c) + str);
          i = 0;
          k++;
          break;
        }
        Iterator localIterator = localHashMap.entrySet().iterator();
        while (localIterator.hasNext())
        {
          Map.Entry localEntry = (Map.Entry)localIterator.next();
          this.url_.append("&");
          this.url_.append(Utils.getURLEncodedString((String)localEntry.getKey()));
          this.url_.append("=");
          this.url_.append(Utils.getURLEncodedString((String)localEntry.getValue()));
        }
        Log.d(LOG_TAG, "Generated AAX url: " + this.url_.toString());
        return;
      }
      catch (UnsupportedEncodingException localUnsupportedEncodingException)
      {
        break label146;
      }
    }
  }

  static enum AAXParameters
  {
    private final String defaultValue_;
    final String name_;

    static
    {
      PAGE_TYPE = new AAXParameters("PAGE_TYPE", 3, "pt", null);
      SLOT = new AAXParameters("SLOT", 4, "slot", null);
      PUBLISHER_KEYWORDS = new AAXParameters("PUBLISHER_KEYWORDS", 5, "pk", null);
      PUBLISHER_ASINS = new AAXParameters("PUBLISHER_ASINS", 6, "pa", null);
      USER_AGENT = new AAXParameters("USER_AGENT", 7, "ua", null);
      SDK_VERSION = new AAXParameters("SDK_VERSION", 8, "adsdk", InternalAdRegistration.getInstance().getSDKVersionID());
      GEOLOCATION = new AAXParameters("GEOLOCATION", 9, "geoloc", null);
      USER_INFO = new AAXParameters("USER_INFO", 10, "uinfo", null);
      DEVICE_INFO = new AAXParameters("DEVICE_INFO", 11, "dinfo", null);
      TEST = new AAXParameters("TEST", 12, "isTest", null);
      ATF = new AAXParameters("ATF", 13, "atf", null);
      ADID = new AAXParameters("ADID", 14, "ad-id", null);
      SHA1_UDID = new AAXParameters("SHA1_UDID", 15, "sha1_udid", null);
      MD5_UDID = new AAXParameters("MD5_UDID", 16, "md5_udid", null);
      SLOT_POSITION = new AAXParameters("SLOT_POSITION", 17, "sp", null);
      AAXParameters[] arrayOfAAXParameters = new AAXParameters[18];
      arrayOfAAXParameters[0] = APPID;
      arrayOfAAXParameters[1] = CHANNEL;
      arrayOfAAXParameters[2] = SIZE;
      arrayOfAAXParameters[3] = PAGE_TYPE;
      arrayOfAAXParameters[4] = SLOT;
      arrayOfAAXParameters[5] = PUBLISHER_KEYWORDS;
      arrayOfAAXParameters[6] = PUBLISHER_ASINS;
      arrayOfAAXParameters[7] = USER_AGENT;
      arrayOfAAXParameters[8] = SDK_VERSION;
      arrayOfAAXParameters[9] = GEOLOCATION;
      arrayOfAAXParameters[10] = USER_INFO;
      arrayOfAAXParameters[11] = DEVICE_INFO;
      arrayOfAAXParameters[12] = TEST;
      arrayOfAAXParameters[13] = ATF;
      arrayOfAAXParameters[14] = ADID;
      arrayOfAAXParameters[15] = SHA1_UDID;
      arrayOfAAXParameters[16] = MD5_UDID;
      arrayOfAAXParameters[17] = SLOT_POSITION;
    }

    private AAXParameters(String paramString1, String paramString2)
    {
      this.name_ = paramString1;
      this.defaultValue_ = paramString2;
    }

    public final String getDefaultValue()
    {
      return this.defaultValue_;
    }

    public final String getUrlComponent(char paramChar)
    {
      return paramChar + this.name_ + "=";
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.AdRequest
 * JD-Core Version:    0.6.2
 */