package com.amazon.device.ads;

import java.util.ArrayList;
import java.util.HashSet;

class AdResponse
{
  protected int adSizeHeight_ = 0;
  protected int adSizeWidth_ = 0;
  protected ArrayList<AAXCreative> creativeTypes_;
  protected String creative_;
  protected AdProperties properties_;

  AdResponse(AdProperties paramAdProperties, String paramString, ArrayList<AAXCreative> paramArrayList, int paramInt1, int paramInt2)
  {
    this.creative_ = paramString;
    this.properties_ = paramAdProperties;
    this.creativeTypes_ = paramArrayList;
    this.adSizeHeight_ = paramInt2;
    this.adSizeWidth_ = paramInt1;
  }

  protected int getAdSizeHeight()
  {
    return this.adSizeHeight_;
  }

  protected int getAdSizeWidth()
  {
    return this.adSizeWidth_;
  }

  protected String getCreative()
  {
    return this.creative_;
  }

  protected ArrayList<AAXCreative> getCreativeTypes()
  {
    return this.creativeTypes_;
  }

  protected AdProperties getProperties()
  {
    return this.properties_;
  }

  protected static enum AAXCreative
  {
    private final String class_;
    private final int id_;

    static
    {
      AAXCreative[] arrayOfAAXCreative = new AAXCreative[2];
      arrayOfAAXCreative[0] = HTML;
      arrayOfAAXCreative[1] = MRAID1;
    }

    private AAXCreative(int paramInt, String paramString)
    {
      this.id_ = paramInt;
      this.class_ = paramString;
    }

    static ArrayList<AAXCreative> determineTypeOrder(HashSet<AAXCreative> paramHashSet)
    {
      ArrayList localArrayList = new ArrayList(2);
      if (paramHashSet.contains(MRAID1))
        localArrayList.add(MRAID1);
      if (paramHashSet.contains(HTML))
        localArrayList.add(HTML);
      return localArrayList;
    }

    static AAXCreative getCreative(int paramInt)
    {
      switch (paramInt)
      {
      default:
        throw new UnsupportedOperationException("Invalid creative type: " + paramInt);
      case 1007:
        return HTML;
      case 1016:
      }
      return MRAID1;
    }

    final String getClassName()
    {
      return this.class_;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.AdResponse
 * JD-Core Version:    0.6.2
 */