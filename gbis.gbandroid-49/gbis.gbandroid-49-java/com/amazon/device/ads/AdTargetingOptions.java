package com.amazon.device.ads;

import java.util.HashMap;
import java.util.Map;

public class AdTargetingOptions
{
  private Map<String, String> advanced_ = new HashMap();
  private int age_ = -1;
  private boolean enableGeoTargeting_ = false;
  private Gender gender_ = Gender.UNKNOWN;

  public boolean containsAdvancedOption(String paramString)
  {
    return this.advanced_.containsKey(paramString);
  }

  public AdTargetingOptions enableGeoLocation(boolean paramBoolean)
  {
    this.enableGeoTargeting_ = paramBoolean;
    return this;
  }

  public String getAdvancedOption(String paramString)
  {
    return (String)this.advanced_.get(paramString);
  }

  public int getAge()
  {
    return this.age_;
  }

  protected HashMap<String, String> getCopyOfAdvancedOptions()
  {
    HashMap localHashMap = new HashMap(this.advanced_.size());
    localHashMap.putAll(this.advanced_);
    return localHashMap;
  }

  public Gender getGender()
  {
    return this.gender_;
  }

  public boolean isGeoLocationEnabled()
  {
    return this.enableGeoTargeting_;
  }

  public AdTargetingOptions setAdvancedOption(String paramString1, String paramString2)
  {
    if (paramString2 != null)
    {
      this.advanced_.put(paramString1, paramString2);
      return this;
    }
    this.advanced_.remove(paramString1);
    return this;
  }

  public AdTargetingOptions setAge(int paramInt)
  {
    this.age_ = paramInt;
    return this;
  }

  public AdTargetingOptions setGender(Gender paramGender)
  {
    this.gender_ = paramGender;
    return this;
  }

  public static enum Gender
  {
    public final String gender;

    static
    {
      MALE = new Gender("MALE", 1, "male");
      FEMALE = new Gender("FEMALE", 2, "female");
      Gender[] arrayOfGender = new Gender[3];
      arrayOfGender[0] = UNKNOWN;
      arrayOfGender[1] = MALE;
      arrayOfGender[2] = FEMALE;
    }

    private Gender(String paramString)
    {
      this.gender = paramString;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.AdTargetingOptions
 * JD-Core Version:    0.6.2
 */