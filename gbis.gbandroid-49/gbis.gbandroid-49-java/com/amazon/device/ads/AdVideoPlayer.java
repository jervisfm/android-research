package com.amazon.device.ads;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.VideoView;

final class AdVideoPlayer extends VideoView
  implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener
{
  private static String LOG_TAG = "AdVideoPlayer";
  private AudioManager audioManager_;
  private String contentUrl_;
  private Context context_;
  private AdVideoPlayerListener listener_;
  private Controller.PlayerProperties playerProperties_;
  private boolean released_ = false;
  private int volumeBeforeMuting_;

  public AdVideoPlayer(Context paramContext)
  {
    super(paramContext);
    this.context_ = paramContext;
    setOnCompletionListener(this);
    setOnErrorListener(this);
    setOnPreparedListener(this);
    this.playerProperties_ = new Controller.PlayerProperties();
    this.audioManager_ = ((AudioManager)this.context_.getSystemService("audio"));
  }

  private void displayPlayerControls()
  {
    Log.d(LOG_TAG, "in displayPlayerControls");
    if (this.playerProperties_.showControl())
    {
      MediaController localMediaController = new MediaController(this.context_);
      setMediaController(localMediaController);
      localMediaController.setAnchorView(this);
      localMediaController.requestFocus();
    }
  }

  private void loadPlayerContent()
  {
    setVideoURI(Uri.parse(this.contentUrl_));
    startPlaying();
  }

  private void removePlayerFromParent()
  {
    Log.d(LOG_TAG, "in removePlayerFromParent");
    ViewGroup localViewGroup = (ViewGroup)getParent();
    if (localViewGroup != null)
      localViewGroup.removeView(this);
  }

  public final void mutePlayer()
  {
    Log.d(LOG_TAG, "in mutePlayer");
    this.volumeBeforeMuting_ = this.audioManager_.getStreamVolume(3);
    this.audioManager_.setStreamVolume(3, 0, 4);
  }

  public final void onCompletion(MediaPlayer paramMediaPlayer)
  {
    if (this.playerProperties_.doLoop())
      start();
    while (true)
    {
      if (this.listener_ != null)
        this.listener_.onComplete();
      return;
      if ((this.playerProperties_.exitOnComplete()) || (this.playerProperties_.inline))
        releasePlayer();
    }
  }

  public final boolean onError(MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2)
  {
    removePlayerFromParent();
    if (this.listener_ != null)
      this.listener_.onError();
    return false;
  }

  public final void onPrepared(MediaPlayer paramMediaPlayer)
  {
    if (this.listener_ != null)
      this.listener_.onPrepared();
  }

  public final void playAudio()
  {
    Log.d(LOG_TAG, "in playAudio");
    loadPlayerContent();
  }

  public final void playVideo()
  {
    Log.d(LOG_TAG, "in playVideo");
    if (this.playerProperties_.doMute())
      mutePlayer();
    loadPlayerContent();
    startPlaying();
  }

  public final void releasePlayer()
  {
    Log.d(LOG_TAG, "in releasePlayer");
    if (this.released_);
    do
    {
      return;
      this.released_ = true;
      stopPlayback();
      removePlayerFromParent();
      if (this.playerProperties_.doMute())
        unmutePlayer();
    }
    while (this.listener_ == null);
    this.listener_.onComplete();
  }

  public final void setListener(AdVideoPlayerListener paramAdVideoPlayerListener)
  {
    this.listener_ = paramAdVideoPlayerListener;
  }

  public final void setPlayData(Controller.PlayerProperties paramPlayerProperties, String paramString)
  {
    this.released_ = false;
    if (paramPlayerProperties != null)
      this.playerProperties_ = paramPlayerProperties;
    this.contentUrl_ = paramString;
  }

  public final void startPlaying()
  {
    Log.d(LOG_TAG, "in startPlaying");
    displayPlayerControls();
    if (this.playerProperties_.isAutoPlay())
      start();
  }

  public final void unmutePlayer()
  {
    Log.d(LOG_TAG, "in unmutePlayer");
    this.audioManager_.setStreamVolume(3, this.volumeBeforeMuting_, 4);
  }

  public static abstract interface AdVideoPlayerListener
  {
    public abstract void onComplete();

    public abstract void onError();

    public abstract void onPrepared();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.AdVideoPlayer
 * JD-Core Version:    0.6.2
 */