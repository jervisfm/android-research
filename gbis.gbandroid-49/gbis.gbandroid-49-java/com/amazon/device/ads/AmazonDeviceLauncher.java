package com.amazon.device.ads;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

class AmazonDeviceLauncher
{
  static boolean isWindowshopPresent(Context paramContext)
  {
    return paramContext.getPackageManager().getLaunchIntentForPackage("com.amazon.windowshop") != null;
  }

  static void launchWindowshopDetailPage(Context paramContext, String paramString)
  {
    Intent localIntent = paramContext.getPackageManager().getLaunchIntentForPackage("com.amazon.windowshop");
    if (localIntent != null)
    {
      localIntent.putExtra("com.amazon.windowshop.refinement.asin", paramString);
      paramContext.startActivity(localIntent);
    }
  }

  static void luanchWindowshopSearchPage(Context paramContext, String paramString)
  {
    Intent localIntent = new Intent("android.intent.action.SEARCH");
    localIntent.setComponent(new ComponentName("com.amazon.windowshop", "com.amazon.windowshop.search.SearchResultsGridActivity"));
    localIntent.putExtra("query", paramString);
    try
    {
      paramContext.startActivity(localIntent);
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.AmazonDeviceLauncher
 * JD-Core Version:    0.6.2
 */