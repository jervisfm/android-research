package com.amazon.device.ads;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class Controller$Dimensions$1
  implements Parcelable.Creator<Controller.Dimensions>
{
  public final Controller.Dimensions createFromParcel(Parcel paramParcel)
  {
    return new Controller.Dimensions(paramParcel);
  }

  public final Controller.Dimensions[] newArray(int paramInt)
  {
    return new Controller.Dimensions[paramInt];
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.Controller.Dimensions.1
 * JD-Core Version:    0.6.2
 */