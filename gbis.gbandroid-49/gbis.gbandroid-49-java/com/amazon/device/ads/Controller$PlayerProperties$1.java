package com.amazon.device.ads;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class Controller$PlayerProperties$1
  implements Parcelable.Creator<Controller.PlayerProperties>
{
  public final Controller.PlayerProperties createFromParcel(Parcel paramParcel)
  {
    return new Controller.PlayerProperties(paramParcel);
  }

  public final Controller.PlayerProperties[] newArray(int paramInt)
  {
    return new Controller.PlayerProperties[paramInt];
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.Controller.PlayerProperties.1
 * JD-Core Version:    0.6.2
 */