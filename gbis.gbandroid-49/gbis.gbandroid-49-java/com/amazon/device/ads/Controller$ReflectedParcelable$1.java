package com.amazon.device.ads;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class Controller$ReflectedParcelable$1
  implements Parcelable.Creator<Controller.ReflectedParcelable>
{
  public final Controller.ReflectedParcelable createFromParcel(Parcel paramParcel)
  {
    return new Controller.ReflectedParcelable(paramParcel);
  }

  public final Controller.ReflectedParcelable[] newArray(int paramInt)
  {
    return new Controller.ReflectedParcelable[paramInt];
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.Controller.ReflectedParcelable.1
 * JD-Core Version:    0.6.2
 */