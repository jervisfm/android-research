package com.amazon.device.ads;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.Log;
import java.lang.reflect.Field;

class Controller
{
  private static String LOG_TAG = "Controller";

  public static class Dimensions extends Controller.ReflectedParcelable
  {
    public static final Parcelable.Creator<Dimensions> CREATOR = new Controller.Dimensions.1();
    public int height;
    public int width;
    public int x;
    public int y;

    public Dimensions()
    {
      this.x = -1;
      this.y = -1;
      this.width = -1;
      this.height = -1;
    }

    public Dimensions(Parcel paramParcel)
    {
      super();
    }
  }

  public static class PlayerProperties extends Controller.ReflectedParcelable
  {
    public static final Parcelable.Creator<PlayerProperties> CREATOR = new Controller.PlayerProperties.1();
    public boolean audioMuted;
    public boolean autoPlay;
    public boolean doLoop;
    public boolean inline;
    public boolean showControl;
    public String startStyle;
    public String stopStyle;

    public PlayerProperties()
    {
      this.autoPlay = true;
      this.showControl = true;
      this.doLoop = false;
      this.audioMuted = false;
      this.startStyle = "normal";
      this.stopStyle = "normal";
    }

    public PlayerProperties(Parcel paramParcel)
    {
      super();
    }

    public boolean doLoop()
    {
      return this.doLoop;
    }

    public boolean doMute()
    {
      return this.audioMuted;
    }

    public boolean exitOnComplete()
    {
      return this.stopStyle.equalsIgnoreCase("exit");
    }

    public boolean isAutoPlay()
    {
      return this.autoPlay;
    }

    public boolean isFullScreen()
    {
      return this.startStyle.equalsIgnoreCase("fullscreen");
    }

    public void muteAudio()
    {
      this.audioMuted = true;
    }

    public void setProperties(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, String paramString1, String paramString2)
    {
      this.audioMuted = paramBoolean1;
      this.autoPlay = paramBoolean2;
      this.showControl = paramBoolean3;
      this.inline = paramBoolean4;
      this.doLoop = paramBoolean5;
      this.startStyle = paramString1;
      this.stopStyle = paramString2;
    }

    public boolean showControl()
    {
      return this.showControl;
    }
  }

  public static class ReflectedParcelable
    implements Parcelable
  {
    public static final Parcelable.Creator<ReflectedParcelable> CREATOR = new Controller.ReflectedParcelable.1();

    public ReflectedParcelable()
    {
    }

    protected ReflectedParcelable(Parcel paramParcel)
    {
      Field[] arrayOfField = getClass().getDeclaredFields();
      int i = arrayOfField.length;
      int j = 0;
      while (true)
        if (j < i)
        {
          Field localField = arrayOfField[j];
          try
          {
            if (!(localField.get(this) instanceof Parcelable.Creator))
              localField.set(this, paramParcel.readValue(null));
            j++;
          }
          catch (IllegalAccessException localIllegalAccessException)
          {
            while (true)
              Log.e(Controller.LOG_TAG, "Error: Could not create object from parcel: " + localIllegalAccessException.getMessage());
          }
          catch (IllegalArgumentException localIllegalArgumentException)
          {
            while (true)
              Log.e(Controller.LOG_TAG, "Error: Could not create object from parcel: " + localIllegalArgumentException.getMessage());
          }
        }
    }

    public int describeContents()
    {
      return 0;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      Field[] arrayOfField = getClass().getDeclaredFields();
      int i = arrayOfField.length;
      int j = 0;
      while (true)
        if (j < i)
        {
          Field localField = arrayOfField[j];
          try
          {
            Object localObject = localField.get(this);
            if (!(localObject instanceof Parcelable.Creator))
              paramParcel.writeValue(localObject);
            j++;
          }
          catch (IllegalAccessException localIllegalAccessException)
          {
            while (true)
              Log.e(Controller.LOG_TAG, "Error: Could not write to parcel: " + localIllegalAccessException.getMessage());
          }
          catch (IllegalArgumentException localIllegalArgumentException)
          {
            while (true)
              Log.e(Controller.LOG_TAG, "Error: Could not write to parcel: " + localIllegalArgumentException.getMessage());
          }
        }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.Controller
 * JD-Core Version:    0.6.2
 */