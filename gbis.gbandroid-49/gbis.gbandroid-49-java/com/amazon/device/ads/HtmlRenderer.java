package com.amazon.device.ads;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build.VERSION;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout.LayoutParams;
import java.util.Map;
import java.util.Set;

class HtmlRenderer extends AdRenderer
{
  private static final String LOG_TAG = "HtmlRenderer";
  protected WebView adView_;
  protected boolean hasLoadedAd_ = false;

  protected HtmlRenderer(AdResponse paramAdResponse, AdBridge paramAdBridge)
  {
    super(paramAdResponse, paramAdBridge);
    this.adView_ = new WebView(paramAdBridge.getContext().getApplicationContext());
    this.adView_.setHorizontalScrollBarEnabled(false);
    this.adView_.setHorizontalScrollbarOverlay(false);
    this.adView_.setVerticalScrollBarEnabled(false);
    this.adView_.setVerticalScrollbarOverlay(false);
    this.adView_.getSettings().setSupportZoom(false);
    this.adView_.getSettings().setJavaScriptEnabled(true);
    this.adView_.getSettings().setPluginsEnabled(true);
    this.adView_.setBackgroundColor(0);
    this.adView_.setWebViewClient(new AdWebViewClient());
  }

  private static boolean isHoneycombVersion()
  {
    return (Build.VERSION.SDK_INT >= 11) && (Build.VERSION.SDK_INT <= 13);
  }

  protected void adLoaded(AdProperties paramAdProperties)
  {
    FrameLayout.LayoutParams localLayoutParams = new FrameLayout.LayoutParams(-2, -2, 17);
    this.bridge_.getAdLayout().addView(this.adView_, localLayoutParams);
    super.adLoaded(paramAdProperties);
  }

  protected void destroy()
  {
    this.adView_ = null;
    this.isDestroyed_ = true;
  }

  protected void prepareToGoAway()
  {
  }

  protected void removeView()
  {
    this.bridge_.getAdLayout().removeView(this.adView_);
    this.viewRemoved_ = true;
  }

  protected void render()
  {
    if ((this.adView_ == null) || (isAdViewDestroyed()))
      return;
    this.adView_.clearView();
    this.hasLoadedAd_ = false;
    String str1 = this.ad_.creative_;
    if (this.usingDownscalingLogic_)
      this.adView_.setInitialScale((int)(100.0D * this.scalingFactor_));
    while (true)
    {
      String str2 = str1.replace("<head>", "<head><script type=\"text/javascript\">htmlWillCallFinishLoad = 1;</script>");
      this.adView_.loadDataWithBaseURL("http://amazon-adsystem.amazon.com/", str2, "text/html", "utf-8", null);
      return;
      this.adView_.setInitialScale(0);
      str1 = "<html><meta name=\"viewport\" content=\"width=" + this.bridge_.getWindowWidth() + ", height=" + this.bridge_.getWindowHeight() + ", initial-scale=" + this.scalingFactor_ + "\">" + str1 + "</html>";
    }
  }

  protected boolean sendCommand(String paramString, Map<String, String> paramMap)
  {
    return true;
  }

  protected boolean shouldReuse()
  {
    return true;
  }

  protected class AdWebViewClient extends WebViewClient
  {
    protected AdWebViewClient()
    {
    }

    public void onPageFinished(WebView paramWebView, String paramString)
    {
      if (!HtmlRenderer.this.hasLoadedAd_)
      {
        HtmlRenderer.this.hasLoadedAd_ = true;
        HtmlRenderer.this.adLoaded(HtmlRenderer.this.ad_.properties_);
      }
    }

    public boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
    {
      boolean bool = true;
      Uri localUri = Uri.parse(paramString);
      if ((HtmlRenderer.this.redirectHosts_.contains(localUri.getHost())) && (!HtmlRenderer.access$000()))
        bool = false;
      while (localUri.getScheme().equals("mopub"))
        return bool;
      if ((localUri.getScheme().equals("tel")) || (localUri.getScheme().equals("voicemail:")) || (localUri.getScheme().equals("sms:")) || (localUri.getScheme().equals("mailto:")) || (localUri.getScheme().equals("geo:")) || (localUri.getScheme().equals("google.streetview:")))
      {
        Intent localIntent = new Intent("android.intent.action.VIEW", Uri.parse(paramString));
        localIntent.addFlags(268435456);
        try
        {
          HtmlRenderer.this.bridge_.getContext().startActivity(localIntent);
          return bool;
        }
        catch (ActivityNotFoundException localActivityNotFoundException)
        {
          Log.w("HtmlRenderer", "Could not handle intent with URI: " + paramString + ".");
          return bool;
        }
      }
      if (localUri.getScheme().equals("amazonmobile"))
      {
        HtmlRenderer.this.bridge_.specialUrlClicked(paramString);
        return bool;
      }
      HtmlRenderer.this.launchExternalBrowserForLink(paramString);
      return bool;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.HtmlRenderer
 * JD-Core Version:    0.6.2
 */