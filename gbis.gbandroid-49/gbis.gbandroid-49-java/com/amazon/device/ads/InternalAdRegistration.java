package com.amazon.device.ads;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONException;
import org.json.JSONObject;

final class InternalAdRegistration
{
  protected static final String ADID_PREF_NAME = "amzn-ad-id";
  protected static final String APPID_PREF_NAME = "amzn-ad-app-id";
  public static final String LOG_TAG = "AmazonAdRegistration";
  protected static final String PREFS_NAME = "AmazonMobileAds";
  protected static final long SIS_CHECKIN_INTERVAL = 86400000L;
  protected static final String THIRD_PARTY_APP_DOMAIN = "3p";
  protected static final String THIRD_PARTY_APP_NAME = "app";
  private static InternalAdRegistration instance_ = null;
  protected String amznDeviceId_ = null;
  protected Context context_ = null;
  protected DeviceInfo deviceInfo_ = null;
  protected DeviceNativeData deviceNativeData_ = null;
  protected boolean enableLogging_ = false;
  protected AmazonAdEndpoint endpoint_ = null;
  protected long lastSISCheckin_ = 0L;
  private String sdkVersionID_;
  private boolean testMode_;
  private boolean usingMSDK_ = false;

  private InternalAdRegistration(Context paramContext)
  {
    this.context_ = paramContext;
    switch (paramContext.getSharedPreferences("AmazonMobileAds", 0).getInt("0x6164616c706861", 4))
    {
    default:
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    }
    while (true)
    {
      this.amznDeviceId_ = getAmazonDeviceAdID();
      this.deviceNativeData_ = new DeviceNativeData(paramContext);
      this.deviceInfo_ = new DeviceInfo(paramContext);
      this.testMode_ = false;
      determineSDKVersion(paramContext);
      return;
      this.endpoint_ = AmazonAdEndpoint.PROD;
      continue;
      this.endpoint_ = AmazonAdEndpoint.GAMMA;
      continue;
      this.endpoint_ = AmazonAdEndpoint.DEVO;
      continue;
      this.endpoint_ = AmazonAdEndpoint.DESKTOP;
      continue;
      this.endpoint_ = AmazonAdEndpoint.PROD_MSDK;
      this.usingMSDK_ = true;
      continue;
      this.endpoint_ = AmazonAdEndpoint.GAMMA_MSDK;
      this.usingMSDK_ = true;
      continue;
      this.endpoint_ = AmazonAdEndpoint.DEVO_MSDK;
      this.usingMSDK_ = true;
    }
  }

  private static String convertStreamToString(InputStream paramInputStream)
  {
    BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(paramInputStream));
    StringBuilder localStringBuilder = new StringBuilder();
    try
    {
      while (true)
      {
        String str = localBufferedReader.readLine();
        if (str == null)
          break;
        localStringBuilder.append(str + "\n");
      }
    }
    catch (IOException localIOException2)
    {
      try
      {
        paramInputStream.close();
        return "";
        try
        {
          paramInputStream.close();
          return localStringBuilder.toString();
        }
        catch (IOException localIOException4)
        {
          return "";
        }
      }
      catch (IOException localIOException3)
      {
        return "";
      }
    }
    finally
    {
      try
      {
        paramInputStream.close();
        throw localObject;
      }
      catch (IOException localIOException1)
      {
      }
    }
    return "";
  }

  private String decodeIfNeeded(boolean paramBoolean, String paramString)
  {
    if (paramBoolean)
      paramString = Utils.getURLDecodedString(paramString);
    return paramString;
  }

  private void determineSDKVersion(Context paramContext)
  {
    this.sdkVersionID_ = null;
    try
    {
      InputStream localInputStream = ResourceLookup.getResourceFile(paramContext, "ad_resources/raw/version.json");
      if (localInputStream != null)
        this.sdkVersionID_ = new JSONObject(convertStreamToString(localInputStream)).getString("sdk_version");
      label36: if (this.sdkVersionID_ == null)
        this.sdkVersionID_ = "(DEV)";
      while (true)
      {
        this.sdkVersionID_ = ("amaznAdSDK-android-" + this.sdkVersionID_);
        return;
        if (this.sdkVersionID_.endsWith("x.x"))
          this.sdkVersionID_ += "(DEV)";
      }
    }
    catch (JSONException localJSONException)
    {
      break label36;
    }
  }

  protected static final String generateSha1Hash(String paramString)
  {
    try
    {
      MessageDigest localMessageDigest = MessageDigest.getInstance("SHA-1");
      localMessageDigest.update(paramString.getBytes());
      byte[] arrayOfByte = localMessageDigest.digest();
      StringBuffer localStringBuffer = new StringBuffer();
      for (int i = 0; i < arrayOfByte.length; i++)
        localStringBuffer.append(Integer.toHexString(0x100 | 0xFF & arrayOfByte[i]).substring(1));
      String str = localStringBuffer.toString();
      return str;
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
    }
    return "";
  }

  protected static final InternalAdRegistration getInstance()
  {
    try
    {
      InternalAdRegistration localInternalAdRegistration = instance_;
      return localInternalAdRegistration;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  protected static final InternalAdRegistration getInstance(Context paramContext)
  {
    try
    {
      if (instance_ == null)
        instance_ = new InternalAdRegistration(paramContext);
      InternalAdRegistration localInternalAdRegistration = instance_;
      return localInternalAdRegistration;
    }
    finally
    {
    }
  }

  protected static final void getMacAddress(Context paramContext, DeviceInfo paramDeviceInfo)
  {
    String str;
    try
    {
      str = ((WifiManager)paramContext.getSystemService("wifi")).getConnectionInfo().getMacAddress();
      if ((str == null) || (str.length() == 0))
      {
        paramDeviceInfo.sha1_mac = null;
        paramDeviceInfo.bad_mac = true;
        return;
      }
      if (!Pattern.compile("((([0-9a-fA-F]){1,2}[-:]){5}([0-9a-fA-F]){1,2})").matcher(str).find())
      {
        paramDeviceInfo.sha1_mac = null;
        paramDeviceInfo.bad_mac = true;
        return;
      }
    }
    catch (SecurityException localSecurityException)
    {
      Log.d("AmazonAdRegistration", "Unable to get WIFI Manager: " + localSecurityException);
      paramDeviceInfo.sha1_mac = null;
      return;
    }
    paramDeviceInfo.sha1_mac = Utils.getURLEncodedString(generateSha1Hash(str));
  }

  protected static final void getTelephonyID(Context paramContext, DeviceInfo paramDeviceInfo)
  {
    String str;
    try
    {
      paramDeviceInfo.sha1_tel = null;
      TelephonyManager localTelephonyManager = (TelephonyManager)paramContext.getSystemService("phone");
      if (localTelephonyManager.getPhoneType() == 0)
        return;
      str = localTelephonyManager.getDeviceId();
      if ((str == null) || (str.equalsIgnoreCase("000000000000000")))
      {
        paramDeviceInfo.bad_tel = true;
        return;
      }
    }
    catch (SecurityException localSecurityException)
    {
      Log.d("AmazonAdRegistration", "Unable to get Telephony Manager: " + localSecurityException);
      paramDeviceInfo.sha1_tel = null;
      return;
    }
    paramDeviceInfo.sha1_tel = Utils.getURLEncodedString(generateSha1Hash(str));
  }

  protected static final void getUdid(Context paramContext, DeviceInfo paramDeviceInfo)
  {
    String str = Settings.Secure.getString(paramContext.getContentResolver(), "android_id");
    if ((str == null) || (str.length() == 0) || (str.equalsIgnoreCase("9774d56d682e549c")))
    {
      paramDeviceInfo.sha1_udid = null;
      paramDeviceInfo.bad_udid = true;
      return;
    }
    paramDeviceInfo.sha1_udid = Utils.getURLEncodedString(generateSha1Hash(str));
  }

  protected final String getAmazonApplicationId()
  {
    return this.context_.getSharedPreferences("AmazonMobileAds", 0).getString("amzn-ad-app-id", null);
  }

  protected final String getAmazonDeviceAdID()
  {
    return this.context_.getSharedPreferences("AmazonMobileAds", 0).getString("amzn-ad-id", null);
  }

  protected final String getAmazonDeviceId()
  {
    return this.amznDeviceId_;
  }

  protected final String getAppId()
  {
    return this.deviceInfo_.appId;
  }

  protected final DeviceInfo getDeviceInfo()
  {
    return this.deviceInfo_;
  }

  protected final Map<String, String> getDeviceInfoParams(boolean paramBoolean)
  {
    HashMap localHashMap = new HashMap();
    if (this.deviceInfo_ == null)
      Log.w("AmazonAdRegistration", "deviceInfo is null");
    do
    {
      return localHashMap;
      localHashMap.put("dt", decodeIfNeeded(paramBoolean, this.deviceInfo_.dt));
      localHashMap.put("app", decodeIfNeeded(paramBoolean, this.deviceInfo_.app));
      localHashMap.put("aud", decodeIfNeeded(paramBoolean, this.deviceInfo_.aud));
      localHashMap.put("appId", decodeIfNeeded(paramBoolean, this.deviceInfo_.appId));
      if (this.deviceInfo_.sha1_mac != null)
        localHashMap.put("sha1_mac", decodeIfNeeded(paramBoolean, this.deviceInfo_.sha1_mac));
      if (this.deviceInfo_.md5_mac != null)
        localHashMap.put("md5_mac", decodeIfNeeded(paramBoolean, this.deviceInfo_.md5_mac));
      if (this.deviceInfo_.sha1_udid != null)
        localHashMap.put("sha1_udid", decodeIfNeeded(paramBoolean, this.deviceInfo_.sha1_udid));
      if (this.deviceInfo_.md5_udid != null)
        localHashMap.put("md5_udid", decodeIfNeeded(paramBoolean, this.deviceInfo_.md5_udid));
      if (this.deviceInfo_.sha1_tel != null)
        localHashMap.put("sha1_tel", decodeIfNeeded(paramBoolean, this.deviceInfo_.sha1_tel));
      if (this.deviceInfo_.md5_tel != null)
        localHashMap.put("md5_tel", decodeIfNeeded(paramBoolean, this.deviceInfo_.md5_tel));
      if (this.deviceInfo_.sha1_serial != null)
        localHashMap.put("sha1_serial", decodeIfNeeded(paramBoolean, this.deviceInfo_.sha1_serial));
      if (this.deviceInfo_.md5_serial != null)
        localHashMap.put("md5_serial", decodeIfNeeded(paramBoolean, this.deviceInfo_.md5_serial));
    }
    while (this.deviceInfo_.referrer == null);
    localHashMap.put("referrer", decodeIfNeeded(paramBoolean, this.deviceInfo_.referrer));
    return localHashMap;
  }

  protected final DeviceNativeData getDeviceNativeData()
  {
    return this.deviceNativeData_;
  }

  protected final Map<String, String> getDeviceNativeDataParams()
  {
    HashMap localHashMap = new HashMap();
    if (this.deviceNativeData_.os != null)
      localHashMap.put("os", this.deviceNativeData_.os);
    if (this.deviceNativeData_.model != null)
      localHashMap.put("model", this.deviceNativeData_.model);
    if (this.deviceNativeData_.make != null)
      localHashMap.put("make", this.deviceNativeData_.make);
    if (this.deviceNativeData_.osVersion != null)
      localHashMap.put("osVersion", this.deviceNativeData_.osVersion);
    if (this.deviceNativeData_.screenSize != null)
      localHashMap.put("screenSize", this.deviceNativeData_.screenSize);
    if (this.deviceNativeData_.sf != null)
      localHashMap.put("sf", this.deviceNativeData_.sf);
    return localHashMap;
  }

  protected final AmazonAdEndpoint getEndpoint()
  {
    return this.endpoint_;
  }

  protected final String getSDKVersionID()
  {
    return this.sdkVersionID_;
  }

  protected final boolean getTestMode()
  {
    return this.testMode_;
  }

  protected final boolean isLoggingEnabled()
  {
    return this.enableLogging_;
  }

  protected final boolean isRegistered()
  {
    return (this.amznDeviceId_ != null) && (this.amznDeviceId_.length() > 0);
  }

  protected final void ping()
  {
    AdRegistrationTasks.A9Request localA9Request = new AdRegistrationTasks.A9Request(this.context_, "Ping", this.endpoint_, this.deviceInfo_, this.deviceNativeData_, this.amznDeviceId_);
    new AdRegistrationTasks.PingTask().execute(new AdRegistrationTasks.A9Request[] { localA9Request });
  }

  protected final void register()
  {
    AdRegistrationTasks.A9Request localA9Request = new AdRegistrationTasks.A9Request(this.context_, "RegisterDevice", this.endpoint_, this.deviceInfo_, this.deviceNativeData_, this.amznDeviceId_);
    new AdRegistrationTasks.RegisterDeviceTask().execute(new AdRegistrationTasks.A9Request[] { localA9Request });
  }

  protected final void registerIfNeeded()
  {
    if (this.amznDeviceId_ != null)
    {
      long l = System.currentTimeMillis();
      if (l - this.lastSISCheckin_ > 86400000L)
      {
        this.lastSISCheckin_ = l;
        updateDeviceInfo();
        ping();
      }
      return;
    }
    register();
  }

  protected final void setApplicationId(String paramString)
  {
    if ((paramString == null) || (paramString.length() == 0))
      throw new IllegalArgumentException("ApplicationId must not be null or empty.");
    if (paramString.equals(getAmazonApplicationId()))
    {
      if (this.deviceInfo_.appId == null)
        this.deviceInfo_.appId = Utils.getURLEncodedString(paramString);
      return;
    }
    this.deviceInfo_.appId = Utils.getURLEncodedString(paramString);
    this.amznDeviceId_ = null;
    SharedPreferences.Editor localEditor = this.context_.getSharedPreferences("AmazonMobileAds", 0).edit();
    localEditor.putString("amzn-ad-app-id", paramString);
    localEditor.remove("amzn-ad-id");
    localEditor.commit();
  }

  protected final void setApplicationName(String paramString)
  {
    this.deviceInfo_.app = Utils.getURLEncodedString(paramString);
  }

  protected final void setAuthenticationDomain(String paramString)
  {
    this.deviceInfo_.aud = Utils.getURLEncodedString(paramString);
  }

  protected final void setLoggingEnabled(boolean paramBoolean)
  {
    this.enableLogging_ = paramBoolean;
  }

  final void setTestMode(boolean paramBoolean)
  {
    this.testMode_ = paramBoolean;
  }

  protected final void updateDeviceInfo()
  {
    AdRegistrationTasks.A9Request localA9Request = new AdRegistrationTasks.A9Request(this.context_, "UpdateDeviceInfo", this.endpoint_, this.deviceInfo_, this.deviceNativeData_, this.amznDeviceId_);
    new AdRegistrationTasks.UpdateDeviceInfoTask().execute(new AdRegistrationTasks.A9Request[] { localA9Request });
  }

  protected final boolean usingMSDK()
  {
    return this.usingMSDK_;
  }

  protected static enum AmazonAdEndpoint
  {
    public final String aaxEndpoint;
    public final String aaxHandler;
    public final String adSystemEndpoint;
    public final String adSystemHandler;

    static
    {
      GAMMA = new AmazonAdEndpoint("GAMMA", 1, "s-gamma.amazon-adsystem.com", "/api3", "aax-us-east-gamma.amazon-adsystem.com", "/x/getad");
      DEVO = new AmazonAdEndpoint("DEVO", 2, "s-beta.amazon-adsystem.com", "/api3", "aax-beta.integ.amazon.com", "/x/getad");
      DESKTOP = new AmazonAdEndpoint("DESKTOP", 3, "s-beta.amazon-adsystem.com", "/api3", "antonf-test.integ.amazon.com:2828", "/x/getad");
      PROD_MSDK = new AmazonAdEndpoint("PROD_MSDK", 4, "s.amazon-adsystem.com", "/api3", "aax-us-east.amazon-adsystem.com", "/x/msdk");
      GAMMA_MSDK = new AmazonAdEndpoint("GAMMA_MSDK", 5, "s-gamma.amazon-adsystem.com", "/api3", "aax-us-east-gamma.amazon-adsystem.com", "/x/msdk");
      DEVO_MSDK = new AmazonAdEndpoint("DEVO_MSDK", 6, "s-beta.amazon-adsystem.com", "/api3", "aax-beta.integ.amazon.com", "/x/msdk");
      AmazonAdEndpoint[] arrayOfAmazonAdEndpoint = new AmazonAdEndpoint[7];
      arrayOfAmazonAdEndpoint[0] = PROD;
      arrayOfAmazonAdEndpoint[1] = GAMMA;
      arrayOfAmazonAdEndpoint[2] = DEVO;
      arrayOfAmazonAdEndpoint[3] = DESKTOP;
      arrayOfAmazonAdEndpoint[4] = PROD_MSDK;
      arrayOfAmazonAdEndpoint[5] = GAMMA_MSDK;
      arrayOfAmazonAdEndpoint[6] = DEVO_MSDK;
    }

    private AmazonAdEndpoint(String paramString1, String paramString2, String paramString3, String paramString4)
    {
      this.adSystemEndpoint = paramString1;
      this.adSystemHandler = paramString2;
      this.aaxEndpoint = paramString3;
      this.aaxHandler = paramString4;
    }
  }

  protected static class DeviceInfo
  {
    public String app = "app";
    public String appId = null;
    public String aud = "3p";
    public boolean bad_mac = false;
    public boolean bad_serial = false;
    public boolean bad_tel = false;
    public boolean bad_udid = false;
    public String dt = "android";
    public String md5_mac = null;
    public String md5_serial = null;
    public String md5_tel = null;
    public String md5_udid = null;
    public String referrer = null;
    public String sha1_mac = null;
    public String sha1_serial = null;
    public String sha1_tel = null;
    public String sha1_udid = null;
    public String ua = null;

    public DeviceInfo(Context paramContext)
    {
      InternalAdRegistration.getMacAddress(paramContext, this);
      InternalAdRegistration.getUdid(paramContext, this);
      InternalAdRegistration.getTelephonyID(paramContext, this);
      setSerial();
    }

    protected void setSerial()
    {
      try
      {
        String str = (String)Build.class.getDeclaredField("SERIAL").get(Build.class);
        if ((str == null) || (str.length() == 0) || (str.equalsIgnoreCase("unknown")))
        {
          this.bad_serial = true;
          return;
        }
        this.sha1_serial = Utils.getURLEncodedString(InternalAdRegistration.generateSha1Hash(str));
        return;
      }
      catch (Exception localException)
      {
      }
    }
  }

  protected static class DeviceNativeData
  {
    public JSONObject json;
    public final String make = Build.MANUFACTURER;
    public final String model = Build.MODEL;
    public String orientation = "";
    public final String os = "Android";
    public final String osVersion = Build.VERSION.RELEASE;
    public final String screenSize;
    public final String sf;
    public String urlEncodedJson;

    public DeviceNativeData(Context paramContext)
    {
      WindowManager localWindowManager = (WindowManager)paramContext.getSystemService("window");
      DisplayMetrics localDisplayMetrics = new DisplayMetrics();
      localWindowManager.getDefaultDisplay().getMetrics(localDisplayMetrics);
      this.screenSize = (String.valueOf(localDisplayMetrics.widthPixels) + "x" + String.valueOf(localDisplayMetrics.heightPixels));
      this.sf = String.valueOf(localDisplayMetrics.scaledDensity);
      this.json = new JSONObject();
      try
      {
        this.json.put("os", this.os);
        this.json.put("model", this.model);
        this.json.put("make", this.make);
        this.json.put("osVersion", this.osVersion);
        this.json.put("screenSize", this.screenSize);
        this.json.put("sf", this.sf);
        label210: this.urlEncodedJson = Utils.getURLEncodedString(this.json.toString());
        return;
      }
      catch (JSONException localJSONException)
      {
        break label210;
      }
    }

    public String getJsonEncodedWithOrientation()
    {
      try
      {
        this.json.put("orientation", this.orientation);
        label14: this.urlEncodedJson = Utils.getURLEncodedString(this.json.toString());
        return this.urlEncodedJson;
      }
      catch (JSONException localJSONException)
      {
        break label14;
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.InternalAdRegistration
 * JD-Core Version:    0.6.2
 */