package com.amazon.device.ads;

class Log
{
  private static final String LOGTAG = "AmazonMobileAds ";

  public static void d(String paramString1, String paramString2)
  {
    if ((InternalAdRegistration.getInstance() != null) && (InternalAdRegistration.getInstance().isLoggingEnabled()))
      android.util.Log.d("AmazonMobileAds " + paramString1, paramString2);
  }

  public static void d(String paramString1, String paramString2, Throwable paramThrowable)
  {
    if ((InternalAdRegistration.getInstance() != null) && (InternalAdRegistration.getInstance().isLoggingEnabled()))
      android.util.Log.d("AmazonMobileAds " + paramString1, paramString2, paramThrowable);
  }

  public static void e(String paramString1, String paramString2)
  {
    if ((InternalAdRegistration.getInstance() != null) && (InternalAdRegistration.getInstance().isLoggingEnabled()))
      android.util.Log.e("AmazonMobileAds " + paramString1, paramString2);
  }

  public static void e(String paramString1, String paramString2, Throwable paramThrowable)
  {
    if ((InternalAdRegistration.getInstance() != null) && (InternalAdRegistration.getInstance().isLoggingEnabled()))
      android.util.Log.e("AmazonMobileAds " + paramString1, paramString2, paramThrowable);
  }

  public static void i(String paramString1, String paramString2)
  {
    if ((InternalAdRegistration.getInstance() != null) && (InternalAdRegistration.getInstance().isLoggingEnabled()))
      android.util.Log.i("AmazonMobileAds " + paramString1, paramString2);
  }

  public static void i(String paramString1, String paramString2, Throwable paramThrowable)
  {
    if ((InternalAdRegistration.getInstance() != null) && (InternalAdRegistration.getInstance().isLoggingEnabled()))
      android.util.Log.i("AmazonMobileAds " + paramString1, paramString2, paramThrowable);
  }

  public static void v(String paramString1, String paramString2)
  {
    if ((InternalAdRegistration.getInstance() != null) && (InternalAdRegistration.getInstance().isLoggingEnabled()))
      android.util.Log.v("AmazonMobileAds " + paramString1, paramString2);
  }

  public static void v(String paramString1, String paramString2, Throwable paramThrowable)
  {
    if ((InternalAdRegistration.getInstance() != null) && (InternalAdRegistration.getInstance().isLoggingEnabled()))
      android.util.Log.v("AmazonMobileAds " + paramString1, paramString2, paramThrowable);
  }

  public static void w(String paramString1, String paramString2)
  {
    if ((InternalAdRegistration.getInstance() != null) && (InternalAdRegistration.getInstance().isLoggingEnabled()))
      android.util.Log.w("AmazonMobileAds " + paramString1, paramString2);
  }

  public static void w(String paramString1, String paramString2, Throwable paramThrowable)
  {
    if ((InternalAdRegistration.getInstance() != null) && (InternalAdRegistration.getInstance().isLoggingEnabled()))
      android.util.Log.w("AmazonMobileAds " + paramString1, paramString2, paramThrowable);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.Log
 * JD-Core Version:    0.6.2
 */