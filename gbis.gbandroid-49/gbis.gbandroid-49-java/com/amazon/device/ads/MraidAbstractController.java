package com.amazon.device.ads;

class MraidAbstractController
{
  private final MraidView mView;

  MraidAbstractController(MraidView paramMraidView)
  {
    this.mView = paramMraidView;
  }

  public MraidView getView()
  {
    return this.mView;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.MraidAbstractController
 * JD-Core Version:    0.6.2
 */