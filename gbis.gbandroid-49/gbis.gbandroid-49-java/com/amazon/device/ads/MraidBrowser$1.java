package com.amazon.device.ads;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.Toast;

class MraidBrowser$1 extends WebViewClient
{
  MraidBrowser$1(MraidBrowser paramMraidBrowser)
  {
  }

  public void onPageFinished(WebView paramWebView, String paramString)
  {
    super.onPageFinished(paramWebView, paramString);
    if (paramWebView.canGoBack())
      MraidBrowser.access$100(this.this$0).setImageBitmap(ResourceLookup.bitmapFromJar(this.this$0.getApplicationContext(), "ad_resources/drawable/amazon_ads_leftarrow.png"));
    while (paramWebView.canGoForward())
    {
      MraidBrowser.access$000(this.this$0).setImageBitmap(ResourceLookup.bitmapFromJar(this.this$0.getApplicationContext(), "ad_resources/drawable/amazon_ads_rightarrow.png"));
      return;
      MraidBrowser.access$100(this.this$0).setImageBitmap(ResourceLookup.bitmapFromJar(this.this$0.getApplicationContext(), "ad_resources/drawable/amazon_ads_unleftarrow.png"));
    }
    MraidBrowser.access$000(this.this$0).setImageBitmap(ResourceLookup.bitmapFromJar(this.this$0.getApplicationContext(), "ad_resources/drawable/amazon_ads_unrightarrow.png"));
  }

  public void onPageStarted(WebView paramWebView, String paramString, Bitmap paramBitmap)
  {
    super.onPageStarted(paramWebView, paramString, paramBitmap);
    MraidBrowser.access$000(this.this$0).setImageBitmap(ResourceLookup.bitmapFromJar(this.this$0.getApplicationContext(), "ad_resources/drawable/amazon_ads_unrightarrow.png"));
  }

  public void onReceivedError(WebView paramWebView, int paramInt, String paramString1, String paramString2)
  {
    Toast.makeText((Activity)paramWebView.getContext(), "MRAID error: " + paramString1, 0).show();
  }

  public boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
  {
    if (paramString == null);
    while ((!paramString.startsWith("market:")) && (!paramString.startsWith("tel:")) && (!paramString.startsWith("voicemail:")) && (!paramString.startsWith("sms:")) && (!paramString.startsWith("mailto:")) && (!paramString.startsWith("geo:")) && (!paramString.startsWith("google.streetview:")))
      return false;
    this.this$0.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(paramString)));
    return true;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.MraidBrowser.1
 * JD-Core Version:    0.6.2
 */