package com.amazon.device.ads;

import android.app.Activity;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

class MraidBrowser$2 extends WebChromeClient
{
  MraidBrowser$2(MraidBrowser paramMraidBrowser)
  {
  }

  public void onProgressChanged(WebView paramWebView, int paramInt)
  {
    Activity localActivity = (Activity)paramWebView.getContext();
    localActivity.setTitle("Loading...");
    localActivity.setProgress(paramInt * 100);
    if (paramInt == 100)
      localActivity.setTitle(paramWebView.getUrl());
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.MraidBrowser.2
 * JD-Core Version:    0.6.2
 */