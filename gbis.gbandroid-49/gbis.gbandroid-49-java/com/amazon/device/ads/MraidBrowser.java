package com.amazon.device.ads;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.Window;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

public class MraidBrowser extends Activity
{
  public static final String URL_EXTRA = "extra_url";
  private ImageButton mBrowserBackButton;
  private ImageButton mBrowserForwardButton;
  private ImageButton mCloseButton;
  private ImageButton mRefreshButton;
  private WebView mWebView;

  private ImageButton createButton(String paramString)
  {
    ImageButton localImageButton = new ImageButton(this);
    localImageButton.setImageBitmap(ResourceLookup.bitmapFromJar(getApplication(), paramString));
    LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(-2, -2);
    localLayoutParams.weight = 25.0F;
    localLayoutParams.gravity = 16;
    localImageButton.setLayoutParams(localLayoutParams);
    localImageButton.setBackgroundColor(0);
    return localImageButton;
  }

  private void enableCookies()
  {
    CookieSyncManager.createInstance(this);
    CookieSyncManager.getInstance().startSync();
  }

  private void initializeButtons(Intent paramIntent)
  {
    this.mBrowserBackButton.setOnClickListener(new MraidBrowser.3(this));
    this.mBrowserForwardButton.setOnClickListener(new MraidBrowser.4(this));
    this.mRefreshButton.setOnClickListener(new MraidBrowser.5(this));
    this.mCloseButton.setOnClickListener(new MraidBrowser.6(this));
  }

  private void initializeEntireView(Intent paramIntent)
  {
    LinearLayout localLinearLayout1 = new LinearLayout(this);
    localLinearLayout1.setId(10280);
    localLinearLayout1.setWeightSum(100.0F);
    RelativeLayout.LayoutParams localLayoutParams1 = new RelativeLayout.LayoutParams(-1, -2);
    localLayoutParams1.addRule(12);
    localLinearLayout1.setLayoutParams(localLayoutParams1);
    localLinearLayout1.setBackgroundDrawable(new BitmapDrawable(ResourceLookup.bitmapFromJar(getApplicationContext(), "resources/drawable/amazon_ads_bkgrnd.png")));
    this.mBrowserBackButton = createButton("ad_resources/drawable/amazon_ads_leftarrow.png");
    this.mBrowserForwardButton = createButton("ad_resources/drawable/amazon_ads_rightarrow.png");
    this.mRefreshButton = createButton("ad_resources/drawable/amazon_ads_refresh.png");
    this.mCloseButton = createButton("ad_resources/drawable/amazon_ads_close.png");
    localLinearLayout1.addView(this.mBrowserBackButton);
    localLinearLayout1.addView(this.mBrowserForwardButton);
    localLinearLayout1.addView(this.mRefreshButton);
    localLinearLayout1.addView(this.mCloseButton);
    this.mWebView = new WebView(this);
    RelativeLayout.LayoutParams localLayoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
    localLayoutParams2.addRule(2, localLinearLayout1.getId());
    this.mWebView.setLayoutParams(localLayoutParams2);
    RelativeLayout localRelativeLayout = new RelativeLayout(this);
    localRelativeLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
    localRelativeLayout.addView(this.mWebView);
    localRelativeLayout.addView(localLinearLayout1);
    LinearLayout localLinearLayout2 = new LinearLayout(this);
    localLinearLayout2.setOrientation(1);
    localLinearLayout2.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
    localLinearLayout2.addView(localRelativeLayout);
    setContentView(localLinearLayout2);
  }

  private void initializeWebView(Intent paramIntent)
  {
    this.mWebView.getSettings().setJavaScriptEnabled(true);
    this.mWebView.loadUrl(paramIntent.getStringExtra("extra_url"));
    this.mWebView.setWebViewClient(new MraidBrowser.1(this));
    this.mWebView.setWebChromeClient(new MraidBrowser.2(this));
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    getWindow().requestFeature(2);
    getWindow().setFeatureInt(2, -1);
    Intent localIntent = getIntent();
    initializeEntireView(localIntent);
    initializeWebView(localIntent);
    initializeButtons(localIntent);
    enableCookies();
  }

  protected void onPause()
  {
    super.onPause();
    CookieSyncManager.getInstance().stopSync();
  }

  protected void onResume()
  {
    super.onResume();
    CookieSyncManager.getInstance().startSync();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.MraidBrowser
 * JD-Core Version:    0.6.2
 */