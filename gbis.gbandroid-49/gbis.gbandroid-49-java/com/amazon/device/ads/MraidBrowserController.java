package com.amazon.device.ads;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

class MraidBrowserController extends MraidAbstractController
{
  private static final String LOGTAG = "MraidBrowserController";

  MraidBrowserController(MraidView paramMraidView)
  {
    super(paramMraidView);
  }

  protected void executeAmazonMobileCallback(MraidView paramMraidView, String paramString)
  {
    if (paramMraidView.getOnSpecialUrlClickListener() != null)
    {
      paramMraidView.getDisplayController().close();
      paramMraidView.getOnSpecialUrlClickListener().onSpecialUrlClick(paramMraidView, paramString);
    }
  }

  protected void open(String paramString)
  {
    Log.d("MraidBrowserController", "Opening in-app browser: " + paramString);
    MraidView localMraidView = getView();
    Uri localUri = Uri.parse(paramString);
    if (localUri.getScheme().equals("amazonmobile"))
    {
      executeAmazonMobileCallback(localMraidView, paramString);
      return;
    }
    String str = localUri.getQueryParameter("d.url");
    if ((str != null) && (str.startsWith("amazonmobile:")))
    {
      executeAmazonMobileCallback(localMraidView, str);
      return;
    }
    if (localMraidView.getOnOpenListener() != null)
      localMraidView.getOnOpenListener().onOpen(localMraidView);
    Context localContext = getView().getContext();
    Intent localIntent = new Intent(localContext, MraidBrowser.class);
    localIntent.putExtra("extra_url", paramString);
    localIntent.addFlags(268435456);
    localContext.startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.MraidBrowserController
 * JD-Core Version:    0.6.2
 */