package com.amazon.device.ads;

import java.util.ArrayList;
import java.util.Map;

abstract class MraidCommand
{
  protected Map<String, String> mParams;
  protected MraidView mView;

  MraidCommand(Map<String, String> paramMap, MraidView paramMraidView)
  {
    this.mParams = paramMap;
    this.mView = paramMraidView;
  }

  abstract void execute();

  protected boolean getBooleanFromParamsForKey(String paramString)
  {
    return "true".equals(this.mParams.get(paramString));
  }

  protected float getFloatFromParamsForKey(String paramString)
  {
    if ((String)this.mParams.get(paramString) == null)
      return 0.0F;
    try
    {
      float f = Float.parseFloat(paramString);
      return f;
    }
    catch (NumberFormatException localNumberFormatException)
    {
    }
    return 0.0F;
  }

  protected Integer[] getIntArrayFromParamsFroKey(String paramString)
  {
    String str1 = (String)this.mParams.get(paramString);
    if (str1 == null)
      return null;
    String[] arrayOfString = str1.split(",");
    ArrayList localArrayList = new ArrayList();
    int i = arrayOfString.length;
    int j = 0;
    while (true)
      if (j < i)
      {
        String str2 = arrayOfString[j];
        try
        {
          localArrayList.add(Integer.valueOf(Integer.parseInt(str2, 10)));
          j++;
        }
        catch (NumberFormatException localNumberFormatException)
        {
          while (true)
            localArrayList.add(Integer.valueOf(-1));
        }
      }
    return (Integer[])localArrayList.toArray(new Integer[localArrayList.size()]);
  }

  protected int getIntFromParamsForKey(String paramString)
  {
    String str = (String)this.mParams.get(paramString);
    if (str == null)
      return -1;
    try
    {
      int i = Integer.parseInt(str, 10);
      return i;
    }
    catch (NumberFormatException localNumberFormatException)
    {
    }
    return -1;
  }

  protected String getStringFromParamsForKey(String paramString)
  {
    return (String)this.mParams.get(paramString);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.MraidCommand
 * JD-Core Version:    0.6.2
 */