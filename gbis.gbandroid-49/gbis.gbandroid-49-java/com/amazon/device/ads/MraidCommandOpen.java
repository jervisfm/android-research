package com.amazon.device.ads;

import java.util.Map;

class MraidCommandOpen extends MraidCommand
{
  MraidCommandOpen(Map<String, String> paramMap, MraidView paramMraidView)
  {
    super(paramMap, paramMraidView);
  }

  void execute()
  {
    String str = getStringFromParamsForKey("url");
    this.mView.getBrowserController().open(str);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.MraidCommandOpen
 * JD-Core Version:    0.6.2
 */