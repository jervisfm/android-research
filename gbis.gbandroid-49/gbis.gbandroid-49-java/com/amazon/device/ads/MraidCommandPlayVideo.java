package com.amazon.device.ads;

import java.util.Map;

class MraidCommandPlayVideo extends MraidCommand
{
  MraidCommandPlayVideo(Map<String, String> paramMap, MraidView paramMraidView)
  {
    super(paramMap, paramMraidView);
  }

  void execute()
  {
    Integer[] arrayOfInteger = getIntArrayFromParamsFroKey("position");
    Controller.Dimensions localDimensions1;
    if (arrayOfInteger[0].intValue() != -1)
    {
      localDimensions1 = new Controller.Dimensions();
      localDimensions1.y = arrayOfInteger[0].intValue();
      localDimensions1.x = arrayOfInteger[1].intValue();
      localDimensions1.width = arrayOfInteger[2].intValue();
      localDimensions1.height = arrayOfInteger[3].intValue();
    }
    for (Controller.Dimensions localDimensions2 = localDimensions1; ; localDimensions2 = null)
    {
      String str = getStringFromParamsForKey("url");
      Controller.PlayerProperties localPlayerProperties = new Controller.PlayerProperties();
      localPlayerProperties.setProperties(getBooleanFromParamsForKey("audioMuted"), getBooleanFromParamsForKey("autoPlay"), getBooleanFromParamsForKey("controls"), true, getBooleanFromParamsForKey("loop"), getStringFromParamsForKey("startStyle"), getStringFromParamsForKey("stopStyle"));
      this.mView.getDisplayController().playVideo(str, localDimensions2, localPlayerProperties);
      return;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.MraidCommandPlayVideo
 * JD-Core Version:    0.6.2
 */