package com.amazon.device.ads;

import java.util.Map;

final class MraidCommandRegistry$4
  implements MraidCommandRegistry.MraidCommandFactory
{
  public final MraidCommand create(Map<String, String> paramMap, MraidView paramMraidView)
  {
    return new MraidCommandOpen(paramMap, paramMraidView);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.MraidCommandRegistry.4
 * JD-Core Version:    0.6.2
 */