package com.amazon.device.ads;

import java.util.HashMap;
import java.util.Map;

class MraidCommandRegistry
{
  private static Map<String, MraidCommandFactory> commandMap;

  static
  {
    HashMap localHashMap = new HashMap();
    commandMap = localHashMap;
    localHashMap.put("close", new MraidCommandRegistry.1());
    commandMap.put("expand", new MraidCommandRegistry.2());
    commandMap.put("usecustomclose", new MraidCommandRegistry.3());
    commandMap.put("open", new MraidCommandRegistry.4());
    commandMap.put("playVideo", new MraidCommandRegistry.5());
  }

  static MraidCommand createCommand(String paramString, Map<String, String> paramMap, MraidView paramMraidView)
  {
    MraidCommandFactory localMraidCommandFactory = (MraidCommandFactory)commandMap.get(paramString);
    if (localMraidCommandFactory != null)
      return localMraidCommandFactory.create(paramMap, paramMraidView);
    return null;
  }

  private static abstract interface MraidCommandFactory
  {
    public abstract MraidCommand create(Map<String, String> paramMap, MraidView paramMraidView);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.MraidCommandRegistry
 * JD-Core Version:    0.6.2
 */