package com.amazon.device.ads;

import java.util.Map;

class MraidCommandUseCustomClose extends MraidCommand
{
  MraidCommandUseCustomClose(Map<String, String> paramMap, MraidView paramMraidView)
  {
    super(paramMap, paramMraidView);
  }

  void execute()
  {
    boolean bool = getBooleanFromParamsForKey("shouldUseCustomClose");
    this.mView.getDisplayController().useCustomClose(bool);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.MraidCommandUseCustomClose
 * JD-Core Version:    0.6.2
 */