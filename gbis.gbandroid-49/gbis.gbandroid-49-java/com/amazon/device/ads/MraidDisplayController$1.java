package com.amazon.device.ads;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

class MraidDisplayController$1 extends BroadcastReceiver
{
  private int mLastRotation;

  MraidDisplayController$1(MraidDisplayController paramMraidDisplayController)
  {
  }

  public void onReceive(Context paramContext, Intent paramIntent)
  {
    if (paramIntent.getAction().equals("android.intent.action.CONFIGURATION_CHANGED"))
    {
      int i = MraidDisplayController.access$000(this.this$0);
      if (i != this.mLastRotation)
      {
        this.mLastRotation = i;
        MraidDisplayController.access$100(this.this$0, this.mLastRotation);
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.MraidDisplayController.1
 * JD-Core Version:    0.6.2
 */