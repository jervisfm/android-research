package com.amazon.device.ads;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.URLUtil;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import java.util.ArrayList;

class MraidDisplayController extends MraidAbstractController
{
  private static final int CLOSE_BUTTON_SIZE_DP = 50;
  private static final String LOGTAG = "MraidDisplayController";
  private boolean mAdWantsCustomCloseButton;
  private ImageView mCloseButton;
  private Context mContext;
  protected float mDensity;
  private final MraidView.ExpansionStyle mExpansionStyle;
  private boolean mIsViewable;
  private final MraidView.NativeCloseButtonStyle mNativeCloseButtonStyle;
  private BroadcastReceiver mOrientationBroadcastReceiver = new MraidDisplayController.1(this);
  private final int mOriginalRequestedOrientation;
  FrameLayout mPlaceholderView;
  private boolean mRegistered;
  private FrameLayout mRootView;
  protected int mScreenHeight;
  protected int mScreenWidth;
  private MraidView mTwoPartExpansionView;
  private int mViewHeight;
  private int mViewIndexInParent;
  private MraidView.ViewState mViewState = MraidView.ViewState.HIDDEN;
  private int mViewWidth;
  private double scalingFactor_;
  private boolean usingDownscalingLogic_;
  private AdVideoPlayer vidPlayer_;
  private boolean vidPlaying_ = false;
  private int windowHeight_;
  private int windowWidth_;

  MraidDisplayController(MraidView paramMraidView, MraidView.ExpansionStyle paramExpansionStyle, MraidView.NativeCloseButtonStyle paramNativeCloseButtonStyle)
  {
    super(paramMraidView);
    this.mScreenWidth = i;
    this.mScreenHeight = i;
    this.mRegistered = false;
    this.mExpansionStyle = paramExpansionStyle;
    this.mNativeCloseButtonStyle = paramNativeCloseButtonStyle;
    this.windowHeight_ = paramMraidView.getWindowHeight();
    this.windowWidth_ = paramMraidView.getWindowWidth();
    this.scalingFactor_ = paramMraidView.getScalingFactor();
    this.usingDownscalingLogic_ = paramMraidView.isUsingDownscalingLogic();
    this.mContext = getView().getContext();
    if ((this.mContext instanceof Activity))
      i = ((Activity)this.mContext).getRequestedOrientation();
    this.mOriginalRequestedOrientation = i;
    this.vidPlayer_ = new AdVideoPlayer(this.mContext);
    initialize();
  }

  private ViewGroup createExpansionViewContainer(View paramView, int paramInt1, int paramInt2)
  {
    int i = (int)(0.5F + 50.0F * this.mDensity);
    if (paramInt1 < i)
      paramInt1 = i;
    if (paramInt2 < i)
      paramInt2 = i;
    RelativeLayout localRelativeLayout = new RelativeLayout(getView().getContext());
    localRelativeLayout.setId(101);
    View localView = new View(getView().getContext());
    localView.setBackgroundColor(0);
    localView.setOnTouchListener(new MraidDisplayController.4(this));
    localRelativeLayout.addView(localView, new RelativeLayout.LayoutParams(-1, -1));
    FrameLayout localFrameLayout = new FrameLayout(getView().getContext());
    localFrameLayout.setId(102);
    localFrameLayout.addView(paramView, new RelativeLayout.LayoutParams(-1, -1));
    RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(paramInt1, paramInt2);
    localLayoutParams.addRule(13);
    localRelativeLayout.addView(localFrameLayout, localLayoutParams);
    return localRelativeLayout;
  }

  private int getDisplayRotation()
  {
    return ((WindowManager)getView().getContext().getSystemService("window")).getDefaultDisplay().getOrientation();
  }

  private void initialize()
  {
    this.mViewState = MraidView.ViewState.LOADING;
    initializeScreenMetrics();
    registerRecievers();
  }

  private void initializeScreenMetrics()
  {
    Context localContext = getView().getContext();
    DisplayMetrics localDisplayMetrics = new DisplayMetrics();
    ((WindowManager)localContext.getSystemService("window")).getDefaultDisplay().getMetrics(localDisplayMetrics);
    this.mDensity = localDisplayMetrics.density;
    int j;
    int i;
    if ((localContext instanceof Activity))
    {
      Window localWindow = ((Activity)localContext).getWindow();
      Rect localRect = new Rect();
      localWindow.getDecorView().getWindowVisibleDisplayFrame(localRect);
      j = localRect.top;
      i = localWindow.findViewById(16908290).getTop() - j;
    }
    while (true)
    {
      int k = localDisplayMetrics.widthPixels;
      int m = localDisplayMetrics.heightPixels - j - i;
      this.mScreenWidth = ((int)(k * (160.0D / localDisplayMetrics.densityDpi)));
      this.mScreenHeight = ((int)(m * (160.0D / localDisplayMetrics.densityDpi)));
      return;
      i = 0;
      j = 0;
    }
  }

  private void onOrientationChanged(int paramInt)
  {
    initializeScreenMetrics();
    if (this.mRegistered == true)
      getView().fireChangeEventForProperty(MraidScreenSizeProperty.createWithSize(this.mScreenWidth, this.mScreenHeight));
  }

  private void resetViewToDefaultState()
  {
    FrameLayout localFrameLayout = (FrameLayout)this.mRootView.findViewById(102);
    RelativeLayout localRelativeLayout = (RelativeLayout)this.mRootView.findViewById(101);
    setNativeCloseButtonEnabled(false);
    localFrameLayout.removeAllViewsInLayout();
    this.mRootView.removeView(localRelativeLayout);
    getView().requestLayout();
    ViewGroup localViewGroup = (ViewGroup)this.mPlaceholderView.getParent();
    localViewGroup.addView(getView(), this.mViewIndexInParent, new ViewGroup.LayoutParams(this.mViewWidth, this.mViewHeight));
    localViewGroup.removeView(this.mPlaceholderView);
    localViewGroup.invalidate();
  }

  private void setOrientationLockEnabled(boolean paramBoolean)
  {
    Context localContext = getView().getContext();
    try
    {
      Activity localActivity = (Activity)localContext;
      if (paramBoolean);
      for (int i = Utils.determineCanonicalScreenOrientation(localActivity); ; i = this.mOriginalRequestedOrientation)
      {
        localActivity.setRequestedOrientation(i);
        return;
      }
    }
    catch (Exception localException)
    {
      Log.d("MraidDisplayController", "Unable to modify device orientation.");
    }
  }

  private void swapViewWithPlaceholderView()
  {
    ViewGroup localViewGroup = (ViewGroup)getView().getParent();
    if (localViewGroup == null)
      return;
    this.mPlaceholderView = new FrameLayout(getView().getContext());
    int i = localViewGroup.getChildCount();
    for (int j = 0; (j < i) && (localViewGroup.getChildAt(j) != getView()); j++);
    this.mViewIndexInParent = j;
    this.mViewHeight = getView().getHeight();
    this.mViewWidth = getView().getWidth();
    localViewGroup.addView(this.mPlaceholderView, j, new ViewGroup.LayoutParams(getView().getWidth(), getView().getHeight()));
    localViewGroup.removeView(getView());
  }

  protected boolean checkViewable()
  {
    return true;
  }

  protected void close()
  {
    if (this.vidPlaying_)
    {
      this.vidPlayer_.releasePlayer();
      this.vidPlaying_ = false;
    }
    if (this.mViewState == MraidView.ViewState.EXPANDED)
    {
      resetViewToDefaultState();
      setOrientationLockEnabled(false);
      this.mViewState = MraidView.ViewState.DEFAULT;
      getView().fireChangeEventForProperty(MraidStateProperty.createWithViewState(this.mViewState));
    }
    while (true)
    {
      if (getView().getOnCloseListener() != null)
        getView().getOnCloseListener().onClose(getView(), this.mViewState);
      return;
      if (this.mViewState == MraidView.ViewState.DEFAULT)
      {
        getView().setVisibility(4);
        this.mViewState = MraidView.ViewState.HIDDEN;
        getView().fireChangeEventForProperty(MraidStateProperty.createWithViewState(this.mViewState));
      }
    }
  }

  public void destroy()
  {
    try
    {
      getView().getContext().unregisterReceiver(this.mOrientationBroadcastReceiver);
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      while (localIllegalArgumentException.getMessage().contains("Receiver not registered"));
      throw localIllegalArgumentException;
    }
  }

  protected void expand(String paramString, int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2)
  {
    if ((this.mExpansionStyle == MraidView.ExpansionStyle.DISABLED) || (this.mViewState == MraidView.ViewState.EXPANDED));
    do
    {
      return;
      if ((paramString != null) && (!URLUtil.isValidUrl(paramString)))
      {
        getView().fireErrorEvent("expand", "URL passed to expand() was invalid.");
        return;
      }
      this.mRootView = ((FrameLayout)getView().getRootView().findViewById(16908290));
      useCustomClose(paramBoolean1);
      setOrientationLockEnabled(paramBoolean2);
      swapViewWithPlaceholderView();
      MraidView localMraidView = getView();
      if (paramString != null)
      {
        this.mTwoPartExpansionView = new MraidView(getView().getContext(), this.windowWidth_, this.windowHeight_, this.scalingFactor_, this.usingDownscalingLogic_, MraidView.ExpansionStyle.DISABLED, MraidView.NativeCloseButtonStyle.AD_CONTROLLED, MraidView.PlacementType.INLINE);
        this.mTwoPartExpansionView.setOnCloseListener(new MraidDisplayController.2(this));
        this.mTwoPartExpansionView.loadUrl(paramString);
        localMraidView = this.mTwoPartExpansionView;
      }
      ViewGroup localViewGroup = createExpansionViewContainer(localMraidView, (int)(paramInt1 * this.mDensity), (int)(paramInt2 * this.mDensity));
      this.mRootView.addView(localViewGroup, new RelativeLayout.LayoutParams(-1, -1));
      localMraidView.requestFocus();
      localMraidView.setOnKeyListener(new MraidDisplayController.3(this));
      if ((this.mNativeCloseButtonStyle == MraidView.NativeCloseButtonStyle.ALWAYS_VISIBLE) || ((!this.mAdWantsCustomCloseButton) && (this.mNativeCloseButtonStyle != MraidView.NativeCloseButtonStyle.ALWAYS_HIDDEN)))
        setNativeCloseButtonEnabled(true);
      this.mViewState = MraidView.ViewState.EXPANDED;
      getView().fireChangeEventForProperty(MraidStateProperty.createWithViewState(this.mViewState));
    }
    while (getView().getOnExpandListener() == null);
    getView().getOnExpandListener().onExpand(getView());
  }

  protected void initializeJavaScriptState()
  {
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(MraidScreenSizeProperty.createWithSize(this.mScreenWidth, this.mScreenHeight));
    localArrayList.add(MraidViewableProperty.createWithViewable(this.mIsViewable));
    getView().fireChangeEventForProperties(localArrayList);
    this.mViewState = MraidView.ViewState.DEFAULT;
    getView().fireChangeEventForProperty(MraidStateProperty.createWithViewState(this.mViewState));
  }

  protected boolean isExpanded()
  {
    return this.mViewState == MraidView.ViewState.EXPANDED;
  }

  protected void playVideo(String paramString, Controller.Dimensions paramDimensions, Controller.PlayerProperties paramPlayerProperties)
  {
    Log.d("MraidDisplayController", "in playVideo");
    if (this.vidPlaying_)
      return;
    if (paramPlayerProperties.isFullScreen())
    {
      Bundle localBundle = new Bundle();
      localBundle.putString("url", paramString);
      localBundle.putParcelable("player_dimensions", paramDimensions);
      localBundle.putParcelable("player_properties", paramPlayerProperties);
      try
      {
        Intent localIntent = new Intent(getView().getContext(), VideoActionHandler.class);
        localIntent.putExtras(localBundle);
        getView().getContext().startActivity(localIntent);
        return;
      }
      catch (ActivityNotFoundException localActivityNotFoundException)
      {
        Log.e("MraidDisplayController", "Failed to open VideoAction activity");
        return;
      }
    }
    this.vidPlayer_.setPlayData(new Controller.PlayerProperties(), paramString);
    this.vidPlayer_.setListener(new MraidDisplayController.6(this));
    FrameLayout.LayoutParams localLayoutParams = new FrameLayout.LayoutParams(paramDimensions.width, paramDimensions.height);
    localLayoutParams.topMargin = paramDimensions.x;
    localLayoutParams.bottomMargin = paramDimensions.y;
    this.vidPlayer_.setLayoutParams(localLayoutParams);
    FrameLayout localFrameLayout = new FrameLayout(getView().getContext());
    localFrameLayout.setId(105);
    localFrameLayout.setPadding(paramDimensions.x, paramDimensions.y, 0, 0);
    localFrameLayout.addView(this.vidPlayer_);
    this.mRootView.addView(localFrameLayout, -1, -1);
    this.vidPlaying_ = true;
    this.vidPlayer_.playVideo();
  }

  protected void registerRecievers()
  {
    if (!this.mRegistered)
    {
      this.mRegistered = true;
      getView().getContext().registerReceiver(this.mOrientationBroadcastReceiver, new IntentFilter("android.intent.action.CONFIGURATION_CHANGED"));
    }
  }

  protected void setNativeCloseButtonEnabled(boolean paramBoolean)
  {
    if (this.mRootView == null)
      return;
    FrameLayout localFrameLayout = (FrameLayout)this.mRootView.findViewById(102);
    if (paramBoolean)
    {
      if (this.mCloseButton == null)
      {
        StateListDrawable localStateListDrawable = new StateListDrawable();
        localStateListDrawable.addState(new int[] { -16842919 }, new BitmapDrawable(ResourceLookup.bitmapFromJar(this.mContext, "ad_resources/drawable/amazon_ads_close_button_normal.png")));
        localStateListDrawable.addState(new int[] { 16842919 }, new BitmapDrawable(ResourceLookup.bitmapFromJar(this.mContext, "ad_resources/drawable/amazon_ads_close_button_pressed.png")));
        this.mCloseButton = new ImageButton(getView().getContext());
        this.mCloseButton.setImageDrawable(localStateListDrawable);
        this.mCloseButton.setBackgroundDrawable(null);
        this.mCloseButton.setOnClickListener(new MraidDisplayController.5(this));
      }
      int i = (int)(0.5F + 50.0F * this.mDensity);
      FrameLayout.LayoutParams localLayoutParams = new FrameLayout.LayoutParams(i, i, 5);
      localFrameLayout.addView(this.mCloseButton, localLayoutParams);
    }
    while (true)
    {
      MraidView localMraidView = getView();
      if (localMraidView.getOnCloseButtonStateChangeListener() == null)
        break;
      localMraidView.getOnCloseButtonStateChangeListener().onCloseButtonStateChange(localMraidView, paramBoolean);
      return;
      localFrameLayout.removeView(this.mCloseButton);
    }
  }

  protected void surfaceAd()
  {
    getView().fireChangeEventForProperty(MraidViewableProperty.createWithViewable(true));
  }

  protected void unregisterRecievers()
  {
    if (this.mRegistered == true)
      this.mRegistered = false;
    try
    {
      getView().getContext().unregisterReceiver(this.mOrientationBroadcastReceiver);
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
    }
  }

  protected void useCustomClose(boolean paramBoolean)
  {
    this.mAdWantsCustomCloseButton = paramBoolean;
    MraidView localMraidView = getView();
    if (!paramBoolean);
    for (boolean bool = true; ; bool = false)
    {
      if (localMraidView.getOnCloseButtonStateChangeListener() != null)
        localMraidView.getOnCloseButtonStateChangeListener().onCloseButtonStateChange(localMraidView, bool);
      return;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.MraidDisplayController
 * JD-Core Version:    0.6.2
 */