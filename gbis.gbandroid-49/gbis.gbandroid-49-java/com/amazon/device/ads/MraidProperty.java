package com.amazon.device.ads;

abstract class MraidProperty
{
  private String sanitize(String paramString)
  {
    if (paramString != null)
      return paramString.replaceAll("[^a-zA-Z0-9_,:\\s\\{\\}\\'\\\"]", "");
    return "";
  }

  public abstract String toJsonPair();

  public String toString()
  {
    return sanitize(toJsonPair());
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.MraidProperty
 * JD-Core Version:    0.6.2
 */