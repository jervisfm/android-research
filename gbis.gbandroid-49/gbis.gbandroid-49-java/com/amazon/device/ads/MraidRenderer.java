package com.amazon.device.ads;

import android.widget.FrameLayout.LayoutParams;
import java.util.Map;

class MraidRenderer extends AdRenderer
  implements MraidView.OnCloseListener, MraidView.OnExpandListener, MraidView.OnReadyListener, MraidView.OnSpecialUrlClickListener
{
  private static final String LOG_TAG = "MraidRenderer";
  protected MraidView mraidView_;

  protected MraidRenderer(AdResponse paramAdResponse, AdBridge paramAdBridge)
  {
    super(paramAdResponse, paramAdBridge);
  }

  protected void adLoaded(AdProperties paramAdProperties)
  {
    super.adLoaded(paramAdProperties);
  }

  protected void destroy()
  {
    if (this.mraidView_ != null)
    {
      this.mraidView_.destroy();
      this.mraidView_ = null;
      this.isDestroyed_ = true;
    }
  }

  public void onClose(MraidView paramMraidView, MraidView.ViewState paramViewState)
  {
    if (!isAdViewRemoved())
      this.bridge_.adClosedExpansion();
  }

  public void onExpand(MraidView paramMraidView)
  {
    if (!isAdViewRemoved())
      this.bridge_.adExpanded();
  }

  public void onReady(MraidView paramMraidView)
  {
    adLoaded(this.ad_.properties_);
  }

  public void onSpecialUrlClick(MraidView paramMraidView, String paramString)
  {
    if (!isAdViewRemoved())
      this.bridge_.specialUrlClicked(paramString);
  }

  protected void prepareToGoAway()
  {
    if (this.mraidView_ != null)
      this.mraidView_.prepareToGoAway();
  }

  protected void removeView()
  {
    if (!this.bridge_.isInvalidated())
      this.bridge_.getAdLayout().removeAllViews();
    this.viewRemoved_ = true;
  }

  protected void render()
  {
    if (isAdViewDestroyed())
      return;
    this.mraidView_ = new MraidView(this.bridge_.getContext(), this.bridge_.getWindowWidth(), this.bridge_.getWindowHeight(), this.scalingFactor_, this.usingDownscalingLogic_);
    this.mraidView_.loadHtmlData(this.ad_.creative_);
    this.mraidView_.setOnReadyListener(this);
    this.mraidView_.setOnSpecialUrlClickListener(this);
    this.mraidView_.setOnExpandListener(this);
    this.mraidView_.setOnCloseListener(this);
    this.bridge_.getAdLayout().removeAllViews();
    FrameLayout.LayoutParams localLayoutParams = new FrameLayout.LayoutParams(-1, -1, 17);
    this.bridge_.getAdLayout().addView(this.mraidView_, localLayoutParams);
  }

  protected boolean sendCommand(String paramString, Map<String, String> paramMap)
  {
    Log.d("MraidRenderer", "sendCommand: " + paramString);
    if ((paramString.equals("close")) && (this.mraidView_ != null) && (this.mraidView_.getDisplayController().isExpanded()))
    {
      this.mraidView_.getDisplayController().close();
      return true;
    }
    return false;
  }

  protected boolean shouldReuse()
  {
    return false;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.MraidRenderer
 * JD-Core Version:    0.6.2
 */