package com.amazon.device.ads;

class MraidScreenSizeProperty extends MraidProperty
{
  private final int mScreenHeight;
  private final int mScreenWidth;

  MraidScreenSizeProperty(int paramInt1, int paramInt2)
  {
    this.mScreenWidth = paramInt1;
    this.mScreenHeight = paramInt2;
  }

  public static MraidScreenSizeProperty createWithSize(int paramInt1, int paramInt2)
  {
    return new MraidScreenSizeProperty(paramInt1, paramInt2);
  }

  public String toJsonPair()
  {
    return "screenSize: { width: " + this.mScreenWidth + ", height: " + this.mScreenHeight + " }";
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.MraidScreenSizeProperty
 * JD-Core Version:    0.6.2
 */