package com.amazon.device.ads;

class MraidStateProperty extends MraidProperty
{
  private final MraidView.ViewState mViewState;

  MraidStateProperty(MraidView.ViewState paramViewState)
  {
    this.mViewState = paramViewState;
  }

  public static MraidStateProperty createWithViewState(MraidView.ViewState paramViewState)
  {
    return new MraidStateProperty(paramViewState);
  }

  public String toJsonPair()
  {
    return "state: '" + this.mViewState.toString().toLowerCase() + "'";
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.MraidStateProperty
 * JD-Core Version:    0.6.2
 */