package com.amazon.device.ads;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;

class MraidView extends WebView
{
  public static final int AD_CONTAINER_LAYOUT_ID = 102;
  private static final String LOGTAG = "MraidView";
  public static final int MODAL_CONTAINER_LAYOUT_ID = 101;
  public static final int PLACEHOLDER_VIEW_ID = 100;
  private boolean mAttached = false;
  private MraidBrowserController mBrowserController;
  private MraidDisplayController mDisplayController;
  private boolean mGoingAway = false;
  private boolean mHasFiredReadyEvent;
  private int mLastVisibility = 8;
  private MraidListenerInfo mListenerInfo;
  private final PlacementType mPlacementType;
  private WebChromeClient mWebChromeClient;
  private WebViewClient mWebViewClient;
  private double scalingFactor_;
  private boolean usingDownscalingLogic_;
  private int windowHeight_;
  private int windowWidth_;

  public MraidView(Context paramContext, int paramInt1, int paramInt2, double paramDouble, boolean paramBoolean)
  {
    this(paramContext, paramInt1, paramInt2, paramDouble, paramBoolean, ExpansionStyle.ENABLED, NativeCloseButtonStyle.AD_CONTROLLED, PlacementType.INLINE);
  }

  MraidView(Context paramContext, int paramInt1, int paramInt2, double paramDouble, boolean paramBoolean, ExpansionStyle paramExpansionStyle, NativeCloseButtonStyle paramNativeCloseButtonStyle, PlacementType paramPlacementType)
  {
    super(paramContext);
    this.mPlacementType = paramPlacementType;
    this.windowHeight_ = paramInt2;
    this.windowWidth_ = paramInt1;
    this.scalingFactor_ = paramDouble;
    this.usingDownscalingLogic_ = paramBoolean;
    initialize(paramExpansionStyle, paramNativeCloseButtonStyle);
  }

  // ERROR //
  private String copyRawResourceToFilesDir(String paramString1, String paramString2)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 114	com/amazon/device/ads/MraidView:getContext	()Landroid/content/Context;
    //   4: aload_1
    //   5: invokestatic 120	com/amazon/device/ads/ResourceLookup:getResourceFile	(Landroid/content/Context;Ljava/lang/String;)Ljava/io/InputStream;
    //   8: astore_3
    //   9: aload_3
    //   10: ifnonnull +6 -> 16
    //   13: ldc 122
    //   15: areturn
    //   16: new 124	java/lang/StringBuilder
    //   19: dup
    //   20: invokespecial 127	java/lang/StringBuilder:<init>	()V
    //   23: aload_0
    //   24: invokevirtual 114	com/amazon/device/ads/MraidView:getContext	()Landroid/content/Context;
    //   27: invokevirtual 133	android/content/Context:getFilesDir	()Ljava/io/File;
    //   30: invokevirtual 139	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   33: invokevirtual 143	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   36: getstatic 146	java/io/File:separator	Ljava/lang/String;
    //   39: invokevirtual 143	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   42: aload_2
    //   43: invokevirtual 143	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   46: invokevirtual 149	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   49: astore 4
    //   51: new 135	java/io/File
    //   54: dup
    //   55: aload 4
    //   57: invokespecial 152	java/io/File:<init>	(Ljava/lang/String;)V
    //   60: astore 5
    //   62: new 154	java/io/FileOutputStream
    //   65: dup
    //   66: aload 5
    //   68: invokespecial 157	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   71: astore 6
    //   73: sipush 8192
    //   76: newarray byte
    //   78: astore 7
    //   80: aload_3
    //   81: aload 7
    //   83: invokevirtual 163	java/io/InputStream:read	([B)I
    //   86: istore 12
    //   88: iload 12
    //   90: iconst_m1
    //   91: if_icmpeq +40 -> 131
    //   94: aload 6
    //   96: aload 7
    //   98: iconst_0
    //   99: iload 12
    //   101: invokevirtual 167	java/io/FileOutputStream:write	([BII)V
    //   104: goto -24 -> 80
    //   107: astore 10
    //   109: aload_3
    //   110: invokevirtual 170	java/io/InputStream:close	()V
    //   113: aload 6
    //   115: invokevirtual 171	java/io/FileOutputStream:close	()V
    //   118: ldc 122
    //   120: areturn
    //   121: astore 11
    //   123: ldc 122
    //   125: areturn
    //   126: astore 14
    //   128: ldc 122
    //   130: areturn
    //   131: aload_3
    //   132: invokevirtual 170	java/io/InputStream:close	()V
    //   135: aload 6
    //   137: invokevirtual 171	java/io/FileOutputStream:close	()V
    //   140: aload 4
    //   142: areturn
    //   143: astore 13
    //   145: aload 4
    //   147: areturn
    //   148: astore 8
    //   150: aload_3
    //   151: invokevirtual 170	java/io/InputStream:close	()V
    //   154: aload 6
    //   156: invokevirtual 171	java/io/FileOutputStream:close	()V
    //   159: aload 8
    //   161: athrow
    //   162: astore 9
    //   164: goto -5 -> 159
    //
    // Exception table:
    //   from	to	target	type
    //   80	88	107	java/io/IOException
    //   94	104	107	java/io/IOException
    //   109	118	121	java/io/IOException
    //   62	73	126	java/io/FileNotFoundException
    //   131	140	143	java/io/IOException
    //   80	88	148	finally
    //   94	104	148	finally
    //   150	159	162	java/io/IOException
  }

  private void initialize(ExpansionStyle paramExpansionStyle, NativeCloseButtonStyle paramNativeCloseButtonStyle)
  {
    setScrollContainer(false);
    setBackgroundColor(0);
    setVerticalScrollBarEnabled(false);
    setHorizontalScrollBarEnabled(false);
    getSettings().setJavaScriptEnabled(true);
    getSettings().setPluginsEnabled(true);
    this.mBrowserController = new MraidBrowserController(this);
    this.mDisplayController = new MraidDisplayController(this, paramExpansionStyle, paramNativeCloseButtonStyle);
    this.mWebViewClient = new MraidWebViewClient(null);
    setWebViewClient(this.mWebViewClient);
    this.mWebChromeClient = new MraidWebChromeClient(null);
    setWebChromeClient(this.mWebChromeClient);
    this.mListenerInfo = new MraidListenerInfo();
  }

  private void notifyOnFailureListener()
  {
    if (this.mListenerInfo.mOnFailureListener != null)
      this.mListenerInfo.mOnFailureListener.onFailure(this);
  }

  private boolean tryCommand(URI paramURI)
  {
    String str = paramURI.getHost();
    List localList = URLEncodedUtils.parse(paramURI, "UTF-8");
    HashMap localHashMap = new HashMap();
    Iterator localIterator = localList.iterator();
    while (localIterator.hasNext())
    {
      NameValuePair localNameValuePair = (NameValuePair)localIterator.next();
      localHashMap.put(localNameValuePair.getName(), localNameValuePair.getValue());
    }
    MraidCommand localMraidCommand = MraidCommandRegistry.createCommand(str, localHashMap, this);
    if (localMraidCommand == null)
    {
      fireNativeCommandCompleteEvent(str);
      return false;
    }
    localMraidCommand.execute();
    fireNativeCommandCompleteEvent(str);
    return true;
  }

  public void destroy()
  {
    this.mDisplayController.destroy();
    super.destroy();
  }

  protected void fireChangeEventForProperties(ArrayList<MraidProperty> paramArrayList)
  {
    String str1 = paramArrayList.toString();
    if (str1.length() < 2)
      return;
    String str2 = "{" + str1.substring(1, -1 + str1.length()) + "}";
    injectJavaScript("window.mraidbridge.fireChangeEvent(" + str2 + ");");
    Log.d("MraidView", "Fire changes: " + str2);
  }

  protected void fireChangeEventForProperty(MraidProperty paramMraidProperty)
  {
    String str = "{" + paramMraidProperty.toString() + "}";
    injectJavaScript("window.mraidbridge.fireChangeEvent(" + str + ");");
    Log.d("MraidView", "Fire change: " + str);
  }

  protected void fireErrorEvent(String paramString1, String paramString2)
  {
    injectJavaScript("window.mraidbridge.fireErrorEvent('" + paramString1 + "', '" + paramString2 + "');");
  }

  protected void fireNativeCommandCompleteEvent(String paramString)
  {
    injectJavaScript("window.mraidbridge.nativeCallComplete('" + paramString + "');");
  }

  protected void fireReadyEvent()
  {
    injectJavaScript("window.mraidbridge.fireReadyEvent();");
  }

  protected MraidBrowserController getBrowserController()
  {
    return this.mBrowserController;
  }

  protected MraidDisplayController getDisplayController()
  {
    return this.mDisplayController;
  }

  public OnCloseButtonStateChangeListener getOnCloseButtonStateChangeListener()
  {
    return this.mListenerInfo.mOnCloseButtonListener;
  }

  public OnCloseListener getOnCloseListener()
  {
    return this.mListenerInfo.mOnCloseListener;
  }

  public OnExpandListener getOnExpandListener()
  {
    return this.mListenerInfo.mOnExpandListener;
  }

  public OnFailureListener getOnFailureListener()
  {
    return this.mListenerInfo.mOnFailureListener;
  }

  public OnOpenListener getOnOpenListener()
  {
    return this.mListenerInfo.mOnOpenListener;
  }

  public OnReadyListener getOnReadyListener()
  {
    return this.mListenerInfo.mOnReadyListener;
  }

  public OnSpecialUrlClickListener getOnSpecialUrlClickListener()
  {
    return this.mListenerInfo.mOnSpecialUrlClickListener;
  }

  protected double getScalingFactor()
  {
    return this.scalingFactor_;
  }

  protected int getWindowHeight()
  {
    return this.windowHeight_;
  }

  protected int getWindowWidth()
  {
    return this.windowWidth_;
  }

  protected void injectJavaScript(String paramString)
  {
    if (paramString != null)
      super.loadUrl("javascript:" + paramString);
  }

  protected boolean isUsingDownscalingLogic()
  {
    return this.usingDownscalingLogic_;
  }

  public void loadHtmlData(String paramString)
  {
    if (paramString.indexOf("<html>") == -1)
    {
      if (!this.usingDownscalingLogic_)
        break label124;
      paramString = "<html><head></head><body style='margin:0;padding:0;'>" + paramString + "</body></html>";
      setInitialScale((int)(100.0D * this.scalingFactor_));
    }
    while (true)
    {
      String str = "file://" + copyRawResourceToFilesDir("ad_resources/raw/amazon_ads_mraid.js", "mraid.js");
      loadDataWithBaseURL(null, paramString.replace("<head>", "<head><script src='" + str + "'></script>"), "text/html", "UTF-8", null);
      return;
      label124: setInitialScale(0);
      paramString = "<html><meta name=\"viewport\" content=\"width=" + this.windowWidth_ + ", height=" + this.windowHeight_ + ", initial-scale=" + this.scalingFactor_ + "\"/><head></head><body style='margin:0;padding:0;'>" + paramString + "</body></html>";
    }
  }

  public void loadUrl(String paramString)
  {
    DefaultHttpClient localDefaultHttpClient = new DefaultHttpClient();
    HttpGet localHttpGet = new HttpGet(paramString);
    StringBuffer localStringBuffer = new StringBuffer();
    try
    {
      HttpEntity localHttpEntity = localDefaultHttpClient.execute(localHttpGet).getEntity();
      if (localHttpEntity != null)
      {
        InputStream localInputStream = localHttpEntity.getContent();
        byte[] arrayOfByte = new byte[4096];
        while (true)
        {
          int i = localInputStream.read(arrayOfByte);
          if (i == -1)
            break;
          localStringBuffer.append(new String(arrayOfByte, 0, i));
        }
      }
    }
    catch (ClientProtocolException localClientProtocolException)
    {
      notifyOnFailureListener();
      return;
    }
    catch (IOException localIOException)
    {
      notifyOnFailureListener();
      return;
    }
    loadHtmlData(localStringBuffer.toString());
  }

  protected void onAttachedToWindow()
  {
    if (this.mGoingAway);
    do
    {
      return;
      super.onAttachedToWindow();
      this.mAttached = true;
    }
    while (this.mDisplayController == null);
    this.mDisplayController.registerRecievers();
  }

  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    this.mAttached = false;
    if (this.mDisplayController != null)
      this.mDisplayController.unregisterRecievers();
  }

  protected void onWindowVisibilityChanged(int paramInt)
  {
    if ((this.mAttached) && (this.mLastVisibility != paramInt) && (paramInt != 0) && (this.mDisplayController != null))
      this.mDisplayController.unregisterRecievers();
  }

  public void prepareToGoAway()
  {
    this.mGoingAway = true;
  }

  public void setOnCloseButtonStateChange(OnCloseButtonStateChangeListener paramOnCloseButtonStateChangeListener)
  {
    MraidListenerInfo.access$602(this.mListenerInfo, paramOnCloseButtonStateChangeListener);
  }

  public void setOnCloseListener(OnCloseListener paramOnCloseListener)
  {
    MraidListenerInfo.access$402(this.mListenerInfo, paramOnCloseListener);
  }

  public void setOnExpandListener(OnExpandListener paramOnExpandListener)
  {
    MraidListenerInfo.access$302(this.mListenerInfo, paramOnExpandListener);
  }

  public void setOnFailureListener(OnFailureListener paramOnFailureListener)
  {
    MraidListenerInfo.access$202(this.mListenerInfo, paramOnFailureListener);
  }

  public void setOnOpenListener(OnOpenListener paramOnOpenListener)
  {
    MraidListenerInfo.access$702(this.mListenerInfo, paramOnOpenListener);
  }

  public void setOnReadyListener(OnReadyListener paramOnReadyListener)
  {
    MraidListenerInfo.access$502(this.mListenerInfo, paramOnReadyListener);
  }

  public void setOnSpecialUrlClickListener(OnSpecialUrlClickListener paramOnSpecialUrlClickListener)
  {
    MraidListenerInfo.access$802(this.mListenerInfo, paramOnSpecialUrlClickListener);
  }

  static enum ExpansionStyle
  {
    static
    {
      DISABLED = new ExpansionStyle("DISABLED", 1);
      ExpansionStyle[] arrayOfExpansionStyle = new ExpansionStyle[2];
      arrayOfExpansionStyle[0] = ENABLED;
      arrayOfExpansionStyle[1] = DISABLED;
    }
  }

  static class MraidListenerInfo
  {
    private MraidView.OnCloseButtonStateChangeListener mOnCloseButtonListener;
    private MraidView.OnCloseListener mOnCloseListener;
    private MraidView.OnExpandListener mOnExpandListener;
    private MraidView.OnFailureListener mOnFailureListener;
    private MraidView.OnOpenListener mOnOpenListener;
    private MraidView.OnReadyListener mOnReadyListener;
    private MraidView.OnSpecialUrlClickListener mOnSpecialUrlClickListener;
  }

  private class MraidWebChromeClient extends WebChromeClient
  {
    private MraidWebChromeClient()
    {
    }

    public boolean onJsAlert(WebView paramWebView, String paramString1, String paramString2, JsResult paramJsResult)
    {
      Log.d("MraidView", paramString2);
      return false;
    }
  }

  private class MraidWebViewClient extends WebViewClient
  {
    private MraidWebViewClient()
    {
    }

    public void onLoadResource(WebView paramWebView, String paramString)
    {
      Log.d("MraidView", "Loaded resource: " + paramString);
    }

    public void onPageFinished(WebView paramWebView, String paramString)
    {
      if (!MraidView.this.mHasFiredReadyEvent)
      {
        MraidView.this.mDisplayController.initializeJavaScriptState();
        MraidView.this.fireChangeEventForProperty(MraidPlacementTypeProperty.createWithType(MraidView.this.mPlacementType));
        MraidView.this.fireReadyEvent();
        if (MraidView.this.getOnReadyListener() != null)
          MraidView.this.getOnReadyListener().onReady(MraidView.this);
        MraidView.access$1102(MraidView.this, true);
        MraidView.this.mDisplayController.surfaceAd();
      }
    }

    public void onReceivedError(WebView paramWebView, int paramInt, String paramString1, String paramString2)
    {
      Log.d("MraidView", "Error: " + paramString1);
      super.onReceivedError(paramWebView, paramInt, paramString1, paramString2);
    }

    public boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
    {
      String str = Uri.parse(paramString).getScheme();
      if (str.equals("mopub"))
        return true;
      if (str.equals("mraid"))
      {
        MraidView.this.tryCommand(URI.create(paramString));
        return true;
      }
      if (str.equals("amazonmobile"))
      {
        MraidView.this.mBrowserController.executeAmazonMobileCallback(MraidView.this, paramString);
        return true;
      }
      Intent localIntent = new Intent();
      localIntent.setAction("android.intent.action.VIEW");
      localIntent.setData(Uri.parse(paramString));
      localIntent.addFlags(268435456);
      try
      {
        MraidView.this.getContext().startActivity(localIntent);
        return true;
      }
      catch (ActivityNotFoundException localActivityNotFoundException)
      {
      }
      return false;
    }
  }

  static enum NativeCloseButtonStyle
  {
    static
    {
      ALWAYS_HIDDEN = new NativeCloseButtonStyle("ALWAYS_HIDDEN", 1);
      AD_CONTROLLED = new NativeCloseButtonStyle("AD_CONTROLLED", 2);
      NativeCloseButtonStyle[] arrayOfNativeCloseButtonStyle = new NativeCloseButtonStyle[3];
      arrayOfNativeCloseButtonStyle[0] = ALWAYS_VISIBLE;
      arrayOfNativeCloseButtonStyle[1] = ALWAYS_HIDDEN;
      arrayOfNativeCloseButtonStyle[2] = AD_CONTROLLED;
    }
  }

  public static abstract interface OnCloseButtonStateChangeListener
  {
    public abstract void onCloseButtonStateChange(MraidView paramMraidView, boolean paramBoolean);
  }

  public static abstract interface OnCloseListener
  {
    public abstract void onClose(MraidView paramMraidView, MraidView.ViewState paramViewState);
  }

  public static abstract interface OnExpandListener
  {
    public abstract void onExpand(MraidView paramMraidView);
  }

  public static abstract interface OnFailureListener
  {
    public abstract void onFailure(MraidView paramMraidView);
  }

  public static abstract interface OnOpenListener
  {
    public abstract void onOpen(MraidView paramMraidView);
  }

  public static abstract interface OnReadyListener
  {
    public abstract void onReady(MraidView paramMraidView);
  }

  public static abstract interface OnSpecialUrlClickListener
  {
    public abstract void onSpecialUrlClick(MraidView paramMraidView, String paramString);
  }

  static enum PlacementType
  {
    static
    {
      PlacementType[] arrayOfPlacementType = new PlacementType[2];
      arrayOfPlacementType[0] = INLINE;
      arrayOfPlacementType[1] = INTERSTITIAL;
    }
  }

  public static enum ViewState
  {
    static
    {
      DEFAULT = new ViewState("DEFAULT", 1);
      EXPANDED = new ViewState("EXPANDED", 2);
      HIDDEN = new ViewState("HIDDEN", 3);
      ViewState[] arrayOfViewState = new ViewState[4];
      arrayOfViewState[0] = LOADING;
      arrayOfViewState[1] = DEFAULT;
      arrayOfViewState[2] = EXPANDED;
      arrayOfViewState[3] = HIDDEN;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.MraidView
 * JD-Core Version:    0.6.2
 */