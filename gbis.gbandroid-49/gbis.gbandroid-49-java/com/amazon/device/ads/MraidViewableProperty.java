package com.amazon.device.ads;

class MraidViewableProperty extends MraidProperty
{
  private final boolean mViewable;

  MraidViewableProperty(boolean paramBoolean)
  {
    this.mViewable = paramBoolean;
  }

  public static MraidViewableProperty createWithViewable(boolean paramBoolean)
  {
    return new MraidViewableProperty(paramBoolean);
  }

  public String toJsonPair()
  {
    StringBuilder localStringBuilder = new StringBuilder("viewable: ");
    if (this.mViewable);
    for (String str = "true"; ; str = "false")
      return str;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.MraidViewableProperty
 * JD-Core Version:    0.6.2
 */