package com.amazon.device.ads;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.jar.JarFile;

class ResourceLookup
{
  private static final int CACHE_SIZE = 101;
  private Class<?> arrr_ = null;
  private Map<String, Integer> cache_ = null;
  private String id_ = null;

  public ResourceLookup(String paramString, Context paramContext)
  {
    this.id_ = paramString.replace('.', '_').replace('-', '_');
    try
    {
      this.arrr_ = Class.forName(paramContext.getPackageName() + ".R");
      this.cache_ = Collections.synchronizedMap(new Cache());
      return;
    }
    catch (Throwable localThrowable)
    {
      throw new RuntimeException("Exception finding R class", localThrowable);
    }
  }

  public static Bitmap bitmapFromJar(Context paramContext, String paramString)
  {
    return BitmapFactory.decodeStream(getResourceFile(paramContext, paramString));
  }

  private Class<?> getResourceClass(String paramString)
  {
    for (Class localClass : this.arrr_.getClasses())
      if (paramString.equals(localClass.getSimpleName()))
        return localClass;
    return null;
  }

  public static InputStream getResourceFile(Context paramContext, String paramString)
  {
    URL localURL = paramContext.getClassLoader().getResource(paramString);
    if (localURL == null)
      try
      {
        InputStream localInputStream2 = paramContext.getAssets().open(paramString);
        return localInputStream2;
      }
      catch (IOException localIOException2)
      {
        return null;
      }
    String str = localURL.getFile();
    if (str.startsWith("file:"))
      str = str.substring(6);
    int i = str.indexOf("!");
    if (i > 0)
      str = str.substring(0, i);
    try
    {
      JarFile localJarFile = new JarFile(str);
      InputStream localInputStream1 = localJarFile.getInputStream(localJarFile.getJarEntry(paramString));
      return localInputStream1;
    }
    catch (IOException localIOException1)
    {
    }
    return null;
  }

  public int getDrawableId(String paramString)
  {
    return getIdentifier(paramString, "drawable", true);
  }

  public int getIdentifier(String paramString1, String paramString2, boolean paramBoolean)
  {
    StringBuilder localStringBuilder1 = new StringBuilder(paramString1);
    localStringBuilder1.append('|');
    localStringBuilder1.append(paramString2);
    Integer localInteger = (Integer)this.cache_.get(localStringBuilder1.toString());
    if (localInteger != null)
      return localInteger.intValue();
    if ((!paramString1.startsWith(this.id_)) && (paramBoolean))
    {
      StringBuilder localStringBuilder2 = new StringBuilder(this.id_);
      localStringBuilder2.append('_');
      localStringBuilder2.append(paramString1);
      paramString1 = localStringBuilder2.toString();
    }
    try
    {
      Class localClass = getResourceClass(paramString2);
      if (localClass != null)
      {
        Field localField = localClass.getDeclaredField(paramString1);
        if (localField != null)
        {
          int i = localField.getInt(localClass);
          this.cache_.put(localStringBuilder1.toString(), Integer.valueOf(i));
          return i;
        }
      }
    }
    catch (Throwable localThrowable)
    {
      throw new RuntimeException("Exception finding resource identifier", localThrowable);
    }
    return -1;
  }

  public int getItemId(String paramString)
  {
    return getIdentifier(paramString, "id", false);
  }

  public int getLayoutId(String paramString)
  {
    return getIdentifier(paramString, "layout", true);
  }

  public int getRawId(String paramString)
  {
    return getIdentifier(paramString, "raw", false);
  }

  public int[] getStyleableArray(String paramString)
  {
    int i = 0;
    try
    {
      Class localClass = getResourceClass("styleable");
      if (localClass != null)
      {
        Field localField = localClass.getDeclaredField(paramString);
        if (localField != null)
        {
          Object localObject = localField.get(localClass);
          if ((localObject instanceof int[]))
          {
            arrayOfInt = new int[Array.getLength(localObject)];
            while (i < Array.getLength(localObject))
            {
              arrayOfInt[i] = Array.getInt(localObject, i);
              i++;
            }
          }
        }
      }
    }
    catch (Throwable localThrowable)
    {
      throw new RuntimeException("Exception finding styleable", localThrowable);
    }
    int[] arrayOfInt = new int[0];
    return arrayOfInt;
  }

  public int getStyleableId(String paramString1, String paramString2)
  {
    return getIdentifier(paramString1 + "_" + paramString2, "styleable", false);
  }

  public class Cache extends LinkedHashMap<String, Integer>
  {
    private static final long serialVersionUID = -5258829086548460019L;

    public Cache()
    {
      super(1.1F, true);
    }

    protected boolean removedEldestEntry(Map.Entry<String, Integer> paramEntry)
    {
      return size() > 101;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.ResourceLookup
 * JD-Core Version:    0.6.2
 */