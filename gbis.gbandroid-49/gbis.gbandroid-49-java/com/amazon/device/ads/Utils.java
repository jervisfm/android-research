package com.amazon.device.ads;

import android.app.Activity;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.view.Display;
import android.view.WindowManager;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

class Utils
{
  public static final String LOG_TAG = "AmazonAdRegistration";
  private static int[][] rotationArray = { { 1, 0, 9, 8 }, { 0, 9, 8, 1 } };

  public static int determineCanonicalScreenOrientation(Activity paramActivity)
  {
    int i = paramActivity.getWindowManager().getDefaultDisplay().getOrientation();
    int j = paramActivity.getResources().getConfiguration().orientation;
    int k;
    int m;
    if (j == 1)
      if ((i == 0) || (i == 2))
      {
        k = 1;
        m = 0;
        if (k == 0)
          break label91;
      }
    while (true)
    {
      return rotationArray[m][i];
      k = 0;
      break;
      if (j == 2)
      {
        if ((i == 1) || (i == 3))
        {
          k = 1;
          break;
        }
        k = 0;
        break;
      }
      k = 1;
      break;
      label91: m = 1;
    }
  }

  public static StringBuffer extractHttpResponse(InputStream paramInputStream)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    byte[] arrayOfByte = new byte[4096];
    while (true)
    {
      int i = paramInputStream.read(arrayOfByte);
      if (i == -1)
        break;
      localStringBuffer.append(new String(arrayOfByte, 0, i));
    }
    return localStringBuffer;
  }

  public static final String getURLDecodedString(String paramString)
  {
    if (paramString == null)
      return null;
    try
    {
      String str = URLDecoder.decode(paramString, "UTF-8");
      return str;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      Log.d("AmazonAdRegistration", "getURLDencodedString threw: " + localUnsupportedEncodingException);
    }
    return paramString;
  }

  public static final String getURLEncodedString(String paramString)
  {
    if (paramString == null)
      return null;
    try
    {
      String str = URLEncoder.encode(paramString, "UTF-8").replace("+", "%20").replace("*", "%2A").replace("%7E", "~");
      return str;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      Log.d("AmazonAdRegistration", "getURLEncodedString threw: " + localUnsupportedEncodingException);
    }
    return paramString;
  }

  public static boolean isLandscape(Activity paramActivity)
  {
    int i = determineCanonicalScreenOrientation(paramActivity);
    return (i == 0) || (i == 8);
  }

  public static boolean isPortrait(Activity paramActivity)
  {
    int i = determineCanonicalScreenOrientation(paramActivity);
    return (i == 1) || (i == 9);
  }

  public static String sha1(String paramString)
  {
    try
    {
      MessageDigest localMessageDigest = MessageDigest.getInstance("SHA-1");
      localMessageDigest.update(paramString.getBytes());
      byte[] arrayOfByte = localMessageDigest.digest();
      StringBuffer localStringBuffer = new StringBuffer();
      for (int i = 0; i < arrayOfByte.length; i++)
        localStringBuffer.append(Integer.toHexString(0x100 | 0xFF & arrayOfByte[i]).substring(1));
      String str = localStringBuffer.toString();
      return str;
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
    }
    return "";
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.Utils
 * JD-Core Version:    0.6.2
 */