package com.amazon.device.ads;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

public class VideoActionHandler extends Activity
{
  private RelativeLayout layout_;
  private AdVideoPlayer player_;

  private void initPlayer(Bundle paramBundle)
  {
    Controller.PlayerProperties localPlayerProperties = (Controller.PlayerProperties)paramBundle.getParcelable("player_properties");
    Controller.Dimensions localDimensions = (Controller.Dimensions)paramBundle.getParcelable("player_dimensions");
    this.player_ = new AdVideoPlayer(this);
    this.player_.setPlayData(localPlayerProperties, paramBundle.getString("url"));
    RelativeLayout.LayoutParams localLayoutParams;
    if (localDimensions == null)
    {
      localLayoutParams = new RelativeLayout.LayoutParams(-1, -1);
      localLayoutParams.addRule(13);
    }
    while (true)
    {
      this.player_.setLayoutParams(localLayoutParams);
      this.layout_.addView(this.player_);
      setPlayerListener(this.player_);
      return;
      localLayoutParams = new RelativeLayout.LayoutParams(localDimensions.width, localDimensions.height);
      localLayoutParams.topMargin = localDimensions.y;
      localLayoutParams.leftMargin = localDimensions.x;
    }
  }

  private void setPlayerListener(AdVideoPlayer paramAdVideoPlayer)
  {
    paramAdVideoPlayer.setListener(new VideoActionHandler.1(this));
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Bundle localBundle = getIntent().getExtras();
    this.layout_ = new RelativeLayout(this);
    this.layout_.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
    setContentView(this.layout_);
    initPlayer(localBundle);
    this.player_.playVideo();
  }

  protected void onStop()
  {
    this.player_.releasePlayer();
    this.player_ = null;
    super.onStop();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.amazon.device.ads.VideoActionHandler
 * JD-Core Version:    0.6.2
 */