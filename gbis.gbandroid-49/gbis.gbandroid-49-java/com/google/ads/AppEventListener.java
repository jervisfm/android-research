package com.google.ads;

public abstract interface AppEventListener
{
  public abstract void onAppEvent(Ad paramAd, String paramString1, String paramString2);
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.ads.AppEventListener
 * JD-Core Version:    0.6.2
 */