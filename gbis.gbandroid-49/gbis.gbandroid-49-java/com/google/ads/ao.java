package com.google.ads;

import android.view.View;
import com.google.ads.internal.d;
import com.google.ads.util.b;

final class ao
  implements Runnable
{
  ao(e parame, h paramh, View paramView, f paramf)
  {
  }

  public final void run()
  {
    if (e.a(this.d, this.a))
    {
      b.a("Trying to switch GWAdNetworkAmbassadors, but GWController().destroy() has been called. Destroying the new ambassador and terminating mediation.");
      return;
    }
    e.c(this.d).a(this.b, this.a, this.c, false);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.ads.ao
 * JD-Core Version:    0.6.2
 */