package com.google.ads;

import com.google.ads.mediation.MediationAdapter;
import com.google.ads.util.a;
import com.google.ads.util.b;

final class ap
  implements Runnable
{
  ap(h paramh)
  {
  }

  public final void run()
  {
    if (this.a.l())
      a.b(h.a(this.a));
    try
    {
      h.a(this.a).destroy();
      b.a("Called destroy() for adapter with class: " + h.a(this.a).getClass().getName());
      return;
    }
    catch (Throwable localThrowable)
    {
      b.b("Error while destroying adapter (" + this.a.h() + "):", localThrowable);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.ads.ap
 * JD-Core Version:    0.6.2
 */