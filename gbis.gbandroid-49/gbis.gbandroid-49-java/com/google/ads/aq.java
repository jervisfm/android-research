package com.google.ads;

import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.util.b;

final class aq
  implements Runnable
{
  aq(h paramh, MediationInterstitialAdapter paramMediationInterstitialAdapter)
  {
  }

  public final void run()
  {
    try
    {
      this.a.showInterstitial();
      return;
    }
    catch (Throwable localThrowable)
    {
      b.b("Error while telling adapter (" + this.b.h() + ") ad to show interstitial: ", localThrowable);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.ads.aq
 * JD-Core Version:    0.6.2
 */