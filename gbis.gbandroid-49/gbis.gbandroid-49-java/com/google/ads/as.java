package com.google.ads;

import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationBannerListener;
import com.google.ads.util.a;
import com.google.ads.util.b;

final class as
  implements MediationBannerListener
{
  private final h a;
  private boolean b;

  public as(h paramh)
  {
    this.a = paramh;
  }

  public final void onClick(MediationBannerAdapter<?, ?> paramMediationBannerAdapter)
  {
    synchronized (this.a)
    {
      a.a(this.a.c());
      this.a.j().a(this.a, this.b);
      return;
    }
  }

  public final void onDismissScreen(MediationBannerAdapter<?, ?> paramMediationBannerAdapter)
  {
    synchronized (this.a)
    {
      this.a.j().b(this.a);
      return;
    }
  }

  public final void onFailedToReceiveAd(MediationBannerAdapter<?, ?> paramMediationBannerAdapter, AdRequest.ErrorCode paramErrorCode)
  {
    synchronized (this.a)
    {
      a.a(paramMediationBannerAdapter, this.a.i());
      b.a("Mediation adapter " + paramMediationBannerAdapter.getClass().getName() + " failed to receive ad with error code: " + paramErrorCode);
      if (!this.a.c())
      {
        h localh2 = this.a;
        if (paramErrorCode == AdRequest.ErrorCode.NO_FILL)
        {
          locala = g.a.b;
          localh2.a(false, locala);
        }
      }
      else
      {
        return;
      }
      g.a locala = g.a.c;
    }
  }

  public final void onLeaveApplication(MediationBannerAdapter<?, ?> paramMediationBannerAdapter)
  {
    synchronized (this.a)
    {
      this.a.j().c(this.a);
      return;
    }
  }

  public final void onPresentScreen(MediationBannerAdapter<?, ?> paramMediationBannerAdapter)
  {
    synchronized (this.a)
    {
      this.a.j().a(this.a);
      return;
    }
  }

  public final void onReceivedAd(MediationBannerAdapter<?, ?> paramMediationBannerAdapter)
  {
    synchronized (this.a)
    {
      a.a(paramMediationBannerAdapter, this.a.i());
      try
      {
        this.a.a(paramMediationBannerAdapter.getBannerView());
        if (!this.a.c())
        {
          this.b = false;
          this.a.a(true, g.a.a);
          return;
        }
      }
      catch (Throwable localThrowable)
      {
        b.b("Error while getting banner View from adapter (" + this.a.h() + "): ", localThrowable);
        if (!this.a.c())
          this.a.a(false, g.a.f);
        return;
      }
    }
    this.b = true;
    this.a.j().a(this.a, this.a.f());
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.ads.as
 * JD-Core Version:    0.6.2
 */