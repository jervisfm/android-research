package com.google.ads;

import java.util.HashMap;

final class i extends HashMap<String, AdSize>
{
  i()
  {
    put("banner", AdSize.BANNER);
    put("mrec", AdSize.IAB_MRECT);
    put("fullbanner", AdSize.IAB_BANNER);
    put("leaderboard", AdSize.IAB_LEADERBOARD);
    put("skyscraper", AdSize.IAB_WIDE_SKYSCRAPER);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.ads.i
 * JD-Core Version:    0.6.2
 */