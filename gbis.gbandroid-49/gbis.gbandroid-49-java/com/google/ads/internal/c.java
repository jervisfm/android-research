package com.google.ads.internal;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.webkit.WebView;
import com.google.ads.AdRequest;
import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.google.ads.l;
import com.google.ads.l.a;
import com.google.ads.m;
import com.google.ads.searchads.SearchAdRequest;
import com.google.ads.util.AdUtil;
import com.google.ads.util.AdUtil.a;
import com.google.ads.util.i.b;
import com.google.ads.util.i.c;
import com.google.ads.util.i.d;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONException;

public class c
  implements Runnable
{
  boolean a;
  private String b;
  private String c;
  private String d;
  private String e;
  private boolean f;
  private f g;
  private d h;
  private AdRequest i;
  private WebView j;
  private String k;
  private LinkedList<String> l;
  private String m;
  private AdSize n;
  private volatile boolean o;
  private boolean p;
  private AdRequest.ErrorCode q;
  private boolean r;
  private int s;
  private Thread t;
  private boolean u;
  private d v = d.b;

  protected c()
  {
  }

  public c(d paramd)
  {
    this.h = paramd;
    this.k = null;
    this.b = null;
    this.c = null;
    this.d = null;
    this.l = new LinkedList();
    this.q = null;
    this.r = false;
    this.s = -1;
    this.f = false;
    this.p = false;
    this.m = null;
    this.n = null;
    if ((Activity)paramd.h().e.a() != null)
    {
      this.j = new AdWebView(paramd.h(), null);
      this.j.setWebViewClient(i.a(paramd, a.b, false, false));
      this.j.setVisibility(8);
      this.j.setWillNotDraw(true);
      this.g = new f(this, paramd);
      return;
    }
    this.j = null;
    this.g = null;
    com.google.ads.util.b.e("activity was null while trying to create an AdLoader.");
  }

  private static void a(String paramString, com.google.ads.c paramc, com.google.ads.d paramd)
  {
    if (paramString == null);
    while ((paramString.contains("no-store")) || (paramString.contains("no-cache")))
      return;
    Matcher localMatcher = Pattern.compile("max-age\\s*=\\s*(\\d+)").matcher(paramString);
    if (localMatcher.find())
      try
      {
        int i1 = Integer.parseInt(localMatcher.group(1));
        paramd.a(paramc, i1);
        Locale localLocale = Locale.US;
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = Integer.valueOf(i1);
        com.google.ads.util.b.c(String.format(localLocale, "Caching gWhirl configuration for: %d seconds", arrayOfObject));
        return;
      }
      catch (NumberFormatException localNumberFormatException)
      {
        com.google.ads.util.b.b("Caught exception trying to parse cache control directive. Overflow?", localNumberFormatException);
        return;
      }
    com.google.ads.util.b.c("Unrecognized cacheControlDirective: '" + paramString + "'. Not caching configuration.");
  }

  private void b(String paramString1, String paramString2)
  {
    this.h.a(new c(this.j, paramString2, paramString1));
  }

  private String d()
  {
    if ((this.i instanceof SearchAdRequest))
      return "AFMA_buildAdURL";
    return "AFMA_buildAdURL";
  }

  private String e()
  {
    if ((this.i instanceof SearchAdRequest))
      return "AFMA_getSdkConstants();";
    return "AFMA_getSdkConstants();";
  }

  private String f()
  {
    if ((this.i instanceof SearchAdRequest))
      return "http://www.gstatic.com/safa/";
    return "http://media.admob.com/";
  }

  private String g()
  {
    if ((this.i instanceof SearchAdRequest))
      return "<html><head><script src=\"http://www.gstatic.com/safa/sdk-core-v40.js\"></script><script>";
    return "<html><head><script src=\"http://media.admob.com/sdk-core-v40.js\"></script><script>";
  }

  private String h()
  {
    if ((this.i instanceof SearchAdRequest))
      return "</script></head><body></body></html>";
    return "</script></head><body></body></html>";
  }

  private void i()
  {
    AdWebView localAdWebView = this.h.k();
    this.h.l().c(true);
    this.h.m().h();
    this.h.a(new c(localAdWebView, this.b, this.c));
  }

  private void j()
  {
    this.h.a(new e(this.h, this.j, this.l, this.s, this.p, this.m, this.n));
  }

  public String a(Map<String, Object> paramMap, Activity paramActivity)
  {
    int i1 = 0;
    Context localContext = paramActivity.getApplicationContext();
    g localg = this.h.m();
    long l1 = localg.m();
    if (l1 > 0L)
      paramMap.put("prl", Long.valueOf(l1));
    long l2 = localg.n();
    if (l2 > 0L)
      paramMap.put("prnl", Long.valueOf(l2));
    String str1 = localg.l();
    if (str1 != null)
      paramMap.put("ppcl", str1);
    String str2 = localg.k();
    if (str2 != null)
      paramMap.put("pcl", str2);
    long l3 = localg.j();
    if (l3 > 0L)
      paramMap.put("pcc", Long.valueOf(l3));
    paramMap.put("preqs", Long.valueOf(localg.o()));
    paramMap.put("oar", Long.valueOf(localg.p()));
    paramMap.put("bas_on", Long.valueOf(localg.s()));
    paramMap.put("bas_off", Long.valueOf(localg.v()));
    if (localg.y())
      paramMap.put("aoi_timeout", "true");
    if (localg.A())
      paramMap.put("aoi_nofill", "true");
    String str3 = localg.D();
    if (str3 != null)
      paramMap.put("pit", str3);
    paramMap.put("ptime", Long.valueOf(g.E()));
    localg.a();
    localg.i();
    String str4;
    if (this.h.h().b())
    {
      paramMap.put("format", "interstitial_mb");
      paramMap.put("slotname", this.h.h().d.a());
      paramMap.put("js", "afma-sdk-a-v6.2.1");
      str4 = localContext.getPackageName();
    }
    while (true)
    {
      StringBuilder localStringBuilder;
      try
      {
        PackageInfo localPackageInfo = localContext.getPackageManager().getPackageInfo(str4, 0);
        int i2 = localPackageInfo.versionCode;
        String str5 = AdUtil.f(localContext);
        if (!TextUtils.isEmpty(str5))
          paramMap.put("mv", str5);
        paramMap.put("msid", localContext.getPackageName());
        paramMap.put("app_name", i2 + ".android." + localContext.getPackageName());
        paramMap.put("isu", AdUtil.a(localContext));
        String str6 = AdUtil.d(localContext);
        if (str6 == null)
          str6 = "null";
        paramMap.put("net", str6);
        String str7 = AdUtil.e(localContext);
        if ((str7 != null) && (str7.length() != 0))
          paramMap.put("cap", str7);
        paramMap.put("u_audio", Integer.valueOf(AdUtil.g(localContext).ordinal()));
        DisplayMetrics localDisplayMetrics1 = AdUtil.a(paramActivity);
        paramMap.put("u_sd", Float.valueOf(localDisplayMetrics1.density));
        paramMap.put("u_h", Integer.valueOf(AdUtil.a(localContext, localDisplayMetrics1)));
        paramMap.put("u_w", Integer.valueOf(AdUtil.b(localContext, localDisplayMetrics1)));
        paramMap.put("hl", Locale.getDefault().getLanguage());
        if ((this.h.h().i != null) && (this.h.h().i.a() != null))
        {
          AdView localAdView = (AdView)this.h.h().i.a();
          if (localAdView.getParent() != null)
          {
            int[] arrayOfInt = new int[2];
            localAdView.getLocationOnScreen(arrayOfInt);
            int i4 = arrayOfInt[0];
            int i5 = arrayOfInt[1];
            DisplayMetrics localDisplayMetrics2 = ((Context)this.h.h().f.a()).getResources().getDisplayMetrics();
            int i6 = localDisplayMetrics2.widthPixels;
            int i7 = localDisplayMetrics2.heightPixels;
            if ((!localAdView.isShown()) || (i4 + localAdView.getWidth() <= 0) || (i5 + localAdView.getHeight() <= 0) || (i4 > i6) || (i5 > i7))
              break label1508;
            i8 = 1;
            HashMap localHashMap2 = new HashMap();
            localHashMap2.put("x", Integer.valueOf(i4));
            localHashMap2.put("y", Integer.valueOf(i5));
            localHashMap2.put("width", Integer.valueOf(localAdView.getWidth()));
            localHashMap2.put("height", Integer.valueOf(localAdView.getHeight()));
            localHashMap2.put("visible", Integer.valueOf(i8));
            paramMap.put("ad_pos", localHashMap2);
          }
        }
        localStringBuilder = new StringBuilder();
        AdSize[] arrayOfAdSize = (AdSize[])this.h.h().l.a();
        if (arrayOfAdSize == null)
          break label1224;
        int i3 = arrayOfAdSize.length;
        if (i1 < i3)
        {
          AdSize localAdSize2 = arrayOfAdSize[i1];
          if (localStringBuilder.length() != 0)
            localStringBuilder.append("|");
          localStringBuilder.append(localAdSize2.getWidth() + "x" + localAdSize2.getHeight());
          i1++;
          continue;
          AdSize localAdSize1 = ((h)this.h.h().k.a()).b();
          if (localAdSize1.isFullWidth())
            paramMap.put("smart_w", "full");
          if (localAdSize1.isAutoHeight())
            paramMap.put("smart_h", "auto");
          if (!localAdSize1.isCustomAdSize())
          {
            paramMap.put("format", localAdSize1.toString());
            break;
          }
          HashMap localHashMap1 = new HashMap();
          localHashMap1.put("w", Integer.valueOf(localAdSize1.getWidth()));
          localHashMap1.put("h", Integer.valueOf(localAdSize1.getHeight()));
          paramMap.put("ad_frame", localHashMap1);
        }
      }
      catch (PackageManager.NameNotFoundException localNameNotFoundException)
      {
        throw new b("NameNotFoundException");
      }
      paramMap.put("sz", localStringBuilder.toString());
      label1224: TelephonyManager localTelephonyManager = (TelephonyManager)localContext.getSystemService("phone");
      paramMap.put("carrier", localTelephonyManager.getNetworkOperator());
      paramMap.put("gnt", Integer.valueOf(localTelephonyManager.getNetworkType()));
      if (AdUtil.c())
        paramMap.put("simulator", Integer.valueOf(1));
      paramMap.put("session_id", com.google.ads.b.a().b().toString());
      paramMap.put("seq_num", com.google.ads.b.a().c().toString());
      String str8 = AdUtil.a(paramMap);
      if (((Boolean)((l.a)((l)this.h.h().a.a()).a.a()).l.a()).booleanValue());
      for (String str9 = g() + d() + "(" + str8 + ");" + h(); ; str9 = g() + e() + d() + "(" + str8 + ");" + h())
      {
        com.google.ads.util.b.c("adRequestUrlHtml: " + str9);
        return str9;
      }
      label1508: int i8 = 0;
    }
  }

  protected void a()
  {
    com.google.ads.util.b.a("AdLoader cancelled.");
    if (this.j != null)
    {
      this.j.stopLoading();
      this.j.destroy();
    }
    if (this.t != null)
    {
      this.t.interrupt();
      this.t = null;
    }
    if (this.g != null)
      this.g.a();
    this.o = true;
  }

  public void a(int paramInt)
  {
    try
    {
      this.s = paramInt;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void a(AdRequest.ErrorCode paramErrorCode)
  {
    try
    {
      this.q = paramErrorCode;
      notify();
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  protected void a(AdRequest.ErrorCode paramErrorCode, boolean paramBoolean)
  {
    this.h.a(new a(this.h, this.j, this.g, paramErrorCode, paramBoolean));
  }

  protected void a(AdRequest paramAdRequest)
  {
    this.i = paramAdRequest;
    this.o = false;
    this.t = new Thread(this);
    this.t.start();
  }

  public void a(AdSize paramAdSize)
  {
    try
    {
      this.n = paramAdSize;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void a(d paramd)
  {
    try
    {
      this.v = paramd;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  protected void a(String paramString)
  {
    try
    {
      this.l.add(paramString);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  protected void a(String paramString1, String paramString2)
  {
    try
    {
      this.b = paramString2;
      this.c = paramString1;
      notify();
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  protected void a(boolean paramBoolean)
  {
    try
    {
      this.f = paramBoolean;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  protected void b()
  {
    try
    {
      if (TextUtils.isEmpty(this.e))
      {
        com.google.ads.util.b.b("Got a mediation response with no content type. Aborting mediation.");
        a(AdRequest.ErrorCode.INTERNAL_ERROR, false);
        return;
      }
      if (!this.e.startsWith("application/json"))
      {
        com.google.ads.util.b.b("Got a mediation response with a content type: '" + this.e + "'. Expected something starting with 'application/json'. Aborting mediation.");
        a(AdRequest.ErrorCode.INTERNAL_ERROR, false);
        return;
      }
    }
    catch (JSONException localJSONException)
    {
      com.google.ads.util.b.b("AdLoader can't parse gWhirl server configuration.", localJSONException);
      a(AdRequest.ErrorCode.INTERNAL_ERROR, false);
      return;
    }
    com.google.ads.c localc = com.google.ads.c.a(this.c);
    a(this.d, localc, this.h.i());
    this.h.a(new o(this, localc));
  }

  protected void b(String paramString)
  {
    try
    {
      this.e = paramString;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void b(boolean paramBoolean)
  {
    try
    {
      this.p = paramBoolean;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  protected void c()
  {
    try
    {
      this.r = true;
      notify();
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  protected void c(String paramString)
  {
    try
    {
      this.d = paramString;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void c(boolean paramBoolean)
  {
    try
    {
      this.u = paramBoolean;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void d(String paramString)
  {
    try
    {
      this.k = paramString;
      notify();
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void d(boolean paramBoolean)
  {
    try
    {
      this.a = paramBoolean;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void e(String paramString)
  {
    try
    {
      this.m = paramString;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  // ERROR //
  public void run()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 105	com/google/ads/internal/c:j	Landroid/webkit/WebView;
    //   6: ifnull +10 -> 16
    //   9: aload_0
    //   10: getfield 136	com/google/ads/internal/c:g	Lcom/google/ads/internal/f;
    //   13: ifnonnull +20 -> 33
    //   16: ldc_w 752
    //   19: invokestatic 143	com/google/ads/util/b:e	(Ljava/lang/String;)V
    //   22: aload_0
    //   23: getstatic 712	com/google/ads/AdRequest$ErrorCode:INTERNAL_ERROR	Lcom/google/ads/AdRequest$ErrorCode;
    //   26: iconst_0
    //   27: invokevirtual 714	com/google/ads/internal/c:a	(Lcom/google/ads/AdRequest$ErrorCode;Z)V
    //   30: aload_0
    //   31: monitorexit
    //   32: return
    //   33: aload_0
    //   34: getfield 54	com/google/ads/internal/c:h	Lcom/google/ads/internal/d;
    //   37: invokevirtual 86	com/google/ads/internal/d:h	()Lcom/google/ads/m;
    //   40: getfield 91	com/google/ads/m:e	Lcom/google/ads/util/i$d;
    //   43: invokevirtual 96	com/google/ads/util/i$d:a	()Ljava/lang/Object;
    //   46: checkcast 98	android/app/Activity
    //   49: astore_3
    //   50: aload_3
    //   51: ifnonnull +25 -> 76
    //   54: ldc_w 754
    //   57: invokestatic 143	com/google/ads/util/b:e	(Ljava/lang/String;)V
    //   60: aload_0
    //   61: getstatic 712	com/google/ads/AdRequest$ErrorCode:INTERNAL_ERROR	Lcom/google/ads/AdRequest$ErrorCode;
    //   64: iconst_0
    //   65: invokevirtual 714	com/google/ads/internal/c:a	(Lcom/google/ads/AdRequest$ErrorCode;Z)V
    //   68: aload_0
    //   69: monitorexit
    //   70: return
    //   71: astore_2
    //   72: aload_0
    //   73: monitorexit
    //   74: aload_2
    //   75: athrow
    //   76: aload_0
    //   77: getfield 54	com/google/ads/internal/c:h	Lcom/google/ads/internal/d;
    //   80: invokevirtual 755	com/google/ads/internal/d:o	()J
    //   83: lstore 4
    //   85: invokestatic 760	android/os/SystemClock:elapsedRealtime	()J
    //   88: lstore 6
    //   90: aload_0
    //   91: getfield 243	com/google/ads/internal/c:i	Lcom/google/ads/AdRequest;
    //   94: aload_0
    //   95: getfield 54	com/google/ads/internal/c:h	Lcom/google/ads/internal/d;
    //   98: invokevirtual 86	com/google/ads/internal/d:h	()Lcom/google/ads/m;
    //   101: getfield 495	com/google/ads/m:f	Lcom/google/ads/util/i$b;
    //   104: invokevirtual 373	com/google/ads/util/i$b:a	()Ljava/lang/Object;
    //   107: checkcast 379	android/content/Context
    //   110: invokevirtual 766	com/google/ads/AdRequest:getRequestMap	(Landroid/content/Context;)Ljava/util/Map;
    //   113: astore 8
    //   115: aload 8
    //   117: ldc_w 768
    //   120: invokeinterface 772 2 0
    //   125: astore 9
    //   127: aload 9
    //   129: instanceof 298
    //   132: ifeq +142 -> 274
    //   135: aload 9
    //   137: checkcast 298	java/util/Map
    //   140: astore 31
    //   142: aload 31
    //   144: ldc_w 774
    //   147: invokeinterface 772 2 0
    //   152: astore 32
    //   154: aload 32
    //   156: instanceof 151
    //   159: ifeq +12 -> 171
    //   162: aload_0
    //   163: aload 32
    //   165: checkcast 151	java/lang/String
    //   168: putfield 58	com/google/ads/internal/c:b	Ljava/lang/String;
    //   171: aload 31
    //   173: ldc_w 776
    //   176: invokeinterface 772 2 0
    //   181: astore 33
    //   183: aload 33
    //   185: instanceof 151
    //   188: ifeq +12 -> 200
    //   191: aload_0
    //   192: aload 33
    //   194: checkcast 151	java/lang/String
    //   197: putfield 56	com/google/ads/internal/c:k	Ljava/lang/String;
    //   200: aload 31
    //   202: ldc_w 778
    //   205: invokeinterface 772 2 0
    //   210: astore 34
    //   212: aload 34
    //   214: instanceof 151
    //   217: ifeq +19 -> 236
    //   220: aload 34
    //   222: ldc_w 779
    //   225: invokevirtual 782	java/lang/Object:equals	(Ljava/lang/Object;)Z
    //   228: ifeq +125 -> 353
    //   231: aload_0
    //   232: iconst_1
    //   233: putfield 73	com/google/ads/internal/c:s	I
    //   236: aload 31
    //   238: ldc_w 784
    //   241: invokeinterface 772 2 0
    //   246: astore 35
    //   248: aload 35
    //   250: instanceof 151
    //   253: ifeq +21 -> 274
    //   256: aload 35
    //   258: ldc_w 785
    //   261: invokevirtual 782	java/lang/Object:equals	(Ljava/lang/Object;)Z
    //   264: ifeq +10 -> 274
    //   267: aload_0
    //   268: getfield 54	com/google/ads/internal/c:h	Lcom/google/ads/internal/d;
    //   271: invokevirtual 787	com/google/ads/internal/d:d	()V
    //   274: aload_0
    //   275: getfield 58	com/google/ads/internal/c:b	Ljava/lang/String;
    //   278: ifnonnull +569 -> 847
    //   281: aload_0
    //   282: getfield 56	com/google/ads/internal/c:k	Ljava/lang/String;
    //   285: astore 15
    //   287: aload 15
    //   289: ifnonnull +226 -> 515
    //   292: aload_0
    //   293: aload 8
    //   295: aload_3
    //   296: invokevirtual 789	com/google/ads/internal/c:a	(Ljava/util/Map;Landroid/app/Activity;)Ljava/lang/String;
    //   299: astore 24
    //   301: aload_0
    //   302: aload 24
    //   304: aload_0
    //   305: invokespecial 791	com/google/ads/internal/c:f	()Ljava/lang/String;
    //   308: invokespecial 793	com/google/ads/internal/c:b	(Ljava/lang/String;Ljava/lang/String;)V
    //   311: invokestatic 760	android/os/SystemClock:elapsedRealtime	()J
    //   314: lstore 25
    //   316: lload 4
    //   318: lload 25
    //   320: lload 6
    //   322: lsub
    //   323: lsub
    //   324: lstore 27
    //   326: lload 27
    //   328: lconst_0
    //   329: lcmp
    //   330: ifle +9 -> 339
    //   333: aload_0
    //   334: lload 27
    //   336: invokevirtual 797	java/lang/Object:wait	(J)V
    //   339: aload_0
    //   340: getfield 677	com/google/ads/internal/c:o	Z
    //   343: istore 29
    //   345: iload 29
    //   347: ifeq +104 -> 451
    //   350: aload_0
    //   351: monitorexit
    //   352: return
    //   353: aload 34
    //   355: ldc_w 798
    //   358: invokevirtual 782	java/lang/Object:equals	(Ljava/lang/Object;)Z
    //   361: ifeq -125 -> 236
    //   364: aload_0
    //   365: iconst_0
    //   366: putfield 73	com/google/ads/internal/c:s	I
    //   369: goto -133 -> 236
    //   372: astore_1
    //   373: ldc_w 800
    //   376: aload_1
    //   377: invokestatic 213	com/google/ads/util/b:b	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   380: aload_0
    //   381: getstatic 712	com/google/ads/AdRequest$ErrorCode:INTERNAL_ERROR	Lcom/google/ads/AdRequest$ErrorCode;
    //   384: iconst_1
    //   385: invokevirtual 714	com/google/ads/internal/c:a	(Lcom/google/ads/AdRequest$ErrorCode;Z)V
    //   388: aload_0
    //   389: monitorexit
    //   390: return
    //   391: astore 23
    //   393: new 215	java/lang/StringBuilder
    //   396: dup
    //   397: ldc_w 802
    //   400: invokespecial 219	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   403: aload 23
    //   405: invokevirtual 805	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   408: invokevirtual 229	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   411: invokestatic 208	com/google/ads/util/b:c	(Ljava/lang/String;)V
    //   414: aload_0
    //   415: getstatic 712	com/google/ads/AdRequest$ErrorCode:INTERNAL_ERROR	Lcom/google/ads/AdRequest$ErrorCode;
    //   418: iconst_0
    //   419: invokevirtual 714	com/google/ads/internal/c:a	(Lcom/google/ads/AdRequest$ErrorCode;Z)V
    //   422: aload_0
    //   423: monitorexit
    //   424: return
    //   425: astore 30
    //   427: new 215	java/lang/StringBuilder
    //   430: dup
    //   431: ldc_w 807
    //   434: invokespecial 219	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   437: aload 30
    //   439: invokevirtual 805	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   442: invokevirtual 229	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   445: invokestatic 661	com/google/ads/util/b:a	(Ljava/lang/String;)V
    //   448: aload_0
    //   449: monitorexit
    //   450: return
    //   451: aload_0
    //   452: getfield 69	com/google/ads/internal/c:q	Lcom/google/ads/AdRequest$ErrorCode;
    //   455: ifnull +15 -> 470
    //   458: aload_0
    //   459: aload_0
    //   460: getfield 69	com/google/ads/internal/c:q	Lcom/google/ads/AdRequest$ErrorCode;
    //   463: iconst_0
    //   464: invokevirtual 714	com/google/ads/internal/c:a	(Lcom/google/ads/AdRequest$ErrorCode;Z)V
    //   467: aload_0
    //   468: monitorexit
    //   469: return
    //   470: aload_0
    //   471: getfield 56	com/google/ads/internal/c:k	Ljava/lang/String;
    //   474: ifnonnull +41 -> 515
    //   477: new 215	java/lang/StringBuilder
    //   480: dup
    //   481: ldc_w 809
    //   484: invokespecial 219	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   487: lload 4
    //   489: invokevirtual 812	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   492: ldc_w 814
    //   495: invokevirtual 223	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   498: invokevirtual 229	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   501: invokestatic 208	com/google/ads/util/b:c	(Ljava/lang/String;)V
    //   504: aload_0
    //   505: getstatic 817	com/google/ads/AdRequest$ErrorCode:NETWORK_ERROR	Lcom/google/ads/AdRequest$ErrorCode;
    //   508: iconst_0
    //   509: invokevirtual 714	com/google/ads/internal/c:a	(Lcom/google/ads/AdRequest$ErrorCode;Z)V
    //   512: aload_0
    //   513: monitorexit
    //   514: return
    //   515: aload_0
    //   516: getfield 54	com/google/ads/internal/c:h	Lcom/google/ads/internal/d;
    //   519: invokevirtual 270	com/google/ads/internal/d:m	()Lcom/google/ads/internal/g;
    //   522: astore 16
    //   524: getstatic 822	com/google/ads/internal/c$1:a	[I
    //   527: aload_0
    //   528: getfield 51	com/google/ads/internal/c:v	Lcom/google/ads/internal/c$d;
    //   531: invokevirtual 823	com/google/ads/internal/c$d:ordinal	()I
    //   534: iaload
    //   535: tableswitch	default:+29 -> 564, 1:+106->641, 2:+130->665, 3:+144->679, 4:+163->698
    //   565: getfield 745	com/google/ads/internal/c:a	Z
    //   568: ifne +248 -> 816
    //   571: ldc_w 825
    //   574: invokestatic 661	com/google/ads/util/b:a	(Ljava/lang/String;)V
    //   577: aload_0
    //   578: getfield 136	com/google/ads/internal/c:g	Lcom/google/ads/internal/f;
    //   581: aload_0
    //   582: getfield 743	com/google/ads/internal/c:u	Z
    //   585: invokevirtual 827	com/google/ads/internal/f:a	(Z)V
    //   588: aload_0
    //   589: getfield 136	com/google/ads/internal/c:g	Lcom/google/ads/internal/f;
    //   592: aload_0
    //   593: getfield 56	com/google/ads/internal/c:k	Ljava/lang/String;
    //   596: invokevirtual 828	com/google/ads/internal/f:a	(Ljava/lang/String;)V
    //   599: invokestatic 760	android/os/SystemClock:elapsedRealtime	()J
    //   602: lstore 17
    //   604: lload 4
    //   606: lload 17
    //   608: lload 6
    //   610: lsub
    //   611: lsub
    //   612: lstore 19
    //   614: lload 19
    //   616: lconst_0
    //   617: lcmp
    //   618: ifle +9 -> 627
    //   621: aload_0
    //   622: lload 19
    //   624: invokevirtual 797	java/lang/Object:wait	(J)V
    //   627: aload_0
    //   628: getfield 677	com/google/ads/internal/c:o	Z
    //   631: istore 21
    //   633: iload 21
    //   635: ifeq +117 -> 752
    //   638: aload_0
    //   639: monitorexit
    //   640: return
    //   641: aload 16
    //   643: invokevirtual 830	com/google/ads/internal/g:r	()V
    //   646: aload 16
    //   648: invokevirtual 832	com/google/ads/internal/g:u	()V
    //   651: aload 16
    //   653: invokevirtual 834	com/google/ads/internal/g:x	()V
    //   656: ldc_w 836
    //   659: invokestatic 208	com/google/ads/util/b:c	(Ljava/lang/String;)V
    //   662: goto -98 -> 564
    //   665: aload 16
    //   667: invokevirtual 838	com/google/ads/internal/g:t	()V
    //   670: ldc_w 840
    //   673: invokestatic 208	com/google/ads/util/b:c	(Ljava/lang/String;)V
    //   676: goto -112 -> 564
    //   679: aload 16
    //   681: invokevirtual 842	com/google/ads/internal/g:w	()V
    //   684: aload 16
    //   686: invokevirtual 844	com/google/ads/internal/g:q	()V
    //   689: ldc_w 846
    //   692: invokestatic 208	com/google/ads/util/b:c	(Ljava/lang/String;)V
    //   695: goto -131 -> 564
    //   698: aload 16
    //   700: invokevirtual 844	com/google/ads/internal/g:q	()V
    //   703: ldc_w 848
    //   706: invokestatic 208	com/google/ads/util/b:c	(Ljava/lang/String;)V
    //   709: ldc_w 850
    //   712: invokestatic 208	com/google/ads/util/b:c	(Ljava/lang/String;)V
    //   715: aload_0
    //   716: getstatic 817	com/google/ads/AdRequest$ErrorCode:NETWORK_ERROR	Lcom/google/ads/AdRequest$ErrorCode;
    //   719: iconst_0
    //   720: invokevirtual 714	com/google/ads/internal/c:a	(Lcom/google/ads/AdRequest$ErrorCode;Z)V
    //   723: aload_0
    //   724: monitorexit
    //   725: return
    //   726: astore 22
    //   728: new 215	java/lang/StringBuilder
    //   731: dup
    //   732: ldc_w 852
    //   735: invokespecial 219	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   738: aload 22
    //   740: invokevirtual 805	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   743: invokevirtual 229	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   746: invokestatic 661	com/google/ads/util/b:a	(Ljava/lang/String;)V
    //   749: aload_0
    //   750: monitorexit
    //   751: return
    //   752: aload_0
    //   753: getfield 69	com/google/ads/internal/c:q	Lcom/google/ads/AdRequest$ErrorCode;
    //   756: ifnull +15 -> 771
    //   759: aload_0
    //   760: aload_0
    //   761: getfield 69	com/google/ads/internal/c:q	Lcom/google/ads/AdRequest$ErrorCode;
    //   764: iconst_0
    //   765: invokevirtual 714	com/google/ads/internal/c:a	(Lcom/google/ads/AdRequest$ErrorCode;Z)V
    //   768: aload_0
    //   769: monitorexit
    //   770: return
    //   771: aload_0
    //   772: getfield 60	com/google/ads/internal/c:c	Ljava/lang/String;
    //   775: ifnonnull +72 -> 847
    //   778: new 215	java/lang/StringBuilder
    //   781: dup
    //   782: ldc_w 809
    //   785: invokespecial 219	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   788: lload 4
    //   790: invokevirtual 812	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   793: ldc_w 854
    //   796: invokevirtual 223	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   799: invokevirtual 229	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   802: invokestatic 208	com/google/ads/util/b:c	(Ljava/lang/String;)V
    //   805: aload_0
    //   806: getstatic 817	com/google/ads/AdRequest$ErrorCode:NETWORK_ERROR	Lcom/google/ads/AdRequest$ErrorCode;
    //   809: iconst_0
    //   810: invokevirtual 714	com/google/ads/internal/c:a	(Lcom/google/ads/AdRequest$ErrorCode;Z)V
    //   813: aload_0
    //   814: monitorexit
    //   815: return
    //   816: aload_0
    //   817: aload_0
    //   818: getfield 56	com/google/ads/internal/c:k	Ljava/lang/String;
    //   821: putfield 58	com/google/ads/internal/c:b	Ljava/lang/String;
    //   824: new 215	java/lang/StringBuilder
    //   827: dup
    //   828: ldc_w 856
    //   831: invokespecial 219	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   834: aload_0
    //   835: getfield 58	com/google/ads/internal/c:b	Ljava/lang/String;
    //   838: invokevirtual 223	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   841: invokevirtual 229	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   844: invokestatic 661	com/google/ads/util/b:a	(Ljava/lang/String;)V
    //   847: aload_0
    //   848: getfield 745	com/google/ads/internal/c:a	Z
    //   851: ifne +221 -> 1072
    //   854: aload_0
    //   855: getfield 75	com/google/ads/internal/c:f	Z
    //   858: ifeq +18 -> 876
    //   861: aload_0
    //   862: getfield 54	com/google/ads/internal/c:h	Lcom/google/ads/internal/d;
    //   865: iconst_1
    //   866: invokevirtual 858	com/google/ads/internal/d:b	(Z)V
    //   869: aload_0
    //   870: invokevirtual 860	com/google/ads/internal/c:b	()V
    //   873: aload_0
    //   874: monitorexit
    //   875: return
    //   876: aload_0
    //   877: getfield 703	com/google/ads/internal/c:e	Ljava/lang/String;
    //   880: ifnull +69 -> 949
    //   883: aload_0
    //   884: getfield 703	com/google/ads/internal/c:e	Ljava/lang/String;
    //   887: ldc_w 716
    //   890: invokevirtual 720	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   893: ifne +16 -> 909
    //   896: aload_0
    //   897: getfield 703	com/google/ads/internal/c:e	Ljava/lang/String;
    //   900: ldc_w 862
    //   903: invokevirtual 720	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   906: ifeq +43 -> 949
    //   909: new 215	java/lang/StringBuilder
    //   912: dup
    //   913: ldc_w 864
    //   916: invokespecial 219	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   919: aload_0
    //   920: getfield 703	com/google/ads/internal/c:e	Ljava/lang/String;
    //   923: invokevirtual 223	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   926: ldc_w 866
    //   929: invokevirtual 223	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   932: invokevirtual 229	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   935: invokestatic 707	com/google/ads/util/b:b	(Ljava/lang/String;)V
    //   938: aload_0
    //   939: getstatic 712	com/google/ads/AdRequest$ErrorCode:INTERNAL_ERROR	Lcom/google/ads/AdRequest$ErrorCode;
    //   942: iconst_0
    //   943: invokevirtual 714	com/google/ads/internal/c:a	(Lcom/google/ads/AdRequest$ErrorCode;Z)V
    //   946: aload_0
    //   947: monitorexit
    //   948: return
    //   949: aload_0
    //   950: getfield 54	com/google/ads/internal/c:h	Lcom/google/ads/internal/d;
    //   953: invokevirtual 86	com/google/ads/internal/d:h	()Lcom/google/ads/m;
    //   956: getfield 537	com/google/ads/m:l	Lcom/google/ads/util/i$c;
    //   959: invokevirtual 540	com/google/ads/util/i$c:a	()Ljava/lang/Object;
    //   962: ifnull +92 -> 1054
    //   965: aload_0
    //   966: getfield 81	com/google/ads/internal/c:n	Lcom/google/ads/AdSize;
    //   969: ifnonnull +20 -> 989
    //   972: ldc_w 868
    //   975: invokestatic 707	com/google/ads/util/b:b	(Ljava/lang/String;)V
    //   978: aload_0
    //   979: getstatic 712	com/google/ads/AdRequest$ErrorCode:INTERNAL_ERROR	Lcom/google/ads/AdRequest$ErrorCode;
    //   982: iconst_0
    //   983: invokevirtual 714	com/google/ads/internal/c:a	(Lcom/google/ads/AdRequest$ErrorCode;Z)V
    //   986: aload_0
    //   987: monitorexit
    //   988: return
    //   989: aload_0
    //   990: getfield 54	com/google/ads/internal/c:h	Lcom/google/ads/internal/d;
    //   993: invokevirtual 86	com/google/ads/internal/d:h	()Lcom/google/ads/m;
    //   996: getfield 537	com/google/ads/m:l	Lcom/google/ads/util/i$c;
    //   999: invokevirtual 540	com/google/ads/util/i$c:a	()Ljava/lang/Object;
    //   1002: checkcast 870	[Ljava/lang/Object;
    //   1005: invokestatic 876	java/util/Arrays:asList	([Ljava/lang/Object;)Ljava/util/List;
    //   1008: aload_0
    //   1009: getfield 81	com/google/ads/internal/c:n	Lcom/google/ads/AdSize;
    //   1012: invokeinterface 880 2 0
    //   1017: ifne +55 -> 1072
    //   1020: new 215	java/lang/StringBuilder
    //   1023: dup
    //   1024: ldc_w 882
    //   1027: invokespecial 219	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   1030: aload_0
    //   1031: getfield 81	com/google/ads/internal/c:n	Lcom/google/ads/AdSize;
    //   1034: invokevirtual 805	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1037: invokevirtual 229	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1040: invokestatic 707	com/google/ads/util/b:b	(Ljava/lang/String;)V
    //   1043: aload_0
    //   1044: getstatic 712	com/google/ads/AdRequest$ErrorCode:INTERNAL_ERROR	Lcom/google/ads/AdRequest$ErrorCode;
    //   1047: iconst_0
    //   1048: invokevirtual 714	com/google/ads/internal/c:a	(Lcom/google/ads/AdRequest$ErrorCode;Z)V
    //   1051: aload_0
    //   1052: monitorexit
    //   1053: return
    //   1054: aload_0
    //   1055: getfield 81	com/google/ads/internal/c:n	Lcom/google/ads/AdSize;
    //   1058: ifnull +14 -> 1072
    //   1061: ldc_w 884
    //   1064: invokestatic 143	com/google/ads/util/b:e	(Ljava/lang/String;)V
    //   1067: aload_0
    //   1068: aconst_null
    //   1069: putfield 81	com/google/ads/internal/c:n	Lcom/google/ads/AdSize;
    //   1072: aload_0
    //   1073: getfield 54	com/google/ads/internal/c:h	Lcom/google/ads/internal/d;
    //   1076: iconst_0
    //   1077: invokevirtual 858	com/google/ads/internal/d:b	(Z)V
    //   1080: aload_0
    //   1081: invokespecial 885	com/google/ads/internal/c:i	()V
    //   1084: invokestatic 760	android/os/SystemClock:elapsedRealtime	()J
    //   1087: lstore 10
    //   1089: lload 4
    //   1091: lload 10
    //   1093: lload 6
    //   1095: lsub
    //   1096: lsub
    //   1097: lstore 12
    //   1099: lload 12
    //   1101: lconst_0
    //   1102: lcmp
    //   1103: ifle +9 -> 1112
    //   1106: aload_0
    //   1107: lload 12
    //   1109: invokevirtual 797	java/lang/Object:wait	(J)V
    //   1112: aload_0
    //   1113: getfield 71	com/google/ads/internal/c:r	Z
    //   1116: ifeq +36 -> 1152
    //   1119: aload_0
    //   1120: invokespecial 887	com/google/ads/internal/c:j	()V
    //   1123: goto -735 -> 388
    //   1126: astore 14
    //   1128: new 215	java/lang/StringBuilder
    //   1131: dup
    //   1132: ldc_w 889
    //   1135: invokespecial 219	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   1138: aload 14
    //   1140: invokevirtual 805	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1143: invokevirtual 229	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1146: invokestatic 661	com/google/ads/util/b:a	(Ljava/lang/String;)V
    //   1149: aload_0
    //   1150: monitorexit
    //   1151: return
    //   1152: new 215	java/lang/StringBuilder
    //   1155: dup
    //   1156: ldc_w 809
    //   1159: invokespecial 219	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   1162: lload 4
    //   1164: invokevirtual 812	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   1167: ldc_w 891
    //   1170: invokevirtual 223	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1173: invokevirtual 229	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1176: invokestatic 208	com/google/ads/util/b:c	(Ljava/lang/String;)V
    //   1179: aload_0
    //   1180: getstatic 817	com/google/ads/AdRequest$ErrorCode:NETWORK_ERROR	Lcom/google/ads/AdRequest$ErrorCode;
    //   1183: iconst_1
    //   1184: invokevirtual 714	com/google/ads/internal/c:a	(Lcom/google/ads/AdRequest$ErrorCode;Z)V
    //   1187: goto -799 -> 388
    //
    // Exception table:
    //   from	to	target	type
    //   2	16	71	finally
    //   16	30	71	finally
    //   30	32	71	finally
    //   33	50	71	finally
    //   54	68	71	finally
    //   68	70	71	finally
    //   76	171	71	finally
    //   171	200	71	finally
    //   200	236	71	finally
    //   236	274	71	finally
    //   274	287	71	finally
    //   292	301	71	finally
    //   301	316	71	finally
    //   333	339	71	finally
    //   339	345	71	finally
    //   350	352	71	finally
    //   353	369	71	finally
    //   373	388	71	finally
    //   388	390	71	finally
    //   393	422	71	finally
    //   422	424	71	finally
    //   427	448	71	finally
    //   448	450	71	finally
    //   451	467	71	finally
    //   467	469	71	finally
    //   470	512	71	finally
    //   512	514	71	finally
    //   515	564	71	finally
    //   564	604	71	finally
    //   621	627	71	finally
    //   627	633	71	finally
    //   638	640	71	finally
    //   641	662	71	finally
    //   665	676	71	finally
    //   679	695	71	finally
    //   698	723	71	finally
    //   723	725	71	finally
    //   728	749	71	finally
    //   749	751	71	finally
    //   752	768	71	finally
    //   768	770	71	finally
    //   771	813	71	finally
    //   813	815	71	finally
    //   816	847	71	finally
    //   847	873	71	finally
    //   873	875	71	finally
    //   876	909	71	finally
    //   909	946	71	finally
    //   946	948	71	finally
    //   949	986	71	finally
    //   986	988	71	finally
    //   989	1051	71	finally
    //   1051	1053	71	finally
    //   1054	1072	71	finally
    //   1072	1089	71	finally
    //   1106	1112	71	finally
    //   1112	1123	71	finally
    //   1128	1149	71	finally
    //   1149	1151	71	finally
    //   1152	1187	71	finally
    //   2	16	372	java/lang/Throwable
    //   16	30	372	java/lang/Throwable
    //   33	50	372	java/lang/Throwable
    //   54	68	372	java/lang/Throwable
    //   76	171	372	java/lang/Throwable
    //   171	200	372	java/lang/Throwable
    //   200	236	372	java/lang/Throwable
    //   236	274	372	java/lang/Throwable
    //   274	287	372	java/lang/Throwable
    //   292	301	372	java/lang/Throwable
    //   301	316	372	java/lang/Throwable
    //   333	339	372	java/lang/Throwable
    //   339	345	372	java/lang/Throwable
    //   353	369	372	java/lang/Throwable
    //   393	422	372	java/lang/Throwable
    //   427	448	372	java/lang/Throwable
    //   451	467	372	java/lang/Throwable
    //   470	512	372	java/lang/Throwable
    //   515	564	372	java/lang/Throwable
    //   564	604	372	java/lang/Throwable
    //   621	627	372	java/lang/Throwable
    //   627	633	372	java/lang/Throwable
    //   641	662	372	java/lang/Throwable
    //   665	676	372	java/lang/Throwable
    //   679	695	372	java/lang/Throwable
    //   698	723	372	java/lang/Throwable
    //   728	749	372	java/lang/Throwable
    //   752	768	372	java/lang/Throwable
    //   771	813	372	java/lang/Throwable
    //   816	847	372	java/lang/Throwable
    //   847	873	372	java/lang/Throwable
    //   876	909	372	java/lang/Throwable
    //   909	946	372	java/lang/Throwable
    //   949	986	372	java/lang/Throwable
    //   989	1051	372	java/lang/Throwable
    //   1054	1072	372	java/lang/Throwable
    //   1072	1089	372	java/lang/Throwable
    //   1106	1112	372	java/lang/Throwable
    //   1112	1123	372	java/lang/Throwable
    //   1128	1149	372	java/lang/Throwable
    //   1152	1187	372	java/lang/Throwable
    //   292	301	391	com/google/ads/internal/c$b
    //   333	339	425	java/lang/InterruptedException
    //   621	627	726	java/lang/InterruptedException
    //   1106	1112	1126	java/lang/InterruptedException
  }

  private static final class a
    implements Runnable
  {
    private final d a;
    private final WebView b;
    private final f c;
    private final AdRequest.ErrorCode d;
    private final boolean e;

    public a(d paramd, WebView paramWebView, f paramf, AdRequest.ErrorCode paramErrorCode, boolean paramBoolean)
    {
      this.a = paramd;
      this.b = paramWebView;
      this.c = paramf;
      this.d = paramErrorCode;
      this.e = paramBoolean;
    }

    public final void run()
    {
      if (this.b != null)
      {
        this.b.stopLoading();
        this.b.destroy();
      }
      if (this.c != null)
        this.c.a();
      if (this.e)
      {
        AdWebView localAdWebView = this.a.k();
        localAdWebView.stopLoading();
        localAdWebView.setVisibility(8);
      }
      this.a.a(this.d);
    }
  }

  private final class b extends Exception
  {
    public b(String arg2)
    {
      super();
    }
  }

  private final class c
    implements Runnable
  {
    private final String b;
    private final String c;
    private final WebView d;

    public c(WebView paramString1, String paramString2, String arg4)
    {
      this.d = paramString1;
      this.b = paramString2;
      Object localObject;
      this.c = localObject;
    }

    public final void run()
    {
      if (this.c != null)
      {
        this.d.loadDataWithBaseURL(this.b, this.c, "text/html", "utf-8", null);
        return;
      }
      this.d.loadUrl(this.b);
    }
  }

  public static enum d
  {
    public String e;

    static
    {
      d[] arrayOfd = new d[4];
      arrayOfd[0] = a;
      arrayOfd[1] = b;
      arrayOfd[2] = c;
      arrayOfd[3] = d;
    }

    private d(String paramString)
    {
      this.e = paramString;
    }
  }

  private static final class e
    implements Runnable
  {
    private final d a;
    private final WebView b;
    private final LinkedList<String> c;
    private final int d;
    private final boolean e;
    private final String f;
    private final AdSize g;

    public e(d paramd, WebView paramWebView, LinkedList<String> paramLinkedList, int paramInt, boolean paramBoolean, String paramString, AdSize paramAdSize)
    {
      this.a = paramd;
      this.b = paramWebView;
      this.c = paramLinkedList;
      this.d = paramInt;
      this.e = paramBoolean;
      this.f = paramString;
      this.g = paramAdSize;
    }

    public final void run()
    {
      if (this.b != null)
      {
        this.b.stopLoading();
        this.b.destroy();
      }
      this.a.a(this.c);
      this.a.a(this.d);
      this.a.a(this.e);
      this.a.a(this.f);
      if (this.g != null)
      {
        ((h)this.a.h().k.a()).b(this.g);
        this.a.k().setAdSize(this.g);
      }
      this.a.C();
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.ads.internal.c
 * JD-Core Version:    0.6.2
 */