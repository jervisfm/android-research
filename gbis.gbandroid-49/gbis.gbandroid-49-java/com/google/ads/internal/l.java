package com.google.ads.internal;

import com.google.ads.aa;
import com.google.ads.ab;
import com.google.ads.ai;
import com.google.ads.n;
import com.google.ads.o;
import com.google.ads.p;
import com.google.ads.q;
import com.google.ads.r;
import com.google.ads.s;
import com.google.ads.t;
import com.google.ads.u;
import com.google.ads.y;
import com.google.ads.z;
import java.util.HashMap;

final class l extends HashMap<String, n>
{
  l()
  {
    put("/open", new z());
    put("/canOpenURLs", new p());
    put("/close", new r());
    put("/customClose", new s());
    put("/appEvent", new o());
    put("/evalInOpener", new t());
    put("/log", new y());
    put("/click", new q());
    put("/httpTrack", new u());
    put("/touch", new aa());
    put("/video", new ab());
    put("/plusOne", new ai());
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.ads.internal.l
 * JD-Core Version:    0.6.2
 */