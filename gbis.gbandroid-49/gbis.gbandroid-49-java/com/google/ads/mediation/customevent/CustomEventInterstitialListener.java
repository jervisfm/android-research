package com.google.ads.mediation.customevent;

public abstract interface CustomEventInterstitialListener extends CustomEventListener
{
  public abstract void onReceivedAd();
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.ads.mediation.customevent.CustomEventInterstitialListener
 * JD-Core Version:    0.6.2
 */