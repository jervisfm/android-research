package com.google.ads.mediation.inmobi;

import android.app.Activity;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.AdRequest.Gender;
import com.google.ads.AdSize;
import com.google.ads.mediation.MediationAdRequest;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationBannerListener;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationInterstitialListener;
import com.inmobi.androidsdk.IMAdInterstitial;
import com.inmobi.androidsdk.IMAdInterstitialListener;
import com.inmobi.androidsdk.IMAdListener;
import com.inmobi.androidsdk.IMAdRequest;
import com.inmobi.androidsdk.IMAdRequest.ErrorCode;
import com.inmobi.androidsdk.IMAdRequest.GenderType;
import com.inmobi.androidsdk.IMAdView;

public final class InMobiAdapter
  implements MediationBannerAdapter<InMobiAdapterExtras, InMobiAdapterServerParameters>, MediationInterstitialAdapter<InMobiAdapterExtras, InMobiAdapterServerParameters>
{
  private MediationBannerListener a;
  private MediationInterstitialListener b;
  private IMAdInterstitial c;
  private IMAdView d;
  private FrameLayout e;

  private static IMAdRequest a(MediationAdRequest paramMediationAdRequest, InMobiAdapterExtras paramInMobiAdapterExtras)
  {
    if (paramInMobiAdapterExtras == null)
      paramInMobiAdapterExtras = new InMobiAdapterExtras();
    IMAdRequest localIMAdRequest = new IMAdRequest();
    if (paramMediationAdRequest.getAgeInYears() != null)
      localIMAdRequest.setAge(paramMediationAdRequest.getAgeInYears().intValue());
    if (paramInMobiAdapterExtras.getAreaCode() != null)
      localIMAdRequest.setAreaCode(paramInMobiAdapterExtras.getAreaCode());
    if (paramMediationAdRequest.getLocation() != null)
      localIMAdRequest.setCurrentLocation(paramMediationAdRequest.getLocation());
    if (paramMediationAdRequest.getBirthday() != null)
      localIMAdRequest.setDateOfBirth(paramMediationAdRequest.getBirthday());
    if (paramInMobiAdapterExtras.getEducation() != null)
      localIMAdRequest.setEducation(paramInMobiAdapterExtras.getEducation().getDescription());
    if (paramInMobiAdapterExtras.getEthnicity() != null)
      localIMAdRequest.setEthnicity(paramInMobiAdapterExtras.getEthnicity().getDescription());
    if (paramMediationAdRequest.getGender() != null)
      switch (1.a[paramMediationAdRequest.getGender().ordinal()])
      {
      default:
      case 1:
      case 2:
      }
    while (true)
    {
      if (paramInMobiAdapterExtras.getIncome() != null)
        localIMAdRequest.setIncome(paramInMobiAdapterExtras.getIncome().intValue());
      if (paramInMobiAdapterExtras.getInterests() != null)
        localIMAdRequest.setInterests(TextUtils.join(", ", paramInMobiAdapterExtras.getInterests()));
      if (paramMediationAdRequest.getKeywords() != null)
        localIMAdRequest.setKeywords(TextUtils.join(", ", paramMediationAdRequest.getKeywords()));
      if (paramInMobiAdapterExtras.getPostalCode() != null)
        localIMAdRequest.setPostalCode(paramInMobiAdapterExtras.getPostalCode());
      if (paramInMobiAdapterExtras.getSearchString() != null)
        localIMAdRequest.setSearchString(paramInMobiAdapterExtras.getSearchString());
      localIMAdRequest.setTestMode(paramMediationAdRequest.isTesting());
      localIMAdRequest.setLocationInquiryAllowed(false);
      return localIMAdRequest;
      localIMAdRequest.setGender(IMAdRequest.GenderType.MALE);
      continue;
      localIMAdRequest.setGender(IMAdRequest.GenderType.FEMALE);
    }
  }

  public final void destroy()
  {
  }

  public final Class<InMobiAdapterExtras> getAdditionalParametersType()
  {
    return InMobiAdapterExtras.class;
  }

  public final View getBannerView()
  {
    return this.e;
  }

  public final Class<InMobiAdapterServerParameters> getServerParametersType()
  {
    return InMobiAdapterServerParameters.class;
  }

  public final void requestBannerAd(MediationBannerListener paramMediationBannerListener, Activity paramActivity, InMobiAdapterServerParameters paramInMobiAdapterServerParameters, AdSize paramAdSize, MediationAdRequest paramMediationAdRequest, InMobiAdapterExtras paramInMobiAdapterExtras)
  {
    int i = 48;
    this.a = paramMediationBannerListener;
    int j;
    int k;
    if (paramAdSize.isSizeAppropriate(320, i))
    {
      j = 9;
      k = 320;
    }
    while (true)
    {
      float f = paramActivity.getResources().getDisplayMetrics().density;
      FrameLayout.LayoutParams localLayoutParams = new FrameLayout.LayoutParams((int)(f * k), (int)(f * i));
      this.d = new IMAdView(paramActivity, j, paramInMobiAdapterServerParameters.appId);
      this.d.setRefreshInterval(-1);
      this.d.setIMAdListener(new a((byte)0));
      this.e = new FrameLayout(paramActivity);
      this.e.setLayoutParams(localLayoutParams);
      this.e.addView(this.d);
      this.d.loadNewAd(a(paramMediationAdRequest, paramInMobiAdapterExtras));
      return;
      if (paramAdSize.isSizeAppropriate(468, 60))
      {
        j = 12;
        k = 468;
        i = 60;
      }
      else if (paramAdSize.isSizeAppropriate(728, 90))
      {
        j = 11;
        k = 728;
        i = 90;
      }
      else if (paramAdSize.isSizeAppropriate(300, 250))
      {
        j = 10;
        k = 300;
        i = 250;
      }
      else
      {
        if (!paramAdSize.isSizeAppropriate(120, 600))
          break;
        i = 600;
        j = 13;
        k = 120;
      }
    }
    paramMediationBannerListener.onFailedToReceiveAd(this, AdRequest.ErrorCode.INVALID_REQUEST);
  }

  public final void requestInterstitialAd(MediationInterstitialListener paramMediationInterstitialListener, Activity paramActivity, InMobiAdapterServerParameters paramInMobiAdapterServerParameters, MediationAdRequest paramMediationAdRequest, InMobiAdapterExtras paramInMobiAdapterExtras)
  {
    this.b = paramMediationInterstitialListener;
    this.c = new IMAdInterstitial(paramActivity, paramInMobiAdapterServerParameters.appId);
    this.c.setImAdInterstitialListener(new b((byte)0));
    this.c.loadNewAd(a(paramMediationAdRequest, paramInMobiAdapterExtras));
  }

  public final void showInterstitial()
  {
    this.c.show();
  }

  private final class a
    implements IMAdListener
  {
    private a()
    {
    }

    public final void onAdRequestCompleted(IMAdView paramIMAdView)
    {
      InMobiAdapter.a(InMobiAdapter.this).onReceivedAd(InMobiAdapter.this);
    }

    public final void onAdRequestFailed(IMAdView paramIMAdView, IMAdRequest.ErrorCode paramErrorCode)
    {
      switch (InMobiAdapter.1.b[paramErrorCode.ordinal()])
      {
      default:
        return;
      case 1:
        InMobiAdapter.a(InMobiAdapter.this).onFailedToReceiveAd(InMobiAdapter.this, AdRequest.ErrorCode.INTERNAL_ERROR);
        return;
      case 2:
        InMobiAdapter.a(InMobiAdapter.this).onFailedToReceiveAd(InMobiAdapter.this, AdRequest.ErrorCode.INVALID_REQUEST);
        return;
      case 3:
        InMobiAdapter.a(InMobiAdapter.this).onFailedToReceiveAd(InMobiAdapter.this, AdRequest.ErrorCode.NETWORK_ERROR);
        return;
      case 4:
      }
      InMobiAdapter.a(InMobiAdapter.this).onFailedToReceiveAd(InMobiAdapter.this, AdRequest.ErrorCode.NO_FILL);
    }

    public final void onDismissAdScreen(IMAdView paramIMAdView)
    {
      InMobiAdapter.a(InMobiAdapter.this).onDismissScreen(InMobiAdapter.this);
    }

    public final void onLeaveApplication(IMAdView paramIMAdView)
    {
      InMobiAdapter.a(InMobiAdapter.this).onClick(InMobiAdapter.this);
      InMobiAdapter.a(InMobiAdapter.this).onPresentScreen(InMobiAdapter.this);
      InMobiAdapter.a(InMobiAdapter.this).onLeaveApplication(InMobiAdapter.this);
    }

    public final void onShowAdScreen(IMAdView paramIMAdView)
    {
      InMobiAdapter.a(InMobiAdapter.this).onClick(InMobiAdapter.this);
      InMobiAdapter.a(InMobiAdapter.this).onPresentScreen(InMobiAdapter.this);
    }
  }

  private final class b
    implements IMAdInterstitialListener
  {
    private b()
    {
    }

    public final void onAdRequestFailed(IMAdInterstitial paramIMAdInterstitial, IMAdRequest.ErrorCode paramErrorCode)
    {
      switch (InMobiAdapter.1.b[paramErrorCode.ordinal()])
      {
      default:
        return;
      case 1:
        InMobiAdapter.b(InMobiAdapter.this).onFailedToReceiveAd(InMobiAdapter.this, AdRequest.ErrorCode.INTERNAL_ERROR);
        return;
      case 2:
        InMobiAdapter.b(InMobiAdapter.this).onFailedToReceiveAd(InMobiAdapter.this, AdRequest.ErrorCode.INVALID_REQUEST);
        return;
      case 3:
        InMobiAdapter.b(InMobiAdapter.this).onFailedToReceiveAd(InMobiAdapter.this, AdRequest.ErrorCode.NETWORK_ERROR);
        return;
      case 4:
      }
      InMobiAdapter.b(InMobiAdapter.this).onFailedToReceiveAd(InMobiAdapter.this, AdRequest.ErrorCode.NO_FILL);
    }

    public final void onAdRequestLoaded(IMAdInterstitial paramIMAdInterstitial)
    {
      InMobiAdapter.b(InMobiAdapter.this).onReceivedAd(InMobiAdapter.this);
    }

    public final void onDismissAdScreen(IMAdInterstitial paramIMAdInterstitial)
    {
      InMobiAdapter.b(InMobiAdapter.this).onDismissScreen(InMobiAdapter.this);
    }

    public final void onLeaveApplication(IMAdInterstitial paramIMAdInterstitial)
    {
      InMobiAdapter.b(InMobiAdapter.this).onPresentScreen(InMobiAdapter.this);
      InMobiAdapter.b(InMobiAdapter.this).onLeaveApplication(InMobiAdapter.this);
    }

    public final void onShowAdScreen(IMAdInterstitial paramIMAdInterstitial)
    {
      InMobiAdapter.b(InMobiAdapter.this).onPresentScreen(InMobiAdapter.this);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.ads.mediation.inmobi.InMobiAdapter
 * JD-Core Version:    0.6.2
 */