package com.google.ads.mediation.inmobi;

import com.google.ads.mediation.NetworkExtras;
import com.inmobi.androidsdk.IMAdRequest.EducationType;
import com.inmobi.androidsdk.IMAdRequest.EthnicityType;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public final class InMobiAdapterExtras
  implements NetworkExtras
{
  private String a = null;
  private Education b = null;
  private Ethnicity c = null;
  private Integer d = null;
  private Set<String> e = null;
  private String f = null;
  private String g = null;

  public final InMobiAdapterExtras addInterest(String paramString)
  {
    if (this.e == null)
      setInterests(null);
    this.e = new HashSet(this.e);
    return this;
  }

  public final InMobiAdapterExtras clearAreaCode()
  {
    return setAreaCode(null);
  }

  public final InMobiAdapterExtras clearEducation()
  {
    return setEducation(null);
  }

  public final InMobiAdapterExtras clearEthnicity()
  {
    return setEthnicity(null);
  }

  public final InMobiAdapterExtras clearIncome()
  {
    return setIncome(null);
  }

  public final InMobiAdapterExtras clearInterests()
  {
    return setInterests(null);
  }

  public final InMobiAdapterExtras clearPostalCode()
  {
    return setPostalCode(null);
  }

  public final InMobiAdapterExtras clearSearchString()
  {
    return setSearchString(null);
  }

  public final String getAreaCode()
  {
    return this.a;
  }

  public final Education getEducation()
  {
    return this.b;
  }

  public final Ethnicity getEthnicity()
  {
    return this.c;
  }

  public final Integer getIncome()
  {
    return this.d;
  }

  public final Set<String> getInterests()
  {
    if (this.e == null)
      return null;
    return Collections.unmodifiableSet(this.e);
  }

  public final String getPostalCode()
  {
    return this.f;
  }

  public final String getSearchString()
  {
    return this.g;
  }

  public final InMobiAdapterExtras setAreaCode(String paramString)
  {
    this.a = paramString;
    return this;
  }

  public final InMobiAdapterExtras setEducation(Education paramEducation)
  {
    this.b = paramEducation;
    return this;
  }

  public final InMobiAdapterExtras setEthnicity(Ethnicity paramEthnicity)
  {
    this.c = paramEthnicity;
    return this;
  }

  public final InMobiAdapterExtras setIncome(Integer paramInteger)
  {
    this.d = paramInteger;
    return this;
  }

  public final InMobiAdapterExtras setInterests(Collection<String> paramCollection)
  {
    if (paramCollection == null)
    {
      this.e = new HashSet();
      return this;
    }
    this.e = new HashSet(paramCollection);
    return this;
  }

  public final InMobiAdapterExtras setPostalCode(String paramString)
  {
    this.f = paramString;
    return this;
  }

  public final InMobiAdapterExtras setSearchString(String paramString)
  {
    this.g = paramString;
    return this;
  }

  public static enum Education
  {
    private final IMAdRequest.EducationType a;

    static
    {
      OTHER = new Education("OTHER", 3, IMAdRequest.EducationType.Edu_Other);
      BACHELORS_DEGREE = new Education("BACHELORS_DEGREE", 4, IMAdRequest.EducationType.Edu_BachelorsDegree);
      DOCTORAL_DEGREE = new Education("DOCTORAL_DEGREE", 5, IMAdRequest.EducationType.Edu_DoctoralDegree);
      MASTERS = new Education("MASTERS", 6, IMAdRequest.EducationType.Edu_MastersDegree);
      NONE = new Education("NONE", 7, IMAdRequest.EducationType.Edu_None);
      Education[] arrayOfEducation = new Education[8];
      arrayOfEducation[0] = HIGH_SCHOOL;
      arrayOfEducation[1] = IN_COLLEGE;
      arrayOfEducation[2] = SOME_COLLEGE;
      arrayOfEducation[3] = OTHER;
      arrayOfEducation[4] = BACHELORS_DEGREE;
      arrayOfEducation[5] = DOCTORAL_DEGREE;
      arrayOfEducation[6] = MASTERS;
      arrayOfEducation[7] = NONE;
    }

    private Education(IMAdRequest.EducationType paramEducationType)
    {
      this.a = paramEducationType;
    }

    public final IMAdRequest.EducationType getDescription()
    {
      return this.a;
    }
  }

  public static enum Ethnicity
  {
    private final IMAdRequest.EthnicityType a;

    static
    {
      Ethnicity[] arrayOfEthnicity = new Ethnicity[8];
      arrayOfEthnicity[0] = ASIAN;
      arrayOfEthnicity[1] = BLACK;
      arrayOfEthnicity[2] = HISPANIC;
      arrayOfEthnicity[3] = MIXED;
      arrayOfEthnicity[4] = NATIVE_AMERICAN;
      arrayOfEthnicity[5] = NONE;
      arrayOfEthnicity[6] = OTHER;
      arrayOfEthnicity[7] = WHITE;
    }

    private Ethnicity(IMAdRequest.EthnicityType paramEthnicityType)
    {
      this.a = paramEthnicityType;
    }

    public final IMAdRequest.EthnicityType getDescription()
    {
      return this.a;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.ads.mediation.inmobi.InMobiAdapterExtras
 * JD-Core Version:    0.6.2
 */