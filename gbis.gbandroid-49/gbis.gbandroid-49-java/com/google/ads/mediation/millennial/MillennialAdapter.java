package com.google.ads.mediation.millennial;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.AdRequest.Gender;
import com.google.ads.AdSize;
import com.google.ads.mediation.MediationAdRequest;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationBannerListener;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationInterstitialListener;
import com.millennialmedia.android.MMAdView;
import com.millennialmedia.android.MMAdView.MMAdListener;
import java.util.Hashtable;

public final class MillennialAdapter
  implements MediationBannerAdapter<MillennialAdapterExtras, MillennialAdapterServerParameters>, MediationInterstitialAdapter<MillennialAdapterExtras, MillennialAdapterServerParameters>
{
  private MediationBannerListener a;
  private MediationInterstitialListener b;
  private MMAdView c;
  private FrameLayout d;

  private static int a(int paramInt, Context paramContext)
  {
    return (int)TypedValue.applyDimension(1, paramInt, paramContext.getResources().getDisplayMetrics());
  }

  private void a(Hashtable<String, String> paramHashtable, MediationAdRequest paramMediationAdRequest, MillennialAdapterExtras paramMillennialAdapterExtras)
  {
    if (paramMillennialAdapterExtras == null)
      paramMillennialAdapterExtras = new MillennialAdapterExtras();
    if (paramMediationAdRequest.getKeywords() != null)
      paramHashtable.put("keywords", TextUtils.join(",", paramMediationAdRequest.getKeywords()));
    String str;
    if (paramMillennialAdapterExtras.getChildren() != null)
    {
      if (paramMillennialAdapterExtras.getChildren().booleanValue())
      {
        str = "true";
        paramHashtable.put("children", str);
      }
    }
    else
    {
      this.c.setId(1897808289);
      if (paramMillennialAdapterExtras.getAdLocation() != null);
      switch (1.a[paramMillennialAdapterExtras.getAdLocation().ordinal()])
      {
      default:
        label116: if (paramMillennialAdapterExtras.getInterstitialTime() != null);
        switch (1.b[paramMillennialAdapterExtras.getInterstitialTime().ordinal()])
        {
        default:
          label156: if (paramMediationAdRequest.getAgeInYears() != null)
            this.c.setAge(paramMediationAdRequest.getAgeInYears().toString());
          if (paramMediationAdRequest.getGender() != null)
            switch (1.c[paramMediationAdRequest.getGender().ordinal()])
            {
            default:
            case 1:
            case 2:
            }
          break;
        case 1:
        case 2:
        }
        break;
      case 1:
      case 2:
      }
    }
    while (true)
    {
      if (paramMillennialAdapterExtras.getIncomeInUsDollars() != null)
        this.c.setIncome(paramMillennialAdapterExtras.getIncomeInUsDollars().toString());
      if (paramMediationAdRequest.getLocation() != null)
        this.c.updateUserLocation(paramMediationAdRequest.getLocation());
      if (paramMillennialAdapterExtras.getPostalCode() != null)
        this.c.setZip(paramMillennialAdapterExtras.getPostalCode());
      if (paramMillennialAdapterExtras.getMaritalStatus() != null)
        this.c.setMarital(paramMillennialAdapterExtras.getMaritalStatus().getDescription());
      if (paramMillennialAdapterExtras.getEthnicity() != null)
        this.c.setEthnicity(paramMillennialAdapterExtras.getEthnicity().getDescription());
      if (paramMillennialAdapterExtras.getOrientation() != null)
        this.c.setOrientation(paramMillennialAdapterExtras.getOrientation().getDescription());
      if (paramMillennialAdapterExtras.getPolitics() != null)
        this.c.setPolitics(paramMillennialAdapterExtras.getPolitics().getDescription());
      if (paramMillennialAdapterExtras.getEducation() != null)
        this.c.setEducation(paramMillennialAdapterExtras.getEducation().getDescription());
      return;
      str = "false";
      break;
      this.c.setAdType("MMBannerAdBottom");
      break label116;
      this.c.setAdType("MMBannerAdTop");
      break label116;
      this.c.setAdType("MMFullScreenAdLaunch");
      break label156;
      this.c.setAdType("MMFullScreenAdTransition");
      break label156;
      this.c.setGender("male");
      continue;
      this.c.setGender("female");
    }
  }

  public final void destroy()
  {
  }

  public final Class<MillennialAdapterExtras> getAdditionalParametersType()
  {
    return MillennialAdapterExtras.class;
  }

  public final View getBannerView()
  {
    return this.d;
  }

  public final Class<MillennialAdapterServerParameters> getServerParametersType()
  {
    return MillennialAdapterServerParameters.class;
  }

  public final void requestBannerAd(MediationBannerListener paramMediationBannerListener, Activity paramActivity, MillennialAdapterServerParameters paramMillennialAdapterServerParameters, AdSize paramAdSize, MediationAdRequest paramMediationAdRequest, MillennialAdapterExtras paramMillennialAdapterExtras)
  {
    this.a = paramMediationBannerListener;
    Hashtable localHashtable = new Hashtable();
    if (paramAdSize.isSizeAppropriate(320, 53))
    {
      localHashtable.put("width", "320");
      localHashtable.put("height", "53");
    }
    for (FrameLayout.LayoutParams localLayoutParams = new FrameLayout.LayoutParams(a(320, paramActivity), a(53, paramActivity)); ; localLayoutParams = new FrameLayout.LayoutParams(a(paramAdSize.getWidth(), paramActivity), a(paramAdSize.getHeight(), paramActivity)))
    {
      this.c = new MMAdView(paramActivity, paramMillennialAdapterServerParameters.apid, "MMBannerAdTop", -1, localHashtable, paramMediationAdRequest.isTesting());
      a(localHashtable, paramMediationAdRequest, paramMillennialAdapterExtras);
      this.c.setListener(new a((byte)0));
      this.d = new FrameLayout(paramActivity);
      this.d.setLayoutParams(localLayoutParams);
      this.d.addView(this.c);
      this.c.callForAd();
      return;
      localHashtable.put("width", Integer.toString(paramAdSize.getWidth()));
      localHashtable.put("height", Integer.toString(paramAdSize.getHeight()));
    }
  }

  public final void requestInterstitialAd(MediationInterstitialListener paramMediationInterstitialListener, Activity paramActivity, MillennialAdapterServerParameters paramMillennialAdapterServerParameters, MediationAdRequest paramMediationAdRequest, MillennialAdapterExtras paramMillennialAdapterExtras)
  {
    this.b = paramMediationInterstitialListener;
    Hashtable localHashtable = new Hashtable();
    this.c = new MMAdView(paramActivity, paramMillennialAdapterServerParameters.apid, "MMFullScreenAdTransition", -1, localHashtable, paramMediationAdRequest.isTesting());
    a(localHashtable, paramMediationAdRequest, paramMillennialAdapterExtras);
    this.c.setListener(new b((byte)0));
    this.c.fetch();
  }

  public final void showInterstitial()
  {
    this.c.display();
  }

  private final class a
    implements MMAdView.MMAdListener
  {
    private a()
    {
    }

    public final void MMAdCachingCompleted(MMAdView paramMMAdView, boolean paramBoolean)
    {
    }

    public final void MMAdClickedToNewBrowser(MMAdView paramMMAdView)
    {
      MillennialAdapter.a(MillennialAdapter.this).onClick(MillennialAdapter.this);
      MillennialAdapter.a(MillennialAdapter.this).onPresentScreen(MillennialAdapter.this);
      MillennialAdapter.a(MillennialAdapter.this).onLeaveApplication(MillennialAdapter.this);
    }

    public final void MMAdClickedToOverlay(MMAdView paramMMAdView)
    {
      MillennialAdapter.a(MillennialAdapter.this).onClick(MillennialAdapter.this);
      MillennialAdapter.a(MillennialAdapter.this).onPresentScreen(MillennialAdapter.this);
    }

    public final void MMAdFailed(MMAdView paramMMAdView)
    {
      MillennialAdapter.a(MillennialAdapter.this).onFailedToReceiveAd(MillennialAdapter.this, AdRequest.ErrorCode.NO_FILL);
    }

    public final void MMAdOverlayLaunched(MMAdView paramMMAdView)
    {
      MillennialAdapter.a(MillennialAdapter.this).onPresentScreen(MillennialAdapter.this);
    }

    public final void MMAdRequestIsCaching(MMAdView paramMMAdView)
    {
    }

    public final void MMAdReturned(MMAdView paramMMAdView)
    {
      MillennialAdapter.a(MillennialAdapter.this).onReceivedAd(MillennialAdapter.this);
    }
  }

  private final class b
    implements MMAdView.MMAdListener
  {
    private b()
    {
    }

    public final void MMAdCachingCompleted(MMAdView paramMMAdView, boolean paramBoolean)
    {
      if (!paramBoolean)
      {
        MillennialAdapter.b(MillennialAdapter.this).onFailedToReceiveAd(MillennialAdapter.this, AdRequest.ErrorCode.NETWORK_ERROR);
        return;
      }
      MillennialAdapter.b(MillennialAdapter.this).onReceivedAd(MillennialAdapter.this);
    }

    public final void MMAdClickedToNewBrowser(MMAdView paramMMAdView)
    {
      MillennialAdapter.b(MillennialAdapter.this).onPresentScreen(MillennialAdapter.this);
      MillennialAdapter.b(MillennialAdapter.this).onLeaveApplication(MillennialAdapter.this);
    }

    public final void MMAdClickedToOverlay(MMAdView paramMMAdView)
    {
      MillennialAdapter.b(MillennialAdapter.this).onPresentScreen(MillennialAdapter.this);
    }

    public final void MMAdFailed(MMAdView paramMMAdView)
    {
      MillennialAdapter.b(MillennialAdapter.this).onFailedToReceiveAd(MillennialAdapter.this, AdRequest.ErrorCode.NO_FILL);
    }

    public final void MMAdOverlayLaunched(MMAdView paramMMAdView)
    {
      MillennialAdapter.b(MillennialAdapter.this).onPresentScreen(MillennialAdapter.this);
    }

    public final void MMAdRequestIsCaching(MMAdView paramMMAdView)
    {
    }

    public final void MMAdReturned(MMAdView paramMMAdView)
    {
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.ads.mediation.millennial.MillennialAdapter
 * JD-Core Version:    0.6.2
 */