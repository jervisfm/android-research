package com.google.ads.mediation.millennial;

import com.google.ads.mediation.NetworkExtras;

public final class MillennialAdapterExtras
  implements NetworkExtras
{
  private AdLocation a = AdLocation.UNKNOWN;
  private InterstitialTime b = InterstitialTime.UNKNOWN;
  private Integer c = null;
  private MaritalStatus d = null;
  private Ethnicity e = null;
  private Orientation f = null;
  private Politics g = null;
  private Education h = null;
  private Boolean i = null;
  private String j = null;

  public final MillennialAdapterExtras clearAdLocation()
  {
    return setAdLocation(null);
  }

  public final MillennialAdapterExtras clearChildren()
  {
    return setChildren(null);
  }

  public final MillennialAdapterExtras clearEducation()
  {
    return setEducation(null);
  }

  public final MillennialAdapterExtras clearEthnicity()
  {
    return setEthnicity(null);
  }

  public final MillennialAdapterExtras clearIncomeInUsDollars()
  {
    return setIncomeInUsDollars(null);
  }

  public final MillennialAdapterExtras clearInterstitialTime()
  {
    return setInterstitialTime(null);
  }

  public final MillennialAdapterExtras clearMaritalStatus()
  {
    return setMaritalStatus(null);
  }

  public final MillennialAdapterExtras clearOrientation()
  {
    return setOrientation(null);
  }

  public final MillennialAdapterExtras clearPolitics()
  {
    return setPolitics(null);
  }

  public final MillennialAdapterExtras clearPostalCode()
  {
    return setPostalCode(null);
  }

  public final AdLocation getAdLocation()
  {
    return this.a;
  }

  public final Boolean getChildren()
  {
    return this.i;
  }

  public final Education getEducation()
  {
    return this.h;
  }

  public final Ethnicity getEthnicity()
  {
    return this.e;
  }

  public final Integer getIncomeInUsDollars()
  {
    return this.c;
  }

  public final InterstitialTime getInterstitialTime()
  {
    return this.b;
  }

  public final MaritalStatus getMaritalStatus()
  {
    return this.d;
  }

  public final Orientation getOrientation()
  {
    return this.f;
  }

  public final Politics getPolitics()
  {
    return this.g;
  }

  public final String getPostalCode()
  {
    return this.j;
  }

  public final MillennialAdapterExtras setAdLocation(AdLocation paramAdLocation)
  {
    this.a = paramAdLocation;
    return this;
  }

  public final MillennialAdapterExtras setChildren(Boolean paramBoolean)
  {
    this.i = paramBoolean;
    return this;
  }

  public final MillennialAdapterExtras setEducation(Education paramEducation)
  {
    this.h = paramEducation;
    return this;
  }

  public final MillennialAdapterExtras setEthnicity(Ethnicity paramEthnicity)
  {
    this.e = paramEthnicity;
    return this;
  }

  public final MillennialAdapterExtras setIncomeInUsDollars(Integer paramInteger)
  {
    this.c = paramInteger;
    return this;
  }

  public final MillennialAdapterExtras setInterstitialTime(InterstitialTime paramInterstitialTime)
  {
    this.b = paramInterstitialTime;
    return this;
  }

  public final MillennialAdapterExtras setMaritalStatus(MaritalStatus paramMaritalStatus)
  {
    this.d = paramMaritalStatus;
    return this;
  }

  public final MillennialAdapterExtras setOrientation(Orientation paramOrientation)
  {
    this.f = paramOrientation;
    return this;
  }

  public final MillennialAdapterExtras setPolitics(Politics paramPolitics)
  {
    this.g = paramPolitics;
    return this;
  }

  public final MillennialAdapterExtras setPostalCode(String paramString)
  {
    this.j = paramString;
    return this;
  }

  public static enum AdLocation
  {
    static
    {
      BOTTOM = new AdLocation("BOTTOM", 1);
      TOP = new AdLocation("TOP", 2);
      AdLocation[] arrayOfAdLocation = new AdLocation[3];
      arrayOfAdLocation[0] = UNKNOWN;
      arrayOfAdLocation[1] = BOTTOM;
      arrayOfAdLocation[2] = TOP;
    }
  }

  public static enum Education
  {
    private final String a;

    static
    {
      ASSOCIATE = new Education("ASSOCIATE", 3, "associate");
      BACHELORS = new Education("BACHELORS", 4, "bachelors");
      MASTERS = new Education("MASTERS", 5, "masters");
      PHD = new Education("PHD", 6, "phd");
      PROFESSIONAL = new Education("PROFESSIONAL", 7, "professional");
      Education[] arrayOfEducation = new Education[8];
      arrayOfEducation[0] = HIGH_SCHOOL;
      arrayOfEducation[1] = IN_COLLEGE;
      arrayOfEducation[2] = SOME_COLLEGE;
      arrayOfEducation[3] = ASSOCIATE;
      arrayOfEducation[4] = BACHELORS;
      arrayOfEducation[5] = MASTERS;
      arrayOfEducation[6] = PHD;
      arrayOfEducation[7] = PROFESSIONAL;
    }

    private Education(String paramString)
    {
      this.a = paramString;
    }

    public final String getDescription()
    {
      return this.a;
    }
  }

  public static enum Ethnicity
  {
    private final String a;

    static
    {
      AFRICAN_AMERICAN = new Ethnicity("AFRICAN_AMERICAN", 1, "africanamerican");
      ASIAN = new Ethnicity("ASIAN", 2, "asian");
      INDIAN = new Ethnicity("INDIAN", 3, "indian");
      MIDDLE_EASTERN = new Ethnicity("MIDDLE_EASTERN", 4, "middleeastern");
      NATIVE_AMERICAN = new Ethnicity("NATIVE_AMERICAN", 5, "nativeamerican");
      PACIFIC_ISLANDER = new Ethnicity("PACIFIC_ISLANDER", 6, "pacificislander");
      WHITE = new Ethnicity("WHITE", 7, "white");
      OTHER = new Ethnicity("OTHER", 8, "other");
      Ethnicity[] arrayOfEthnicity = new Ethnicity[9];
      arrayOfEthnicity[0] = HISPANIC;
      arrayOfEthnicity[1] = AFRICAN_AMERICAN;
      arrayOfEthnicity[2] = ASIAN;
      arrayOfEthnicity[3] = INDIAN;
      arrayOfEthnicity[4] = MIDDLE_EASTERN;
      arrayOfEthnicity[5] = NATIVE_AMERICAN;
      arrayOfEthnicity[6] = PACIFIC_ISLANDER;
      arrayOfEthnicity[7] = WHITE;
      arrayOfEthnicity[8] = OTHER;
    }

    private Ethnicity(String paramString)
    {
      this.a = paramString;
    }

    public final String getDescription()
    {
      return this.a;
    }
  }

  public static enum InterstitialTime
  {
    static
    {
      APP_LAUNCH = new InterstitialTime("APP_LAUNCH", 1);
      TRANSITION = new InterstitialTime("TRANSITION", 2);
      InterstitialTime[] arrayOfInterstitialTime = new InterstitialTime[3];
      arrayOfInterstitialTime[0] = UNKNOWN;
      arrayOfInterstitialTime[1] = APP_LAUNCH;
      arrayOfInterstitialTime[2] = TRANSITION;
    }
  }

  public static enum MaritalStatus
  {
    private final String a;

    static
    {
      DIVORCED = new MaritalStatus("DIVORCED", 1, "divorced");
      ENGAGED = new MaritalStatus("ENGAGED", 2, "engaged");
      RELATIONSHIP = new MaritalStatus("RELATIONSHIP", 3, "relationship");
      SWINGER = new MaritalStatus("SWINGER", 4, "swinger");
      MaritalStatus[] arrayOfMaritalStatus = new MaritalStatus[5];
      arrayOfMaritalStatus[0] = SINGLE;
      arrayOfMaritalStatus[1] = DIVORCED;
      arrayOfMaritalStatus[2] = ENGAGED;
      arrayOfMaritalStatus[3] = RELATIONSHIP;
      arrayOfMaritalStatus[4] = SWINGER;
    }

    private MaritalStatus(String paramString)
    {
      this.a = paramString;
    }

    public final String getDescription()
    {
      return this.a;
    }
  }

  public static enum Orientation
  {
    private final String a;

    static
    {
      GAY = new Orientation("GAY", 1, "gay");
      BISEXUAL = new Orientation("BISEXUAL", 2, "bisexual");
      NOT_SURE = new Orientation("NOT_SURE", 3, "notsure");
      Orientation[] arrayOfOrientation = new Orientation[4];
      arrayOfOrientation[0] = STRAIGHT;
      arrayOfOrientation[1] = GAY;
      arrayOfOrientation[2] = BISEXUAL;
      arrayOfOrientation[3] = NOT_SURE;
    }

    private Orientation(String paramString)
    {
      this.a = paramString;
    }

    public final String getDescription()
    {
      return this.a;
    }
  }

  public static enum Politics
  {
    private final String a;

    static
    {
      DEMOCRAT = new Politics("DEMOCRAT", 1, "democrat");
      CONSERVATIVE = new Politics("CONSERVATIVE", 2, "conservative");
      MODERATE = new Politics("MODERATE", 3, "moderate");
      LIBERAL = new Politics("LIBERAL", 4, "liberal");
      INDEPENDENT = new Politics("INDEPENDENT", 5, "independent");
      OTHER = new Politics("OTHER", 6, "other");
      UNKNOWN = new Politics("UNKNOWN", 7, "unknown");
      Politics[] arrayOfPolitics = new Politics[8];
      arrayOfPolitics[0] = REPUBLICAN;
      arrayOfPolitics[1] = DEMOCRAT;
      arrayOfPolitics[2] = CONSERVATIVE;
      arrayOfPolitics[3] = MODERATE;
      arrayOfPolitics[4] = LIBERAL;
      arrayOfPolitics[5] = INDEPENDENT;
      arrayOfPolitics[6] = OTHER;
      arrayOfPolitics[7] = UNKNOWN;
    }

    private Politics(String paramString)
    {
      this.a = paramString;
    }

    public final String getDescription()
    {
      return this.a;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.ads.mediation.millennial.MillennialAdapterExtras
 * JD-Core Version:    0.6.2
 */