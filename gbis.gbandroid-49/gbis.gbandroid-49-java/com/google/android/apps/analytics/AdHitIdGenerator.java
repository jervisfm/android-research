package com.google.android.apps.analytics;

public class AdHitIdGenerator
{
  private boolean a;

  public AdHitIdGenerator()
  {
    try
    {
      if (Class.forName("com.google.ads.AdRequest") != null);
      for (boolean bool = true; ; bool = false)
      {
        this.a = bool;
        return;
      }
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      this.a = false;
    }
  }

  final int a()
  {
    if (!this.a)
      return 0;
    return AdMobInfo.getInstance().generateAdHitId();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.android.apps.analytics.AdHitIdGenerator
 * JD-Core Version:    0.6.2
 */