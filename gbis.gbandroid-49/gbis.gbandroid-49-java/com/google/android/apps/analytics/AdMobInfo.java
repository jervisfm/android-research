package com.google.android.apps.analytics;

import java.util.Random;

public class AdMobInfo
{
  private static final AdMobInfo a = new AdMobInfo();
  private int b;
  private Random c = new Random();

  public static AdMobInfo getInstance()
  {
    return a;
  }

  public int generateAdHitId()
  {
    this.b = this.c.nextInt();
    return this.b;
  }

  public int getAdHitId()
  {
    return this.b;
  }

  public String getJoinId()
  {
    if (this.b == 0);
    String str1;
    String str2;
    do
    {
      return null;
      GoogleAnalyticsTracker localGoogleAnalyticsTracker = GoogleAnalyticsTracker.getInstance();
      str1 = localGoogleAnalyticsTracker.b();
      str2 = localGoogleAnalyticsTracker.c();
    }
    while ((str1 == null) || (str2 == null));
    Object[] arrayOfObject = new Object[3];
    arrayOfObject[0] = str1;
    arrayOfObject[1] = str2;
    arrayOfObject[2] = Integer.valueOf(this.b);
    return String.format("A,%s,%s,%d", arrayOfObject);
  }

  public void setAdHitId(int paramInt)
  {
    this.b = paramInt;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.android.apps.analytics.AdMobInfo
 * JD-Core Version:    0.6.2
 */