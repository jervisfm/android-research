package com.google.android.apps.analytics;

abstract interface Dispatcher
{
  public abstract void a();

  public abstract void a(Callbacks paramCallbacks);

  public abstract void a(boolean paramBoolean);

  public abstract void a(Hit[] paramArrayOfHit);

  public static abstract interface Callbacks
  {
    public abstract void dispatchFinished();

    public abstract void hitDispatched(long paramLong);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.android.apps.analytics.Dispatcher
 * JD-Core Version:    0.6.2
 */