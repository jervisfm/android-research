package com.google.android.apps.analytics;

import android.content.Context;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class GoogleAnalyticsTracker
{
  public static final String LOG_TAG = "GoogleAnalyticsTracker";
  public static final String PRODUCT = "GoogleAnalytics";
  public static final String VERSION = "1.4.2";
  public static final String WIRE_VERSION = "4.8.1ma";
  private static GoogleAnalyticsTracker a = new GoogleAnalyticsTracker();
  private boolean b = false;
  private boolean c = false;
  private boolean d = false;
  private boolean e = false;
  private int f = 100;
  private String g;
  private Context h;
  private ConnectivityManager i;
  private String j = "GoogleAnalytics";
  private String k = "1.4.2";
  private int l;
  private g m;
  private Dispatcher n;
  private boolean o;
  private boolean p;
  private AdHitIdGenerator q;
  private b r;
  private Map<String, Transaction> s = new HashMap();
  private Map<String, Map<String, Item>> t = new HashMap();
  private Handler u;
  private Runnable v = new d(this);

  private void a(String paramString, int paramInt, Context paramContext)
  {
    if (paramContext == null)
      throw new NullPointerException("Context cannot be null");
    Object localObject1;
    Object localObject2;
    if (this.m == null)
    {
      localObject1 = new i(paramContext);
      ((g)localObject1).a(this.d);
      ((g)localObject1).b(this.f);
      if (this.n != null)
        break label110;
      localObject2 = new h(this.j, this.k);
      ((Dispatcher)localObject2).a(this.c);
    }
    while (true)
    {
      a(paramString, paramInt, paramContext, (g)localObject1, (Dispatcher)localObject2, true);
      return;
      localObject1 = this.m;
      break;
      label110: localObject2 = this.n;
    }
  }

  private void a(String paramString, int paramInt, Context paramContext, g paramg, Dispatcher paramDispatcher, boolean paramBoolean)
  {
    a(paramString, paramInt, paramContext, paramg, paramDispatcher, true, new a());
  }

  private void a(String paramString, int paramInt, Context paramContext, g paramg, Dispatcher paramDispatcher, boolean paramBoolean, Dispatcher.Callbacks paramCallbacks)
  {
    this.g = paramString;
    if (paramContext == null)
      throw new NullPointerException("Context cannot be null");
    this.h = paramContext.getApplicationContext();
    this.m = paramg;
    this.q = new AdHitIdGenerator();
    if (paramBoolean)
      this.m.e();
    this.n = paramDispatcher;
    this.n.a(paramCallbacks);
    this.p = false;
    if (this.i == null)
      this.i = ((ConnectivityManager)this.h.getSystemService("connectivity"));
    if (this.u == null)
      this.u = new Handler(paramContext.getMainLooper());
    while (true)
    {
      setDispatchPeriod(paramInt);
      return;
      e();
    }
  }

  private void a(String paramString1, String paramString2, String paramString3, String paramString4, int paramInt)
  {
    c localc = new c(paramString1, paramString2, paramString3, paramString4, paramInt, this.h.getResources().getDisplayMetrics().widthPixels, this.h.getResources().getDisplayMetrics().heightPixels);
    localc.a(this.r);
    localc.b(this.q.a());
    localc.b(this.e);
    this.r = new b();
    this.m.a(localc);
    f();
  }

  private void d()
  {
    if (this.l < 0);
    while ((!this.u.postDelayed(this.v, 1000 * this.l)) || (!this.b))
      return;
    Log.v("GoogleAnalyticsTracker", "Scheduled next dispatch");
  }

  private void e()
  {
    if (this.u != null)
      this.u.removeCallbacks(this.v);
  }

  private void f()
  {
    if (this.o)
    {
      this.o = false;
      d();
    }
  }

  public static GoogleAnalyticsTracker getInstance()
  {
    return a;
  }

  final void a()
  {
    this.p = false;
  }

  public void addItem(Item paramItem)
  {
    if ((Transaction)this.s.get(paramItem.a()) == null)
    {
      Log.i("GoogleAnalyticsTracker", "No transaction with orderId " + paramItem.a() + " found, creating one");
      Transaction localTransaction = new Transaction.Builder(paramItem.a(), 0.0D).build();
      this.s.put(paramItem.a(), localTransaction);
    }
    Object localObject = (Map)this.t.get(paramItem.a());
    if (localObject == null)
    {
      localObject = new HashMap();
      this.t.put(paramItem.a(), localObject);
    }
    ((Map)localObject).put(paramItem.b(), paramItem);
  }

  public void addTransaction(Transaction paramTransaction)
  {
    this.s.put(paramTransaction.a(), paramTransaction);
  }

  final String b()
  {
    if (this.m == null)
      return null;
    return this.m.c();
  }

  final String c()
  {
    if (this.m == null)
      return null;
    return this.m.d();
  }

  public void clearTransactions()
  {
    this.s.clear();
    this.t.clear();
  }

  public boolean dispatch()
  {
    if (this.b)
      Log.v("GoogleAnalyticsTracker", "Called dispatch");
    if (this.p)
    {
      if (this.b)
        Log.v("GoogleAnalyticsTracker", "...but dispatcher was busy");
      d();
    }
    do
    {
      return false;
      NetworkInfo localNetworkInfo = this.i.getActiveNetworkInfo();
      if ((localNetworkInfo == null) || (!localNetworkInfo.isAvailable()))
      {
        if (this.b)
          Log.v("GoogleAnalyticsTracker", "...but there was no network available");
        d();
        return false;
      }
      if (this.m.b() != 0)
      {
        Hit[] arrayOfHit = this.m.a();
        this.n.a(arrayOfHit);
        this.p = true;
        d();
        if (this.b)
          Log.v("GoogleAnalyticsTracker", "Sending " + arrayOfHit.length + " hits to dispatcher");
        return true;
      }
      this.o = true;
    }
    while (!this.b);
    Log.v("GoogleAnalyticsTracker", "...but there was nothing to dispatch");
    return false;
  }

  public boolean getAnonymizeIp()
  {
    return this.d;
  }

  public boolean getDebug()
  {
    return this.b;
  }

  public int getSampleRate()
  {
    return this.f;
  }

  public String getVisitorCustomVar(int paramInt)
  {
    if ((paramInt <= 0) || (paramInt > 5))
      throw new IllegalArgumentException("Index must be between 1 and 5 inclusive.");
    return this.m.a(paramInt);
  }

  public boolean isDryRun()
  {
    return this.c;
  }

  public void setAnonymizeIp(boolean paramBoolean)
  {
    this.d = paramBoolean;
    if (this.m != null)
      this.m.a(this.d);
  }

  public boolean setCustomVar(int paramInt, String paramString1, String paramString2)
  {
    return setCustomVar(paramInt, paramString1, paramString2, 3);
  }

  public boolean setCustomVar(int paramInt1, String paramString1, String paramString2, int paramInt2)
  {
    try
    {
      a locala = new a(paramInt1, paramString1, paramString2, paramInt2);
      if (this.r == null)
        this.r = new b();
      this.r.a(locala);
      return true;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
    }
    return false;
  }

  public void setDebug(boolean paramBoolean)
  {
    this.b = paramBoolean;
  }

  public void setDispatchPeriod(int paramInt)
  {
    int i1 = this.l;
    this.l = paramInt;
    if (i1 <= 0)
      d();
    while (i1 <= 0)
      return;
    e();
    d();
  }

  public boolean setDispatcher(Dispatcher paramDispatcher)
  {
    if (this.p)
      return false;
    if (this.n != null)
      this.n.a();
    this.n = paramDispatcher;
    this.n.a(new a());
    this.n.a(this.c);
    return true;
  }

  public void setDryRun(boolean paramBoolean)
  {
    this.c = paramBoolean;
    if (this.n != null)
      this.n.a(paramBoolean);
  }

  public void setProductVersion(String paramString1, String paramString2)
  {
    this.j = paramString1;
    this.k = paramString2;
  }

  public boolean setReferrer(String paramString)
  {
    if (this.m == null)
      throw new IllegalStateException("Can't set a referrer before starting the tracker");
    return this.m.a(paramString);
  }

  public void setSampleRate(int paramInt)
  {
    if ((paramInt < 0) || (paramInt > 100))
      Log.w("GoogleAnalyticsTracker", "Invalid sample rate: " + paramInt + " (should be between 0 and 100");
    do
    {
      return;
      this.f = paramInt;
    }
    while (this.m == null);
    this.m.b(this.f);
  }

  public void setUseServerTime(boolean paramBoolean)
  {
    this.e = paramBoolean;
  }

  @Deprecated
  public void start(String paramString, int paramInt, Context paramContext)
  {
    startNewSession(paramString, paramInt, paramContext);
  }

  @Deprecated
  public void start(String paramString, Context paramContext)
  {
    start(paramString, -1, paramContext);
  }

  public void startNewSession(String paramString, int paramInt, Context paramContext)
  {
    a(paramString, paramInt, paramContext);
  }

  public void startNewSession(String paramString, Context paramContext)
  {
    startNewSession(paramString, -1, paramContext);
  }

  @Deprecated
  public void stop()
  {
    if (this.n != null)
      this.n.a();
    e();
  }

  public void stopSession()
  {
    stop();
  }

  public void trackEvent(String paramString1, String paramString2, String paramString3, int paramInt)
  {
    if (paramString1 == null)
      throw new IllegalArgumentException("category cannot be null");
    if (paramString2 == null)
      throw new IllegalArgumentException("action cannot be null");
    a(this.g, paramString1, paramString2, paramString3, paramInt);
  }

  public void trackPageView(String paramString)
  {
    a(this.g, "__##GOOGLEPAGEVIEW##__", paramString, null, -1);
  }

  public void trackTransactions()
  {
    Iterator localIterator1 = this.s.values().iterator();
    while (localIterator1.hasNext())
    {
      Transaction localTransaction = (Transaction)localIterator1.next();
      c localc1 = new c(this.g, "__##GOOGLETRANSACTION##__", "", "", 0, this.h.getResources().getDisplayMetrics().widthPixels, this.h.getResources().getDisplayMetrics().heightPixels);
      localc1.a(localTransaction);
      this.m.a(localc1);
      Map localMap = (Map)this.t.get(localTransaction.a());
      if (localMap != null)
      {
        Iterator localIterator2 = localMap.values().iterator();
        while (localIterator2.hasNext())
        {
          Item localItem = (Item)localIterator2.next();
          c localc2 = new c(this.g, "__##GOOGLEITEM##__", "", "", 0, this.h.getResources().getDisplayMetrics().widthPixels, this.h.getResources().getDisplayMetrics().heightPixels);
          localc2.a(localItem);
          this.m.a(localc2);
        }
      }
    }
    clearTransactions();
    f();
  }

  final class a
    implements Dispatcher.Callbacks
  {
    a()
    {
    }

    public final void dispatchFinished()
    {
      GoogleAnalyticsTracker.a(GoogleAnalyticsTracker.this).post(new e(this));
    }

    public final void hitDispatched(long paramLong)
    {
      GoogleAnalyticsTracker.b(GoogleAnalyticsTracker.this).a(paramLong);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.android.apps.analytics.GoogleAnalyticsTracker
 * JD-Core Version:    0.6.2
 */