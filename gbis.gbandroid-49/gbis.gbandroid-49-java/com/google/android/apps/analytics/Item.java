package com.google.android.apps.analytics;

public class Item
{
  private final String a;
  private final String b;
  private final String c;
  private final String d;
  private final double e;
  private final long f;

  private Item(Builder paramBuilder)
  {
    this.a = Builder.a(paramBuilder);
    this.b = Builder.b(paramBuilder);
    this.e = Builder.c(paramBuilder);
    this.f = Builder.d(paramBuilder);
    this.c = Builder.e(paramBuilder);
    this.d = Builder.f(paramBuilder);
  }

  final String a()
  {
    return this.a;
  }

  final String b()
  {
    return this.b;
  }

  final String c()
  {
    return this.c;
  }

  final String d()
  {
    return this.d;
  }

  final double e()
  {
    return this.e;
  }

  final long f()
  {
    return this.f;
  }

  public static class Builder
  {
    private final String a;
    private final String b;
    private final double c;
    private final long d;
    private String e = null;
    private String f = null;

    public Builder(String paramString1, String paramString2, double paramDouble, long paramLong)
    {
      if ((paramString1 == null) || (paramString1.trim().length() == 0))
        throw new IllegalArgumentException("orderId must not be empty or null");
      if ((paramString2 == null) || (paramString2.trim().length() == 0))
        throw new IllegalArgumentException("itemSKU must not be empty or null");
      this.a = paramString1;
      this.b = paramString2;
      this.c = paramDouble;
      this.d = paramLong;
    }

    public Item build()
    {
      return new Item(this, (byte)0);
    }

    public Builder setItemCategory(String paramString)
    {
      this.f = paramString;
      return this;
    }

    public Builder setItemName(String paramString)
    {
      this.e = paramString;
      return this;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.android.apps.analytics.Item
 * JD-Core Version:    0.6.2
 */