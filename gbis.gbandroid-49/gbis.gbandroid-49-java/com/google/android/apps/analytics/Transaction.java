package com.google.android.apps.analytics;

public class Transaction
{
  private final String a;
  private final String b;
  private final double c;
  private final double d;
  private final double e;

  private Transaction(Builder paramBuilder)
  {
    this.a = Builder.a(paramBuilder);
    this.c = Builder.b(paramBuilder);
    this.b = Builder.c(paramBuilder);
    this.d = Builder.d(paramBuilder);
    this.e = Builder.e(paramBuilder);
  }

  final String a()
  {
    return this.a;
  }

  final String b()
  {
    return this.b;
  }

  final double c()
  {
    return this.c;
  }

  final double d()
  {
    return this.d;
  }

  final double e()
  {
    return this.e;
  }

  public static class Builder
  {
    private final String a;
    private String b = null;
    private final double c;
    private double d = 0.0D;
    private double e = 0.0D;

    public Builder(String paramString, double paramDouble)
    {
      if ((paramString == null) || (paramString.trim().length() == 0))
        throw new IllegalArgumentException("orderId must not be empty or null");
      this.a = paramString;
      this.c = paramDouble;
    }

    public Transaction build()
    {
      return new Transaction(this, (byte)0);
    }

    public Builder setShippingCost(double paramDouble)
    {
      this.e = paramDouble;
      return this;
    }

    public Builder setStoreName(String paramString)
    {
      this.b = paramString;
      return this;
    }

    public Builder setTotalTax(double paramDouble)
    {
      this.d = paramDouble;
      return this;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.android.apps.analytics.Transaction
 * JD-Core Version:    0.6.2
 */