package com.google.android.apps.analytics;

final class a
{
  private final int a;
  private final String b;
  private final String c;
  private final int d;

  public a(int paramInt1, String paramString1, String paramString2, int paramInt2)
  {
    if ((paramInt2 != 1) && (paramInt2 != 2) && (paramInt2 != 3))
      throw new IllegalArgumentException("Invalid Scope:" + paramInt2);
    if ((paramInt1 <= 0) || (paramInt1 > 5))
      throw new IllegalArgumentException("Index must be between 1 and 5 inclusive.");
    if ((paramString1 == null) || (paramString1.length() == 0))
      throw new IllegalArgumentException("Invalid argument for name:  Cannot be empty");
    if ((paramString2 == null) || (paramString2.length() == 0))
      throw new IllegalArgumentException("Invalid argument for value:  Cannot be empty");
    int i = AnalyticsParameterEncoder.encode(paramString1 + paramString2).length();
    if (i > 64)
      throw new IllegalArgumentException("Encoded form of name and value must not exceed 64 characters combined.  Character length: " + i);
    this.d = paramInt1;
    this.a = paramInt2;
    this.b = paramString1;
    this.c = paramString2;
  }

  public final int a()
  {
    return this.a;
  }

  public final String b()
  {
    return this.b;
  }

  public final String c()
  {
    return this.c;
  }

  public final int d()
  {
    return this.d;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.android.apps.analytics.a
 * JD-Core Version:    0.6.2
 */