package com.google.android.apps.analytics;

final class b
{
  private a[] a = new a[5];

  private static void d(int paramInt)
  {
    if ((paramInt <= 0) || (paramInt > 5))
      throw new IllegalArgumentException("Index must be between 1 and 5 inclusive.");
  }

  public final void a(a parama)
  {
    d(parama.d());
    this.a[(-1 + parama.d())] = parama;
  }

  public final boolean a(int paramInt)
  {
    d(paramInt);
    return this.a[(paramInt - 1)] == null;
  }

  public final a[] a()
  {
    return (a[])this.a.clone();
  }

  public final a b(int paramInt)
  {
    d(paramInt);
    return this.a[(paramInt - 1)];
  }

  public final boolean b()
  {
    for (int i = 0; ; i++)
    {
      int j = this.a.length;
      boolean bool = false;
      if (i < j)
      {
        if (this.a[i] != null)
          bool = true;
      }
      else
        return bool;
    }
  }

  public final void c(int paramInt)
  {
    d(paramInt);
    this.a[(paramInt - 1)] = null;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.android.apps.analytics.b
 * JD-Core Version:    0.6.2
 */