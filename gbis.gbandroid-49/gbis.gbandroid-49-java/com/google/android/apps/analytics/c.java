package com.google.android.apps.analytics;

final class c
{
  final long a;
  final String b;
  final String c;
  final String d;
  final String e;
  final int f;
  final int g;
  final int h;
  b i;
  private int j;
  private int k;
  private int l;
  private int m;
  private int n;
  private int o;
  private int p;
  private boolean q;
  private boolean r;
  private Transaction s;
  private Item t;

  c(long paramLong, String paramString1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, String paramString2, String paramString3, String paramString4, int paramInt6, int paramInt7, int paramInt8)
  {
    this.a = paramLong;
    this.b = paramString1;
    this.j = paramInt1;
    this.l = paramInt2;
    this.m = paramInt3;
    this.n = paramInt4;
    this.o = paramInt5;
    this.c = paramString2;
    this.d = paramString3;
    this.e = paramString4;
    this.f = paramInt6;
    this.h = paramInt8;
    this.g = paramInt7;
    this.p = -1;
    this.r = false;
  }

  c(c paramc, String paramString)
  {
    this(paramc.a, paramString, paramc.j, paramc.l, paramc.m, paramc.n, paramc.o, paramc.c, paramc.d, paramc.e, paramc.f, paramc.g, paramc.h);
    this.k = paramc.k;
    this.p = paramc.p;
    this.q = paramc.q;
    this.r = paramc.r;
    this.i = paramc.i;
    this.s = paramc.s;
    this.t = paramc.t;
  }

  c(String paramString1, String paramString2, String paramString3, String paramString4, int paramInt1, int paramInt2, int paramInt3)
  {
    this(-1L, paramString1, -1, -1, -1, -1, -1, paramString2, paramString3, paramString4, paramInt1, paramInt2, paramInt3);
  }

  final int a()
  {
    return this.j;
  }

  final void a(int paramInt)
  {
    this.j = paramInt;
  }

  public final void a(Item paramItem)
  {
    if (!this.c.equals("__##GOOGLEITEM##__"))
      throw new IllegalStateException("Attempted to add an item to an event of type " + this.c);
    this.t = paramItem;
  }

  public final void a(Transaction paramTransaction)
  {
    if (!this.c.equals("__##GOOGLETRANSACTION##__"))
      throw new IllegalStateException("Attempted to add a transction to an event of type " + this.c);
    this.s = paramTransaction;
  }

  public final void a(b paramb)
  {
    this.i = paramb;
  }

  final void a(boolean paramBoolean)
  {
    this.q = paramBoolean;
  }

  final int b()
  {
    return this.k;
  }

  final void b(int paramInt)
  {
    this.k = paramInt;
  }

  final void b(boolean paramBoolean)
  {
    this.r = paramBoolean;
  }

  final int c()
  {
    return this.l;
  }

  final void c(int paramInt)
  {
    this.l = paramInt;
  }

  final int d()
  {
    return this.m;
  }

  final void d(int paramInt)
  {
    this.m = paramInt;
  }

  final int e()
  {
    return this.n;
  }

  final void e(int paramInt)
  {
    this.n = paramInt;
  }

  final int f()
  {
    return this.o;
  }

  final void f(int paramInt)
  {
    this.o = paramInt;
  }

  final int g()
  {
    return this.p;
  }

  final void g(int paramInt)
  {
    this.p = paramInt;
  }

  final boolean h()
  {
    return this.q;
  }

  final boolean i()
  {
    return this.r;
  }

  public final b j()
  {
    return this.i;
  }

  public final Transaction k()
  {
    return this.s;
  }

  public final Item l()
  {
    return this.t;
  }

  public final boolean m()
  {
    return this.l != -1;
  }

  public final String toString()
  {
    return "id:" + this.a + " random:" + this.j + " timestampCurrent:" + this.n + " timestampPrevious:" + this.m + " timestampFirst:" + this.l + " visits:" + this.o + " value:" + this.f + " category:" + this.c + " action:" + this.d + " label:" + this.e + " width:" + this.g + " height:" + this.h;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.android.apps.analytics.c
 * JD-Core Version:    0.6.2
 */