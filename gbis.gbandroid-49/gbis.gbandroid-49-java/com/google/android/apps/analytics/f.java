package com.google.android.apps.analytics;

import java.util.Locale;

final class f
{
  private static String a(c paramc)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    b localb = paramc.j();
    if (localb == null)
      return "";
    if (!localb.b())
      return "";
    a[] arrayOfa = localb.a();
    a(arrayOfa, localStringBuilder, 8);
    a(arrayOfa, localStringBuilder, 9);
    a(arrayOfa, localStringBuilder, 11);
    return localStringBuilder.toString();
  }

  public static String a(c paramc, k paramk)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if ("__##GOOGLEPAGEVIEW##__".equals(paramc.c))
      localStringBuilder.append(b(paramc, paramk));
    while (true)
    {
      if (paramc.h())
        localStringBuilder.append("&aip=1");
      if (!paramc.i())
        localStringBuilder.append("&utmht=" + System.currentTimeMillis());
      return localStringBuilder.toString();
      if ("__##GOOGLEITEM##__".equals(paramc.c))
        localStringBuilder.append(e(paramc, paramk));
      else if ("__##GOOGLETRANSACTION##__".equals(paramc.c))
        localStringBuilder.append(d(paramc, paramk));
      else
        localStringBuilder.append(c(paramc, paramk));
    }
  }

  private static String a(String paramString)
  {
    return paramString.replace("'", "'0").replace(")", "'1").replace("*", "'2").replace("!", "'3");
  }

  private static void a(StringBuilder paramStringBuilder, String paramString, double paramDouble)
  {
    paramStringBuilder.append(paramString).append("=");
    double d = Math.floor(0.5D + paramDouble * 1000000.0D) / 1000000.0D;
    if (d != 0.0D)
      paramStringBuilder.append(Double.toString(d));
  }

  private static void a(StringBuilder paramStringBuilder, String paramString1, String paramString2)
  {
    paramStringBuilder.append(paramString1).append("=");
    if ((paramString2 != null) && (paramString2.trim().length() > 0))
      paramStringBuilder.append(AnalyticsParameterEncoder.encode(paramString2));
  }

  private static void a(a[] paramArrayOfa, StringBuilder paramStringBuilder, int paramInt)
  {
    paramStringBuilder.append(paramInt).append("(");
    int i = 1;
    int j = 0;
    if (j < paramArrayOfa.length)
    {
      a locala;
      if (paramArrayOfa[j] != null)
      {
        locala = paramArrayOfa[j];
        if (i != 0)
          break label102;
        paramStringBuilder.append("*");
        label49: paramStringBuilder.append(locala.d()).append("!");
        switch (paramInt)
        {
        case 10:
        default:
        case 8:
        case 9:
        case 11:
        }
      }
      while (true)
      {
        j++;
        break;
        label102: i = 0;
        break label49;
        paramStringBuilder.append(a(b(locala.b())));
        continue;
        paramStringBuilder.append(a(b(locala.c())));
        continue;
        paramStringBuilder.append(locala.a());
      }
    }
    paramStringBuilder.append(")");
  }

  private static String b(c paramc, k paramk)
  {
    String str1 = "";
    if (paramc.d != null)
      str1 = paramc.d;
    if (!str1.startsWith("/"))
      str1 = "/" + str1;
    String str2 = b(str1);
    String str3 = a(paramc);
    Locale localLocale = Locale.getDefault();
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("/__utm.gif");
    localStringBuilder.append("?utmwv=4.8.1ma");
    localStringBuilder.append("&utmn=").append(paramc.a());
    if (str3.length() > 0)
      localStringBuilder.append("&utme=").append(str3);
    localStringBuilder.append("&utmcs=UTF-8");
    Object[] arrayOfObject1 = new Object[2];
    arrayOfObject1[0] = Integer.valueOf(paramc.g);
    arrayOfObject1[1] = Integer.valueOf(paramc.h);
    localStringBuilder.append(String.format("&utmsr=%dx%d", arrayOfObject1));
    Object[] arrayOfObject2 = new Object[2];
    arrayOfObject2[0] = localLocale.getLanguage();
    arrayOfObject2[1] = localLocale.getCountry();
    localStringBuilder.append(String.format("&utmul=%s-%s", arrayOfObject2));
    localStringBuilder.append("&utmp=").append(str2);
    localStringBuilder.append("&utmac=").append(paramc.b);
    localStringBuilder.append("&utmcc=").append(f(paramc, paramk));
    if (paramc.b() != 0)
      localStringBuilder.append("&utmhid=").append(paramc.b());
    return localStringBuilder.toString();
  }

  private static String b(String paramString)
  {
    return AnalyticsParameterEncoder.encode(paramString);
  }

  private static String c(c paramc, k paramk)
  {
    Locale localLocale = Locale.getDefault();
    StringBuilder localStringBuilder1 = new StringBuilder();
    StringBuilder localStringBuilder2 = new StringBuilder();
    Object[] arrayOfObject1 = new Object[2];
    arrayOfObject1[0] = b(paramc.c);
    arrayOfObject1[1] = b(paramc.d);
    localStringBuilder2.append(String.format("5(%s*%s", arrayOfObject1));
    if (paramc.e != null)
      localStringBuilder2.append("*").append(b(paramc.e));
    localStringBuilder2.append(")");
    if (paramc.f >= 0)
    {
      Object[] arrayOfObject4 = new Object[1];
      arrayOfObject4[0] = Integer.valueOf(paramc.f);
      localStringBuilder2.append(String.format("(%d)", arrayOfObject4));
    }
    localStringBuilder2.append(a(paramc));
    localStringBuilder1.append("/__utm.gif");
    localStringBuilder1.append("?utmwv=4.8.1ma");
    localStringBuilder1.append("&utmn=").append(paramc.a());
    localStringBuilder1.append("&utmt=event");
    localStringBuilder1.append("&utme=").append(localStringBuilder2.toString());
    localStringBuilder1.append("&utmcs=UTF-8");
    Object[] arrayOfObject2 = new Object[2];
    arrayOfObject2[0] = Integer.valueOf(paramc.g);
    arrayOfObject2[1] = Integer.valueOf(paramc.h);
    localStringBuilder1.append(String.format("&utmsr=%dx%d", arrayOfObject2));
    Object[] arrayOfObject3 = new Object[2];
    arrayOfObject3[0] = localLocale.getLanguage();
    arrayOfObject3[1] = localLocale.getCountry();
    localStringBuilder1.append(String.format("&utmul=%s-%s", arrayOfObject3));
    localStringBuilder1.append("&utmac=").append(paramc.b);
    localStringBuilder1.append("&utmcc=").append(f(paramc, paramk));
    if (paramc.b() != 0)
      localStringBuilder1.append("&utmhid=").append(paramc.b());
    return localStringBuilder1.toString();
  }

  private static String d(c paramc, k paramk)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("/__utm.gif");
    localStringBuilder.append("?utmwv=4.8.1ma");
    localStringBuilder.append("&utmn=").append(paramc.a());
    localStringBuilder.append("&utmt=tran");
    Transaction localTransaction = paramc.k();
    if (localTransaction != null)
    {
      a(localStringBuilder, "&utmtid", localTransaction.a());
      a(localStringBuilder, "&utmtst", localTransaction.b());
      a(localStringBuilder, "&utmtto", localTransaction.c());
      a(localStringBuilder, "&utmttx", localTransaction.d());
      a(localStringBuilder, "&utmtsp", localTransaction.e());
      a(localStringBuilder, "&utmtci", "");
      a(localStringBuilder, "&utmtrg", "");
      a(localStringBuilder, "&utmtco", "");
    }
    localStringBuilder.append("&utmac=").append(paramc.b);
    localStringBuilder.append("&utmcc=").append(f(paramc, paramk));
    return localStringBuilder.toString();
  }

  private static String e(c paramc, k paramk)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("/__utm.gif");
    localStringBuilder.append("?utmwv=4.8.1ma");
    localStringBuilder.append("&utmn=").append(paramc.a());
    localStringBuilder.append("&utmt=item");
    Item localItem = paramc.l();
    if (localItem != null)
    {
      a(localStringBuilder, "&utmtid", localItem.a());
      a(localStringBuilder, "&utmipc", localItem.b());
      a(localStringBuilder, "&utmipn", localItem.c());
      a(localStringBuilder, "&utmiva", localItem.d());
      a(localStringBuilder, "&utmipr", localItem.e());
      localStringBuilder.append("&utmiqt=");
      if (localItem.f() != 0L)
        localStringBuilder.append(localItem.f());
    }
    localStringBuilder.append("&utmac=").append(paramc.b);
    localStringBuilder.append("&utmcc=").append(f(paramc, paramk));
    return localStringBuilder.toString();
  }

  private static String f(c paramc, k paramk)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("__utma=");
    localStringBuilder.append("1.");
    localStringBuilder.append(paramc.g()).append(".");
    localStringBuilder.append(paramc.c()).append(".");
    localStringBuilder.append(paramc.d()).append(".");
    localStringBuilder.append(paramc.e()).append(".");
    localStringBuilder.append(paramc.f()).append(";");
    if (paramk != null)
    {
      localStringBuilder.append("+__utmz=");
      localStringBuilder.append("1.");
      localStringBuilder.append(paramk.b()).append(".");
      localStringBuilder.append(Integer.valueOf(paramk.c()).toString()).append(".");
      localStringBuilder.append(Integer.valueOf(paramk.d()).toString()).append(".");
      localStringBuilder.append(paramk.a()).append(";");
    }
    return b(localStringBuilder.toString());
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.android.apps.analytics.f
 * JD-Core Version:    0.6.2
 */