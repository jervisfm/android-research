package com.google.android.apps.analytics;

import android.os.Build;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Locale;
import org.apache.http.Header;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;

final class h
  implements Dispatcher
{
  private final String a;
  private final HttpHost b;
  private a c;
  private boolean d = false;

  public h()
  {
    this("GoogleAnalytics", "1.4.2");
  }

  public h(String paramString1, String paramString2)
  {
    this(paramString1, paramString2, "www.google-analytics.com");
  }

  private h(String paramString1, String paramString2, String paramString3)
  {
    this.b = new HttpHost(paramString3, 80);
    Locale localLocale = Locale.getDefault();
    Object[] arrayOfObject = new Object[7];
    arrayOfObject[0] = paramString1;
    arrayOfObject[1] = paramString2;
    arrayOfObject[2] = Build.VERSION.RELEASE;
    String str1;
    if (localLocale.getLanguage() != null)
    {
      str1 = localLocale.getLanguage().toLowerCase();
      arrayOfObject[3] = str1;
      if (localLocale.getCountry() == null)
        break label134;
    }
    label134: for (String str2 = localLocale.getCountry().toLowerCase(); ; str2 = "")
    {
      arrayOfObject[4] = str2;
      arrayOfObject[5] = Build.MODEL;
      arrayOfObject[6] = Build.ID;
      this.a = String.format("%s/%s (Linux; U; Android %s; %s-%s; %s Build/%s)", arrayOfObject);
      return;
      str1 = "en";
      break;
    }
  }

  public final void a()
  {
    if ((this.c != null) && (this.c.getLooper() != null))
    {
      this.c.getLooper().quit();
      this.c = null;
    }
  }

  public final void a(Dispatcher.Callbacks paramCallbacks)
  {
    a();
    this.c = new a(paramCallbacks, this.a, this, (byte)0);
    this.c.start();
  }

  public final void a(boolean paramBoolean)
  {
    this.d = paramBoolean;
  }

  public final void a(Hit[] paramArrayOfHit)
  {
    if (this.c == null)
      return;
    this.c.a(paramArrayOfHit);
  }

  public final boolean b()
  {
    return this.d;
  }

  private static final class a extends HandlerThread
  {
    volatile Handler a;
    private final j b;
    private final String c;
    private int d;
    private int e = 30;
    private long f;
    private a g = null;
    private final Dispatcher.Callbacks h;
    private final b i;
    private final h j;

    private a(Dispatcher.Callbacks paramCallbacks, j paramj, String paramString, h paramh)
    {
      super();
      this.h = paramCallbacks;
      this.c = paramString;
      this.b = paramj;
      this.i = new b((byte)0);
      this.b.a(this.i);
      this.j = paramh;
    }

    private a(Dispatcher.Callbacks paramCallbacks, String paramString, h paramh)
    {
      this(paramCallbacks, new j(h.a(paramh)), paramString, paramh);
    }

    public final void a(Hit[] paramArrayOfHit)
    {
      if (this.a == null)
        return;
      this.a.post(new a(paramArrayOfHit));
    }

    protected final void onLooperPrepared()
    {
      this.a = new Handler();
    }

    private final class a
      implements Runnable
    {
      private final LinkedList<Hit> b = new LinkedList();

      public a(Hit[] arg2)
      {
        Object[] arrayOfObject;
        Collections.addAll(this.b, arrayOfObject);
      }

      private void a(boolean paramBoolean)
      {
        if ((GoogleAnalyticsTracker.getInstance().getDebug()) && (paramBoolean))
          Log.v("GoogleAnalyticsTracker", "dispatching hits in dry run mode");
        int i = 0;
        if ((i < this.b.size()) && (i < h.a.h(h.a.this)))
        {
          String str1 = l.a(((Hit)this.b.get(i)).a, System.currentTimeMillis());
          int j = str1.indexOf('?');
          String str2;
          String str3;
          if (j < 0)
          {
            str2 = str1;
            str3 = "";
            if (str3.length() >= 2036)
              break label320;
          }
          StringBuffer localStringBuffer;
          label318: label320: BasicHttpEntityEnclosingRequest localBasicHttpEntityEnclosingRequest;
          for (Object localObject = new BasicHttpEntityEnclosingRequest("GET", str1); ; localObject = localBasicHttpEntityEnclosingRequest)
          {
            String str4 = h.a(h.this).getHostName();
            if (h.a(h.this).getPort() != 80)
              str4 = str4 + ":" + h.a(h.this).getPort();
            ((HttpEntityEnclosingRequest)localObject).addHeader("Host", str4);
            ((HttpEntityEnclosingRequest)localObject).addHeader("User-Agent", h.a.i(h.a.this));
            if (!GoogleAnalyticsTracker.getInstance().getDebug())
              break label431;
            localStringBuffer = new StringBuffer();
            Header[] arrayOfHeader = ((HttpEntityEnclosingRequest)localObject).getAllHeaders();
            int k = arrayOfHeader.length;
            for (int m = 0; m < k; m++)
              localStringBuffer.append(arrayOfHeader[m].toString()).append("\n");
            if (j > 0);
            for (str2 = str1.substring(0, j); ; str2 = "")
            {
              if (j >= -2 + str1.length())
                break label318;
              str3 = str1.substring(j + 1);
              break;
            }
            break;
            localBasicHttpEntityEnclosingRequest = new BasicHttpEntityEnclosingRequest("POST", "/p" + str2);
            localBasicHttpEntityEnclosingRequest.addHeader("Content-Length", Integer.toString(str3.length()));
            localBasicHttpEntityEnclosingRequest.addHeader("Content-Type", "text/plain");
            localBasicHttpEntityEnclosingRequest.setEntity(new StringEntity(str3));
          }
          localStringBuffer.append(((HttpEntityEnclosingRequest)localObject).getRequestLine().toString()).append("\n");
          Log.i("GoogleAnalyticsTracker", localStringBuffer.toString());
          label431: if (str3.length() > 8192)
          {
            Log.w("GoogleAnalyticsTracker", "Hit too long (> 8192 bytes)--not sent");
            h.a.j(h.a.this).b();
          }
          while (true)
          {
            i++;
            break;
            if (paramBoolean)
              h.a.j(h.a.this).b();
            else
              h.a.f(h.a.this).a((HttpEntityEnclosingRequest)localObject);
          }
        }
        if (!paramBoolean)
          h.a.f(h.a.this).a();
      }

      public final Hit a()
      {
        return (Hit)this.b.poll();
      }

      public final void run()
      {
        h.a.a(h.a.this, this);
        int i = 0;
        while (true)
        {
          long l;
          if ((i < 5) && (this.b.size() > 0))
            l = 0L;
          try
          {
            if ((h.a.a(h.a.this) == 500) || (h.a.a(h.a.this) == 503))
            {
              l = ()(Math.random() * h.a.b(h.a.this));
              if (h.a.b(h.a.this) < 256L)
                h.a.c(h.a.this);
            }
            while (true)
            {
              Thread.sleep(l * 1000L);
              a(h.this.b());
              i++;
              break;
              h.a.d(h.a.this);
            }
          }
          catch (InterruptedException localInterruptedException)
          {
            Log.w("GoogleAnalyticsTracker", "Couldn't sleep.", localInterruptedException);
            h.a.f(h.a.this).b();
            h.a.g(h.a.this).dispatchFinished();
            h.a.a(h.a.this, null);
            return;
          }
          catch (IOException localIOException)
          {
            while (true)
              Log.w("GoogleAnalyticsTracker", "Problem with socket or streams.", localIOException);
          }
          catch (HttpException localHttpException)
          {
            while (true)
              Log.w("GoogleAnalyticsTracker", "Problem with http streams.", localHttpException);
          }
        }
      }
    }

    private final class b
      implements j.a
    {
      private b()
      {
      }

      public final void a()
      {
        h.a.a(h.a.this, 1);
      }

      public final void a(int paramInt)
      {
        h.a.b(h.a.this, paramInt);
      }

      public final void b()
      {
        if (h.a.k(h.a.this) == null);
        Hit localHit;
        do
        {
          return;
          localHit = h.a.k(h.a.this).a();
        }
        while (localHit == null);
        h.a.g(h.a.this).hitDispatched(localHit.b);
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.android.apps.analytics.h
 * JD-Core Version:    0.6.2
 */