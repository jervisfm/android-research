package com.google.android.apps.analytics;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;
import java.util.Random;

final class i
  implements g
{
  private static final String n = "CREATE TABLE events (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", new Object[] { "event_id" }) + String.format(" '%s' INTEGER NOT NULL,", new Object[] { "user_id" }) + String.format(" '%s' CHAR(256) NOT NULL,", new Object[] { "account_id" }) + String.format(" '%s' INTEGER NOT NULL,", new Object[] { "random_val" }) + String.format(" '%s' INTEGER NOT NULL,", new Object[] { "timestamp_first" }) + String.format(" '%s' INTEGER NOT NULL,", new Object[] { "timestamp_previous" }) + String.format(" '%s' INTEGER NOT NULL,", new Object[] { "timestamp_current" }) + String.format(" '%s' INTEGER NOT NULL,", new Object[] { "visits" }) + String.format(" '%s' CHAR(256) NOT NULL,", new Object[] { "category" }) + String.format(" '%s' CHAR(256) NOT NULL,", new Object[] { "action" }) + String.format(" '%s' CHAR(256), ", new Object[] { "label" }) + String.format(" '%s' INTEGER,", new Object[] { "value" }) + String.format(" '%s' INTEGER,", new Object[] { "screen_width" }) + String.format(" '%s' INTEGER);", new Object[] { "screen_height" });
  private static final String o = "CREATE TABLE IF NOT EXISTS session (" + String.format(" '%s' INTEGER PRIMARY KEY,", new Object[] { "timestamp_first" }) + String.format(" '%s' INTEGER NOT NULL,", new Object[] { "timestamp_previous" }) + String.format(" '%s' INTEGER NOT NULL,", new Object[] { "timestamp_current" }) + String.format(" '%s' INTEGER NOT NULL,", new Object[] { "visits" }) + String.format(" '%s' INTEGER NOT NULL);", new Object[] { "store_id" });
  private static final String p = "CREATE TABLE custom_variables (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", new Object[] { "cv_id" }) + String.format(" '%s' INTEGER NOT NULL,", new Object[] { "event_id" }) + String.format(" '%s' INTEGER NOT NULL,", new Object[] { "cv_index" }) + String.format(" '%s' CHAR(64) NOT NULL,", new Object[] { "cv_name" }) + String.format(" '%s' CHAR(64) NOT NULL,", new Object[] { "cv_value" }) + String.format(" '%s' INTEGER NOT NULL);", new Object[] { "cv_scope" });
  private static final String q = "CREATE TABLE IF NOT EXISTS custom_var_cache (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", new Object[] { "cv_id" }) + String.format(" '%s' INTEGER NOT NULL,", new Object[] { "event_id" }) + String.format(" '%s' INTEGER NOT NULL,", new Object[] { "cv_index" }) + String.format(" '%s' CHAR(64) NOT NULL,", new Object[] { "cv_name" }) + String.format(" '%s' CHAR(64) NOT NULL,", new Object[] { "cv_value" }) + String.format(" '%s' INTEGER NOT NULL);", new Object[] { "cv_scope" });
  private static final String r = "CREATE TABLE transaction_events (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", new Object[] { "tran_id" }) + String.format(" '%s' INTEGER NOT NULL,", new Object[] { "event_id" }) + String.format(" '%s' TEXT NOT NULL,", new Object[] { "order_id" }) + String.format(" '%s' TEXT,", new Object[] { "tran_storename" }) + String.format(" '%s' TEXT NOT NULL,", new Object[] { "tran_totalcost" }) + String.format(" '%s' TEXT,", new Object[] { "tran_totaltax" }) + String.format(" '%s' TEXT);", new Object[] { "tran_shippingcost" });
  private static final String s = "CREATE TABLE item_events (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", new Object[] { "item_id" }) + String.format(" '%s' INTEGER NOT NULL,", new Object[] { "event_id" }) + String.format(" '%s' TEXT NOT NULL,", new Object[] { "order_id" }) + String.format(" '%s' TEXT NOT NULL,", new Object[] { "item_sku" }) + String.format(" '%s' TEXT,", new Object[] { "item_name" }) + String.format(" '%s' TEXT,", new Object[] { "item_category" }) + String.format(" '%s' TEXT NOT NULL,", new Object[] { "item_price" }) + String.format(" '%s' TEXT NOT NULL);", new Object[] { "item_count" });
  private static final String t = "CREATE TABLE IF NOT EXISTS hits (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", new Object[] { "hit_id" }) + String.format(" '%s' TEXT NOT NULL,", new Object[] { "hit_string" }) + String.format(" '%s' INTEGER NOT NULL);", new Object[] { "hit_time" });
  private a a = new a(paramContext, paramString, 5, this);
  private int b;
  private long c;
  private long d;
  private long e;
  private int f;
  private volatile int g;
  private boolean h;
  private boolean i;
  private boolean j;
  private int k = 100;
  private Random l = new Random();
  private b m;

  i(Context paramContext)
  {
    this(paramContext, "google_analytics.db");
  }

  private i(Context paramContext, String paramString)
  {
    p();
    this.m = n();
  }

  // ERROR //
  private static Transaction a(long paramLong, SQLiteDatabase paramSQLiteDatabase)
  {
    // Byte code:
    //   0: iconst_1
    //   1: anewarray 49	java/lang/String
    //   4: astore 7
    //   6: aload 7
    //   8: iconst_0
    //   9: lload_0
    //   10: invokestatic 216	java/lang/Long:toString	(J)Ljava/lang/String;
    //   13: aastore
    //   14: aload_2
    //   15: ldc 218
    //   17: aconst_null
    //   18: ldc 220
    //   20: aload 7
    //   22: aconst_null
    //   23: aconst_null
    //   24: aconst_null
    //   25: invokevirtual 226	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   28: astore 8
    //   30: aload 8
    //   32: astore 4
    //   34: aload 4
    //   36: invokeinterface 232 1 0
    //   41: ifeq +119 -> 160
    //   44: new 234	com/google/android/apps/analytics/Transaction$Builder
    //   47: dup
    //   48: aload 4
    //   50: aload 4
    //   52: ldc 137
    //   54: invokeinterface 238 2 0
    //   59: invokeinterface 242 2 0
    //   64: aload 4
    //   66: aload 4
    //   68: ldc 143
    //   70: invokeinterface 238 2 0
    //   75: invokeinterface 246 2 0
    //   80: invokespecial 249	com/google/android/apps/analytics/Transaction$Builder:<init>	(Ljava/lang/String;D)V
    //   83: aload 4
    //   85: aload 4
    //   87: ldc 141
    //   89: invokeinterface 238 2 0
    //   94: invokeinterface 242 2 0
    //   99: invokevirtual 253	com/google/android/apps/analytics/Transaction$Builder:setStoreName	(Ljava/lang/String;)Lcom/google/android/apps/analytics/Transaction$Builder;
    //   102: aload 4
    //   104: aload 4
    //   106: ldc 145
    //   108: invokeinterface 238 2 0
    //   113: invokeinterface 246 2 0
    //   118: invokevirtual 257	com/google/android/apps/analytics/Transaction$Builder:setTotalTax	(D)Lcom/google/android/apps/analytics/Transaction$Builder;
    //   121: aload 4
    //   123: aload 4
    //   125: ldc 149
    //   127: invokeinterface 238 2 0
    //   132: invokeinterface 246 2 0
    //   137: invokevirtual 260	com/google/android/apps/analytics/Transaction$Builder:setShippingCost	(D)Lcom/google/android/apps/analytics/Transaction$Builder;
    //   140: invokevirtual 264	com/google/android/apps/analytics/Transaction$Builder:build	()Lcom/google/android/apps/analytics/Transaction;
    //   143: astore 9
    //   145: aload 4
    //   147: ifnull +10 -> 157
    //   150: aload 4
    //   152: invokeinterface 267 1 0
    //   157: aload 9
    //   159: areturn
    //   160: aload 4
    //   162: ifnull +10 -> 172
    //   165: aload 4
    //   167: invokeinterface 267 1 0
    //   172: aconst_null
    //   173: areturn
    //   174: astore 5
    //   176: aconst_null
    //   177: astore 4
    //   179: ldc_w 269
    //   182: aload 5
    //   184: invokevirtual 270	android/database/sqlite/SQLiteException:toString	()Ljava/lang/String;
    //   187: invokestatic 275	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   190: pop
    //   191: aload 4
    //   193: ifnull -21 -> 172
    //   196: aload 4
    //   198: invokeinterface 267 1 0
    //   203: goto -31 -> 172
    //   206: astore_3
    //   207: aconst_null
    //   208: astore 4
    //   210: aload 4
    //   212: ifnull +10 -> 222
    //   215: aload 4
    //   217: invokeinterface 267 1 0
    //   222: aload_3
    //   223: athrow
    //   224: astore_3
    //   225: goto -15 -> 210
    //   228: astore 5
    //   230: goto -51 -> 179
    //
    // Exception table:
    //   from	to	target	type
    //   0	30	174	android/database/sqlite/SQLiteException
    //   0	30	206	finally
    //   34	145	224	finally
    //   179	191	224	finally
    //   34	145	228	android/database/sqlite/SQLiteException
  }

  // ERROR //
  static b a(SQLiteDatabase paramSQLiteDatabase)
  {
    // Byte code:
    //   0: new 278	com/google/android/apps/analytics/b
    //   3: dup
    //   4: invokespecial 279	com/google/android/apps/analytics/b:<init>	()V
    //   7: astore_1
    //   8: iconst_1
    //   9: anewarray 49	java/lang/String
    //   12: astore 6
    //   14: aload 6
    //   16: iconst_0
    //   17: iconst_1
    //   18: invokestatic 283	java/lang/Integer:toString	(I)Ljava/lang/String;
    //   21: aastore
    //   22: aload_0
    //   23: ldc_w 285
    //   26: aconst_null
    //   27: ldc_w 287
    //   30: aload 6
    //   32: aconst_null
    //   33: aconst_null
    //   34: aconst_null
    //   35: invokevirtual 226	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   38: astore 7
    //   40: aload 7
    //   42: astore_3
    //   43: aload_3
    //   44: invokeinterface 290 1 0
    //   49: ifeq +97 -> 146
    //   52: aload_1
    //   53: new 292	com/google/android/apps/analytics/a
    //   56: dup
    //   57: aload_3
    //   58: aload_3
    //   59: ldc 115
    //   61: invokeinterface 238 2 0
    //   66: invokeinterface 296 2 0
    //   71: aload_3
    //   72: aload_3
    //   73: ldc 119
    //   75: invokeinterface 238 2 0
    //   80: invokeinterface 242 2 0
    //   85: aload_3
    //   86: aload_3
    //   87: ldc 121
    //   89: invokeinterface 238 2 0
    //   94: invokeinterface 242 2 0
    //   99: aload_3
    //   100: aload_3
    //   101: ldc 123
    //   103: invokeinterface 238 2 0
    //   108: invokeinterface 296 2 0
    //   113: invokespecial 299	com/google/android/apps/analytics/a:<init>	(ILjava/lang/String;Ljava/lang/String;I)V
    //   116: invokevirtual 302	com/google/android/apps/analytics/b:a	(Lcom/google/android/apps/analytics/a;)V
    //   119: goto -76 -> 43
    //   122: astore_2
    //   123: ldc_w 269
    //   126: aload_2
    //   127: invokevirtual 270	android/database/sqlite/SQLiteException:toString	()Ljava/lang/String;
    //   130: invokestatic 275	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   133: pop
    //   134: aload_3
    //   135: ifnull +9 -> 144
    //   138: aload_3
    //   139: invokeinterface 267 1 0
    //   144: aload_1
    //   145: areturn
    //   146: aload_3
    //   147: ifnull -3 -> 144
    //   150: aload_3
    //   151: invokeinterface 267 1 0
    //   156: aload_1
    //   157: areturn
    //   158: astore 4
    //   160: aconst_null
    //   161: astore_3
    //   162: aload_3
    //   163: ifnull +9 -> 172
    //   166: aload_3
    //   167: invokeinterface 267 1 0
    //   172: aload 4
    //   174: athrow
    //   175: astore 4
    //   177: goto -15 -> 162
    //   180: astore_2
    //   181: aconst_null
    //   182: astore_3
    //   183: goto -60 -> 123
    //
    // Exception table:
    //   from	to	target	type
    //   43	119	122	android/database/sqlite/SQLiteException
    //   8	40	158	finally
    //   43	119	175	finally
    //   123	134	175	finally
    //   8	40	180	android/database/sqlite/SQLiteException
  }

  private void a(c paramc, SQLiteDatabase paramSQLiteDatabase)
  {
    if (("__##GOOGLEITEM##__".equals(paramc.c)) || ("__##GOOGLETRANSACTION##__".equals(paramc.c)))
      return;
    while (true)
    {
      b localb1;
      try
      {
        localb1 = paramc.j();
        if (this.i)
        {
          if (localb1 != null)
            break label309;
          localb1 = new b();
          paramc.a(localb1);
          break label309;
          if (i1 <= 5)
          {
            a locala1 = this.m.b(i1);
            a locala2 = localb1.b(i1);
            if ((locala1 == null) || (locala2 != null))
              break label315;
            localb1.a(locala1);
            break label315;
          }
          this.i = false;
          localb2 = localb1;
          break label321;
          if (i2 > 5)
            break;
          if (localb2.a(i2))
            break label332;
          a locala3 = localb2.b(i2);
          ContentValues localContentValues = new ContentValues();
          localContentValues.put("event_id", Integer.valueOf(0));
          localContentValues.put("cv_index", Integer.valueOf(locala3.d()));
          localContentValues.put("cv_name", locala3.b());
          localContentValues.put("cv_scope", Integer.valueOf(locala3.a()));
          localContentValues.put("cv_value", locala3.c());
          String[] arrayOfString = new String[1];
          arrayOfString[0] = Integer.toString(locala3.d());
          paramSQLiteDatabase.update("custom_var_cache", localContentValues, "cv_index = ?", arrayOfString);
          if (locala3.a() == 1)
            this.m.a(locala3);
          else
            this.m.c(locala3.d());
        }
      }
      catch (SQLiteException localSQLiteException)
      {
        Log.e("GoogleAnalyticsTracker", localSQLiteException.toString());
        return;
      }
      b localb2 = localb1;
      break label321;
      label309: int i1 = 1;
      continue;
      label315: i1++;
      continue;
      label321: if (localb2 == null)
        break;
      int i2 = 1;
      continue;
      label332: i2++;
    }
  }

  private void a(c paramc, SQLiteDatabase paramSQLiteDatabase, boolean paramBoolean)
  {
    if (!paramc.m())
    {
      paramc.a(this.l.nextInt(2147483647));
      paramc.c((int)this.c);
      paramc.d((int)this.d);
      paramc.e((int)this.e);
      paramc.f(this.f);
    }
    paramc.a(this.j);
    if (paramc.g() == -1)
      paramc.g(this.b);
    a(paramc, paramSQLiteDatabase);
    k localk = f(paramSQLiteDatabase);
    String[] arrayOfString = paramc.b.split(",");
    if (arrayOfString.length == 1)
      a(paramc, localk, paramSQLiteDatabase, paramBoolean);
    while (true)
    {
      return;
      int i1 = arrayOfString.length;
      for (int i2 = 0; i2 < i1; i2++)
        a(new c(paramc, arrayOfString[i2]), localk, paramSQLiteDatabase, paramBoolean);
    }
  }

  private void a(c paramc, k paramk, SQLiteDatabase paramSQLiteDatabase, boolean paramBoolean)
  {
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("hit_string", f.a(paramc, paramk));
    if (paramBoolean);
    for (long l1 = System.currentTimeMillis(); ; l1 = 0L)
    {
      localContentValues.put("hit_time", Long.valueOf(l1));
      paramSQLiteDatabase.insert("hits", null, localContentValues);
      this.g = (1 + this.g);
      return;
    }
  }

  private static boolean a(SQLiteDatabase paramSQLiteDatabase, ContentValues paramContentValues)
  {
    try
    {
      paramSQLiteDatabase.beginTransaction();
      paramSQLiteDatabase.delete("referrer", null, null);
      paramSQLiteDatabase.insert("referrer", null, paramContentValues);
      paramSQLiteDatabase.setTransactionSuccessful();
      if ((paramSQLiteDatabase.inTransaction()) && (!d(paramSQLiteDatabase)))
        return false;
    }
    catch (SQLiteException localSQLiteException)
    {
      do
        Log.e("GoogleAnalyticsTracker", localSQLiteException.toString());
      while ((!paramSQLiteDatabase.inTransaction()) || (d(paramSQLiteDatabase)));
      return false;
    }
    finally
    {
      do
        if (!paramSQLiteDatabase.inTransaction())
          break;
      while (!d(paramSQLiteDatabase));
    }
    return true;
  }

  // ERROR //
  public static c[] a(SQLiteDatabase paramSQLiteDatabase, int paramInt)
  {
    // Byte code:
    //   0: new 465	java/util/ArrayList
    //   3: dup
    //   4: invokespecial 466	java/util/ArrayList:<init>	()V
    //   7: astore_2
    //   8: aload_0
    //   9: ldc_w 468
    //   12: aconst_null
    //   13: aconst_null
    //   14: aconst_null
    //   15: aconst_null
    //   16: aconst_null
    //   17: ldc 47
    //   19: sipush 1000
    //   22: invokestatic 283	java/lang/Integer:toString	(I)Ljava/lang/String;
    //   25: invokevirtual 471	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   28: astore 9
    //   30: aload 9
    //   32: astore 6
    //   34: aload 6
    //   36: invokeinterface 290 1 0
    //   41: ifeq +381 -> 422
    //   44: new 308	com/google/android/apps/analytics/c
    //   47: dup
    //   48: aload 6
    //   50: iconst_0
    //   51: invokeinterface 475 2 0
    //   56: aload 6
    //   58: iconst_2
    //   59: invokeinterface 242 2 0
    //   64: aload 6
    //   66: iconst_3
    //   67: invokeinterface 296 2 0
    //   72: aload 6
    //   74: iconst_4
    //   75: invokeinterface 296 2 0
    //   80: aload 6
    //   82: iconst_5
    //   83: invokeinterface 296 2 0
    //   88: aload 6
    //   90: bipush 6
    //   92: invokeinterface 296 2 0
    //   97: aload 6
    //   99: bipush 7
    //   101: invokeinterface 296 2 0
    //   106: aload 6
    //   108: bipush 8
    //   110: invokeinterface 242 2 0
    //   115: aload 6
    //   117: bipush 9
    //   119: invokeinterface 242 2 0
    //   124: aload 6
    //   126: bipush 10
    //   128: invokeinterface 242 2 0
    //   133: aload 6
    //   135: bipush 11
    //   137: invokeinterface 296 2 0
    //   142: aload 6
    //   144: bipush 12
    //   146: invokeinterface 296 2 0
    //   151: aload 6
    //   153: bipush 13
    //   155: invokeinterface 296 2 0
    //   160: invokespecial 478	com/google/android/apps/analytics/c:<init>	(JLjava/lang/String;IIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V
    //   163: astore 10
    //   165: aload 10
    //   167: aload 6
    //   169: iconst_1
    //   170: invokeinterface 296 2 0
    //   175: invokevirtual 396	com/google/android/apps/analytics/c:g	(I)V
    //   178: aload 6
    //   180: ldc 47
    //   182: invokeinterface 238 2 0
    //   187: istore 11
    //   189: aload 6
    //   191: iload 11
    //   193: invokeinterface 475 2 0
    //   198: lstore 12
    //   200: ldc_w 316
    //   203: aload 10
    //   205: getfield 310	com/google/android/apps/analytics/c:c	Ljava/lang/String;
    //   208: invokevirtual 314	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   211: ifeq +97 -> 308
    //   214: lload 12
    //   216: aload_0
    //   217: invokestatic 480	com/google/android/apps/analytics/i:a	(JLandroid/database/sqlite/SQLiteDatabase;)Lcom/google/android/apps/analytics/Transaction;
    //   220: astore 18
    //   222: aload 18
    //   224: ifnonnull +28 -> 252
    //   227: ldc_w 269
    //   230: new 37	java/lang/StringBuilder
    //   233: dup
    //   234: ldc_w 482
    //   237: invokespecial 43	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   240: lload 12
    //   242: invokevirtual 485	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   245: invokevirtual 97	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   248: invokestatic 488	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
    //   251: pop
    //   252: aload 10
    //   254: aload 18
    //   256: invokevirtual 491	com/google/android/apps/analytics/c:a	(Lcom/google/android/apps/analytics/Transaction;)V
    //   259: aload_2
    //   260: aload 10
    //   262: invokeinterface 496 2 0
    //   267: pop
    //   268: goto -234 -> 34
    //   271: astore_3
    //   272: aload 6
    //   274: astore 4
    //   276: ldc_w 269
    //   279: aload_3
    //   280: invokevirtual 270	android/database/sqlite/SQLiteException:toString	()Ljava/lang/String;
    //   283: invokestatic 275	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   286: pop
    //   287: iconst_0
    //   288: anewarray 308	com/google/android/apps/analytics/c
    //   291: astore 8
    //   293: aload 4
    //   295: ifnull +10 -> 305
    //   298: aload 4
    //   300: invokeinterface 267 1 0
    //   305: aload 8
    //   307: areturn
    //   308: ldc_w 306
    //   311: aload 10
    //   313: getfield 310	com/google/android/apps/analytics/c:c	Ljava/lang/String;
    //   316: invokevirtual 314	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   319: ifeq +68 -> 387
    //   322: lload 12
    //   324: aload_0
    //   325: invokestatic 499	com/google/android/apps/analytics/i:b	(JLandroid/database/sqlite/SQLiteDatabase;)Lcom/google/android/apps/analytics/Item;
    //   328: astore 16
    //   330: aload 16
    //   332: ifnonnull +28 -> 360
    //   335: ldc_w 269
    //   338: new 37	java/lang/StringBuilder
    //   341: dup
    //   342: ldc_w 501
    //   345: invokespecial 43	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   348: lload 12
    //   350: invokevirtual 485	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   353: invokevirtual 97	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   356: invokestatic 488	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
    //   359: pop
    //   360: aload 10
    //   362: aload 16
    //   364: invokevirtual 504	com/google/android/apps/analytics/c:a	(Lcom/google/android/apps/analytics/Item;)V
    //   367: goto -108 -> 259
    //   370: astore 5
    //   372: aload 6
    //   374: ifnull +10 -> 384
    //   377: aload 6
    //   379: invokeinterface 267 1 0
    //   384: aload 5
    //   386: athrow
    //   387: iload_1
    //   388: iconst_1
    //   389: if_icmple +21 -> 410
    //   392: lload 12
    //   394: aload_0
    //   395: invokestatic 507	com/google/android/apps/analytics/i:c	(JLandroid/database/sqlite/SQLiteDatabase;)Lcom/google/android/apps/analytics/b;
    //   398: astore 14
    //   400: aload 10
    //   402: aload 14
    //   404: invokevirtual 323	com/google/android/apps/analytics/c:a	(Lcom/google/android/apps/analytics/b;)V
    //   407: goto -148 -> 259
    //   410: new 278	com/google/android/apps/analytics/b
    //   413: dup
    //   414: invokespecial 279	com/google/android/apps/analytics/b:<init>	()V
    //   417: astore 14
    //   419: goto -19 -> 400
    //   422: aload 6
    //   424: ifnull +10 -> 434
    //   427: aload 6
    //   429: invokeinterface 267 1 0
    //   434: aload_2
    //   435: aload_2
    //   436: invokeinterface 510 1 0
    //   441: anewarray 308	com/google/android/apps/analytics/c
    //   444: invokeinterface 514 2 0
    //   449: checkcast 516	[Lcom/google/android/apps/analytics/c;
    //   452: areturn
    //   453: astore 5
    //   455: aconst_null
    //   456: astore 6
    //   458: goto -86 -> 372
    //   461: astore 5
    //   463: aload 4
    //   465: astore 6
    //   467: goto -95 -> 372
    //   470: astore_3
    //   471: aconst_null
    //   472: astore 4
    //   474: goto -198 -> 276
    //
    // Exception table:
    //   from	to	target	type
    //   34	222	271	android/database/sqlite/SQLiteException
    //   227	252	271	android/database/sqlite/SQLiteException
    //   252	259	271	android/database/sqlite/SQLiteException
    //   259	268	271	android/database/sqlite/SQLiteException
    //   308	330	271	android/database/sqlite/SQLiteException
    //   335	360	271	android/database/sqlite/SQLiteException
    //   360	367	271	android/database/sqlite/SQLiteException
    //   392	400	271	android/database/sqlite/SQLiteException
    //   400	407	271	android/database/sqlite/SQLiteException
    //   410	419	271	android/database/sqlite/SQLiteException
    //   34	222	370	finally
    //   227	252	370	finally
    //   252	259	370	finally
    //   259	268	370	finally
    //   308	330	370	finally
    //   335	360	370	finally
    //   360	367	370	finally
    //   392	400	370	finally
    //   400	407	370	finally
    //   410	419	370	finally
    //   8	30	453	finally
    //   276	293	461	finally
    //   8	30	470	android/database/sqlite/SQLiteException
  }

  // ERROR //
  private static Item b(long paramLong, SQLiteDatabase paramSQLiteDatabase)
  {
    // Byte code:
    //   0: iconst_1
    //   1: anewarray 49	java/lang/String
    //   4: astore 8
    //   6: aload 8
    //   8: iconst_0
    //   9: lload_0
    //   10: invokestatic 216	java/lang/Long:toString	(J)Ljava/lang/String;
    //   13: aastore
    //   14: aload_2
    //   15: ldc_w 518
    //   18: aconst_null
    //   19: ldc 220
    //   21: aload 8
    //   23: aconst_null
    //   24: aconst_null
    //   25: aconst_null
    //   26: invokevirtual 226	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   29: astore 9
    //   31: aload 9
    //   33: astore 4
    //   35: aload 4
    //   37: invokeinterface 232 1 0
    //   42: ifeq +132 -> 174
    //   45: new 520	com/google/android/apps/analytics/Item$Builder
    //   48: dup
    //   49: aload 4
    //   51: aload 4
    //   53: ldc 137
    //   55: invokeinterface 238 2 0
    //   60: invokeinterface 242 2 0
    //   65: aload 4
    //   67: aload 4
    //   69: ldc 157
    //   71: invokeinterface 238 2 0
    //   76: invokeinterface 242 2 0
    //   81: aload 4
    //   83: aload 4
    //   85: ldc 163
    //   87: invokeinterface 238 2 0
    //   92: invokeinterface 246 2 0
    //   97: aload 4
    //   99: aload 4
    //   101: ldc 167
    //   103: invokeinterface 238 2 0
    //   108: invokeinterface 475 2 0
    //   113: invokespecial 523	com/google/android/apps/analytics/Item$Builder:<init>	(Ljava/lang/String;Ljava/lang/String;DJ)V
    //   116: aload 4
    //   118: aload 4
    //   120: ldc 159
    //   122: invokeinterface 238 2 0
    //   127: invokeinterface 242 2 0
    //   132: invokevirtual 527	com/google/android/apps/analytics/Item$Builder:setItemName	(Ljava/lang/String;)Lcom/google/android/apps/analytics/Item$Builder;
    //   135: aload 4
    //   137: aload 4
    //   139: ldc 161
    //   141: invokeinterface 238 2 0
    //   146: invokeinterface 242 2 0
    //   151: invokevirtual 530	com/google/android/apps/analytics/Item$Builder:setItemCategory	(Ljava/lang/String;)Lcom/google/android/apps/analytics/Item$Builder;
    //   154: invokevirtual 533	com/google/android/apps/analytics/Item$Builder:build	()Lcom/google/android/apps/analytics/Item;
    //   157: astore 10
    //   159: aload 4
    //   161: ifnull +10 -> 171
    //   164: aload 4
    //   166: invokeinterface 267 1 0
    //   171: aload 10
    //   173: areturn
    //   174: aload 4
    //   176: ifnull +10 -> 186
    //   179: aload 4
    //   181: invokeinterface 267 1 0
    //   186: aconst_null
    //   187: areturn
    //   188: astore 5
    //   190: aconst_null
    //   191: astore 6
    //   193: ldc_w 269
    //   196: aload 5
    //   198: invokevirtual 270	android/database/sqlite/SQLiteException:toString	()Ljava/lang/String;
    //   201: invokestatic 275	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   204: pop
    //   205: aload 6
    //   207: ifnull -21 -> 186
    //   210: aload 6
    //   212: invokeinterface 267 1 0
    //   217: goto -31 -> 186
    //   220: astore_3
    //   221: aconst_null
    //   222: astore 4
    //   224: aload 4
    //   226: ifnull +10 -> 236
    //   229: aload 4
    //   231: invokeinterface 267 1 0
    //   236: aload_3
    //   237: athrow
    //   238: astore_3
    //   239: goto -15 -> 224
    //   242: astore_3
    //   243: aload 6
    //   245: astore 4
    //   247: goto -23 -> 224
    //   250: astore 5
    //   252: aload 4
    //   254: astore 6
    //   256: goto -63 -> 193
    //
    // Exception table:
    //   from	to	target	type
    //   0	31	188	android/database/sqlite/SQLiteException
    //   0	31	220	finally
    //   35	159	238	finally
    //   193	205	242	finally
    //   35	159	250	android/database/sqlite/SQLiteException
  }

  private static String b(String paramString)
  {
    if (paramString == null)
      return null;
    if ((paramString.contains("=")) || (paramString.contains("%3D")));
    Map localMap;
    while (true)
    {
      try
      {
        String str2 = URLDecoder.decode(paramString, "UTF-8");
        paramString = str2;
        localMap = l.a(paramString);
        if (localMap.get("utm_campaign") == null)
          break label137;
        i1 = 1;
        if (localMap.get("utm_medium") == null)
          break label142;
        i2 = 1;
        if (localMap.get("utm_source") == null)
          break label147;
        i3 = 1;
        if (localMap.get("gclid") == null)
          break label153;
        i4 = 1;
        if ((i4 != 0) || ((i1 != 0) && (i2 != 0) && (i3 != 0)))
          break;
        Log.w("GoogleAnalyticsTracker", "Badly formatted referrer missing campaign, medium and source or click ID");
        return null;
      }
      catch (UnsupportedEncodingException localUnsupportedEncodingException)
      {
        return null;
      }
      return null;
      label137: int i1 = 0;
      continue;
      label142: int i2 = 0;
      continue;
      label147: int i3 = 0;
      continue;
      label153: int i4 = 0;
    }
    String[][] arrayOfString; = new String[7][];
    String[] arrayOfString1 = new String[2];
    arrayOfString1[0] = "utmcid";
    arrayOfString1[1] = ((String)localMap.get("utm_id"));
    arrayOfString;[0] = arrayOfString1;
    String[] arrayOfString2 = new String[2];
    arrayOfString2[0] = "utmcsr";
    arrayOfString2[1] = ((String)localMap.get("utm_source"));
    arrayOfString;[1] = arrayOfString2;
    String[] arrayOfString3 = new String[2];
    arrayOfString3[0] = "utmgclid";
    arrayOfString3[1] = ((String)localMap.get("gclid"));
    arrayOfString;[2] = arrayOfString3;
    String[] arrayOfString4 = new String[2];
    arrayOfString4[0] = "utmccn";
    arrayOfString4[1] = ((String)localMap.get("utm_campaign"));
    arrayOfString;[3] = arrayOfString4;
    String[] arrayOfString5 = new String[2];
    arrayOfString5[0] = "utmcmd";
    arrayOfString5[1] = ((String)localMap.get("utm_medium"));
    arrayOfString;[4] = arrayOfString5;
    String[] arrayOfString6 = new String[2];
    arrayOfString6[0] = "utmctr";
    arrayOfString6[1] = ((String)localMap.get("utm_term"));
    arrayOfString;[5] = arrayOfString6;
    String[] arrayOfString7 = new String[2];
    arrayOfString7[0] = "utmcct";
    arrayOfString7[1] = ((String)localMap.get("utm_content"));
    arrayOfString;[6] = arrayOfString7;
    StringBuilder localStringBuilder = new StringBuilder();
    int i5 = 0;
    int i6 = 1;
    if (i5 < arrayOfString;.length)
    {
      String str1;
      if (arrayOfString;[i5][1] != null)
      {
        str1 = arrayOfString;[i5][1].replace("+", "%20").replace(" ", "%20");
        if (i6 == 0)
          break label510;
        i6 = 0;
      }
      while (true)
      {
        localStringBuilder.append(arrayOfString;[i5][0]).append("=").append(str1);
        i5++;
        break;
        label510: localStringBuilder.append("|");
      }
    }
    return localStringBuilder.toString();
  }

  // ERROR //
  private static b c(long paramLong, SQLiteDatabase paramSQLiteDatabase)
  {
    // Byte code:
    //   0: new 278	com/google/android/apps/analytics/b
    //   3: dup
    //   4: invokespecial 279	com/google/android/apps/analytics/b:<init>	()V
    //   7: astore_3
    //   8: iconst_1
    //   9: anewarray 49	java/lang/String
    //   12: astore 8
    //   14: aload 8
    //   16: iconst_0
    //   17: lload_0
    //   18: invokestatic 216	java/lang/Long:toString	(J)Ljava/lang/String;
    //   21: aastore
    //   22: aload_2
    //   23: ldc_w 610
    //   26: aconst_null
    //   27: ldc 220
    //   29: aload 8
    //   31: aconst_null
    //   32: aconst_null
    //   33: aconst_null
    //   34: invokevirtual 226	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   37: astore 9
    //   39: aload 9
    //   41: astore 5
    //   43: aload 5
    //   45: invokeinterface 290 1 0
    //   50: ifeq +109 -> 159
    //   53: aload_3
    //   54: new 292	com/google/android/apps/analytics/a
    //   57: dup
    //   58: aload 5
    //   60: aload 5
    //   62: ldc 115
    //   64: invokeinterface 238 2 0
    //   69: invokeinterface 296 2 0
    //   74: aload 5
    //   76: aload 5
    //   78: ldc 119
    //   80: invokeinterface 238 2 0
    //   85: invokeinterface 242 2 0
    //   90: aload 5
    //   92: aload 5
    //   94: ldc 121
    //   96: invokeinterface 238 2 0
    //   101: invokeinterface 242 2 0
    //   106: aload 5
    //   108: aload 5
    //   110: ldc 123
    //   112: invokeinterface 238 2 0
    //   117: invokeinterface 296 2 0
    //   122: invokespecial 299	com/google/android/apps/analytics/a:<init>	(ILjava/lang/String;Ljava/lang/String;I)V
    //   125: invokevirtual 302	com/google/android/apps/analytics/b:a	(Lcom/google/android/apps/analytics/a;)V
    //   128: goto -85 -> 43
    //   131: astore 4
    //   133: ldc_w 269
    //   136: aload 4
    //   138: invokevirtual 270	android/database/sqlite/SQLiteException:toString	()Ljava/lang/String;
    //   141: invokestatic 275	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   144: pop
    //   145: aload 5
    //   147: ifnull +10 -> 157
    //   150: aload 5
    //   152: invokeinterface 267 1 0
    //   157: aload_3
    //   158: areturn
    //   159: aload 5
    //   161: ifnull -4 -> 157
    //   164: aload 5
    //   166: invokeinterface 267 1 0
    //   171: aload_3
    //   172: areturn
    //   173: astore 6
    //   175: aconst_null
    //   176: astore 5
    //   178: aload 5
    //   180: ifnull +10 -> 190
    //   183: aload 5
    //   185: invokeinterface 267 1 0
    //   190: aload 6
    //   192: athrow
    //   193: astore 6
    //   195: goto -17 -> 178
    //   198: astore 4
    //   200: aconst_null
    //   201: astore 5
    //   203: goto -70 -> 133
    //
    // Exception table:
    //   from	to	target	type
    //   43	128	131	android/database/sqlite/SQLiteException
    //   8	39	173	finally
    //   43	128	193	finally
    //   133	145	193	finally
    //   8	39	198	android/database/sqlite/SQLiteException
  }

  private static boolean d(SQLiteDatabase paramSQLiteDatabase)
  {
    try
    {
      paramSQLiteDatabase.endTransaction();
      return true;
    }
    catch (SQLiteException localSQLiteException)
    {
      Log.e("GoogleAnalyticsTracker", "exception ending transaction:" + localSQLiteException.toString());
    }
    return false;
  }

  // ERROR //
  private static k e(SQLiteDatabase paramSQLiteDatabase)
  {
    // Byte code:
    //   0: aload_0
    //   1: ldc_w 449
    //   4: iconst_4
    //   5: anewarray 49	java/lang/String
    //   8: dup
    //   9: iconst_0
    //   10: ldc_w 449
    //   13: aastore
    //   14: dup
    //   15: iconst_1
    //   16: ldc_w 617
    //   19: aastore
    //   20: dup
    //   21: iconst_2
    //   22: ldc_w 619
    //   25: aastore
    //   26: dup
    //   27: iconst_3
    //   28: ldc_w 621
    //   31: aastore
    //   32: aconst_null
    //   33: aconst_null
    //   34: aconst_null
    //   35: aconst_null
    //   36: aconst_null
    //   37: invokevirtual 226	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   40: astore 6
    //   42: aload 6
    //   44: astore_2
    //   45: aload_2
    //   46: invokeinterface 232 1 0
    //   51: ifeq +159 -> 210
    //   54: aload_2
    //   55: aload_2
    //   56: ldc_w 617
    //   59: invokeinterface 238 2 0
    //   64: invokeinterface 475 2 0
    //   69: lstore 8
    //   71: aload_2
    //   72: aload_2
    //   73: ldc_w 619
    //   76: invokeinterface 238 2 0
    //   81: invokeinterface 296 2 0
    //   86: istore 10
    //   88: aload_2
    //   89: aload_2
    //   90: ldc_w 621
    //   93: invokeinterface 238 2 0
    //   98: invokeinterface 296 2 0
    //   103: istore 11
    //   105: new 623	com/google/android/apps/analytics/k
    //   108: dup
    //   109: aload_2
    //   110: aload_2
    //   111: ldc_w 449
    //   114: invokeinterface 238 2 0
    //   119: invokeinterface 242 2 0
    //   124: lload 8
    //   126: iload 10
    //   128: iload 11
    //   130: invokespecial 626	com/google/android/apps/analytics/k:<init>	(Ljava/lang/String;JII)V
    //   133: astore 7
    //   135: aload_2
    //   136: ifnull +9 -> 145
    //   139: aload_2
    //   140: invokeinterface 267 1 0
    //   145: aload 7
    //   147: areturn
    //   148: astore_3
    //   149: aconst_null
    //   150: astore 4
    //   152: ldc_w 269
    //   155: aload_3
    //   156: invokevirtual 270	android/database/sqlite/SQLiteException:toString	()Ljava/lang/String;
    //   159: invokestatic 275	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   162: pop
    //   163: aload 4
    //   165: ifnull +10 -> 175
    //   168: aload 4
    //   170: invokeinterface 267 1 0
    //   175: aconst_null
    //   176: areturn
    //   177: astore_1
    //   178: aconst_null
    //   179: astore_2
    //   180: aload_2
    //   181: ifnull +9 -> 190
    //   184: aload_2
    //   185: invokeinterface 267 1 0
    //   190: aload_1
    //   191: athrow
    //   192: astore_1
    //   193: goto -13 -> 180
    //   196: astore_1
    //   197: aload 4
    //   199: astore_2
    //   200: goto -20 -> 180
    //   203: astore_3
    //   204: aload_2
    //   205: astore 4
    //   207: goto -55 -> 152
    //   210: aconst_null
    //   211: astore 7
    //   213: goto -78 -> 135
    //
    // Exception table:
    //   from	to	target	type
    //   0	42	148	android/database/sqlite/SQLiteException
    //   0	42	177	finally
    //   45	135	192	finally
    //   152	163	196	finally
    //   45	135	203	android/database/sqlite/SQLiteException
  }

  private k f(SQLiteDatabase paramSQLiteDatabase)
  {
    k localk = e(paramSQLiteDatabase);
    if (localk == null);
    int i1;
    String str;
    ContentValues localContentValues;
    do
    {
      return null;
      if (localk.b() != 0L)
        return localk;
      i1 = localk.d();
      str = localk.a();
      localContentValues = new ContentValues();
      localContentValues.put("referrer", str);
      localContentValues.put("timestamp_referrer", Long.valueOf(this.e));
      localContentValues.put("referrer_visit", Integer.valueOf(this.f));
      localContentValues.put("referrer_index", Integer.valueOf(i1));
    }
    while (!a(paramSQLiteDatabase, localContentValues));
    return new k(str, this.e, this.f, i1);
  }

  // ERROR //
  private Hit[] m()
  {
    // Byte code:
    //   0: new 465	java/util/ArrayList
    //   3: dup
    //   4: invokespecial 466	java/util/ArrayList:<init>	()V
    //   7: astore_1
    //   8: aload_0
    //   9: getfield 201	com/google/android/apps/analytics/i:a	Lcom/google/android/apps/analytics/i$a;
    //   12: invokevirtual 640	com/google/android/apps/analytics/i$a:getReadableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   15: ldc_w 434
    //   18: aconst_null
    //   19: aconst_null
    //   20: aconst_null
    //   21: aconst_null
    //   22: aconst_null
    //   23: ldc 173
    //   25: sipush 1000
    //   28: invokestatic 283	java/lang/Integer:toString	(I)Ljava/lang/String;
    //   31: invokevirtual 471	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   34: astore 7
    //   36: aload 7
    //   38: astore_3
    //   39: aload_3
    //   40: invokeinterface 290 1 0
    //   45: ifeq +65 -> 110
    //   48: aload_1
    //   49: new 642	com/google/android/apps/analytics/Hit
    //   52: dup
    //   53: aload_3
    //   54: iconst_1
    //   55: invokeinterface 242 2 0
    //   60: aload_3
    //   61: iconst_0
    //   62: invokeinterface 475 2 0
    //   67: invokespecial 645	com/google/android/apps/analytics/Hit:<init>	(Ljava/lang/String;J)V
    //   70: invokeinterface 496 2 0
    //   75: pop
    //   76: goto -37 -> 39
    //   79: astore_2
    //   80: ldc_w 269
    //   83: aload_2
    //   84: invokevirtual 270	android/database/sqlite/SQLiteException:toString	()Ljava/lang/String;
    //   87: invokestatic 275	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   90: pop
    //   91: iconst_0
    //   92: anewarray 642	com/google/android/apps/analytics/Hit
    //   95: astore 6
    //   97: aload_3
    //   98: ifnull +9 -> 107
    //   101: aload_3
    //   102: invokeinterface 267 1 0
    //   107: aload 6
    //   109: areturn
    //   110: aload_3
    //   111: ifnull +9 -> 120
    //   114: aload_3
    //   115: invokeinterface 267 1 0
    //   120: aload_1
    //   121: aload_1
    //   122: invokeinterface 510 1 0
    //   127: anewarray 642	com/google/android/apps/analytics/Hit
    //   130: invokeinterface 514 2 0
    //   135: checkcast 647	[Lcom/google/android/apps/analytics/Hit;
    //   138: areturn
    //   139: astore 4
    //   141: aconst_null
    //   142: astore_3
    //   143: aload_3
    //   144: ifnull +9 -> 153
    //   147: aload_3
    //   148: invokeinterface 267 1 0
    //   153: aload 4
    //   155: athrow
    //   156: astore 4
    //   158: goto -15 -> 143
    //   161: astore_2
    //   162: aconst_null
    //   163: astore_3
    //   164: goto -84 -> 80
    //
    // Exception table:
    //   from	to	target	type
    //   39	76	79	android/database/sqlite/SQLiteException
    //   8	36	139	finally
    //   39	76	156	finally
    //   80	97	156	finally
    //   8	36	161	android/database/sqlite/SQLiteException
  }

  private b n()
  {
    try
    {
      b localb = a(this.a.getReadableDatabase());
      return localb;
    }
    catch (SQLiteException localSQLiteException)
    {
      Log.e("GoogleAnalyticsTracker", localSQLiteException.toString());
    }
    return new b();
  }

  private int o()
  {
    Cursor localCursor = null;
    try
    {
      localCursor = this.a.getReadableDatabase().rawQuery("SELECT COUNT(*) from hits", null);
      boolean bool = localCursor.moveToFirst();
      i1 = 0;
      if (bool)
      {
        long l1 = localCursor.getLong(0);
        i1 = (int)l1;
      }
      return i1;
    }
    catch (SQLiteException localSQLiteException)
    {
      Log.e("GoogleAnalyticsTracker", localSQLiteException.toString());
      int i1 = 0;
      return 0;
    }
    finally
    {
      if (localCursor != null)
        localCursor.close();
    }
  }

  private void p()
  {
    try
    {
      b(this.a.getWritableDatabase());
      return;
    }
    catch (SQLiteException localSQLiteException)
    {
      Log.e("GoogleAnalyticsTracker", localSQLiteException.toString());
    }
  }

  private void q()
  {
    SQLiteDatabase localSQLiteDatabase = this.a.getWritableDatabase();
    localSQLiteDatabase.delete("session", null, null);
    if (this.c == 0L)
    {
      long l1 = System.currentTimeMillis() / 1000L;
      this.c = l1;
      this.d = l1;
      this.e = l1;
    }
    for (this.f = 1; ; this.f = (1 + this.f))
    {
      ContentValues localContentValues = new ContentValues();
      localContentValues.put("timestamp_first", Long.valueOf(this.c));
      localContentValues.put("timestamp_previous", Long.valueOf(this.d));
      localContentValues.put("timestamp_current", Long.valueOf(this.e));
      localContentValues.put("visits", Integer.valueOf(this.f));
      localContentValues.put("store_id", Integer.valueOf(this.b));
      localSQLiteDatabase.insert("session", null, localContentValues);
      this.h = true;
      return;
      this.d = this.e;
      this.e = (System.currentTimeMillis() / 1000L);
      if (this.e == this.d)
        this.e = (1L + this.e);
    }
  }

  public final String a(int paramInt)
  {
    a locala = this.m.b(paramInt);
    if ((locala == null) || (locala.a() != 1))
      return null;
    return locala.c();
  }

  public final void a(long paramLong)
  {
    try
    {
      int i1 = this.g;
      SQLiteDatabase localSQLiteDatabase = this.a.getWritableDatabase();
      String[] arrayOfString = new String[1];
      arrayOfString[0] = Long.toString(paramLong);
      this.g = (i1 - localSQLiteDatabase.delete("hits", "hit_id = ?", arrayOfString));
      return;
    }
    catch (SQLiteException localSQLiteException)
    {
      while (true)
        Log.e("GoogleAnalyticsTracker", localSQLiteException.toString());
    }
    finally
    {
    }
  }

  // ERROR //
  public final void a(c paramc)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 440	com/google/android/apps/analytics/i:g	I
    //   4: sipush 1000
    //   7: if_icmplt +14 -> 21
    //   10: ldc_w 269
    //   13: ldc_w 673
    //   16: invokestatic 488	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
    //   19: pop
    //   20: return
    //   21: aload_0
    //   22: getfield 189	com/google/android/apps/analytics/i:k	I
    //   25: bipush 100
    //   27: if_icmpeq +62 -> 89
    //   30: aload_1
    //   31: invokevirtual 392	com/google/android/apps/analytics/c:g	()I
    //   34: iconst_m1
    //   35: if_icmpne +45 -> 80
    //   38: aload_0
    //   39: getfield 394	com/google/android/apps/analytics/i:b	I
    //   42: istore 12
    //   44: iload 12
    //   46: sipush 10000
    //   49: irem
    //   50: bipush 100
    //   52: aload_0
    //   53: getfield 189	com/google/android/apps/analytics/i:k	I
    //   56: imul
    //   57: if_icmplt +32 -> 89
    //   60: invokestatic 679	com/google/android/apps/analytics/GoogleAnalyticsTracker:getInstance	()Lcom/google/android/apps/analytics/GoogleAnalyticsTracker;
    //   63: invokevirtual 682	com/google/android/apps/analytics/GoogleAnalyticsTracker:getDebug	()Z
    //   66: ifeq -46 -> 20
    //   69: ldc_w 269
    //   72: ldc_w 684
    //   75: invokestatic 687	android/util/Log:v	(Ljava/lang/String;Ljava/lang/String;)I
    //   78: pop
    //   79: return
    //   80: aload_1
    //   81: invokevirtual 392	com/google/android/apps/analytics/c:g	()I
    //   84: istore 12
    //   86: goto -42 -> 44
    //   89: aload_0
    //   90: monitorenter
    //   91: aload_0
    //   92: getfield 201	com/google/android/apps/analytics/i:a	Lcom/google/android/apps/analytics/i$a;
    //   95: invokevirtual 658	com/google/android/apps/analytics/i$a:getWritableDatabase	()Landroid/database/sqlite/SQLiteDatabase;
    //   98: astore 5
    //   100: aload 5
    //   102: invokevirtual 447	android/database/sqlite/SQLiteDatabase:beginTransaction	()V
    //   105: aload_0
    //   106: getfield 667	com/google/android/apps/analytics/i:h	Z
    //   109: ifne +7 -> 116
    //   112: aload_0
    //   113: invokespecial 689	com/google/android/apps/analytics/i:q	()V
    //   116: aload_0
    //   117: aload_1
    //   118: aload 5
    //   120: iconst_1
    //   121: invokespecial 443	com/google/android/apps/analytics/i:a	(Lcom/google/android/apps/analytics/c;Landroid/database/sqlite/SQLiteDatabase;Z)V
    //   124: aload 5
    //   126: invokevirtual 456	android/database/sqlite/SQLiteDatabase:setTransactionSuccessful	()V
    //   129: aload 5
    //   131: invokevirtual 459	android/database/sqlite/SQLiteDatabase:inTransaction	()Z
    //   134: ifeq +9 -> 143
    //   137: aload 5
    //   139: invokestatic 462	com/google/android/apps/analytics/i:d	(Landroid/database/sqlite/SQLiteDatabase;)Z
    //   142: pop
    //   143: aload_0
    //   144: monitorexit
    //   145: return
    //   146: astore 4
    //   148: aload_0
    //   149: monitorexit
    //   150: aload 4
    //   152: athrow
    //   153: astore_2
    //   154: ldc_w 269
    //   157: new 37	java/lang/StringBuilder
    //   160: dup
    //   161: ldc_w 691
    //   164: invokespecial 43	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   167: aload_2
    //   168: invokevirtual 270	android/database/sqlite/SQLiteException:toString	()Ljava/lang/String;
    //   171: invokevirtual 57	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   174: invokevirtual 97	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   177: invokestatic 275	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   180: pop
    //   181: aload_0
    //   182: monitorexit
    //   183: return
    //   184: astore 8
    //   186: ldc_w 269
    //   189: new 37	java/lang/StringBuilder
    //   192: dup
    //   193: ldc_w 693
    //   196: invokespecial 43	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   199: aload 8
    //   201: invokevirtual 270	android/database/sqlite/SQLiteException:toString	()Ljava/lang/String;
    //   204: invokevirtual 57	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   207: invokevirtual 97	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   210: invokestatic 275	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   213: pop
    //   214: aload 5
    //   216: invokevirtual 459	android/database/sqlite/SQLiteDatabase:inTransaction	()Z
    //   219: ifeq -76 -> 143
    //   222: aload 5
    //   224: invokestatic 462	com/google/android/apps/analytics/i:d	(Landroid/database/sqlite/SQLiteDatabase;)Z
    //   227: pop
    //   228: goto -85 -> 143
    //   231: astore 6
    //   233: aload 5
    //   235: invokevirtual 459	android/database/sqlite/SQLiteDatabase:inTransaction	()Z
    //   238: ifeq +9 -> 247
    //   241: aload 5
    //   243: invokestatic 462	com/google/android/apps/analytics/i:d	(Landroid/database/sqlite/SQLiteDatabase;)Z
    //   246: pop
    //   247: aload 6
    //   249: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   91	100	146	finally
    //   129	143	146	finally
    //   143	145	146	finally
    //   154	183	146	finally
    //   214	228	146	finally
    //   233	247	146	finally
    //   247	250	146	finally
    //   91	100	153	android/database/sqlite/SQLiteException
    //   100	116	184	android/database/sqlite/SQLiteException
    //   116	129	184	android/database/sqlite/SQLiteException
    //   100	116	231	finally
    //   116	129	231	finally
    //   186	214	231	finally
  }

  public final void a(boolean paramBoolean)
  {
    this.j = paramBoolean;
  }

  public final boolean a(String paramString)
  {
    String str = b(paramString);
    if (str == null)
      return false;
    while (true)
    {
      try
      {
        SQLiteDatabase localSQLiteDatabase = this.a.getWritableDatabase();
        k localk = e(localSQLiteDatabase);
        ContentValues localContentValues = new ContentValues();
        localContentValues.put("referrer", str);
        localContentValues.put("timestamp_referrer", Long.valueOf(0L));
        localContentValues.put("referrer_visit", Integer.valueOf(0));
        if (localk == null)
          break label143;
        l1 = localk.d();
        if (localk.b() > 0L)
          l1 += 1L;
        localContentValues.put("referrer_index", Long.valueOf(l1));
        if (a(localSQLiteDatabase, localContentValues))
        {
          e();
          return true;
        }
      }
      catch (SQLiteException localSQLiteException)
      {
        Log.e("GoogleAnalyticsTracker", localSQLiteException.toString());
        return false;
      }
      return false;
      label143: long l1 = 1L;
    }
  }

  public final Hit[] a()
  {
    return m();
  }

  public final int b()
  {
    return this.g;
  }

  public final void b(int paramInt)
  {
    this.k = paramInt;
  }

  // ERROR //
  public final void b(SQLiteDatabase paramSQLiteDatabase)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc_w 663
    //   4: aconst_null
    //   5: aconst_null
    //   6: aconst_null
    //   7: aconst_null
    //   8: aconst_null
    //   9: aconst_null
    //   10: invokevirtual 226	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   13: astore 6
    //   15: aload 6
    //   17: astore_3
    //   18: aload_3
    //   19: invokeinterface 232 1 0
    //   24: ifeq +114 -> 138
    //   27: aload_0
    //   28: aload_3
    //   29: iconst_0
    //   30: invokeinterface 475 2 0
    //   35: putfield 372	com/google/android/apps/analytics/i:c	J
    //   38: aload_0
    //   39: aload_3
    //   40: iconst_1
    //   41: invokeinterface 475 2 0
    //   46: putfield 375	com/google/android/apps/analytics/i:d	J
    //   49: aload_0
    //   50: aload_3
    //   51: iconst_2
    //   52: invokeinterface 475 2 0
    //   57: putfield 379	com/google/android/apps/analytics/i:e	J
    //   60: aload_0
    //   61: aload_3
    //   62: iconst_3
    //   63: invokeinterface 296 2 0
    //   68: putfield 383	com/google/android/apps/analytics/i:f	I
    //   71: aload_0
    //   72: aload_3
    //   73: iconst_4
    //   74: invokeinterface 296 2 0
    //   79: putfield 394	com/google/android/apps/analytics/i:b	I
    //   82: aload_1
    //   83: invokestatic 628	com/google/android/apps/analytics/i:e	(Landroid/database/sqlite/SQLiteDatabase;)Lcom/google/android/apps/analytics/k;
    //   86: astore 10
    //   88: aload_0
    //   89: getfield 372	com/google/android/apps/analytics/i:c	J
    //   92: lconst_0
    //   93: lcmp
    //   94: ifeq +38 -> 132
    //   97: aload 10
    //   99: ifnull +207 -> 306
    //   102: aload 10
    //   104: invokevirtual 630	com/google/android/apps/analytics/k:b	()J
    //   107: lconst_0
    //   108: lcmp
    //   109: ifeq +23 -> 132
    //   112: goto +194 -> 306
    //   115: aload_0
    //   116: iload 11
    //   118: putfield 667	com/google/android/apps/analytics/i:h	Z
    //   121: aload_3
    //   122: ifnull +9 -> 131
    //   125: aload_3
    //   126: invokeinterface 267 1 0
    //   131: return
    //   132: iconst_0
    //   133: istore 11
    //   135: goto -20 -> 115
    //   138: aload_0
    //   139: iconst_0
    //   140: putfield 667	com/google/android/apps/analytics/i:h	Z
    //   143: aload_0
    //   144: iconst_1
    //   145: putfield 320	com/google/android/apps/analytics/i:i	Z
    //   148: aload_0
    //   149: ldc_w 365
    //   152: new 702	java/security/SecureRandom
    //   155: dup
    //   156: invokespecial 703	java/security/SecureRandom:<init>	()V
    //   159: invokevirtual 705	java/security/SecureRandom:nextInt	()I
    //   162: iand
    //   163: putfield 394	com/google/android/apps/analytics/i:b	I
    //   166: aload_3
    //   167: invokeinterface 267 1 0
    //   172: new 331	android/content/ContentValues
    //   175: dup
    //   176: invokespecial 332	android/content/ContentValues:<init>	()V
    //   179: astore 7
    //   181: aload 7
    //   183: ldc 69
    //   185: lconst_0
    //   186: invokestatic 429	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   189: invokevirtual 432	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Long;)V
    //   192: aload 7
    //   194: ldc 71
    //   196: lconst_0
    //   197: invokestatic 429	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   200: invokevirtual 432	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Long;)V
    //   203: aload 7
    //   205: ldc 73
    //   207: lconst_0
    //   208: invokestatic 429	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   211: invokevirtual 432	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Long;)V
    //   214: aload 7
    //   216: ldc 75
    //   218: iconst_0
    //   219: invokestatic 336	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   222: invokevirtual 340	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
    //   225: aload 7
    //   227: ldc 107
    //   229: aload_0
    //   230: getfield 394	com/google/android/apps/analytics/i:b	I
    //   233: invokestatic 336	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   236: invokevirtual 340	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
    //   239: aload_1
    //   240: ldc_w 663
    //   243: aconst_null
    //   244: aload 7
    //   246: invokevirtual 438	android/database/sqlite/SQLiteDatabase:insert	(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    //   249: pop2
    //   250: aconst_null
    //   251: astore_3
    //   252: goto -131 -> 121
    //   255: astore 4
    //   257: aconst_null
    //   258: astore_3
    //   259: ldc_w 269
    //   262: aload 4
    //   264: invokevirtual 270	android/database/sqlite/SQLiteException:toString	()Ljava/lang/String;
    //   267: invokestatic 275	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   270: pop
    //   271: aload_3
    //   272: ifnull -141 -> 131
    //   275: aload_3
    //   276: invokeinterface 267 1 0
    //   281: return
    //   282: astore_2
    //   283: aconst_null
    //   284: astore_3
    //   285: aload_3
    //   286: ifnull +9 -> 295
    //   289: aload_3
    //   290: invokeinterface 267 1 0
    //   295: aload_2
    //   296: athrow
    //   297: astore_2
    //   298: goto -13 -> 285
    //   301: astore 4
    //   303: goto -44 -> 259
    //   306: iconst_1
    //   307: istore 11
    //   309: goto -194 -> 115
    //
    // Exception table:
    //   from	to	target	type
    //   0	15	255	android/database/sqlite/SQLiteException
    //   172	250	255	android/database/sqlite/SQLiteException
    //   0	15	282	finally
    //   172	250	282	finally
    //   18	97	297	finally
    //   102	112	297	finally
    //   115	121	297	finally
    //   138	172	297	finally
    //   259	271	297	finally
    //   18	97	301	android/database/sqlite/SQLiteException
    //   102	112	301	android/database/sqlite/SQLiteException
    //   115	121	301	android/database/sqlite/SQLiteException
    //   138	172	301	android/database/sqlite/SQLiteException
  }

  public final String c()
  {
    if (!this.h)
      return null;
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = Integer.valueOf(this.b);
    arrayOfObject[1] = Long.valueOf(this.c);
    return String.format("%d.%d", arrayOfObject);
  }

  public final String d()
  {
    if (!this.h)
      return null;
    return Integer.toString((int)this.e);
  }

  public final void e()
  {
    try
    {
      this.h = false;
      this.i = true;
      this.g = o();
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  static final class a extends SQLiteOpenHelper
  {
    private final int a = 5;
    private final i b;

    a(Context paramContext, String paramString, int paramInt, i parami)
    {
      super(paramString, null, 5);
      this.b = parami;
    }

    private static void a(SQLiteDatabase paramSQLiteDatabase)
    {
      paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS custom_variables;");
      paramSQLiteDatabase.execSQL(i.h());
      paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS custom_var_cache;");
      paramSQLiteDatabase.execSQL(i.i());
      for (int i = 1; i <= 5; i++)
      {
        ContentValues localContentValues = new ContentValues();
        localContentValues.put("event_id", Integer.valueOf(0));
        localContentValues.put("cv_index", Integer.valueOf(i));
        localContentValues.put("cv_name", "");
        localContentValues.put("cv_scope", Integer.valueOf(3));
        localContentValues.put("cv_value", "");
        paramSQLiteDatabase.insert("custom_var_cache", "event_id", localContentValues);
      }
    }

    private void a(SQLiteDatabase paramSQLiteDatabase, int paramInt)
    {
      this.b.b(paramSQLiteDatabase);
      i locali = this.b;
      i.a(locali, i.a(paramSQLiteDatabase));
      c[] arrayOfc = i.a(paramSQLiteDatabase, paramInt);
      for (int i = 0; i < arrayOfc.length; i++)
        i.a(this.b, arrayOfc[i], paramSQLiteDatabase);
      paramSQLiteDatabase.execSQL("DELETE from events;");
      paramSQLiteDatabase.execSQL("DELETE from item_events;");
      paramSQLiteDatabase.execSQL("DELETE from transaction_events;");
      paramSQLiteDatabase.execSQL("DELETE from custom_variables;");
    }

    private static void b(SQLiteDatabase paramSQLiteDatabase)
    {
      paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS transaction_events;");
      paramSQLiteDatabase.execSQL(i.j());
      paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS item_events;");
      paramSQLiteDatabase.execSQL(i.k());
    }

    private static void c(SQLiteDatabase paramSQLiteDatabase)
    {
      paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS hits;");
      paramSQLiteDatabase.execSQL(i.l());
    }

    private static void d(SQLiteDatabase paramSQLiteDatabase)
    {
      paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS referrer;");
      paramSQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS referrer (referrer TEXT PRIMARY KEY NOT NULL,timestamp_referrer INTEGER NOT NULL,referrer_visit INTEGER NOT NULL DEFAULT 1,referrer_index INTEGER NOT NULL DEFAULT 1);");
    }

    // ERROR //
    private static void e(SQLiteDatabase paramSQLiteDatabase)
    {
      // Byte code:
      //   0: aconst_null
      //   1: astore_1
      //   2: aload_0
      //   3: ldc 122
      //   5: iconst_1
      //   6: anewarray 124	java/lang/String
      //   9: dup
      //   10: iconst_0
      //   11: ldc 126
      //   13: aastore
      //   14: aconst_null
      //   15: aconst_null
      //   16: aconst_null
      //   17: aconst_null
      //   18: aconst_null
      //   19: invokevirtual 130	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
      //   22: astore 7
      //   24: aload 7
      //   26: astore 4
      //   28: aload 4
      //   30: invokeinterface 136 1 0
      //   35: ifeq +240 -> 275
      //   38: aload 4
      //   40: iconst_0
      //   41: invokeinterface 140 2 0
      //   46: astore 8
      //   48: aload_0
      //   49: ldc 142
      //   51: aconst_null
      //   52: aconst_null
      //   53: aconst_null
      //   54: aconst_null
      //   55: aconst_null
      //   56: aconst_null
      //   57: invokevirtual 130	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
      //   60: astore 9
      //   62: aload 9
      //   64: astore_3
      //   65: aload_3
      //   66: invokeinterface 136 1 0
      //   71: ifeq +198 -> 269
      //   74: aload_3
      //   75: iconst_0
      //   76: invokeinterface 146 2 0
      //   81: lstore 10
      //   83: new 39	android/content/ContentValues
      //   86: dup
      //   87: invokespecial 42	android/content/ContentValues:<init>	()V
      //   90: astore 12
      //   92: aload 12
      //   94: ldc 126
      //   96: aload 8
      //   98: invokevirtual 63	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
      //   101: aload 12
      //   103: ldc 148
      //   105: lload 10
      //   107: invokestatic 153	java/lang/Long:valueOf	(J)Ljava/lang/Long;
      //   110: invokevirtual 156	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Long;)V
      //   113: aload 12
      //   115: ldc 158
      //   117: iconst_1
      //   118: invokestatic 50	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
      //   121: invokevirtual 54	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
      //   124: aload 12
      //   126: ldc 160
      //   128: iconst_1
      //   129: invokestatic 50	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
      //   132: invokevirtual 54	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
      //   135: aload_0
      //   136: ldc 126
      //   138: aconst_null
      //   139: aload 12
      //   141: invokevirtual 73	android/database/sqlite/SQLiteDatabase:insert	(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
      //   144: pop2
      //   145: aload 4
      //   147: ifnull +10 -> 157
      //   150: aload 4
      //   152: invokeinterface 163 1 0
      //   157: aload_3
      //   158: ifnull +9 -> 167
      //   161: aload_3
      //   162: invokeinterface 163 1 0
      //   167: return
      //   168: astore 5
      //   170: aconst_null
      //   171: astore_3
      //   172: ldc 165
      //   174: aload 5
      //   176: invokevirtual 168	android/database/sqlite/SQLiteException:toString	()Ljava/lang/String;
      //   179: invokestatic 173	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
      //   182: pop
      //   183: aload_1
      //   184: ifnull +9 -> 193
      //   187: aload_1
      //   188: invokeinterface 163 1 0
      //   193: aload_3
      //   194: ifnull -27 -> 167
      //   197: aload_3
      //   198: invokeinterface 163 1 0
      //   203: return
      //   204: astore_2
      //   205: aconst_null
      //   206: astore_3
      //   207: aconst_null
      //   208: astore 4
      //   210: aload 4
      //   212: ifnull +10 -> 222
      //   215: aload 4
      //   217: invokeinterface 163 1 0
      //   222: aload_3
      //   223: ifnull +9 -> 232
      //   226: aload_3
      //   227: invokeinterface 163 1 0
      //   232: aload_2
      //   233: athrow
      //   234: astore_2
      //   235: aconst_null
      //   236: astore_3
      //   237: goto -27 -> 210
      //   240: astore_2
      //   241: goto -31 -> 210
      //   244: astore_2
      //   245: aload_1
      //   246: astore 4
      //   248: goto -38 -> 210
      //   251: astore 5
      //   253: aload 4
      //   255: astore_1
      //   256: aconst_null
      //   257: astore_3
      //   258: goto -86 -> 172
      //   261: astore 5
      //   263: aload 4
      //   265: astore_1
      //   266: goto -94 -> 172
      //   269: lconst_0
      //   270: lstore 10
      //   272: goto -189 -> 83
      //   275: aconst_null
      //   276: astore_3
      //   277: goto -132 -> 145
      //
      // Exception table:
      //   from	to	target	type
      //   2	24	168	android/database/sqlite/SQLiteException
      //   2	24	204	finally
      //   28	62	234	finally
      //   65	83	240	finally
      //   83	145	240	finally
      //   172	183	244	finally
      //   28	62	251	android/database/sqlite/SQLiteException
      //   65	83	261	android/database/sqlite/SQLiteException
      //   83	145	261	android/database/sqlite/SQLiteException
    }

    // ERROR //
    private static void f(SQLiteDatabase paramSQLiteDatabase)
    {
      // Byte code:
      //   0: aload_0
      //   1: ldc 126
      //   3: aconst_null
      //   4: aconst_null
      //   5: aconst_null
      //   6: aconst_null
      //   7: aconst_null
      //   8: aconst_null
      //   9: invokevirtual 130	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
      //   12: astore 8
      //   14: aload 8
      //   16: astore_2
      //   17: aload_2
      //   18: invokeinterface 178 1 0
      //   23: astore 9
      //   25: iconst_0
      //   26: istore 10
      //   28: iconst_0
      //   29: istore 11
      //   31: iconst_0
      //   32: istore 12
      //   34: iload 10
      //   36: aload 9
      //   38: arraylength
      //   39: if_icmpge +376 -> 415
      //   42: aload 9
      //   44: iload 10
      //   46: aaload
      //   47: ldc 160
      //   49: invokevirtual 182	java/lang/String:equals	(Ljava/lang/Object;)Z
      //   52: ifeq +9 -> 61
      //   55: iconst_1
      //   56: istore 12
      //   58: goto +351 -> 409
      //   61: aload 9
      //   63: iload 10
      //   65: aaload
      //   66: ldc 158
      //   68: invokevirtual 182	java/lang/String:equals	(Ljava/lang/Object;)Z
      //   71: ifeq +338 -> 409
      //   74: iconst_1
      //   75: istore 11
      //   77: goto +332 -> 409
      //   80: aload_2
      //   81: invokeinterface 136 1 0
      //   86: ifeq +317 -> 403
      //   89: aload_2
      //   90: ldc 158
      //   92: invokeinterface 186 2 0
      //   97: istore 18
      //   99: aload_2
      //   100: ldc 160
      //   102: invokeinterface 186 2 0
      //   107: istore 19
      //   109: aload_2
      //   110: aload_2
      //   111: ldc 126
      //   113: invokeinterface 186 2 0
      //   118: invokeinterface 140 2 0
      //   123: astore 20
      //   125: aload_2
      //   126: aload_2
      //   127: ldc 148
      //   129: invokeinterface 186 2 0
      //   134: invokeinterface 146 2 0
      //   139: lstore 21
      //   141: iload 18
      //   143: iconst_m1
      //   144: if_icmpne +142 -> 286
      //   147: iconst_1
      //   148: istore 23
      //   150: goto +278 -> 428
      //   153: new 188	com/google/android/apps/analytics/k
      //   156: dup
      //   157: aload 20
      //   159: lload 21
      //   161: iload 23
      //   163: iload 25
      //   165: invokespecial 191	com/google/android/apps/analytics/k:<init>	(Ljava/lang/String;JII)V
      //   168: astore 13
      //   170: aload_0
      //   171: invokevirtual 194	android/database/sqlite/SQLiteDatabase:beginTransaction	()V
      //   174: aload_0
      //   175: invokestatic 196	com/google/android/apps/analytics/i$a:d	(Landroid/database/sqlite/SQLiteDatabase;)V
      //   178: aload 13
      //   180: ifnull +79 -> 259
      //   183: new 39	android/content/ContentValues
      //   186: dup
      //   187: invokespecial 42	android/content/ContentValues:<init>	()V
      //   190: astore 14
      //   192: aload 14
      //   194: ldc 126
      //   196: aload 13
      //   198: invokevirtual 198	com/google/android/apps/analytics/k:a	()Ljava/lang/String;
      //   201: invokevirtual 63	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
      //   204: aload 14
      //   206: ldc 148
      //   208: aload 13
      //   210: invokevirtual 201	com/google/android/apps/analytics/k:b	()J
      //   213: invokestatic 153	java/lang/Long:valueOf	(J)Ljava/lang/Long;
      //   216: invokevirtual 156	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Long;)V
      //   219: aload 14
      //   221: ldc 158
      //   223: aload 13
      //   225: invokevirtual 204	com/google/android/apps/analytics/k:c	()I
      //   228: invokestatic 50	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
      //   231: invokevirtual 54	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
      //   234: aload 14
      //   236: ldc 160
      //   238: aload 13
      //   240: invokevirtual 206	com/google/android/apps/analytics/k:d	()I
      //   243: invokestatic 50	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
      //   246: invokevirtual 54	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
      //   249: aload_0
      //   250: ldc 126
      //   252: aconst_null
      //   253: aload 14
      //   255: invokevirtual 73	android/database/sqlite/SQLiteDatabase:insert	(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
      //   258: pop2
      //   259: aload_0
      //   260: invokevirtual 209	android/database/sqlite/SQLiteDatabase:setTransactionSuccessful	()V
      //   263: aload_2
      //   264: ifnull +9 -> 273
      //   267: aload_2
      //   268: invokeinterface 163 1 0
      //   273: aload_0
      //   274: invokevirtual 212	android/database/sqlite/SQLiteDatabase:inTransaction	()Z
      //   277: ifeq +8 -> 285
      //   280: aload_0
      //   281: invokestatic 215	com/google/android/apps/analytics/i:c	(Landroid/database/sqlite/SQLiteDatabase;)Z
      //   284: pop
      //   285: return
      //   286: aload_2
      //   287: iload 18
      //   289: invokeinterface 219 2 0
      //   294: istore 23
      //   296: goto +132 -> 428
      //   299: aload_2
      //   300: iload 19
      //   302: invokeinterface 219 2 0
      //   307: istore 24
      //   309: iload 24
      //   311: istore 25
      //   313: goto -160 -> 153
      //   316: astore 4
      //   318: aconst_null
      //   319: astore 5
      //   321: ldc 165
      //   323: aload 4
      //   325: invokevirtual 168	android/database/sqlite/SQLiteException:toString	()Ljava/lang/String;
      //   328: invokestatic 173	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
      //   331: pop
      //   332: aload 5
      //   334: ifnull +10 -> 344
      //   337: aload 5
      //   339: invokeinterface 163 1 0
      //   344: aload_0
      //   345: invokevirtual 212	android/database/sqlite/SQLiteDatabase:inTransaction	()Z
      //   348: ifeq -63 -> 285
      //   351: aload_0
      //   352: invokestatic 215	com/google/android/apps/analytics/i:c	(Landroid/database/sqlite/SQLiteDatabase;)Z
      //   355: pop
      //   356: return
      //   357: astore_1
      //   358: aconst_null
      //   359: astore_2
      //   360: aload_2
      //   361: ifnull +9 -> 370
      //   364: aload_2
      //   365: invokeinterface 163 1 0
      //   370: aload_0
      //   371: invokevirtual 212	android/database/sqlite/SQLiteDatabase:inTransaction	()Z
      //   374: ifeq +8 -> 382
      //   377: aload_0
      //   378: invokestatic 215	com/google/android/apps/analytics/i:c	(Landroid/database/sqlite/SQLiteDatabase;)Z
      //   381: pop
      //   382: aload_1
      //   383: athrow
      //   384: astore_1
      //   385: goto -25 -> 360
      //   388: astore_1
      //   389: aload 5
      //   391: astore_2
      //   392: goto -32 -> 360
      //   395: astore 4
      //   397: aload_2
      //   398: astore 5
      //   400: goto -79 -> 321
      //   403: aconst_null
      //   404: astore 13
      //   406: goto -236 -> 170
      //   409: iinc 10 1
      //   412: goto -378 -> 34
      //   415: iload 12
      //   417: ifeq -337 -> 80
      //   420: iload 11
      //   422: ifne -159 -> 263
      //   425: goto -345 -> 80
      //   428: iload 19
      //   430: iconst_m1
      //   431: if_icmpne -132 -> 299
      //   434: iconst_1
      //   435: istore 25
      //   437: goto -284 -> 153
      //
      // Exception table:
      //   from	to	target	type
      //   0	14	316	android/database/sqlite/SQLiteException
      //   0	14	357	finally
      //   17	25	384	finally
      //   34	55	384	finally
      //   61	74	384	finally
      //   80	141	384	finally
      //   153	170	384	finally
      //   170	178	384	finally
      //   183	259	384	finally
      //   259	263	384	finally
      //   286	296	384	finally
      //   299	309	384	finally
      //   321	332	388	finally
      //   17	25	395	android/database/sqlite/SQLiteException
      //   34	55	395	android/database/sqlite/SQLiteException
      //   61	74	395	android/database/sqlite/SQLiteException
      //   80	141	395	android/database/sqlite/SQLiteException
      //   153	170	395	android/database/sqlite/SQLiteException
      //   170	178	395	android/database/sqlite/SQLiteException
      //   183	259	395	android/database/sqlite/SQLiteException
      //   259	263	395	android/database/sqlite/SQLiteException
      //   286	296	395	android/database/sqlite/SQLiteException
      //   299	309	395	android/database/sqlite/SQLiteException
    }

    public final void onCreate(SQLiteDatabase paramSQLiteDatabase)
    {
      paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS events;");
      paramSQLiteDatabase.execSQL(i.f());
      paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS install_referrer;");
      paramSQLiteDatabase.execSQL("CREATE TABLE install_referrer (referrer TEXT PRIMARY KEY NOT NULL);");
      paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS session;");
      paramSQLiteDatabase.execSQL(i.g());
      if (this.a > 1)
        a(paramSQLiteDatabase);
      if (this.a > 2)
        b(paramSQLiteDatabase);
      if (this.a > 3)
      {
        c(paramSQLiteDatabase);
        d(paramSQLiteDatabase);
      }
    }

    // ERROR //
    public final void onDowngrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
    {
      // Byte code:
      //   0: ldc 165
      //   2: new 242	java/lang/StringBuilder
      //   5: dup
      //   6: ldc 244
      //   8: invokespecial 246	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
      //   11: iload_2
      //   12: invokevirtual 250	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
      //   15: ldc 252
      //   17: invokevirtual 255	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   20: iload_3
      //   21: invokevirtual 250	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
      //   24: ldc_w 257
      //   27: invokevirtual 255	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   30: invokevirtual 258	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   33: invokestatic 261	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
      //   36: pop
      //   37: aload_1
      //   38: ldc 117
      //   40: invokevirtual 26	android/database/sqlite/SQLiteDatabase:execSQL	(Ljava/lang/String;)V
      //   43: aload_1
      //   44: invokestatic 112	com/google/android/apps/analytics/i:l	()Ljava/lang/String;
      //   47: invokevirtual 26	android/database/sqlite/SQLiteDatabase:execSQL	(Ljava/lang/String;)V
      //   50: aload_1
      //   51: invokestatic 37	com/google/android/apps/analytics/i:i	()Ljava/lang/String;
      //   54: invokevirtual 26	android/database/sqlite/SQLiteDatabase:execSQL	(Ljava/lang/String;)V
      //   57: aload_1
      //   58: invokestatic 233	com/google/android/apps/analytics/i:g	()Ljava/lang/String;
      //   61: invokevirtual 26	android/database/sqlite/SQLiteDatabase:execSQL	(Ljava/lang/String;)V
      //   64: new 263	java/util/HashSet
      //   67: dup
      //   68: invokespecial 264	java/util/HashSet:<init>	()V
      //   71: astore 5
      //   73: aload_1
      //   74: ldc 69
      //   76: aconst_null
      //   77: aconst_null
      //   78: aconst_null
      //   79: aconst_null
      //   80: aconst_null
      //   81: aconst_null
      //   82: aconst_null
      //   83: invokevirtual 267	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
      //   86: astore 6
      //   88: aload 6
      //   90: invokeinterface 270 1 0
      //   95: ifeq +171 -> 266
      //   98: aload 5
      //   100: aload 6
      //   102: aload 6
      //   104: ldc 56
      //   106: invokeinterface 186 2 0
      //   111: invokeinterface 219 2 0
      //   116: invokestatic 50	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
      //   119: invokeinterface 275 2 0
      //   124: pop
      //   125: goto -37 -> 88
      //   128: astore 8
      //   130: ldc 165
      //   132: new 242	java/lang/StringBuilder
      //   135: dup
      //   136: ldc_w 277
      //   139: invokespecial 246	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
      //   142: aload 8
      //   144: invokevirtual 168	android/database/sqlite/SQLiteException:toString	()Ljava/lang/String;
      //   147: invokevirtual 255	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   150: invokevirtual 258	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   153: invokestatic 173	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
      //   156: pop
      //   157: aload 6
      //   159: invokeinterface 163 1 0
      //   164: iconst_1
      //   165: istore 10
      //   167: iload 10
      //   169: iconst_5
      //   170: if_icmpgt +150 -> 320
      //   173: aload 5
      //   175: iload 10
      //   177: invokestatic 50	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
      //   180: invokeinterface 280 2 0
      //   185: ifne +75 -> 260
      //   188: new 39	android/content/ContentValues
      //   191: dup
      //   192: invokespecial 42	android/content/ContentValues:<init>	()V
      //   195: astore 13
      //   197: aload 13
      //   199: ldc 44
      //   201: iconst_0
      //   202: invokestatic 50	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
      //   205: invokevirtual 54	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
      //   208: aload 13
      //   210: ldc 56
      //   212: iload 10
      //   214: invokestatic 50	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
      //   217: invokevirtual 54	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
      //   220: aload 13
      //   222: ldc 58
      //   224: ldc 60
      //   226: invokevirtual 63	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
      //   229: aload 13
      //   231: ldc 65
      //   233: iconst_3
      //   234: invokestatic 50	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
      //   237: invokevirtual 54	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Integer;)V
      //   240: aload 13
      //   242: ldc 67
      //   244: ldc 60
      //   246: invokevirtual 63	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
      //   249: aload_1
      //   250: ldc 69
      //   252: ldc 44
      //   254: aload 13
      //   256: invokevirtual 73	android/database/sqlite/SQLiteDatabase:insert	(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
      //   259: pop2
      //   260: iinc 10 1
      //   263: goto -96 -> 167
      //   266: aload 6
      //   268: invokeinterface 163 1 0
      //   273: goto -109 -> 164
      //   276: astore 7
      //   278: aload 6
      //   280: invokeinterface 163 1 0
      //   285: aload 7
      //   287: athrow
      //   288: astore 11
      //   290: ldc 165
      //   292: new 242	java/lang/StringBuilder
      //   295: dup
      //   296: ldc_w 282
      //   299: invokespecial 246	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
      //   302: aload 11
      //   304: invokevirtual 168	android/database/sqlite/SQLiteException:toString	()Ljava/lang/String;
      //   307: invokevirtual 255	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   310: invokevirtual 258	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   313: invokestatic 173	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
      //   316: pop
      //   317: goto -57 -> 260
      //   320: return
      //
      // Exception table:
      //   from	to	target	type
      //   88	125	128	android/database/sqlite/SQLiteException
      //   88	125	276	finally
      //   130	157	276	finally
      //   173	260	288	android/database/sqlite/SQLiteException
    }

    public final void onOpen(SQLiteDatabase paramSQLiteDatabase)
    {
      if (paramSQLiteDatabase.isReadOnly())
      {
        Log.w("GoogleAnalyticsTracker", "Warning: Need to update database, but it's read only.");
        return;
      }
      f(paramSQLiteDatabase);
    }

    public final void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
    {
      if (paramInt1 > paramInt2)
        onDowngrade(paramSQLiteDatabase, paramInt1, paramInt2);
      do
      {
        return;
        if ((paramInt1 < 2) && (paramInt2 > 1))
          a(paramSQLiteDatabase);
        if ((paramInt1 < 3) && (paramInt2 > 2))
          b(paramSQLiteDatabase);
      }
      while ((paramInt1 >= 4) || (paramInt2 <= 3));
      c(paramSQLiteDatabase);
      d(paramSQLiteDatabase);
      a(paramSQLiteDatabase, paramInt1);
      e(paramSQLiteDatabase);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.android.apps.analytics.i
 * JD-Core Version:    0.6.2
 */