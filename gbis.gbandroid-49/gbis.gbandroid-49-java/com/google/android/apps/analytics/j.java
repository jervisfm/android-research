package com.google.android.apps.analytics;

import android.util.Log;
import java.io.IOException;
import java.net.Socket;
import org.apache.http.Header;
import org.apache.http.HttpConnectionMetrics;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.ProtocolVersion;
import org.apache.http.StatusLine;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.SocketFactory;
import org.apache.http.impl.DefaultHttpClientConnection;
import org.apache.http.params.BasicHttpParams;

final class j
{
  DefaultHttpClientConnection a = new DefaultHttpClientConnection();
  a b;
  int c;
  boolean d = true;
  HttpHost e;
  SocketFactory f;

  public j(HttpHost paramHttpHost)
  {
    this(paramHttpHost, new PlainSocketFactory());
  }

  private j(HttpHost paramHttpHost, SocketFactory paramSocketFactory)
  {
    this.e = paramHttpHost;
    this.f = paramSocketFactory;
  }

  private void c()
  {
    if ((this.a == null) || (!this.a.isOpen()))
    {
      BasicHttpParams localBasicHttpParams = new BasicHttpParams();
      Socket localSocket1 = this.f.createSocket();
      Socket localSocket2 = this.f.connectSocket(localSocket1, this.e.getHostName(), this.e.getPort(), null, 0, localBasicHttpParams);
      localSocket2.setReceiveBufferSize(8192);
      this.a.bind(localSocket2, localBasicHttpParams);
    }
  }

  private void d()
  {
    if ((this.a != null) && (this.a.isOpen()));
    try
    {
      this.a.close();
      return;
    }
    catch (IOException localIOException)
    {
    }
  }

  public final void a()
  {
    this.a.flush();
    HttpConnectionMetrics localHttpConnectionMetrics = this.a.getMetrics();
    do
    {
      HttpResponse localHttpResponse;
      if (localHttpConnectionMetrics.getResponseCount() < localHttpConnectionMetrics.getRequestCount())
      {
        localHttpResponse = this.a.receiveResponseHeader();
        if (!localHttpResponse.getStatusLine().getProtocolVersion().greaterEquals(HttpVersion.HTTP_1_1))
        {
          this.b.a();
          this.d = false;
        }
        Header[] arrayOfHeader = localHttpResponse.getHeaders("Connection");
        if (arrayOfHeader != null)
        {
          int i = arrayOfHeader.length;
          for (int j = 0; j < i; j++)
            if ("close".equalsIgnoreCase(arrayOfHeader[j].getValue()))
            {
              this.b.a();
              this.d = false;
            }
        }
        this.c = localHttpResponse.getStatusLine().getStatusCode();
        if (this.c != 200)
        {
          this.b.a(this.c);
          d();
        }
      }
      else
      {
        return;
      }
      this.a.receiveResponseEntity(localHttpResponse);
      localHttpResponse.getEntity().consumeContent();
      this.b.b();
      if (GoogleAnalyticsTracker.getInstance().getDebug())
        Log.v("GoogleAnalyticsTracker", "HTTP Response Code: " + localHttpResponse.getStatusLine().getStatusCode());
    }
    while (this.d);
    d();
  }

  public final void a(a parama)
  {
    this.b = parama;
  }

  public final void a(HttpEntityEnclosingRequest paramHttpEntityEnclosingRequest)
  {
    c();
    this.a.sendRequestHeader(paramHttpEntityEnclosingRequest);
    this.a.sendRequestEntity(paramHttpEntityEnclosingRequest);
  }

  public final void b()
  {
    d();
  }

  static abstract interface a
  {
    public abstract void a();

    public abstract void a(int paramInt);

    public abstract void b();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.android.apps.analytics.j
 * JD-Core Version:    0.6.2
 */