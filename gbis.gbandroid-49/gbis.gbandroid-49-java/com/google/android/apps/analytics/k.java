package com.google.android.apps.analytics;

final class k
{
  private final String a;
  private final long b;
  private final int c;
  private final int d;

  k(String paramString, long paramLong, int paramInt1, int paramInt2)
  {
    this.a = paramString;
    this.b = paramLong;
    this.c = paramInt1;
    this.d = paramInt2;
  }

  final String a()
  {
    return this.a;
  }

  final long b()
  {
    return this.b;
  }

  final int c()
  {
    return this.c;
  }

  final int d()
  {
    return this.d;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.android.apps.analytics.k
 * JD-Core Version:    0.6.2
 */