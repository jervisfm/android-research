package com.google.gbson;

import com.google.gbson.internal..Gson.Preconditions;
import com.google.gbson.internal..Gson.Types;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

public final class FieldAttributes
{
  private static final c<ai<Class<?>, String>, Collection<Annotation>> a = new z(d());
  private final Class<?> b;
  private final Field c;
  private final Class<?> d;
  private final boolean e;
  private final int f;
  private final String g;
  private final Type h;
  private Type i;
  private Collection<Annotation> j;

  FieldAttributes(Class<?> paramClass, Field paramField, Type paramType)
  {
    this.b = ((Class).Gson.Preconditions.checkNotNull(paramClass));
    this.g = paramField.getName();
    this.d = paramField.getType();
    this.e = paramField.isSynthetic();
    this.f = paramField.getModifiers();
    this.c = paramField;
    this.h = a(paramField, paramType);
  }

  private static <T extends Annotation> T a(Collection<Annotation> paramCollection, Class<T> paramClass)
  {
    Iterator localIterator = paramCollection.iterator();
    while (localIterator.hasNext())
    {
      Annotation localAnnotation = (Annotation)localIterator.next();
      if (localAnnotation.annotationType() == paramClass)
        return localAnnotation;
    }
    return null;
  }

  private static Type a(Field paramField, Type paramType)
  {
    Class localClass = .Gson.Types.getRawType(paramType);
    if (!paramField.getDeclaringClass().isAssignableFrom(localClass))
      return paramField.getGenericType();
    return .Gson.Types.resolve(paramType, localClass, paramField.getGenericType());
  }

  private static int d()
  {
    try
    {
      int k = Integer.parseInt(System.getProperty("com.google.gbson.annotation_cache_size_hint", String.valueOf(2000)));
      return k;
    }
    catch (NumberFormatException localNumberFormatException)
    {
    }
    return 2000;
  }

  final Object a(Object paramObject)
  {
    return this.c.get(paramObject);
  }

  final void a(Object paramObject1, Object paramObject2)
  {
    this.c.set(paramObject1, paramObject2);
  }

  final boolean a()
  {
    return this.e;
  }

  @Deprecated
  final Field b()
  {
    return this.c;
  }

  final Type c()
  {
    return this.h;
  }

  public final <T extends Annotation> T getAnnotation(Class<T> paramClass)
  {
    return a(getAnnotations(), paramClass);
  }

  public final Collection<Annotation> getAnnotations()
  {
    if (this.j == null)
    {
      ai localai = new ai(this.b, this.g);
      this.j = ((Collection)a.a(localai));
      if (this.j == null)
      {
        this.j = Collections.unmodifiableCollection(Arrays.asList(this.c.getAnnotations()));
        a.a(localai, this.j);
      }
    }
    return this.j;
  }

  public final Class<?> getDeclaredClass()
  {
    return this.d;
  }

  public final Type getDeclaredType()
  {
    if (this.i == null)
      this.i = this.c.getGenericType();
    return this.i;
  }

  public final Class<?> getDeclaringClass()
  {
    return this.b;
  }

  public final String getName()
  {
    return this.g;
  }

  public final boolean hasModifier(int paramInt)
  {
    return (paramInt & this.f) != 0;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.FieldAttributes
 * JD-Core Version:    0.6.2
 */