package com.google.gbson;

public enum FieldNamingPolicy
{
  private final m a;

  static
  {
    LOWER_CASE_WITH_UNDERSCORES = new FieldNamingPolicy("LOWER_CASE_WITH_UNDERSCORES", 2, new x("_"));
    LOWER_CASE_WITH_DASHES = new FieldNamingPolicy("LOWER_CASE_WITH_DASHES", 3, new x("-"));
    FieldNamingPolicy[] arrayOfFieldNamingPolicy = new FieldNamingPolicy[4];
    arrayOfFieldNamingPolicy[0] = UPPER_CAMEL_CASE;
    arrayOfFieldNamingPolicy[1] = UPPER_CAMEL_CASE_WITH_SPACES;
    arrayOfFieldNamingPolicy[2] = LOWER_CASE_WITH_UNDERSCORES;
    arrayOfFieldNamingPolicy[3] = LOWER_CASE_WITH_DASHES;
  }

  private FieldNamingPolicy(m paramm)
  {
    this.a = paramm;
  }

  final m a()
  {
    return this.a;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.FieldNamingPolicy
 * JD-Core Version:    0.6.2
 */