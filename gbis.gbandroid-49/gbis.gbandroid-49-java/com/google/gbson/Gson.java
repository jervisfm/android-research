package com.google.gbson;

import com.google.gbson.stream.JsonReader;
import com.google.gbson.stream.JsonToken;
import com.google.gbson.stream.JsonWriter;
import com.google.gbson.stream.MalformedJsonException;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;

public final class Gson
{
  static final a a = new a();
  static final ap b = new ap();
  static final ae c = new ae(new int[] { 128, 8 });
  static final m d = new an(new p());
  private static final ExclusionStrategy e = a();
  private final ExclusionStrategy f;
  private final ExclusionStrategy g;
  private final m h;
  private final ac i;
  private final aj<JsonSerializer<?>> j;
  private final aj<JsonDeserializer<?>> k;
  private final boolean l;
  private final boolean m;
  private final boolean n;
  private final boolean o;

  public Gson()
  {
    this(e, e, d, new ac(h.d()), false, h.a(), h.b(), false, true, false);
  }

  Gson(ExclusionStrategy paramExclusionStrategy1, ExclusionStrategy paramExclusionStrategy2, m paramm, ac paramac, boolean paramBoolean1, aj<JsonSerializer<?>> paramaj, aj<JsonDeserializer<?>> paramaj1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4)
  {
    this.f = paramExclusionStrategy1;
    this.g = paramExclusionStrategy2;
    this.h = paramm;
    this.i = paramac;
    this.l = paramBoolean1;
    this.j = paramaj;
    this.k = paramaj1;
    this.n = paramBoolean2;
    this.m = paramBoolean3;
    this.o = paramBoolean4;
  }

  private static ExclusionStrategy a()
  {
    LinkedList localLinkedList = new LinkedList();
    localLinkedList.add(a);
    localLinkedList.add(b);
    localLinkedList.add(c);
    return new i(localLinkedList);
  }

  private static void a(Object paramObject, JsonReader paramJsonReader)
  {
    if (paramObject != null)
      try
      {
        if (paramJsonReader.peek() != JsonToken.END_DOCUMENT)
          throw new JsonIOException("JSON document was not fully consumed.");
      }
      catch (MalformedJsonException localMalformedJsonException)
      {
        throw new JsonSyntaxException(localMalformedJsonException);
      }
      catch (IOException localIOException)
      {
        throw new JsonIOException(localIOException);
      }
  }

  public final <T> T fromJson(JsonElement paramJsonElement, Class<T> paramClass)
  {
    Object localObject = fromJson(paramJsonElement, paramClass);
    return ak.a(paramClass).cast(localObject);
  }

  public final <T> T fromJson(JsonElement paramJsonElement, Type paramType)
  {
    if (paramJsonElement == null)
      return null;
    return new r(new ObjectNavigator(this.f), this.h, this.k, this.i).deserialize(paramJsonElement, paramType);
  }

  public final <T> T fromJson(JsonReader paramJsonReader, Type paramType)
  {
    boolean bool = paramJsonReader.isLenient();
    paramJsonReader.setLenient(true);
    try
    {
      Object localObject2 = fromJson(ao.a(paramJsonReader), paramType);
      return localObject2;
    }
    finally
    {
      paramJsonReader.setLenient(bool);
    }
  }

  public final <T> T fromJson(Reader paramReader, Class<T> paramClass)
  {
    JsonReader localJsonReader = new JsonReader(paramReader);
    Object localObject = fromJson(localJsonReader, paramClass);
    a(localObject, localJsonReader);
    return ak.a(paramClass).cast(localObject);
  }

  public final <T> T fromJson(Reader paramReader, Type paramType)
  {
    JsonReader localJsonReader = new JsonReader(paramReader);
    Object localObject = fromJson(localJsonReader, paramType);
    a(localObject, localJsonReader);
    return localObject;
  }

  public final <T> T fromJson(String paramString, Class<T> paramClass)
  {
    Object localObject = fromJson(paramString, paramClass);
    return ak.a(paramClass).cast(localObject);
  }

  public final <T> T fromJson(String paramString, Type paramType)
  {
    if (paramString == null)
      return null;
    return fromJson(new StringReader(paramString), paramType);
  }

  public final String toJson(JsonElement paramJsonElement)
  {
    StringWriter localStringWriter = new StringWriter();
    toJson(paramJsonElement, localStringWriter);
    return localStringWriter.toString();
  }

  public final String toJson(Object paramObject)
  {
    if (paramObject == null)
      return toJson(JsonNull.c());
    return toJson(paramObject, paramObject.getClass());
  }

  public final String toJson(Object paramObject, Type paramType)
  {
    StringWriter localStringWriter = new StringWriter();
    toJson(toJsonTree(paramObject, paramType), localStringWriter);
    return localStringWriter.toString();
  }

  public final void toJson(JsonElement paramJsonElement, JsonWriter paramJsonWriter)
  {
    boolean bool1 = paramJsonWriter.isLenient();
    paramJsonWriter.setLenient(true);
    boolean bool2 = paramJsonWriter.isHtmlSafe();
    paramJsonWriter.setHtmlSafe(this.m);
    try
    {
      ao.a(paramJsonElement, this.l, paramJsonWriter);
      return;
    }
    catch (IOException localIOException)
    {
      throw new JsonIOException(localIOException);
    }
    finally
    {
      paramJsonWriter.setLenient(bool1);
      paramJsonWriter.setHtmlSafe(bool2);
    }
  }

  public final void toJson(JsonElement paramJsonElement, Appendable paramAppendable)
  {
    try
    {
      if (this.n)
        paramAppendable.append(")]}'\n");
      JsonWriter localJsonWriter = new JsonWriter(ao.a(paramAppendable));
      if (this.o)
        localJsonWriter.setIndent("  ");
      toJson(paramJsonElement, localJsonWriter);
      return;
    }
    catch (IOException localIOException)
    {
      throw new RuntimeException(localIOException);
    }
  }

  public final void toJson(Object paramObject, Appendable paramAppendable)
  {
    if (paramObject != null)
    {
      toJson(paramObject, paramObject.getClass(), paramAppendable);
      return;
    }
    toJson(JsonNull.c(), paramAppendable);
  }

  public final void toJson(Object paramObject, Type paramType, JsonWriter paramJsonWriter)
  {
    toJson(toJsonTree(paramObject, paramType), paramJsonWriter);
  }

  public final void toJson(Object paramObject, Type paramType, Appendable paramAppendable)
  {
    toJson(toJsonTree(paramObject, paramType), paramAppendable);
  }

  public final JsonElement toJsonTree(Object paramObject)
  {
    if (paramObject == null)
      return JsonNull.c();
    return toJsonTree(paramObject, paramObject.getClass());
  }

  public final JsonElement toJsonTree(Object paramObject, Type paramType)
  {
    return new v(new ObjectNavigator(this.g), this.h, this.l, this.j).serialize(paramObject, paramType);
  }

  public final String toString()
  {
    return "{serializeNulls:" + this.l + ",serializers:" + this.j + ",deserializers:" + this.k + ",instanceCreators:" + this.i + "}";
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.Gson
 * JD-Core Version:    0.6.2
 */