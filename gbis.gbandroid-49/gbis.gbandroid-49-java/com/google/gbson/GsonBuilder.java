package com.google.gbson;

import com.google.gbson.internal..Gson.Preconditions;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class GsonBuilder
{
  private static final aa a = new aa();
  private static final o b = new o();
  private static final k c = new k();
  private static final l d = new l();
  private final Set<ExclusionStrategy> e = new HashSet();
  private final Set<ExclusionStrategy> f = new HashSet();
  private double g;
  private ae h;
  private boolean i;
  private boolean j;
  private LongSerializationPolicy k;
  private m l;
  private final aj<InstanceCreator<?>> m;
  private final aj<JsonSerializer<?>> n;
  private final aj<JsonDeserializer<?>> o;
  private boolean p;
  private String q;
  private int r;
  private int s;
  private boolean t;
  private boolean u;
  private boolean v;
  private boolean w;

  public GsonBuilder()
  {
    this.f.add(Gson.a);
    this.f.add(Gson.b);
    this.e.add(Gson.a);
    this.e.add(Gson.b);
    this.g = -1.0D;
    this.i = true;
    this.v = false;
    this.u = true;
    this.h = Gson.c;
    this.j = false;
    this.k = LongSerializationPolicy.DEFAULT;
    this.l = Gson.d;
    this.m = new aj();
    this.n = new aj();
    this.o = new aj();
    this.p = false;
    this.r = 2;
    this.s = 2;
    this.t = false;
    this.w = false;
  }

  private GsonBuilder a(m paramm)
  {
    this.l = new an(paramm);
    return this;
  }

  private <T> GsonBuilder a(Class<?> paramClass, InstanceCreator<? extends T> paramInstanceCreator)
  {
    this.m.a(paramClass, paramInstanceCreator);
    return this;
  }

  private <T> GsonBuilder a(Class<?> paramClass, JsonDeserializer<T> paramJsonDeserializer)
  {
    this.o.a(paramClass, new t(paramJsonDeserializer));
    return this;
  }

  private <T> GsonBuilder a(Class<?> paramClass, JsonSerializer<T> paramJsonSerializer)
  {
    this.n.a(paramClass, paramJsonSerializer);
    return this;
  }

  private <T> GsonBuilder a(Type paramType, InstanceCreator<? extends T> paramInstanceCreator)
  {
    this.m.a(paramType, paramInstanceCreator);
    return this;
  }

  private <T> GsonBuilder a(Type paramType, JsonDeserializer<T> paramJsonDeserializer)
  {
    this.o.a(paramType, new t(paramJsonDeserializer));
    return this;
  }

  private <T> GsonBuilder a(Type paramType, JsonSerializer<T> paramJsonSerializer)
  {
    this.n.a(paramType, paramJsonSerializer);
    return this;
  }

  private static <T> void a(Class<?> paramClass, aj<T> paramaj, T paramT)
  {
    if (!paramaj.b(paramClass))
      paramaj.a(paramClass, paramT);
  }

  private static void a(String paramString, int paramInt1, int paramInt2, aj<JsonSerializer<?>> paramaj, aj<JsonDeserializer<?>> paramaj1)
  {
    h.h localh;
    if ((paramString != null) && (!"".equals(paramString.trim())))
      localh = new h.h(paramString);
    while (true)
    {
      if (localh != null)
      {
        a(java.util.Date.class, paramaj, localh);
        a(java.util.Date.class, paramaj1, localh);
        a(Timestamp.class, paramaj, localh);
        a(Timestamp.class, paramaj1, localh);
        a(java.sql.Date.class, paramaj, localh);
        a(java.sql.Date.class, paramaj1, localh);
      }
      return;
      localh = null;
      if (paramInt1 != 2)
      {
        localh = null;
        if (paramInt2 != 2)
          localh = new h.h(paramInt1, paramInt2);
      }
    }
  }

  public final GsonBuilder addDeserializationExclusionStrategy(ExclusionStrategy paramExclusionStrategy)
  {
    this.f.add(paramExclusionStrategy);
    return this;
  }

  public final GsonBuilder addSerializationExclusionStrategy(ExclusionStrategy paramExclusionStrategy)
  {
    this.e.add(paramExclusionStrategy);
    return this;
  }

  public final Gson create()
  {
    LinkedList localLinkedList1 = new LinkedList(this.f);
    LinkedList localLinkedList2 = new LinkedList(this.e);
    localLinkedList1.add(this.h);
    localLinkedList2.add(this.h);
    if (!this.i)
    {
      localLinkedList1.add(b);
      localLinkedList2.add(b);
    }
    if (this.g != -1.0D)
    {
      aw localaw = new aw(this.g);
      localLinkedList1.add(localaw);
      localLinkedList2.add(localaw);
    }
    if (this.j)
    {
      localLinkedList1.add(c);
      localLinkedList2.add(d);
    }
    aj localaj1 = h.a.b();
    localaj1.b(this.n.b());
    aj localaj2 = h.b.b();
    localaj2.b(this.o.b());
    a(this.q, this.r, this.s, localaj1, localaj2);
    localaj1.a(h.a(this.t, this.k));
    localaj2.a(h.c());
    aj localaj3 = this.m.b();
    localaj3.a(h.d());
    localaj1.a();
    localaj2.a();
    this.m.a();
    ac localac = new ac(localaj3);
    return new Gson(new i(localLinkedList1), new i(localLinkedList2), this.l, localac, this.p, localaj1, localaj2, this.w, this.u, this.v);
  }

  public final GsonBuilder disableHtmlEscaping()
  {
    this.u = false;
    return this;
  }

  public final GsonBuilder disableInnerClassSerialization()
  {
    this.i = false;
    return this;
  }

  public final GsonBuilder enableComplexMapKeySerialization()
  {
    registerTypeHierarchyAdapter(Map.class, a);
    return this;
  }

  public final GsonBuilder excludeFieldsWithModifiers(int[] paramArrayOfInt)
  {
    this.h = new ae(paramArrayOfInt);
    return this;
  }

  public final GsonBuilder excludeFieldsWithoutExposeAnnotation()
  {
    this.j = true;
    return this;
  }

  public final GsonBuilder generateNonExecutableJson()
  {
    this.w = true;
    return this;
  }

  public final GsonBuilder registerTypeAdapter(Type paramType, Object paramObject)
  {
    if (((paramObject instanceof JsonSerializer)) || ((paramObject instanceof JsonDeserializer)) || ((paramObject instanceof InstanceCreator)));
    for (boolean bool = true; ; bool = false)
    {
      .Gson.Preconditions.checkArgument(bool);
      if ((paramObject instanceof InstanceCreator))
        a(paramType, (InstanceCreator)paramObject);
      if ((paramObject instanceof JsonSerializer))
        a(paramType, (JsonSerializer)paramObject);
      if ((paramObject instanceof JsonDeserializer))
        a(paramType, (JsonDeserializer)paramObject);
      return this;
    }
  }

  public final GsonBuilder registerTypeHierarchyAdapter(Class<?> paramClass, Object paramObject)
  {
    if (((paramObject instanceof JsonSerializer)) || ((paramObject instanceof JsonDeserializer)) || ((paramObject instanceof InstanceCreator)));
    for (boolean bool = true; ; bool = false)
    {
      .Gson.Preconditions.checkArgument(bool);
      if ((paramObject instanceof InstanceCreator))
        a(paramClass, (InstanceCreator)paramObject);
      if ((paramObject instanceof JsonSerializer))
        a(paramClass, (JsonSerializer)paramObject);
      if ((paramObject instanceof JsonDeserializer))
        a(paramClass, (JsonDeserializer)paramObject);
      return this;
    }
  }

  public final GsonBuilder serializeNulls()
  {
    this.p = true;
    return this;
  }

  public final GsonBuilder serializeSpecialFloatingPointValues()
  {
    this.t = true;
    return this;
  }

  public final GsonBuilder setDateFormat(int paramInt)
  {
    this.r = paramInt;
    this.q = null;
    return this;
  }

  public final GsonBuilder setDateFormat(int paramInt1, int paramInt2)
  {
    this.r = paramInt1;
    this.s = paramInt2;
    this.q = null;
    return this;
  }

  public final GsonBuilder setDateFormat(String paramString)
  {
    this.q = paramString;
    return this;
  }

  public final GsonBuilder setExclusionStrategies(ExclusionStrategy[] paramArrayOfExclusionStrategy)
  {
    List localList = Arrays.asList(paramArrayOfExclusionStrategy);
    this.e.addAll(localList);
    this.f.addAll(localList);
    return this;
  }

  public final GsonBuilder setFieldNamingPolicy(FieldNamingPolicy paramFieldNamingPolicy)
  {
    return a(paramFieldNamingPolicy.a());
  }

  public final GsonBuilder setFieldNamingStrategy(FieldNamingStrategy paramFieldNamingStrategy)
  {
    return a(new n(paramFieldNamingStrategy));
  }

  public final GsonBuilder setLongSerializationPolicy(LongSerializationPolicy paramLongSerializationPolicy)
  {
    this.k = paramLongSerializationPolicy;
    return this;
  }

  public final GsonBuilder setPrettyPrinting()
  {
    this.v = true;
    return this;
  }

  public final GsonBuilder setVersion(double paramDouble)
  {
    this.g = paramDouble;
    return this;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.GsonBuilder
 * JD-Core Version:    0.6.2
 */