package com.google.gbson;

public final class JsonIOException extends JsonParseException
{
  private static final long serialVersionUID = 1L;

  public JsonIOException(String paramString)
  {
    super(paramString);
  }

  public JsonIOException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }

  public JsonIOException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.JsonIOException
 * JD-Core Version:    0.6.2
 */