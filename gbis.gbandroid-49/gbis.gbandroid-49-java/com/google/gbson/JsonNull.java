package com.google.gbson;

public final class JsonNull extends JsonElement
{
  private static final JsonNull a = new JsonNull();

  static JsonNull c()
  {
    return a;
  }

  public final boolean equals(Object paramObject)
  {
    return (this == paramObject) || ((paramObject instanceof JsonNull));
  }

  public final int hashCode()
  {
    return JsonNull.class.hashCode();
  }

  protected final void toString(Appendable paramAppendable, j paramj)
  {
    paramAppendable.append("null");
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.JsonNull
 * JD-Core Version:    0.6.2
 */