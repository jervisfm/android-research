package com.google.gbson;

import com.google.gbson.internal..Gson.Preconditions;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class JsonObject extends JsonElement
{
  private final Map<String, JsonElement> a = new LinkedHashMap();

  private static JsonElement a(Object paramObject)
  {
    if (paramObject == null)
      return JsonNull.c();
    return new JsonPrimitive(paramObject);
  }

  public final void add(String paramString, JsonElement paramJsonElement)
  {
    if (paramJsonElement == null)
      paramJsonElement = JsonNull.c();
    this.a.put(.Gson.Preconditions.checkNotNull(paramString), paramJsonElement);
  }

  public final void addProperty(String paramString, Boolean paramBoolean)
  {
    add(paramString, a(paramBoolean));
  }

  public final void addProperty(String paramString, Character paramCharacter)
  {
    add(paramString, a(paramCharacter));
  }

  public final void addProperty(String paramString, Number paramNumber)
  {
    add(paramString, a(paramNumber));
  }

  public final void addProperty(String paramString1, String paramString2)
  {
    add(paramString1, a(paramString2));
  }

  public final Set<Map.Entry<String, JsonElement>> entrySet()
  {
    return this.a.entrySet();
  }

  public final boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof JsonObject)) && (((JsonObject)paramObject).a.equals(this.a)));
  }

  public final JsonElement get(String paramString)
  {
    if (this.a.containsKey(paramString))
    {
      Object localObject = (JsonElement)this.a.get(paramString);
      if (localObject == null)
        localObject = JsonNull.c();
      return localObject;
    }
    return null;
  }

  public final JsonArray getAsJsonArray(String paramString)
  {
    return (JsonArray)this.a.get(paramString);
  }

  public final JsonObject getAsJsonObject(String paramString)
  {
    return (JsonObject)this.a.get(paramString);
  }

  public final JsonPrimitive getAsJsonPrimitive(String paramString)
  {
    return (JsonPrimitive)this.a.get(paramString);
  }

  public final boolean has(String paramString)
  {
    return this.a.containsKey(paramString);
  }

  public final int hashCode()
  {
    return this.a.hashCode();
  }

  public final JsonElement remove(String paramString)
  {
    return (JsonElement)this.a.remove(paramString);
  }

  protected final void toString(Appendable paramAppendable, j paramj)
  {
    paramAppendable.append('{');
    Iterator localIterator = this.a.entrySet().iterator();
    int i = 1;
    if (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      if (i != 0);
      for (int j = 0; ; j = i)
      {
        paramAppendable.append('"');
        paramAppendable.append(paramj.a((CharSequence)localEntry.getKey()));
        paramAppendable.append("\":");
        ((JsonElement)localEntry.getValue()).toString(paramAppendable, paramj);
        i = j;
        break;
        paramAppendable.append(',');
      }
    }
    paramAppendable.append('}');
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.JsonObject
 * JD-Core Version:    0.6.2
 */