package com.google.gbson;

public class JsonParseException extends RuntimeException
{
  static final long serialVersionUID = -4086729973971783390L;

  public JsonParseException(String paramString)
  {
    super(paramString);
  }

  public JsonParseException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }

  public JsonParseException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.JsonParseException
 * JD-Core Version:    0.6.2
 */