package com.google.gbson;

import com.google.gbson.stream.JsonReader;
import com.google.gbson.stream.JsonToken;
import com.google.gbson.stream.MalformedJsonException;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

public final class JsonParser
{
  // ERROR //
  public final JsonElement parse(JsonReader paramJsonReader)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual 22	com/google/gbson/stream/JsonReader:isLenient	()Z
    //   4: istore_2
    //   5: aload_1
    //   6: iconst_1
    //   7: invokevirtual 26	com/google/gbson/stream/JsonReader:setLenient	(Z)V
    //   10: aload_1
    //   11: invokestatic 31	com/google/gbson/ao:a	(Lcom/google/gbson/stream/JsonReader;)Lcom/google/gbson/JsonElement;
    //   14: astore 8
    //   16: aload_1
    //   17: iload_2
    //   18: invokevirtual 26	com/google/gbson/stream/JsonReader:setLenient	(Z)V
    //   21: aload 8
    //   23: areturn
    //   24: astore 7
    //   26: new 16	com/google/gbson/JsonParseException
    //   29: dup
    //   30: new 33	java/lang/StringBuilder
    //   33: dup
    //   34: ldc 35
    //   36: invokespecial 38	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   39: aload_1
    //   40: invokevirtual 42	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   43: ldc 44
    //   45: invokevirtual 47	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   48: invokevirtual 51	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   51: aload 7
    //   53: invokespecial 54	com/google/gbson/JsonParseException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   56: athrow
    //   57: astore 4
    //   59: aload_1
    //   60: iload_2
    //   61: invokevirtual 26	com/google/gbson/stream/JsonReader:setLenient	(Z)V
    //   64: aload 4
    //   66: athrow
    //   67: astore 6
    //   69: new 16	com/google/gbson/JsonParseException
    //   72: dup
    //   73: new 33	java/lang/StringBuilder
    //   76: dup
    //   77: ldc 35
    //   79: invokespecial 38	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   82: aload_1
    //   83: invokevirtual 42	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   86: ldc 44
    //   88: invokevirtual 47	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   91: invokevirtual 51	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   94: aload 6
    //   96: invokespecial 54	com/google/gbson/JsonParseException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   99: athrow
    //   100: astore_3
    //   101: aload_3
    //   102: invokevirtual 58	com/google/gbson/JsonParseException:getCause	()Ljava/lang/Throwable;
    //   105: instanceof 60
    //   108: ifeq +16 -> 124
    //   111: invokestatic 66	com/google/gbson/JsonNull:c	()Lcom/google/gbson/JsonNull;
    //   114: astore 5
    //   116: aload_1
    //   117: iload_2
    //   118: invokevirtual 26	com/google/gbson/stream/JsonReader:setLenient	(Z)V
    //   121: aload 5
    //   123: areturn
    //   124: aload_3
    //   125: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   10	16	24	java/lang/StackOverflowError
    //   10	16	57	finally
    //   26	57	57	finally
    //   69	100	57	finally
    //   101	116	57	finally
    //   124	126	57	finally
    //   10	16	67	java/lang/OutOfMemoryError
    //   10	16	100	com/google/gbson/JsonParseException
  }

  public final JsonElement parse(Reader paramReader)
  {
    JsonElement localJsonElement;
    try
    {
      JsonReader localJsonReader = new JsonReader(paramReader);
      localJsonElement = parse(localJsonReader);
      if ((!localJsonElement.isJsonNull()) && (localJsonReader.peek() != JsonToken.END_DOCUMENT))
        throw new JsonSyntaxException("Did not consume the entire document.");
    }
    catch (MalformedJsonException localMalformedJsonException)
    {
      throw new JsonSyntaxException(localMalformedJsonException);
    }
    catch (IOException localIOException)
    {
      throw new JsonIOException(localIOException);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      throw new JsonSyntaxException(localNumberFormatException);
    }
    return localJsonElement;
  }

  public final JsonElement parse(String paramString)
  {
    return parse(new StringReader(paramString));
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.JsonParser
 * JD-Core Version:    0.6.2
 */