package com.google.gbson;

import com.google.gbson.internal..Gson.Preconditions;
import java.math.BigDecimal;
import java.math.BigInteger;

public final class JsonPrimitive extends JsonElement
{
  private static final Class<?>[] a = arrayOfClass;
  private static final BigInteger b = BigInteger.valueOf(2147483647L);
  private static final BigInteger c = BigInteger.valueOf(9223372036854775807L);
  private Object d;

  static
  {
    Class[] arrayOfClass = new Class[16];
    arrayOfClass[0] = Integer.TYPE;
    arrayOfClass[1] = Long.TYPE;
    arrayOfClass[2] = Short.TYPE;
    arrayOfClass[3] = Float.TYPE;
    arrayOfClass[4] = Double.TYPE;
    arrayOfClass[5] = Byte.TYPE;
    arrayOfClass[6] = Boolean.TYPE;
    arrayOfClass[7] = Character.TYPE;
    arrayOfClass[8] = Integer.class;
    arrayOfClass[9] = Long.class;
    arrayOfClass[10] = Short.class;
    arrayOfClass[11] = Float.class;
    arrayOfClass[12] = Double.class;
    arrayOfClass[13] = Byte.class;
    arrayOfClass[14] = Boolean.class;
    arrayOfClass[15] = Character.class;
  }

  public JsonPrimitive(Boolean paramBoolean)
  {
    a(paramBoolean);
  }

  public JsonPrimitive(Character paramCharacter)
  {
    a(paramCharacter);
  }

  public JsonPrimitive(Number paramNumber)
  {
    a(paramNumber);
  }

  JsonPrimitive(Object paramObject)
  {
    a(paramObject);
  }

  public JsonPrimitive(String paramString)
  {
    a(paramString);
  }

  static Number a(String paramString)
  {
    try
    {
      long l = Long.parseLong(paramString);
      if ((l >= -2147483648L) && (l <= 2147483647L))
        return Integer.valueOf((int)l);
      Long localLong = Long.valueOf(l);
      return localLong;
    }
    catch (NumberFormatException localNumberFormatException1)
    {
      try
      {
        BigDecimal localBigDecimal = new BigDecimal(paramString);
        return localBigDecimal;
      }
      catch (NumberFormatException localNumberFormatException2)
      {
      }
    }
    return Double.valueOf(Double.parseDouble(paramString));
  }

  private void a(Object paramObject)
  {
    if ((paramObject instanceof Character))
    {
      this.d = String.valueOf(((Character)paramObject).charValue());
      return;
    }
    if (((paramObject instanceof Number)) || (b(paramObject)));
    for (boolean bool = true; ; bool = false)
    {
      .Gson.Preconditions.checkArgument(bool);
      this.d = paramObject;
      return;
    }
  }

  private static boolean a(JsonPrimitive paramJsonPrimitive)
  {
    if ((paramJsonPrimitive.d instanceof Number))
    {
      Number localNumber = (Number)paramJsonPrimitive.d;
      return ((localNumber instanceof BigInteger)) || ((localNumber instanceof Long)) || ((localNumber instanceof Integer)) || ((localNumber instanceof Short)) || ((localNumber instanceof Byte));
    }
    return false;
  }

  private static boolean b(JsonPrimitive paramJsonPrimitive)
  {
    if ((paramJsonPrimitive.d instanceof Number))
    {
      Number localNumber = (Number)paramJsonPrimitive.d;
      return ((localNumber instanceof BigDecimal)) || ((localNumber instanceof Double)) || ((localNumber instanceof Float));
    }
    return false;
  }

  private static boolean b(Object paramObject)
  {
    if ((paramObject instanceof String))
      return true;
    Class localClass = paramObject.getClass();
    Class[] arrayOfClass = a;
    int i = arrayOfClass.length;
    for (int j = 0; ; j++)
    {
      if (j >= i)
        break label47;
      if (arrayOfClass[j].isAssignableFrom(localClass))
        break;
    }
    label47: return false;
  }

  final Object a()
  {
    if ((this.d instanceof BigInteger))
    {
      BigInteger localBigInteger = (BigInteger)this.d;
      if (localBigInteger.compareTo(b) < 0)
        return Integer.valueOf(localBigInteger.intValue());
      if (localBigInteger.compareTo(c) < 0)
        return Long.valueOf(localBigInteger.longValue());
    }
    return this.d;
  }

  final Boolean b()
  {
    return (Boolean)this.d;
  }

  public final boolean equals(Object paramObject)
  {
    if (this == paramObject);
    JsonPrimitive localJsonPrimitive;
    double d1;
    double d2;
    do
    {
      do
      {
        do
        {
          return true;
          if ((paramObject == null) || (getClass() != paramObject.getClass()))
            return false;
          localJsonPrimitive = (JsonPrimitive)paramObject;
          if (this.d != null)
            break;
        }
        while (localJsonPrimitive.d == null);
        return false;
        if ((!a(this)) || (!a(localJsonPrimitive)))
          break;
      }
      while (getAsNumber().longValue() == localJsonPrimitive.getAsNumber().longValue());
      return false;
      if ((!b(this)) || (!b(localJsonPrimitive)))
        break;
      d1 = getAsNumber().doubleValue();
      d2 = localJsonPrimitive.getAsNumber().doubleValue();
    }
    while ((d1 == d2) || ((Double.isNaN(d1)) && (Double.isNaN(d2))));
    return false;
    return this.d.equals(localJsonPrimitive.d);
  }

  public final BigDecimal getAsBigDecimal()
  {
    if ((this.d instanceof BigDecimal))
      return (BigDecimal)this.d;
    return new BigDecimal(this.d.toString());
  }

  public final BigInteger getAsBigInteger()
  {
    if ((this.d instanceof BigInteger))
      return (BigInteger)this.d;
    return new BigInteger(this.d.toString());
  }

  public final boolean getAsBoolean()
  {
    if (isBoolean())
      return b().booleanValue();
    return Boolean.parseBoolean(getAsString());
  }

  public final byte getAsByte()
  {
    if (isNumber())
      return getAsNumber().byteValue();
    return Byte.parseByte(getAsString());
  }

  public final char getAsCharacter()
  {
    return getAsString().charAt(0);
  }

  public final double getAsDouble()
  {
    if (isNumber())
      return getAsNumber().doubleValue();
    return Double.parseDouble(getAsString());
  }

  public final float getAsFloat()
  {
    if (isNumber())
      return getAsNumber().floatValue();
    return Float.parseFloat(getAsString());
  }

  public final int getAsInt()
  {
    if (isNumber())
      return getAsNumber().intValue();
    return Integer.parseInt(getAsString());
  }

  public final long getAsLong()
  {
    if (isNumber())
      return getAsNumber().longValue();
    return Long.parseLong(getAsString());
  }

  public final Number getAsNumber()
  {
    if ((this.d instanceof String))
      return a((String)this.d);
    return (Number)this.d;
  }

  public final short getAsShort()
  {
    if (isNumber())
      return getAsNumber().shortValue();
    return Short.parseShort(getAsString());
  }

  public final String getAsString()
  {
    if (isNumber())
      return getAsNumber().toString();
    if (isBoolean())
      return b().toString();
    return (String)this.d;
  }

  public final int hashCode()
  {
    if (this.d == null)
      return 31;
    if (a(this))
    {
      long l2 = getAsNumber().longValue();
      return (int)(l2 ^ l2 >>> 32);
    }
    if (b(this))
    {
      long l1 = Double.doubleToLongBits(getAsNumber().doubleValue());
      return (int)(l1 ^ l1 >>> 32);
    }
    return this.d.hashCode();
  }

  public final boolean isBoolean()
  {
    return this.d instanceof Boolean;
  }

  public final boolean isNumber()
  {
    return this.d instanceof Number;
  }

  public final boolean isString()
  {
    return this.d instanceof String;
  }

  protected final void toString(Appendable paramAppendable, j paramj)
  {
    if (isString())
    {
      paramAppendable.append('"');
      paramAppendable.append(paramj.a(this.d.toString()));
      paramAppendable.append('"');
      return;
    }
    paramAppendable.append(this.d.toString());
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.JsonPrimitive
 * JD-Core Version:    0.6.2
 */