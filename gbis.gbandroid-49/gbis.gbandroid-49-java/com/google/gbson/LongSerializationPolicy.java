package com.google.gbson;

public enum LongSerializationPolicy
{
  private final b a;

  static
  {
    LongSerializationPolicy[] arrayOfLongSerializationPolicy = new LongSerializationPolicy[2];
    arrayOfLongSerializationPolicy[0] = DEFAULT;
    arrayOfLongSerializationPolicy[1] = STRING;
  }

  private LongSerializationPolicy(b paramb)
  {
    this.a = paramb;
  }

  public final JsonElement serialize(Long paramLong)
  {
    return this.a.a(paramLong);
  }

  private static final class a
    implements LongSerializationPolicy.b
  {
    public final JsonElement a(Long paramLong)
    {
      return new JsonPrimitive(paramLong);
    }
  }

  private static abstract interface b
  {
    public abstract JsonElement a(Long paramLong);
  }

  private static final class c
    implements LongSerializationPolicy.b
  {
    public final JsonElement a(Long paramLong)
    {
      return new JsonPrimitive(String.valueOf(paramLong));
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.LongSerializationPolicy
 * JD-Core Version:    0.6.2
 */