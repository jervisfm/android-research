package com.google.gbson;

import com.google.gbson.internal..Gson.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Collection;

final class ModifyFirstLetterNamingPolicy extends al
{
  private final LetterModifier a;

  ModifyFirstLetterNamingPolicy(LetterModifier paramLetterModifier)
  {
    this.a = ((LetterModifier).Gson.Preconditions.checkNotNull(paramLetterModifier));
  }

  private static String a(char paramChar, String paramString, int paramInt)
  {
    if (paramInt < paramString.length())
      return paramChar + paramString.substring(paramInt);
    return String.valueOf(paramChar);
  }

  protected final String a(String paramString, Type paramType, Collection<Annotation> paramCollection)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    char c = paramString.charAt(0);
    int i = 0;
    while ((i < -1 + paramString.length()) && (!Character.isLetter(c)))
    {
      localStringBuilder.append(c);
      i++;
      c = paramString.charAt(i);
    }
    if (i == paramString.length())
      paramString = localStringBuilder.toString();
    int j;
    do
    {
      return paramString;
      LetterModifier localLetterModifier1 = this.a;
      LetterModifier localLetterModifier2 = LetterModifier.UPPER;
      j = 0;
      if (localLetterModifier1 == localLetterModifier2)
        j = 1;
      if ((j != 0) && (!Character.isUpperCase(c)))
        return a(Character.toUpperCase(c), paramString, i + 1);
    }
    while ((j != 0) || (!Character.isUpperCase(c)));
    return a(Character.toLowerCase(c), paramString, i + 1);
  }

  public static enum LetterModifier
  {
    static
    {
      LOWER = new LetterModifier("LOWER", 1);
      LetterModifier[] arrayOfLetterModifier = new LetterModifier[2];
      arrayOfLetterModifier[0] = UPPER;
      arrayOfLetterModifier[1] = LOWER;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.ModifyFirstLetterNamingPolicy
 * JD-Core Version:    0.6.2
 */