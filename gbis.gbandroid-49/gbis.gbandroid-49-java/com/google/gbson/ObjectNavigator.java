package com.google.gbson;

import com.google.gbson.internal..Gson.Types;
import java.lang.reflect.Type;

final class ObjectNavigator
{
  private final ExclusionStrategy a;
  private final am b;

  ObjectNavigator(ExclusionStrategy paramExclusionStrategy)
  {
    if (paramExclusionStrategy == null)
      paramExclusionStrategy = new af();
    this.a = paramExclusionStrategy;
    this.b = new am(this.a);
  }

  private static boolean a(Object paramObject)
  {
    Class localClass = paramObject.getClass();
    return (localClass == Object.class) || (localClass == String.class) || (ak.b(localClass).isPrimitive());
  }

  public final void a(ah paramah, Visitor paramVisitor)
  {
    if (this.a.shouldSkipClass(.Gson.Types.getRawType(paramah.a)));
    Object localObject1;
    do
    {
      do
        return;
      while (paramVisitor.visitUsingCustomHandler(paramah));
      localObject1 = paramah.a();
      if (localObject1 == null)
        localObject1 = paramVisitor.getTarget();
    }
    while (localObject1 == null);
    paramah.a(localObject1);
    paramVisitor.start(paramah);
    while (true)
    {
      try
      {
        if (.Gson.Types.isArray(paramah.a))
        {
          paramVisitor.visitArray(localObject1, paramah.a);
          return;
        }
        if ((paramah.a == Object.class) && (a(localObject1)))
        {
          paramVisitor.visitPrimitive(localObject1);
          paramVisitor.getTarget();
          continue;
        }
      }
      finally
      {
        paramVisitor.end(paramah);
      }
      paramVisitor.startVisitingObject(localObject1);
      this.b.a(paramah, paramVisitor);
    }
  }

  public static abstract interface Visitor
  {
    public abstract void end(ah paramah);

    public abstract Object getTarget();

    public abstract void start(ah paramah);

    public abstract void startVisitingObject(Object paramObject);

    public abstract void visitArray(Object paramObject, Type paramType);

    public abstract void visitArrayField(FieldAttributes paramFieldAttributes, Type paramType, Object paramObject);

    public abstract boolean visitFieldUsingCustomHandler(FieldAttributes paramFieldAttributes, Type paramType, Object paramObject);

    public abstract void visitObjectField(FieldAttributes paramFieldAttributes, Type paramType, Object paramObject);

    public abstract void visitPrimitive(Object paramObject);

    public abstract boolean visitUsingCustomHandler(ah paramah);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.ObjectNavigator
 * JD-Core Version:    0.6.2
 */