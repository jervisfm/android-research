package com.google.gbson;

final class a
  implements ExclusionStrategy
{
  private static boolean a(Class<?> paramClass)
  {
    return (!Enum.class.isAssignableFrom(paramClass)) && ((paramClass.isAnonymousClass()) || (paramClass.isLocalClass()));
  }

  public final boolean shouldSkipClass(Class<?> paramClass)
  {
    return a(paramClass);
  }

  public final boolean shouldSkipField(FieldAttributes paramFieldAttributes)
  {
    return a(paramFieldAttributes.getDeclaredClass());
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.a
 * JD-Core Version:    0.6.2
 */