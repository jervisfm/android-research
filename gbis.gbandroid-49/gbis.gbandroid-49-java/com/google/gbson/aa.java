package com.google.gbson;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

final class aa extends b
  implements JsonDeserializer<Map<?, ?>>, JsonSerializer<Map<?, ?>>
{
  private static JsonElement a(Map<?, ?> paramMap, Type paramType, JsonSerializationContext paramJsonSerializationContext)
  {
    int i = 0;
    Type[] arrayOfType = a(paramType);
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = paramMap.entrySet().iterator();
    int j = 0;
    if (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      JsonElement localJsonElement = a(paramJsonSerializationContext, localEntry.getKey(), arrayOfType[0]);
      if ((localJsonElement.isJsonObject()) || (localJsonElement.isJsonArray()));
      for (int k = 1; ; k = 0)
      {
        j |= k;
        localArrayList.add(localJsonElement);
        localArrayList.add(a(paramJsonSerializationContext, localEntry.getValue(), arrayOfType[1]));
        break;
      }
    }
    if (j != 0)
    {
      JsonArray localJsonArray1 = new JsonArray();
      while (i < localArrayList.size())
      {
        JsonArray localJsonArray2 = new JsonArray();
        localJsonArray2.add((JsonElement)localArrayList.get(i));
        localJsonArray2.add((JsonElement)localArrayList.get(i + 1));
        localJsonArray1.add(localJsonArray2);
        i += 2;
      }
      return localJsonArray1;
    }
    JsonObject localJsonObject = new JsonObject();
    while (i < localArrayList.size())
    {
      localJsonObject.add(((JsonElement)localArrayList.get(i)).getAsString(), (JsonElement)localArrayList.get(i + 1));
      i += 2;
    }
    a(paramMap, paramMap.size(), localJsonObject, localJsonObject.entrySet().size());
    return localJsonObject;
  }

  private static Map<?, ?> a(JsonElement paramJsonElement, Type paramType, JsonDeserializationContext paramJsonDeserializationContext)
  {
    Map localMap = a(paramType, paramJsonDeserializationContext);
    Type[] arrayOfType = a(paramType);
    if (paramJsonElement.isJsonArray())
    {
      JsonArray localJsonArray1 = paramJsonElement.getAsJsonArray();
      for (int i = 0; i < localJsonArray1.size(); i++)
      {
        JsonArray localJsonArray2 = localJsonArray1.get(i).getAsJsonArray();
        localMap.put(paramJsonDeserializationContext.deserialize(localJsonArray2.get(0), arrayOfType[0]), paramJsonDeserializationContext.deserialize(localJsonArray2.get(1), arrayOfType[1]));
      }
      a(localJsonArray1, localJsonArray1.size(), localMap, localMap.size());
      return localMap;
    }
    JsonObject localJsonObject = paramJsonElement.getAsJsonObject();
    Iterator localIterator = localJsonObject.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      localMap.put(paramJsonDeserializationContext.deserialize(new JsonPrimitive((String)localEntry.getKey()), arrayOfType[0]), paramJsonDeserializationContext.deserialize((JsonElement)localEntry.getValue(), arrayOfType[1]));
    }
    a(localJsonObject, localJsonObject.entrySet().size(), localMap, localMap.size());
    return localMap;
  }

  private static void a(Object paramObject1, int paramInt1, Object paramObject2, int paramInt2)
  {
    if (paramInt1 != paramInt2)
      throw new JsonSyntaxException("Input size " + paramInt1 + " != output size " + paramInt2 + " for input " + paramObject1 + " and output " + paramObject2);
  }

  private static Type[] a(Type paramType)
  {
    Type[] arrayOfType;
    if ((paramType instanceof ParameterizedType))
    {
      arrayOfType = ((ParameterizedType)paramType).getActualTypeArguments();
      if (arrayOfType.length != 2)
        throw new IllegalArgumentException("MapAsArrayTypeAdapter cannot handle " + paramType);
    }
    else
    {
      arrayOfType = new Type[] { Object.class, Object.class };
    }
    return arrayOfType;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.aa
 * JD-Core Version:    0.6.2
 */