package com.google.gbson;

import com.google.gbson.internal..Gson.Types;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

final class ab extends b
{
  private static JsonElement a(Map paramMap, Type paramType, JsonSerializationContext paramJsonSerializationContext)
  {
    JsonObject localJsonObject = new JsonObject();
    if ((paramType instanceof ParameterizedType));
    for (Type localType = .Gson.Types.getMapKeyAndValueTypes(paramType, .Gson.Types.getRawType(paramType))[1]; ; localType = null)
    {
      Iterator localIterator = paramMap.entrySet().iterator();
      while (localIterator.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator.next();
        Object localObject1 = localEntry.getValue();
        Object localObject3;
        if (localObject1 == null)
        {
          localObject3 = JsonNull.c();
          localJsonObject.add(String.valueOf(localEntry.getKey()), (JsonElement)localObject3);
        }
        else
        {
          if (localType == null);
          for (Object localObject2 = localObject1.getClass(); ; localObject2 = localType)
          {
            localObject3 = a(paramJsonSerializationContext, localObject1, (Type)localObject2);
            break;
          }
        }
      }
      return localJsonObject;
    }
  }

  private static Map a(JsonElement paramJsonElement, Type paramType, JsonDeserializationContext paramJsonDeserializationContext)
  {
    Map localMap = a(paramType, paramJsonDeserializationContext);
    Type[] arrayOfType = .Gson.Types.getMapKeyAndValueTypes(paramType, .Gson.Types.getRawType(paramType));
    Iterator localIterator = paramJsonElement.getAsJsonObject().entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      localMap.put(paramJsonDeserializationContext.deserialize(new JsonPrimitive((String)localEntry.getKey()), arrayOfType[0]), paramJsonDeserializationContext.deserialize((JsonElement)localEntry.getValue(), arrayOfType[1]));
    }
    return localMap;
  }

  public final String toString()
  {
    return ab.class.getSimpleName();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.ab
 * JD-Core Version:    0.6.2
 */