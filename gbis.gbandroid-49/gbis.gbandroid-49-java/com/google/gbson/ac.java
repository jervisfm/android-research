package com.google.gbson;

import com.google.gbson.internal..Gson.Types;
import java.lang.reflect.Array;
import java.lang.reflect.Type;

final class ac
  implements ag
{
  private static final aq a = aq.a();
  private static final g b = new g(500);
  private final aj<InstanceCreator<?>> c;

  public ac(aj<InstanceCreator<?>> paramaj)
  {
    this.c = paramaj;
  }

  private static <T> T b(Type paramType)
  {
    try
    {
      Class localClass = .Gson.Types.getRawType(paramType);
      Object localObject1 = b.a(localClass);
      if (localObject1 == null)
      {
        Object localObject2 = a.a(localClass);
        localObject1 = localObject2;
      }
      return localObject1;
    }
    catch (Exception localException)
    {
      throw new RuntimeException("Unable to invoke no-args constructor for " + paramType + ". Register an InstanceCreator with Gson for this type may fix this problem.", localException);
    }
  }

  public final <T> T a(Type paramType)
  {
    InstanceCreator localInstanceCreator = (InstanceCreator)this.c.a(paramType);
    if (localInstanceCreator != null)
      return localInstanceCreator.createInstance(paramType);
    return b(paramType);
  }

  public final Object a(Type paramType, int paramInt)
  {
    return Array.newInstance(.Gson.Types.getRawType(paramType), paramInt);
  }

  public final String toString()
  {
    return this.c.toString();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.ac
 * JD-Core Version:    0.6.2
 */