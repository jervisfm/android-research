package com.google.gbson;

import com.google.gbson.internal..Gson.Preconditions;
import java.util.Iterator;
import java.util.Stack;

final class ad
{
  private final Stack<ah> a = new Stack();

  public final ah a()
  {
    return (ah)this.a.pop();
  }

  public final ah a(ah paramah)
  {
    .Gson.Preconditions.checkNotNull(paramah);
    return (ah)this.a.push(paramah);
  }

  public final boolean b(ah paramah)
  {
    if (paramah == null)
      return false;
    Iterator localIterator = this.a.iterator();
    while (localIterator.hasNext())
    {
      ah localah = (ah)localIterator.next();
      if ((localah.a() == paramah.a()) && (localah.a.equals(paramah.a)))
        return true;
    }
    return false;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.ad
 * JD-Core Version:    0.6.2
 */