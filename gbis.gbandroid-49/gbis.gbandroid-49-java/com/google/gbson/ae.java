package com.google.gbson;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

final class ae
  implements ExclusionStrategy
{
  private final Collection<Integer> a = new HashSet();

  public ae(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt != null)
    {
      int i = paramArrayOfInt.length;
      for (int j = 0; j < i; j++)
      {
        int k = paramArrayOfInt[j];
        this.a.add(Integer.valueOf(k));
      }
    }
  }

  public final boolean shouldSkipClass(Class<?> paramClass)
  {
    return false;
  }

  public final boolean shouldSkipField(FieldAttributes paramFieldAttributes)
  {
    Iterator localIterator = this.a.iterator();
    while (localIterator.hasNext())
      if (paramFieldAttributes.hasModifier(((Integer)localIterator.next()).intValue()))
        return true;
    return false;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.ae
 * JD-Core Version:    0.6.2
 */