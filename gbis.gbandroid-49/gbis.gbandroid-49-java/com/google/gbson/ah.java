package com.google.gbson;

import java.lang.reflect.Type;

final class ah
{
  final Type a;
  private Object b;
  private final boolean c;

  ah(Object paramObject, Type paramType, boolean paramBoolean)
  {
    this.b = paramObject;
    this.a = paramType;
    this.c = paramBoolean;
  }

  private static Type a(Type paramType, Class<?> paramClass)
  {
    if ((paramType instanceof Class))
    {
      if (((Class)paramType).isAssignableFrom(paramClass))
        paramType = paramClass;
      if (paramType == Object.class)
        paramType = paramClass;
    }
    return paramType;
  }

  private ah d()
  {
    if ((this.c) || (this.b == null));
    Type localType;
    do
    {
      return this;
      localType = a(this.a, this.b.getClass());
    }
    while (localType == this.a);
    return new ah(this.b, localType, this.c);
  }

  final <HANDLER> ai<HANDLER, ah> a(aj<HANDLER> paramaj)
  {
    if ((!this.c) && (this.b != null))
    {
      ah localah = d();
      Object localObject2 = paramaj.a(localah.a);
      if (localObject2 != null)
        return new ai(localObject2, localah);
    }
    Object localObject1 = paramaj.a(this.a);
    if (localObject1 == null)
      return null;
    return new ai(localObject1, this);
  }

  final Object a()
  {
    return this.b;
  }

  final void a(Object paramObject)
  {
    this.b = paramObject;
  }

  final Type b()
  {
    return this.a;
  }

  final Type c()
  {
    if ((this.c) || (this.b == null))
      return this.a;
    return a(this.a, this.b.getClass());
  }

  public final boolean equals(Object paramObject)
  {
    if (this == paramObject);
    ah localah;
    do
    {
      return true;
      if (paramObject == null)
        return false;
      if (getClass() != paramObject.getClass())
        return false;
      localah = (ah)paramObject;
      if (this.b == null)
      {
        if (localah.b != null)
          return false;
      }
      else if (this.b != localah.b)
        return false;
      if (this.a == null)
      {
        if (localah.a != null)
          return false;
      }
      else if (!this.a.equals(localah.a))
        return false;
    }
    while (this.c == localah.c);
    return false;
  }

  public final int hashCode()
  {
    if (this.b == null)
      return 31;
    return this.b.hashCode();
  }

  public final String toString()
  {
    Object[] arrayOfObject = new Object[3];
    arrayOfObject[0] = Boolean.valueOf(this.c);
    arrayOfObject[1] = this.a;
    arrayOfObject[2] = this.b;
    return String.format("preserveType: %b, type: %s, obj: %s", arrayOfObject);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.ah
 * JD-Core Version:    0.6.2
 */