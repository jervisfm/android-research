package com.google.gbson;

final class ai<FIRST, SECOND>
{
  public final FIRST a;
  public final SECOND b;

  public ai(FIRST paramFIRST, SECOND paramSECOND)
  {
    this.a = paramFIRST;
    this.b = paramSECOND;
  }

  private static boolean a(Object paramObject1, Object paramObject2)
  {
    return (paramObject1 == paramObject2) || ((paramObject1 != null) && (paramObject1.equals(paramObject2)));
  }

  public final boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof ai));
    ai localai;
    do
    {
      return false;
      localai = (ai)paramObject;
    }
    while ((!a(this.a, localai.a)) || (!a(this.b, localai.b)));
    return true;
  }

  public final int hashCode()
  {
    if (this.a != null);
    for (int i = this.a.hashCode(); ; i = 0)
    {
      int j = i * 17;
      Object localObject = this.b;
      int k = 0;
      if (localObject != null)
        k = this.b.hashCode();
      return j + k * 17;
    }
  }

  public final String toString()
  {
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = this.a;
    arrayOfObject[1] = this.b;
    return String.format("{%s,%s}", arrayOfObject);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.ai
 * JD-Core Version:    0.6.2
 */