package com.google.gbson;

import com.google.gbson.internal..Gson.Types;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

final class aj<T>
{
  private static final Logger a = Logger.getLogger(aj.class.getName());
  private final Map<Type, T> b = new HashMap();
  private final List<ai<Class<?>, T>> c = new ArrayList();
  private boolean d = true;

  private int a(Class<?> paramClass)
  {
    for (int i = -1 + this.c.size(); i >= 0; i--)
      if (paramClass.isAssignableFrom((Class)((ai)this.c.get(i)).a))
        return i;
    return -1;
  }

  private void a(ai<Class<?>, T> paramai)
  {
    try
    {
      if (!this.d)
        throw new IllegalStateException("Attempted to modify an unmodifiable map.");
    }
    finally
    {
    }
    int i = c((Class)paramai.a);
    if (i >= 0)
    {
      a.log(Level.WARNING, "Overriding the existing type handler for {0}", paramai.a);
      this.c.remove(i);
    }
    int j = a((Class)paramai.a);
    if (j >= 0)
      throw new IllegalArgumentException("The specified type handler for type " + paramai.a + " hides the previously registered type hierarchy handler for " + ((ai)this.c.get(j)).a + ". Gson does not allow this.");
    this.c.add(0, paramai);
  }

  private T b(Class<?> paramClass)
  {
    Iterator localIterator = this.c.iterator();
    while (localIterator.hasNext())
    {
      ai localai = (ai)localIterator.next();
      if (((Class)localai.a).isAssignableFrom(paramClass))
        return localai.b;
    }
    return null;
  }

  private int c(Class<?> paramClass)
  {
    try
    {
      int i = -1 + this.c.size();
      if (i >= 0)
      {
        boolean bool = paramClass.equals(((ai)this.c.get(i)).a);
        if (!bool);
      }
      for (int j = i; ; j = -1)
      {
        return j;
        i--;
        break;
      }
    }
    finally
    {
    }
  }

  private static String c(Type paramType)
  {
    return .Gson.Types.getRawType(paramType).getSimpleName();
  }

  public final T a(Type paramType)
  {
    try
    {
      Object localObject2 = this.b.get(paramType);
      if (localObject2 == null)
      {
        Class localClass = .Gson.Types.getRawType(paramType);
        if (localClass != paramType)
          localObject2 = a(localClass);
        if (localObject2 == null)
        {
          Object localObject3 = b(localClass);
          localObject2 = localObject3;
        }
      }
      return localObject2;
    }
    finally
    {
    }
  }

  public final void a()
  {
    try
    {
      this.d = false;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public final void a(aj<T> paramaj)
  {
    try
    {
      if (!this.d)
        throw new IllegalStateException("Attempted to modify an unmodifiable map.");
    }
    finally
    {
    }
    Iterator localIterator = paramaj.b.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      if (!this.b.containsKey(localEntry.getKey()))
        a((Type)localEntry.getKey(), localEntry.getValue());
    }
    for (int i = -1 + paramaj.c.size(); i >= 0; i--)
    {
      ai localai = (ai)paramaj.c.get(i);
      if (c((Class)localai.a) < 0)
        a(localai);
    }
  }

  public final void a(Class<?> paramClass, T paramT)
  {
    try
    {
      a(new ai(paramClass, paramT));
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public final void a(Type paramType, T paramT)
  {
    try
    {
      if (!this.d)
        throw new IllegalStateException("Attempted to modify an unmodifiable map.");
    }
    finally
    {
    }
    if (b(paramType))
      a.log(Level.WARNING, "Overriding the existing type handler for {0}", paramType);
    this.b.put(paramType, paramT);
  }

  public final aj<T> b()
  {
    try
    {
      aj localaj = new aj();
      localaj.b.putAll(this.b);
      localaj.c.addAll(this.c);
      return localaj;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public final void b(aj<T> paramaj)
  {
    try
    {
      if (!this.d)
        throw new IllegalStateException("Attempted to modify an unmodifiable map.");
    }
    finally
    {
    }
    Iterator localIterator = paramaj.b.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      a((Type)localEntry.getKey(), localEntry.getValue());
    }
    for (int i = -1 + paramaj.c.size(); i >= 0; i--)
      a((ai)paramaj.c.get(i));
  }

  public final void b(Type paramType, T paramT)
  {
    try
    {
      if (!this.d)
        throw new IllegalStateException("Attempted to modify an unmodifiable map.");
    }
    finally
    {
    }
    if (!this.b.containsKey(paramType))
      a(paramType, paramT);
  }

  public final boolean b(Type paramType)
  {
    try
    {
      boolean bool = this.b.containsKey(paramType);
      return bool;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public final String toString()
  {
    int i = 1;
    StringBuilder localStringBuilder = new StringBuilder("{mapForTypeHierarchy:{");
    Iterator localIterator1 = this.c.iterator();
    int j = i;
    if (localIterator1.hasNext())
    {
      ai localai = (ai)localIterator1.next();
      if (j != 0);
      for (int k = 0; ; k = j)
      {
        localStringBuilder.append(c((Type)localai.a)).append(':');
        localStringBuilder.append(localai.b);
        j = k;
        break;
        localStringBuilder.append(',');
      }
    }
    localStringBuilder.append("},map:{");
    Iterator localIterator2 = this.b.entrySet().iterator();
    if (localIterator2.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator2.next();
      if (i != 0)
        i = 0;
      while (true)
      {
        localStringBuilder.append(c((Type)localEntry.getKey())).append(':');
        localStringBuilder.append(localEntry.getValue());
        break;
        localStringBuilder.append(',');
      }
    }
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.aj
 * JD-Core Version:    0.6.2
 */