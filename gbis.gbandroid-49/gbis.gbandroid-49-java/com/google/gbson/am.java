package com.google.gbson;

import com.google.gbson.internal..Gson.Preconditions;
import com.google.gbson.internal..Gson.Types;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

final class am
{
  private static final c<Type, List<FieldAttributes>> a = new z(500);
  private final ExclusionStrategy b;

  am(ExclusionStrategy paramExclusionStrategy)
  {
    this.b = ((ExclusionStrategy).Gson.Preconditions.checkNotNull(paramExclusionStrategy));
  }

  private static List<Class<?>> a(Type paramType)
  {
    ArrayList localArrayList = new ArrayList();
    for (Class localClass = .Gson.Types.getRawType(paramType); (localClass != null) && (!localClass.equals(Object.class)); localClass = localClass.getSuperclass())
      if (!localClass.isSynthetic())
        localArrayList.add(localClass);
    return localArrayList;
  }

  private static List<FieldAttributes> a(Type paramType1, Type paramType2)
  {
    Object localObject = (List)a.a(paramType1);
    if (localObject == null)
    {
      ArrayList localArrayList = new ArrayList();
      Iterator localIterator = a(paramType1).iterator();
      while (localIterator.hasNext())
      {
        Class localClass = (Class)localIterator.next();
        Field[] arrayOfField = localClass.getDeclaredFields();
        AccessibleObject.setAccessible(arrayOfField, true);
        int i = arrayOfField.length;
        for (int j = 0; j < i; j++)
          localArrayList.add(new FieldAttributes(localClass, arrayOfField[j], paramType2));
      }
      a.a(paramType1, localArrayList);
      localObject = localArrayList;
    }
    return localObject;
  }

  final void a(ah paramah, ObjectNavigator.Visitor paramVisitor)
  {
    Type localType1 = paramah.c();
    Object localObject = paramah.a();
    Iterator localIterator = a(localType1, paramah.b()).iterator();
    while (localIterator.hasNext())
    {
      FieldAttributes localFieldAttributes = (FieldAttributes)localIterator.next();
      if ((!this.b.shouldSkipField(localFieldAttributes)) && (!this.b.shouldSkipClass(localFieldAttributes.getDeclaredClass())))
      {
        Type localType2 = localFieldAttributes.c();
        if (!paramVisitor.visitFieldUsingCustomHandler(localFieldAttributes, localType2, localObject))
          if (.Gson.Types.isArray(localType2))
            paramVisitor.visitArrayField(localFieldAttributes, localType2, localObject);
          else
            paramVisitor.visitObjectField(localFieldAttributes, localType2, localObject);
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.am
 * JD-Core Version:    0.6.2
 */