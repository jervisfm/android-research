package com.google.gbson;

import com.google.gbson.annotations.SerializedName;

final class an
  implements m
{
  private final m a;

  an(m paramm)
  {
    this.a = paramm;
  }

  public final String a(FieldAttributes paramFieldAttributes)
  {
    SerializedName localSerializedName = (SerializedName)paramFieldAttributes.getAnnotation(SerializedName.class);
    if (localSerializedName == null)
      return this.a.a(paramFieldAttributes);
    return localSerializedName.value();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.an
 * JD-Core Version:    0.6.2
 */