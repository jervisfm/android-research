package com.google.gbson;

import com.google.gbson.stream.JsonReader;
import com.google.gbson.stream.JsonWriter;
import com.google.gbson.stream.MalformedJsonException;
import java.io.EOFException;
import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

final class ao
{
  static JsonElement a(JsonReader paramJsonReader)
  {
    int i = 1;
    try
    {
      paramJsonReader.peek();
      i = 0;
      JsonElement localJsonElement = b(paramJsonReader);
      return localJsonElement;
    }
    catch (EOFException localEOFException)
    {
      if (i != 0)
        return JsonNull.c();
      throw new JsonIOException(localEOFException);
    }
    catch (MalformedJsonException localMalformedJsonException)
    {
      throw new JsonSyntaxException(localMalformedJsonException);
    }
    catch (IOException localIOException)
    {
      throw new JsonIOException(localIOException);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      throw new JsonSyntaxException(localNumberFormatException);
    }
  }

  static Writer a(Appendable paramAppendable)
  {
    if ((paramAppendable instanceof Writer))
      return (Writer)paramAppendable;
    return new a(paramAppendable, (byte)0);
  }

  static void a(JsonElement paramJsonElement, boolean paramBoolean, JsonWriter paramJsonWriter)
  {
    if ((paramJsonElement == null) || (paramJsonElement.isJsonNull()))
    {
      if (paramBoolean)
        paramJsonWriter.nullValue();
      return;
    }
    if (paramJsonElement.isJsonPrimitive())
    {
      JsonPrimitive localJsonPrimitive = paramJsonElement.getAsJsonPrimitive();
      if (localJsonPrimitive.isNumber())
      {
        paramJsonWriter.value(localJsonPrimitive.getAsNumber());
        return;
      }
      if (localJsonPrimitive.isBoolean())
      {
        paramJsonWriter.value(localJsonPrimitive.getAsBoolean());
        return;
      }
      paramJsonWriter.value(localJsonPrimitive.getAsString());
      return;
    }
    if (paramJsonElement.isJsonArray())
    {
      paramJsonWriter.beginArray();
      Iterator localIterator2 = paramJsonElement.getAsJsonArray().iterator();
      while (localIterator2.hasNext())
      {
        JsonElement localJsonElement2 = (JsonElement)localIterator2.next();
        if (localJsonElement2.isJsonNull())
          paramJsonWriter.nullValue();
        else
          a(localJsonElement2, paramBoolean, paramJsonWriter);
      }
      paramJsonWriter.endArray();
      return;
    }
    if (paramJsonElement.isJsonObject())
    {
      paramJsonWriter.beginObject();
      Iterator localIterator1 = paramJsonElement.getAsJsonObject().entrySet().iterator();
      while (localIterator1.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator1.next();
        JsonElement localJsonElement1 = (JsonElement)localEntry.getValue();
        if ((paramBoolean) || (!localJsonElement1.isJsonNull()))
        {
          paramJsonWriter.name((String)localEntry.getKey());
          a(localJsonElement1, paramBoolean, paramJsonWriter);
        }
      }
      paramJsonWriter.endObject();
      return;
    }
    throw new IllegalArgumentException("Couldn't write " + paramJsonElement.getClass());
  }

  private static JsonElement b(JsonReader paramJsonReader)
  {
    switch (1.a[paramJsonReader.peek().ordinal()])
    {
    default:
      throw new IllegalArgumentException();
    case 1:
      return new JsonPrimitive(paramJsonReader.nextString());
    case 2:
      return new JsonPrimitive(JsonPrimitive.a(paramJsonReader.nextString()));
    case 3:
      return new JsonPrimitive(Boolean.valueOf(paramJsonReader.nextBoolean()));
    case 4:
      paramJsonReader.nextNull();
      return JsonNull.c();
    case 5:
      JsonArray localJsonArray = new JsonArray();
      paramJsonReader.beginArray();
      while (paramJsonReader.hasNext())
        localJsonArray.add(b(paramJsonReader));
      paramJsonReader.endArray();
      return localJsonArray;
    case 6:
    }
    JsonObject localJsonObject = new JsonObject();
    paramJsonReader.beginObject();
    while (paramJsonReader.hasNext())
      localJsonObject.add(paramJsonReader.nextName(), b(paramJsonReader));
    paramJsonReader.endObject();
    return localJsonObject;
  }

  private static final class a extends Writer
  {
    private final Appendable a;
    private final a b = new a();

    private a(Appendable paramAppendable)
    {
      this.a = paramAppendable;
    }

    public final void close()
    {
    }

    public final void flush()
    {
    }

    public final void write(int paramInt)
    {
      this.a.append((char)paramInt);
    }

    public final void write(char[] paramArrayOfChar, int paramInt1, int paramInt2)
    {
      this.b.a = paramArrayOfChar;
      this.a.append(this.b, paramInt1, paramInt1 + paramInt2);
    }

    static final class a
      implements CharSequence
    {
      char[] a;

      public final char charAt(int paramInt)
      {
        return this.a[paramInt];
      }

      public final int length()
      {
        return this.a.length;
      }

      public final CharSequence subSequence(int paramInt1, int paramInt2)
      {
        return new String(this.a, paramInt1, paramInt2 - paramInt1);
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.ao
 * JD-Core Version:    0.6.2
 */