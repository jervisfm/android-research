package com.google.gbson;

final class ap
  implements ExclusionStrategy
{
  private final boolean a = true;

  public final boolean shouldSkipClass(Class<?> paramClass)
  {
    return false;
  }

  public final boolean shouldSkipField(FieldAttributes paramFieldAttributes)
  {
    return (this.a) && (paramFieldAttributes.a());
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.ap
 * JD-Core Version:    0.6.2
 */