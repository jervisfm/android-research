package com.google.gbson;

import com.google.gbson.annotations.Since;
import com.google.gbson.annotations.Until;
import com.google.gbson.internal..Gson.Preconditions;

final class aw
  implements ExclusionStrategy
{
  private final double a;

  aw(double paramDouble)
  {
    if (paramDouble >= 0.0D);
    for (boolean bool = true; ; bool = false)
    {
      .Gson.Preconditions.checkArgument(bool);
      this.a = paramDouble;
      return;
    }
  }

  private boolean a(Since paramSince)
  {
    return (paramSince == null) || (paramSince.value() <= this.a);
  }

  private boolean a(Since paramSince, Until paramUntil)
  {
    return (a(paramSince)) && (a(paramUntil));
  }

  private boolean a(Until paramUntil)
  {
    return (paramUntil == null) || (paramUntil.value() > this.a);
  }

  public final boolean shouldSkipClass(Class<?> paramClass)
  {
    return !a((Since)paramClass.getAnnotation(Since.class), (Until)paramClass.getAnnotation(Until.class));
  }

  public final boolean shouldSkipField(FieldAttributes paramFieldAttributes)
  {
    return !a((Since)paramFieldAttributes.getAnnotation(Since.class), (Until)paramFieldAttributes.getAnnotation(Until.class));
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.aw
 * JD-Core Version:    0.6.2
 */