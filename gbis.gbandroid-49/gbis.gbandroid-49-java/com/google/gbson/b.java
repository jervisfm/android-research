package com.google.gbson;

import java.lang.reflect.Type;
import java.util.Map;

abstract class b
  implements JsonDeserializer<Map<?, ?>>, JsonSerializer<Map<?, ?>>
{
  protected static final JsonElement a(JsonSerializationContext paramJsonSerializationContext, Object paramObject, Type paramType)
  {
    return ((v)paramJsonSerializationContext).a(paramObject, paramType, false);
  }

  protected static final Map<Object, Object> a(Type paramType, JsonDeserializationContext paramJsonDeserializationContext)
  {
    return (Map)((r)paramJsonDeserializationContext).a().a(paramType);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.b
 * JD-Core Version:    0.6.2
 */