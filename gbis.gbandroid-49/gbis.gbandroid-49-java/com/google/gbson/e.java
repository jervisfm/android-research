package com.google.gbson;

final class e extends RuntimeException
{
  private static final long serialVersionUID = 7444343294106513081L;
  private final Object a;

  e(Object paramObject)
  {
    super("circular reference error");
    this.a = paramObject;
  }

  public final IllegalStateException a(FieldAttributes paramFieldAttributes)
  {
    StringBuilder localStringBuilder = new StringBuilder(getMessage());
    if (paramFieldAttributes != null)
      localStringBuilder.append("\n  Offending field: ").append(paramFieldAttributes.getName() + "\n");
    if (this.a != null)
      localStringBuilder.append("\n  Offending object: ").append(this.a);
    return new IllegalStateException(localStringBuilder.toString(), this);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.e
 * JD-Core Version:    0.6.2
 */