package com.google.gbson;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Collection;

abstract class f extends al
{
  private final al[] a;

  public f(al[] paramArrayOfal)
  {
    if (paramArrayOfal == null)
      throw new NullPointerException("naming policies can not be null.");
    this.a = paramArrayOfal;
  }

  protected final String a(String paramString, Type paramType, Collection<Annotation> paramCollection)
  {
    al[] arrayOfal = this.a;
    int i = arrayOfal.length;
    for (int j = 0; j < i; j++)
      paramString = arrayOfal[j].a(paramString, paramType, paramCollection);
    return paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.f
 * JD-Core Version:    0.6.2
 */