package com.google.gbson;

import java.lang.reflect.Constructor;

final class g
{
  private static final Constructor<a> a = a();
  private final c<Class<?>, Constructor<?>> b;

  public g()
  {
    this(200);
  }

  public g(int paramInt)
  {
    this.b = new z(paramInt);
  }

  private static final Constructor<a> a()
  {
    try
    {
      Constructor localConstructor = c(a.class);
      return localConstructor;
    }
    catch (Exception localException)
    {
    }
    return null;
  }

  private <T> Constructor<T> b(Class<T> paramClass)
  {
    Constructor localConstructor1 = (Constructor)this.b.a(paramClass);
    if (localConstructor1 != null)
    {
      if (localConstructor1 == a)
        localConstructor1 = null;
      return localConstructor1;
    }
    Constructor localConstructor2 = c(paramClass);
    if (localConstructor2 != null)
    {
      this.b.a(paramClass, localConstructor2);
      return localConstructor2;
    }
    this.b.a(paramClass, a);
    return localConstructor2;
  }

  private static <T> Constructor<T> c(Class<T> paramClass)
  {
    try
    {
      Constructor localConstructor = paramClass.getDeclaredConstructor(new Class[0]);
      localConstructor.setAccessible(true);
      return localConstructor;
    }
    catch (Exception localException)
    {
    }
    return null;
  }

  public final <T> T a(Class<T> paramClass)
  {
    Constructor localConstructor = b(paramClass);
    if (localConstructor != null)
      return localConstructor.newInstance(new Object[0]);
    return null;
  }

  private static final class a
  {
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.g
 * JD-Core Version:    0.6.2
 */