package com.google.gbson;

import com.google.gbson.internal..Gson.Preconditions;
import java.util.Collection;
import java.util.Iterator;

final class i
  implements ExclusionStrategy
{
  private final Collection<ExclusionStrategy> a;

  i(Collection<ExclusionStrategy> paramCollection)
  {
    this.a = ((Collection).Gson.Preconditions.checkNotNull(paramCollection));
  }

  public final boolean shouldSkipClass(Class<?> paramClass)
  {
    Iterator localIterator = this.a.iterator();
    while (localIterator.hasNext())
      if (((ExclusionStrategy)localIterator.next()).shouldSkipClass(paramClass))
        return true;
    return false;
  }

  public final boolean shouldSkipField(FieldAttributes paramFieldAttributes)
  {
    Iterator localIterator = this.a.iterator();
    while (localIterator.hasNext())
      if (((ExclusionStrategy)localIterator.next()).shouldSkipField(paramFieldAttributes))
        return true;
    return false;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.i
 * JD-Core Version:    0.6.2
 */