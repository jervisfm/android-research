package com.google.gbson.internal;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.GenericDeclaration;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;

public final class $Gson$Types
{
  static final Type[] a = new Type[0];

  private static int a(Object[] paramArrayOfObject, Object paramObject)
  {
    for (int i = 0; i < paramArrayOfObject.length; i++)
      if (paramObject.equals(paramArrayOfObject[i]))
        return i;
    throw new NoSuchElementException();
  }

  private static Class<?> a(TypeVariable paramTypeVariable)
  {
    GenericDeclaration localGenericDeclaration = paramTypeVariable.getGenericDeclaration();
    if ((localGenericDeclaration instanceof Class))
      return (Class)localGenericDeclaration;
    return null;
  }

  private static Type a(Type paramType, Class<?> paramClass1, Class<?> paramClass2)
  {
    if (paramClass2 == paramClass1)
      return paramType;
    if (paramClass2.isInterface())
    {
      Class[] arrayOfClass = paramClass1.getInterfaces();
      int i = 0;
      int j = arrayOfClass.length;
      while (i < j)
      {
        if (arrayOfClass[i] == paramClass2)
          return paramClass1.getGenericInterfaces()[i];
        if (paramClass2.isAssignableFrom(arrayOfClass[i]))
          return a(paramClass1.getGenericInterfaces()[i], arrayOfClass[i], paramClass2);
        i++;
      }
    }
    if (!paramClass1.isInterface())
      while (paramClass1 != Object.class)
      {
        Class localClass = paramClass1.getSuperclass();
        if (localClass == paramClass2)
          return paramClass1.getGenericSuperclass();
        if (paramClass2.isAssignableFrom(localClass))
          return a(paramClass1.getGenericSuperclass(), localClass, paramClass2);
        paramClass1 = localClass;
      }
    return paramClass2;
  }

  private static Type a(Type paramType, Class<?> paramClass, TypeVariable paramTypeVariable)
  {
    Class localClass = a(paramTypeVariable);
    if (localClass == null);
    Type localType;
    do
    {
      return paramTypeVariable;
      localType = a(paramType, paramClass, localClass);
    }
    while (!(localType instanceof ParameterizedType));
    int i = a(localClass.getTypeParameters(), paramTypeVariable);
    return ((ParameterizedType)localType).getActualTypeArguments()[i];
  }

  private static boolean a(Object paramObject1, Object paramObject2)
  {
    return (paramObject1 == paramObject2) || ((paramObject1 != null) && (paramObject1.equals(paramObject2)));
  }

  public static GenericArrayType arrayOf(Type paramType)
  {
    return new a(paramType);
  }

  private static int b(Object paramObject)
  {
    if (paramObject != null)
      return paramObject.hashCode();
    return 0;
  }

  private static Type b(Type paramType, Class<?> paramClass1, Class<?> paramClass2)
  {
    .Gson.Preconditions.checkArgument(paramClass2.isAssignableFrom(paramClass1));
    return resolve(paramType, paramClass1, a(paramType, paramClass1, paramClass2));
  }

  private static void b(Type paramType)
  {
    if ((!(paramType instanceof Class)) || (!((Class)paramType).isPrimitive()));
    for (boolean bool = true; ; bool = false)
    {
      .Gson.Preconditions.checkArgument(bool);
      return;
    }
  }

  public static Type canonicalize(Type paramType)
  {
    if ((paramType instanceof Class))
    {
      paramType = (Class)paramType;
      if (paramType.isArray())
        paramType = new a(canonicalize(paramType.getComponentType()));
    }
    do
    {
      return paramType;
      if ((paramType instanceof ParameterizedType))
      {
        ParameterizedType localParameterizedType = (ParameterizedType)paramType;
        return new b(localParameterizedType.getOwnerType(), localParameterizedType.getRawType(), localParameterizedType.getActualTypeArguments());
      }
      if ((paramType instanceof GenericArrayType))
        return new a(((GenericArrayType)paramType).getGenericComponentType());
    }
    while (!(paramType instanceof WildcardType));
    WildcardType localWildcardType = (WildcardType)paramType;
    return new c(localWildcardType.getUpperBounds(), localWildcardType.getLowerBounds());
  }

  public static boolean equals(Type paramType1, Type paramType2)
  {
    if (paramType1 == paramType2);
    TypeVariable localTypeVariable1;
    TypeVariable localTypeVariable2;
    do
    {
      WildcardType localWildcardType1;
      WildcardType localWildcardType2;
      do
      {
        ParameterizedType localParameterizedType1;
        ParameterizedType localParameterizedType2;
        do
        {
          return true;
          if ((paramType1 instanceof Class))
            return paramType1.equals(paramType2);
          if (!(paramType1 instanceof ParameterizedType))
            break;
          if (!(paramType2 instanceof ParameterizedType))
            return false;
          localParameterizedType1 = (ParameterizedType)paramType1;
          localParameterizedType2 = (ParameterizedType)paramType2;
        }
        while ((a(localParameterizedType1.getOwnerType(), localParameterizedType2.getOwnerType())) && (localParameterizedType1.getRawType().equals(localParameterizedType2.getRawType())) && (Arrays.equals(localParameterizedType1.getActualTypeArguments(), localParameterizedType2.getActualTypeArguments())));
        return false;
        if ((paramType1 instanceof GenericArrayType))
        {
          if (!(paramType2 instanceof GenericArrayType))
            return false;
          GenericArrayType localGenericArrayType1 = (GenericArrayType)paramType1;
          GenericArrayType localGenericArrayType2 = (GenericArrayType)paramType2;
          return equals(localGenericArrayType1.getGenericComponentType(), localGenericArrayType2.getGenericComponentType());
        }
        if (!(paramType1 instanceof WildcardType))
          break;
        if (!(paramType2 instanceof WildcardType))
          return false;
        localWildcardType1 = (WildcardType)paramType1;
        localWildcardType2 = (WildcardType)paramType2;
      }
      while ((Arrays.equals(localWildcardType1.getUpperBounds(), localWildcardType2.getUpperBounds())) && (Arrays.equals(localWildcardType1.getLowerBounds(), localWildcardType2.getLowerBounds())));
      return false;
      if (!(paramType1 instanceof TypeVariable))
        break;
      if (!(paramType2 instanceof TypeVariable))
        return false;
      localTypeVariable1 = (TypeVariable)paramType1;
      localTypeVariable2 = (TypeVariable)paramType2;
    }
    while ((localTypeVariable1.getGenericDeclaration() == localTypeVariable2.getGenericDeclaration()) && (localTypeVariable1.getName().equals(localTypeVariable2.getName())));
    return false;
    return false;
  }

  public static Type getArrayComponentType(Type paramType)
  {
    if ((paramType instanceof GenericArrayType))
      return ((GenericArrayType)paramType).getGenericComponentType();
    return ((Class)paramType).getComponentType();
  }

  public static Type getCollectionElementType(Type paramType, Class<?> paramClass)
  {
    return ((ParameterizedType)b(paramType, paramClass, java.util.Collection.class)).getActualTypeArguments()[0];
  }

  public static Type[] getMapKeyAndValueTypes(Type paramType, Class<?> paramClass)
  {
    if (paramType == Properties.class)
      return new Type[] { String.class, String.class };
    return ((ParameterizedType)b(paramType, paramClass, Map.class)).getActualTypeArguments();
  }

  public static Class<?> getRawType(Type paramType)
  {
    if ((paramType instanceof Class))
      return (Class)paramType;
    if ((paramType instanceof ParameterizedType))
    {
      Type localType = ((ParameterizedType)paramType).getRawType();
      .Gson.Preconditions.checkArgument(localType instanceof Class);
      return (Class)localType;
    }
    if ((paramType instanceof GenericArrayType))
      return Array.newInstance(getRawType(((GenericArrayType)paramType).getGenericComponentType()), 0).getClass();
    if ((paramType instanceof TypeVariable))
      return Object.class;
    if ((paramType instanceof WildcardType))
      return getRawType(((WildcardType)paramType).getUpperBounds()[0]);
    if (paramType == null);
    for (String str = "null"; ; str = paramType.getClass().getName())
      throw new IllegalArgumentException("Expected a Class, ParameterizedType, or GenericArrayType, but <" + paramType + "> is of type " + str);
  }

  public static boolean isArray(Type paramType)
  {
    return ((paramType instanceof GenericArrayType)) || (((paramType instanceof Class)) && (((Class)paramType).isArray()));
  }

  public static ParameterizedType newParameterizedTypeWithOwner(Type paramType1, Type paramType2, Type[] paramArrayOfType)
  {
    return new b(paramType1, paramType2, paramArrayOfType);
  }

  public static Type resolve(Type paramType1, Class<?> paramClass, Type paramType2)
  {
    Object localObject = paramType2;
    Type localType9;
    if ((localObject instanceof TypeVariable))
    {
      TypeVariable localTypeVariable = (TypeVariable)localObject;
      localType9 = a(paramType1, paramClass, localTypeVariable);
      if (localType9 == localTypeVariable)
        localObject = localType9;
    }
    label92: Type[] arrayOfType2;
    label134: label278: label339: Type localType1;
    do
    {
      do
      {
        Type[] arrayOfType1;
        Type localType2;
        do
        {
          do
          {
            Type localType4;
            int i;
            Type[] arrayOfType4;
            do
            {
              Type localType6;
              Type localType7;
              do
              {
                Class localClass;
                Type localType8;
                do
                {
                  return localObject;
                  localObject = localType9;
                  break;
                  if ((!(localObject instanceof Class)) || (!((Class)localObject).isArray()))
                    break label92;
                  localObject = (Class)localObject;
                  localClass = ((Class)localObject).getComponentType();
                  localType8 = resolve(paramType1, paramClass, localClass);
                }
                while (localClass == localType8);
                return arrayOf(localType8);
                if (!(localObject instanceof GenericArrayType))
                  break label134;
                localObject = (GenericArrayType)localObject;
                localType6 = ((GenericArrayType)localObject).getGenericComponentType();
                localType7 = resolve(paramType1, paramClass, localType6);
              }
              while (localType6 == localType7);
              return arrayOf(localType7);
              if (!(localObject instanceof ParameterizedType))
                break label278;
              localObject = (ParameterizedType)localObject;
              Type localType3 = ((ParameterizedType)localObject).getOwnerType();
              localType4 = resolve(paramType1, paramClass, localType3);
              if (localType4 != localType3);
              for (i = 1; ; i = 0)
              {
                Type[] arrayOfType3 = ((ParameterizedType)localObject).getActualTypeArguments();
                int j = arrayOfType3.length;
                arrayOfType4 = arrayOfType3;
                for (int k = 0; k < j; k++)
                {
                  Type localType5 = resolve(paramType1, paramClass, arrayOfType4[k]);
                  if (localType5 != arrayOfType4[k])
                  {
                    if (i == 0)
                    {
                      arrayOfType4 = (Type[])arrayOfType4.clone();
                      i = 1;
                    }
                    arrayOfType4[k] = localType5;
                  }
                }
              }
            }
            while (i == 0);
            return newParameterizedTypeWithOwner(localType4, ((ParameterizedType)localObject).getRawType(), arrayOfType4);
          }
          while (!(localObject instanceof WildcardType));
          localObject = (WildcardType)localObject;
          arrayOfType1 = ((WildcardType)localObject).getLowerBounds();
          arrayOfType2 = ((WildcardType)localObject).getUpperBounds();
          if (arrayOfType1.length != 1)
            break label339;
          localType2 = resolve(paramType1, paramClass, arrayOfType1[0]);
        }
        while (localType2 == arrayOfType1[0]);
        return supertypeOf(localType2);
      }
      while (arrayOfType2.length != 1);
      localType1 = resolve(paramType1, paramClass, arrayOfType2[0]);
    }
    while (localType1 == arrayOfType2[0]);
    return subtypeOf(localType1);
  }

  public static WildcardType subtypeOf(Type paramType)
  {
    return new c(new Type[] { paramType }, a);
  }

  public static WildcardType supertypeOf(Type paramType)
  {
    return new c(new Type[] { Object.class }, new Type[] { paramType });
  }

  public static String typeToString(Type paramType)
  {
    if ((paramType instanceof Class))
      return ((Class)paramType).getName();
    return paramType.toString();
  }

  private static final class a
    implements Serializable, GenericArrayType
  {
    private static final long serialVersionUID;
    private final Type a;

    public a(Type paramType)
    {
      this.a = .Gson.Types.canonicalize(paramType);
    }

    public final boolean equals(Object paramObject)
    {
      return ((paramObject instanceof GenericArrayType)) && (.Gson.Types.equals(this, (GenericArrayType)paramObject));
    }

    public final Type getGenericComponentType()
    {
      return this.a;
    }

    public final int hashCode()
    {
      return this.a.hashCode();
    }

    public final String toString()
    {
      return .Gson.Types.typeToString(this.a) + "[]";
    }
  }

  private static final class b
    implements Serializable, ParameterizedType
  {
    private static final long serialVersionUID;
    private final Type a;
    private final Type b;
    private final Type[] c;

    public b(Type paramType1, Type paramType2, Type[] paramArrayOfType)
    {
      boolean bool2;
      if ((paramType2 instanceof Class))
      {
        Class localClass = (Class)paramType2;
        if ((paramType1 != null) || (localClass.getEnclosingClass() == null))
        {
          bool2 = bool1;
          .Gson.Preconditions.checkArgument(bool2);
          if ((paramType1 != null) && (localClass.getEnclosingClass() == null))
            break label153;
          label56: .Gson.Preconditions.checkArgument(bool1);
        }
      }
      else
      {
        if (paramType1 != null)
          break label159;
      }
      label153: label159: for (Type localType = null; ; localType = .Gson.Types.canonicalize(paramType1))
      {
        this.a = localType;
        this.b = .Gson.Types.canonicalize(paramType2);
        this.c = ((Type[])paramArrayOfType.clone());
        while (i < this.c.length)
        {
          .Gson.Preconditions.checkNotNull(this.c[i]);
          .Gson.Types.a(this.c[i]);
          this.c[i] = .Gson.Types.canonicalize(this.c[i]);
          i++;
        }
        bool2 = false;
        break;
        bool1 = false;
        break label56;
      }
    }

    public final boolean equals(Object paramObject)
    {
      return ((paramObject instanceof ParameterizedType)) && (.Gson.Types.equals(this, (ParameterizedType)paramObject));
    }

    public final Type[] getActualTypeArguments()
    {
      return (Type[])this.c.clone();
    }

    public final Type getOwnerType()
    {
      return this.a;
    }

    public final Type getRawType()
    {
      return this.b;
    }

    public final int hashCode()
    {
      return Arrays.hashCode(this.c) ^ this.b.hashCode() ^ .Gson.Types.a(this.a);
    }

    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(30 * (1 + this.c.length));
      localStringBuilder.append(.Gson.Types.typeToString(this.b));
      if (this.c.length == 0)
        return localStringBuilder.toString();
      localStringBuilder.append("<").append(.Gson.Types.typeToString(this.c[0]));
      for (int i = 1; i < this.c.length; i++)
        localStringBuilder.append(", ").append(.Gson.Types.typeToString(this.c[i]));
      return ">";
    }
  }

  private static final class c
    implements Serializable, WildcardType
  {
    private static final long serialVersionUID;
    private final Type a;
    private final Type b;

    public c(Type[] paramArrayOfType1, Type[] paramArrayOfType2)
    {
      if (paramArrayOfType2.length <= i)
      {
        int k = i;
        .Gson.Preconditions.checkArgument(k);
        if (paramArrayOfType1.length != i)
          break label88;
        int n = i;
        label29: .Gson.Preconditions.checkArgument(n);
        if (paramArrayOfType2.length != i)
          break label99;
        .Gson.Preconditions.checkNotNull(paramArrayOfType2[0]);
        .Gson.Types.a(paramArrayOfType2[0]);
        if (paramArrayOfType1[0] != Object.class)
          break label94;
      }
      while (true)
      {
        .Gson.Preconditions.checkArgument(i);
        this.b = .Gson.Types.canonicalize(paramArrayOfType2[0]);
        this.a = Object.class;
        return;
        int m = 0;
        break;
        label88: int i1 = 0;
        break label29;
        label94: int j = 0;
      }
      label99: .Gson.Preconditions.checkNotNull(paramArrayOfType1[0]);
      .Gson.Types.a(paramArrayOfType1[0]);
      this.b = null;
      this.a = .Gson.Types.canonicalize(paramArrayOfType1[0]);
    }

    public final boolean equals(Object paramObject)
    {
      return ((paramObject instanceof WildcardType)) && (.Gson.Types.equals(this, (WildcardType)paramObject));
    }

    public final Type[] getLowerBounds()
    {
      if (this.b != null)
      {
        Type[] arrayOfType = new Type[1];
        arrayOfType[0] = this.b;
        return arrayOfType;
      }
      return .Gson.Types.a;
    }

    public final Type[] getUpperBounds()
    {
      Type[] arrayOfType = new Type[1];
      arrayOfType[0] = this.a;
      return arrayOfType;
    }

    public final int hashCode()
    {
      if (this.b != null);
      for (int i = 31 + this.b.hashCode(); ; i = 1)
        return i ^ 31 + this.a.hashCode();
    }

    public final String toString()
    {
      if (this.b != null)
        return "? super " + .Gson.Types.typeToString(this.b);
      if (this.a == Object.class)
        return "?";
      return "? extends " + .Gson.Types.typeToString(this.a);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.internal..Gson.Types
 * JD-Core Version:    0.6.2
 */