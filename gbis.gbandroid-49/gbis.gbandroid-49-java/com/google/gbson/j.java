package com.google.gbson;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

final class j
{
  private static final char[] a = { 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102 };
  private static final Set<Character> b;
  private static final Set<Character> c = Collections.unmodifiableSet(localHashSet2);
  private final boolean d = false;

  static
  {
    HashSet localHashSet1 = new HashSet();
    localHashSet1.add(Character.valueOf('"'));
    localHashSet1.add(Character.valueOf('\\'));
    b = Collections.unmodifiableSet(localHashSet1);
    HashSet localHashSet2 = new HashSet();
    localHashSet2.add(Character.valueOf('<'));
    localHashSet2.add(Character.valueOf('>'));
    localHashSet2.add(Character.valueOf('&'));
    localHashSet2.add(Character.valueOf('='));
    localHashSet2.add(Character.valueOf('\''));
  }

  private static void a(int paramInt, Appendable paramAppendable)
  {
    if (Character.isSupplementaryCodePoint(paramInt))
    {
      char[] arrayOfChar = Character.toChars(paramInt);
      a(arrayOfChar[0], paramAppendable);
      a(arrayOfChar[1], paramAppendable);
      return;
    }
    paramAppendable.append("\\u").append(a[(0xF & paramInt >>> 12)]).append(a[(0xF & paramInt >>> 8)]).append(a[(0xF & paramInt >>> 4)]).append(a[(paramInt & 0xF)]);
  }

  private void a(CharSequence paramCharSequence, StringBuilder paramStringBuilder)
  {
    int i = 0;
    int j = paramCharSequence.length();
    int k = 0;
    if (k < j)
    {
      int m = Character.codePointAt(paramCharSequence, k);
      int n = Character.charCount(m);
      if ((b(m)) || (a(m)))
      {
        paramStringBuilder.append(paramCharSequence, i, k);
        i = k + n;
        switch (m)
        {
        default:
          a(m, paramStringBuilder);
        case 8:
        case 9:
        case 10:
        case 12:
        case 13:
        case 92:
        case 47:
        case 34:
        }
      }
      while (true)
      {
        k += n;
        break;
        paramStringBuilder.append("\\b");
        continue;
        paramStringBuilder.append("\\t");
        continue;
        paramStringBuilder.append("\\n");
        continue;
        paramStringBuilder.append("\\f");
        continue;
        paramStringBuilder.append("\\r");
        continue;
        paramStringBuilder.append("\\\\");
        continue;
        paramStringBuilder.append("\\/");
        continue;
        paramStringBuilder.append("\\\"");
      }
    }
    paramStringBuilder.append(paramCharSequence, i, j);
  }

  private boolean a(int paramInt)
  {
    boolean bool1 = Character.isSupplementaryCodePoint(paramInt);
    boolean bool2 = false;
    if (!bool1)
    {
      char c1 = (char)paramInt;
      if (!b.contains(Character.valueOf(c1)))
      {
        boolean bool3 = this.d;
        bool2 = false;
        if (bool3)
        {
          boolean bool4 = c.contains(Character.valueOf(c1));
          bool2 = false;
          if (!bool4);
        }
      }
      else
      {
        bool2 = true;
      }
    }
    return bool2;
  }

  private static boolean b(int paramInt)
  {
    return (paramInt < 32) || (paramInt == 8232) || (paramInt == 8233) || ((paramInt >= 127) && (paramInt <= 159));
  }

  public final String a(CharSequence paramCharSequence)
  {
    StringBuilder localStringBuilder = new StringBuilder(20 + paramCharSequence.length());
    try
    {
      a(paramCharSequence, localStringBuilder);
      return localStringBuilder.toString();
    }
    catch (IOException localIOException)
    {
      throw new RuntimeException(localIOException);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.j
 * JD-Core Version:    0.6.2
 */