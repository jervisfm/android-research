package com.google.gbson;

import com.google.gbson.annotations.Expose;

final class l
  implements ExclusionStrategy
{
  public final boolean shouldSkipClass(Class<?> paramClass)
  {
    return false;
  }

  public final boolean shouldSkipField(FieldAttributes paramFieldAttributes)
  {
    Expose localExpose = (Expose)paramFieldAttributes.getAnnotation(Expose.class);
    if (localExpose == null)
      return true;
    return !localExpose.serialize();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.l
 * JD-Core Version:    0.6.2
 */