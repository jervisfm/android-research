package com.google.gbson;

import com.google.gbson.internal..Gson.Preconditions;

final class n
  implements m
{
  private final FieldNamingStrategy a;

  n(FieldNamingStrategy paramFieldNamingStrategy)
  {
    this.a = ((FieldNamingStrategy).Gson.Preconditions.checkNotNull(paramFieldNamingStrategy));
  }

  public final String a(FieldAttributes paramFieldAttributes)
  {
    return this.a.translateName(paramFieldAttributes.b());
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.n
 * JD-Core Version:    0.6.2
 */