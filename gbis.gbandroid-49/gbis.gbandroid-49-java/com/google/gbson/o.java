package com.google.gbson;

final class o
  implements ExclusionStrategy
{
  private static boolean a(Class<?> paramClass)
  {
    return (paramClass.isMemberClass()) && (!b(paramClass));
  }

  private static boolean b(Class<?> paramClass)
  {
    return (0x8 & paramClass.getModifiers()) != 0;
  }

  public final boolean shouldSkipClass(Class<?> paramClass)
  {
    return a(paramClass);
  }

  public final boolean shouldSkipField(FieldAttributes paramFieldAttributes)
  {
    return a(paramFieldAttributes.getDeclaredClass());
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.o
 * JD-Core Version:    0.6.2
 */