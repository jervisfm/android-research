package com.google.gbson;

import com.google.gbson.internal..Gson.Types;
import java.lang.reflect.Array;
import java.lang.reflect.Type;

final class q<T> extends s<T>
{
  q(JsonArray paramJsonArray, Type paramType, ObjectNavigator paramObjectNavigator, m paramm, ag paramag, aj<JsonDeserializer<?>> paramaj, JsonDeserializationContext paramJsonDeserializationContext)
  {
    super(paramJsonArray, paramType, paramObjectNavigator, paramm, paramag, paramaj, paramJsonDeserializationContext);
  }

  protected final T a()
  {
    if (!this.f.isJsonArray())
      throw new JsonParseException("Expecting array found: " + this.f);
    JsonArray localJsonArray = this.f.getAsJsonArray();
    if (.Gson.Types.isArray(this.g))
      return this.c.a(.Gson.Types.getArrayComponentType(this.g), localJsonArray.size());
    return this.c.a(.Gson.Types.getRawType(this.g));
  }

  public final void startVisitingObject(Object paramObject)
  {
    throw new JsonParseException("Expecting array but found object: " + paramObject);
  }

  public final void visitArray(Object paramObject, Type paramType)
  {
    if (!this.f.isJsonArray())
      throw new JsonParseException("Expecting array found: " + this.f);
    JsonArray localJsonArray = this.f.getAsJsonArray();
    int i = 0;
    if (i < localJsonArray.size())
    {
      JsonElement localJsonElement = localJsonArray.get(i);
      Object localObject;
      if ((localJsonElement == null) || (localJsonElement.isJsonNull()))
        localObject = null;
      while (true)
      {
        Array.set(paramObject, i, localObject);
        i++;
        break;
        if ((localJsonElement instanceof JsonObject))
        {
          localObject = a(.Gson.Types.getArrayComponentType(paramType), localJsonElement);
        }
        else if ((localJsonElement instanceof JsonArray))
        {
          localObject = a(.Gson.Types.getArrayComponentType(paramType), localJsonElement.getAsJsonArray());
        }
        else
        {
          if (!(localJsonElement instanceof JsonPrimitive))
            break label170;
          localObject = a(.Gson.Types.getArrayComponentType(paramType), localJsonElement.getAsJsonPrimitive());
        }
      }
      label170: throw new IllegalStateException();
    }
  }

  public final void visitArrayField(FieldAttributes paramFieldAttributes, Type paramType, Object paramObject)
  {
    throw new JsonParseException("Expecting array but found array field " + paramFieldAttributes.getName() + ": " + paramObject);
  }

  public final boolean visitFieldUsingCustomHandler(FieldAttributes paramFieldAttributes, Type paramType, Object paramObject)
  {
    throw new JsonParseException("Expecting array but found field " + paramFieldAttributes.getName() + ": " + paramObject);
  }

  public final void visitObjectField(FieldAttributes paramFieldAttributes, Type paramType, Object paramObject)
  {
    throw new JsonParseException("Expecting array but found object field " + paramFieldAttributes.getName() + ": " + paramObject);
  }

  public final void visitPrimitive(Object paramObject)
  {
    throw new JsonParseException("Type information is unavailable, and the target is not a primitive: " + this.f);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.q
 * JD-Core Version:    0.6.2
 */