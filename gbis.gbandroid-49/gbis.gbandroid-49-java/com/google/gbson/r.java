package com.google.gbson;

import java.lang.reflect.Type;

final class r
  implements JsonDeserializationContext
{
  private final ObjectNavigator a;
  private final m b;
  private final aj<JsonDeserializer<?>> c;
  private final ac d;

  r(ObjectNavigator paramObjectNavigator, m paramm, aj<JsonDeserializer<?>> paramaj, ac paramac)
  {
    this.a = paramObjectNavigator;
    this.b = paramm;
    this.c = paramaj;
    this.d = paramac;
  }

  private <T> T a(Type paramType, JsonArray paramJsonArray, JsonDeserializationContext paramJsonDeserializationContext)
  {
    q localq = new q(paramJsonArray, paramType, this.a, this.b, this.d, this.c, paramJsonDeserializationContext);
    this.a.a(new ah(null, paramType, true), localq);
    return localq.getTarget();
  }

  private <T> T a(Type paramType, JsonObject paramJsonObject, JsonDeserializationContext paramJsonDeserializationContext)
  {
    u localu = new u(paramJsonObject, paramType, this.a, this.b, this.d, this.c, paramJsonDeserializationContext);
    this.a.a(new ah(null, paramType, true), localu);
    return localu.getTarget();
  }

  private <T> T a(Type paramType, JsonPrimitive paramJsonPrimitive, JsonDeserializationContext paramJsonDeserializationContext)
  {
    u localu = new u(paramJsonPrimitive, paramType, this.a, this.b, this.d, this.c, paramJsonDeserializationContext);
    this.a.a(new ah(paramJsonPrimitive.a(), paramType, true), localu);
    return localu.getTarget();
  }

  final ag a()
  {
    return this.d;
  }

  public final <T> T deserialize(JsonElement paramJsonElement, Type paramType)
  {
    if ((paramJsonElement == null) || (paramJsonElement.isJsonNull()))
      return null;
    if (paramJsonElement.isJsonArray())
      return a(paramType, paramJsonElement.getAsJsonArray(), this);
    if (paramJsonElement.isJsonObject())
      return a(paramType, paramJsonElement.getAsJsonObject(), this);
    if (paramJsonElement.isJsonPrimitive())
      return a(paramType, paramJsonElement.getAsJsonPrimitive(), this);
    throw new JsonParseException("Failed parsing JSON source: " + paramJsonElement + " to Json");
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.r
 * JD-Core Version:    0.6.2
 */