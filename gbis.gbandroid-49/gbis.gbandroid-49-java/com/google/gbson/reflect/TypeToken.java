package com.google.gbson.reflect;

import com.google.gbson.internal..Gson.Preconditions;
import com.google.gbson.internal..Gson.Types;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.HashMap;
import java.util.Map;

public class TypeToken<T>
{
  final Class<? super T> a;
  final Type b;
  final int c;

  protected TypeToken()
  {
    this.b = a(getClass());
    this.a = .Gson.Types.getRawType(this.b);
    this.c = this.b.hashCode();
  }

  private TypeToken(Type paramType)
  {
    this.b = .Gson.Types.canonicalize((Type).Gson.Preconditions.checkNotNull(paramType));
    this.a = .Gson.Types.getRawType(this.b);
    this.c = this.b.hashCode();
  }

  private static AssertionError a(Type paramType, Class<?>[] paramArrayOfClass)
  {
    StringBuilder localStringBuilder = new StringBuilder("Unexpected type. Expected one of: ");
    int i = paramArrayOfClass.length;
    for (int j = 0; j < i; j++)
      localStringBuilder.append(paramArrayOfClass[j].getName()).append(", ");
    localStringBuilder.append("but got: ").append(paramType.getClass().getName()).append(", for type token: ").append(paramType.toString()).append('.');
    return new AssertionError(localStringBuilder.toString());
  }

  private static Type a(Class<?> paramClass)
  {
    Type localType = paramClass.getGenericSuperclass();
    if ((localType instanceof Class))
      throw new RuntimeException("Missing type parameter.");
    return .Gson.Types.canonicalize(((ParameterizedType)localType).getActualTypeArguments()[0]);
  }

  private static boolean a(ParameterizedType paramParameterizedType1, ParameterizedType paramParameterizedType2, Map<String, Type> paramMap)
  {
    Type[] arrayOfType1;
    Type[] arrayOfType2;
    if (paramParameterizedType1.getRawType().equals(paramParameterizedType2.getRawType()))
    {
      arrayOfType1 = paramParameterizedType1.getActualTypeArguments();
      arrayOfType2 = paramParameterizedType2.getActualTypeArguments();
    }
    for (int i = 0; i < arrayOfType1.length; i++)
      if (!a(arrayOfType1[i], arrayOfType2[i], paramMap))
        return false;
    return true;
  }

  private static boolean a(Type paramType, GenericArrayType paramGenericArrayType)
  {
    Type localType = paramGenericArrayType.getGenericComponentType();
    if ((localType instanceof ParameterizedType))
    {
      if ((paramType instanceof GenericArrayType))
        paramType = ((GenericArrayType)paramType).getGenericComponentType();
      while (true)
      {
        return a(paramType, (ParameterizedType)localType, new HashMap());
        if ((paramType instanceof Class))
          for (paramType = (Class)paramType; paramType.isArray(); paramType = paramType.getComponentType());
      }
    }
    return true;
  }

  private static boolean a(Type paramType, ParameterizedType paramParameterizedType, Map<String, Type> paramMap)
  {
    int i = 0;
    if (paramType == null)
      return false;
    if (paramParameterizedType.equals(paramType))
      return true;
    Class localClass = .Gson.Types.getRawType(paramType);
    if ((paramType instanceof ParameterizedType));
    for (ParameterizedType localParameterizedType = (ParameterizedType)paramType; ; localParameterizedType = null)
    {
      if (localParameterizedType != null)
      {
        Type[] arrayOfType2 = localParameterizedType.getActualTypeArguments();
        TypeVariable[] arrayOfTypeVariable = localClass.getTypeParameters();
        for (int k = 0; k < arrayOfType2.length; k++)
        {
          Type localType = arrayOfType2[k];
          TypeVariable localTypeVariable = arrayOfTypeVariable[k];
          while ((localType instanceof TypeVariable))
            localType = (Type)paramMap.get(((TypeVariable)localType).getName());
          paramMap.put(localTypeVariable.getName(), localType);
        }
        if (a(localParameterizedType, paramParameterizedType, paramMap))
          return true;
      }
      Type[] arrayOfType1 = localClass.getGenericInterfaces();
      int j = arrayOfType1.length;
      while (i < j)
      {
        if (a(arrayOfType1[i], paramParameterizedType, new HashMap(paramMap)))
          return true;
        i++;
      }
      return a(localClass.getGenericSuperclass(), paramParameterizedType, new HashMap(paramMap));
    }
  }

  private static boolean a(Type paramType1, Type paramType2, Map<String, Type> paramMap)
  {
    return (paramType2.equals(paramType1)) || (((paramType1 instanceof TypeVariable)) && (paramType2.equals(paramMap.get(((TypeVariable)paramType1).getName()))));
  }

  public static <T> TypeToken<T> get(Class<T> paramClass)
  {
    return new TypeToken(paramClass);
  }

  public static TypeToken<?> get(Type paramType)
  {
    return new TypeToken(paramType);
  }

  public final boolean equals(Object paramObject)
  {
    return ((paramObject instanceof TypeToken)) && (.Gson.Types.equals(this.b, ((TypeToken)paramObject).b));
  }

  public final Class<? super T> getRawType()
  {
    return this.a;
  }

  public final Type getType()
  {
    return this.b;
  }

  public final int hashCode()
  {
    return this.c;
  }

  @Deprecated
  public boolean isAssignableFrom(TypeToken<?> paramTypeToken)
  {
    return isAssignableFrom(paramTypeToken.getType());
  }

  @Deprecated
  public boolean isAssignableFrom(Class<?> paramClass)
  {
    return isAssignableFrom(paramClass);
  }

  @Deprecated
  public boolean isAssignableFrom(Type paramType)
  {
    if (paramType == null)
      return false;
    if (this.b.equals(paramType))
      return true;
    if ((this.b instanceof Class))
      return this.a.isAssignableFrom(.Gson.Types.getRawType(paramType));
    if ((this.b instanceof ParameterizedType))
      return a(paramType, (ParameterizedType)this.b, new HashMap());
    if ((this.b instanceof GenericArrayType))
      return (this.a.isAssignableFrom(.Gson.Types.getRawType(paramType))) && (a(paramType, (GenericArrayType)this.b));
    throw a(this.b, new Class[] { Class.class, ParameterizedType.class, GenericArrayType.class });
  }

  public final String toString()
  {
    return .Gson.Types.typeToString(this.b);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.reflect.TypeToken
 * JD-Core Version:    0.6.2
 */