package com.google.gbson;

import com.google.gbson.internal..Gson.Preconditions;
import java.lang.reflect.Type;

abstract class s<T>
  implements ObjectNavigator.Visitor
{
  protected final ObjectNavigator a;
  protected final m b;
  protected final ag c;
  protected final aj<JsonDeserializer<?>> d;
  protected T e;
  protected final JsonElement f;
  protected final Type g;
  protected final JsonDeserializationContext h;
  protected boolean i;

  s(JsonElement paramJsonElement, Type paramType, ObjectNavigator paramObjectNavigator, m paramm, ag paramag, aj<JsonDeserializer<?>> paramaj, JsonDeserializationContext paramJsonDeserializationContext)
  {
    this.g = paramType;
    this.a = paramObjectNavigator;
    this.b = paramm;
    this.c = paramag;
    this.d = paramaj;
    this.f = ((JsonElement).Gson.Preconditions.checkNotNull(paramJsonElement));
    this.h = paramJsonDeserializationContext;
    this.i = false;
  }

  private Object a(Type paramType, s<?> params)
  {
    this.a.a(new ah(null, paramType, false), params);
    return params.getTarget();
  }

  protected abstract T a();

  protected final Object a(JsonElement paramJsonElement, ai<JsonDeserializer<?>, ah> paramai)
  {
    if ((paramJsonElement == null) || (paramJsonElement.isJsonNull()))
      return null;
    Type localType = ((ah)paramai.b).a;
    return ((JsonDeserializer)paramai.a).deserialize(paramJsonElement, localType, this.h);
  }

  final Object a(Type paramType, JsonArray paramJsonArray)
  {
    return a(paramType, new q(paramJsonArray.getAsJsonArray(), paramType, this.a, this.b, this.c, this.d, this.h));
  }

  final Object a(Type paramType, JsonElement paramJsonElement)
  {
    return a(paramType, new u(paramJsonElement, paramType, this.a, this.b, this.c, this.d, this.h));
  }

  public void end(ah paramah)
  {
  }

  public T getTarget()
  {
    if (!this.i)
    {
      this.e = a();
      this.i = true;
    }
    return this.e;
  }

  public void start(ah paramah)
  {
  }

  public final boolean visitUsingCustomHandler(ah paramah)
  {
    ai localai = paramah.a(this.d);
    if (localai == null)
      return false;
    this.e = a(this.f, localai);
    this.i = true;
    return true;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.s
 * JD-Core Version:    0.6.2
 */