package com.google.gbson.stream;

import java.io.Closeable;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public final class JsonWriter
  implements Closeable
{
  private boolean htmlSafe;
  private String indent;
  private boolean lenient;
  private final Writer out;
  private String separator;
  private final List<JsonScope> stack = new ArrayList();

  public JsonWriter(Writer paramWriter)
  {
    this.stack.add(JsonScope.EMPTY_DOCUMENT);
    this.separator = ":";
    if (paramWriter == null)
      throw new NullPointerException("out == null");
    this.out = paramWriter;
  }

  private void beforeName()
  {
    JsonScope localJsonScope = peek();
    if (localJsonScope == JsonScope.NONEMPTY_OBJECT)
      this.out.write(44);
    while (localJsonScope == JsonScope.EMPTY_OBJECT)
    {
      newline();
      replaceTop(JsonScope.DANGLING_NAME);
      return;
    }
    throw new IllegalStateException("Nesting problem: " + this.stack);
  }

  private void beforeValue(boolean paramBoolean)
  {
    switch (1.$SwitchMap$com$google$gson$stream$JsonScope[peek().ordinal()])
    {
    default:
      throw new IllegalStateException("Nesting problem: " + this.stack);
    case 1:
      if ((!this.lenient) && (!paramBoolean))
        throw new IllegalStateException("JSON must start with an array or an object.");
      replaceTop(JsonScope.NONEMPTY_DOCUMENT);
      return;
    case 2:
      replaceTop(JsonScope.NONEMPTY_ARRAY);
      newline();
      return;
    case 3:
      this.out.append(',');
      newline();
      return;
    case 4:
      this.out.append(this.separator);
      replaceTop(JsonScope.NONEMPTY_OBJECT);
      return;
    case 5:
    }
    throw new IllegalStateException("JSON must have only one top-level value.");
  }

  private JsonWriter close(JsonScope paramJsonScope1, JsonScope paramJsonScope2, String paramString)
  {
    JsonScope localJsonScope = peek();
    if ((localJsonScope != paramJsonScope2) && (localJsonScope != paramJsonScope1))
      throw new IllegalStateException("Nesting problem: " + this.stack);
    this.stack.remove(-1 + this.stack.size());
    if (localJsonScope == paramJsonScope2)
      newline();
    this.out.write(paramString);
    return this;
  }

  private void newline()
  {
    if (this.indent == null);
    while (true)
    {
      return;
      this.out.write("\n");
      for (int i = 1; i < this.stack.size(); i++)
        this.out.write(this.indent);
    }
  }

  private JsonWriter open(JsonScope paramJsonScope, String paramString)
  {
    beforeValue(true);
    this.stack.add(paramJsonScope);
    this.out.write(paramString);
    return this;
  }

  private JsonScope peek()
  {
    return (JsonScope)this.stack.get(-1 + this.stack.size());
  }

  private void replaceTop(JsonScope paramJsonScope)
  {
    this.stack.set(-1 + this.stack.size(), paramJsonScope);
  }

  private void string(String paramString)
  {
    this.out.write("\"");
    int i = paramString.length();
    int j = 0;
    if (j < i)
    {
      int k = paramString.charAt(j);
      switch (k)
      {
      default:
        if (k <= 31)
        {
          Writer localWriter2 = this.out;
          Object[] arrayOfObject2 = new Object[1];
          arrayOfObject2[0] = Integer.valueOf(k);
          localWriter2.write(String.format("\\u%04x", arrayOfObject2));
        }
        break;
      case 34:
      case 92:
      case 9:
      case 8:
      case 10:
      case 13:
      case 12:
      case 38:
      case 39:
      case 60:
      case 61:
      case 62:
      }
      while (true)
      {
        j++;
        break;
        this.out.write(92);
        this.out.write(k);
        continue;
        this.out.write("\\t");
        continue;
        this.out.write("\\b");
        continue;
        this.out.write("\\n");
        continue;
        this.out.write("\\r");
        continue;
        this.out.write("\\f");
        continue;
        if (this.htmlSafe)
        {
          Writer localWriter1 = this.out;
          Object[] arrayOfObject1 = new Object[1];
          arrayOfObject1[0] = Integer.valueOf(k);
          localWriter1.write(String.format("\\u%04x", arrayOfObject1));
        }
        else
        {
          this.out.write(k);
        }
      }
    }
    this.out.write("\"");
  }

  public final JsonWriter beginArray()
  {
    return open(JsonScope.EMPTY_ARRAY, "[");
  }

  public final JsonWriter beginObject()
  {
    return open(JsonScope.EMPTY_OBJECT, "{");
  }

  public final void close()
  {
    this.out.close();
    if (peek() != JsonScope.NONEMPTY_DOCUMENT)
      throw new IOException("Incomplete document");
  }

  public final JsonWriter endArray()
  {
    return close(JsonScope.EMPTY_ARRAY, JsonScope.NONEMPTY_ARRAY, "]");
  }

  public final JsonWriter endObject()
  {
    return close(JsonScope.EMPTY_OBJECT, JsonScope.NONEMPTY_OBJECT, "}");
  }

  public final void flush()
  {
    this.out.flush();
  }

  public final boolean isHtmlSafe()
  {
    return this.htmlSafe;
  }

  public final boolean isLenient()
  {
    return this.lenient;
  }

  public final JsonWriter name(String paramString)
  {
    if (paramString == null)
      throw new NullPointerException("name == null");
    beforeName();
    string(paramString);
    return this;
  }

  public final JsonWriter nullValue()
  {
    beforeValue(false);
    this.out.write("null");
    return this;
  }

  public final void setHtmlSafe(boolean paramBoolean)
  {
    this.htmlSafe = paramBoolean;
  }

  public final void setIndent(String paramString)
  {
    if (paramString.length() == 0)
    {
      this.indent = null;
      this.separator = ":";
      return;
    }
    this.indent = paramString;
    this.separator = ": ";
  }

  public final void setLenient(boolean paramBoolean)
  {
    this.lenient = paramBoolean;
  }

  public final JsonWriter value(double paramDouble)
  {
    if ((Double.isNaN(paramDouble)) || (Double.isInfinite(paramDouble)))
      throw new IllegalArgumentException("Numeric values must be finite, but was " + paramDouble);
    beforeValue(false);
    this.out.append(Double.toString(paramDouble));
    return this;
  }

  public final JsonWriter value(long paramLong)
  {
    beforeValue(false);
    this.out.write(Long.toString(paramLong));
    return this;
  }

  public final JsonWriter value(Number paramNumber)
  {
    if (paramNumber == null)
      return nullValue();
    String str = paramNumber.toString();
    if ((!this.lenient) && ((str.equals("-Infinity")) || (str.equals("Infinity")) || (str.equals("NaN"))))
      throw new IllegalArgumentException("Numeric values must be finite, but was " + paramNumber);
    beforeValue(false);
    this.out.append(str);
    return this;
  }

  public final JsonWriter value(String paramString)
  {
    if (paramString == null)
      return nullValue();
    beforeValue(false);
    string(paramString);
    return this;
  }

  public final JsonWriter value(boolean paramBoolean)
  {
    beforeValue(false);
    Writer localWriter = this.out;
    if (paramBoolean);
    for (String str = "true"; ; str = "false")
    {
      localWriter.write(str);
      return this;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.stream.JsonWriter
 * JD-Core Version:    0.6.2
 */