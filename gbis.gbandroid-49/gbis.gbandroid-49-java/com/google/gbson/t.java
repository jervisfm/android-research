package com.google.gbson;

import com.google.gbson.internal..Gson.Preconditions;
import java.lang.reflect.Type;

final class t<T>
  implements JsonDeserializer<T>
{
  private final JsonDeserializer<T> a;

  t(JsonDeserializer<T> paramJsonDeserializer)
  {
    this.a = ((JsonDeserializer).Gson.Preconditions.checkNotNull(paramJsonDeserializer));
  }

  public final T deserialize(JsonElement paramJsonElement, Type paramType, JsonDeserializationContext paramJsonDeserializationContext)
  {
    try
    {
      Object localObject = this.a.deserialize(paramJsonElement, paramType, paramJsonDeserializationContext);
      return localObject;
    }
    catch (JsonParseException localJsonParseException)
    {
      throw localJsonParseException;
    }
    catch (Exception localException)
    {
      throw new JsonParseException("The JsonDeserializer " + this.a + " failed to deserialize json object " + paramJsonElement + " given the type " + paramType, localException);
    }
  }

  public final String toString()
  {
    return this.a.toString();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.t
 * JD-Core Version:    0.6.2
 */