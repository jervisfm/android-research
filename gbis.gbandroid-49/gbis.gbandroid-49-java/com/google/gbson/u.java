package com.google.gbson;

import java.lang.reflect.Type;

final class u<T> extends s<T>
{
  u(JsonElement paramJsonElement, Type paramType, ObjectNavigator paramObjectNavigator, m paramm, ag paramag, aj<JsonDeserializer<?>> paramaj, JsonDeserializationContext paramJsonDeserializationContext)
  {
    super(paramJsonElement, paramType, paramObjectNavigator, paramm, paramag, paramaj, paramJsonDeserializationContext);
  }

  private String a(FieldAttributes paramFieldAttributes)
  {
    return this.b.a(paramFieldAttributes);
  }

  protected final T a()
  {
    return this.c.a(this.g);
  }

  public final void startVisitingObject(Object paramObject)
  {
  }

  public final void visitArray(Object paramObject, Type paramType)
  {
    throw new JsonParseException("Expecting object but found array: " + paramObject);
  }

  public final void visitArrayField(FieldAttributes paramFieldAttributes, Type paramType, Object paramObject)
  {
    try
    {
      if (!this.f.isJsonObject())
        throw new JsonParseException("Expecting object found: " + this.f);
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      throw new RuntimeException(localIllegalAccessException);
    }
    JsonArray localJsonArray = (JsonArray)this.f.getAsJsonObject().get(a(paramFieldAttributes));
    if (localJsonArray != null)
    {
      paramFieldAttributes.a(paramObject, a(paramType, localJsonArray));
      return;
    }
    paramFieldAttributes.a(paramObject, null);
  }

  public final boolean visitFieldUsingCustomHandler(FieldAttributes paramFieldAttributes, Type paramType, Object paramObject)
  {
    String str;
    try
    {
      str = a(paramFieldAttributes);
      if (!this.f.isJsonObject())
        throw new JsonParseException("Expecting object found: " + this.f);
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      throw new RuntimeException();
    }
    JsonElement localJsonElement = this.f.getAsJsonObject().get(str);
    boolean bool = ak.a(paramType);
    if (localJsonElement == null)
      return true;
    if (localJsonElement.isJsonNull())
    {
      if (!bool)
      {
        paramFieldAttributes.a(paramObject, null);
        return true;
      }
    }
    else
    {
      ai localai = new ah(null, paramType, false).a(this.d);
      if (localai == null)
        return false;
      Object localObject = a(localJsonElement, localai);
      if ((localObject != null) || (!bool))
        paramFieldAttributes.a(paramObject, localObject);
    }
    return true;
  }

  public final void visitObjectField(FieldAttributes paramFieldAttributes, Type paramType, Object paramObject)
  {
    try
    {
      if (!this.f.isJsonObject())
        throw new JsonParseException("Expecting object found: " + this.f);
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      throw new RuntimeException(localIllegalAccessException);
    }
    JsonElement localJsonElement = this.f.getAsJsonObject().get(a(paramFieldAttributes));
    if (localJsonElement != null)
    {
      paramFieldAttributes.a(paramObject, a(paramType, localJsonElement));
      return;
    }
    paramFieldAttributes.a(paramObject, null);
  }

  public final void visitPrimitive(Object paramObject)
  {
    if (!this.f.isJsonPrimitive())
      throw new JsonParseException("Type information is unavailable, and the target object is not a primitive: " + this.f);
    this.e = this.f.getAsJsonPrimitive().a();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.u
 * JD-Core Version:    0.6.2
 */