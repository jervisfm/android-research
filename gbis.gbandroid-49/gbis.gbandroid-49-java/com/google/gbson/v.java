package com.google.gbson;

import java.lang.reflect.Type;

final class v
  implements JsonSerializationContext
{
  private final ObjectNavigator a;
  private final m b;
  private final aj<JsonSerializer<?>> c;
  private final boolean d;
  private final ad e;

  v(ObjectNavigator paramObjectNavigator, m paramm, boolean paramBoolean, aj<JsonSerializer<?>> paramaj)
  {
    this.a = paramObjectNavigator;
    this.b = paramm;
    this.d = paramBoolean;
    this.c = paramaj;
    this.e = new ad();
  }

  final JsonElement a(Object paramObject, Type paramType, boolean paramBoolean)
  {
    if (paramObject == null)
      return JsonNull.c();
    w localw = new w(this.a, this.b, this.d, this.c, this, this.e);
    this.a.a(new ah(paramObject, paramType, paramBoolean), localw);
    return localw.a();
  }

  public final JsonElement serialize(Object paramObject)
  {
    if (paramObject == null)
      return JsonNull.c();
    return a(paramObject, paramObject.getClass(), false);
  }

  public final JsonElement serialize(Object paramObject, Type paramType)
  {
    return a(paramObject, paramType, true);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.v
 * JD-Core Version:    0.6.2
 */