package com.google.gbson;

import com.google.gbson.internal..Gson.Preconditions;
import com.google.gbson.internal..Gson.Types;
import java.lang.reflect.Array;
import java.lang.reflect.Type;

final class w
  implements ObjectNavigator.Visitor
{
  private final ObjectNavigator a;
  private final m b;
  private final aj<JsonSerializer<?>> c;
  private final boolean d;
  private final JsonSerializationContext e;
  private final ad f;
  private JsonElement g;

  w(ObjectNavigator paramObjectNavigator, m paramm, boolean paramBoolean, aj<JsonSerializer<?>> paramaj, JsonSerializationContext paramJsonSerializationContext, ad paramad)
  {
    this.a = paramObjectNavigator;
    this.b = paramm;
    this.d = paramBoolean;
    this.c = paramaj;
    this.e = paramJsonSerializationContext;
    this.f = paramad;
  }

  private void a(FieldAttributes paramFieldAttributes, JsonElement paramJsonElement)
  {
    this.g.getAsJsonObject().add(this.b.a(paramFieldAttributes), paramJsonElement);
  }

  private void a(FieldAttributes paramFieldAttributes, ah paramah)
  {
    a(paramFieldAttributes, b(paramah));
  }

  private void a(JsonElement paramJsonElement)
  {
    this.g = ((JsonElement).Gson.Preconditions.checkNotNull(paramJsonElement));
  }

  private void a(ah paramah)
  {
    if (paramah.a() == null)
    {
      this.g.getAsJsonArray().add(JsonNull.c());
      return;
    }
    JsonElement localJsonElement = b(paramah);
    this.g.getAsJsonArray().add(localJsonElement);
  }

  private static boolean a(FieldAttributes paramFieldAttributes, Object paramObject)
  {
    return b(paramFieldAttributes, paramObject) == null;
  }

  private JsonElement b(ah paramah)
  {
    w localw = new w(this.a, this.b, this.d, this.c, this.e, this.f);
    this.a.a(paramah, localw);
    return localw.a();
  }

  private static Object b(FieldAttributes paramFieldAttributes, Object paramObject)
  {
    try
    {
      Object localObject = paramFieldAttributes.a(paramObject);
      return localObject;
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      throw new RuntimeException(localIllegalAccessException);
    }
  }

  private JsonElement c(ah paramah)
  {
    ai localai = paramah.a(this.c);
    if (localai == null)
      return null;
    JsonSerializer localJsonSerializer = (JsonSerializer)localai.a;
    ah localah = (ah)localai.b;
    start(localah);
    try
    {
      Object localObject2 = localJsonSerializer.serialize(localah.a(), localah.b(), this.e);
      if (localObject2 == null)
      {
        JsonNull localJsonNull = JsonNull.c();
        localObject2 = localJsonNull;
      }
      return localObject2;
    }
    finally
    {
      end(localah);
    }
  }

  public final JsonElement a()
  {
    return this.g;
  }

  public final void end(ah paramah)
  {
    if (paramah != null)
      this.f.a();
  }

  public final Object getTarget()
  {
    return null;
  }

  public final void start(ah paramah)
  {
    if (paramah == null)
      return;
    if (this.f.b(paramah))
      throw new e(paramah);
    this.f.a(paramah);
  }

  public final void startVisitingObject(Object paramObject)
  {
    a(new JsonObject());
  }

  public final void visitArray(Object paramObject, Type paramType)
  {
    a(new JsonArray());
    int i = Array.getLength(paramObject);
    Type localType = .Gson.Types.getArrayComponentType(paramType);
    for (int j = 0; j < i; j++)
      a(new ah(Array.get(paramObject, j), localType, false));
  }

  public final void visitArrayField(FieldAttributes paramFieldAttributes, Type paramType, Object paramObject)
  {
    try
    {
      if (a(paramFieldAttributes, paramObject))
      {
        if (this.d)
          a(paramFieldAttributes, JsonNull.c());
      }
      else
      {
        a(paramFieldAttributes, new ah(b(paramFieldAttributes, paramObject), paramType, false));
        return;
      }
    }
    catch (e locale)
    {
      throw locale.a(paramFieldAttributes);
    }
  }

  public final boolean visitFieldUsingCustomHandler(FieldAttributes paramFieldAttributes, Type paramType, Object paramObject)
  {
    try
    {
      .Gson.Preconditions.checkState(this.g.isJsonObject());
      Object localObject = paramFieldAttributes.a(paramObject);
      if (localObject == null)
      {
        if (this.d)
        {
          a(paramFieldAttributes, JsonNull.c());
          return true;
        }
      }
      else
      {
        JsonElement localJsonElement = c(new ah(localObject, paramType, false));
        if (localJsonElement != null)
        {
          a(paramFieldAttributes, localJsonElement);
          return true;
        }
      }
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      throw new RuntimeException();
      return false;
    }
    catch (e locale)
    {
      throw locale.a(paramFieldAttributes);
    }
    return true;
  }

  public final void visitObjectField(FieldAttributes paramFieldAttributes, Type paramType, Object paramObject)
  {
    try
    {
      if (a(paramFieldAttributes, paramObject))
      {
        if (this.d)
          a(paramFieldAttributes, JsonNull.c());
      }
      else
      {
        a(paramFieldAttributes, new ah(b(paramFieldAttributes, paramObject), paramType, false));
        return;
      }
    }
    catch (e locale)
    {
      throw locale.a(paramFieldAttributes);
    }
  }

  public final void visitPrimitive(Object paramObject)
  {
    if (paramObject == null);
    for (Object localObject = JsonNull.c(); ; localObject = new JsonPrimitive(paramObject))
    {
      a((JsonElement)localObject);
      return;
    }
  }

  public final boolean visitUsingCustomHandler(ah paramah)
  {
    boolean bool = true;
    try
    {
      if (paramah.a() == null)
      {
        if (!this.d)
          break label54;
        a(JsonNull.c());
        return bool;
      }
      JsonElement localJsonElement = c(paramah);
      if (localJsonElement != null)
      {
        a(localJsonElement);
        return bool;
      }
    }
    catch (e locale)
    {
      throw locale.a(null);
    }
    bool = false;
    label54: return bool;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.w
 * JD-Core Version:    0.6.2
 */