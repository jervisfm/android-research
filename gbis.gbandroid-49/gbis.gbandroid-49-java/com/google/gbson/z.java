package com.google.gbson;

import java.util.LinkedHashMap;
import java.util.Map.Entry;

final class z<K, V> extends LinkedHashMap<K, V>
  implements c<K, V>
{
  private static final long serialVersionUID = 1L;
  private final int a;

  public z(int paramInt)
  {
    super(paramInt, 0.7F, true);
    this.a = paramInt;
  }

  public final V a(K paramK)
  {
    try
    {
      Object localObject2 = get(paramK);
      return localObject2;
    }
    finally
    {
      localObject1 = finally;
      throw localObject1;
    }
  }

  public final void a(K paramK, V paramV)
  {
    try
    {
      put(paramK, paramV);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  protected final boolean removeEldestEntry(Map.Entry<K, V> paramEntry)
  {
    return size() > this.a;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.google.gbson.z
 * JD-Core Version:    0.6.2
 */