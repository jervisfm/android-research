package com.inmobi.androidsdk;

import android.util.Log;
import com.inmobi.androidsdk.ai.container.IMWebView;
import com.inmobi.androidsdk.impl.AdUnit;
import com.inmobi.androidsdk.impl.Constants;

class IMAdInterstitial$1$1
  implements Runnable
{
  IMAdInterstitial$1$1(IMAdInterstitial.1 param1, Object paramObject)
  {
  }

  public void run()
  {
    try
    {
      IMAdInterstitial.a(IMAdInterstitial.1.a(this.a), (AdUnit)this.b);
      if (IMAdInterstitial.b(IMAdInterstitial.1.a(this.a)) == null)
        IMAdInterstitial.a(IMAdInterstitial.1.a(this.a), new IMWebView(IMAdInterstitial.a(IMAdInterstitial.1.a(this.a)), IMAdInterstitial.c(IMAdInterstitial.1.a(this.a)), true, false));
      IMAdInterstitial.b(IMAdInterstitial.1.a(this.a), IMAdInterstitial.d(IMAdInterstitial.1.a(this.a)));
      return;
    }
    catch (Exception localException)
    {
      if (Constants.DEBUG)
        Log.d("InMobiAndroidSDK_3.5.2", "Error retriving ad", localException);
      IMAdInterstitial.a(IMAdInterstitial.1.a(this.a), IMAdInterstitial.State.INIT);
      IMAdInterstitial.a(IMAdInterstitial.1.a(this.a), 101, IMAdRequest.ErrorCode.INTERNAL_ERROR);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.IMAdInterstitial.1.1
 * JD-Core Version:    0.6.2
 */