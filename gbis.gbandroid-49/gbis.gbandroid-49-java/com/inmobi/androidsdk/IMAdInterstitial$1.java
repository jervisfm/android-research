package com.inmobi.androidsdk;

import android.app.Activity;
import android.util.Log;
import com.inmobi.androidsdk.impl.Constants;
import com.inmobi.androidsdk.impl.net.HttpRequestCallback;

class IMAdInterstitial$1
  implements HttpRequestCallback
{
  IMAdInterstitial$1(IMAdInterstitial paramIMAdInterstitial)
  {
  }

  public void notifyResult(int paramInt, Object paramObject)
  {
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", ">>> Got HTTP REQUEST callback. Status: " + paramInt + " ,data=" + paramObject);
    if (paramInt == 0)
      IMAdInterstitial.a(this.a).runOnUiThread(new IMAdInterstitial.1.1(this, paramObject));
    while (paramInt != 1)
      return;
    IMAdInterstitial.a(this.a, IMAdInterstitial.State.INIT);
    IMAdInterstitial.a(this.a, 101, (IMAdRequest.ErrorCode)paramObject);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.IMAdInterstitial.1
 * JD-Core Version:    0.6.2
 */