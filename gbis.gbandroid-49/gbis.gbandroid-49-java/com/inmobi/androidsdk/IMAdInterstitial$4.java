package com.inmobi.androidsdk;

import android.util.Log;

class IMAdInterstitial$4
  implements Runnable
{
  IMAdInterstitial$4(IMAdInterstitial paramIMAdInterstitial, int paramInt, IMAdRequest.ErrorCode paramErrorCode)
  {
  }

  public void run()
  {
    switch (this.b)
    {
    default:
      return;
    case 100:
      IMAdInterstitial.a(this.a, System.currentTimeMillis());
      IMAdInterstitial.e(this.a).onAdRequestLoaded(this.a);
      return;
    case 101:
      switch (a()[this.c.ordinal()])
      {
      default:
      case 3:
      case 2:
      }
      while (true)
      {
        IMAdInterstitial.e(this.a).onAdRequestFailed(this.a, this.c);
        return;
        Log.i("InMobiAndroidSDK_3.5.2", "Ad click in progress. Your request cannot be processed at this time. Try again later.");
        continue;
        Log.i("InMobiAndroidSDK_3.5.2", "Ad download in progress. Your request cannot be processed at this time. Try again later.");
      }
    case 103:
      IMAdInterstitial.e(this.a).onDismissAdScreen(this.a);
      return;
    case 102:
      IMAdInterstitial.e(this.a).onShowAdScreen(this.a);
      return;
    case 104:
    }
    IMAdInterstitial.e(this.a).onLeaveApplication(this.a);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.IMAdInterstitial.4
 * JD-Core Version:    0.6.2
 */