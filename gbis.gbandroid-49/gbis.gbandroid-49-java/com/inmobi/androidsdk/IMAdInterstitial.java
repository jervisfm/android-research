package com.inmobi.androidsdk;

import android.app.Activity;
import android.os.Build.VERSION;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.inmobi.androidsdk.ai.container.IMWebView;
import com.inmobi.androidsdk.ai.container.IMWebView.IMWebViewListener;
import com.inmobi.androidsdk.ai.controller.util.Utils;
import com.inmobi.androidsdk.impl.AdUnit;
import com.inmobi.androidsdk.impl.AdUnit.AdTypes;
import com.inmobi.androidsdk.impl.Constants;
import com.inmobi.androidsdk.impl.UserInfo;
import com.inmobi.androidsdk.impl.net.HttpRequestCallback;
import com.inmobi.androidsdk.impl.net.RequestResponseManager;
import com.inmobi.androidsdk.impl.net.RequestResponseManager.ActionType;

public class IMAdInterstitial
{
  private static final int i = 303;
  private static final int j = 304;
  private static final int k = 20;
  private static final int m = 100;
  private static final int n = 101;
  private static final int o = 102;
  private static final int p = 103;
  private static final int q = 104;
  private State a = State.INIT;
  private IMAdRequest b;
  private Activity c;
  private String d;
  private IMAdInterstitialListener e;
  private AdUnit f;
  private UserInfo g;
  private IMWebView h;
  private long l = 0L;
  private String r;
  private HttpRequestCallback s = new IMAdInterstitial.1(this);
  private Handler t = new IMAdInterstitial.2(this);
  private IMWebView.IMWebViewListener u = new IMAdInterstitial.3(this);

  public IMAdInterstitial(Activity paramActivity, String paramString)
  {
    String str = Integer.toString(Utils.incrementBaseUrl());
    this.r = ("http://www.inmobi.com/" + str + "/");
    if (paramActivity == null)
      throw new NullPointerException("activity cannot be null");
    if (paramString == null)
      throw new NullPointerException("site-id cannot be null");
    if (paramString.trim().equalsIgnoreCase(""))
      throw new IllegalArgumentException("site-id cannot be empty");
    this.c = paramActivity;
    this.d = paramString;
    a();
  }

  private void a()
  {
    if (this.g == null)
    {
      this.g = new UserInfo(this.c);
      DisplayMetrics localDisplayMetrics = new DisplayMetrics();
      this.c.getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
      float f1 = localDisplayMetrics.density;
      Display localDisplay = ((WindowManager)this.c.getSystemService("window")).getDefaultDisplay();
      int i2 = localDisplay.getWidth();
      int i3 = localDisplay.getHeight();
      this.g.setScreenDensity(String.valueOf(f1));
      this.g.setScreenSize(i2 + "X" + i3);
    }
    try
    {
      if (this.g.getPhoneDefaultUserAgent().equals(""))
      {
        WebView localWebView = new WebView(this.c);
        this.g.setPhoneDefaultUserAgent(localWebView.getSettings().getUserAgentString());
      }
      this.g.updateInfo(this.d, this.b);
      int i1 = 14;
      if ((Build.VERSION.SDK_INT == 11) || (Build.VERSION.SDK_INT == 12) || (Build.VERSION.SDK_INT == 13))
        i1 = 17;
      this.g.setAdUnitSlot(String.valueOf(i1));
      return;
    }
    catch (Exception localException)
    {
      while (true)
        Log.w("InMobiAndroidSDK_3.5.2", "Exception occured while setting user agent" + localException);
    }
  }

  private void a(int paramInt, IMAdRequest.ErrorCode paramErrorCode)
  {
    if (this.e == null)
      return;
    this.c.runOnUiThread(new IMAdInterstitial.4(this, paramInt, paramErrorCode));
  }

  private void a(AdUnit paramAdUnit)
  {
    if ((paramAdUnit != null) && (AdUnit.AdTypes.NONE != paramAdUnit.getAdType()) && (paramAdUnit.getCDATABlock() != null))
    {
      String str = paramAdUnit.getCDATABlock().replaceAll("%", "%25");
      if (Constants.DEBUG)
        Log.d("InMobiAndroidSDK_3.5.2", "Final HTML String: " + str);
      this.h.requestOnPageFinishedCallback(this.t.obtainMessage(303));
      this.h.loadDataWithBaseURL(this.r, "<html><head><meta name=\"viewport\" content=\"width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1\"><meta http-equiv=\"Content-Type\" content=\"text/html charset=utf-16le\"></head><body style=\"margin:0;padding:0\">" + str + "</body></html>", "text/html", null, this.r);
    }
  }

  private boolean a(IMAdRequest paramIMAdRequest)
  {
    long l1 = System.currentTimeMillis();
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "Time gap: " + (l1 - this.l));
    if (l1 - this.l < 20000L)
      Log.v("InMobiAndroidSDK_3.5.2", "Ad cannot be refreshed now, as the minimum refresh interval is20 seconds.");
    while (true)
    {
      return false;
      if (paramIMAdRequest == null);
      for (boolean bool = false; (bool) || (Utils.validateAppId(this.d)); bool = paramIMAdRequest.isTestMode())
        return true;
    }
  }

  public IMAdInterstitialListener getImAdInterstitialListener()
  {
    return this.e;
  }

  public State getState()
  {
    return this.a;
  }

  public void loadNewAd(IMAdRequest paramIMAdRequest)
  {
    if (Constants.DEBUG)
    {
      Log.d("InMobiAndroidSDK_3.5.2", " ");
      Log.e("InMobiAndroidSDK_3.5.2", ">>>> Start loading new Interstitial Ad <<<<");
    }
    if (!a(paramIMAdRequest))
    {
      a(101, IMAdRequest.ErrorCode.INVALID_REQUEST);
      return;
    }
    if (this.a == State.LOADING)
    {
      a(101, IMAdRequest.ErrorCode.AD_DOWNLOAD_IN_PROGRESS);
      return;
    }
    if (this.a == State.ACTIVE)
    {
      Log.w("InMobiAndroidSDK_3.5.2", "Interstitial ad is in ACTIVE state. Try again after sometime.");
      a(101, IMAdRequest.ErrorCode.INVALID_REQUEST);
      return;
    }
    this.a = State.LOADING;
    this.b = paramIMAdRequest;
    a();
    new RequestResponseManager(this.c).asyncRequestAd(this.g, RequestResponseManager.ActionType.AdRequest_Interstitial, this.s);
  }

  public void setImAdInterstitialListener(IMAdInterstitialListener paramIMAdInterstitialListener)
  {
    this.e = paramIMAdInterstitialListener;
  }

  public void show()
  {
    do
      try
      {
        if (Constants.DEBUG)
          Log.d("InMobiAndroidSDK_3.5.2", "Showing the Interstitial Ad.");
        if (this.a != State.READY)
          throw new IllegalStateException("Interstitial ad is not in the 'READY' state. Current state: " + this.a);
      }
      catch (Exception localException)
      {
        if (Constants.DEBUG)
          Log.d("InMobiAndroidSDK_3.5.2", "Error showing ad", localException);
        return;
      }
    while (this.f == null);
    this.h.setAdUnit(this.f);
    this.h.requestOnInterstitialClosed(this.t.obtainMessage(304));
    this.h.changeContentAreaForInterstitials();
  }

  public static enum State
  {
    static
    {
      LOADING = new State("LOADING", 2);
      ACTIVE = new State("ACTIVE", 3);
      State[] arrayOfState = new State[4];
      arrayOfState[0] = INIT;
      arrayOfState[1] = READY;
      arrayOfState[2] = LOADING;
      arrayOfState[3] = ACTIVE;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.IMAdInterstitial
 * JD-Core Version:    0.6.2
 */