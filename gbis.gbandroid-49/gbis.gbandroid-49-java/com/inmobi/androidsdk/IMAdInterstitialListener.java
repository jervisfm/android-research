package com.inmobi.androidsdk;

public abstract interface IMAdInterstitialListener
{
  public abstract void onAdRequestFailed(IMAdInterstitial paramIMAdInterstitial, IMAdRequest.ErrorCode paramErrorCode);

  public abstract void onAdRequestLoaded(IMAdInterstitial paramIMAdInterstitial);

  public abstract void onDismissAdScreen(IMAdInterstitial paramIMAdInterstitial);

  public abstract void onLeaveApplication(IMAdInterstitial paramIMAdInterstitial);

  public abstract void onShowAdScreen(IMAdInterstitial paramIMAdInterstitial);
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.IMAdInterstitialListener
 * JD-Core Version:    0.6.2
 */