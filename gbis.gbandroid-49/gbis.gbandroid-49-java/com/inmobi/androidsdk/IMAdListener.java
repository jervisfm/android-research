package com.inmobi.androidsdk;

public abstract interface IMAdListener
{
  public abstract void onAdRequestCompleted(IMAdView paramIMAdView);

  public abstract void onAdRequestFailed(IMAdView paramIMAdView, IMAdRequest.ErrorCode paramErrorCode);

  public abstract void onDismissAdScreen(IMAdView paramIMAdView);

  public abstract void onLeaveApplication(IMAdView paramIMAdView);

  public abstract void onShowAdScreen(IMAdView paramIMAdView);
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.IMAdListener
 * JD-Core Version:    0.6.2
 */