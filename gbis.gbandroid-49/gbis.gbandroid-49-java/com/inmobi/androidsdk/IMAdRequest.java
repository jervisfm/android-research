package com.inmobi.androidsdk;

import android.location.Location;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class IMAdRequest
{
  public static final int ID_DEVICE_NONE = 1;
  public static final int ID_DEVICE_ODIN_1 = 2;
  private boolean a = true;
  private Location b;
  private String c;
  private boolean d = false;
  private String e;
  private String f;
  private Date g;
  private GenderType h;
  private String i;
  private String j;
  private int k;
  private EducationType l;
  private EthnicityType m;
  private int n;
  private String o;
  private Map<String, String> p;
  private int q = 2;
  private Map<IMIDType, String> r = new HashMap();

  public void addIDType(IMIDType paramIMIDType, String paramString)
  {
    if (this.r != null)
      this.r.put(paramIMIDType, paramString);
  }

  public int getAge()
  {
    return this.n;
  }

  public String getAreaCode()
  {
    return this.f;
  }

  public Location getCurrentLocation()
  {
    return this.b;
  }

  public Date getDateOfBirth()
  {
    return this.g;
  }

  public int getDeviceIdMask()
  {
    return this.q;
  }

  public EducationType getEducation()
  {
    return this.l;
  }

  public EthnicityType getEthnicity()
  {
    return this.m;
  }

  public GenderType getGender()
  {
    return this.h;
  }

  public String getIDType(IMIDType paramIMIDType)
  {
    if (this.r != null)
      return (String)this.r.get(paramIMIDType);
    return null;
  }

  public int getIncome()
  {
    return this.k;
  }

  public String getInterests()
  {
    return this.o;
  }

  public String getKeywords()
  {
    return this.i;
  }

  public String getLocationWithCityStateCountry()
  {
    return this.c;
  }

  public String getPostalCode()
  {
    return this.e;
  }

  public Map<String, String> getRequestParams()
  {
    return this.p;
  }

  public String getSearchString()
  {
    return this.j;
  }

  public boolean isLocationInquiryAllowed()
  {
    return this.a;
  }

  public boolean isTestMode()
  {
    return this.d;
  }

  public void removeIDType(IMIDType paramIMIDType)
  {
    if (this.r != null)
      this.r.remove(paramIMIDType);
  }

  public void setAge(int paramInt)
  {
    this.n = paramInt;
  }

  public void setAreaCode(String paramString)
  {
    this.f = paramString;
  }

  public void setCurrentLocation(Location paramLocation)
  {
    this.b = paramLocation;
  }

  public void setDateOfBirth(Date paramDate)
  {
    this.g = paramDate;
  }

  public void setDeviceIDMask(int paramInt)
  {
    this.q = paramInt;
  }

  public void setEducation(EducationType paramEducationType)
  {
    this.l = paramEducationType;
  }

  public void setEthnicity(EthnicityType paramEthnicityType)
  {
    this.m = paramEthnicityType;
  }

  public void setGender(GenderType paramGenderType)
  {
    this.h = paramGenderType;
  }

  public void setIncome(int paramInt)
  {
    this.k = paramInt;
  }

  public void setInterests(String paramString)
  {
    this.o = paramString;
  }

  public void setKeywords(String paramString)
  {
    this.i = paramString;
  }

  public void setLocationInquiryAllowed(boolean paramBoolean)
  {
    this.a = paramBoolean;
  }

  public void setLocationWithCityStateCountry(String paramString1, String paramString2, String paramString3)
  {
    this.c = (paramString1 + "-" + paramString2 + "-" + paramString3);
  }

  public void setPostalCode(String paramString)
  {
    this.e = paramString;
  }

  public void setRequestParams(Map<String, String> paramMap)
  {
    this.p = paramMap;
  }

  public void setSearchString(String paramString)
  {
    this.j = paramString;
  }

  public void setTestMode(boolean paramBoolean)
  {
    this.d = paramBoolean;
  }

  public static enum EducationType
  {
    static
    {
      Edu_HighSchool = new EducationType("Edu_HighSchool", 1);
      Edu_SomeCollege = new EducationType("Edu_SomeCollege", 2);
      Edu_InCollege = new EducationType("Edu_InCollege", 3);
      Edu_BachelorsDegree = new EducationType("Edu_BachelorsDegree", 4);
      Edu_MastersDegree = new EducationType("Edu_MastersDegree", 5);
      Edu_DoctoralDegree = new EducationType("Edu_DoctoralDegree", 6);
      Edu_Other = new EducationType("Edu_Other", 7);
      EducationType[] arrayOfEducationType = new EducationType[8];
      arrayOfEducationType[0] = Edu_None;
      arrayOfEducationType[1] = Edu_HighSchool;
      arrayOfEducationType[2] = Edu_SomeCollege;
      arrayOfEducationType[3] = Edu_InCollege;
      arrayOfEducationType[4] = Edu_BachelorsDegree;
      arrayOfEducationType[5] = Edu_MastersDegree;
      arrayOfEducationType[6] = Edu_DoctoralDegree;
      arrayOfEducationType[7] = Edu_Other;
    }
  }

  public static enum ErrorCode
  {
    static
    {
      AD_DOWNLOAD_IN_PROGRESS = new ErrorCode("AD_DOWNLOAD_IN_PROGRESS", 1);
      AD_CLICK_IN_PROGRESS = new ErrorCode("AD_CLICK_IN_PROGRESS", 2);
      NETWORK_ERROR = new ErrorCode("NETWORK_ERROR", 3);
      INTERNAL_ERROR = new ErrorCode("INTERNAL_ERROR", 4);
      NO_FILL = new ErrorCode("NO_FILL", 5);
      ErrorCode[] arrayOfErrorCode = new ErrorCode[6];
      arrayOfErrorCode[0] = INVALID_REQUEST;
      arrayOfErrorCode[1] = AD_DOWNLOAD_IN_PROGRESS;
      arrayOfErrorCode[2] = AD_CLICK_IN_PROGRESS;
      arrayOfErrorCode[3] = NETWORK_ERROR;
      arrayOfErrorCode[4] = INTERNAL_ERROR;
      arrayOfErrorCode[5] = NO_FILL;
    }
  }

  public static enum EthnicityType
  {
    static
    {
      Eth_Mixed = new EthnicityType("Eth_Mixed", 1);
      Eth_Asian = new EthnicityType("Eth_Asian", 2);
      Eth_Black = new EthnicityType("Eth_Black", 3);
      Eth_Hispanic = new EthnicityType("Eth_Hispanic", 4);
      Eth_NativeAmerican = new EthnicityType("Eth_NativeAmerican", 5);
      Eth_White = new EthnicityType("Eth_White", 6);
      Eth_Other = new EthnicityType("Eth_Other", 7);
      EthnicityType[] arrayOfEthnicityType = new EthnicityType[8];
      arrayOfEthnicityType[0] = Eth_None;
      arrayOfEthnicityType[1] = Eth_Mixed;
      arrayOfEthnicityType[2] = Eth_Asian;
      arrayOfEthnicityType[3] = Eth_Black;
      arrayOfEthnicityType[4] = Eth_Hispanic;
      arrayOfEthnicityType[5] = Eth_NativeAmerican;
      arrayOfEthnicityType[6] = Eth_White;
      arrayOfEthnicityType[7] = Eth_Other;
    }
  }

  public static enum GenderType
  {
    static
    {
      MALE = new GenderType("MALE", 1);
      FEMALE = new GenderType("FEMALE", 2);
      GenderType[] arrayOfGenderType = new GenderType[3];
      arrayOfGenderType[0] = NONE;
      arrayOfGenderType[1] = MALE;
      arrayOfGenderType[2] = FEMALE;
    }
  }

  public static enum IMIDType
  {
    static
    {
      IMIDType[] arrayOfIMIDType = new IMIDType[2];
      arrayOfIMIDType[0] = ID_LOGIN;
      arrayOfIMIDType[1] = ID_SESSION;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.IMAdRequest
 * JD-Core Version:    0.6.2
 */