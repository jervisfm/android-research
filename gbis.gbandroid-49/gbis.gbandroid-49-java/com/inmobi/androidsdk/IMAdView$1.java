package com.inmobi.androidsdk;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

class IMAdView$1 extends Handler
{
  IMAdView$1(IMAdView paramIMAdView)
  {
  }

  public void handleMessage(Message paramMessage)
  {
    switch (paramMessage.what)
    {
    default:
    case 100:
      do
        return;
      while (IMAdView.a(this.a) == -1);
      if (!IMAdView.b(this.a).hasWindowFocus())
        Log.v("InMobiAndroidSDK_3.5.2", "Activity is not in the foreground. New ad will not be loaded.");
      while (true)
      {
        sendEmptyMessageDelayed(100, 1000 * IMAdView.a(this.a));
        return;
        this.a.loadNewAd();
      }
    case 101:
      if (IMAdView.c(this.a))
      {
        IMAdView.d(this.a);
        IMAdView.a(this.a, false);
      }
      while (true)
      {
        IMAdView.a(this.a, System.currentTimeMillis());
        IMAdView.a(this.a, 100, null);
        return;
        if (IMAdView.e(this.a) == IMAdView.AnimationType.ANIMATION_OFF)
          IMAdView.d(this.a);
        else
          IMAdView.f(this.a).a(IMAdView.e(this.a));
      }
    case 102:
      IMAdView.b(this.a, false);
      return;
    case 103:
      IMAdView.g(this.a);
      return;
    case 104:
      IMAdView.a(this.a, 103, null);
      return;
    case 105:
      IMAdView.a(this.a, 102, null);
      return;
    case 106:
    }
    IMAdView.a(this.a, 104, null);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.IMAdView.1
 * JD-Core Version:    0.6.2
 */