package com.inmobi.androidsdk;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import com.inmobi.androidsdk.impl.Constants;

class IMAdView$2
  implements View.OnTouchListener
{
  IMAdView$2(IMAdView paramIMAdView)
  {
  }

  public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "onTouch: view: " + paramView + ", event: " + paramMotionEvent);
    if ((IMAdView.h(this.a) != null) && (paramView.equals(IMAdView.h(this.a))))
      paramView.requestFocusFromTouch();
    do
    {
      return true;
      if ((IMAdView.i(this.a) != null) && (paramView.equals(IMAdView.i(this.a))))
      {
        paramView.requestFocusFromTouch();
        return true;
      }
      if (paramMotionEvent.getAction() == 1)
      {
        IMAdView.j(this.a);
        IMAdView.g(this.a);
        return true;
      }
      if (paramMotionEvent.getAction() == 0)
      {
        if ((IMAdView.k(this.a)) || (IMAdView.l(this.a)) || (IMAdView.m(this.a) == null))
        {
          IMAdView.j(this.a);
          return true;
        }
        IMAdView.n(this.a);
        return true;
      }
      if (paramMotionEvent.getAction() == 3)
      {
        IMAdView.j(this.a);
        return true;
      }
    }
    while (paramMotionEvent.getAction() != 4);
    IMAdView.j(this.a);
    return true;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.IMAdView.2
 * JD-Core Version:    0.6.2
 */