package com.inmobi.androidsdk;

import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import com.inmobi.androidsdk.ai.container.IMWebView;
import com.inmobi.androidsdk.impl.AdUnit;
import com.inmobi.androidsdk.impl.AdUnit.AdActionNames;
import com.inmobi.androidsdk.impl.AdUnit.AdTypes;
import com.inmobi.androidsdk.impl.Constants;

class IMAdView$3
  implements Animation.AnimationListener
{
  IMAdView$3(IMAdView paramIMAdView)
  {
  }

  public void onAnimationEnd(Animation paramAnimation)
  {
    try
    {
      if (paramAnimation.equals(this.a.a()))
      {
        this.a.removeAllViews();
        if (IMAdView.o(this.a))
        {
          this.a.addView(IMAdView.h(this.a));
          if ((IMAdView.m(this.a) != null) && (IMAdView.m(this.a).getAdActionName() == AdUnit.AdActionNames.AdActionName_Search))
            IMAdView.h(this.a).requestFocusFromTouch();
        }
        while (true)
        {
          if ((IMAdView.m(this.a).getAdType() != AdUnit.AdTypes.RICH_MEDIA) && (IMAdView.m(this.a).getAdActionName() != AdUnit.AdActionNames.AdActionName_Search))
            this.a.addView(IMAdView.p(this.a));
          this.a.startAnimation(this.a.b());
          return;
          this.a.addView(IMAdView.i(this.a));
          if ((IMAdView.m(this.a) != null) && (IMAdView.m(this.a).getAdActionName() == AdUnit.AdActionNames.AdActionName_Search))
            IMAdView.i(this.a).requestFocusFromTouch();
        }
      }
    }
    catch (Exception localException)
    {
      if (Constants.DEBUG)
      {
        Log.d("InMobiAndroidSDK_3.5.2", "Error animating banner ads");
        return;
        IMAdView localIMAdView = this.a;
        boolean bool1 = IMAdView.o(this.a);
        boolean bool2 = false;
        if (bool1);
        while (true)
        {
          IMAdView.c(localIMAdView, bool2);
          IMAdView.d(this.a, false);
          IMAdView.j(this.a);
          return;
          bool2 = true;
        }
      }
    }
  }

  public void onAnimationRepeat(Animation paramAnimation)
  {
  }

  public void onAnimationStart(Animation paramAnimation)
  {
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.IMAdView.3
 * JD-Core Version:    0.6.2
 */