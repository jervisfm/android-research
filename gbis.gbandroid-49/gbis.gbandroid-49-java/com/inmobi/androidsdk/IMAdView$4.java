package com.inmobi.androidsdk;

import android.app.Activity;
import android.util.Log;
import com.inmobi.androidsdk.impl.Constants;
import com.inmobi.androidsdk.impl.net.HttpRequestCallback;

class IMAdView$4
  implements HttpRequestCallback
{
  IMAdView$4(IMAdView paramIMAdView)
  {
  }

  public void notifyResult(int paramInt, Object paramObject)
  {
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", ">>> Got HTTP REQUEST callback. Status: " + paramInt + " ,data=" + paramObject);
    if (paramInt == 0)
      IMAdView.b(this.a).runOnUiThread(new IMAdView.4.1(this, paramObject));
    while (paramInt != 1)
      return;
    IMAdView.a(this.a, 101, (IMAdRequest.ErrorCode)paramObject);
    IMAdView.d(this.a, false);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.IMAdView.4
 * JD-Core Version:    0.6.2
 */