package com.inmobi.androidsdk;

import android.util.Log;
import com.inmobi.androidsdk.impl.Constants;

class IMAdView$6
  implements Runnable
{
  IMAdView$6(IMAdView paramIMAdView, int paramInt, IMAdRequest.ErrorCode paramErrorCode)
  {
  }

  public void run()
  {
    try
    {
      switch (this.b)
      {
      case 100:
        IMAdView.q(this.a).onAdRequestCompleted(this.a);
        return;
      case 101:
      case 103:
      case 102:
      case 104:
      }
    }
    catch (Exception localException)
    {
      if (Constants.DEBUG)
      {
        Log.w("InMobiAndroidSDK_3.5.2", "Exception giving callback to the publisher", localException);
        return;
        switch (a()[this.c.ordinal()])
        {
        case 4:
        case 5:
        default:
        case 3:
        case 2:
        case 6:
        }
        while (true)
        {
          IMAdView.q(this.a).onAdRequestFailed(this.a, this.c);
          return;
          Log.w("InMobiAndroidSDK_3.5.2", "Ad click in progress. Your request cannot be processed at this time. Try again later.");
          continue;
          Log.w("InMobiAndroidSDK_3.5.2", "Ad download in progress. Your request cannot be processed at this time. Try again later.");
          continue;
          Log.w("InMobiAndroidSDK_3.5.2", "Ad request successful, but no ad was returned due to lack of ad inventory.");
        }
        IMAdView.q(this.a).onDismissAdScreen(this.a);
        return;
        IMAdView.q(this.a).onShowAdScreen(this.a);
        return;
        IMAdView.q(this.a).onLeaveApplication(this.a);
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.IMAdView.6
 * JD-Core Version:    0.6.2
 */