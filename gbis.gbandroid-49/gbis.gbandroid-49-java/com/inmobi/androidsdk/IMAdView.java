package com.inmobi.androidsdk;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.webkit.WebSettings;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import com.inmobi.androidsdk.ai.container.IMWebView;
import com.inmobi.androidsdk.ai.container.IMWebView.IMWebViewListener;
import com.inmobi.androidsdk.ai.container.IMWebView.ViewState;
import com.inmobi.androidsdk.ai.controller.util.Utils;
import com.inmobi.androidsdk.impl.AdUnit;
import com.inmobi.androidsdk.impl.AdUnit.AdActionNames;
import com.inmobi.androidsdk.impl.AdUnit.AdTypes;
import com.inmobi.androidsdk.impl.ClickProcessingTask;
import com.inmobi.androidsdk.impl.Constants;
import com.inmobi.androidsdk.impl.UserInfo;
import com.inmobi.androidsdk.impl.net.HttpRequestCallback;
import com.inmobi.androidsdk.impl.net.RequestResponseManager;
import com.inmobi.androidsdk.impl.net.RequestResponseManager.ActionType;
import java.util.concurrent.atomic.AtomicBoolean;

public final class IMAdView extends RelativeLayout
{
  public static final int INMOBI_AD_UNIT_120X600 = 13;
  public static final int INMOBI_AD_UNIT_300X250 = 10;
  public static final int INMOBI_AD_UNIT_320X48 = 9;
  public static final int INMOBI_AD_UNIT_320X50 = 15;
  public static final int INMOBI_AD_UNIT_468X60 = 12;
  public static final int INMOBI_AD_UNIT_728X90 = 11;
  public static final String INMOBI_INTERNAL_TAG = "ref-__in__rt";
  public static final String INMOBI_REF_TAG = "ref-tag";
  private static final int K = 100;
  private static final int L = 101;
  private static final int M = 102;
  private static final int N = 103;
  private static final int O = 104;
  public static final int REFRESH_INTERVAL_DEFAULT = 60;
  public static final int REFRESH_INTERVAL_OFF = -1;
  private static final int a = 20;
  private static final int b = 100;
  private static final int c = 101;
  private static final int d = 102;
  private static final int e = 103;
  private static final int f = 104;
  private static final int g = 105;
  private static final int h = 106;
  private static final String x = "<style>#im_c { background: -webkit-gradient(linear, left top, left bottom, from(#BGCOLOR1), to(#BGCOLOR2)) !important;\tbackground: -moz-linear-gradient(top,  #BGCOLOR1,  #BGCOLOR2) !important;} </style>";
  private static final String y = "<style>#im_c { \tbackground:#BGCOLOR1 !important;} </style>";
  private static final String z = "<style>#im_text {\tcolor:#TEXTCOLOR !important;} </style>";
  private IMAdListener A;
  private IMAdRequest B;
  private String C;
  private int D;
  private boolean E;
  private long F = 0L;
  private boolean G = true;
  private AnimationType H = AnimationType.ROTATE_HORIZONTAL_AXIS;
  private boolean I = true;
  private a J;
  private String P;
  private Handler Q = new IMAdView.1(this);
  private View.OnTouchListener R = new IMAdView.2(this);
  private Animation.AnimationListener S = new IMAdView.3(this);
  private HttpRequestCallback T = new IMAdView.4(this);
  private IMWebView.IMWebViewListener U = new IMAdView.5(this);
  private int i = 60;
  private IMWebView j;
  private IMWebView k;
  private LinearLayout l;
  private Activity m;
  private boolean n = true;
  private UserInfo o;
  private AtomicBoolean p = new AtomicBoolean();
  private AtomicBoolean q = new AtomicBoolean();
  private Animation r;
  private Animation s;
  private AdUnit t = null;
  private String u;
  private String v;
  private String w;

  public IMAdView(Activity paramActivity, int paramInt, String paramString)
  {
    super(paramActivity);
    String str = Integer.toString(Utils.incrementBaseUrl());
    this.P = ("http://www.inmobi.com/" + str + "/");
    a(paramActivity, paramInt, paramString);
  }

  private IMAdView(Context paramContext)
  {
    super(paramContext);
  }

  public IMAdView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    String str1 = Integer.toString(Utils.incrementBaseUrl());
    this.P = ("http://www.inmobi.com/" + str1 + "/");
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "IMAdView Constructor context: " + paramContext);
    String str2 = paramAttributeSet.getAttributeValue(null, "appId");
    int i1 = paramAttributeSet.getAttributeIntValue(null, "adSlot", -1);
    if ((str2 == null) || (str2.trim().equals("")))
      throw new IllegalArgumentException("Please provide a valid 'appId' attribute in the 'com.inmobi.androidsdk.IMAdView' tag of layout XML. For example, appId=\"yourAppId\"");
    if (i1 < 0)
      throw new IllegalArgumentException("Please provide a valid 'adSlot' attribute in the 'com.inmobi.androidsdk.IMAdView' tag of layout XML. For example, adSlot=\"yourAddSlot\"");
    a((Activity)paramContext, i1, str2);
  }

  private void a(int paramInt, IMAdRequest.ErrorCode paramErrorCode)
  {
    if (!this.I)
      Log.w("InMobiAndroidSDK_3.5.2", "IMAdView not sending callback because the view is not added to any window.");
    while (this.A == null)
      return;
    this.m.runOnUiThread(new IMAdView.6(this, paramInt, paramErrorCode));
  }

  private void a(Activity paramActivity, int paramInt, String paramString)
  {
    if (paramActivity == null)
      throw new NullPointerException("Activity cannot be null");
    if (paramInt < 0)
      throw new IllegalArgumentException("Ad Slot value cannot be negative");
    if (paramString == null)
      throw new NullPointerException("App ID cannot be null");
    if (paramString.trim().equalsIgnoreCase(""))
      throw new IllegalArgumentException("App ID cannot be empty");
    this.C = paramString;
    this.D = paramInt;
    this.m = paramActivity;
    if (this.j == null)
      this.j = new IMWebView(this.m, this.U, false, false);
    if (this.k == null)
    {
      this.k = new IMWebView(this.m, this.U, false, false);
      addView(this.k);
    }
    if (this.l == null)
    {
      this.l = new LinearLayout(this.m);
      this.l.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
      this.l.setOnTouchListener(this.R);
      this.l.setBackgroundColor(0);
      addView(this.l);
    }
    c();
    this.J = new a(this, this.S);
  }

  private void a(AdUnit paramAdUnit)
  {
    String str;
    if ((paramAdUnit != null) && (AdUnit.AdTypes.NONE != paramAdUnit.getAdType()) && (paramAdUnit.getCDATABlock() != null))
    {
      StringBuffer localStringBuffer = new StringBuffer(paramAdUnit.getCDATABlock());
      if (paramAdUnit.getAdType() == AdUnit.AdTypes.TEXT)
        a(localStringBuffer);
      str = localStringBuffer.toString().replaceAll("%", "%25");
      if (!h())
        break label208;
      if (this.j == null)
        this.j = new IMWebView(this.m, this.U, false, false);
    }
    for (IMWebView localIMWebView = this.j; ; localIMWebView = this.k)
    {
      if (paramAdUnit.getAdActionName() == AdUnit.AdActionNames.AdActionName_Search)
      {
        if (Constants.DEBUG)
          Log.d("InMobiAndroidSDK_3.5.2", "It came to AdActionType_Search method of displayad");
        localIMWebView.requestOnSearchAdClicked(this.Q.obtainMessage(103));
      }
      localIMWebView.setAdUnit(paramAdUnit);
      localIMWebView.requestOnPageFinishedCallback(this.Q.obtainMessage(101));
      localIMWebView.reinitializeExpandProperties();
      localIMWebView.loadDataWithBaseURL(this.P, "<html><head><meta name=\"viewport\" content=\"width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1\"><meta http-equiv=\"Content-Type\" content=\"text/html charset=utf-16le\"></head><body style=\"margin:0;padding:0\">" + str + "</body></html>", "text/html", null, this.P);
      return;
      label208: if (this.k == null)
        this.k = new IMWebView(this.m, this.U, false, false);
    }
  }

  private void a(StringBuffer paramStringBuffer)
  {
    if (this.u != null)
    {
      if (this.v == null)
        break label66;
      paramStringBuffer.append("<style>#im_c { background: -webkit-gradient(linear, left top, left bottom, from(#BGCOLOR1), to(#BGCOLOR2)) !important;\tbackground: -moz-linear-gradient(top,  #BGCOLOR1,  #BGCOLOR2) !important;} </style>".replaceAll("#BGCOLOR1", this.u).replaceAll("#BGCOLOR2", this.v));
    }
    while (true)
    {
      if (this.w != null)
        paramStringBuffer.append("<style>#im_text {\tcolor:#TEXTCOLOR !important;} </style>".replaceAll("#TEXTCOLOR", this.w));
      return;
      label66: paramStringBuffer.append("<style>#im_c { \tbackground:#BGCOLOR1 !important;} </style>".replaceAll("#BGCOLOR1", this.u));
    }
  }

  private void a(boolean paramBoolean)
  {
    this.p.set(paramBoolean);
  }

  private boolean a(String paramString)
  {
    if (paramString == null)
      throw new IllegalArgumentException("color cannot be null");
    int i1 = paramString.length();
    if ((!paramString.startsWith("#")) || ((i1 != 4) && (i1 != 7)))
      throw new IllegalArgumentException("color should be of the format #rgb or #rrggbb ");
    return true;
  }

  private void b(boolean paramBoolean)
  {
    this.q.set(paramBoolean);
  }

  private void c()
  {
    if (this.o == null)
    {
      this.o = new UserInfo(this.m.getApplicationContext());
      DisplayMetrics localDisplayMetrics = new DisplayMetrics();
      this.m.getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
      float f1 = localDisplayMetrics.density;
      Display localDisplay = ((WindowManager)this.m.getSystemService("window")).getDefaultDisplay();
      int i1 = localDisplay.getWidth();
      int i2 = localDisplay.getHeight();
      this.o.setScreenDensity(String.valueOf(f1));
      this.o.setScreenSize(i1 + "X" + i2);
    }
    try
    {
      if ((this.j != null) && (this.o.getPhoneDefaultUserAgent().equals("")))
        this.o.setPhoneDefaultUserAgent(this.j.getSettings().getUserAgentString());
      this.o.setAdUnitSlot(String.valueOf(this.D));
      this.o.updateInfo(this.C, this.B);
      return;
    }
    catch (Exception localException)
    {
      while (true)
        Log.w("InMobiAndroidSDK_3.5.2", "Exception occured while setting user agent" + localException);
    }
  }

  private void c(boolean paramBoolean)
  {
    this.n = paramBoolean;
    if (paramBoolean)
    {
      this.j.deinit();
      this.j = null;
      return;
    }
    this.k.deinit();
    this.k = null;
  }

  private void d()
  {
    try
    {
      removeAllViews();
      if (h())
      {
        addView(this.j);
        if ((this.t != null) && (this.t.getAdActionName() == AdUnit.AdActionNames.AdActionName_Search))
          this.j.requestFocusFromTouch();
      }
      while (true)
      {
        if ((this.t.getAdType() != AdUnit.AdTypes.RICH_MEDIA) && (this.t.getAdActionName() != AdUnit.AdActionNames.AdActionName_Search))
          addView(this.l);
        boolean bool1 = h();
        bool2 = false;
        if (!bool1)
          break;
        c(bool2);
        a(false);
        k();
        return;
        addView(this.k);
        if ((this.t != null) && (this.t.getAdActionName() == AdUnit.AdActionNames.AdActionName_Search))
          this.k.requestFocusFromTouch();
      }
    }
    catch (Exception localException)
    {
      boolean bool2;
      while (Constants.DEBUG)
      {
        Log.d("InMobiAndroidSDK_3.5.2", "Error swapping banner ads");
        return;
        bool2 = true;
      }
    }
  }

  private boolean e()
  {
    long l1 = System.currentTimeMillis();
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "Time Gap: " + (l1 - this.F));
    if (l1 - this.F < 20000L)
      Log.w("InMobiAndroidSDK_3.5.2", "Ad cannot be refreshed now, as the minimum refresh interval is20 seconds.");
    while ((!this.E) && (!Utils.validateAppId(this.C)))
      return false;
    if (this.D < 0)
    {
      Log.v("InMobiAndroidSDK_3.5.2", "Cannot load ad because adSlot is negative. Please provide a valid adSlot.");
      return false;
    }
    return true;
  }

  private boolean f()
  {
    return this.p.get();
  }

  private boolean g()
  {
    if (this.q.get())
      return true;
    if (h());
    for (IMWebView localIMWebView = this.k; ; localIMWebView = this.j)
    {
      String str = localIMWebView.getState();
      if (Constants.DEBUG)
        Log.d("InMobiAndroidSDK_3.5.2", "Current Ad State: " + str);
      if ((!IMWebView.ViewState.EXPANDED.toString().equalsIgnoreCase(str)) && (!IMWebView.ViewState.RESIZED.toString().equalsIgnoreCase(str)) && (!IMWebView.ViewState.EXPANDING.toString().equalsIgnoreCase(str)))
        break;
      Log.w("InMobiAndroidSDK_3.5.2", "Current Ad State is neither default nor loading. New ad will not be shown.");
      return true;
    }
    if (localIMWebView.isBusy())
    {
      Log.w("InMobiAndroidSDK_3.5.2", "New ad will not be shown because the present ad is busy. Eg. Video/audio is playing, etc.");
      return true;
    }
    return false;
  }

  private boolean h()
  {
    return this.n;
  }

  private void i()
  {
    try
    {
      if ((this.t != null) && (!g()))
      {
        b(true);
        if (this.t.getTargetUrl() != null)
        {
          IMBrowserActivity.requestCallbackOnClosed(this.Q.obtainMessage(104));
          new ClickProcessingTask(this.t, this.o, this.m, this.Q.obtainMessage(102), this.Q.obtainMessage(105), this.Q.obtainMessage(106), this.U).execute(new Void[] { null });
        }
      }
      k();
      return;
    }
    catch (Exception localException)
    {
      while (true)
      {
        if (Constants.DEBUG)
        {
          localException.printStackTrace();
          Log.w("InMobiAndroidSDK_3.5.2", "Exception processing ad click", localException);
        }
        b(false);
      }
    }
  }

  private void j()
  {
    try
    {
      int i1 = Color.argb(100, 0, 0, 0);
      if (this.k != null)
        this.k.setBackgroundColor(i1);
      if (this.j != null)
        this.j.setBackgroundColor(i1);
      this.l.setBackgroundColor(i1);
      return;
    }
    catch (Exception localException)
    {
      while (!Constants.DEBUG);
      Log.d("InMobiAndroidSDK_3.5.2", "Error setHighlightedBGColor");
    }
  }

  private void k()
  {
    try
    {
      if (this.k != null)
        this.k.setBackgroundColor(0);
      if (this.j != null)
        this.j.setBackgroundColor(0);
      this.l.setBackgroundColor(0);
      return;
    }
    catch (Exception localException)
    {
      while (!Constants.DEBUG);
      Log.d("InMobiAndroidSDK_3.5.2", "Error setNormalBGColor");
    }
  }

  final Animation a()
  {
    return this.r;
  }

  final void a(Animation paramAnimation)
  {
    this.r = paramAnimation;
  }

  final Animation b()
  {
    return this.s;
  }

  final void b(Animation paramAnimation)
  {
    this.s = paramAnimation;
  }

  public final int getAdSlot()
  {
    return this.D;
  }

  public final String getAppId()
  {
    return this.C;
  }

  public final IMAdListener getIMAdListener()
  {
    return this.A;
  }

  public final IMAdRequest getIMAdRequest()
  {
    return this.B;
  }

  public final void loadNewAd()
  {
    while (true)
    {
      try
      {
        if (Constants.DEBUG)
        {
          Log.d("InMobiAndroidSDK_3.5.2", " ");
          Log.e("InMobiAndroidSDK_3.5.2", ">>>> Start loading new Ad <<<<");
        }
        try
        {
          if (f())
          {
            a(101, IMAdRequest.ErrorCode.AD_DOWNLOAD_IN_PROGRESS);
            return;
          }
          if (g())
          {
            a(101, IMAdRequest.ErrorCode.AD_CLICK_IN_PROGRESS);
            continue;
          }
        }
        catch (Exception localException)
        {
          if (!Constants.DEBUG)
            continue;
          Log.d("InMobiAndroidSDK_3.5.2", "Error in loading ad", localException);
          continue;
        }
      }
      finally
      {
      }
      if (!e())
      {
        a(101, IMAdRequest.ErrorCode.INVALID_REQUEST);
      }
      else
      {
        a(true);
        c();
        new RequestResponseManager(this.m.getApplicationContext()).asyncRequestAd(this.o, RequestResponseManager.ActionType.AdRequest, this.T);
      }
    }
  }

  public final void loadNewAd(IMAdRequest paramIMAdRequest)
  {
    if (paramIMAdRequest != null)
    {
      this.E = paramIMAdRequest.isTestMode();
      setIMAdRequest(paramIMAdRequest);
    }
    loadNewAd();
  }

  protected final void onAttachedToWindow()
  {
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "onAttachedToWindow");
    this.I = true;
    setRefreshInterval(this.i);
    if (this.i != -1)
      loadNewAd();
  }

  protected final void onDetachedFromWindow()
  {
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "onDetachedFromWindow");
    this.I = false;
    setRefreshInterval(-1);
    if (h());
    for (IMWebView localIMWebView = this.k; ; localIMWebView = this.j)
    {
      if (localIMWebView != null)
        localIMWebView.deinit();
      return;
    }
  }

  public final void setAdBackgroundColor(String paramString)
  {
    if (!a(paramString))
      return;
    this.u = paramString;
    a(this.t);
  }

  public final void setAdBackgroundGradientColor(String paramString1, String paramString2)
  {
    if ((!a(paramString1)) && (!a(paramString2)))
      return;
    this.u = paramString1;
    this.v = paramString2;
    a(this.t);
  }

  public final void setAdSlot(int paramInt)
  {
    this.D = paramInt;
  }

  public final void setAdTextColor(String paramString)
  {
    if (!a(paramString))
      return;
    this.w = paramString;
    a(this.t);
  }

  public final void setAnimationType(AnimationType paramAnimationType)
  {
    this.H = paramAnimationType;
  }

  public final void setAppId(String paramString)
  {
    this.C = paramString;
  }

  public final void setIMAdListener(IMAdListener paramIMAdListener)
  {
    this.A = paramIMAdListener;
  }

  public final void setIMAdRequest(IMAdRequest paramIMAdRequest)
  {
    this.B = paramIMAdRequest;
  }

  public final void setRefTagParam(String paramString1, String paramString2)
  {
    if ((paramString1 == null) || (paramString2 == null))
      throw new NullPointerException("Key or value cannot be null");
    if ((paramString1.trim().equals("")) || (paramString2.trim().equals("")))
      throw new IllegalArgumentException("Key or value cannot be empty");
    this.o.setRefTagKey(paramString1.toLowerCase());
    this.o.setRefTagValue(paramString2.toLowerCase());
  }

  public final void setRefreshInterval(int paramInt)
  {
    if (paramInt == -1)
    {
      this.i = -1;
      this.Q.removeMessages(100);
      return;
    }
    if (paramInt < 20)
      throw new IllegalArgumentException("Refresh Interval cannot be less than 20 seconds.");
    this.i = paramInt;
    this.Q.removeMessages(100);
    this.Q.sendEmptyMessageDelayed(100, paramInt * 1000);
  }

  public static enum AnimationType
  {
    static
    {
      ANIMATION_ALPHA = new AnimationType("ANIMATION_ALPHA", 2);
      ROTATE_VERTICAL_AXIS = new AnimationType("ROTATE_VERTICAL_AXIS", 3);
      AnimationType[] arrayOfAnimationType = new AnimationType[4];
      arrayOfAnimationType[0] = ANIMATION_OFF;
      arrayOfAnimationType[1] = ROTATE_HORIZONTAL_AXIS;
      arrayOfAnimationType[2] = ANIMATION_ALPHA;
      arrayOfAnimationType[3] = ROTATE_VERTICAL_AXIS;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.IMAdView
 * JD-Core Version:    0.6.2
 */