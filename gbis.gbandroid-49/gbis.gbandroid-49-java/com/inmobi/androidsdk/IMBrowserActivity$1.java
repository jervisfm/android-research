package com.inmobi.androidsdk;

import android.graphics.Bitmap;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import com.inmobi.androidsdk.ai.container.IMWebView;

class IMBrowserActivity$1 extends WebViewClient
{
  IMBrowserActivity$1(IMBrowserActivity paramIMBrowserActivity)
  {
  }

  public void onPageFinished(WebView paramWebView, String paramString)
  {
    super.onPageFinished(paramWebView, paramString);
    if (IMBrowserActivity.a(this.a) != null)
    {
      if (!paramWebView.canGoForward())
        break label45;
      IMBrowserActivity.a(this.a).setImageBitmap(IMWebView.bitmapFromJar("assets/im_next_arrow_active.png"));
    }
    while (true)
    {
      CookieSyncManager.getInstance().sync();
      return;
      label45: IMBrowserActivity.a(this.a).setImageBitmap(IMWebView.bitmapFromJar("assets/im_next_arrow_inactive.png"));
    }
  }

  public void onPageStarted(WebView paramWebView, String paramString, Bitmap paramBitmap)
  {
    super.onPageStarted(paramWebView, paramString, paramBitmap);
    if (IMBrowserActivity.a(this.a) != null)
      IMBrowserActivity.a(this.a).setImageBitmap(IMWebView.bitmapFromJar("assets/im_next_arrow_inactive.png"));
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.IMBrowserActivity.1
 * JD-Core Version:    0.6.2
 */