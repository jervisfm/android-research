package com.inmobi.androidsdk;

import android.view.View;
import android.view.View.OnClickListener;
import com.inmobi.androidsdk.ai.container.IMWebView;

class IMBrowserActivity$7
  implements View.OnClickListener
{
  IMBrowserActivity$7(IMBrowserActivity paramIMBrowserActivity)
  {
  }

  public void onClick(View paramView)
  {
    if (IMBrowserActivity.b(this.a).canGoBack())
    {
      IMBrowserActivity.b(this.a).goBack();
      return;
    }
    this.a.finish();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.IMBrowserActivity.7
 * JD-Core Version:    0.6.2
 */