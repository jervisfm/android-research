package com.inmobi.androidsdk;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.inmobi.androidsdk.ai.container.IMWebView;
import com.inmobi.androidsdk.ai.container.IMWebView.IMWebViewListener;
import com.inmobi.androidsdk.ai.container.IMWebView.ViewState;
import com.inmobi.androidsdk.impl.Constants;
import java.util.concurrent.atomic.AtomicBoolean;

public class IMBrowserActivity extends Activity
{
  public static final String EXTRA_ADUNIT = "extra_adunit";
  public static final String EXTRA_BROWSER_ACTIVITY_TYPE = "extra_browser_type";
  public static final int EXTRA_BROWSER_CLOSE = 100;
  public static final int EXTRA_BROWSER_EXPAND_URL = 102;
  public static final int EXTRA_BROWSER_STATUS_BAR = 101;
  public static final String EXTRA_URL = "extra_url";
  private static final int b = 100;
  private static IMWebView c;
  private static IMWebView d;
  private static IMWebView f;
  private static IMWebView.IMWebViewListener g;
  private static Message h;
  RelativeLayout a;
  private IMWebView e;
  private float i;
  private boolean j;
  private boolean k;
  private String l;
  private ImageView m;
  private WebViewClient n = new IMBrowserActivity.1(this);

  private void a(ViewGroup paramViewGroup)
  {
    LinearLayout localLinearLayout = new LinearLayout(this);
    localLinearLayout.setOrientation(0);
    localLinearLayout.setId(100);
    localLinearLayout.setWeightSum(100.0F);
    localLinearLayout.setOnTouchListener(new IMBrowserActivity.4(this));
    localLinearLayout.setBackgroundDrawable(new BitmapDrawable(IMWebView.bitmapFromJar("assets/im_bkgrnd.png")));
    RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(-1, (int)(44.0F * this.i));
    localLayoutParams.addRule(12);
    paramViewGroup.addView(localLinearLayout, localLayoutParams);
    LinearLayout.LayoutParams localLayoutParams1 = new LinearLayout.LayoutParams((int)(20.0F * this.i), (int)(20.0F * this.i));
    localLayoutParams1.weight = 25.0F;
    localLayoutParams1.gravity = 16;
    ImageView localImageView1 = new ImageView(this);
    localImageView1.setImageBitmap(IMWebView.bitmapFromJar("assets/im_close_icon.png"));
    localImageView1.setBackgroundColor(17170445);
    localLinearLayout.addView(localImageView1, localLayoutParams1);
    localImageView1.setOnClickListener(new IMBrowserActivity.5(this));
    ImageView localImageView2 = new ImageView(this);
    localImageView2.setImageBitmap(IMWebView.bitmapFromJar("assets/im_refresh.png"));
    localImageView2.setBackgroundColor(17170445);
    localLinearLayout.addView(localImageView2, localLayoutParams1);
    localImageView2.setOnClickListener(new IMBrowserActivity.6(this));
    ImageView localImageView3 = new ImageView(this);
    localImageView3.setImageBitmap(IMWebView.bitmapFromJar("assets/im_previous_arrow_active.png"));
    localImageView3.setBackgroundColor(17170445);
    localLinearLayout.addView(localImageView3, localLayoutParams1);
    localImageView3.setOnClickListener(new IMBrowserActivity.7(this));
    this.m = new ImageView(this);
    this.m.setImageBitmap(IMWebView.bitmapFromJar("assets/im_next_arrow_inactive.png"));
    this.m.setBackgroundColor(17170445);
    localLinearLayout.addView(this.m, localLayoutParams1);
    this.m.setOnClickListener(new IMBrowserActivity.8(this));
  }

  private void a(ViewGroup paramViewGroup, boolean paramBoolean)
  {
    RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams((int)(50.0F * this.i), (int)(50.0F * this.i));
    localLayoutParams.addRule(11);
    ImageView localImageView = new ImageView(this);
    if (paramBoolean)
      localImageView.setImageBitmap(IMWebView.bitmapFromJar("assets/im_close_transparent.png"));
    while (true)
    {
      paramViewGroup.addView(localImageView, localLayoutParams);
      localImageView.setOnClickListener(new IMBrowserActivity.3(this));
      return;
      localImageView.setImageBitmap(IMWebView.bitmapFromJar("assets/im_close_button.png"));
    }
  }

  public static void requestCallbackOnClosed(Message paramMessage)
  {
    h = paramMessage;
  }

  public static void setIntWebView(IMWebView paramIMWebView)
  {
    f = paramIMWebView;
  }

  public static void setOriginalWebView(IMWebView paramIMWebView)
  {
    d = paramIMWebView;
  }

  public static void setWebView(IMWebView paramIMWebView)
  {
    c = paramIMWebView;
  }

  public static void setWebViewListener(IMWebView.IMWebViewListener paramIMWebViewListener)
  {
    g = paramIMWebViewListener;
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    requestWindowFeature(1);
    getWindow().setFlags(1024, 1024);
    WindowManager localWindowManager = (WindowManager)getSystemService("window");
    DisplayMetrics localDisplayMetrics = new DisplayMetrics();
    localWindowManager.getDefaultDisplay().getMetrics(localDisplayMetrics);
    this.i = getResources().getDisplayMetrics().density;
    int i1 = getResources().getDisplayMetrics().widthPixels;
    int i2 = getResources().getDisplayMetrics().heightPixels;
    Intent localIntent = getIntent();
    Bundle localBundle = localIntent.getExtras();
    int i3 = localBundle.getInt("EXPAND_WIDTH");
    int i4 = localBundle.getInt("EXPAND_HEIGHT");
    boolean bool = localBundle.getBoolean("EXPAND_CUSTOM_CLOSE");
    String str1 = localBundle.getString("EXPAND_ORIENTATION");
    int i5 = localBundle.getInt("EXPAND_BACKGROUND_ID");
    String str2 = localBundle.getString("INTERSTITIAL_ORIENTATION");
    String str3 = localIntent.getStringExtra("extra_url");
    this.l = localIntent.getStringExtra("FIRST_INSTANCE");
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "IMBrowserActivity-> onCreate");
    if ((str3 != null) || (str1 != null) || (str2 != null))
    {
      if (str1 == null)
        break label544;
      this.j = true;
      this.k = false;
      c.setExpandedActivity(this);
      d.setExpandedActivity(this);
      d.replaceByPlaceholder();
      if (!str1.equals("portrait"))
        break label509;
      setRequestedOrientation(1);
      if (i1 <= i2)
        break label521;
      if ((i3 <= 0) || (i4 <= 0))
      {
        i4 = i1;
        i3 = i2;
      }
      if (i3 <= i2)
        break label979;
    }
    while (true)
    {
      if (i4 > i1);
      while (true)
      {
        while (true)
        {
          FrameLayout localFrameLayout = new FrameLayout(this);
          FrameLayout.LayoutParams localLayoutParams5 = new FrameLayout.LayoutParams(-1, -1);
          localFrameLayout.setId(i5);
          FrameLayout.LayoutParams localLayoutParams6 = new FrameLayout.LayoutParams(i2, i1);
          RelativeLayout localRelativeLayout2 = new RelativeLayout(this);
          RelativeLayout.LayoutParams localLayoutParams7 = new RelativeLayout.LayoutParams(i2, i1);
          localRelativeLayout2.addView(c, localLayoutParams7);
          a(localRelativeLayout2, bool);
          localFrameLayout.addView(localRelativeLayout2, localLayoutParams6);
          setContentView(localFrameLayout, localLayoutParams5);
          c.videoValidateWidth = i2;
          d.videoValidateWidth = i2;
          d.setState(IMWebView.ViewState.EXPANDED);
          label521: label544: synchronized (d.mutex)
          {
            d.isMutexAquired.set(false);
            d.mutex.notifyAll();
            if (d.mIsExpandUrlValid)
              c.loadUrl(localBundle.getString("EXPAND_WITH_URL"));
            if (d.mListener != null)
              d.mListener.onExpand();
            return;
            label509: setRequestedOrientation(0);
            if (i2 > i1)
              break;
            int i6 = i2;
            i2 = i1;
            i1 = i6;
          }
        }
        this.j = true;
        this.k = true;
        f.setExpandedActivity(this);
        RelativeLayout localRelativeLayout1;
        RelativeLayout.LayoutParams localLayoutParams3;
        ImageView localImageView;
        if (str2.equals("portrait"))
        {
          setRequestedOrientation(1);
          localRelativeLayout1 = new RelativeLayout(this);
          RelativeLayout.LayoutParams localLayoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
          localLayoutParams2.addRule(10);
          localRelativeLayout1.addView(f, localLayoutParams2);
          localLayoutParams3 = new RelativeLayout.LayoutParams((int)(50.0F * this.i), (int)(50.0F * this.i));
          localImageView = new ImageView(this);
          if (localBundle.getBoolean("INTERSTITIAL_CUSTOM_CLOSE"))
            break label806;
          localImageView.setImageBitmap(IMWebView.bitmapFromJar("assets/im_close_button.png"));
        }
        while (true)
        {
          localLayoutParams3.addRule(11);
          localImageView.setId(localBundle.getInt("INTERSTITIAL_CLOSE_ID"));
          localRelativeLayout1.addView(localImageView, localLayoutParams3);
          localImageView.setOnClickListener(new IMBrowserActivity.2(this));
          RelativeLayout.LayoutParams localLayoutParams4 = new RelativeLayout.LayoutParams(-1, -1);
          localRelativeLayout1.setId(localBundle.getInt("INTERSTTIAL_BACKGROUND_ID"));
          localRelativeLayout1.setBackgroundColor(-16777216);
          setContentView(localRelativeLayout1, localLayoutParams4);
          if (f.mListener != null)
            f.mListener.onExpand();
          f.setViewable(true);
          return;
          if (!str2.equals("landscape"))
            break;
          setRequestedOrientation(0);
          break;
          label806: localImageView.setImageBitmap(IMWebView.bitmapFromJar("assets/im_close_transparent.png"));
        }
        this.j = false;
        this.k = false;
        this.a = new RelativeLayout(this);
        this.e = new IMWebView(this, g, true, true);
        RelativeLayout.LayoutParams localLayoutParams1 = new RelativeLayout.LayoutParams(-1, -1);
        localLayoutParams1.addRule(10);
        this.a.setBackgroundColor(-1);
        this.a.addView(this.e, localLayoutParams1);
        a(this.a);
        this.e.getSettings().setJavaScriptEnabled(true);
        this.e.setExternalWebViewClient(this.n);
        this.e.getSettings().setUseWideViewPort(true);
        this.e.loadUrl(localIntent.getStringExtra("extra_url"));
        CookieSyncManager.createInstance(this);
        CookieSyncManager.getInstance().startSync();
        setContentView(this.a);
        return;
        i1 = i4;
      }
      label979: i2 = i3;
    }
  }

  protected void onDestroy()
  {
    super.onDestroy();
    try
    {
      if ((this.e != null) && (!this.j))
        this.e.releaseAllPlayers();
      if ((h != null) && (!this.j) && (this.l != null))
        h.sendToTarget();
      return;
    }
    catch (Exception localException)
    {
      while (!Constants.DEBUG);
      Log.d("InMobiAndroidSDK_3.5.2", "Exception in onDestroy", localException);
    }
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 4)
    {
      IMWebView.userInitiatedClose = true;
      if ((this.j) && (!this.k))
      {
        d.close();
        return true;
      }
      if ((this.j) && (this.k))
      {
        f.close();
        return true;
      }
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  protected void onPause()
  {
    super.onPause();
    CookieSyncManager.getInstance().stopSync();
  }

  protected void onResume()
  {
    super.onResume();
    CookieSyncManager.getInstance().startSync();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.IMBrowserActivity
 * JD-Core Version:    0.6.2
 */