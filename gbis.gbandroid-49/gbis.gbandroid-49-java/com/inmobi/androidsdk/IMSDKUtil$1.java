package com.inmobi.androidsdk;

import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import com.inmobi.androidsdk.impl.Constants;
import com.inmobi.androidsdk.impl.InMobiAndroidTrackerHTTPRequest;

class IMSDKUtil$1 extends Thread
{
  IMSDKUtil$1(Context paramContext, String paramString, SharedPreferences.Editor paramEditor)
  {
  }

  public void run()
  {
    boolean bool = new InMobiAndroidTrackerHTTPRequest(this.a, this.b).setupConnection();
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "Ping Status: " + bool);
    if (bool)
    {
      this.c.putString("InMobi_Prefs_key", "InMobiAdCampaign");
      this.c.commit();
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.IMSDKUtil.1
 * JD-Core Version:    0.6.2
 */