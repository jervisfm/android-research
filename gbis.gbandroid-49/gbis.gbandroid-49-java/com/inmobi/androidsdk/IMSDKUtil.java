package com.inmobi.androidsdk;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import com.inmobi.androidsdk.impl.Constants;

public class IMSDKUtil
{
  public static String getSDKVersion()
  {
    return "3.5.2";
  }

  public static void sendAppTrackerConversion(Context paramContext, String paramString)
  {
    if ((paramContext == null) || (paramString == null))
      try
      {
        throw new NullPointerException("Arguments cannot be null.");
      }
      finally
      {
      }
    if (paramString.trim().equals(""))
      throw new IllegalArgumentException("Advertiser ID cannot be empty.");
    SharedPreferences localSharedPreferences = paramContext.getSharedPreferences("InMobi_Prefs_key", 0);
    SharedPreferences.Editor localEditor = localSharedPreferences.edit();
    if (localSharedPreferences.getString("InMobi_Prefs_key", null) == null)
    {
      if (Constants.DEBUG)
        Log.d("InMobiAndroidSDK_3.5.2", "App tracker ping is not sent. Sending now...");
      new IMSDKUtil.1(paramContext, paramString, localEditor).start();
    }
  }

  public static void setLogLevel(int paramInt)
  {
    if ((paramInt != 0) && (paramInt != 1))
      throw new IllegalArgumentException("Currently, log level can only be 0 or 1");
    if (paramInt == 0)
      Constants.DEBUG = false;
    while (paramInt != 1)
      return;
    Constants.DEBUG = true;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.IMSDKUtil
 * JD-Core Version:    0.6.2
 */