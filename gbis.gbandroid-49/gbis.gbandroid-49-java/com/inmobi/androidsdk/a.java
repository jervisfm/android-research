package com.inmobi.androidsdk;

import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.DecelerateInterpolator;
import com.inmobi.androidsdk.impl.anim.Rotate3dAnimation;
import com.inmobi.androidsdk.impl.anim.Rotate3dAnimationVert;

class a
{
  private IMAdView a;
  private Animation.AnimationListener b;

  public a(IMAdView paramIMAdView, Animation.AnimationListener paramAnimationListener)
  {
    this.a = paramIMAdView;
    this.b = paramAnimationListener;
  }

  public void a(IMAdView.AnimationType paramAnimationType)
  {
    if (paramAnimationType == IMAdView.AnimationType.ANIMATION_ALPHA)
    {
      AlphaAnimation localAlphaAnimation1 = new AlphaAnimation(0.0F, 0.5F);
      AlphaAnimation localAlphaAnimation2 = new AlphaAnimation(0.5F, 1.0F);
      localAlphaAnimation1.setDuration(1000L);
      localAlphaAnimation1.setFillAfter(false);
      localAlphaAnimation1.setAnimationListener(this.b);
      localAlphaAnimation1.setInterpolator(new DecelerateInterpolator());
      localAlphaAnimation2.setDuration(500L);
      localAlphaAnimation2.setFillAfter(false);
      localAlphaAnimation2.setAnimationListener(this.b);
      localAlphaAnimation2.setInterpolator(new DecelerateInterpolator());
      this.a.a(localAlphaAnimation1);
      this.a.b(localAlphaAnimation2);
    }
    while (true)
    {
      this.a.startAnimation(this.a.a());
      return;
      if (paramAnimationType == IMAdView.AnimationType.ROTATE_HORIZONTAL_AXIS)
      {
        Rotate3dAnimation localRotate3dAnimation1 = new Rotate3dAnimation(0.0F, 90.0F, this.a.getWidth() / 2.0F, this.a.getHeight() / 2.0F, 0.0F, true);
        Rotate3dAnimation localRotate3dAnimation2 = new Rotate3dAnimation(270.0F, 360.0F, this.a.getWidth() / 2.0F, this.a.getHeight() / 2.0F, 0.0F, true);
        localRotate3dAnimation1.setDuration(500L);
        localRotate3dAnimation1.setFillAfter(false);
        localRotate3dAnimation1.setAnimationListener(this.b);
        localRotate3dAnimation1.setInterpolator(new AccelerateInterpolator());
        localRotate3dAnimation2.setDuration(500L);
        localRotate3dAnimation2.setFillAfter(false);
        localRotate3dAnimation2.setAnimationListener(this.b);
        localRotate3dAnimation2.setInterpolator(new DecelerateInterpolator());
        this.a.a(localRotate3dAnimation1);
        this.a.b(localRotate3dAnimation2);
      }
      else if (paramAnimationType == IMAdView.AnimationType.ROTATE_VERTICAL_AXIS)
      {
        Rotate3dAnimationVert localRotate3dAnimationVert1 = new Rotate3dAnimationVert(0.0F, 90.0F, this.a.getWidth() / 2.0F, this.a.getHeight() / 2.0F, 0.0F, true);
        Rotate3dAnimationVert localRotate3dAnimationVert2 = new Rotate3dAnimationVert(270.0F, 360.0F, this.a.getWidth() / 2.0F, this.a.getHeight() / 2.0F, 0.0F, true);
        localRotate3dAnimationVert1.setDuration(500L);
        localRotate3dAnimationVert1.setFillAfter(false);
        localRotate3dAnimationVert1.setAnimationListener(this.b);
        localRotate3dAnimationVert1.setInterpolator(new AccelerateInterpolator());
        localRotate3dAnimationVert2.setDuration(500L);
        localRotate3dAnimationVert2.setFillAfter(false);
        localRotate3dAnimationVert2.setAnimationListener(this.b);
        localRotate3dAnimationVert2.setInterpolator(new DecelerateInterpolator());
        this.a.a(localRotate3dAnimationVert1);
        this.a.b(localRotate3dAnimationVert2);
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.a
 * JD-Core Version:    0.6.2
 */