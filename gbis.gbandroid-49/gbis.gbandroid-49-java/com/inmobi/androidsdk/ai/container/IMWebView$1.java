package com.inmobi.androidsdk.ai.container;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.inmobi.androidsdk.ai.controller.util.AVPlayer;
import com.inmobi.androidsdk.impl.Constants;
import com.inmobi.androidsdk.impl.Constants.playerState;
import java.util.Hashtable;

class IMWebView$1 extends Handler
{
  IMWebView$1(IMWebView paramIMWebView)
  {
  }

  public void handleMessage(Message paramMessage)
  {
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "IMWebView->handleMessag: msg:" + paramMessage);
    Bundle localBundle1 = paramMessage.getData();
    switch (paramMessage.what)
    {
    case 1006:
    default:
    case 1005:
    case 1001:
    case 1002:
    case 1003:
    case 1004:
    case 1008:
    case 1007:
    case 1012:
    case 1021:
    case 1022:
    case 1023:
    case 1024:
    case 1013:
    case 1014:
    case 1015:
    case 1016:
    case 1017:
    case 1018:
    case 1020:
    case 1019:
    case 1009:
    case 1010:
    case 1025:
    case 1011:
    case 1026:
    case 1027:
    }
    while (true)
    {
      super.handleMessage(paramMessage);
      return;
      if (this.a.mListener != null)
      {
        this.a.mListener.onExpandClose();
        continue;
        switch (a()[IMWebView.a(this.a).ordinal()])
        {
        case 3:
        default:
          break;
        case 2:
          if (this.a.mIsInterstitialAd)
            IMWebView.b(this.a, IMWebView.b(this.a));
          break;
        case 4:
        case 5:
          IMWebView.a(this.a, IMWebView.b(this.a));
          this.a.mIsExpandUrlValid = false;
          break;
        case 6:
          this.a.injectJavaScript("window.mraidview.fireErrorEvent(\"Current state is not expanded or default\", \"close\")");
          continue;
          this.a.hide();
          continue;
          this.a.setVisibility(4);
          this.a.setState(IMWebView.ViewState.HIDDEN);
          continue;
          this.a.injectJavaScript("window.mraidview.fireChangeEvent({ state: 'default' });");
          this.a.setVisibility(0);
          continue;
          if (IMWebView.a(this.a) == IMWebView.ViewState.EXPANDING)
          {
            IMWebView.a(this.a, localBundle1);
            continue;
            this.a.playAudioImpl(localBundle1, IMWebView.b(this.a));
            continue;
            try
            {
              this.a.playVideoImpl(localBundle1, IMWebView.b(this.a));
            }
            catch (Exception localException)
            {
            }
            if (Constants.DEBUG)
            {
              localException.printStackTrace();
              continue;
              AVPlayer localAVPlayer7 = (AVPlayer)IMWebView.c(this.a).get(localBundle1.getString("aplayerref"));
              if (localAVPlayer7 != null)
              {
                localAVPlayer7.pause();
                continue;
                AVPlayer localAVPlayer6 = (AVPlayer)IMWebView.c(this.a).get(localBundle1.getString("aplayerref"));
                if (localAVPlayer6 != null)
                {
                  localAVPlayer6.mute();
                  continue;
                  AVPlayer localAVPlayer5 = (AVPlayer)IMWebView.c(this.a).get(localBundle1.getString("aplayerref"));
                  if (localAVPlayer5 != null)
                  {
                    localAVPlayer5.unMute();
                    continue;
                    AVPlayer localAVPlayer4 = (AVPlayer)IMWebView.c(this.a).get(localBundle1.getString("aplayerref"));
                    if (localAVPlayer4 != null)
                    {
                      localAVPlayer4.setVolume(localBundle1.getInt("vol"));
                      continue;
                      ((AVPlayer)paramMessage.obj).seekPlayer(1000 * localBundle1.getInt("seekaudio"));
                      continue;
                      String str8 = localBundle1.getString("pid");
                      AVPlayer localAVPlayer3 = this.a.b(str8);
                      if (localAVPlayer3 == null);
                      for (String str9 = "window.mraidview.fireErrorEvent(\"Invalid property ID\", \"pauseVideo\")"; ; str9 = "window.mraidview.fireErrorEvent(\"Invalid player state\", \"pauseVideo\")")
                      {
                        this.a.injectJavaScript(str9);
                        break;
                        if (localAVPlayer3.getState() == Constants.playerState.PLAYING)
                          break label657;
                      }
                      label657: localAVPlayer3.pause();
                      return;
                      ((AVPlayer)paramMessage.obj).releasePlayer(false);
                      continue;
                      String str6 = localBundle1.getString("pid");
                      AVPlayer localAVPlayer2 = this.a.b(str6);
                      if (localAVPlayer2 == null);
                      for (String str7 = "window.mraidview.fireErrorEvent(\"Invalid property ID\", \"hideVideo\")"; ; str7 = "window.mraidview.fireErrorEvent(\"Invalid player state\", \"hideVideo\")")
                      {
                        this.a.injectJavaScript(str7);
                        break;
                        if (localAVPlayer2.getState() != Constants.playerState.RELEASED)
                          break label735;
                      }
                      label735: IMWebView.d(this.a).put(str6, localAVPlayer2);
                      localAVPlayer2.hide();
                      localAVPlayer2.releasePlayer(false);
                      return;
                      String str4 = localBundle1.getString("pid");
                      AVPlayer localAVPlayer1 = this.a.b(str4);
                      String str5;
                      if (localAVPlayer1 == null)
                        str5 = "window.mraidview.fireErrorEvent(\"Invalid property ID\", \"showVideo\")";
                      while (true)
                      {
                        this.a.injectJavaScript(str5);
                        break;
                        if (localAVPlayer1.getState() != Constants.playerState.RELEASED)
                        {
                          str5 = "window.mraidview.fireErrorEvent(\"Invalid player state\", \"showVideo\")";
                        }
                        else
                        {
                          if ((IMWebView.e(this.a) == null) || (IMWebView.e(this.a).getPropertyID().equalsIgnoreCase(str4)))
                            break label856;
                          str5 = "window.mraidview.fireErrorEvent(\"Show failed. There is already a video playing\", \"showVideo\")";
                        }
                      }
                      label856: IMWebView.d(this.a).remove(str4);
                      Bundle localBundle2 = new Bundle();
                      localBundle2.putString("expand_url", localAVPlayer1.getMediaURL());
                      localBundle2.putParcelable("expand_dimensions", localAVPlayer1.getPlayDimensions());
                      localBundle2.putParcelable("player_properties", localAVPlayer1.getProperties());
                      this.a.playVideoImpl(localBundle2, IMWebView.b(this.a));
                      return;
                      ((AVPlayer)paramMessage.obj).mute();
                      continue;
                      ((AVPlayer)paramMessage.obj).unMute();
                      continue;
                      ((AVPlayer)paramMessage.obj).seekPlayer(1000 * localBundle1.getInt("seek"));
                      continue;
                      ((AVPlayer)paramMessage.obj).setVolume(localBundle1.getInt("volume"));
                      continue;
                      String str1 = localBundle1.getString("message");
                      String str2 = localBundle1.getString("action");
                      String str3 = "window.mraidview.fireErrorEvent(\"" + str1 + "\", \"" + str2 + "\")";
                      this.a.injectJavaScript(str3);
                      continue;
                      IMWebView.a(this.a, false);
                      continue;
                      if (this.a.mListener != null)
                      {
                        this.a.mListener.onDismissAdScreen();
                        continue;
                        IMWebView.f(this.a);
                        continue;
                        IMWebView.g(this.a);
                        continue;
                        IMWebView.a(this.a, localBundle1.getString("expand_url"));
                      }
                    }
                  }
                }
              }
            }
          }
          break;
        }
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.ai.container.IMWebView.1
 * JD-Core Version:    0.6.2
 */