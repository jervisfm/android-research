package com.inmobi.androidsdk.ai.container;

import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import com.inmobi.androidsdk.ai.controller.util.AVPlayer;
import com.inmobi.androidsdk.impl.Constants;

class IMWebView$11
  implements View.OnKeyListener
{
  IMWebView$11(IMWebView paramIMWebView)
  {
  }

  public boolean onKey(View paramView, int paramInt, KeyEvent paramKeyEvent)
  {
    if ((4 == paramKeyEvent.getKeyCode()) && (paramKeyEvent.getAction() == 0))
    {
      if (Constants.DEBUG)
        Log.d("InMobiAndroidSDK_3.5.2", "Back Button pressed while fullscreen audio is playing ");
      IMWebView.v(this.a).releasePlayer(true);
      return true;
    }
    return false;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.ai.container.IMWebView.11
 * JD-Core Version:    0.6.2
 */