package com.inmobi.androidsdk.ai.container;

import android.util.Log;
import android.view.ViewGroup;
import com.inmobi.androidsdk.ai.controller.JSController.PlayerProperties;
import com.inmobi.androidsdk.ai.controller.util.AVPlayer;
import com.inmobi.androidsdk.ai.controller.util.AVPlayerListener;
import com.inmobi.androidsdk.impl.Constants;

class IMWebView$12
  implements AVPlayerListener
{
  IMWebView$12(IMWebView paramIMWebView, JSController.PlayerProperties paramPlayerProperties)
  {
  }

  public void onComplete(AVPlayer paramAVPlayer)
  {
    try
    {
      if (this.b.isFullScreen())
      {
        ViewGroup localViewGroup2 = (ViewGroup)paramAVPlayer.getBackGroundLayout().getParent();
        if (localViewGroup2 != null)
          localViewGroup2.removeView(paramAVPlayer.getBackGroundLayout());
      }
      else
      {
        ViewGroup localViewGroup1 = (ViewGroup)paramAVPlayer.getParent();
        if (localViewGroup1 != null)
        {
          localViewGroup1.removeView(paramAVPlayer);
          return;
        }
      }
    }
    catch (Exception localException)
    {
      if (Constants.DEBUG)
        Log.d("InMobiAndroidSDK_3.5.2", "Problem removing the audio relativelayout");
    }
  }

  public void onError(AVPlayer paramAVPlayer)
  {
    onComplete(paramAVPlayer);
  }

  public void onPrepared(AVPlayer paramAVPlayer)
  {
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.ai.container.IMWebView.12
 * JD-Core Version:    0.6.2
 */