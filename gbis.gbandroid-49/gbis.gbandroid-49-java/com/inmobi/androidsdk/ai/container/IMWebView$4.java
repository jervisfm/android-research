package com.inmobi.androidsdk.ai.container;

import android.util.Log;
import android.view.ViewGroup;
import com.inmobi.androidsdk.ai.controller.util.AVPlayer;
import com.inmobi.androidsdk.ai.controller.util.AVPlayerListener;
import com.inmobi.androidsdk.impl.Constants;

class IMWebView$4
  implements AVPlayerListener
{
  IMWebView$4(IMWebView paramIMWebView)
  {
  }

  public void onComplete(AVPlayer paramAVPlayer)
  {
    IMWebView.a(this.a, false);
    try
    {
      ViewGroup localViewGroup = (ViewGroup)paramAVPlayer.getBackGroundLayout().getParent();
      if (localViewGroup != null)
        localViewGroup.removeView(paramAVPlayer.getBackGroundLayout());
      paramAVPlayer.setBackGroundLayout(null);
    }
    catch (Exception localException)
    {
      try
      {
        while (true)
        {
          if ((IMWebView.e(this.a) != null) && (paramAVPlayer.getPropertyID().equalsIgnoreCase(IMWebView.e(this.a).getPropertyID())))
            IMWebView.a(this.a, null);
          return;
          localException = localException;
          if (Constants.DEBUG)
            Log.d("InMobiAndroidSDK_3.5.2", "Problem removing the video framelayout or relativelayout depending on video startstyle");
        }
      }
      finally
      {
      }
    }
  }

  public void onError(AVPlayer paramAVPlayer)
  {
    onComplete(paramAVPlayer);
  }

  public void onPrepared(AVPlayer paramAVPlayer)
  {
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.ai.container.IMWebView.4
 * JD-Core Version:    0.6.2
 */