package com.inmobi.androidsdk.ai.container;

import android.app.Activity;
import android.content.res.Resources;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.os.Build.VERSION;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.inmobi.androidsdk.ai.controller.JSController.Dimensions;
import com.inmobi.androidsdk.ai.controller.JSController.ExpandProperties;
import com.inmobi.androidsdk.ai.controller.util.AVPlayer;
import com.inmobi.androidsdk.impl.Constants;

class IMWebView$7
  implements SensorEventListener
{
  IMWebView$7(IMWebView paramIMWebView)
  {
  }

  public void onAccuracyChanged(Sensor paramSensor, int paramInt)
  {
  }

  public void onSensorChanged(SensorEvent paramSensorEvent)
  {
    int i = 0;
    if (Build.VERSION.SDK_INT >= 8)
    {
      IMWebView.a(this.a, IMWebView.o(this.a).getRotation());
      if ((IMWebView.p(this.a) == -5) || (IMWebView.p(this.a) == IMWebView.q(this.a)));
    }
    while (true)
    {
      int j;
      int k;
      int m;
      int n;
      RelativeLayout localRelativeLayout;
      FrameLayout localFrameLayout3;
      int i3;
      try
      {
        if (Constants.DEBUG)
          Log.d("InMobiAndroidSDK_3.5.2", "IMWebView-> SensorEventListener, It came inside the listener" + IMWebView.p(this.a));
        IMWebView.b(this.a, IMWebView.p(this.a));
        j = IMWebView.r(this.a).getResources().getDisplayMetrics().widthPixels;
        k = IMWebView.r(this.a).getResources().getDisplayMetrics().heightPixels;
        if (this.a.isTablet)
        {
          IMWebView localIMWebView = this.a;
          IMWebView.a(localIMWebView, 1 + IMWebView.p(localIMWebView));
          if (IMWebView.p(this.a) > 3)
            IMWebView.a(this.a, 0);
          if (Constants.DEBUG)
            Log.d("InMobiAndroidSDK_3.5.2", "IMWebView-> SensorEventListener, It is a tablet" + IMWebView.p(this.a));
        }
        if (!this.a.mIsInterstitialAd)
        {
          if ((IMWebView.p(this.a) != 0) && (IMWebView.p(this.a) != 2))
            continue;
          IMWebView.s(this.a).actualWidthRequested = IMWebView.s(this.a).portraitWidthRequested;
          IMWebView.s(this.a).actualHeightRequested = IMWebView.s(this.a).portraitHeightRequested;
          if (j <= k)
            break label1891;
          m = k;
          if (IMWebView.s(this.a).zeroWidthHeight)
          {
            IMWebView.s(this.a).actualWidthRequested = m;
            IMWebView.s(this.a).actualHeightRequested = j;
          }
          n = j - IMWebView.s(this.a).topStuff;
          if (IMWebView.t(this.a) != -5)
          {
            if (!this.a.mIsExpandUrlValid)
              continue;
            localFrameLayout1 = (FrameLayout)IMWebView.u(this.a).getRootView().findViewById(16908290);
            if (localFrameLayout1 == null)
              break label1882;
            FrameLayout localFrameLayout2 = (FrameLayout)localFrameLayout1.findViewById(435);
            localRelativeLayout = (RelativeLayout)localFrameLayout2.findViewById(438);
            localFrameLayout3 = localFrameLayout2;
            if ((IMWebView.p(this.a) != 0) && (IMWebView.p(this.a) != 2))
              break label1220;
            if (Constants.DEBUG)
              Log.d("InMobiAndroidSDK_3.5.2", "IMWebView-> SensorEventListener, It is the case from landscape to portrait");
            IMWebView.s(this.a).width = Math.min(m, IMWebView.s(this.a).actualWidthRequested);
            int i1 = n - IMWebView.s(this.a).y;
            IMWebView.s(this.a).height = Math.min(IMWebView.s(this.a).actualHeightRequested, i1);
            int i2 = m - (IMWebView.s(this.a).x + IMWebView.s(this.a).width);
            if (i2 >= 0)
              continue;
            i3 = i2 + IMWebView.s(this.a).x;
            if (i3 >= 0)
              break label1876;
            IMWebView.s(this.a).width = (i3 + IMWebView.s(this.a).width);
            if (localFrameLayout3 != null)
            {
              localFrameLayout3.setPadding(i, IMWebView.s(this.a).y, 0, 0);
              IMWebView.s(this.a).currentX = i;
              IMWebView.s(this.a).currentY = IMWebView.s(this.a).y;
              AVPlayer localAVPlayer1 = IMWebView.e(this.a);
              if (this.a.mIsExpandUrlValid)
              {
                IMWebView.s(IMWebView.u(this.a)).currentX = IMWebView.s(this.a).currentX;
                IMWebView.s(IMWebView.u(this.a)).currentY = IMWebView.s(this.a).currentY;
                localAVPlayer1 = IMWebView.e(IMWebView.u(this.a));
              }
              if ((localAVPlayer1 != null) && (localAVPlayer1.isInlineVideo()))
              {
                JSController.Dimensions localDimensions1 = localAVPlayer1.getPlayDimensions();
                if ((localDimensions1 != null) && (localDimensions1.x >= 0) && (localDimensions1.y >= 0))
                  ((FrameLayout)localAVPlayer1.getBackGroundLayout()).setPadding(i + localDimensions1.x, IMWebView.s(this.a).y + localDimensions1.y, 0, 0);
              }
              localRelativeLayout.setLayoutParams(new FrameLayout.LayoutParams(IMWebView.s(this.a).width, IMWebView.s(this.a).height));
              if (!this.a.mIsExpandUrlValid)
                continue;
              IMWebView.u(this.a).setLayoutParams(new RelativeLayout.LayoutParams(IMWebView.s(this.a).width, IMWebView.s(this.a).height));
              IMWebView.u(this.a).videoValidateWidth = IMWebView.s(this.a).width;
              if (Constants.DEBUG)
                Log.d("InMobiAndroidSDK_3.5.2", "Dimentions: {" + i + " ," + IMWebView.s(this.a).y + " ," + IMWebView.s(this.a).width + " ," + IMWebView.s(this.a).height + "}");
            }
          }
        }
        if (IMWebView.t(this.a) != -5)
          IMWebView.c(this.a, IMWebView.p(this.a));
        IMWebView.d(this.a, IMWebView.p(this.a));
        return;
        IMWebView.a(this.a, IMWebView.o(this.a).getOrientation());
        break;
        IMWebView.s(this.a).actualWidthRequested = IMWebView.s(this.a).portraitHeightRequested;
        IMWebView.s(this.a).actualHeightRequested = IMWebView.s(this.a).portraitWidthRequested;
        if (j >= k)
          break label1891;
        m = k;
        continue;
        FrameLayout localFrameLayout1 = (FrameLayout)this.a.getRootView().findViewById(16908290);
        continue;
        i = IMWebView.s(this.a).x;
        continue;
        this.a.setLayoutParams(new RelativeLayout.LayoutParams(IMWebView.s(this.a).width, IMWebView.s(this.a).height));
        this.a.videoValidateWidth = IMWebView.s(this.a).width;
        continue;
      }
      catch (Exception localException)
      {
        if (!Constants.DEBUG)
          continue;
        Log.w("InMobiAndroidSDK_3.5.2", "Exception while changing the container coordinates or width while orientation change", localException);
        return;
      }
      label1220: if ((IMWebView.p(this.a) == 1) || (IMWebView.p(this.a) == 3))
      {
        if (Constants.DEBUG)
          Log.d("InMobiAndroidSDK_3.5.2", "IMWebView-> SensorEventListener, It is the case from portrait to landscape");
        IMWebView.s(this.a).height = Math.min(n, IMWebView.s(this.a).actualHeightRequested);
        int i4 = m - IMWebView.s(this.a).x;
        IMWebView.s(this.a).width = Math.min(IMWebView.s(this.a).actualWidthRequested, i4);
        int i5 = n - (IMWebView.s(this.a).y + IMWebView.s(this.a).height);
        int i6;
        int i7;
        if (i5 < 0)
        {
          i6 = i5 + IMWebView.s(this.a).y;
          if (i6 >= 0)
            break label1869;
          IMWebView.s(this.a).height = (i6 + IMWebView.s(this.a).height);
          i7 = 0;
        }
        while (true)
        {
          label1399: if (localFrameLayout3 == null)
            break label1874;
          localFrameLayout3.setPadding(IMWebView.s(this.a).x, i7, 0, 0);
          IMWebView.s(this.a).currentX = IMWebView.s(this.a).x;
          IMWebView.s(this.a).currentY = i7;
          AVPlayer localAVPlayer2 = IMWebView.e(this.a);
          if (this.a.mIsExpandUrlValid)
          {
            IMWebView.s(IMWebView.u(this.a)).currentX = IMWebView.s(this.a).currentX;
            IMWebView.s(IMWebView.u(this.a)).currentY = IMWebView.s(this.a).currentY;
            localAVPlayer2 = IMWebView.e(IMWebView.u(this.a));
          }
          if ((localAVPlayer2 != null) && (localAVPlayer2.isInlineVideo()))
          {
            JSController.Dimensions localDimensions2 = localAVPlayer2.getPlayDimensions();
            if ((localDimensions2 != null) && (localDimensions2.x >= 0) && (localDimensions2.y >= 0))
              ((FrameLayout)localAVPlayer2.getBackGroundLayout()).setPadding(IMWebView.s(this.a).x + localDimensions2.x, i7 + localDimensions2.y, 0, 0);
          }
          localRelativeLayout.setLayoutParams(new FrameLayout.LayoutParams(IMWebView.s(this.a).width, IMWebView.s(this.a).height));
          if (this.a.mIsExpandUrlValid)
            IMWebView.u(this.a).setLayoutParams(new RelativeLayout.LayoutParams(IMWebView.s(this.a).width, IMWebView.s(this.a).height));
          for (IMWebView.u(this.a).videoValidateWidth = IMWebView.s(this.a).width; ; this.a.videoValidateWidth = IMWebView.s(this.a).width)
          {
            if (!Constants.DEBUG)
              break label1867;
            Log.d("InMobiAndroidSDK_3.5.2", "Dimentions: {" + IMWebView.s(this.a).x + " ," + i7 + " ," + IMWebView.s(this.a).width + " ," + IMWebView.s(this.a).height + "}");
            break;
            i7 = IMWebView.s(this.a).y;
            break label1399;
            this.a.setLayoutParams(new RelativeLayout.LayoutParams(IMWebView.s(this.a).width, IMWebView.s(this.a).height));
          }
          label1867: break;
          label1869: i7 = i6;
        }
        label1874: continue;
        label1876: i = i3;
        continue;
        label1882: localRelativeLayout = null;
        localFrameLayout3 = null;
        continue;
        label1891: m = j;
        j = k;
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.ai.container.IMWebView.7
 * JD-Core Version:    0.6.2
 */