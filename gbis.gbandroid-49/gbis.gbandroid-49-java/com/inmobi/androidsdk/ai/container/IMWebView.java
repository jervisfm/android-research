package com.inmobi.androidsdk.ai.container;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.URLUtil;
import android.webkit.WebBackForwardList;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.inmobi.androidsdk.IMBrowserActivity;
import com.inmobi.androidsdk.ai.controller.JSController.Dimensions;
import com.inmobi.androidsdk.ai.controller.JSController.ExpandProperties;
import com.inmobi.androidsdk.ai.controller.JSController.PlayerProperties;
import com.inmobi.androidsdk.ai.controller.JSUtilityController;
import com.inmobi.androidsdk.ai.controller.util.AVPlayer;
import com.inmobi.androidsdk.impl.AdUnit;
import com.inmobi.androidsdk.impl.Constants;
import com.inmobi.androidsdk.impl.Constants.playerState;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

public class IMWebView extends WebView
  implements Serializable
{
  private static final int A = 1022;
  private static final int B = 1023;
  private static final int C = 1024;
  private static final int D = 1025;
  public static final String DIMENSIONS = "expand_dimensions";
  private static final int E = 1026;
  public static final String EXPAND_PROPERTIES = "expand_properties";
  public static final String EXPAND_URL = "expand_url";
  private static final int F = 1027;
  private static final String G = "_mraid_current";
  private static final String H = "AD_PATH";
  private static final String I = "message";
  protected static final int IMWEBVIEW_INTERSTITIAL_ID = 117;
  protected static final int INT_BACKGROUND_ID = 224;
  protected static final int INT_CLOSE_BUTTON = 225;
  private static final String J = "action";
  private static final String K = "volume";
  private static final String L = "seek";
  private static final String M = "pid";
  private static final String N = "vol";
  private static final String O = "aplayerref";
  private static final String P = "seekaudio";
  protected static final int PLACEHOLDER_ID = 437;
  public static final String PLAYER_PROPERTIES = "player_properties";
  private static final int Q = 435;
  private static final String R = "inline";
  protected static final int RELATIVELAYOUT_ID = 438;
  private static final String S = "interstitial";
  private static final String T = "mraid.js";
  private static final String aA = "Cannot play audio.Ad is not in an expanded state";
  private static final String aB = "Player Error. Exceeding permissible limit for saved play instances";
  private static final String aC = "Invalid property ID";
  private static final String aD = "Invalid player state";
  private static final String aE = "Show failed. There is already a video playing";
  private static final int aF = -99999;
  private static String ae = "http://dl.dropbox.com/u/30899852/mraid/inmobi_mraid_bridge.js";
  private static String af = "http://dl.dropbox.com/u/30899852/mraid/inmobi_mraid.js";
  private static final int aw = 5;
  private static final int ax = -1;
  private static final String ay = "Request must specify a valid URL";
  private static final String az = "Cannot play video.Ad is not in an expanded state";
  private static final String c = "(function(){var c=window.mraidview={},f={},g=[],j=!1;c.fireReadyEvent=function(){var b=f.ready;if(null!=b)for(var a=0;a<b.length;a++)b[a]();return\"OK\"};c.fireStateChangeEvent=function(b){var a=f.stateChange;if(null!=a)for(var d=0;d<a.length;d++)a[d](b);return\"OK\"};c.fireViewableChangeEvent=function(b){var a=f.viewableChange;if(null!=a)for(var d=0;d<a.length;d++)a[d](b);return\"OK\"};c.fireErrorEvent=function(b,a){var d=f.error;if(null!=d)for(var c=0;c<d.length;c++)d[c](b,a);return\"OK\"};c.fireOrientationChangeEvent=function(b){var a=f.orientationChange;if(null!=a)for(var c=0;c<a.length;c++)a[c](b);return\"OK\"};c.fireMediaTrackingEvent=function(b,a){var c={};c.name=b;var e=\"inmobi_media_\"+b;\"undefined\"!=typeof a&&null!=a&&\"\"!=a&&(e=e+\"_\"+a);e=f[e];if(null!=e)for(var h=0;h<e.length;h++)e[h](c);return\"OK\"};c.fireMediaErrorEvent=function(b,a){var c={name:\"error\"};c.code=a;var e=\"inmobi_media_\"+c.name;\"undefined\"!=typeof b&&null!=b&&\"\"!=b&&(e=e+\"_\"+b);e=f[e];if(null!=e)for(var h=0;h<e.length;h++)e[h](c);return\"OK\"};c.fireMediaTimeUpdateEvent=function(b,a,c){var e={name:\"timeupdate\",target:{}};e.target.currentTime=a;e.target.duration=c;a=\"inmobi_media_\"+e.name;\"undefined\"!=typeof b&&null!=b&&\"\"!=b&&(a=a+\"_\"+b);b=f[a];if(null!=b)for(a=0;a<b.length;a++)b[a](e);return\"OK\"};c.fireMediaCloseEvent=function(b,a,c){var e={name:\"close\"};e.viaUserInteraction=a;e.target={};e.target.currentTime=c;a=\"inmobi_media_\"+e.name;\"undefined\"!=typeof b&&null!=b&&\"\"!=b&&(a=a+\"_\"+b);b=f[a];if(null!=b)for(a=0;a<b.length;a++)b[a](e);return\"OK\"};c.fireMediaVolumeChangeEvent=function(b,a,c){var e={name:\"volumechange\",target:{}};e.target.volume=a;e.target.muted=c;a=\"inmobi_media_\"+e.name;\"undefined\"!=typeof b&&null!=b&&\"\"!=b&&(a=a+\"_\"+b);b=f[a];if(null!=b)for(a=0;a<b.length;a++)b[a](e);return\"OK\"};c.showAlert=function(b){utilityController.showAlert(b)};c.zeroPad=function(b){var a=\"\";10>b&&(a+=\"0\");return a+b};c.addEventListener=function(b,a){var c=f[b];null==c&&(f[b]=[],c=f[b]);for(var e in c)if(a==e)return;c.push(a)};c.removeEventListener=function(b){try{var a=f[b];null!=a&&delete a}catch(d){c.log(d)}};c.useCustomClose=function(b){try{displayController.useCustomClose(b)}catch(a){c.showAlert(\"use CustomClose: \"+a)}};c.close=function(){try{displayController.close()}catch(b){c.showAlert(\"close: \"+b)}};c.stackCommands=function(b,a){j?g.push(b):(eval(b),a&&(j=!0))};c.executeStack=function(){for(j=!1;0<g.length;){var b=g.shift();eval(b)}};c.emptyStack=function(){for(;0<g.length;)g.shift()};c.expand=function(b){try{displayController.expand(b)}catch(a){c.showAlert(\"executeNativeExpand: \"+a+\", URL = \"+b)}};c.setExpandProperties=function(b){try{b?this.props=b:b=null,displayController.setExpandProperties(c.stringify(b))}catch(a){c.showAlert(\"executeNativesetExpandProperties: \"+a+\", props = \"+b)}};c.open=function(b){try{displayController.open(b)}catch(a){c.showAlert(\"open: \"+a)}};c.resize=function(b,a){try{displayController.resize(b,a)}catch(d){c.showAlert(\"resize: \"+d)}};c.getState=function(){try{return\"\"+displayController.getState()}catch(b){c.showAlert(\"getState: \"+b)}};c.getOrientation=function(){try{return\"\"+displayController.getOrientation()}catch(b){c.showAlert(\"getOrientation: \"+b)}};c.isViewable=function(){try{return displayController.isViewable()}catch(b){c.showAlert(\"isViewable: \"+b)}};c.log=function(b){try{utilityController.log(b)}catch(a){c.showAlert(\"log: \"+a)}};c.getPlacementType=function(){return displayController.getPlacementType()};c.asyncPing=function(b){try{utilityController.asyncPing(b)}catch(a){c.showAlert(\"asyncPing: \"+a)}};c.close=function(){try{displayController.close()}catch(b){c.showAlert(\"close: \"+b)}};c.makeCall=function(b){try{utilityController.makeCall(b)}catch(a){c.showAlert(\"makeCall: \"+a)}};c.sendMail=function(b,a,d){try{utilityController.sendMail(b,a,d)}catch(e){c.showAlert(\"sendMail: \"+e)}};c.sendSMS=function(b,a){try{utilityController.sendSMS(b,a)}catch(d){c.showAlert(\"sendSMS: \"+d)}};c.pauseAudio=function(b){try{var a=getPID(b);utilityController.pauseAudio(a)}catch(d){c.showAlert(\"pauseAudio: \"+d)}};c.muteAudio=function(b){try{var a=getPID(b);utilityController.muteAudio(a)}catch(d){c.showAlert(\"muteAudio: \"+d)}};c.unMuteAudio=function(b){try{var a=getPID(b);utilityController.unMuteAudio(a)}catch(d){c.showAlert(\"unMuteAudio: \"+d)}};c.isAudioMuted=function(b){try{var a=getPID(b);return utilityController.isAudioMuted(a)}catch(d){c.showAlert(\"isAudioMuted: \"+d)}};c.setAudioVolume=function(b,a){try{var d=getPID(b);utilityController.setAudioVolume(d,a)}catch(e){c.showAlert(\"setAudioVolume: \"+e)}};c.getAudioVolume=function(b){try{var a=getPID(b);return utilityController.getAudioVolume(a)}catch(d){c.showAlert(\"getAudioVolume: \"+d)}};c.seekAudio=function(b,a){try{var d=getPID(b);utilityController.seekAudio(d,a)}catch(e){c.showAlert(\"seekAudio: \"+e)}};c.playAudio=function(b,a){var d=!0,e=!1,h=\"normal\",f=\"normal\",g=!0,k=\"\",l=getPID(a);null!=b&&(k=b);null!=a&&(\"undefined\"!=typeof a.autoplay&&!1===a.autoplay&&(d=!1),\"undefined\"!=typeof a.loop&&!0===a.loop&&(e=!0),\"undefined\"!=typeof a.startStyle&&null!=a.startStyle&&(h=a.startStyle),\"undefined\"!=typeof a.stopStyle&&null!=a.stopStyle&&(f=a.stopStyle),\"fullscreen\"==h&&(g=!0));try{utilityController.playAudio(k,d,g,e,h,f,l)}catch(m){c.showAlert(\"playAudio: \"+m)}};c.pauseVideo=function(b){try{var a=getPID(b);utilityController.pauseVideo(a)}catch(d){c.showAlert(\"pauseVideo: \"+d)}};c.closeVideo=function(b){try{var a=getPID(b);utilityController.closeVideo(a)}catch(d){c.showAlert(\"closeVideo: \"+d)}};c.hideVideo=function(b){try{var a=getPID(b);utilityController.hideVideo(a)}catch(d){c.showAlert(\"hideVideo: \"+d)}};c.showVideo=function(b){try{var a=getPID(b);utilityController.showVideo(a)}catch(d){c.showAlert(\"showVideo: \"+d)}};c.muteVideo=function(b){try{var a=getPID(b);utilityController.muteVideo(a)}catch(d){c.showAlert(\"muteVideo: \"+d)}};c.unMuteVideo=function(b){try{var a=getPID(b);utilityController.unMuteVideo(a)}catch(d){c.showAlert(\"unMuteVideo: \"+d)}};c.seekVideo=function(b,a){try{var d=getPID(b);utilityController.seekVideo(d,a)}catch(e){c.showAlert(\"seekVideo: \"+e)}};c.isVideoMuted=function(b){try{var a=getPID(b);return utilityController.isVideoMuted(a)}catch(d){c.showAlert(\"isVideoMuted: \"+d)}};c.setVideoVolume=function(b,a){try{var d=getPID(b);utilityController.setVideoVolume(d,a)}catch(e){c.showAlert(\"setVideoVolume: \"+e)}};c.getVideoVolume=function(b){try{var a=getPID(b);return utilityController.getVideoVolume(a)}catch(d){c.showAlert(\"getVideoVolume: \"+d)}};c.playVideo=function(b,a){var d=!1,e=!0,f=!0,g=!1,j=-99999,k=-99999,l=-99999,m=-99999,n=\"normal\",o=\"exit\",p=\"\",q=getPID(a);null!=b&&(p=b);if(null!=a){\"undefined\"!=typeof a.audio&&\"muted\"==a.audio&&(d=!0);\"undefined\"!=typeof a.autoplay&&!1===a.autoplay&&(e=!1);\"undefined\"!=typeof a.controls&&!1===a.controls&&(f=!1);\"undefined\"!=typeof a.loop&&!0===a.loop&&(g=!0);if(\"undefined\"!=typeof a.inline&&null!=a.inline&&(j=a.inline.left,k=a.inline.top,\"undefined\"!=typeof a.width&&null!=a.width&&(l=a.width),\"undefined\"!=typeof a.height&&null!=a.height))m=a.height;\"undefined\"!=typeof a.startStyle&&null!=a.startStyle&&(n=a.startStyle);\"undefined\"!=typeof a.stopStyle&&null!=a.stopStyle&&(o=a.stopStyle);\"fullscreen\"==n&&(f=!0)}try{utilityController.playVideo(p,d,e,f,g,j,k,l,m,n,o,q)}catch(r){c.showAlert(\"playVideo: \"+r)}};c.stringify=function(b){if(\"undefined\"===typeof JSON){var a=\"\",d;if(\"undefined\"==typeof b.length)return c.stringifyArg(b);for(d=0;d<b.length;d++)0<d&&(a+=\",\"),a+=c.stringifyArg(b[d]);return a+\"]\"}return JSON.stringify(b)};c.stringifyArg=function(b){var a,d,e;d=typeof b;a=\"\";if(\"number\"===d||\"boolean\"===d)a+=args;else if(b instanceof Array)a=a+\"[\"+b+\"]\";else if(b instanceof Object){d=!0;a+=\"{\";for(e in b)null!==b[e]&&(d||(a+=\",\"),a=a+'\"'+e+'\":',d=typeof b[e],a=\"number\"===d||\"boolean\"===d?a+b[e]:\"function\"===typeof b[e]?a+'\"\"':b[e]instanceof Object?a+this.stringify(args[i][e]):a+'\"'+b[e]+'\"',d=!1);a+=\"}\"}else b=b.replace(/\\\\/g,\"\\\\\\\\\"),b=b.replace(/\"/g,'\\\\\"'),a=a+'\"'+b+'\"';c.showAlert(\"json:\"+a);return a};getPID=function(b){var a=\"\";null!=b&&\"undefined\"!=typeof b.id&&null!=b.id&&(a=b.id);return a}})();";
  private static final String d = "(function(){var c=window.mraid={};c.STATES={LOADING:\"loading\",DEFAULT:\"default\",RESIZED:\"resized\",EXPANDED:\"expanded\",HIDDEN:\"hidden\"};var d=c.EVENTS={READY:\"ready\",ERROR:\"error\",STATECHANGE:\"stateChange\",VIEWABLECHANGE:\"viewableChange\",ORIENTATIONCHANGE:\"orientationChange\"},i={width:0,height:0},g={width:0,height:0},f={},h={width:0,height:0,useCustomClose:!1,isModal:!0,lockOrientation:!1,orientation:\"\"},l=function(a){this.event=a;this.count=0;var b={};this.add=function(a){var c=\"\"+a;b[c]||(b[c]=a,this.count++)};this.remove=function(a){a=\"\"+a;return b[a]?(b[a]=null,delete b[a],this.count--,!0):!1};this.removeAll=function(){for(var a in b)this.remove(b[a])};this.broadcast=function(a){for(var c in b)b[c].apply({},a)};this.toString=function(){var c=[a,\":\"],d;for(d in b)c.push(\"|\",d,\"|\");return c.join(\"\")}};mraidview.addEventListener(d.READY,function(){e(d.READY)});mraidview.addEventListener(d.STATECHANGE,function(a){e(d.STATECHANGE,a)});mraidview.addEventListener(d.VIEWABLECHANGE,function(a){e(d.VIEWABLECHANGE,a)});mraidview.addEventListener(\"error\",function(a,b){e(d.ERROR,a,b)});mraidview.addEventListener(d.ORIENTATIONCHANGE,function(a){e(d.ORIENTATIONCHANGE,a)});var k=function(a){var b=function(){};b.prototype=a;return new b},e=function(){for(var a=Array(arguments.length),b=0;b<arguments.length;b++)a[b]=arguments[b];b=a.shift();try{f[b]&&f[b].broadcast(a)}catch(c){}},j=function(a){for(var b=0,c=a.length-1;b<a.length&&\" \"==a[b];)b++;for(;c>b&&\" \"==a[c];)c-=1;return a.substring(b,c+1)};c.addEventListener=function(a,b){try{!a||!b?e(d.ERROR,\"Both event and listener are required.\",\"addEventListener\"):d.ERROR==a||d.READY==a||d.STATECHANGE==a||d.VIEWABLECHANGE==a||d.ORIENTATIONCHANGE==a?(f[a]||(f[a]=new l(a)),f[a].add(b)):mraidview.addEventListener(a,b)}catch(c){mraidview.log(c)}};c.useCustomClose=function(a){h.useCustomClose=a;mraidview.useCustomClose(a)};c.close=function(){mraidview.close()};c.getExpandProperties=function(){return h};c.setExpandProperties=function(a){h=a;h.isModal=!0;mraidview.setExpandProperties(h)};c.expand=function(a){mraidview.expand(a)};c.getMaxSize=function(){return k(g)};c.getSize=function(){return k(i)};c.getState=function(){return mraidview.getState()};c.getOrientation=function(){return mraidview.getOrientation()};c.isViewable=function(){return mraidview.isViewable()};c.open=function(a){a?mraidview.open(a):e(d.ERROR,\"URL is required.\",\"open\")};c.removeEventListener=function(a,b){try{if(a){if(b)if(f[a])f[a].remove(b);else{mraidview.removeEventListener(a,b);return}else f[a]&&f[a].removeAll();f[a]&&0==f[a].count&&(f[a]=null,delete f[a])}else e(d.ERROR,\"Must specify an event.\",\"removeEventListener\")}catch(c){mraidview.log(\"removeEventListener\"+c)}};c.resize=function(a,b){null==a||null==b||isNaN(a)||isNaN(b)||0>a||0>b?e(d.ERROR,\"Requested size must be numeric values between 0 and maxSize.\",\"resize\"):a>g.width||b>g.height?e(d.ERROR,\"Request (\"+a+\" x \"+b+\") exceeds maximum allowable size of (\"+g.width+\" x \"+g.height+\")\",\"resize\"):a==i.width&&b==i.height?e(d.ERROR,\"Requested size equals current size.\",\"resize\"):mraidview.resize(a,b)};c.log=function(a){a?mraidview.log(a):e(d.ERROR,\"message is required.\",\"log\")};c.getVersion=function(){return\"1.0\"};c.getInMobiAIVersion=function(){return 1.1};c.getPlacementType=function(){return mraidview.getPlacementType()};c.asyncPing=function(a){a?mraidview.asyncPing(a):e(d.ERROR,\"URL is required.\",\"asyncPing\")};c.makeCall=function(a){!a||\"string\"!=typeof a||0==j(a).length?e(d.ERROR,\"Request must provide a number to call.\",\"makeCall\"):mraidview.makeCall(a)};c.sendMail=function(a,b,c){!a||\"string\"!=typeof a||0==j(a).length?e(d.ERROR,\"Request must specify a recipient.\",\"sendMail\"):mraidview.sendMail(a,b,c)};c.sendSMS=function(a,b){!a||\"string\"!=typeof a||0==j(a).length?e(d.ERROR,\"Request must specify a recipient.\",\"sendSMS\"):mraidview.sendSMS(a,b)};c.playAudio=function(a,b){\"undefined\"==typeof b||null==b?\"string\"==typeof a?mraidview.playAudio(a,null):\"object\"==typeof a?mraidview.playAudio(null,a):mraidview.playAudio(null,null):mraidview.playAudio(a,b)};c.playVideo=function(a,b){\"undefined\"==typeof b||null==b?\"string\"==typeof a?mraidview.playVideo(a,null):\"object\"==typeof a?mraidview.playVideo(null,a):mraidview.playVideo(null,null):mraidview.playVideo(a,b)};c.pauseAudio=function(a){mraidview.pauseAudio(a)};c.muteAudio=function(a){mraidview.muteAudio(a)};c.unMuteAudio=function(a){mraidview.unMuteAudio(a)};c.isAudioMuted=function(a){return mraidview.isAudioMuted(a)};c.setAudioVolume=function(a){var b=a.volume;\"undefined\"==typeof b||null==b?e(d.ERROR,\"Request must specify a valid volume\",\"setAudioVolume\"):0>b||100<b?e(d.ERROR,\"Request must specify a valid volume value in the range 0..100\",\"setAudioVolume\"):mraidview.setAudioVolume(a,b)};c.getAudioVolume=function(a){return mraidview.getAudioVolume(a)};c.pauseVideo=function(a){mraidview.pauseVideo(a)};c.closeVideo=function(a){mraidview.closeVideo(a)};c.hideVideo=function(a){mraidview.hideVideo(a)};c.showVideo=function(a){mraidview.showVideo(a)};c.muteVideo=function(a){mraidview.muteVideo(a)};c.unMuteVideo=function(a){mraidview.unMuteVideo(a)};c.isVideoMuted=function(a){return mraidview.isVideoMuted(a)};c.setVideoVolume=function(a){var b=a.volume;\"undefined\"==typeof b||null==b?e(d.ERROR,\"Request must specify a valid volume\",\"setVideoVolume\"):0>b||100<b?e(d.ERROR,\"Request must specify a valid volume value in the range 0..100\",\"setVideoVolume\"):mraidview.setVideoVolume(a,b)};c.getVideoVolume=function(a){return mraidview.getVideoVolume(a)};c.seekAudio=function(a){var b=a.time;\"undefined\"==typeof b||null==b?e(d.ERROR,\"Request must specify a valid time\",\"seekAudio\"):0!=b?e(d.ERROR,\"Cannot seek audio other than 0\",\"seekAudio\"):mraidview.seekAudio(a,b)};c.seekVideo=function(a){var b=a.time;\"undefined\"==typeof b||null==b?e(d.ERROR,\"Request must specify a valid time\",\"seekVideo\"):0!=b?e(d.ERROR,\"Cannot seek video other than 0\",\"seekVideo\"):mraidview.seekVideo(a,b)}})();";
  private static int[] e = { 16843039, 16843040 };
  private static final int f = 1001;
  private static final int g = 1002;
  private static final int h = 1003;
  private static final int i = 1004;
  private static final int j = 1005;
  private static final int k = 1006;
  private static final int l = 1007;
  private static final int m = 1008;
  private static final int n = 1009;
  private static final int o = 1010;
  private static final int p = 1011;
  private static final int q = 1012;
  private static final int r = 1013;
  private static final int s = 1014;
  private static final long serialVersionUID = 7098506283154473782L;
  private static final int t = 1015;
  private static final int u = 1016;
  public static boolean userInitiatedClose = false;
  private static final int v = 1017;
  private static final int w = 1018;
  private static final int x = 1019;
  private static final int y = 1020;
  private static final int z = 1021;
  private b U;
  private boolean V;
  private JSUtilityController W;
  private float X;
  private int Y;
  private int Z;
  WebViewClient a = new IMWebView.5(this);
  private ArrayList<String> aG = new ArrayList();
  private Handler aH = new IMWebView.1(this);
  private Display aI;
  private ViewGroup aJ;
  private SensorEventListener aK = new IMWebView.7(this);
  private boolean aL;
  private boolean aM = true;
  private Message aN;
  private Message aO;
  private Message aP;
  private Activity aQ;
  private Activity aR;
  private Message aS;
  private int aT = -5;
  private int aU = -5;
  private int aV = -5;
  private WebViewClient aW;
  private int aa;
  private ViewState ab = ViewState.LOADING;
  private String ac;
  private final HashSet<String> ad = new HashSet();
  private boolean ag = false;
  private boolean ah = false;
  private AdUnit ai;
  private IMWebView aj;
  private boolean ak;
  private boolean al = false;
  private SensorManager am;
  private Sensor an;
  private boolean ao = true;
  private JSController.ExpandProperties ap;
  private boolean aq = false;
  private String ar;
  private AVPlayer as;
  private AVPlayer at;
  private Hashtable<String, AVPlayer> au = new Hashtable();
  private Hashtable<String, AVPlayer> av = new Hashtable();
  WebChromeClient b = new IMWebView.6(this);
  public AtomicBoolean isMutexAquired = new AtomicBoolean(false);
  public boolean isTablet = false;
  public boolean mIsExpandUrlValid = false;
  public boolean mIsInterstitialAd = false;
  public IMWebViewListener mListener;
  public IMWebView mOriginalWebviewForExpandUrl = null;
  public Object mutex = new Object();
  public int publisherOrientation;
  public boolean useLockOrient;
  public int videoValidateWidth;

  public IMWebView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    c();
    getContext().obtainStyledAttributes(paramAttributeSet, e).recycle();
  }

  public IMWebView(Context paramContext, IMWebViewListener paramIMWebViewListener)
  {
    super(paramContext);
    this.mListener = paramIMWebViewListener;
    c();
  }

  public IMWebView(Context paramContext, IMWebViewListener paramIMWebViewListener, boolean paramBoolean1, boolean paramBoolean2)
  {
    super(paramContext);
    this.aQ = ((Activity)paramContext);
    this.mIsInterstitialAd = paramBoolean1;
    this.ak = paramBoolean2;
    if (this.mIsInterstitialAd)
      setId(117);
    this.mListener = paramIMWebViewListener;
    c();
  }

  private FrameLayout a(JSController.ExpandProperties paramExpandProperties)
  {
    FrameLayout localFrameLayout1 = (FrameLayout)getRootView().findViewById(16908290);
    replaceByPlaceholder();
    FrameLayout localFrameLayout2 = new FrameLayout(getContext());
    FrameLayout.LayoutParams localLayoutParams1 = new FrameLayout.LayoutParams(-1, -1);
    localFrameLayout2.setId(435);
    localFrameLayout2.setOnTouchListener(new IMWebView.8(this));
    localFrameLayout2.setPadding(paramExpandProperties.x, paramExpandProperties.y, 0, 0);
    FrameLayout.LayoutParams localLayoutParams2 = new FrameLayout.LayoutParams(paramExpandProperties.width, paramExpandProperties.height);
    RelativeLayout localRelativeLayout = new RelativeLayout(getContext());
    localRelativeLayout.setId(438);
    RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(paramExpandProperties.width, paramExpandProperties.height);
    if (this.mIsExpandUrlValid)
      localRelativeLayout.addView(this.aj, localLayoutParams);
    while (true)
    {
      a(localRelativeLayout, paramExpandProperties.useCustomClose);
      localFrameLayout2.addView(localRelativeLayout, localLayoutParams2);
      localFrameLayout1.addView(localFrameLayout2, localLayoutParams1);
      return localFrameLayout2;
      localRelativeLayout.addView(this, localLayoutParams);
    }
  }

  private void a(int paramInt)
  {
    String str = "window.mraidview.fireOrientationChangeEvent(" + getCurrentRotation(paramInt) + ");";
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "JSDisplayController-> " + str);
    injectJavaScript(str);
    if (this.mIsExpandUrlValid)
      this.aj.injectJavaScript(str);
  }

  private void a(Activity paramActivity)
  {
    synchronized (this.mutex)
    {
      this.isMutexAquired.set(false);
      this.mutex.notifyAll();
      if ((!this.ao) && (this.publisherOrientation == -1))
      {
        this.am.unregisterListener(this.aK);
        this.ao = true;
      }
      resetContents(paramActivity);
      releaseAllPlayers();
      this.aG.clear();
      if ((paramActivity instanceof IMBrowserActivity))
      {
        setExpandedActivity(this.aQ);
        paramActivity.finish();
      }
      this.aH.sendEmptyMessage(1005);
      setVisibility(0);
      this.mIsExpandUrlValid = false;
      if (this.useLockOrient)
        this.aQ.setRequestedOrientation(-1);
      setState(ViewState.DEFAULT);
      return;
    }
  }

  private void a(Bundle paramBundle)
  {
    String str;
    try
    {
      str = paramBundle.getString("expand_url");
      if (URLUtil.isValidUrl(str))
      {
        this.mIsExpandUrlValid = true;
        this.aj = new IMWebView(getContext(), this.mListener, false, false);
        this.aj.mIsExpandUrlValid = true;
        this.aj.publisherOrientation = this.publisherOrientation;
        this.aj.ao = this.ao;
        this.aj.ap = new JSController.ExpandProperties();
        this.aj.ap.x = this.ap.x;
        this.aj.ap.y = this.ap.y;
        this.aj.ap.currentX = this.ap.currentX;
        this.aj.ap.currentY = this.ap.currentY;
        this.aj.useLockOrient = this.useLockOrient;
        this.aj.mOriginalWebviewForExpandUrl = this;
        if (!this.ap.checkFlag)
          break label401;
        setExpandedActivity(this.aQ);
        FrameLayout localFrameLayout = a(this.ap);
        b(this.ap);
        localFrameLayout.setBackgroundColor(0);
        this.videoValidateWidth = this.ap.width;
        if (this.aj != null)
          this.aj.videoValidateWidth = this.ap.width;
        setState(ViewState.EXPANDED);
      }
    }
    catch (Exception localException)
    {
      synchronized (this.mutex)
      {
        while (true)
        {
          this.isMutexAquired.set(false);
          this.mutex.notifyAll();
          if (this.mIsExpandUrlValid)
            this.aj.loadUrl(str);
          if (this.mListener == null)
            break;
          this.mListener.onExpand();
          return;
          this.mIsExpandUrlValid = false;
        }
        localException = localException;
        if (Constants.DEBUG)
          Log.d("InMobiAndroidSDK_3.5.2", "Exception in doexpand" + localException);
        this.ab = ViewState.DEFAULT;
        synchronized (this.mutex)
        {
          this.isMutexAquired.set(false);
          this.mutex.notifyAll();
          return;
        }
      }
    }
    label401: a(this.ap, str);
  }

  private void a(ViewGroup paramViewGroup, boolean paramBoolean)
  {
    RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams((int)(50.0F * this.X), (int)(50.0F * this.X));
    localLayoutParams.addRule(11);
    ImageView localImageView = new ImageView(getContext());
    if (paramBoolean)
      localImageView.setImageBitmap(bitmapFromJar("assets/im_close_transparent.png"));
    while (true)
    {
      paramViewGroup.addView(localImageView, localLayoutParams);
      localImageView.setOnClickListener(new IMWebView.9(this));
      return;
      localImageView.setImageBitmap(bitmapFromJar("assets/im_close_button.png"));
    }
  }

  private void a(WebView paramWebView, String paramString)
  {
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "IMWebView-> Search query requested:" + paramString);
    try
    {
      paramWebView.stopLoading();
      int i1 = paramString.indexOf("?");
      if (i1 > 0)
      {
        String str1 = paramString.substring(i1);
        if (str1 != null)
        {
          String str2 = this.ai.getDefaultTargetUrl();
          this.ai.setTargetUrl(str2 + str1);
          System.out.println(str2 + str1);
          this.aS.getTarget().obtainMessage(this.aS.what).sendToTarget();
        }
      }
      return;
    }
    catch (Exception localException)
    {
    }
  }

  private void a(JSController.Dimensions paramDimensions)
  {
    paramDimensions.width = ((int)(paramDimensions.width * this.X));
    paramDimensions.height = ((int)(paramDimensions.height * this.X));
    paramDimensions.x = ((int)(paramDimensions.x * this.X));
    paramDimensions.y = ((int)(paramDimensions.y * this.X));
    int i1 = (int)(50.0F * this.X);
    int i2 = this.videoValidateWidth - (int)(50.0F * this.X);
    int i3;
    if (paramDimensions.width > 0)
    {
      int i4 = paramDimensions.height;
      i3 = 0;
      if (i4 > 0);
    }
    else
    {
      paramDimensions.width = 1;
      paramDimensions.height = 1;
      i3 = 1;
    }
    if ((i3 == 0) && (paramDimensions.x + paramDimensions.width > i2) && (paramDimensions.x < i2) && (paramDimensions.y < i1) && (paramDimensions.y + paramDimensions.height > i1))
      paramDimensions.y = i1;
  }

  private void a(JSController.ExpandProperties paramExpandProperties, String paramString)
  {
    Intent localIntent = new Intent(this.aQ, IMBrowserActivity.class);
    Bundle localBundle = new Bundle();
    localBundle.putInt("EXPAND_WIDTH", paramExpandProperties.width);
    localBundle.putInt("EXPAND_HEIGHT", paramExpandProperties.height);
    localBundle.putBoolean("EXPAND_CUSTOM_CLOSE", paramExpandProperties.useCustomClose);
    localBundle.putString("EXPAND_ORIENTATION", paramExpandProperties.orientation);
    localBundle.putInt("EXPAND_BACKGROUND_ID", 435);
    localBundle.putString("EXPAND_WITH_URL", paramString);
    localIntent.putExtras(localBundle);
    if (this.mIsExpandUrlValid)
      IMBrowserActivity.setWebView(this.aj);
    while (true)
    {
      IMBrowserActivity.setOriginalWebView(this);
      this.aQ.startActivity(localIntent);
      return;
      IMBrowserActivity.setWebView(this);
    }
  }

  private void a(AVPlayer paramAVPlayer, JSController.Dimensions paramDimensions)
  {
    int i1 = (int)(-99999.0F * this.X);
    FrameLayout localFrameLayout;
    if ((paramAVPlayer.isInlineVideo()) && (paramDimensions.x != i1) && (paramDimensions.y != i1))
    {
      paramAVPlayer.setLayoutParams(new FrameLayout.LayoutParams(paramDimensions.width, paramDimensions.height));
      localFrameLayout = (FrameLayout)paramAVPlayer.getBackGroundLayout();
      if (this.ap == null)
        localFrameLayout.setPadding(paramDimensions.x, paramDimensions.y, 0, 0);
    }
    else
    {
      return;
    }
    localFrameLayout.setPadding(paramDimensions.x + this.ap.currentX, paramDimensions.y + this.ap.currentY, 0, 0);
  }

  // ERROR //
  private void a(InputStream paramInputStream, String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial 791	com/inmobi/androidsdk/ai/container/IMWebView:b	()V
    //   4: aload_0
    //   5: getfield 793	com/inmobi/androidsdk/ai/container/IMWebView:U	Lcom/inmobi/androidsdk/ai/container/IMWebView$b;
    //   8: ifnull +11 -> 19
    //   11: aload_0
    //   12: getfield 793	com/inmobi/androidsdk/ai/container/IMWebView:U	Lcom/inmobi/androidsdk/ai/container/IMWebView$b;
    //   15: invokevirtual 798	com/inmobi/androidsdk/ai/container/IMWebView$b:cancel	()Z
    //   18: pop
    //   19: aload_0
    //   20: new 795	com/inmobi/androidsdk/ai/container/IMWebView$b
    //   23: dup
    //   24: aload_0
    //   25: invokespecial 799	com/inmobi/androidsdk/ai/container/IMWebView$b:<init>	(Lcom/inmobi/androidsdk/ai/container/IMWebView;)V
    //   28: putfield 793	com/inmobi/androidsdk/ai/container/IMWebView:U	Lcom/inmobi/androidsdk/ai/container/IMWebView$b;
    //   31: aload_0
    //   32: aload_0
    //   33: getfield 801	com/inmobi/androidsdk/ai/container/IMWebView:W	Lcom/inmobi/androidsdk/ai/controller/JSUtilityController;
    //   36: aload_1
    //   37: ldc 32
    //   39: iconst_1
    //   40: aload_2
    //   41: getstatic 258	com/inmobi/androidsdk/ai/container/IMWebView:ae	Ljava/lang/String;
    //   44: getstatic 262	com/inmobi/androidsdk/ai/container/IMWebView:af	Ljava/lang/String;
    //   47: invokevirtual 807	com/inmobi/androidsdk/ai/controller/JSUtilityController:writeToDiskWrap	(Ljava/io/InputStream;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   50: putfield 809	com/inmobi/androidsdk/ai/container/IMWebView:ac	Ljava/lang/String;
    //   53: new 457	java/lang/StringBuilder
    //   56: dup
    //   57: ldc_w 811
    //   60: invokespecial 462	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   63: aload_0
    //   64: getfield 809	com/inmobi/androidsdk/ai/container/IMWebView:ac	Ljava/lang/String;
    //   67: invokevirtual 470	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   70: getstatic 816	java/io/File:separator	Ljava/lang/String;
    //   73: invokevirtual 470	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   76: ldc 32
    //   78: invokevirtual 470	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   81: invokevirtual 476	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   84: astore 9
    //   86: new 818	java/util/Timer
    //   89: dup
    //   90: invokespecial 819	java/util/Timer:<init>	()V
    //   93: aload_0
    //   94: getfield 793	com/inmobi/androidsdk/ai/container/IMWebView:U	Lcom/inmobi/androidsdk/ai/container/IMWebView$b;
    //   97: ldc2_w 820
    //   100: ldc2_w 820
    //   103: invokevirtual 825	java/util/Timer:schedule	(Ljava/util/TimerTask;JJ)V
    //   106: aload_2
    //   107: ifnull +8 -> 115
    //   110: aload_0
    //   111: aload_2
    //   112: invokevirtual 493	com/inmobi/androidsdk/ai/container/IMWebView:injectJavaScript	(Ljava/lang/String;)V
    //   115: aload_0
    //   116: aload 9
    //   118: invokespecial 826	android/webkit/WebView:loadUrl	(Ljava/lang/String;)V
    //   121: aload_1
    //   122: ifnull +7 -> 129
    //   125: aload_1
    //   126: invokevirtual 831	java/io/InputStream:close	()V
    //   129: return
    //   130: astore 7
    //   132: aload 7
    //   134: invokevirtual 834	java/lang/IllegalStateException:printStackTrace	()V
    //   137: aload_1
    //   138: ifnull -9 -> 129
    //   141: aload_1
    //   142: invokevirtual 831	java/io/InputStream:close	()V
    //   145: return
    //   146: astore 8
    //   148: return
    //   149: astore 5
    //   151: aload 5
    //   153: invokevirtual 835	java/io/IOException:printStackTrace	()V
    //   156: aload_1
    //   157: ifnull -28 -> 129
    //   160: aload_1
    //   161: invokevirtual 831	java/io/InputStream:close	()V
    //   164: return
    //   165: astore 6
    //   167: return
    //   168: astore_3
    //   169: aload_1
    //   170: ifnull +7 -> 177
    //   173: aload_1
    //   174: invokevirtual 831	java/io/InputStream:close	()V
    //   177: aload_3
    //   178: athrow
    //   179: astore 4
    //   181: goto -4 -> 177
    //   184: astore 10
    //   186: return
    //
    // Exception table:
    //   from	to	target	type
    //   31	106	130	java/lang/IllegalStateException
    //   110	115	130	java/lang/IllegalStateException
    //   115	121	130	java/lang/IllegalStateException
    //   141	145	146	java/lang/Exception
    //   31	106	149	java/io/IOException
    //   110	115	149	java/io/IOException
    //   115	121	149	java/io/IOException
    //   160	164	165	java/lang/Exception
    //   31	106	168	finally
    //   110	115	168	finally
    //   115	121	168	finally
    //   132	137	168	finally
    //   151	156	168	finally
    //   173	177	179	java/lang/Exception
    //   125	129	184	java/lang/Exception
  }

  private boolean a(Uri paramUri)
  {
    String str = paramUri.getScheme();
    if (str == null)
      return false;
    Iterator localIterator = this.ad.iterator();
    do
      if (!localIterator.hasNext())
        return false;
    while (!((String)localIterator.next()).equalsIgnoreCase(str));
    return true;
  }

  private AVPlayer b(String paramString1, String paramString2, Activity paramActivity)
  {
    if (!this.av.isEmpty())
    {
      this.at = ((AVPlayer)this.av.get(paramString1));
      if (this.at == null)
        if (this.av.size() > 4)
        {
          raiseError("Too many audio players", "playAudio");
          this.at = null;
        }
    }
    while (true)
    {
      return this.at;
      this.at = new AVPlayer(paramActivity, this);
      continue;
      if ((this.at.getMediaURL().equals(paramString2)) || (paramString2.length() == 0))
      {
        if (this.at.getState() == Constants.playerState.PLAYING)
        {
          this.at = null;
        }
        else if (this.at.getState() == Constants.playerState.PAUSED)
        {
          this.at.start();
          this.at = null;
        }
        else
        {
          JSController.PlayerProperties localPlayerProperties = this.at.getProperties();
          String str = this.at.getMediaURL();
          this.at.releasePlayer(false);
          this.av.remove(paramString1);
          this.at = new AVPlayer(paramActivity, this);
          this.at.setPlayData(localPlayerProperties, str);
        }
      }
      else
      {
        this.at.releasePlayer(false);
        this.av.remove(paramString1);
        this.at = new AVPlayer(paramActivity, this);
        continue;
        this.at = new AVPlayer(paramActivity, this);
      }
    }
  }

  private void b()
  {
    if (this.ab == ViewState.EXPANDED)
      a(this.aR);
    invalidate();
    this.W.deleteOldAds();
    this.W.stopAllListeners();
    f();
  }

  private void b(Activity paramActivity)
  {
    try
    {
      releaseAllPlayers();
      FrameLayout localFrameLayout = (FrameLayout)getRootView().findViewById(16908290);
      RelativeLayout localRelativeLayout = (RelativeLayout)localFrameLayout.findViewById(224);
      if (localRelativeLayout != null)
      {
        localRelativeLayout.removeView(this);
        localFrameLayout.removeView(localRelativeLayout);
      }
      if ((paramActivity instanceof IMBrowserActivity))
      {
        setExpandedActivity(this.aQ);
        paramActivity.finish();
      }
      if (this.aP != null)
        this.aP.sendToTarget();
      return;
    }
    catch (Exception localException)
    {
      Log.w("InMobiAndroidSDK_3.5.2", "Exception while closing the Interstitial Ad", localException);
    }
  }

  private void b(JSController.ExpandProperties paramExpandProperties)
  {
    try
    {
      if ((!paramExpandProperties.lockOrientation) && (this.publisherOrientation == -1))
      {
        this.aT = -5;
        this.aU = -5;
        this.aV = -5;
        this.am.registerListener(this.aK, this.an, 3);
      }
      return;
    }
    catch (Exception localException)
    {
      while (!Constants.DEBUG);
      Log.d("InMobiAndroidSDK_3.5.2", "Exception handling the orientation", localException);
    }
  }

  // ERROR //
  public static android.graphics.Bitmap bitmapFromJar(String paramString)
  {
    // Byte code:
    //   0: ldc_w 995
    //   3: invokevirtual 1001	java/lang/Class:getClassLoader	()Ljava/lang/ClassLoader;
    //   6: aload_0
    //   7: invokevirtual 1007	java/lang/ClassLoader:getResource	(Ljava/lang/String;)Ljava/net/URL;
    //   10: invokevirtual 1012	java/net/URL:getFile	()Ljava/lang/String;
    //   13: astore 9
    //   15: aload 9
    //   17: ldc_w 1014
    //   20: invokevirtual 1017	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   23: ifeq +11 -> 34
    //   26: aload 9
    //   28: iconst_5
    //   29: invokevirtual 655	java/lang/String:substring	(I)Ljava/lang/String;
    //   32: astore 9
    //   34: aload 9
    //   36: ldc_w 1019
    //   39: invokevirtual 652	java/lang/String:indexOf	(Ljava/lang/String;)I
    //   42: istore 10
    //   44: iload 10
    //   46: ifle +13 -> 59
    //   49: aload 9
    //   51: iconst_0
    //   52: iload 10
    //   54: invokevirtual 1022	java/lang/String:substring	(II)Ljava/lang/String;
    //   57: astore 9
    //   59: new 1024	java/util/jar/JarFile
    //   62: dup
    //   63: aload 9
    //   65: invokespecial 1025	java/util/jar/JarFile:<init>	(Ljava/lang/String;)V
    //   68: astore 11
    //   70: aload 11
    //   72: aload 11
    //   74: aload_0
    //   75: invokevirtual 1029	java/util/jar/JarFile:getJarEntry	(Ljava/lang/String;)Ljava/util/jar/JarEntry;
    //   78: invokevirtual 1033	java/util/jar/JarFile:getInputStream	(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;
    //   81: astore 12
    //   83: aload 12
    //   85: astore_2
    //   86: aload_2
    //   87: invokestatic 1039	android/graphics/BitmapFactory:decodeStream	(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    //   90: astore 13
    //   92: aload 13
    //   94: astore 6
    //   96: aload_2
    //   97: ifnull +7 -> 104
    //   100: aload_2
    //   101: invokevirtual 831	java/io/InputStream:close	()V
    //   104: aload 6
    //   106: areturn
    //   107: astore 5
    //   109: aconst_null
    //   110: astore_2
    //   111: getstatic 481	com/inmobi/androidsdk/impl/Constants:DEBUG	Z
    //   114: ifeq +15 -> 129
    //   117: ldc_w 483
    //   120: ldc_w 1041
    //   123: aload 5
    //   125: invokestatic 993	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   128: pop
    //   129: aconst_null
    //   130: astore 6
    //   132: aload_2
    //   133: ifnull -29 -> 104
    //   136: aload_2
    //   137: invokevirtual 831	java/io/InputStream:close	()V
    //   140: aconst_null
    //   141: areturn
    //   142: astore 7
    //   144: aconst_null
    //   145: areturn
    //   146: astore_1
    //   147: aconst_null
    //   148: astore_2
    //   149: aload_1
    //   150: astore_3
    //   151: aload_2
    //   152: ifnull +7 -> 159
    //   155: aload_2
    //   156: invokevirtual 831	java/io/InputStream:close	()V
    //   159: aload_3
    //   160: athrow
    //   161: astore 14
    //   163: aload 6
    //   165: areturn
    //   166: astore 4
    //   168: goto -9 -> 159
    //   171: astore_3
    //   172: goto -21 -> 151
    //   175: astore 5
    //   177: goto -66 -> 111
    //
    // Exception table:
    //   from	to	target	type
    //   0	34	107	java/lang/Exception
    //   34	44	107	java/lang/Exception
    //   49	59	107	java/lang/Exception
    //   59	83	107	java/lang/Exception
    //   136	140	142	java/lang/Exception
    //   0	34	146	finally
    //   34	44	146	finally
    //   49	59	146	finally
    //   59	83	146	finally
    //   100	104	161	java/lang/Exception
    //   155	159	166	java/lang/Exception
    //   86	92	171	finally
    //   111	129	171	finally
    //   86	92	175	java/lang/Exception
  }

  private String c(String paramString)
  {
    Object localObject;
    try
    {
      DefaultHttpClient localDefaultHttpClient = new DefaultHttpClient();
      BasicHttpContext localBasicHttpContext = new BasicHttpContext();
      if (localDefaultHttpClient.execute(new HttpGet(paramString), localBasicHttpContext).getStatusLine().getStatusCode() != 200)
        break label147;
      HttpUriRequest localHttpUriRequest = (HttpUriRequest)localBasicHttpContext.getAttribute("http.request");
      HttpHost localHttpHost = (HttpHost)localBasicHttpContext.getAttribute("http.target_host");
      if (localHttpUriRequest.getURI().isAbsolute())
      {
        localObject = localHttpUriRequest.getURI().toString();
      }
      else
      {
        String str = localHttpHost.toURI() + localHttpUriRequest.getURI();
        localObject = str;
      }
    }
    catch (Exception localException)
    {
      return paramString;
    }
    paramString = (String)localObject;
    label147: return paramString;
  }

  private void c()
  {
    this.aR = this.aQ;
    userInitiatedClose = false;
    setScrollContainer(false);
    setVerticalScrollBarEnabled(false);
    setHorizontalScrollBarEnabled(false);
    setBackgroundColor(0);
    DisplayMetrics localDisplayMetrics = new DisplayMetrics();
    ((WindowManager)getContext().getSystemService("window")).getDefaultDisplay().getMetrics(localDisplayMetrics);
    this.X = this.aQ.getResources().getDisplayMetrics().density;
    this.V = false;
    getSettings().setJavaScriptEnabled(true);
    this.W = new JSUtilityController(this, getContext());
    addJavascriptInterface(this.W, "utilityController");
    setWebViewClient(this.a);
    setWebChromeClient(this.b);
    this.am = ((SensorManager)this.aQ.getSystemService("sensor"));
    this.an = this.am.getDefaultSensor(3);
    this.aI = ((WindowManager)this.aQ.getSystemService("window")).getDefaultDisplay();
    this.videoValidateWidth = this.aQ.getResources().getDisplayMetrics().widthPixels;
  }

  private void d()
  {
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "IMWebView-> initStates");
    this.ab = ViewState.LOADING;
    this.aM = false;
  }

  private void d(String paramString)
  {
    if ((paramString.startsWith("https://")) || (paramString.startsWith("market://")) || (paramString.contains("play.google.com")) || (paramString.contains("market.android.com")))
    {
      String str = c(paramString);
      if (str != null)
      {
        Intent localIntent1 = new Intent();
        localIntent1.setAction("android.intent.action.VIEW");
        localIntent1.setData(Uri.parse(str));
        localIntent1.addFlags(268435456);
        this.aR.startActivity(localIntent1);
        if (this.mListener != null)
          this.mListener.onLeaveApplication();
      }
      return;
    }
    Intent localIntent2 = new Intent(this.aR, IMBrowserActivity.class);
    Log.d("InMobiAndroidSDK_3.5.2", "IMWebView-> open:" + paramString);
    localIntent2.putExtra("extra_url", paramString);
    IMBrowserActivity.setWebViewListener(this.mListener);
    this.aR.startActivity(localIntent2);
  }

  private AVPlayer e(String paramString)
  {
    try
    {
      AVPlayer localAVPlayer;
      if ((this.at != null) && (this.at.getPropertyID().equalsIgnoreCase(paramString)))
        localAVPlayer = this.at;
      while (true)
      {
        return localAVPlayer;
        boolean bool1 = this.av.isEmpty();
        localAVPlayer = null;
        if (!bool1)
        {
          boolean bool2 = this.av.containsKey(paramString);
          localAVPlayer = null;
          if (bool2)
            localAVPlayer = (AVPlayer)this.av.get(paramString);
        }
      }
    }
    finally
    {
    }
  }

  private void e()
  {
    ImageView localImageView = (ImageView)this.aR.findViewById(225);
    if (localImageView != null)
    {
      if (this.al)
        localImageView.setImageBitmap(bitmapFromJar("assets/im_close_transparent.png"));
    }
    else
      return;
    localImageView.setImageBitmap(bitmapFromJar("assets/im_close_button.png"));
  }

  private void f()
  {
    ViewGroup.LayoutParams localLayoutParams = getLayoutParams();
    if (this.aL)
    {
      localLayoutParams.height = this.Y;
      localLayoutParams.width = this.Z;
    }
    setVisibility(0);
    requestLayout();
  }

  private void g()
  {
    getIntegerCurrentRotation();
    int i1 = this.aQ.getRequestedOrientation();
    if ((!this.aq) && (i1 == -1))
    {
      this.aT = -5;
      this.aU = -5;
      this.aV = -5;
      this.am.registerListener(this.aK, this.an, 3);
    }
  }

  private void h()
  {
    int i1 = getIntegerCurrentRotation();
    if (this.aq)
    {
      if (!this.ar.equals("portrait"))
        break label57;
      if ((i1 != 2) || (Build.VERSION.SDK_INT < 9))
        break label48;
      this.aQ.setRequestedOrientation(9);
    }
    label48: label57: 
    while (!this.ar.equals("landscape"))
    {
      return;
      this.aQ.setRequestedOrientation(1);
      return;
    }
    if ((i1 == 3) && (Build.VERSION.SDK_INT >= 9))
    {
      this.aQ.setRequestedOrientation(8);
      return;
    }
    this.aQ.setRequestedOrientation(0);
  }

  private void i()
  {
    if ((this.as != null) && (this.as.getState() != Constants.playerState.RELEASED))
    {
      this.au.put(this.as.getPropertyID(), this.as);
      this.as.hide();
      this.as.releasePlayer(false);
    }
    Iterator localIterator = this.av.entrySet().iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      AVPlayer localAVPlayer = (AVPlayer)localEntry.getValue();
      switch (a()[localAVPlayer.getState().ordinal()])
      {
      default:
        break;
      case 1:
        localAVPlayer.releasePlayer(false);
        this.av.remove(localEntry.getKey());
        break;
      case 2:
        localAVPlayer.pause();
      }
    }
  }

  AVPlayer a(String paramString)
  {
    if (!this.au.isEmpty())
      return (AVPlayer)this.au.get(paramString);
    return null;
  }

  boolean a(String paramString1, String paramString2, Activity paramActivity)
  {
    if (((paramString2.length() != 0) && (!URLUtil.isValidUrl(paramString2))) || ((paramString2.length() == 0) && (!this.au.containsKey(paramString1))))
    {
      raiseError("Request must specify a valid URL", "playVideo");
      return false;
    }
    if (this.as != null)
      this.as.releasePlayer(false);
    AVPlayer localAVPlayer = a(paramString1);
    if (localAVPlayer == null);
    for (this.as = new AVPlayer(paramActivity, this); ; this.as = localAVPlayer)
    {
      if (paramString2.length() == 0)
      {
        this.as.setPlayData(localAVPlayer.getProperties(), localAVPlayer.getMediaURL());
        this.as.setPlayDimensions(localAVPlayer.getPlayDimensions());
      }
      this.au.remove(paramString1);
      return true;
    }
  }

  boolean a(String paramString1, String paramString2, Activity paramActivity, JSController.Dimensions paramDimensions)
  {
    boolean bool1;
    if ((this.as == null) || (!paramString1.equalsIgnoreCase(this.as.getPropertyID())))
      bool1 = a(paramString1, paramString2, paramActivity);
    boolean bool2;
    do
    {
      return bool1;
      Constants.playerState localplayerState = this.as.getState();
      if (!paramString1.equalsIgnoreCase(this.as.getPropertyID()))
        break label215;
      String str = this.as.getMediaURL();
      if ((paramString2.length() != 0) && (!paramString2.equalsIgnoreCase(str)))
        break;
      switch (a()[localplayerState.ordinal()])
      {
      case 4:
      case 5:
      default:
        return false;
      case 2:
        a(this.as, paramDimensions);
        return false;
      case 3:
        this.as.start();
        return false;
      case 6:
      }
      bool2 = this.as.getProperties().doLoop();
      bool1 = false;
    }
    while (bool2);
    this.as.start();
    return false;
    if (!URLUtil.isValidUrl(paramString2))
    {
      raiseError("Request must specify a valid URL", "playVideo");
      return false;
    }
    this.as.releasePlayer(false);
    this.as = new AVPlayer(paramActivity, this);
    label215: return true;
  }

  public boolean adCreativeLocksOrientation(JSController.ExpandProperties paramExpandProperties, int paramInt)
  {
    if ((paramExpandProperties.orientation.equals("portrait")) && (paramInt == 0))
    {
      this.aQ.setRequestedOrientation(1);
      this.useLockOrient = true;
      return true;
    }
    if ((paramExpandProperties.orientation.equals("landscape")) && (paramInt == 1))
    {
      this.aQ.setRequestedOrientation(0);
      this.useLockOrient = true;
      return true;
    }
    return false;
  }

  public void addJavascriptObject(Object paramObject, String paramString)
  {
    addJavascriptInterface(paramObject, paramString);
  }

  AVPlayer b(String paramString)
  {
    try
    {
      AVPlayer localAVPlayer;
      if ((this.as != null) && (this.as.getPropertyID().equalsIgnoreCase(paramString)))
        localAVPlayer = this.as;
      while (true)
      {
        return localAVPlayer;
        boolean bool1 = this.au.isEmpty();
        localAVPlayer = null;
        if (!bool1)
        {
          boolean bool2 = this.au.containsKey(paramString);
          localAVPlayer = null;
          if (bool2)
            localAVPlayer = (AVPlayer)this.au.get(paramString);
        }
      }
    }
    finally
    {
    }
  }

  public void changeContentAreaForInterstitials()
  {
    try
    {
      g();
      Intent localIntent = new Intent(this.aQ, IMBrowserActivity.class);
      Bundle localBundle = new Bundle();
      localBundle.putBoolean("INTERSTITIAL_CUSTOM_CLOSE", getCustomClose());
      if (this.aq)
        localBundle.putString("INTERSTITIAL_ORIENTATION", this.ar);
      while (true)
      {
        localBundle.putInt("INTERSTTIAL_BACKGROUND_ID", 224);
        localBundle.putInt("INTERSTITIAL_CLOSE_ID", 225);
        localIntent.putExtras(localBundle);
        IMBrowserActivity.setIntWebView(this);
        this.aQ.startActivity(localIntent);
        return;
        localBundle.putString("INTERSTITIAL_ORIENTATION", "notlock");
      }
    }
    catch (Exception localException)
    {
      if (Constants.DEBUG)
      {
        Log.d("InMobiAndroidSDK_3.5.2", "changeContentAreaForInterstitial " + localException);
        localException.printStackTrace();
      }
    }
  }

  public void clearView()
  {
    b();
    super.clearView();
  }

  public void close()
  {
    this.aH.sendEmptyMessage(1001);
  }

  protected void closeOpened(View paramView)
  {
    ((ViewGroup)((Activity)getContext()).getWindow().getDecorView()).removeView(paramView);
    requestLayout();
  }

  public void closeVideo(String paramString)
  {
    AVPlayer localAVPlayer = b(paramString);
    if (localAVPlayer == null)
    {
      raiseError("Invalid property ID", "closeVideo");
      return;
    }
    if (localAVPlayer.getState() == Constants.playerState.RELEASED)
    {
      raiseError("Invalid player state", "closeVideo");
      return;
    }
    this.au.remove(paramString);
    Message localMessage = this.aH.obtainMessage(1014);
    localMessage.obj = localAVPlayer;
    this.aH.sendMessage(localMessage);
  }

  public void deinit()
  {
    if ((this.ab == ViewState.EXPANDED) || (this.ab == ViewState.EXPANDING))
    {
      this.am.unregisterListener(this.aK);
      close();
    }
  }

  public void deregisterProtocol(String paramString)
  {
    if (paramString != null)
      this.ad.remove(paramString.toLowerCase());
  }

  public void doHidePlayers()
  {
    this.aH.sendEmptyMessage(1026);
  }

  public void expand(String paramString, JSController.ExpandProperties paramExpandProperties)
  {
    setState(ViewState.EXPANDING);
    this.mIsExpandUrlValid = false;
    this.isMutexAquired.set(true);
    Message localMessage = this.aH.obtainMessage(1004);
    Bundle localBundle = new Bundle();
    localBundle.putString("expand_url", paramString);
    localMessage.setData(localBundle);
    this.ap = paramExpandProperties;
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "Dimentions: {" + this.ap.x + " ," + this.ap.y + " ," + this.ap.width + " ," + this.ap.height + "}");
    this.ao = this.ap.lockOrientation;
    this.aH.sendMessage(localMessage);
  }

  public void fireOnLeaveApplication()
  {
    doHidePlayers();
    if (this.mListener != null)
      this.mListener.onLeaveApplication();
  }

  public AdUnit getAdUnit()
  {
    return this.ai;
  }

  public int getAudioVolume(String paramString)
  {
    AVPlayer localAVPlayer = e(paramString);
    if (localAVPlayer == null)
    {
      raiseError("Invalid property ID", "getAudioVolume");
      return -1;
    }
    return localAVPlayer.getVolume();
  }

  public ConnectivityManager getConnectivityManager()
  {
    return (ConnectivityManager)getContext().getSystemService("connectivity");
  }

  public String getCurrentRotation(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return "-1";
    case 0:
      return "0";
    case 1:
      return "90";
    case 2:
      return "180";
    case 3:
    }
    return "270";
  }

  public boolean getCustomClose()
  {
    return this.al;
  }

  public Activity getExpandedActivity()
  {
    return this.aR;
  }

  public int getIntegerCurrentRotation()
  {
    Display localDisplay = ((WindowManager)this.aR.getSystemService("window")).getDefaultDisplay();
    if (Build.VERSION.SDK_INT >= 8);
    for (int i1 = localDisplay.getRotation(); ; i1 = localDisplay.getOrientation())
    {
      if (getWhetherTablet(i1, this.aR.getResources().getDisplayMetrics().widthPixels, this.aR.getResources().getDisplayMetrics().heightPixels))
      {
        i1++;
        if (i1 > 3)
          i1 = 0;
        this.isTablet = true;
      }
      return i1;
    }
  }

  public String getPlacementType()
  {
    if (this.mIsInterstitialAd)
      return "interstitial";
    return "inline";
  }

  public String getSize()
  {
    return "{ width: " + (int)(getWidth() / this.X) + ", height: " + (int)(getHeight() / this.X) + "}";
  }

  public String getState()
  {
    return this.ab.toString().toLowerCase();
  }

  public ViewState getStateVariable()
  {
    return this.ab;
  }

  // ERROR //
  protected String getStringFromJarFile(String paramString)
  {
    // Byte code:
    //   0: ldc_w 995
    //   3: invokevirtual 1001	java/lang/Class:getClassLoader	()Ljava/lang/ClassLoader;
    //   6: aload_1
    //   7: invokevirtual 1007	java/lang/ClassLoader:getResource	(Ljava/lang/String;)Ljava/net/URL;
    //   10: invokevirtual 1012	java/net/URL:getFile	()Ljava/lang/String;
    //   13: astore 10
    //   15: aload 10
    //   17: ldc_w 1014
    //   20: invokevirtual 1017	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   23: ifeq +11 -> 34
    //   26: aload 10
    //   28: iconst_5
    //   29: invokevirtual 655	java/lang/String:substring	(I)Ljava/lang/String;
    //   32: astore 10
    //   34: aload 10
    //   36: ldc_w 1019
    //   39: invokevirtual 652	java/lang/String:indexOf	(Ljava/lang/String;)I
    //   42: istore 11
    //   44: iload 11
    //   46: ifle +13 -> 59
    //   49: aload 10
    //   51: iconst_0
    //   52: iload 11
    //   54: invokevirtual 1022	java/lang/String:substring	(II)Ljava/lang/String;
    //   57: astore 10
    //   59: new 1024	java/util/jar/JarFile
    //   62: dup
    //   63: aload 10
    //   65: invokespecial 1025	java/util/jar/JarFile:<init>	(Ljava/lang/String;)V
    //   68: astore 12
    //   70: aload 12
    //   72: aload 12
    //   74: aload_1
    //   75: invokevirtual 1029	java/util/jar/JarFile:getJarEntry	(Ljava/lang/String;)Ljava/util/jar/JarEntry;
    //   78: invokevirtual 1033	java/util/jar/JarFile:getInputStream	(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;
    //   81: astore 13
    //   83: aload 13
    //   85: astore_3
    //   86: new 1477	java/io/BufferedReader
    //   89: dup
    //   90: new 1479	java/io/InputStreamReader
    //   93: dup
    //   94: aload_3
    //   95: invokespecial 1482	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
    //   98: invokespecial 1485	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   101: astore 14
    //   103: new 457	java/lang/StringBuilder
    //   106: dup
    //   107: invokespecial 1486	java/lang/StringBuilder:<init>	()V
    //   110: astore 15
    //   112: aload 14
    //   114: invokevirtual 1489	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   117: astore 16
    //   119: aload 16
    //   121: ifnonnull +25 -> 146
    //   124: aload 15
    //   126: invokevirtual 476	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   129: astore 17
    //   131: aload 17
    //   133: astore 6
    //   135: aload_3
    //   136: ifnull +7 -> 143
    //   139: aload_3
    //   140: invokevirtual 831	java/io/InputStream:close	()V
    //   143: aload 6
    //   145: areturn
    //   146: aload 15
    //   148: aload 16
    //   150: invokevirtual 470	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   153: pop
    //   154: aload 15
    //   156: ldc_w 1491
    //   159: invokevirtual 470	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   162: pop
    //   163: goto -51 -> 112
    //   166: astore_2
    //   167: getstatic 481	com/inmobi/androidsdk/impl/Constants:DEBUG	Z
    //   170: ifeq +14 -> 184
    //   173: ldc_w 483
    //   176: ldc_w 1493
    //   179: aload_2
    //   180: invokestatic 993	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   183: pop
    //   184: aconst_null
    //   185: astore 6
    //   187: aload_3
    //   188: ifnull -45 -> 143
    //   191: aload_3
    //   192: invokevirtual 831	java/io/InputStream:close	()V
    //   195: aconst_null
    //   196: areturn
    //   197: astore 7
    //   199: aconst_null
    //   200: areturn
    //   201: astore 9
    //   203: aconst_null
    //   204: astore_3
    //   205: aload 9
    //   207: astore 4
    //   209: aload_3
    //   210: ifnull +7 -> 217
    //   213: aload_3
    //   214: invokevirtual 831	java/io/InputStream:close	()V
    //   217: aload 4
    //   219: athrow
    //   220: astore 18
    //   222: aload 6
    //   224: areturn
    //   225: astore 5
    //   227: goto -10 -> 217
    //   230: astore 4
    //   232: goto -23 -> 209
    //   235: astore_2
    //   236: aconst_null
    //   237: astore_3
    //   238: goto -71 -> 167
    //
    // Exception table:
    //   from	to	target	type
    //   86	112	166	java/lang/Exception
    //   112	119	166	java/lang/Exception
    //   124	131	166	java/lang/Exception
    //   146	163	166	java/lang/Exception
    //   191	195	197	java/lang/Exception
    //   0	34	201	finally
    //   34	44	201	finally
    //   49	59	201	finally
    //   59	83	201	finally
    //   139	143	220	java/lang/Exception
    //   213	217	225	java/lang/Exception
    //   86	112	230	finally
    //   112	119	230	finally
    //   124	131	230	finally
    //   146	163	230	finally
    //   167	184	230	finally
    //   0	34	235	java/lang/Exception
    //   34	44	235	java/lang/Exception
    //   49	59	235	java/lang/Exception
    //   59	83	235	java/lang/Exception
  }

  public int getVideoVolume(String paramString)
  {
    AVPlayer localAVPlayer = b(paramString);
    if (localAVPlayer == null)
    {
      raiseError("Invalid property ID", "getVideoVolume");
      return -1;
    }
    return localAVPlayer.getVolume();
  }

  public boolean getWhetherTablet(int paramInt1, int paramInt2, int paramInt3)
  {
    if ((paramInt2 > paramInt3) && ((paramInt1 == 0) || (paramInt1 == 2)));
    while ((paramInt2 < paramInt3) && ((paramInt1 == 1) || (paramInt1 == 3)))
      return true;
    return false;
  }

  public void hide()
  {
    this.aH.sendEmptyMessage(1002);
  }

  public void hideVideo(String paramString)
  {
    Message localMessage = this.aH.obtainMessage(1015);
    Bundle localBundle = new Bundle();
    localBundle.putString("pid", paramString);
    localMessage.setData(localBundle);
    this.aH.sendMessage(localMessage);
  }

  public void injectJavaScript(String paramString)
  {
    if ((paramString != null) && (this.ag))
    {
      if ((Constants.DEBUG) && (paramString.length() < 400))
        Log.d("InMobiAndroidSDK_3.5.2", "Injecting JavaScript: " + paramString);
      super.loadUrl("javascript:" + paramString);
    }
  }

  public boolean isAudioMuted(String paramString)
  {
    AVPlayer localAVPlayer = e(paramString);
    if (localAVPlayer == null)
    {
      raiseError("Invalid property ID", "isAudioMuted");
      return false;
    }
    return localAVPlayer.isMediaMuted();
  }

  public boolean isBusy()
  {
    return this.ah;
  }

  public boolean isExpanded()
  {
    return this.ab == ViewState.EXPANDED;
  }

  public boolean isPageFinished()
  {
    return this.V;
  }

  public boolean isVideoMuted(String paramString)
  {
    AVPlayer localAVPlayer = b(paramString);
    if (localAVPlayer == null)
    {
      raiseError("Invalid property ID", "isVideoMuted");
      return false;
    }
    return localAVPlayer.isMediaMuted();
  }

  public boolean isViewable()
  {
    return this.aM;
  }

  public void loadData(String paramString1, String paramString2, String paramString3)
  {
    super.loadData(paramString1, paramString2, paramString3);
  }

  public void loadDataWithBaseURL(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    d();
    super.loadDataWithBaseURL(paramString1, paramString2, paramString3, paramString4, paramString5);
  }

  public void loadFile(File paramFile, String paramString)
  {
    try
    {
      a(new FileInputStream(paramFile), paramString);
      return;
    }
    catch (Exception localException)
    {
      while (!Constants.DEBUG);
      Log.d("InMobiAndroidSDK_3.5.2", "Exception loading file", localException);
    }
  }

  public void loadUrl(String paramString)
  {
    d();
    super.loadUrl(paramString);
  }

  public void loadUrl(String paramString1, String paramString2)
  {
    loadUrl(paramString1, false, paramString2);
  }

  public void loadUrl(String paramString1, boolean paramBoolean, String paramString2)
  {
    if (URLUtil.isValidUrl(paramString1))
    {
      if (!paramBoolean)
      {
        this.V = false;
        try
        {
          URL localURL = new URL(paramString1);
          localURL.getFile();
          String str;
          if (paramString1.startsWith("file:///android_asset/"))
            str = paramString1.replace("file:///android_asset/", "");
          InputStream localInputStream;
          for (Object localObject = getContext().getAssets().open(str); ; localObject = localInputStream)
          {
            a((InputStream)localObject, paramString2);
            return;
            localInputStream = localURL.openStream();
          }
        }
        catch (IOException localIOException)
        {
          localIOException.printStackTrace();
          return;
        }
        catch (MalformedURLException localMalformedURLException)
        {
        }
      }
      super.loadUrl(paramString1);
    }
  }

  public void muteAudio(String paramString)
  {
    AVPlayer localAVPlayer = e(paramString);
    if (localAVPlayer == null)
    {
      raiseError("Invalid property ID", "muteAudio");
      return;
    }
    if (localAVPlayer.getState() == Constants.playerState.RELEASED)
    {
      raiseError("Invalid player state", "muteAudio");
      return;
    }
    Message localMessage = this.aH.obtainMessage(1021);
    Bundle localBundle = new Bundle();
    localBundle.putString("aplayerref", paramString);
    localMessage.setData(localBundle);
    localMessage.sendToTarget();
  }

  public void muteVideo(String paramString)
  {
    AVPlayer localAVPlayer = b(paramString);
    if (localAVPlayer == null)
    {
      raiseError("Invalid property ID", "muteVideo");
      return;
    }
    if ((localAVPlayer.getState() == Constants.playerState.RELEASED) || (localAVPlayer.getState() == Constants.playerState.INIT))
    {
      raiseError("Invalid player state", "muteVideo");
      return;
    }
    Message localMessage = this.aH.obtainMessage(1017);
    localMessage.obj = localAVPlayer;
    this.aH.sendMessage(localMessage);
  }

  protected void onAttachedToWindow()
  {
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "IMWebView->onAttachedToWindow");
    if (!this.aL)
    {
      ViewGroup.LayoutParams localLayoutParams = getLayoutParams();
      this.Y = localLayoutParams.height;
      this.Z = localLayoutParams.width;
      this.aL = true;
    }
    super.onAttachedToWindow();
  }

  protected void onDetachedFromWindow()
  {
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "IMWebView->onDetachedFromWindow");
    this.W.stopAllListeners();
    this.aG.clear();
    if ((this.mIsInterstitialAd) && (!this.ak));
    try
    {
      this.am.unregisterListener(this.aK);
      label55: super.onDetachedFromWindow();
      return;
    }
    catch (Exception localException)
    {
      break label55;
    }
  }

  public void openURL(String paramString)
  {
    doHidePlayers();
    Message localMessage = this.aH.obtainMessage(1027);
    Bundle localBundle = new Bundle();
    localBundle.putString("expand_url", paramString);
    localMessage.setData(localBundle);
    this.aH.sendMessage(localMessage);
  }

  public void pageFinishedCallbackForAdCreativeTesting(Message paramMessage)
  {
    this.aO = paramMessage;
  }

  public void pauseAudio(String paramString)
  {
    AVPlayer localAVPlayer = e(paramString);
    if (localAVPlayer == null)
      raiseError("Invalid property ID", "pauseAudio");
    do
    {
      return;
      if (localAVPlayer.getState() != Constants.playerState.PLAYING)
      {
        raiseError("Invalid player state", "pauseAudio");
        return;
      }
    }
    while (!localAVPlayer.isPlaying());
    Message localMessage = this.aH.obtainMessage(1012);
    Bundle localBundle = new Bundle();
    localBundle.putString("aplayerref", paramString);
    localMessage.setData(localBundle);
    localMessage.sendToTarget();
  }

  public void pauseVideo(String paramString)
  {
    Message localMessage = this.aH.obtainMessage(1013);
    Bundle localBundle = new Bundle();
    localBundle.putString("pid", paramString);
    localMessage.setData(localBundle);
    this.aH.sendMessage(localMessage);
  }

  public void playAudio(String paramString1, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, String paramString2, String paramString3, String paramString4)
  {
    synchronized (this.mutex)
    {
      boolean bool = this.isMutexAquired.get();
      if (bool);
      try
      {
        this.mutex.wait();
        if ((!this.mIsInterstitialAd) && (this.ab != ViewState.EXPANDED))
        {
          raiseError("Cannot play audio.Ad is not in an expanded state", "playAudio");
          return;
        }
      }
      catch (InterruptedException localInterruptedException)
      {
        while (true)
          if (Constants.DEBUG)
            Log.d("InMobiAndroidSDK_3.5.2", "mutex failed " + localInterruptedException);
      }
    }
    JSController.PlayerProperties localPlayerProperties = new JSController.PlayerProperties();
    localPlayerProperties.setProperties(false, paramBoolean1, paramBoolean2, paramBoolean3, paramString2, paramString3, paramString4);
    Bundle localBundle = new Bundle();
    localBundle.putString("expand_url", paramString1);
    localBundle.putParcelable("player_properties", localPlayerProperties);
    Message localMessage = this.aH.obtainMessage(1008);
    localMessage.setData(localBundle);
    this.aH.sendMessage(localMessage);
  }

  public void playAudioImpl(Bundle paramBundle, Activity paramActivity)
  {
    JSController.PlayerProperties localPlayerProperties = (JSController.PlayerProperties)paramBundle.getParcelable("player_properties");
    String str = paramBundle.getString("expand_url");
    if (str == null)
      str = "";
    this.at = b(localPlayerProperties.id, str, paramActivity);
    FrameLayout localFrameLayout;
    if (this.at != null)
    {
      if (str.length() != 0)
        this.at.setPlayData(localPlayerProperties, str);
      this.av.put(localPlayerProperties.id, this.at);
      localFrameLayout = (FrameLayout)getRootView().findViewById(16908290);
      if (!localPlayerProperties.isFullScreen())
        break label245;
      RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(-1, -1);
      localLayoutParams.addRule(13);
      this.at.setLayoutParams(localLayoutParams);
      RelativeLayout localRelativeLayout = new RelativeLayout(paramActivity);
      localRelativeLayout.setOnTouchListener(new IMWebView.10(this));
      localRelativeLayout.setBackgroundColor(-16777216);
      localFrameLayout.addView(localRelativeLayout, new RelativeLayout.LayoutParams(-1, -1));
      localRelativeLayout.addView(this.at);
      this.at.setBackGroundLayout(localRelativeLayout);
      this.at.requestFocus();
      this.at.setOnKeyListener(new IMWebView.11(this));
    }
    while (true)
    {
      this.at.setListener(new IMWebView.12(this, localPlayerProperties));
      this.at.play();
      return;
      label245: this.at.setLayoutParams(new ViewGroup.LayoutParams(1, 1));
      localFrameLayout.addView(this.at);
    }
  }

  public void playVideo(String paramString1, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, JSController.Dimensions paramDimensions, String paramString2, String paramString3, String paramString4)
  {
    synchronized (this.mutex)
    {
      boolean bool = this.isMutexAquired.get();
      if (bool);
      try
      {
        this.mutex.wait();
        if ((!this.mIsInterstitialAd) && (this.ab != ViewState.EXPANDED))
        {
          raiseError("Cannot play video.Ad is not in an expanded state", "playVideo");
          return;
        }
      }
      catch (InterruptedException localInterruptedException)
      {
        while (true)
          if (Constants.DEBUG)
            Log.d("InMobiAndroidSDK_3.5.2", "mutex failed " + localInterruptedException);
      }
    }
    if ((!this.au.isEmpty()) && (this.au.size() == 5) && (!this.au.containsKey(paramString4)))
    {
      raiseError("Player Error. Exceeding permissible limit for saved play instances", "playVideo");
      return;
    }
    Message localMessage = this.aH.obtainMessage(1007);
    JSController.PlayerProperties localPlayerProperties = new JSController.PlayerProperties();
    localPlayerProperties.setProperties(paramBoolean1, paramBoolean2, paramBoolean3, paramBoolean4, paramString2, paramString3, paramString4);
    Bundle localBundle = new Bundle();
    localBundle.putString("expand_url", paramString1);
    localBundle.putParcelable("player_properties", localPlayerProperties);
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "Before validation dimension: (" + paramDimensions.x + ", " + paramDimensions.y + ", " + paramDimensions.width + ", " + paramDimensions.height + ")");
    a(paramDimensions);
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "After validation dimension: (" + paramDimensions.x + ", " + paramDimensions.y + ", " + paramDimensions.width + ", " + paramDimensions.height + ")");
    localBundle.putParcelable("expand_dimensions", paramDimensions);
    localMessage.setData(localBundle);
    this.aH.sendMessage(localMessage);
  }

  public void playVideoImpl(Bundle paramBundle, Activity paramActivity)
  {
    JSController.PlayerProperties localPlayerProperties1 = (JSController.PlayerProperties)paramBundle.getParcelable("player_properties");
    JSController.Dimensions localDimensions = (JSController.Dimensions)paramBundle.getParcelable("expand_dimensions");
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "Final dimensions: " + localDimensions);
    String str = paramBundle.getString("expand_url");
    if (!a(localPlayerProperties1.id, str, paramActivity, localDimensions))
      return;
    this.ah = true;
    JSController.PlayerProperties localPlayerProperties3;
    if (str.length() == 0)
    {
      localPlayerProperties3 = this.as.getProperties();
      localDimensions = this.as.getPlayDimensions();
      this.as.getMediaURL();
    }
    FrameLayout localFrameLayout1;
    for (JSController.PlayerProperties localPlayerProperties2 = localPlayerProperties3; ; localPlayerProperties2 = localPlayerProperties1)
    {
      localFrameLayout1 = (FrameLayout)getRootView().findViewById(16908290);
      if (!localPlayerProperties2.isFullScreen())
        break;
      RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(-1, -1);
      localLayoutParams.addRule(13);
      this.as.setLayoutParams(localLayoutParams);
      RelativeLayout localRelativeLayout = new RelativeLayout(paramActivity);
      localRelativeLayout.setOnTouchListener(new IMWebView.2(this));
      localRelativeLayout.setBackgroundColor(-16777216);
      localFrameLayout1.addView(localRelativeLayout, new FrameLayout.LayoutParams(-1, -1));
      localRelativeLayout.addView(this.as);
      this.as.setBackGroundLayout(localRelativeLayout);
      this.as.requestFocus();
      this.as.setOnKeyListener(new IMWebView.3(this));
      this.as.setListener(new IMWebView.4(this));
      this.as.play();
      return;
      this.as.setPlayData(localPlayerProperties1, str);
      this.as.setPlayDimensions(localDimensions);
    }
    this.as.setLayoutParams(new FrameLayout.LayoutParams(localDimensions.width, localDimensions.height));
    FrameLayout localFrameLayout2 = new FrameLayout(paramActivity);
    if (this.ap == null)
      localFrameLayout2.setPadding(localDimensions.x, localDimensions.y, 0, 0);
    while (true)
    {
      localFrameLayout1.addView(localFrameLayout2, new FrameLayout.LayoutParams(-1, -1));
      this.as.setBackGroundLayout(localFrameLayout2);
      localFrameLayout2.addView(this.as);
      break;
      localFrameLayout2.setPadding(localDimensions.x + this.ap.currentX, localDimensions.y + this.ap.currentY, 0, 0);
    }
  }

  public void raiseError(String paramString1, String paramString2)
  {
    Message localMessage = this.aH.obtainMessage(1009);
    Bundle localBundle = new Bundle();
    localBundle.putString("message", paramString1);
    localBundle.putString("action", paramString2);
    localMessage.setData(localBundle);
    this.aH.sendMessage(localMessage);
  }

  public void registerProtocol(String paramString)
  {
    if (paramString != null)
      this.ad.add(paramString.toLowerCase());
  }

  public void reinitializeExpandProperties()
  {
    this.W.reinitializeExpandProperties();
  }

  public void releaseAllPlayers()
  {
    if (this.as != null)
      this.au.put(this.as.getPropertyID(), this.as);
    try
    {
      localIterator2 = this.au.entrySet().iterator();
      boolean bool2 = localIterator2.hasNext();
      if (!bool2)
      {
        label54: this.au.clear();
        this.as = null;
      }
    }
    catch (Exception localException1)
    {
      try
      {
        Iterator localIterator2;
        Iterator localIterator1 = this.av.entrySet().iterator();
        while (true)
        {
          boolean bool1 = localIterator1.hasNext();
          if (!bool1)
          {
            label92: userInitiatedClose = false;
            this.av.clear();
            this.at = null;
            return;
            ((AVPlayer)((Map.Entry)localIterator2.next()).getValue()).releasePlayer(userInitiatedClose);
            break;
            localException1 = localException1;
            break label54;
          }
          ((AVPlayer)((Map.Entry)localIterator1.next()).getValue()).releasePlayer(userInitiatedClose);
        }
      }
      catch (Exception localException2)
      {
        break label92;
      }
    }
  }

  public void replaceByPlaceholder()
  {
    while (true)
    {
      int i1;
      int i2;
      try
      {
        ViewGroup localViewGroup = (ViewGroup)getParent();
        i1 = localViewGroup.getChildCount();
        i2 = 0;
        break label133;
        this.aa = i2;
        FrameLayout localFrameLayout = new FrameLayout(getContext());
        localFrameLayout.setId(437);
        localViewGroup.addView(localFrameLayout, i2, new ViewGroup.LayoutParams(getWidth(), getHeight()));
        localViewGroup.removeView(this);
        this.aJ = localViewGroup;
        return;
        View localView = localViewGroup.getChildAt(i2);
        if (localView == this)
          continue;
        i2++;
      }
      catch (Exception localException)
      {
        if (!Constants.DEBUG)
          break;
      }
      Log.d("InMobiAndroidSDK_3.5.2", "Exception in replaceByPlaceHolder" + localException);
      return;
      label133: if (i2 < i1);
    }
  }

  public void requestOnInterstitialClosed(Message paramMessage)
  {
    this.aP = paramMessage;
  }

  public void requestOnPageFinishedCallback(Message paramMessage)
  {
    this.aN = paramMessage;
  }

  public void requestOnSearchAdClicked(Message paramMessage)
  {
    this.aS = paramMessage;
  }

  public void resetContents(Activity paramActivity)
  {
    try
    {
      FrameLayout localFrameLayout1;
      FrameLayout localFrameLayout2;
      FrameLayout localFrameLayout3;
      if (this.mIsExpandUrlValid)
      {
        localFrameLayout1 = (FrameLayout)this.aj.getRootView().findViewById(16908290);
        localFrameLayout2 = (FrameLayout)this.aQ.findViewById(437);
        localFrameLayout3 = (FrameLayout)localFrameLayout1.findViewById(435);
        Log.w("InMobiAndroidSDK_3.5.2", "PlaceHolder ID: " + localFrameLayout2 + " Bg ID: " + localFrameLayout3);
        if (this.mIsExpandUrlValid)
          this.aj.releaseAllPlayers();
        if (localFrameLayout3 != null)
        {
          if (!this.mIsExpandUrlValid)
            break label199;
          ((ViewGroup)localFrameLayout3.getChildAt(0)).removeView(this.aj);
          this.aj = null;
        }
      }
      while (true)
      {
        localFrameLayout1.removeView(localFrameLayout3);
        f();
        if (localFrameLayout2 != null)
        {
          this.aJ.removeView(localFrameLayout2);
          this.aJ.addView(this, this.aa);
        }
        this.aJ.invalidate();
        return;
        localFrameLayout1 = (FrameLayout)getRootView().findViewById(16908290);
        break;
        label199: ((ViewGroup)localFrameLayout3.getChildAt(0)).removeView(this);
      }
    }
    catch (Exception localException1)
    {
    }
    try
    {
      ViewGroup localViewGroup = (ViewGroup)getParent();
      localViewGroup.removeAllViews();
      ((ViewGroup)localViewGroup.getParent()).removeAllViews();
      label241: Log.w("InMobiAndroidSDK_3.5.2", "Exception while closing the expanded Ad", localException1);
      return;
    }
    catch (Exception localException2)
    {
      break label241;
    }
  }

  public WebBackForwardList restoreState(Bundle paramBundle)
  {
    this.ac = paramBundle.getString("AD_PATH");
    super.loadUrl("file://" + this.ac + File.separator + "_mraid_current");
    return null;
  }

  public WebBackForwardList saveState(Bundle paramBundle)
  {
    paramBundle.putString("AD_PATH", this.ac);
    return null;
  }

  public void seekAudio(String paramString, int paramInt)
  {
    AVPlayer localAVPlayer = e(paramString);
    if (localAVPlayer == null)
    {
      raiseError("Invalid property ID", "seekAudio");
      return;
    }
    if (localAVPlayer.getState() == Constants.playerState.RELEASED)
    {
      raiseError("Invalid player state", "seekAudio");
      return;
    }
    Message localMessage = this.aH.obtainMessage(1024);
    Bundle localBundle = new Bundle();
    localBundle.putInt("seekaudio", paramInt);
    localMessage.setData(localBundle);
    localMessage.obj = localAVPlayer;
    localMessage.sendToTarget();
  }

  public void seekVideo(String paramString, int paramInt)
  {
    AVPlayer localAVPlayer = b(paramString);
    if (localAVPlayer == null)
    {
      raiseError("Invalid property ID", "seekVideo");
      return;
    }
    if ((localAVPlayer.getState() == Constants.playerState.RELEASED) || (localAVPlayer.getState() == Constants.playerState.INIT))
    {
      raiseError("Invalid player state", "seekVideo");
      return;
    }
    Message localMessage = this.aH.obtainMessage(1020);
    Bundle localBundle = new Bundle();
    localBundle.putInt("seek", paramInt);
    localMessage.setData(localBundle);
    localMessage.obj = localAVPlayer;
    this.aH.sendMessage(localMessage);
  }

  public void setActivity(Activity paramActivity)
  {
    this.aQ = paramActivity;
  }

  public void setAdUnit(AdUnit paramAdUnit)
  {
    this.ai = paramAdUnit;
  }

  public void setAudioVolume(String paramString, int paramInt)
  {
    if (e(paramString) == null)
    {
      raiseError("Invalid property ID", "setAudioVolume");
      return;
    }
    Message localMessage = this.aH.obtainMessage(1023);
    Bundle localBundle = new Bundle();
    localBundle.putInt("vol", paramInt);
    localBundle.putString("aplayerref", paramString);
    localMessage.setData(localBundle);
    localMessage.sendToTarget();
  }

  public void setCustomClose(boolean paramBoolean)
  {
    this.al = paramBoolean;
    if (this.mIsInterstitialAd)
    {
      Message localMessage = this.aH.obtainMessage(1011);
      this.aH.sendMessage(localMessage);
    }
  }

  public void setExpandPropertiesForInterstitial(boolean paramBoolean1, boolean paramBoolean2, String paramString)
  {
    setCustomClose(paramBoolean1);
    this.aq = paramBoolean2;
    this.ar = paramString;
    if (this.ak)
      h();
  }

  public void setExpandedActivity(Activity paramActivity)
  {
    this.aR = paramActivity;
  }

  public void setExternalWebViewClient(WebViewClient paramWebViewClient)
  {
    this.aW = paramWebViewClient;
  }

  public void setState(ViewState paramViewState)
  {
    this.ab = paramViewState;
    if (paramViewState != ViewState.EXPANDING)
      injectJavaScript("window.mraidview.fireStateChangeEvent('" + getState() + "');");
  }

  public void setVideoVolume(String paramString, int paramInt)
  {
    AVPlayer localAVPlayer = b(paramString);
    if (localAVPlayer == null)
    {
      raiseError("Invalid property ID", "setVideoVolume");
      return;
    }
    if (localAVPlayer.getState() == Constants.playerState.RELEASED)
    {
      raiseError("Invalid player state", "setVideoVolume");
      return;
    }
    Message localMessage = this.aH.obtainMessage(1019);
    Bundle localBundle = new Bundle();
    localBundle.putInt("volume", paramInt);
    localMessage.setData(localBundle);
    localMessage.obj = localAVPlayer;
    this.aH.sendMessage(localMessage);
  }

  public void setViewable(boolean paramBoolean)
  {
    this.aM = paramBoolean;
    injectJavaScript("window.mraidview.fireViewableChangeEvent(" + isViewable() + ");");
  }

  public void show()
  {
    this.aH.sendEmptyMessage(1003);
  }

  public void showVideo(String paramString)
  {
    Message localMessage = this.aH.obtainMessage(1016);
    Bundle localBundle = new Bundle();
    localBundle.putString("pid", paramString);
    localMessage.setData(localBundle);
    this.aH.sendMessage(localMessage);
  }

  public void unMuteAudio(String paramString)
  {
    AVPlayer localAVPlayer = e(paramString);
    if (localAVPlayer == null)
    {
      raiseError("Invalid property ID", "unmuteAudio");
      return;
    }
    if (localAVPlayer.getState() == Constants.playerState.RELEASED)
    {
      raiseError("Invalid player state", "unmuteAudio");
      return;
    }
    Message localMessage = this.aH.obtainMessage(1022);
    Bundle localBundle = new Bundle();
    localBundle.putString("aplayerref", paramString);
    localMessage.setData(localBundle);
    localMessage.sendToTarget();
  }

  public void unMuteVideo(String paramString)
  {
    AVPlayer localAVPlayer = b(paramString);
    if (localAVPlayer == null)
    {
      raiseError("Invalid property ID", "unMuteVideo");
      return;
    }
    if ((localAVPlayer.getState() == Constants.playerState.RELEASED) || (localAVPlayer.getState() == Constants.playerState.INIT))
    {
      raiseError("Invalid player state", "unMuteVideo");
      return;
    }
    Message localMessage = this.aH.obtainMessage(1018);
    localMessage.obj = localAVPlayer;
    this.aH.sendMessage(localMessage);
  }

  public static abstract interface IMWebViewListener
  {
    public abstract void handleRequest(String paramString);

    public abstract boolean onDismissAdScreen();

    public abstract boolean onEventFired();

    public abstract boolean onExpand();

    public abstract boolean onExpandClose();

    public abstract boolean onLeaveApplication();

    public abstract boolean onReady();

    public abstract boolean onResize();

    public abstract boolean onResizeClose();

    public abstract boolean onShowScreen();
  }

  public static abstract class NewLocationReciever
  {
    public abstract void OnNewLocation(IMWebView.ViewState paramViewState);
  }

  public static enum ViewState
  {
    static
    {
      DEFAULT = new ViewState("DEFAULT", 1);
      RESIZED = new ViewState("RESIZED", 2);
      EXPANDED = new ViewState("EXPANDED", 3);
      EXPANDING = new ViewState("EXPANDING", 4);
      HIDDEN = new ViewState("HIDDEN", 5);
      ViewState[] arrayOfViewState = new ViewState[6];
      arrayOfViewState[0] = LOADING;
      arrayOfViewState[1] = DEFAULT;
      arrayOfViewState[2] = RESIZED;
      arrayOfViewState[3] = EXPANDED;
      arrayOfViewState[4] = EXPANDING;
      arrayOfViewState[5] = HIDDEN;
    }
  }

  class a extends GestureDetector.SimpleOnGestureListener
  {
    a()
    {
    }

    public boolean onScroll(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2)
    {
      return true;
    }
  }

  class b extends TimerTask
  {
    int a = 0;
    int b = 0;

    b()
    {
    }

    public void run()
    {
      int i = IMWebView.this.getProgress();
      if (i == 100)
        cancel();
      while (true)
      {
        this.a = i;
        return;
        if (this.a != i)
          continue;
        this.b = (1 + this.b);
        if (this.b != 3)
          continue;
        try
        {
          IMWebView.this.stopLoading();
          cancel();
        }
        catch (Exception localException)
        {
          while (true)
          {
            Log.w("InMobiAndroidSDK_3.5.2", "IMWebView-> error in stopLoading");
            localException.printStackTrace();
          }
        }
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.ai.container.IMWebView
 * JD-Core Version:    0.6.2
 */