package com.inmobi.androidsdk.ai.controller;

import android.content.Context;
import android.os.StatFs;
import android.util.Log;
import com.inmobi.androidsdk.ai.container.IMWebView;
import com.inmobi.androidsdk.impl.Constants;
import java.io.File;
import java.io.FileOutputStream;
import java.security.MessageDigest;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class JSAssetController extends JSController
{
  private static final char[] a = { 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102 };

  public JSAssetController(IMWebView paramIMWebView, Context paramContext)
  {
    super(paramIMWebView, paramContext);
  }

  private String a()
  {
    return this.mContext.getFilesDir().getPath();
  }

  private String a(String paramString1, String paramString2, String paramString3)
  {
    File localFile1 = new File(paramString2 + File.separator + paramString1);
    new File(paramString2 + File.separator + "ad").mkdir();
    File localFile2 = new File(paramString2 + File.separator + "ad" + File.separator + paramString3);
    localFile2.mkdir();
    localFile1.renameTo(new File(localFile2, localFile1.getName()));
    return localFile2.getPath() + File.separator;
  }

  private String a(MessageDigest paramMessageDigest)
  {
    int i = 0;
    byte[] arrayOfByte = paramMessageDigest.digest();
    char[] arrayOfChar = new char[2 * arrayOfByte.length];
    for (int j = 0; ; j++)
    {
      if (j >= arrayOfByte.length)
        return new String(arrayOfChar);
      int k = i + 1;
      arrayOfChar[i] = a[(0xF & arrayOfByte[j] >>> 4)];
      i = k + 1;
      arrayOfChar[k] = a[(0xF & arrayOfByte[j])];
    }
  }

  private HttpEntity a(String paramString)
  {
    try
    {
      HttpEntity localHttpEntity = new DefaultHttpClient().execute(new HttpGet(paramString)).getEntity();
      return localHttpEntity;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
    return null;
  }

  private File b(String paramString)
  {
    File localFile = this.mContext.getFilesDir();
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "Tmp File dir: " + localFile);
    return new File(localFile.getPath() + File.separator + paramString);
  }

  private String c(String paramString)
  {
    int i = paramString.lastIndexOf(File.separatorChar);
    String str = "/";
    if (i >= 0)
      str = paramString.substring(0, paramString.lastIndexOf(File.separatorChar));
    return str;
  }

  private String d(String paramString)
  {
    if (paramString.lastIndexOf(File.separatorChar) >= 0)
      paramString = paramString.substring(1 + paramString.lastIndexOf(File.separatorChar));
    return paramString;
  }

  public static boolean deleteDirectory(File paramFile)
  {
    File[] arrayOfFile;
    int i;
    if (paramFile.exists())
    {
      arrayOfFile = paramFile.listFiles();
      i = 0;
      if (i < arrayOfFile.length);
    }
    else
    {
      return paramFile.delete();
    }
    if (arrayOfFile[i].isDirectory())
      deleteDirectory(arrayOfFile[i]);
    while (true)
    {
      i++;
      break;
      arrayOfFile[i].delete();
    }
  }

  public static boolean deleteDirectory(String paramString)
  {
    if (paramString != null)
      return deleteDirectory(new File(paramString));
    return false;
  }

  // ERROR //
  public void addAsset(String paramString1, String paramString2)
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_2
    //   2: invokespecial 180	com/inmobi/androidsdk/ai/controller/JSAssetController:a	(Ljava/lang/String;)Lorg/apache/http/HttpEntity;
    //   5: astore_3
    //   6: aconst_null
    //   7: astore 4
    //   9: aload_3
    //   10: invokeinterface 186 1 0
    //   15: astore 4
    //   17: aload_0
    //   18: aload 4
    //   20: aload_1
    //   21: iconst_0
    //   22: invokevirtual 190	com/inmobi/androidsdk/ai/controller/JSAssetController:writeToDisk	(Ljava/io/InputStream;Ljava/lang/String;Z)Ljava/lang/String;
    //   25: pop
    //   26: new 49	java/lang/StringBuilder
    //   29: dup
    //   30: ldc 192
    //   32: invokespecial 58	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   35: aload_1
    //   36: invokevirtual 66	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   39: ldc 194
    //   41: invokevirtual 66	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   44: invokevirtual 69	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   47: astore 11
    //   49: aload_0
    //   50: getfield 198	com/inmobi/androidsdk/ai/controller/JSAssetController:imWebView	Lcom/inmobi/androidsdk/ai/container/IMWebView;
    //   53: aload 11
    //   55: invokevirtual 203	com/inmobi/androidsdk/ai/container/IMWebView:injectJavaScript	(Ljava/lang/String;)V
    //   58: aload 4
    //   60: ifnull +8 -> 68
    //   63: aload 4
    //   65: invokevirtual 208	java/io/InputStream:close	()V
    //   68: aload_3
    //   69: invokeinterface 211 1 0
    //   74: return
    //   75: astore 7
    //   77: aload 7
    //   79: invokevirtual 119	java/lang/Exception:printStackTrace	()V
    //   82: aload 4
    //   84: ifnull -16 -> 68
    //   87: aload 4
    //   89: invokevirtual 208	java/io/InputStream:close	()V
    //   92: goto -24 -> 68
    //   95: astore 8
    //   97: goto -29 -> 68
    //   100: astore 5
    //   102: aload 4
    //   104: ifnull +8 -> 112
    //   107: aload 4
    //   109: invokevirtual 208	java/io/InputStream:close	()V
    //   112: aload 5
    //   114: athrow
    //   115: astore 9
    //   117: aload 9
    //   119: invokevirtual 119	java/lang/Exception:printStackTrace	()V
    //   122: return
    //   123: astore 6
    //   125: goto -13 -> 112
    //   128: astore 12
    //   130: goto -62 -> 68
    //
    // Exception table:
    //   from	to	target	type
    //   9	58	75	java/lang/Exception
    //   87	92	95	java/lang/Exception
    //   9	58	100	finally
    //   77	82	100	finally
    //   68	74	115	java/lang/Exception
    //   107	112	123	java/lang/Exception
    //   63	68	128	java/lang/Exception
  }

  public int cacheRemaining()
  {
    StatFs localStatFs = new StatFs(this.mContext.getFilesDir().getPath());
    return localStatFs.getFreeBlocks() * localStatFs.getBlockSize();
  }

  // ERROR //
  public String copyTextFromJarIntoAssetDir(String paramString1, String paramString2)
  {
    // Byte code:
    //   0: ldc 2
    //   2: invokevirtual 230	java/lang/Class:getClassLoader	()Ljava/lang/ClassLoader;
    //   5: aload_2
    //   6: invokevirtual 236	java/lang/ClassLoader:getResource	(Ljava/lang/String;)Ljava/net/URL;
    //   9: invokevirtual 241	java/net/URL:getFile	()Ljava/lang/String;
    //   12: astore 10
    //   14: aload 10
    //   16: ldc 243
    //   18: invokevirtual 246	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   21: ifeq +11 -> 32
    //   24: aload 10
    //   26: iconst_5
    //   27: invokevirtual 159	java/lang/String:substring	(I)Ljava/lang/String;
    //   30: astore 10
    //   32: aload 10
    //   34: ldc 248
    //   36: invokevirtual 252	java/lang/String:indexOf	(Ljava/lang/String;)I
    //   39: istore 11
    //   41: iload 11
    //   43: ifle +13 -> 56
    //   46: aload 10
    //   48: iconst_0
    //   49: iload 11
    //   51: invokevirtual 156	java/lang/String:substring	(II)Ljava/lang/String;
    //   54: astore 10
    //   56: new 254	java/util/jar/JarFile
    //   59: dup
    //   60: aload 10
    //   62: invokespecial 255	java/util/jar/JarFile:<init>	(Ljava/lang/String;)V
    //   65: astore 12
    //   67: aload 12
    //   69: aload 12
    //   71: aload_2
    //   72: invokevirtual 259	java/util/jar/JarFile:getJarEntry	(Ljava/lang/String;)Ljava/util/jar/JarEntry;
    //   75: invokevirtual 263	java/util/jar/JarFile:getInputStream	(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;
    //   78: astore 13
    //   80: aload 13
    //   82: astore 4
    //   84: aload_0
    //   85: aload 4
    //   87: aload_1
    //   88: iconst_0
    //   89: invokevirtual 190	com/inmobi/androidsdk/ai/controller/JSAssetController:writeToDisk	(Ljava/io/InputStream;Ljava/lang/String;Z)Ljava/lang/String;
    //   92: astore 14
    //   94: aload 14
    //   96: astore 8
    //   98: aload 4
    //   100: ifnull +8 -> 108
    //   103: aload 4
    //   105: invokevirtual 208	java/io/InputStream:close	()V
    //   108: aload 8
    //   110: areturn
    //   111: astore 7
    //   113: aconst_null
    //   114: astore 4
    //   116: aload 7
    //   118: invokevirtual 119	java/lang/Exception:printStackTrace	()V
    //   121: aconst_null
    //   122: astore 8
    //   124: aload 4
    //   126: ifnull -18 -> 108
    //   129: aload 4
    //   131: invokevirtual 208	java/io/InputStream:close	()V
    //   134: aconst_null
    //   135: areturn
    //   136: astore 9
    //   138: aconst_null
    //   139: areturn
    //   140: astore_3
    //   141: aconst_null
    //   142: astore 4
    //   144: aload_3
    //   145: astore 5
    //   147: aload 4
    //   149: ifnull +8 -> 157
    //   152: aload 4
    //   154: invokevirtual 208	java/io/InputStream:close	()V
    //   157: aload 5
    //   159: athrow
    //   160: astore 15
    //   162: aload 8
    //   164: areturn
    //   165: astore 6
    //   167: goto -10 -> 157
    //   170: astore 5
    //   172: goto -25 -> 147
    //   175: astore 7
    //   177: goto -61 -> 116
    //
    // Exception table:
    //   from	to	target	type
    //   0	32	111	java/lang/Exception
    //   32	41	111	java/lang/Exception
    //   46	56	111	java/lang/Exception
    //   56	80	111	java/lang/Exception
    //   129	134	136	java/lang/Exception
    //   0	32	140	finally
    //   32	41	140	finally
    //   46	56	140	finally
    //   56	80	140	finally
    //   103	108	160	java/lang/Exception
    //   152	157	165	java/lang/Exception
    //   84	94	170	finally
    //   116	121	170	finally
    //   84	94	175	java/lang/Exception
  }

  public void deleteOldAds()
  {
    deleteDirectory(new File(a() + File.separator + "ad"));
  }

  public FileOutputStream getAssetOutputString(String paramString)
  {
    File localFile = b(c(paramString));
    localFile.mkdirs();
    return new FileOutputStream(new File(localFile, d(paramString)));
  }

  public String getAssetPath()
  {
    return "file://" + this.mContext.getFilesDir() + "/";
  }

  public void removeAsset(String paramString)
  {
    File localFile = b(c(paramString));
    localFile.mkdirs();
    new File(localFile, d(paramString)).delete();
    String str = "mraidAdController.assetRemoved('" + paramString + "' )";
    this.imWebView.injectJavaScript(str);
  }

  public void stopAllListeners()
  {
  }

  // ERROR //
  public String writeToDisk(java.io.InputStream paramInputStream, String paramString, boolean paramBoolean)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 4
    //   3: sipush 1024
    //   6: newarray byte
    //   8: astore 5
    //   10: iload_3
    //   11: ifeq +86 -> 97
    //   14: ldc_w 293
    //   17: invokestatic 297	java/security/MessageDigest:getInstance	(Ljava/lang/String;)Ljava/security/MessageDigest;
    //   20: astore 14
    //   22: aload 14
    //   24: astore 6
    //   26: aload_0
    //   27: aload_2
    //   28: invokevirtual 299	com/inmobi/androidsdk/ai/controller/JSAssetController:getAssetOutputString	(Ljava/lang/String;)Ljava/io/FileOutputStream;
    //   31: astore 4
    //   33: aload_1
    //   34: aload 5
    //   36: invokevirtual 303	java/io/InputStream:read	([B)I
    //   39: istore 9
    //   41: iload 9
    //   43: ifle +60 -> 103
    //   46: iload_3
    //   47: ifeq +15 -> 62
    //   50: aload 6
    //   52: ifnull +10 -> 62
    //   55: aload 6
    //   57: aload 5
    //   59: invokevirtual 307	java/security/MessageDigest:update	([B)V
    //   62: aload 4
    //   64: aload 5
    //   66: iconst_0
    //   67: iload 9
    //   69: invokevirtual 311	java/io/FileOutputStream:write	([BII)V
    //   72: goto -39 -> 33
    //   75: astore 7
    //   77: aload 4
    //   79: ifnull +8 -> 87
    //   82: aload 4
    //   84: invokevirtual 312	java/io/FileOutputStream:close	()V
    //   87: aload 7
    //   89: athrow
    //   90: astore 13
    //   92: aload 13
    //   94: invokevirtual 313	java/security/NoSuchAlgorithmException:printStackTrace	()V
    //   97: aconst_null
    //   98: astore 6
    //   100: goto -74 -> 26
    //   103: aload 4
    //   105: invokevirtual 316	java/io/FileOutputStream:flush	()V
    //   108: aload 4
    //   110: ifnull +8 -> 118
    //   113: aload 4
    //   115: invokevirtual 312	java/io/FileOutputStream:close	()V
    //   118: aload_0
    //   119: invokespecial 266	com/inmobi/androidsdk/ai/controller/JSAssetController:a	()Ljava/lang/String;
    //   122: astore 10
    //   124: iload_3
    //   125: ifeq +53 -> 178
    //   128: aload 6
    //   130: ifnull +48 -> 178
    //   133: aload_0
    //   134: aload_2
    //   135: aload 10
    //   137: aload_0
    //   138: aload 6
    //   140: invokespecial 318	com/inmobi/androidsdk/ai/controller/JSAssetController:a	(Ljava/security/MessageDigest;)Ljava/lang/String;
    //   143: invokespecial 320	com/inmobi/androidsdk/ai/controller/JSAssetController:a	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   146: astore 11
    //   148: new 49	java/lang/StringBuilder
    //   151: dup
    //   152: aload 11
    //   154: invokestatic 55	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   157: invokespecial 58	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   160: aload_2
    //   161: invokevirtual 66	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   164: invokevirtual 69	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   167: areturn
    //   168: astore 8
    //   170: goto -83 -> 87
    //   173: astore 12
    //   175: goto -57 -> 118
    //   178: aload 10
    //   180: astore 11
    //   182: goto -34 -> 148
    //
    // Exception table:
    //   from	to	target	type
    //   26	33	75	finally
    //   33	41	75	finally
    //   55	62	75	finally
    //   62	72	75	finally
    //   103	108	75	finally
    //   14	22	90	java/security/NoSuchAlgorithmException
    //   82	87	168	java/lang/Exception
    //   113	118	173	java/lang/Exception
  }

  // ERROR //
  public String writeToDiskWrap(java.io.InputStream paramInputStream, String paramString1, boolean paramBoolean, String paramString2, String paramString3, String paramString4)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 7
    //   3: sipush 1024
    //   6: newarray byte
    //   8: astore 8
    //   10: iload_3
    //   11: ifeq +96 -> 107
    //   14: ldc_w 293
    //   17: invokestatic 297	java/security/MessageDigest:getInstance	(Ljava/lang/String;)Ljava/security/MessageDigest;
    //   20: astore 26
    //   22: aload 26
    //   24: astore 9
    //   26: new 324	java/io/ByteArrayOutputStream
    //   29: dup
    //   30: invokespecial 325	java/io/ByteArrayOutputStream:<init>	()V
    //   33: astore 10
    //   35: aload_1
    //   36: aload 8
    //   38: invokevirtual 303	java/io/InputStream:read	([B)I
    //   41: istore 14
    //   43: iload 14
    //   45: ifle +68 -> 113
    //   48: iload_3
    //   49: ifeq +18 -> 67
    //   52: aconst_null
    //   53: astore 7
    //   55: aload 9
    //   57: ifnull +10 -> 67
    //   60: aload 9
    //   62: aload 8
    //   64: invokevirtual 307	java/security/MessageDigest:update	([B)V
    //   67: aload 10
    //   69: aload 8
    //   71: iconst_0
    //   72: iload 14
    //   74: invokevirtual 326	java/io/ByteArrayOutputStream:write	([BII)V
    //   77: goto -42 -> 35
    //   80: astore 11
    //   82: aload 10
    //   84: invokevirtual 327	java/io/ByteArrayOutputStream:close	()V
    //   87: aload 7
    //   89: ifnull +8 -> 97
    //   92: aload 7
    //   94: invokevirtual 312	java/io/FileOutputStream:close	()V
    //   97: aload 11
    //   99: athrow
    //   100: astore 25
    //   102: aload 25
    //   104: invokevirtual 313	java/security/NoSuchAlgorithmException:printStackTrace	()V
    //   107: aconst_null
    //   108: astore 9
    //   110: goto -84 -> 26
    //   113: aload 10
    //   115: invokevirtual 328	java/io/ByteArrayOutputStream:toString	()Ljava/lang/String;
    //   118: astore 15
    //   120: aload 15
    //   122: ldc_w 330
    //   125: invokevirtual 252	java/lang/String:indexOf	(Ljava/lang/String;)I
    //   128: iflt +399 -> 527
    //   131: iconst_1
    //   132: istore 16
    //   134: aconst_null
    //   135: astore 7
    //   137: iload 16
    //   139: ifeq +433 -> 572
    //   142: new 332	java/lang/StringBuffer
    //   145: dup
    //   146: aload 15
    //   148: invokespecial 333	java/lang/StringBuffer:<init>	(Ljava/lang/String;)V
    //   151: astore 17
    //   153: aload 17
    //   155: ldc_w 335
    //   158: invokevirtual 336	java/lang/StringBuffer:indexOf	(Ljava/lang/String;)I
    //   161: istore 18
    //   163: aload 17
    //   165: iload 18
    //   167: iload 18
    //   169: bipush 16
    //   171: iadd
    //   172: new 49	java/lang/StringBuilder
    //   175: dup
    //   176: ldc_w 338
    //   179: invokespecial 58	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   182: aload 5
    //   184: invokevirtual 66	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   187: invokevirtual 69	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   190: invokevirtual 342	java/lang/StringBuffer:replace	(IILjava/lang/String;)Ljava/lang/StringBuffer;
    //   193: pop
    //   194: aload 17
    //   196: ldc_w 344
    //   199: invokevirtual 336	java/lang/StringBuffer:indexOf	(Ljava/lang/String;)I
    //   202: istore 20
    //   204: aload 17
    //   206: iload 20
    //   208: iload 20
    //   210: bipush 9
    //   212: iadd
    //   213: new 49	java/lang/StringBuilder
    //   216: dup
    //   217: ldc_w 338
    //   220: invokespecial 58	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   223: aload 6
    //   225: invokevirtual 66	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   228: invokevirtual 69	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   231: invokevirtual 342	java/lang/StringBuffer:replace	(IILjava/lang/String;)Ljava/lang/StringBuffer;
    //   234: pop
    //   235: aload_0
    //   236: aload_2
    //   237: invokevirtual 299	com/inmobi/androidsdk/ai/controller/JSAssetController:getAssetOutputString	(Ljava/lang/String;)Ljava/io/FileOutputStream;
    //   240: astore 7
    //   242: iload 16
    //   244: ifne +181 -> 425
    //   247: aload 7
    //   249: ldc_w 346
    //   252: invokevirtual 349	java/lang/String:getBytes	()[B
    //   255: invokevirtual 351	java/io/FileOutputStream:write	([B)V
    //   258: aload 7
    //   260: ldc_w 353
    //   263: invokevirtual 349	java/lang/String:getBytes	()[B
    //   266: invokevirtual 351	java/io/FileOutputStream:write	([B)V
    //   269: aload 7
    //   271: ldc_w 355
    //   274: invokevirtual 349	java/lang/String:getBytes	()[B
    //   277: invokevirtual 351	java/io/FileOutputStream:write	([B)V
    //   280: aload 7
    //   282: ldc_w 357
    //   285: invokevirtual 349	java/lang/String:getBytes	()[B
    //   288: invokevirtual 351	java/io/FileOutputStream:write	([B)V
    //   291: aload 7
    //   293: new 49	java/lang/StringBuilder
    //   296: dup
    //   297: ldc_w 359
    //   300: invokespecial 58	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   303: aload 5
    //   305: invokevirtual 66	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   308: ldc_w 361
    //   311: invokevirtual 66	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   314: invokevirtual 69	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   317: invokevirtual 349	java/lang/String:getBytes	()[B
    //   320: invokevirtual 351	java/io/FileOutputStream:write	([B)V
    //   323: aload 7
    //   325: new 49	java/lang/StringBuilder
    //   328: dup
    //   329: ldc_w 359
    //   332: invokespecial 58	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   335: aload 6
    //   337: invokevirtual 66	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   340: ldc_w 361
    //   343: invokevirtual 66	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   346: invokevirtual 69	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   349: invokevirtual 349	java/lang/String:getBytes	()[B
    //   352: invokevirtual 351	java/io/FileOutputStream:write	([B)V
    //   355: aload 4
    //   357: ifnull +35 -> 392
    //   360: aload 7
    //   362: ldc_w 363
    //   365: invokevirtual 349	java/lang/String:getBytes	()[B
    //   368: invokevirtual 351	java/io/FileOutputStream:write	([B)V
    //   371: aload 7
    //   373: aload 4
    //   375: invokevirtual 349	java/lang/String:getBytes	()[B
    //   378: invokevirtual 351	java/io/FileOutputStream:write	([B)V
    //   381: aload 7
    //   383: ldc_w 365
    //   386: invokevirtual 349	java/lang/String:getBytes	()[B
    //   389: invokevirtual 351	java/io/FileOutputStream:write	([B)V
    //   392: aload 7
    //   394: ldc_w 367
    //   397: invokevirtual 349	java/lang/String:getBytes	()[B
    //   400: invokevirtual 351	java/io/FileOutputStream:write	([B)V
    //   403: aload 7
    //   405: ldc_w 369
    //   408: invokevirtual 349	java/lang/String:getBytes	()[B
    //   411: invokevirtual 351	java/io/FileOutputStream:write	([B)V
    //   414: aload 7
    //   416: ldc_w 371
    //   419: invokevirtual 349	java/lang/String:getBytes	()[B
    //   422: invokevirtual 351	java/io/FileOutputStream:write	([B)V
    //   425: iload 16
    //   427: ifne +106 -> 533
    //   430: aload 7
    //   432: aload 10
    //   434: invokevirtual 374	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   437: invokevirtual 351	java/io/FileOutputStream:write	([B)V
    //   440: iload 16
    //   442: ifne +36 -> 478
    //   445: aload 7
    //   447: ldc_w 376
    //   450: invokevirtual 349	java/lang/String:getBytes	()[B
    //   453: invokevirtual 351	java/io/FileOutputStream:write	([B)V
    //   456: aload 7
    //   458: ldc_w 378
    //   461: invokevirtual 349	java/lang/String:getBytes	()[B
    //   464: invokevirtual 351	java/io/FileOutputStream:write	([B)V
    //   467: aload 7
    //   469: ldc_w 380
    //   472: invokevirtual 349	java/lang/String:getBytes	()[B
    //   475: invokevirtual 351	java/io/FileOutputStream:write	([B)V
    //   478: aload 7
    //   480: invokevirtual 316	java/io/FileOutputStream:flush	()V
    //   483: aload 10
    //   485: invokevirtual 327	java/io/ByteArrayOutputStream:close	()V
    //   488: aload 7
    //   490: ifnull +8 -> 498
    //   493: aload 7
    //   495: invokevirtual 312	java/io/FileOutputStream:close	()V
    //   498: aload_0
    //   499: invokespecial 266	com/inmobi/androidsdk/ai/controller/JSAssetController:a	()Ljava/lang/String;
    //   502: astore 23
    //   504: iload_3
    //   505: ifeq +64 -> 569
    //   508: aload 9
    //   510: ifnull +59 -> 569
    //   513: aload_0
    //   514: aload_2
    //   515: aload 23
    //   517: aload_0
    //   518: aload 9
    //   520: invokespecial 318	com/inmobi/androidsdk/ai/controller/JSAssetController:a	(Ljava/security/MessageDigest;)Ljava/lang/String;
    //   523: invokespecial 320	com/inmobi/androidsdk/ai/controller/JSAssetController:a	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   526: areturn
    //   527: iconst_0
    //   528: istore 16
    //   530: goto -396 -> 134
    //   533: aload 7
    //   535: aload 17
    //   537: invokevirtual 381	java/lang/StringBuffer:toString	()Ljava/lang/String;
    //   540: invokevirtual 349	java/lang/String:getBytes	()[B
    //   543: invokevirtual 351	java/io/FileOutputStream:write	([B)V
    //   546: goto -106 -> 440
    //   549: astore 12
    //   551: goto -464 -> 87
    //   554: astore 13
    //   556: goto -459 -> 97
    //   559: astore 22
    //   561: goto -73 -> 488
    //   564: astore 24
    //   566: goto -68 -> 498
    //   569: aload 23
    //   571: areturn
    //   572: aconst_null
    //   573: astore 17
    //   575: goto -340 -> 235
    //
    // Exception table:
    //   from	to	target	type
    //   35	43	80	finally
    //   60	67	80	finally
    //   67	77	80	finally
    //   113	131	80	finally
    //   142	235	80	finally
    //   235	242	80	finally
    //   247	355	80	finally
    //   360	392	80	finally
    //   392	425	80	finally
    //   430	440	80	finally
    //   445	478	80	finally
    //   478	483	80	finally
    //   533	546	80	finally
    //   14	22	100	java/security/NoSuchAlgorithmException
    //   82	87	549	java/lang/Exception
    //   92	97	554	java/lang/Exception
    //   483	488	559	java/lang/Exception
    //   493	498	564	java/lang/Exception
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.ai.controller.JSAssetController
 * JD-Core Version:    0.6.2
 */