package com.inmobi.androidsdk.ai.controller;

import android.os.Parcel;
import android.os.Parcelable.Creator;

class JSController$Dimensions$1
  implements Parcelable.Creator<JSController.Dimensions>
{
  public JSController.Dimensions a(Parcel paramParcel)
  {
    return new JSController.Dimensions(paramParcel);
  }

  public JSController.Dimensions[] a(int paramInt)
  {
    return new JSController.Dimensions[paramInt];
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.ai.controller.JSController.Dimensions.1
 * JD-Core Version:    0.6.2
 */