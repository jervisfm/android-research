package com.inmobi.androidsdk.ai.controller;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.inmobi.androidsdk.ai.container.IMWebView;
import com.inmobi.androidsdk.ai.controller.util.NavigationStringEnum;
import com.inmobi.androidsdk.ai.controller.util.TransitionStringEnum;
import java.lang.reflect.Field;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class JSController
{
  public static final String EXIT = "exit";
  public static final String FULL_SCREEN = "fullscreen";
  public static final String STYLE_NORMAL = "normal";
  private static final String a = "class java.lang.String";
  private static final String b = "int";
  private static final String c = "boolean";
  private static final String d = "float";
  private static final String e = "class com.mraid.NavigationStringEnum";
  private static final String f = "class com.mraid.TransitionStringEnum";
  protected ExpandProperties expProps;
  protected IMWebView imWebView;
  protected Context mContext;
  protected ExpandProperties temporaryexpProps;

  public JSController(IMWebView paramIMWebView, Context paramContext)
  {
    this.imWebView = paramIMWebView;
    this.mContext = paramContext;
    this.expProps = new ExpandProperties();
    this.temporaryexpProps = new ExpandProperties();
  }

  protected static Object getFromJSON(JSONObject paramJSONObject, Class<?> paramClass)
  {
    Field[] arrayOfField = paramClass.getDeclaredFields();
    Object localObject = paramClass.newInstance();
    int i = 0;
    while (true)
    {
      if (i >= arrayOfField.length)
        return localObject;
      Field localField = arrayOfField[i];
      String str1 = localField.getName().replace('_', '-');
      String str2 = localField.getType().toString();
      try
      {
        String str3;
        int j;
        if (str2.equals("int"))
        {
          str3 = paramJSONObject.getString(str1).toLowerCase();
          boolean bool = str3.startsWith("#");
          if (bool)
            j = -1;
        }
        try
        {
          if (str3.startsWith("#0x"))
          {
            int m = Integer.decode(str3.substring(1)).intValue();
            j = m;
            label118: localField.set(localObject, Integer.valueOf(j));
          }
          while (true)
          {
            label129: i++;
            break;
            int k = Integer.parseInt(str3.substring(1), 16);
            j = k;
            break label118;
            j = Integer.parseInt(str3);
            break label118;
            if (str2.equals("class java.lang.String"))
              localField.set(localObject, paramJSONObject.getString(str1));
            else if (str2.equals("boolean"))
              localField.set(localObject, Boolean.valueOf(paramJSONObject.getBoolean(str1)));
            else if (str2.equals("float"))
              localField.set(localObject, Float.valueOf(Float.parseFloat(paramJSONObject.getString(str1))));
            else if (str2.equals("class com.mraid.NavigationStringEnum"))
              localField.set(localObject, NavigationStringEnum.fromString(paramJSONObject.getString(str1)));
            else if (str2.equals("class com.mraid.TransitionStringEnum"))
              localField.set(localObject, TransitionStringEnum.fromString(paramJSONObject.getString(str1)));
          }
        }
        catch (NumberFormatException localNumberFormatException)
        {
          break label118;
        }
      }
      catch (JSONException localJSONException)
      {
        break label129;
      }
    }
  }

  public void reinitializeExpandProperties()
  {
    this.expProps.reinitializeExpandProperties();
  }

  public abstract void stopAllListeners();

  public static class Dimensions extends JSController.ReflectedParcelable
  {
    public static final Parcelable.Creator<Dimensions> CREATOR = new JSController.Dimensions.1();
    public int height;
    public int width;
    public int x;
    public int y;

    public Dimensions()
    {
      this.x = -1;
      this.y = -1;
      this.width = -1;
      this.height = -1;
    }

    protected Dimensions(Parcel paramParcel)
    {
      super();
    }

    public String toString()
    {
      return "x: " + this.x + ", y: " + this.y + ", width: " + this.width + ", height: " + this.height;
    }
  }

  public static class ExpandProperties extends JSController.ReflectedParcelable
  {
    public static final Parcelable.Creator<ExpandProperties> CREATOR = new JSController.ExpandProperties.1();
    public int actualHeightRequested;
    public int actualWidthRequested;
    public int bottomStuff;
    public boolean checkFlag;
    public int currentX;
    public int currentY;
    public int height;
    public boolean isModal;
    public boolean lockOrientation;
    public String orientation;
    public int portraitHeightRequested;
    public int portraitWidthRequested;
    public String rotationAtExpand;
    public int topStuff;
    public boolean useCustomClose;
    public int width;
    public int x;
    public int y;
    public boolean zeroWidthHeight;

    public ExpandProperties()
    {
      this.width = 0;
      this.height = 0;
      this.x = -1;
      this.y = -1;
      this.useCustomClose = false;
      this.isModal = true;
      this.lockOrientation = false;
      this.orientation = "";
      this.actualWidthRequested = 0;
      this.actualHeightRequested = 0;
      this.topStuff = 0;
      this.bottomStuff = 0;
      this.portraitWidthRequested = 0;
      this.portraitHeightRequested = 0;
      this.zeroWidthHeight = false;
      this.rotationAtExpand = "";
      this.checkFlag = true;
      this.currentX = 0;
      this.currentY = 0;
    }

    protected ExpandProperties(Parcel paramParcel)
    {
      super();
    }

    public void reinitializeExpandProperties()
    {
      this.width = 0;
      this.height = 0;
      this.x = -1;
      this.y = -1;
      this.useCustomClose = false;
      this.isModal = true;
      this.lockOrientation = false;
      this.orientation = "";
      this.actualWidthRequested = 0;
      this.actualHeightRequested = 0;
      this.topStuff = 0;
      this.bottomStuff = 0;
      this.portraitWidthRequested = 0;
      this.portraitHeightRequested = 0;
      this.zeroWidthHeight = false;
      this.rotationAtExpand = "";
      this.checkFlag = true;
      this.currentX = 0;
      this.currentY = 0;
    }
  }

  public static class PlayerProperties extends JSController.ReflectedParcelable
  {
    public static final Parcelable.Creator<PlayerProperties> CREATOR = new JSController.PlayerProperties.1();
    public boolean audioMuted;
    public boolean autoPlay;
    public boolean doLoop;
    public String id;
    public boolean showControl;
    public String startStyle;
    public String stopStyle;

    public PlayerProperties()
    {
      this.showControl = true;
      this.autoPlay = true;
      this.audioMuted = false;
      this.doLoop = false;
      this.stopStyle = "normal";
      this.startStyle = "normal";
      this.id = "";
    }

    public PlayerProperties(Parcel paramParcel)
    {
      super();
    }

    public boolean doLoop()
    {
      return this.doLoop;
    }

    public boolean doMute()
    {
      return this.audioMuted;
    }

    public boolean exitOnComplete()
    {
      return this.stopStyle.equalsIgnoreCase("exit");
    }

    public boolean isAutoPlay()
    {
      return this.autoPlay;
    }

    public boolean isFullScreen()
    {
      return this.startStyle.equalsIgnoreCase("fullscreen");
    }

    public void setFullScreen()
    {
      this.startStyle = "fullscreen";
    }

    public void setProperties(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, String paramString1, String paramString2, String paramString3)
    {
      this.autoPlay = paramBoolean2;
      this.showControl = paramBoolean3;
      this.doLoop = paramBoolean4;
      this.audioMuted = paramBoolean1;
      this.startStyle = paramString1;
      this.stopStyle = paramString2;
      this.id = paramString3;
    }

    public void setStopStyle(String paramString)
    {
      this.stopStyle = paramString;
    }

    public boolean showControl()
    {
      return this.showControl;
    }
  }

  public static class Properties extends JSController.ReflectedParcelable
  {
    public static final Parcelable.Creator<Properties> CREATOR = new JSController.Properties.1();
    public int backgroundColor;
    public float backgroundOpacity;
    public boolean useBackground;

    public Properties()
    {
      this.useBackground = false;
      this.backgroundColor = 0;
      this.backgroundOpacity = 0.0F;
    }

    protected Properties(Parcel paramParcel)
    {
      super();
    }
  }

  public static class ReflectedParcelable
    implements Parcelable
  {
    public ReflectedParcelable()
    {
    }

    protected ReflectedParcelable(Parcel paramParcel)
    {
      Field[] arrayOfField = getClass().getDeclaredFields();
      for (int i = 0; ; i++)
        try
        {
          if (i >= arrayOfField.length)
            return;
          localField = arrayOfField[i];
          Class localClass = localField.getType();
          if (localClass.isEnum())
          {
            String str = localClass.toString();
            if (str.equals("class com.mraid.NavigationStringEnum"))
              localField.set(this, NavigationStringEnum.fromString(paramParcel.readString()));
            else if (str.equals("class com.mraid.TransitionStringEnum"))
              localField.set(this, TransitionStringEnum.fromString(paramParcel.readString()));
          }
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
          Field localField;
          localIllegalArgumentException.printStackTrace();
          return;
          if (!(localField.get(this) instanceof Parcelable.Creator))
            localField.set(this, paramParcel.readValue(null));
        }
        catch (IllegalAccessException localIllegalAccessException)
        {
          localIllegalAccessException.printStackTrace();
          return;
        }
    }

    public int describeContents()
    {
      return 0;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      Field[] arrayOfField = getClass().getDeclaredFields();
      for (int i = 0; ; i++)
        try
        {
          if (i >= arrayOfField.length)
            return;
          localField = arrayOfField[i];
          Class localClass = localField.getType();
          if (localClass.isEnum())
          {
            String str = localClass.toString();
            if (str.equals("class com.mraid.NavigationStringEnum"))
              paramParcel.writeString(((NavigationStringEnum)localField.get(this)).getText());
            else if (str.equals("class com.mraid.TransitionStringEnum"))
              paramParcel.writeString(((TransitionStringEnum)localField.get(this)).getText());
          }
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
          Field localField;
          localIllegalArgumentException.printStackTrace();
          return;
          Object localObject = localField.get(this);
          if (!(localObject instanceof Parcelable.Creator))
            paramParcel.writeValue(localObject);
        }
        catch (IllegalAccessException localIllegalAccessException)
        {
          localIllegalAccessException.printStackTrace();
          return;
        }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.ai.controller.JSController
 * JD-Core Version:    0.6.2
 */