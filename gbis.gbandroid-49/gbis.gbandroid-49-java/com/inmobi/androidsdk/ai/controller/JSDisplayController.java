package com.inmobi.androidsdk.ai.controller;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.URLUtil;
import com.inmobi.androidsdk.ai.container.IMWebView;
import com.inmobi.androidsdk.ai.container.IMWebView.ViewState;
import com.inmobi.androidsdk.impl.Constants;
import org.json.JSONObject;

public class JSDisplayController extends JSController
{
  private WindowManager a;
  private float b;

  public JSDisplayController(IMWebView paramIMWebView, Context paramContext)
  {
    super(paramIMWebView, paramContext);
    DisplayMetrics localDisplayMetrics = new DisplayMetrics();
    this.a = ((WindowManager)paramContext.getSystemService("window"));
    this.a.getDefaultDisplay().getMetrics(localDisplayMetrics);
    this.b = ((Activity)this.mContext).getResources().getDisplayMetrics().density;
  }

  private JSController.ExpandProperties a(JSController.ExpandProperties paramExpandProperties)
  {
    Display localDisplay = this.a.getDefaultDisplay();
    int i = ((Activity)this.mContext).getResources().getDisplayMetrics().widthPixels;
    int j = ((Activity)this.mContext).getResources().getDisplayMetrics().heightPixels;
    View localView = ((Activity)this.mContext).getWindow().findViewById(16908290);
    paramExpandProperties.topStuff = localView.getTop();
    paramExpandProperties.bottomStuff = (j - localView.getBottom());
    int k;
    if (Build.VERSION.SDK_INT >= 8)
    {
      k = localDisplay.getRotation();
      if (this.imWebView.getWhetherTablet(k, i, j))
      {
        k++;
        if (k > 3)
          k = 0;
        this.imWebView.isTablet = true;
      }
      int m = k;
      if (Constants.DEBUG)
      {
        Log.d("InMobiAndroidSDK_3.5.2", "Device current rotation: " + m);
        Log.d("InMobiAndroidSDK_3.5.2", "Density of device: " + this.b);
      }
      paramExpandProperties.width = ((int)(paramExpandProperties.width * this.b));
      paramExpandProperties.height = ((int)(paramExpandProperties.height * this.b));
      paramExpandProperties.x = ((int)(paramExpandProperties.x * this.b));
      paramExpandProperties.y = ((int)(paramExpandProperties.y * this.b));
      paramExpandProperties.currentX = 0;
      paramExpandProperties.currentY = 0;
      this.imWebView.publisherOrientation = ((Activity)this.imWebView.getContext()).getRequestedOrientation();
      if ((m != 0) && (m != 2))
        break label906;
      paramExpandProperties.rotationAtExpand = "portrait";
      label297: paramExpandProperties.checkFlag = false;
      if ((paramExpandProperties.lockOrientation) && (this.imWebView.publisherOrientation == -1) && (paramExpandProperties.rotationAtExpand.equals(paramExpandProperties.orientation)))
        paramExpandProperties.checkFlag = this.imWebView.adCreativeLocksOrientation(paramExpandProperties, m);
      if ((paramExpandProperties.lockOrientation) && (this.imWebView.publisherOrientation == 0) && (paramExpandProperties.orientation.equals("landscape")))
        paramExpandProperties.checkFlag = true;
      if ((paramExpandProperties.lockOrientation) && (this.imWebView.publisherOrientation == 1) && (paramExpandProperties.orientation.equals("portrait")))
        paramExpandProperties.checkFlag = true;
      if (!paramExpandProperties.lockOrientation)
        paramExpandProperties.checkFlag = true;
      if (paramExpandProperties.checkFlag)
      {
        if ((paramExpandProperties.height <= 0) || (paramExpandProperties.width <= 0))
        {
          paramExpandProperties.height = j;
          paramExpandProperties.width = i;
          paramExpandProperties.zeroWidthHeight = true;
        }
        if ((m != 0) && (m != 2))
          break label915;
        paramExpandProperties.portraitWidthRequested = paramExpandProperties.width;
      }
    }
    for (paramExpandProperties.portraitHeightRequested = paramExpandProperties.height; ; paramExpandProperties.portraitHeightRequested = paramExpandProperties.width)
    {
      if (Constants.DEBUG)
        Log.d("InMobiAndroidSDK_3.5.2", " Device Width: " + i + " Device height: " + j);
      int n = j - paramExpandProperties.topStuff;
      if (paramExpandProperties.width > i)
        paramExpandProperties.width = i;
      if (paramExpandProperties.height > n)
        paramExpandProperties.height = n;
      int[] arrayOfInt = new int[2];
      this.imWebView.getLocationOnScreen(arrayOfInt);
      if (paramExpandProperties.x < 0)
        paramExpandProperties.x = arrayOfInt[0];
      if (paramExpandProperties.y < 0)
      {
        paramExpandProperties.y = (arrayOfInt[1] - paramExpandProperties.topStuff);
        if (Constants.DEBUG)
          Log.d("InMobiAndroidSDK_3.5.2", "topStuff: " + paramExpandProperties.topStuff + " ,bottomStuff: " + paramExpandProperties.bottomStuff);
      }
      if (Constants.DEBUG)
        Log.d("InMobiAndroidSDK_3.5.2", "loc 0: " + arrayOfInt[0] + " loc 1: " + arrayOfInt[1]);
      int i1 = i - (paramExpandProperties.x + paramExpandProperties.width);
      if (i1 < 0)
      {
        paramExpandProperties.x = (i1 + paramExpandProperties.x);
        if (paramExpandProperties.x < 0)
        {
          paramExpandProperties.width += paramExpandProperties.x;
          paramExpandProperties.x = 0;
        }
      }
      int i2 = n - (paramExpandProperties.y + paramExpandProperties.height);
      if (i2 < 0)
      {
        paramExpandProperties.y = (i2 + paramExpandProperties.y);
        if (paramExpandProperties.y < 0)
        {
          paramExpandProperties.height += paramExpandProperties.y;
          paramExpandProperties.y = 0;
        }
      }
      paramExpandProperties.currentX = paramExpandProperties.x;
      paramExpandProperties.currentY = paramExpandProperties.y;
      if (Constants.DEBUG)
        Log.d("InMobiAndroidSDK_3.5.2", "final expanded width after density : " + paramExpandProperties.width + "final expanded height after density " + paramExpandProperties.height + "portrait width requested :" + paramExpandProperties.portraitWidthRequested + "portrait height requested :" + paramExpandProperties.portraitHeightRequested);
      return paramExpandProperties;
      k = localDisplay.getOrientation();
      break;
      label906: paramExpandProperties.rotationAtExpand = "landscape";
      break label297;
      label915: paramExpandProperties.portraitWidthRequested = paramExpandProperties.height;
    }
  }

  private void a(JSController.ExpandProperties paramExpandProperties1, JSController.ExpandProperties paramExpandProperties2)
  {
    paramExpandProperties1.width = paramExpandProperties2.width;
    paramExpandProperties1.height = paramExpandProperties2.height;
    paramExpandProperties1.x = paramExpandProperties2.x;
    paramExpandProperties1.y = paramExpandProperties2.y;
    paramExpandProperties1.actualWidthRequested = paramExpandProperties2.actualWidthRequested;
    paramExpandProperties1.actualHeightRequested = paramExpandProperties2.actualHeightRequested;
    paramExpandProperties1.lockOrientation = paramExpandProperties2.lockOrientation;
    paramExpandProperties1.isModal = paramExpandProperties2.isModal;
    paramExpandProperties1.useCustomClose = paramExpandProperties2.useCustomClose;
    paramExpandProperties1.orientation = paramExpandProperties2.orientation;
    paramExpandProperties1.topStuff = paramExpandProperties2.topStuff;
    paramExpandProperties1.bottomStuff = paramExpandProperties2.bottomStuff;
    paramExpandProperties1.portraitWidthRequested = paramExpandProperties2.portraitWidthRequested;
    paramExpandProperties1.portraitHeightRequested = paramExpandProperties2.portraitHeightRequested;
    paramExpandProperties1.zeroWidthHeight = paramExpandProperties2.zeroWidthHeight;
    paramExpandProperties1.rotationAtExpand = paramExpandProperties2.rotationAtExpand;
    paramExpandProperties1.checkFlag = paramExpandProperties2.checkFlag;
    paramExpandProperties1.currentX = paramExpandProperties2.currentX;
    paramExpandProperties1.currentY = paramExpandProperties2.currentY;
  }

  JSController.Dimensions a(JSController.Dimensions paramDimensions)
  {
    Display localDisplay = this.a.getDefaultDisplay();
    int i = localDisplay.getWidth();
    int j = localDisplay.getHeight();
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "Width: " + i + " height: " + j);
    paramDimensions.width = ((int)(paramDimensions.width * this.b));
    paramDimensions.height = ((int)(paramDimensions.height * this.b));
    paramDimensions.x = ((int)(paramDimensions.x * this.b));
    paramDimensions.y = ((int)(paramDimensions.y * this.b));
    if (paramDimensions.height < 0)
      paramDimensions.height = this.imWebView.getHeight();
    if (paramDimensions.width < 0)
      paramDimensions.width = this.imWebView.getWidth();
    int[] arrayOfInt = new int[2];
    this.imWebView.getLocationOnScreen(arrayOfInt);
    if (paramDimensions.x < 0)
      paramDimensions.x = arrayOfInt[0];
    int m;
    int k;
    if (paramDimensions.y < 0)
    {
      View localView = ((Activity)this.mContext).findViewById(16908290);
      m = localView.getTop();
      k = j - localView.getBottom();
      paramDimensions.y = (arrayOfInt[1] - m);
      if (Constants.DEBUG)
        Log.d("InMobiAndroidSDK_3.5.2", "topStuff: " + m + " ,bottomStuff: " + k);
    }
    while (true)
    {
      if (Constants.DEBUG)
        Log.d("InMobiAndroidSDK_3.5.2", "loc 0: " + arrayOfInt[0] + " loc 1: " + arrayOfInt[1]);
      int n = i - (paramDimensions.x + paramDimensions.width);
      if (n < 0)
        paramDimensions.x = (n + paramDimensions.x);
      if (paramDimensions.x < 0)
        paramDimensions.x = 0;
      int i1 = j - (paramDimensions.y + paramDimensions.height);
      if (i1 < 0)
      {
        paramDimensions.y = (i1 + paramDimensions.y);
        paramDimensions.y -= m;
        paramDimensions.y -= k;
      }
      if (paramDimensions.y < 0)
        paramDimensions.y = 0;
      return paramDimensions;
      k = 0;
      m = 0;
    }
  }

  public void close()
  {
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "JSDisplayController-> close");
    if (this.imWebView.mOriginalWebviewForExpandUrl != null)
    {
      this.imWebView.mOriginalWebviewForExpandUrl.close();
      return;
    }
    this.imWebView.close();
  }

  public void expand(String paramString)
  {
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "JSDisplayController-> expand: url: " + paramString);
    try
    {
      this.imWebView.useLockOrient = false;
      if (this.imWebView.getStateVariable() != IMWebView.ViewState.DEFAULT)
      {
        this.imWebView.injectJavaScript("window.mraidview.fireErrorEvent(\"Current state is not default\", \"expand\")");
        return;
      }
      if ((this.imWebView.getStateVariable() == IMWebView.ViewState.DEFAULT) && (this.imWebView.mIsInterstitialAd))
      {
        this.imWebView.injectJavaScript("window.mraidview.fireErrorEvent(\"Expand cannot be called on interstitial ad\", \"expand\")");
        return;
      }
    }
    catch (Exception localException)
    {
      if (Constants.DEBUG)
      {
        Log.d("InMobiAndroidSDK_3.5.2", "Exception while expanding the ad.", localException);
        return;
        a(this.temporaryexpProps, this.expProps);
        if (Constants.DEBUG)
          Log.d("InMobiAndroidSDK_3.5.2", "JSDisplayController-> At the time of expand the properties are: tempexpProps.width: " + this.temporaryexpProps.width + "tempexpProps.height: " + this.temporaryexpProps.height + "tempexpProps.orientation: " + this.temporaryexpProps.orientation + "tempexpProps.boolean1: " + this.temporaryexpProps.lockOrientation + "tempexpProps.boolean2: " + this.temporaryexpProps.isModal + "tempexpProps.boolean3: " + this.temporaryexpProps.useCustomClose);
        this.imWebView.expand(paramString, a(this.temporaryexpProps));
      }
    }
  }

  public String getOrientation()
  {
    String str;
    try
    {
      str = this.imWebView.getCurrentRotation(this.imWebView.getIntegerCurrentRotation());
      if (Constants.DEBUG)
        Log.d("InMobiAndroidSDK_3.5.2", "JSDisplayController-> getOrientation: " + str);
      return str;
    }
    catch (Exception localException)
    {
      do
        str = "-1";
      while (!Constants.DEBUG);
      Log.d("InMobiAndroidSDK_3.5.2", "JSDisplayController-> Error getOrientation: " + str);
    }
    return str;
  }

  public String getPlacementType()
  {
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "JSDisplayController-> getPlacementType ");
    return this.imWebView.getPlacementType();
  }

  public String getState()
  {
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "JSDisplayController-> getState ");
    return this.imWebView.getState();
  }

  public boolean isViewable()
  {
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "JSDisplayController-> isViewable ");
    return this.imWebView.isViewable();
  }

  public void open(String paramString)
  {
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "JSDisplayController-> open: url: " + paramString);
    if (!URLUtil.isValidUrl(paramString))
    {
      this.imWebView.raiseError("Invalid url", "open");
      return;
    }
    this.imWebView.openURL(paramString);
  }

  public void setExpandProperties(String paramString)
  {
    try
    {
      this.expProps = ((JSController.ExpandProperties)getFromJSON(new JSONObject(paramString), JSController.ExpandProperties.class));
      if (Constants.DEBUG)
        Log.d("InMobiAndroidSDK_3.5.2", "JSDisplayController-> ExpandProperties is set: expProps.width: " + this.expProps.width + "expProps.height: " + this.expProps.height + "expProps.orientation: " + this.expProps.orientation + "expProps.boolean1: " + this.expProps.lockOrientation + "expProps.boolean2: " + this.expProps.isModal + "expProps.boolean3: " + this.expProps.useCustomClose);
      this.imWebView.setExpandPropertiesForInterstitial(this.expProps.useCustomClose, this.expProps.lockOrientation, this.expProps.orientation);
      return;
    }
    catch (Exception localException)
    {
      while (!Constants.DEBUG);
      Log.d("InMobiAndroidSDK_3.5.2", "Exception while setting the expand properties", localException);
    }
  }

  public void stopAllListeners()
  {
  }

  public void useCustomClose(boolean paramBoolean)
  {
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "JSDisplayController-> useCustomClose" + paramBoolean);
    this.imWebView.setCustomClose(paramBoolean);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.ai.controller.JSDisplayController
 * JD-Core Version:    0.6.2
 */