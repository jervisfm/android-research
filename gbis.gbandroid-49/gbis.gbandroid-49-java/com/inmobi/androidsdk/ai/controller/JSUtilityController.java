package com.inmobi.androidsdk.ai.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build.VERSION;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.URLUtil;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import com.inmobi.androidsdk.ai.container.IMWebView;
import com.inmobi.androidsdk.impl.Constants;
import com.inmobi.androidsdk.impl.net.RequestResponseManager;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JSUtilityController extends JSController
{
  private JSAssetController a;
  private JSDisplayController b;

  public JSUtilityController(IMWebView paramIMWebView, Context paramContext)
  {
    super(paramIMWebView, paramContext);
    this.a = new JSAssetController(paramIMWebView, paramContext);
    this.b = new JSDisplayController(paramIMWebView, paramContext);
    paramIMWebView.addJavascriptInterface(this.b, "displayController");
  }

  private String a(String paramString)
  {
    if (TextUtils.isEmpty(paramString))
      paramString = null;
    while (paramString.startsWith("tel:"))
      return paramString;
    StringBuilder localStringBuilder = new StringBuilder("tel:");
    localStringBuilder.append(paramString);
    return localStringBuilder.toString();
  }

  private void a(int paramInt, String paramString1, String paramString2, String paramString3)
  {
    ContentResolver localContentResolver = this.mContext.getContentResolver();
    long l1 = Long.parseLong(paramString1);
    long l2 = 3600000L + l1;
    ContentValues localContentValues1 = new ContentValues();
    localContentValues1.put("calendar_id", Integer.valueOf(paramInt));
    localContentValues1.put("title", paramString2);
    localContentValues1.put("description", paramString3);
    localContentValues1.put("dtstart", Long.valueOf(l1));
    localContentValues1.put("hasAlarm", Integer.valueOf(1));
    localContentValues1.put("dtend", Long.valueOf(l2));
    Uri localUri;
    ContentValues localContentValues2;
    if (Integer.parseInt(Build.VERSION.SDK) == 8)
    {
      localUri = localContentResolver.insert(Uri.parse("content://com.android.calendar/events"), localContentValues1);
      if (localUri != null)
      {
        long l3 = Long.parseLong(localUri.getLastPathSegment());
        localContentValues2 = new ContentValues();
        localContentValues2.put("event_id", Long.valueOf(l3));
        localContentValues2.put("method", Integer.valueOf(1));
        localContentValues2.put("minutes", Integer.valueOf(15));
        if (Integer.parseInt(Build.VERSION.SDK) != 8)
          break label234;
        localContentResolver.insert(Uri.parse("content://com.android.calendar/reminders"), localContentValues2);
      }
    }
    while (true)
    {
      Toast.makeText(this.mContext, "Event added to calendar", 0).show();
      return;
      localUri = localContentResolver.insert(Uri.parse("content://calendar/events"), localContentValues1);
      break;
      label234: localContentResolver.insert(Uri.parse("content://calendar/reminders"), localContentValues2);
    }
  }

  public void activate(String paramString)
  {
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "JSUtilityController-> activate: " + paramString);
  }

  public void asyncPing(String paramString)
  {
    try
    {
      if (Constants.DEBUG)
        Log.d("InMobiAndroidSDK_3.5.2", "JSUtilityController-> asyncPing: url: " + paramString);
      if (!URLUtil.isValidUrl(paramString))
      {
        this.imWebView.raiseError("Invalid url", "asyncPing");
        return;
      }
      new RequestResponseManager(this.imWebView.getExpandedActivity()).asyncPing(paramString);
      return;
    }
    catch (Exception localException)
    {
    }
  }

  public void closeVideo(String paramString)
  {
    Log.d("InMobiAndroidSDK_3.5.2", "JSUtilityController-> closeVideo: id :" + paramString);
    this.imWebView.closeVideo(paramString);
  }

  public String copyTextFromJarIntoAssetDir(String paramString1, String paramString2)
  {
    return this.a.copyTextFromJarIntoAssetDir(paramString1, paramString2);
  }

  public void createEvent(String paramString1, String paramString2, String paramString3)
  {
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "JSUtilityController-> createEvent: date: " + paramString1 + " title: " + paramString2 + " body: " + paramString3);
    ContentResolver localContentResolver = this.mContext.getContentResolver();
    String[] arrayOfString = { "_id", "displayName", "_sync_account" };
    if (Integer.parseInt(Build.VERSION.SDK) == 8);
    for (Cursor localCursor = localContentResolver.query(Uri.parse("content://com.android.calendar/calendars"), arrayOfString, null, null, null); (localCursor == null) || ((localCursor != null) && (!localCursor.moveToFirst())); localCursor = localContentResolver.query(Uri.parse("content://calendar/calendars"), arrayOfString, null, null, null))
    {
      Toast.makeText(this.mContext, "No calendar account found", 1).show();
      if (localCursor != null)
        localCursor.close();
      return;
    }
    if (localCursor.getCount() == 1)
    {
      a(localCursor.getInt(0), paramString1, paramString2, paramString3);
      localCursor.close();
      return;
    }
    ArrayList localArrayList = new ArrayList();
    for (int i = 0; ; i++)
    {
      if (i >= localCursor.getCount())
      {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this.mContext);
        localBuilder.setTitle("Choose Calendar to save event to");
        localBuilder.setSingleChoiceItems(new SimpleAdapter(this.mContext, localArrayList, 17367053, new String[] { "NAME", "EMAILID" }, new int[] { 16908308, 16908309 }), -1, new JSUtilityController.1(this, localArrayList, paramString1, paramString2, paramString3));
        localBuilder.create().show();
        break;
      }
      HashMap localHashMap = new HashMap();
      localHashMap.put("ID", localCursor.getString(0));
      localHashMap.put("NAME", localCursor.getString(1));
      localHashMap.put("EMAILID", localCursor.getString(2));
      localArrayList.add(localHashMap);
      localCursor.moveToNext();
    }
  }

  public void deactivate(String paramString)
  {
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "JSUtilityController-> deactivate: " + paramString);
  }

  public void deleteOldAds()
  {
    this.a.deleteOldAds();
  }

  public int getAudioVolume(String paramString)
  {
    Log.d("InMobiAndroidSDK_3.5.2", "JSUtilityController-> getAudioVolume: ");
    return this.imWebView.getAudioVolume(paramString);
  }

  public int getVideoVolume(String paramString)
  {
    Log.d("InMobiAndroidSDK_3.5.2", "JSUtilityController-> getVideoVolume: ");
    return this.imWebView.getVideoVolume(paramString);
  }

  public void hideVideo(String paramString)
  {
    Log.d("InMobiAndroidSDK_3.5.2", "JSUtilityController-> hideVideo: id :" + paramString);
    this.imWebView.hideVideo(paramString);
  }

  public boolean isAudioMuted(String paramString)
  {
    Log.d("InMobiAndroidSDK_3.5.2", "JSUtilityController-> isAudioMuted: ");
    return this.imWebView.isAudioMuted(paramString);
  }

  public boolean isVideoMuted(String paramString)
  {
    Log.d("InMobiAndroidSDK_3.5.2", "JSUtilityController-> isVideoMuted: ");
    return this.imWebView.isVideoMuted(paramString);
  }

  public void log(String paramString)
  {
    if (Constants.DEBUG)
      Log.v("InMobiAndroidSDK_3.5.2", "Ad Log Message: " + paramString);
  }

  public void makeCall(String paramString)
  {
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "JSUtilityController-> makeCall: number: " + paramString);
    String str;
    try
    {
      if (this.mContext.checkCallingOrSelfPermission("android.permission.CALL_PHONE") == -1)
      {
        if (!Constants.DEBUG)
          return;
        Log.d("InMobiAndroidSDK_3.5.2", "No Permisson to make call");
        this.imWebView.raiseError("No Permisson to make call", "makeCall");
        return;
      }
      str = a(paramString);
      if (str == null)
      {
        this.imWebView.raiseError("Bad Phone Number", "makeCall");
        return;
      }
    }
    catch (Exception localException)
    {
      if (Constants.DEBUG)
        Log.d("InMobiAndroidSDK_3.5.2", "Exception in making call", localException);
      this.imWebView.raiseError("Exception in making call", "makeCall");
      return;
    }
    Intent localIntent = new Intent("android.intent.action.CALL", Uri.parse(str.toString()));
    localIntent.addFlags(268435456);
    this.imWebView.getExpandedActivity().startActivity(localIntent);
    this.imWebView.fireOnLeaveApplication();
  }

  public void muteAudio(String paramString)
  {
    Log.d("InMobiAndroidSDK_3.5.2", "JSUtilityController-> muteAudio: ");
    this.imWebView.muteAudio(paramString);
  }

  public void muteVideo(String paramString)
  {
    Log.d("InMobiAndroidSDK_3.5.2", "JSUtilityController-> muteVideo: ");
    this.imWebView.muteVideo(paramString);
  }

  public void pauseAudio(String paramString)
  {
    Log.d("InMobiAndroidSDK_3.5.2", "JSUtilityController-> pauseAudio: id :" + paramString);
    this.imWebView.pauseAudio(paramString);
  }

  public void pauseVideo(String paramString)
  {
    Log.d("InMobiAndroidSDK_3.5.2", "JSUtilityController-> pauseVideo: id :" + paramString);
    this.imWebView.pauseVideo(paramString);
  }

  public void playAudio(String paramString1, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, String paramString2, String paramString3, String paramString4)
  {
    Log.d("InMobiAndroidSDK_3.5.2", "playAudio: url: " + paramString1 + " autoPlay: " + paramBoolean1 + " controls: " + paramBoolean2 + " loop: " + paramBoolean3 + " startStyle: " + paramString2 + " stopStyle: " + paramString3 + " id:" + paramString4);
    this.imWebView.playAudio(paramString1, paramBoolean1, paramBoolean2, paramBoolean3, paramString2, paramString3, paramString4);
  }

  public void playVideo(String paramString1, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8)
  {
    Log.d("InMobiAndroidSDK_3.5.2", "JSUtilityController-> playVideo: url: " + paramString1 + " audioMuted: " + paramBoolean1 + " autoPlay: " + paramBoolean2 + " controls: " + paramBoolean3 + " loop: " + paramBoolean4 + " x: " + paramString2 + " y: " + paramString3 + " width: " + paramString4 + " height: " + paramString5 + " startStyle: " + paramString6 + " stopStyle: " + paramString7 + " id:" + paramString8);
    JSController.Dimensions localDimensions = new JSController.Dimensions();
    localDimensions.x = Integer.parseInt(paramString2);
    localDimensions.y = Integer.parseInt(paramString3);
    localDimensions.width = Integer.parseInt(paramString4);
    localDimensions.height = Integer.parseInt(paramString5);
    this.imWebView.playVideo(paramString1, paramBoolean1, paramBoolean2, paramBoolean3, paramBoolean4, localDimensions, paramString6, paramString7, paramString8);
  }

  public void seekAudio(String paramString, int paramInt)
  {
    Log.d("InMobiAndroidSDK_3.5.2", "JSUtilityController-> seekAudio: ");
    this.imWebView.seekAudio(paramString, paramInt);
  }

  public void seekVideo(String paramString, int paramInt)
  {
    Log.d("InMobiAndroidSDK_3.5.2", "JSUtilityController-> seekVideo: ");
    this.imWebView.seekVideo(paramString, paramInt);
  }

  public void sendMail(String paramString1, String paramString2, String paramString3)
  {
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "JSUtilityController-> sendMail: recipient: " + paramString1 + " subject: " + paramString2 + " body: " + paramString3);
    try
    {
      Intent localIntent1 = new Intent("android.intent.action.SEND");
      localIntent1.setType("plain/text");
      localIntent1.putExtra("android.intent.extra.EMAIL", new String[] { paramString1 });
      localIntent1.putExtra("android.intent.extra.SUBJECT", paramString2);
      localIntent1.putExtra("android.intent.extra.TEXT", paramString3);
      localIntent1.addFlags(268435456);
      Intent localIntent2 = Intent.createChooser(localIntent1, "Choose the Email Client.");
      if (Constants.DEBUG)
        Log.d("InMobiAndroidSDK_3.5.2", "target Intent: " + localIntent2);
      this.imWebView.getExpandedActivity().startActivity(localIntent2);
      this.imWebView.fireOnLeaveApplication();
      return;
    }
    catch (Exception localException)
    {
      if (Constants.DEBUG)
        Log.d("InMobiAndroidSDK_3.5.2", "Exception in sending mail", localException);
      this.imWebView.raiseError("Exception in sending mail", "sendMail");
    }
  }

  public void sendSMS(String paramString1, String paramString2)
  {
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "JSUtilityController-> sendSMS: recipient: " + paramString1 + " body: " + paramString2);
    try
    {
      Intent localIntent = new Intent("android.intent.action.VIEW");
      localIntent.putExtra("address", paramString1);
      localIntent.putExtra("sms_body", paramString2);
      localIntent.setType("vnd.android-dir/mms-sms");
      localIntent.addFlags(268435456);
      this.imWebView.getExpandedActivity().startActivity(localIntent);
      this.imWebView.fireOnLeaveApplication();
      return;
    }
    catch (Exception localException)
    {
      if (Constants.DEBUG)
        Log.d("InMobiAndroidSDK_3.5.2", "Exception in sending SMS", localException);
      this.imWebView.raiseError("Exception in sending SMS", "sendSMS");
    }
  }

  public void setAudioVolume(String paramString, int paramInt)
  {
    Log.d("InMobiAndroidSDK_3.5.2", "JSUtilityController-> setAudioVolume: ");
    this.imWebView.setAudioVolume(paramString, paramInt);
  }

  public void setVideoVolume(String paramString, int paramInt)
  {
    Log.d("InMobiAndroidSDK_3.5.2", "JSUtilityController-> setVideoVolume: ");
    this.imWebView.setVideoVolume(paramString, paramInt);
  }

  public void showAlert(String paramString)
  {
    Log.e("InMobiAndroidSDK_3.5.2", paramString);
  }

  public void showVideo(String paramString)
  {
    Log.d("InMobiAndroidSDK_3.5.2", "JSUtilityController-> showVideo: id :" + paramString);
    this.imWebView.showVideo(paramString);
  }

  public void stopAllListeners()
  {
    try
    {
      this.a.stopAllListeners();
      this.b.stopAllListeners();
      return;
    }
    catch (Exception localException)
    {
    }
  }

  public void unMuteAudio(String paramString)
  {
    Log.d("InMobiAndroidSDK_3.5.2", "JSUtilityController-> unMuteAudio: ");
    this.imWebView.unMuteAudio(paramString);
  }

  public void unMuteVideo(String paramString)
  {
    Log.d("InMobiAndroidSDK_3.5.2", "JSUtilityController-> unMuteVideo: ");
    this.imWebView.unMuteVideo(paramString);
  }

  public String writeToDiskWrap(InputStream paramInputStream, String paramString1, boolean paramBoolean, String paramString2, String paramString3, String paramString4)
  {
    return this.a.writeToDiskWrap(paramInputStream, paramString1, paramBoolean, paramString2, paramString3, paramString4);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.ai.controller.JSUtilityController
 * JD-Core Version:    0.6.2
 */