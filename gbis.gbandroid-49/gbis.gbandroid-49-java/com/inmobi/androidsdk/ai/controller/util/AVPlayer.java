package com.inmobi.androidsdk.ai.controller.util;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Handler;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.VideoView;
import com.inmobi.androidsdk.ai.container.IMWebView;
import com.inmobi.androidsdk.ai.controller.JSController.Dimensions;
import com.inmobi.androidsdk.ai.controller.JSController.PlayerProperties;
import com.inmobi.androidsdk.impl.Constants;
import com.inmobi.androidsdk.impl.Constants.playerState;

public class AVPlayer extends VideoView
  implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener
{
  private static final int a = 1001;
  private static String g = "play";
  private static String h = "pause";
  private static String i = "ended";
  private static int j = -1;
  private static int k = 2;
  private static String l = "Loading. Please Wait..";
  private JSController.PlayerProperties b;
  private AudioManager c = (AudioManager)getContext().getSystemService("audio");
  private AVPlayerListener d;
  private String e;
  private RelativeLayout f;
  private boolean m = false;
  private boolean n = false;
  private IMWebView o;
  private int p;
  private int q;
  private int r = -1;
  private Constants.playerState s;
  private MediaPlayer t;
  private boolean u;
  private ViewGroup v;
  private JSController.Dimensions w;
  private Handler x = new AVPlayer.1(this);

  public AVPlayer(Context paramContext, IMWebView paramIMWebView)
  {
    super(paramContext);
    this.o = paramIMWebView;
    setFocusable(true);
    setFocusableInTouchMode(true);
    this.p = this.c.getStreamVolume(3);
    this.q = this.p;
    getHolder().addCallback(new AVPlayer.2(this));
  }

  private void a(int paramInt)
  {
    if (this.o != null)
      this.o.injectJavaScript("window.mraidview.fireMediaErrorEvent('" + this.b.id + "'," + paramInt + ");");
  }

  private void a(int paramInt1, int paramInt2)
  {
    if (this.o != null)
      this.o.injectJavaScript("window.mraidview.fireMediaTimeUpdateEvent('" + this.b.id + "'," + paramInt1 + "," + paramInt2 + ");");
  }

  private void a(String paramString)
  {
    if (this.o != null)
      this.o.injectJavaScript("window.mraidview.fireMediaTrackingEvent('" + paramString + "','" + this.b.id + "');");
  }

  private void a(boolean paramBoolean, int paramInt)
  {
    if (this.o != null)
      this.o.injectJavaScript("window.mraidview.fireMediaCloseEvent('" + this.b.id + "'," + paramBoolean + "," + paramInt + ");");
  }

  private int b(int paramInt)
  {
    return paramInt * this.c.getStreamMaxVolume(3) / 100;
  }

  private void g()
  {
    if (this.o != null)
      this.o.injectJavaScript("window.mraidview.fireMediaVolumeChangeEvent('" + this.b.id + "'," + getVolume() + "," + isMediaMuted() + ");");
  }

  private void h()
  {
    this.x.sendEmptyMessage(1001);
  }

  private void i()
  {
    this.x.removeMessages(1001);
  }

  private boolean j()
  {
    return (this.s == Constants.playerState.PAUSED) || (this.s == Constants.playerState.HIDDEN);
  }

  private boolean k()
  {
    return this.s == Constants.playerState.RELEASED;
  }

  private boolean l()
  {
    return this.s == Constants.playerState.PLAYING;
  }

  private void m()
  {
    this.c.setStreamVolume(3, this.q, 4);
  }

  void a()
  {
    if (this.b.showControl())
    {
      MediaController localMediaController = new MediaController(getContext());
      setMediaController(localMediaController);
      localMediaController.setAnchorView(this);
    }
  }

  void b()
  {
    this.e = this.e.trim();
    this.e = Utils.convert(this.e);
    this.s = Constants.playerState.INIT;
    e();
    setVideoPath(this.e);
    a();
    setOnCompletionListener(this);
    setOnErrorListener(this);
    setOnPreparedListener(this);
  }

  void c()
  {
    if (this.s == Constants.playerState.SHOWING)
      if (this.n)
      {
        localplayerState = Constants.playerState.COMPLETED;
        this.s = localplayerState;
      }
    while ((!this.b.isAutoPlay()) || (this.s != Constants.playerState.INIT))
      while (true)
      {
        return;
        Constants.playerState localplayerState = Constants.playerState.PAUSED;
      }
    start();
  }

  void d()
  {
    try
    {
      ViewGroup localViewGroup = (ViewGroup)getParent();
      if (localViewGroup != null)
        localViewGroup.removeView(this);
      return;
    }
    catch (Exception localException)
    {
    }
  }

  void e()
  {
    this.f = new RelativeLayout(getContext());
    this.f.setLayoutParams(getLayoutParams());
    this.f.setBackgroundColor(-16777216);
    TextView localTextView = new TextView(getContext());
    localTextView.setText(l);
    localTextView.setTextColor(-1);
    RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(-2, -2);
    localLayoutParams.addRule(13);
    this.f.addView(localTextView, localLayoutParams);
    ((ViewGroup)getParent()).addView(this.f);
  }

  void f()
  {
    if (this.f != null)
      ((ViewGroup)getParent()).removeView(this.f);
  }

  public ViewGroup getBackGroundLayout()
  {
    return this.v;
  }

  public String getMediaURL()
  {
    return this.e;
  }

  public JSController.Dimensions getPlayDimensions()
  {
    return this.w;
  }

  public JSController.PlayerProperties getProperties()
  {
    return this.b;
  }

  public String getPropertyID()
  {
    return this.b.id;
  }

  public Constants.playerState getState()
  {
    return this.s;
  }

  public int getVolume()
  {
    try
    {
      if (!isPlaying())
        this.p = this.c.getStreamVolume(3);
      int i1 = 100 * this.p / this.c.getStreamMaxVolume(3);
      return i1;
    }
    finally
    {
    }
  }

  public void hide()
  {
    try
    {
      if (isPlaying())
        pause();
      this.v.setVisibility(8);
      this.s = Constants.playerState.HIDDEN;
      return;
    }
    catch (Exception localException)
    {
    }
  }

  public boolean isInlineVideo()
  {
    return !this.b.isFullScreen();
  }

  public boolean isMediaMuted()
  {
    return (this.p == 0) || (this.u);
  }

  public void mute()
  {
    if ((this.t != null) && (!this.u))
      this.u = true;
    try
    {
      this.t.setVolume(0.0F, 0.0F);
      label28: g();
      return;
    }
    catch (Exception localException)
    {
      break label28;
    }
  }

  public void onCompletion(MediaPlayer paramMediaPlayer)
  {
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "AVPlayer-> onCompletion");
    this.s = Constants.playerState.COMPLETED;
    this.n = true;
    a(i);
    i();
    if (this.b.doLoop());
    while (!this.b.exitOnComplete())
      try
      {
        if (!j())
          start();
        return;
      }
      finally
      {
      }
    releasePlayer(false);
  }

  public boolean onError(MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2)
  {
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "AVPlayer-> Player error : " + paramInt1);
    f();
    releasePlayer(false);
    if (this.d != null)
      this.d.onError(this);
    int i1 = j;
    if (paramInt1 == 100)
      i1 = k;
    a(i1);
    return false;
  }

  public void onPrepared(MediaPlayer paramMediaPlayer)
  {
    this.t = paramMediaPlayer;
    if (this.u);
    try
    {
      this.t.setVolume(0.0F, 0.0F);
      label21: if (Constants.DEBUG)
        Log.d("InMobiAndroidSDK_3.5.2", "AVPlayer-> onPrepared");
      f();
      if (this.d != null)
        this.d.onPrepared(this);
      this.m = true;
      c();
      return;
    }
    catch (Exception localException)
    {
      break label21;
    }
  }

  protected void onWindowVisibilityChanged(int paramInt)
  {
    try
    {
      super.onWindowVisibilityChanged(paramInt);
      return;
    }
    catch (Exception localException)
    {
    }
  }

  public void pause()
  {
    try
    {
      if (this.s != null)
      {
        Constants.playerState localplayerState1 = this.s;
        Constants.playerState localplayerState2 = Constants.playerState.PAUSED;
        if (localplayerState1 != localplayerState2);
      }
      while (true)
      {
        return;
        super.pause();
        this.s = Constants.playerState.PAUSED;
        i();
        if (Constants.DEBUG)
          Log.d("InMobiAndroidSDK_3.5.2", "AVPlayer -> pause");
        a(h);
      }
    }
    finally
    {
    }
  }

  public void play()
  {
    if (this.b.doMute())
      mute();
    b();
  }

  public void releasePlayer(boolean paramBoolean)
  {
    while (true)
    {
      try
      {
        if (k())
          return;
        this.s = Constants.playerState.RELEASED;
        if (this.r != -1)
        {
          i1 = this.r;
          a(paramBoolean, i1);
          this.r = -1;
          i();
          unMute();
          m();
          stopPlayback();
          d();
          if (this.d == null)
            continue;
          this.d.onComplete(this);
          return;
        }
      }
      finally
      {
      }
      int i1 = Math.round(getCurrentPosition() / 1000);
    }
  }

  public void seekPlayer(int paramInt)
  {
    if (paramInt <= getDuration())
      seekTo(paramInt);
  }

  public void setBackGroundLayout(ViewGroup paramViewGroup)
  {
    this.v = paramViewGroup;
  }

  public void setListener(AVPlayerListener paramAVPlayerListener)
  {
    this.d = paramAVPlayerListener;
  }

  public void setPlayData(JSController.PlayerProperties paramPlayerProperties, String paramString)
  {
    this.b = paramPlayerProperties;
    this.e = paramString;
  }

  public void setPlayDimensions(JSController.Dimensions paramDimensions)
  {
    this.w = paramDimensions;
  }

  public void setVolume(int paramInt)
  {
    try
    {
      int i1 = b(paramInt);
      int i2 = this.p;
      if (i2 == i1);
      while (true)
      {
        return;
        this.p = i1;
        this.c.setStreamVolume(3, this.p, 4);
        g();
      }
    }
    finally
    {
    }
  }

  public void show()
  {
    this.s = Constants.playerState.SHOWING;
    this.v.setVisibility(0);
    setVisibility(0);
  }

  public void start()
  {
    try
    {
      if (this.s != null)
      {
        Constants.playerState localplayerState1 = this.s;
        Constants.playerState localplayerState2 = Constants.playerState.PLAYING;
        if (localplayerState1 != localplayerState2);
      }
      while (true)
      {
        return;
        super.start();
        this.s = Constants.playerState.PLAYING;
        this.n = false;
        h();
        if (Constants.DEBUG)
          Log.d("InMobiAndroidSDK_3.5.2", "AVPlayer -> start playing");
        if (this.m)
          a(g);
      }
    }
    finally
    {
    }
  }

  public void unMute()
  {
    if ((this.t != null) && (this.u))
      this.u = false;
    try
    {
      this.t.setVolume(0.0F, 1.0F);
      label28: g();
      return;
    }
    catch (Exception localException)
    {
      break label28;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.ai.controller.util.AVPlayer
 * JD-Core Version:    0.6.2
 */