package com.inmobi.androidsdk.ai.controller.util;

public abstract interface AVPlayerListener
{
  public abstract void onComplete(AVPlayer paramAVPlayer);

  public abstract void onError(AVPlayer paramAVPlayer);

  public abstract void onPrepared(AVPlayer paramAVPlayer);
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.ai.controller.util.AVPlayerListener
 * JD-Core Version:    0.6.2
 */