package com.inmobi.androidsdk.ai.controller.util;

import java.io.UnsupportedEncodingException;

public class Base64
{
  public static final int CRLF = 4;
  public static final int DEFAULT = 0;
  public static final int NO_CLOSE = 16;
  public static final int NO_PADDING = 1;
  public static final int NO_WRAP = 2;
  public static final int URL_SAFE = 8;

  static
  {
    if (!Base64.class.desiredAssertionStatus());
    for (boolean bool = true; ; bool = false)
    {
      a = bool;
      return;
    }
  }

  public static byte[] decode(String paramString, int paramInt)
  {
    return decode(paramString.getBytes(), paramInt);
  }

  public static byte[] decode(byte[] paramArrayOfByte, int paramInt)
  {
    return decode(paramArrayOfByte, 0, paramArrayOfByte.length, paramInt);
  }

  public static byte[] decode(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3)
  {
    b localb = new b(paramInt3, new byte[paramInt2 * 3 / 4]);
    if (!localb.a(paramArrayOfByte, paramInt1, paramInt2, true))
      throw new IllegalArgumentException("bad base-64");
    if (localb.b == localb.a.length)
      return localb.a;
    byte[] arrayOfByte = new byte[localb.b];
    System.arraycopy(localb.a, 0, arrayOfByte, 0, localb.b);
    return arrayOfByte;
  }

  public static byte[] encode(byte[] paramArrayOfByte, int paramInt)
  {
    return encode(paramArrayOfByte, 0, paramArrayOfByte.length, paramInt);
  }

  public static byte[] encode(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3)
  {
    c localc = new c(paramInt3, null);
    int i = 4 * (paramInt2 / 3);
    int j;
    if (localc.e)
    {
      if (paramInt2 % 3 > 0)
        i += 4;
      if ((localc.f) && (paramInt2 > 0))
      {
        j = 1 + (paramInt2 - 1) / 57;
        if (!localc.g)
          break label167;
      }
    }
    label167: for (int k = 2; ; k = 1)
    {
      i += k * j;
      localc.a = new byte[i];
      localc.a(paramArrayOfByte, paramInt1, paramInt2, true);
      if ((a) || (localc.b == i))
        break label173;
      throw new AssertionError();
      switch (paramInt2 % 3)
      {
      case 0:
      default:
        break;
      case 1:
        i += 2;
        break;
      case 2:
        i += 3;
        break;
      }
    }
    label173: return localc.a;
  }

  public static String encodeToString(byte[] paramArrayOfByte, int paramInt)
  {
    try
    {
      String str = new String(encode(paramArrayOfByte, paramInt), "US-ASCII");
      return str;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      throw new AssertionError(localUnsupportedEncodingException);
    }
  }

  public static String encodeToString(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3)
  {
    try
    {
      String str = new String(encode(paramArrayOfByte, paramInt1, paramInt2, paramInt3), "US-ASCII");
      return str;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      throw new AssertionError(localUnsupportedEncodingException);
    }
  }

  static abstract class a
  {
    public byte[] a;
    public int b;

    public abstract int a(int paramInt);

    public abstract boolean a(byte[] paramArrayOfByte, int paramInt1, int paramInt2, boolean paramBoolean);
  }

  static class b extends Base64.a
  {
    private static final int[] c;
    private static final int[] d = arrayOfInt2;
    private static final int e = -1;
    private static final int f = -2;
    private int g;
    private int h;
    private final int[] i;

    static
    {
      int[] arrayOfInt1 = new int[256];
      arrayOfInt1[0] = -1;
      arrayOfInt1[1] = -1;
      arrayOfInt1[2] = -1;
      arrayOfInt1[3] = -1;
      arrayOfInt1[4] = -1;
      arrayOfInt1[5] = -1;
      arrayOfInt1[6] = -1;
      arrayOfInt1[7] = -1;
      arrayOfInt1[8] = -1;
      arrayOfInt1[9] = -1;
      arrayOfInt1[10] = -1;
      arrayOfInt1[11] = -1;
      arrayOfInt1[12] = -1;
      arrayOfInt1[13] = -1;
      arrayOfInt1[14] = -1;
      arrayOfInt1[15] = -1;
      arrayOfInt1[16] = -1;
      arrayOfInt1[17] = -1;
      arrayOfInt1[18] = -1;
      arrayOfInt1[19] = -1;
      arrayOfInt1[20] = -1;
      arrayOfInt1[21] = -1;
      arrayOfInt1[22] = -1;
      arrayOfInt1[23] = -1;
      arrayOfInt1[24] = -1;
      arrayOfInt1[25] = -1;
      arrayOfInt1[26] = -1;
      arrayOfInt1[27] = -1;
      arrayOfInt1[28] = -1;
      arrayOfInt1[29] = -1;
      arrayOfInt1[30] = -1;
      arrayOfInt1[31] = -1;
      arrayOfInt1[32] = -1;
      arrayOfInt1[33] = -1;
      arrayOfInt1[34] = -1;
      arrayOfInt1[35] = -1;
      arrayOfInt1[36] = -1;
      arrayOfInt1[37] = -1;
      arrayOfInt1[38] = -1;
      arrayOfInt1[39] = -1;
      arrayOfInt1[40] = -1;
      arrayOfInt1[41] = -1;
      arrayOfInt1[42] = -1;
      arrayOfInt1[43] = 62;
      arrayOfInt1[44] = -1;
      arrayOfInt1[45] = -1;
      arrayOfInt1[46] = -1;
      arrayOfInt1[47] = 63;
      arrayOfInt1[48] = 52;
      arrayOfInt1[49] = 53;
      arrayOfInt1[50] = 54;
      arrayOfInt1[51] = 55;
      arrayOfInt1[52] = 56;
      arrayOfInt1[53] = 57;
      arrayOfInt1[54] = 58;
      arrayOfInt1[55] = 59;
      arrayOfInt1[56] = 60;
      arrayOfInt1[57] = 61;
      arrayOfInt1[58] = -1;
      arrayOfInt1[59] = -1;
      arrayOfInt1[60] = -1;
      arrayOfInt1[61] = -2;
      arrayOfInt1[62] = -1;
      arrayOfInt1[63] = -1;
      arrayOfInt1[64] = -1;
      arrayOfInt1[66] = 1;
      arrayOfInt1[67] = 2;
      arrayOfInt1[68] = 3;
      arrayOfInt1[69] = 4;
      arrayOfInt1[70] = 5;
      arrayOfInt1[71] = 6;
      arrayOfInt1[72] = 7;
      arrayOfInt1[73] = 8;
      arrayOfInt1[74] = 9;
      arrayOfInt1[75] = 10;
      arrayOfInt1[76] = 11;
      arrayOfInt1[77] = 12;
      arrayOfInt1[78] = 13;
      arrayOfInt1[79] = 14;
      arrayOfInt1[80] = 15;
      arrayOfInt1[81] = 16;
      arrayOfInt1[82] = 17;
      arrayOfInt1[83] = 18;
      arrayOfInt1[84] = 19;
      arrayOfInt1[85] = 20;
      arrayOfInt1[86] = 21;
      arrayOfInt1[87] = 22;
      arrayOfInt1[88] = 23;
      arrayOfInt1[89] = 24;
      arrayOfInt1[90] = 25;
      arrayOfInt1[91] = -1;
      arrayOfInt1[92] = -1;
      arrayOfInt1[93] = -1;
      arrayOfInt1[94] = -1;
      arrayOfInt1[95] = -1;
      arrayOfInt1[96] = -1;
      arrayOfInt1[97] = 26;
      arrayOfInt1[98] = 27;
      arrayOfInt1[99] = 28;
      arrayOfInt1[100] = 29;
      arrayOfInt1[101] = 30;
      arrayOfInt1[102] = 31;
      arrayOfInt1[103] = 32;
      arrayOfInt1[104] = 33;
      arrayOfInt1[105] = 34;
      arrayOfInt1[106] = 35;
      arrayOfInt1[107] = 36;
      arrayOfInt1[108] = 37;
      arrayOfInt1[109] = 38;
      arrayOfInt1[110] = 39;
      arrayOfInt1[111] = 40;
      arrayOfInt1[112] = 41;
      arrayOfInt1[113] = 42;
      arrayOfInt1[114] = 43;
      arrayOfInt1[115] = 44;
      arrayOfInt1[116] = 45;
      arrayOfInt1[117] = 46;
      arrayOfInt1[118] = 47;
      arrayOfInt1[119] = 48;
      arrayOfInt1[120] = 49;
      arrayOfInt1[121] = 50;
      arrayOfInt1[122] = 51;
      arrayOfInt1[123] = -1;
      arrayOfInt1[124] = -1;
      arrayOfInt1[125] = -1;
      arrayOfInt1[126] = -1;
      arrayOfInt1[127] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[''] = -1;
      arrayOfInt1[' '] = -1;
      arrayOfInt1['¡'] = -1;
      arrayOfInt1['¢'] = -1;
      arrayOfInt1['£'] = -1;
      arrayOfInt1['¤'] = -1;
      arrayOfInt1['¥'] = -1;
      arrayOfInt1['¦'] = -1;
      arrayOfInt1['§'] = -1;
      arrayOfInt1['¨'] = -1;
      arrayOfInt1['©'] = -1;
      arrayOfInt1['ª'] = -1;
      arrayOfInt1['«'] = -1;
      arrayOfInt1['¬'] = -1;
      arrayOfInt1['­'] = -1;
      arrayOfInt1['®'] = -1;
      arrayOfInt1['¯'] = -1;
      arrayOfInt1['°'] = -1;
      arrayOfInt1['±'] = -1;
      arrayOfInt1['²'] = -1;
      arrayOfInt1['³'] = -1;
      arrayOfInt1['´'] = -1;
      arrayOfInt1['µ'] = -1;
      arrayOfInt1['¶'] = -1;
      arrayOfInt1['·'] = -1;
      arrayOfInt1['¸'] = -1;
      arrayOfInt1['¹'] = -1;
      arrayOfInt1['º'] = -1;
      arrayOfInt1['»'] = -1;
      arrayOfInt1['¼'] = -1;
      arrayOfInt1['½'] = -1;
      arrayOfInt1['¾'] = -1;
      arrayOfInt1['¿'] = -1;
      arrayOfInt1['À'] = -1;
      arrayOfInt1['Á'] = -1;
      arrayOfInt1['Â'] = -1;
      arrayOfInt1['Ã'] = -1;
      arrayOfInt1['Ä'] = -1;
      arrayOfInt1['Å'] = -1;
      arrayOfInt1['Æ'] = -1;
      arrayOfInt1['Ç'] = -1;
      arrayOfInt1['È'] = -1;
      arrayOfInt1['É'] = -1;
      arrayOfInt1['Ê'] = -1;
      arrayOfInt1['Ë'] = -1;
      arrayOfInt1['Ì'] = -1;
      arrayOfInt1['Í'] = -1;
      arrayOfInt1['Î'] = -1;
      arrayOfInt1['Ï'] = -1;
      arrayOfInt1['Ð'] = -1;
      arrayOfInt1['Ñ'] = -1;
      arrayOfInt1['Ò'] = -1;
      arrayOfInt1['Ó'] = -1;
      arrayOfInt1['Ô'] = -1;
      arrayOfInt1['Õ'] = -1;
      arrayOfInt1['Ö'] = -1;
      arrayOfInt1['×'] = -1;
      arrayOfInt1['Ø'] = -1;
      arrayOfInt1['Ù'] = -1;
      arrayOfInt1['Ú'] = -1;
      arrayOfInt1['Û'] = -1;
      arrayOfInt1['Ü'] = -1;
      arrayOfInt1['Ý'] = -1;
      arrayOfInt1['Þ'] = -1;
      arrayOfInt1['ß'] = -1;
      arrayOfInt1['à'] = -1;
      arrayOfInt1['á'] = -1;
      arrayOfInt1['â'] = -1;
      arrayOfInt1['ã'] = -1;
      arrayOfInt1['ä'] = -1;
      arrayOfInt1['å'] = -1;
      arrayOfInt1['æ'] = -1;
      arrayOfInt1['ç'] = -1;
      arrayOfInt1['è'] = -1;
      arrayOfInt1['é'] = -1;
      arrayOfInt1['ê'] = -1;
      arrayOfInt1['ë'] = -1;
      arrayOfInt1['ì'] = -1;
      arrayOfInt1['í'] = -1;
      arrayOfInt1['î'] = -1;
      arrayOfInt1['ï'] = -1;
      arrayOfInt1['ð'] = -1;
      arrayOfInt1['ñ'] = -1;
      arrayOfInt1['ò'] = -1;
      arrayOfInt1['ó'] = -1;
      arrayOfInt1['ô'] = -1;
      arrayOfInt1['õ'] = -1;
      arrayOfInt1['ö'] = -1;
      arrayOfInt1['÷'] = -1;
      arrayOfInt1['ø'] = -1;
      arrayOfInt1['ù'] = -1;
      arrayOfInt1['ú'] = -1;
      arrayOfInt1['û'] = -1;
      arrayOfInt1['ü'] = -1;
      arrayOfInt1['ý'] = -1;
      arrayOfInt1['þ'] = -1;
      arrayOfInt1['ÿ'] = -1;
      c = arrayOfInt1;
      int[] arrayOfInt2 = new int[256];
      arrayOfInt2[0] = -1;
      arrayOfInt2[1] = -1;
      arrayOfInt2[2] = -1;
      arrayOfInt2[3] = -1;
      arrayOfInt2[4] = -1;
      arrayOfInt2[5] = -1;
      arrayOfInt2[6] = -1;
      arrayOfInt2[7] = -1;
      arrayOfInt2[8] = -1;
      arrayOfInt2[9] = -1;
      arrayOfInt2[10] = -1;
      arrayOfInt2[11] = -1;
      arrayOfInt2[12] = -1;
      arrayOfInt2[13] = -1;
      arrayOfInt2[14] = -1;
      arrayOfInt2[15] = -1;
      arrayOfInt2[16] = -1;
      arrayOfInt2[17] = -1;
      arrayOfInt2[18] = -1;
      arrayOfInt2[19] = -1;
      arrayOfInt2[20] = -1;
      arrayOfInt2[21] = -1;
      arrayOfInt2[22] = -1;
      arrayOfInt2[23] = -1;
      arrayOfInt2[24] = -1;
      arrayOfInt2[25] = -1;
      arrayOfInt2[26] = -1;
      arrayOfInt2[27] = -1;
      arrayOfInt2[28] = -1;
      arrayOfInt2[29] = -1;
      arrayOfInt2[30] = -1;
      arrayOfInt2[31] = -1;
      arrayOfInt2[32] = -1;
      arrayOfInt2[33] = -1;
      arrayOfInt2[34] = -1;
      arrayOfInt2[35] = -1;
      arrayOfInt2[36] = -1;
      arrayOfInt2[37] = -1;
      arrayOfInt2[38] = -1;
      arrayOfInt2[39] = -1;
      arrayOfInt2[40] = -1;
      arrayOfInt2[41] = -1;
      arrayOfInt2[42] = -1;
      arrayOfInt2[43] = -1;
      arrayOfInt2[44] = -1;
      arrayOfInt2[45] = 62;
      arrayOfInt2[46] = -1;
      arrayOfInt2[47] = -1;
      arrayOfInt2[48] = 52;
      arrayOfInt2[49] = 53;
      arrayOfInt2[50] = 54;
      arrayOfInt2[51] = 55;
      arrayOfInt2[52] = 56;
      arrayOfInt2[53] = 57;
      arrayOfInt2[54] = 58;
      arrayOfInt2[55] = 59;
      arrayOfInt2[56] = 60;
      arrayOfInt2[57] = 61;
      arrayOfInt2[58] = -1;
      arrayOfInt2[59] = -1;
      arrayOfInt2[60] = -1;
      arrayOfInt2[61] = -2;
      arrayOfInt2[62] = -1;
      arrayOfInt2[63] = -1;
      arrayOfInt2[64] = -1;
      arrayOfInt2[66] = 1;
      arrayOfInt2[67] = 2;
      arrayOfInt2[68] = 3;
      arrayOfInt2[69] = 4;
      arrayOfInt2[70] = 5;
      arrayOfInt2[71] = 6;
      arrayOfInt2[72] = 7;
      arrayOfInt2[73] = 8;
      arrayOfInt2[74] = 9;
      arrayOfInt2[75] = 10;
      arrayOfInt2[76] = 11;
      arrayOfInt2[77] = 12;
      arrayOfInt2[78] = 13;
      arrayOfInt2[79] = 14;
      arrayOfInt2[80] = 15;
      arrayOfInt2[81] = 16;
      arrayOfInt2[82] = 17;
      arrayOfInt2[83] = 18;
      arrayOfInt2[84] = 19;
      arrayOfInt2[85] = 20;
      arrayOfInt2[86] = 21;
      arrayOfInt2[87] = 22;
      arrayOfInt2[88] = 23;
      arrayOfInt2[89] = 24;
      arrayOfInt2[90] = 25;
      arrayOfInt2[91] = -1;
      arrayOfInt2[92] = -1;
      arrayOfInt2[93] = -1;
      arrayOfInt2[94] = -1;
      arrayOfInt2[95] = 63;
      arrayOfInt2[96] = -1;
      arrayOfInt2[97] = 26;
      arrayOfInt2[98] = 27;
      arrayOfInt2[99] = 28;
      arrayOfInt2[100] = 29;
      arrayOfInt2[101] = 30;
      arrayOfInt2[102] = 31;
      arrayOfInt2[103] = 32;
      arrayOfInt2[104] = 33;
      arrayOfInt2[105] = 34;
      arrayOfInt2[106] = 35;
      arrayOfInt2[107] = 36;
      arrayOfInt2[108] = 37;
      arrayOfInt2[109] = 38;
      arrayOfInt2[110] = 39;
      arrayOfInt2[111] = 40;
      arrayOfInt2[112] = 41;
      arrayOfInt2[113] = 42;
      arrayOfInt2[114] = 43;
      arrayOfInt2[115] = 44;
      arrayOfInt2[116] = 45;
      arrayOfInt2[117] = 46;
      arrayOfInt2[118] = 47;
      arrayOfInt2[119] = 48;
      arrayOfInt2[120] = 49;
      arrayOfInt2[121] = 50;
      arrayOfInt2[122] = 51;
      arrayOfInt2[123] = -1;
      arrayOfInt2[124] = -1;
      arrayOfInt2[125] = -1;
      arrayOfInt2[126] = -1;
      arrayOfInt2[127] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[''] = -1;
      arrayOfInt2[' '] = -1;
      arrayOfInt2['¡'] = -1;
      arrayOfInt2['¢'] = -1;
      arrayOfInt2['£'] = -1;
      arrayOfInt2['¤'] = -1;
      arrayOfInt2['¥'] = -1;
      arrayOfInt2['¦'] = -1;
      arrayOfInt2['§'] = -1;
      arrayOfInt2['¨'] = -1;
      arrayOfInt2['©'] = -1;
      arrayOfInt2['ª'] = -1;
      arrayOfInt2['«'] = -1;
      arrayOfInt2['¬'] = -1;
      arrayOfInt2['­'] = -1;
      arrayOfInt2['®'] = -1;
      arrayOfInt2['¯'] = -1;
      arrayOfInt2['°'] = -1;
      arrayOfInt2['±'] = -1;
      arrayOfInt2['²'] = -1;
      arrayOfInt2['³'] = -1;
      arrayOfInt2['´'] = -1;
      arrayOfInt2['µ'] = -1;
      arrayOfInt2['¶'] = -1;
      arrayOfInt2['·'] = -1;
      arrayOfInt2['¸'] = -1;
      arrayOfInt2['¹'] = -1;
      arrayOfInt2['º'] = -1;
      arrayOfInt2['»'] = -1;
      arrayOfInt2['¼'] = -1;
      arrayOfInt2['½'] = -1;
      arrayOfInt2['¾'] = -1;
      arrayOfInt2['¿'] = -1;
      arrayOfInt2['À'] = -1;
      arrayOfInt2['Á'] = -1;
      arrayOfInt2['Â'] = -1;
      arrayOfInt2['Ã'] = -1;
      arrayOfInt2['Ä'] = -1;
      arrayOfInt2['Å'] = -1;
      arrayOfInt2['Æ'] = -1;
      arrayOfInt2['Ç'] = -1;
      arrayOfInt2['È'] = -1;
      arrayOfInt2['É'] = -1;
      arrayOfInt2['Ê'] = -1;
      arrayOfInt2['Ë'] = -1;
      arrayOfInt2['Ì'] = -1;
      arrayOfInt2['Í'] = -1;
      arrayOfInt2['Î'] = -1;
      arrayOfInt2['Ï'] = -1;
      arrayOfInt2['Ð'] = -1;
      arrayOfInt2['Ñ'] = -1;
      arrayOfInt2['Ò'] = -1;
      arrayOfInt2['Ó'] = -1;
      arrayOfInt2['Ô'] = -1;
      arrayOfInt2['Õ'] = -1;
      arrayOfInt2['Ö'] = -1;
      arrayOfInt2['×'] = -1;
      arrayOfInt2['Ø'] = -1;
      arrayOfInt2['Ù'] = -1;
      arrayOfInt2['Ú'] = -1;
      arrayOfInt2['Û'] = -1;
      arrayOfInt2['Ü'] = -1;
      arrayOfInt2['Ý'] = -1;
      arrayOfInt2['Þ'] = -1;
      arrayOfInt2['ß'] = -1;
      arrayOfInt2['à'] = -1;
      arrayOfInt2['á'] = -1;
      arrayOfInt2['â'] = -1;
      arrayOfInt2['ã'] = -1;
      arrayOfInt2['ä'] = -1;
      arrayOfInt2['å'] = -1;
      arrayOfInt2['æ'] = -1;
      arrayOfInt2['ç'] = -1;
      arrayOfInt2['è'] = -1;
      arrayOfInt2['é'] = -1;
      arrayOfInt2['ê'] = -1;
      arrayOfInt2['ë'] = -1;
      arrayOfInt2['ì'] = -1;
      arrayOfInt2['í'] = -1;
      arrayOfInt2['î'] = -1;
      arrayOfInt2['ï'] = -1;
      arrayOfInt2['ð'] = -1;
      arrayOfInt2['ñ'] = -1;
      arrayOfInt2['ò'] = -1;
      arrayOfInt2['ó'] = -1;
      arrayOfInt2['ô'] = -1;
      arrayOfInt2['õ'] = -1;
      arrayOfInt2['ö'] = -1;
      arrayOfInt2['÷'] = -1;
      arrayOfInt2['ø'] = -1;
      arrayOfInt2['ù'] = -1;
      arrayOfInt2['ú'] = -1;
      arrayOfInt2['û'] = -1;
      arrayOfInt2['ü'] = -1;
      arrayOfInt2['ý'] = -1;
      arrayOfInt2['þ'] = -1;
      arrayOfInt2['ÿ'] = -1;
    }

    public b(int paramInt, byte[] paramArrayOfByte)
    {
      this.a = paramArrayOfByte;
      if ((paramInt & 0x8) == 0);
      for (int[] arrayOfInt = c; ; arrayOfInt = d)
      {
        this.i = arrayOfInt;
        this.g = 0;
        this.h = 0;
        return;
      }
    }

    public int a(int paramInt)
    {
      return 10 + paramInt * 3 / 4;
    }

    public boolean a(byte[] paramArrayOfByte, int paramInt1, int paramInt2, boolean paramBoolean)
    {
      if (this.g == 6)
        return false;
      int j = paramInt2 + paramInt1;
      int k = this.g;
      int m = this.h;
      int n = 0;
      byte[] arrayOfByte = this.a;
      int[] arrayOfInt = this.i;
      int i1 = k;
      int i2 = paramInt1;
      if (i2 >= j);
      int i6;
      label91: 
      do
      {
        i6 = m;
        if (paramBoolean)
          break label598;
        this.g = i1;
        this.h = i6;
        this.b = n;
        return true;
        if (i1 != 0)
          break;
        if (i2 + 4 <= j)
        {
          m = arrayOfInt[(0xFF & paramArrayOfByte[i2])] << 18 | arrayOfInt[(0xFF & paramArrayOfByte[(i2 + 1)])] << 12 | arrayOfInt[(0xFF & paramArrayOfByte[(i2 + 2)])] << 6 | arrayOfInt[(0xFF & paramArrayOfByte[(i2 + 3)])];
          if (m >= 0)
            break label243;
        }
      }
      while (i2 >= j);
      int i3 = i2 + 1;
      int i4 = arrayOfInt[(0xFF & paramArrayOfByte[i2])];
      switch (i1)
      {
      default:
      case 0:
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      }
      label243: 
      do
      {
        do
        {
          do
          {
            do
            {
              do
              {
                do
                {
                  i2 = i3;
                  break;
                  arrayOfByte[(n + 2)] = ((byte)m);
                  arrayOfByte[(n + 1)] = ((byte)(m >> 8));
                  arrayOfByte[n] = ((byte)(m >> 16));
                  n += 3;
                  i2 += 4;
                  break label91;
                  if (i4 >= 0)
                  {
                    i1++;
                    m = i4;
                    i2 = i3;
                    break;
                  }
                }
                while (i4 == -1);
                this.g = 6;
                return false;
                if (i4 >= 0)
                {
                  m = i4 | m << 6;
                  i1++;
                  i2 = i3;
                  break;
                }
              }
              while (i4 == -1);
              this.g = 6;
              return false;
              if (i4 >= 0)
              {
                m = i4 | m << 6;
                i1++;
                i2 = i3;
                break;
              }
              if (i4 == -2)
              {
                int i5 = n + 1;
                arrayOfByte[n] = ((byte)(m >> 4));
                i1 = 4;
                n = i5;
                i2 = i3;
                break;
              }
            }
            while (i4 == -1);
            this.g = 6;
            return false;
            if (i4 >= 0)
            {
              m = i4 | m << 6;
              arrayOfByte[(n + 2)] = ((byte)m);
              arrayOfByte[(n + 1)] = ((byte)(m >> 8));
              arrayOfByte[n] = ((byte)(m >> 16));
              n += 3;
              i2 = i3;
              i1 = 0;
              break;
            }
            if (i4 == -2)
            {
              arrayOfByte[(n + 1)] = ((byte)(m >> 2));
              arrayOfByte[n] = ((byte)(m >> 10));
              n += 2;
              i1 = 5;
              i2 = i3;
              break;
            }
          }
          while (i4 == -1);
          this.g = 6;
          return false;
          if (i4 == -2)
          {
            i1++;
            i2 = i3;
            break;
          }
        }
        while (i4 == -1);
        this.g = 6;
        return false;
      }
      while (i4 == -1);
      this.g = 6;
      return false;
      switch (i1)
      {
      case 0:
      default:
      case 1:
      case 2:
      case 3:
        while (true)
        {
          label598: this.g = i1;
          this.b = n;
          return true;
          this.g = 6;
          return false;
          int i8 = n + 1;
          arrayOfByte[n] = ((byte)(i6 >> 4));
          n = i8;
          continue;
          int i7 = n + 1;
          arrayOfByte[n] = ((byte)(i6 >> 10));
          n = i7 + 1;
          arrayOfByte[i7] = ((byte)(i6 >> 2));
        }
      case 4:
      }
      this.g = 6;
      return false;
    }
  }

  static class c extends Base64.a
  {
    public static final int c = 19;
    private static final byte[] i;
    private static final byte[] j;
    int d;
    public final boolean e;
    public final boolean f;
    public final boolean g;
    private final byte[] k;
    private int l;
    private final byte[] m;

    static
    {
      if (!Base64.class.desiredAssertionStatus());
      for (boolean bool = true; ; bool = false)
      {
        h = bool;
        i = new byte[] { 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47 };
        j = new byte[] { 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95 };
        return;
      }
    }

    public c(int paramInt, byte[] paramArrayOfByte)
    {
      this.a = paramArrayOfByte;
      boolean bool2;
      boolean bool3;
      label35: label47: byte[] arrayOfByte;
      if ((paramInt & 0x1) == 0)
      {
        bool2 = bool1;
        this.e = bool2;
        if ((paramInt & 0x2) != 0)
          break label106;
        bool3 = bool1;
        this.f = bool3;
        if ((paramInt & 0x4) == 0)
          break label112;
        this.g = bool1;
        if ((paramInt & 0x8) != 0)
          break label117;
        arrayOfByte = i;
        label64: this.m = arrayOfByte;
        this.k = new byte[2];
        this.d = 0;
        if (!this.f)
          break label125;
      }
      label106: label112: label117: label125: for (int n = 19; ; n = -1)
      {
        this.l = n;
        return;
        bool2 = false;
        break;
        bool3 = false;
        break label35;
        bool1 = false;
        break label47;
        arrayOfByte = j;
        break label64;
      }
    }

    public int a(int paramInt)
    {
      return 10 + paramInt * 8 / 5;
    }

    public boolean a(byte[] paramArrayOfByte, int paramInt1, int paramInt2, boolean paramBoolean)
    {
      byte[] arrayOfByte1 = this.m;
      byte[] arrayOfByte2 = this.a;
      int n = this.l;
      int i1 = paramInt2 + paramInt1;
      int i5;
      int i3;
      label58: int i37;
      int i38;
      int i7;
      int i6;
      switch (this.d)
      {
      default:
        i5 = -1;
        i3 = paramInt1;
        if (i5 != -1)
        {
          arrayOfByte2[0] = arrayOfByte1[(0x3F & i5 >> 18)];
          arrayOfByte2[1] = arrayOfByte1[(0x3F & i5 >> 12)];
          arrayOfByte2[2] = arrayOfByte1[(0x3F & i5 >> 6)];
          i37 = 4;
          arrayOfByte2[3] = arrayOfByte1[(i5 & 0x3F)];
          i38 = n - 1;
          if (i38 == 0)
          {
            if (this.g)
            {
              i37 = 5;
              arrayOfByte2[4] = 13;
            }
            i7 = i37 + 1;
            arrayOfByte2[i37] = 10;
            i6 = 19;
          }
        }
        break;
      case 0:
      case 1:
      case 2:
      }
      while (true)
      {
        int i32;
        int i31;
        label218: int i27;
        if (i3 + 3 > i1)
        {
          if (!paramBoolean)
            break label1074;
          if (i3 - this.d != i1 - 1)
            break label725;
          if (this.d <= 0)
            break label703;
          byte[] arrayOfByte8 = this.k;
          i32 = 1;
          i31 = arrayOfByte8[0];
          int i33 = (i31 & 0xFF) << 4;
          this.d -= i32;
          int i34 = i7 + 1;
          arrayOfByte2[i7] = arrayOfByte1[(0x3F & i33 >> 6)];
          i27 = i34 + 1;
          arrayOfByte2[i34] = arrayOfByte1[(i33 & 0x3F)];
          if (this.e)
          {
            int i36 = i27 + 1;
            arrayOfByte2[i27] = 61;
            i27 = i36 + 1;
            arrayOfByte2[i36] = 61;
          }
          if (!this.f)
            break label1205;
          if (this.g)
          {
            int i35 = i27 + 1;
            arrayOfByte2[i27] = 13;
            i27 = i35;
          }
          i7 = i27 + 1;
          arrayOfByte2[i27] = 10;
          if ((h) || (this.d == 0))
            break label1053;
          throw new AssertionError();
          i5 = -1;
          i3 = paramInt1;
          break label58;
          if (paramInt1 + 2 > i1)
            break;
          int i39 = (0xFF & this.k[0]) << 16;
          int i40 = paramInt1 + 1;
          int i41 = i39 | (0xFF & paramArrayOfByte[paramInt1]) << 8;
          int i42 = i40 + 1;
          int i43 = i41 | 0xFF & paramArrayOfByte[i40];
          this.d = 0;
          i5 = i43;
          i3 = i42;
          break label58;
          if (paramInt1 + 1 > i1)
            break;
          int i2 = (0xFF & this.k[0]) << 16 | (0xFF & this.k[1]) << 8;
          i3 = paramInt1 + 1;
          int i4 = i2 | 0xFF & paramArrayOfByte[paramInt1];
          this.d = 0;
          i5 = i4;
          break label58;
        }
        int i8 = (0xFF & paramArrayOfByte[i3]) << 16 | (0xFF & paramArrayOfByte[(i3 + 1)]) << 8 | 0xFF & paramArrayOfByte[(i3 + 2)];
        arrayOfByte2[i7] = arrayOfByte1[(0x3F & i8 >> 18)];
        arrayOfByte2[(i7 + 1)] = arrayOfByte1[(0x3F & i8 >> 12)];
        arrayOfByte2[(i7 + 2)] = arrayOfByte1[(0x3F & i8 >> 6)];
        arrayOfByte2[(i7 + 3)] = arrayOfByte1[(i8 & 0x3F)];
        int i9 = i3 + 3;
        int i10 = i7 + 4;
        int i11 = i6 - 1;
        if (i11 == 0)
        {
          int i12;
          if (this.g)
          {
            i12 = i10 + 1;
            arrayOfByte2[i10] = 13;
          }
          while (true)
          {
            i7 = i12 + 1;
            arrayOfByte2[i12] = 10;
            i3 = i9;
            i6 = 19;
            break;
            label703: int i30 = i3 + 1;
            i31 = paramArrayOfByte[i3];
            i3 = i30;
            i32 = 0;
            break label218;
            label725: int i19;
            int i18;
            label762: int i22;
            label803: int i26;
            if (i3 - this.d == i1 - 2)
              if (this.d > 1)
              {
                byte[] arrayOfByte7 = this.k;
                i19 = 1;
                i18 = arrayOfByte7[0];
                int i20 = (i18 & 0xFF) << 10;
                if (this.d <= 0)
                  break label979;
                byte[] arrayOfByte6 = this.k;
                int i29 = i19 + 1;
                i22 = arrayOfByte6[i19];
                i19 = i29;
                int i23 = i20 | (i22 & 0xFF) << 2;
                this.d -= i19;
                int i24 = i7 + 1;
                arrayOfByte2[i7] = arrayOfByte1[(0x3F & i23 >> 12)];
                int i25 = i24 + 1;
                arrayOfByte2[i24] = arrayOfByte1[(0x3F & i23 >> 6)];
                i26 = i25 + 1;
                arrayOfByte2[i25] = arrayOfByte1[(i23 & 0x3F)];
                if (!this.e)
                  break label1212;
                i27 = i26 + 1;
                arrayOfByte2[i26] = 61;
              }
            while (true)
            {
              if (this.f)
              {
                if (this.g)
                {
                  int i28 = i27 + 1;
                  arrayOfByte2[i27] = 13;
                  i27 = i28;
                }
                i7 = i27 + 1;
                arrayOfByte2[i27] = 10;
                break;
                int i17 = i3 + 1;
                i18 = paramArrayOfByte[i3];
                i3 = i17;
                i19 = 0;
                break label762;
                label979: int i21 = i3 + 1;
                i22 = paramArrayOfByte[i3];
                i3 = i21;
                break label803;
                if ((!this.f) || (i7 <= 0) || (i6 == 19))
                  break;
                int i16;
                if (this.g)
                {
                  i16 = i7 + 1;
                  arrayOfByte2[i7] = 13;
                }
                while (true)
                {
                  i7 = i16 + 1;
                  arrayOfByte2[i16] = 10;
                  break;
                  label1053: if ((!h) && (i3 != i1))
                  {
                    throw new AssertionError();
                    label1074: if (i3 != i1 - 1)
                      break label1126;
                    byte[] arrayOfByte5 = this.k;
                    int i15 = this.d;
                    this.d = (i15 + 1);
                    arrayOfByte5[i15] = paramArrayOfByte[i3];
                  }
                  while (true)
                  {
                    this.b = i7;
                    this.l = i6;
                    return true;
                    label1126: if (i3 == i1 - 2)
                    {
                      byte[] arrayOfByte3 = this.k;
                      int i13 = this.d;
                      this.d = (i13 + 1);
                      arrayOfByte3[i13] = paramArrayOfByte[i3];
                      byte[] arrayOfByte4 = this.k;
                      int i14 = this.d;
                      this.d = (i14 + 1);
                      arrayOfByte4[i14] = paramArrayOfByte[(i3 + 1)];
                    }
                  }
                  i16 = i7;
                }
              }
              label1205: i7 = i27;
              break;
              label1212: i27 = i26;
            }
            i12 = i10;
          }
        }
        i6 = i11;
        i7 = i10;
        i3 = i9;
        continue;
        i6 = i38;
        i7 = i37;
        continue;
        i6 = n;
        i7 = 0;
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.ai.controller.util.Base64
 * JD-Core Version:    0.6.2
 */