package com.inmobi.androidsdk.ai.controller.util;

public enum NavigationStringEnum
{
  private String a;

  static
  {
    CLOSE = new NavigationStringEnum("CLOSE", 1, "close");
    BACK = new NavigationStringEnum("BACK", 2, "back");
    FORWARD = new NavigationStringEnum("FORWARD", 3, "forward");
    REFRESH = new NavigationStringEnum("REFRESH", 4, "refresh");
    NavigationStringEnum[] arrayOfNavigationStringEnum = new NavigationStringEnum[5];
    arrayOfNavigationStringEnum[0] = NONE;
    arrayOfNavigationStringEnum[1] = CLOSE;
    arrayOfNavigationStringEnum[2] = BACK;
    arrayOfNavigationStringEnum[3] = FORWARD;
    arrayOfNavigationStringEnum[4] = REFRESH;
  }

  private NavigationStringEnum(String arg3)
  {
    Object localObject;
    this.a = localObject;
  }

  public static NavigationStringEnum fromString(String paramString)
  {
    NavigationStringEnum[] arrayOfNavigationStringEnum;
    int i;
    if (paramString != null)
    {
      arrayOfNavigationStringEnum = values();
      i = arrayOfNavigationStringEnum.length;
    }
    for (int j = 0; ; j++)
    {
      NavigationStringEnum localNavigationStringEnum;
      if (j >= i)
        localNavigationStringEnum = null;
      do
      {
        return localNavigationStringEnum;
        localNavigationStringEnum = arrayOfNavigationStringEnum[j];
      }
      while (paramString.equalsIgnoreCase(localNavigationStringEnum.a));
    }
  }

  public final String getText()
  {
    return this.a;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.ai.controller.util.NavigationStringEnum
 * JD-Core Version:    0.6.2
 */