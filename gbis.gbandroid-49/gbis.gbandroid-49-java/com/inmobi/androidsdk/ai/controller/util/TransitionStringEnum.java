package com.inmobi.androidsdk.ai.controller.util;

public enum TransitionStringEnum
{
  private String a;

  static
  {
    NONE = new TransitionStringEnum("NONE", 6, "none");
    TransitionStringEnum[] arrayOfTransitionStringEnum = new TransitionStringEnum[7];
    arrayOfTransitionStringEnum[0] = DEFAULT;
    arrayOfTransitionStringEnum[1] = DISSOLVE;
    arrayOfTransitionStringEnum[2] = FADE;
    arrayOfTransitionStringEnum[3] = ROLL;
    arrayOfTransitionStringEnum[4] = SLIDE;
    arrayOfTransitionStringEnum[5] = ZOOM;
    arrayOfTransitionStringEnum[6] = NONE;
  }

  private TransitionStringEnum(String arg3)
  {
    Object localObject;
    this.a = localObject;
  }

  public static TransitionStringEnum fromString(String paramString)
  {
    TransitionStringEnum[] arrayOfTransitionStringEnum;
    int i;
    if (paramString != null)
    {
      arrayOfTransitionStringEnum = values();
      i = arrayOfTransitionStringEnum.length;
    }
    for (int j = 0; ; j++)
    {
      TransitionStringEnum localTransitionStringEnum;
      if (j >= i)
        localTransitionStringEnum = null;
      do
      {
        return localTransitionStringEnum;
        localTransitionStringEnum = arrayOfTransitionStringEnum[j];
      }
      while (paramString.equalsIgnoreCase(localTransitionStringEnum.a));
    }
  }

  public final String getText()
  {
    return this.a;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.ai.controller.util.TransitionStringEnum
 * JD-Core Version:    0.6.2
 */