package com.inmobi.androidsdk.ai.controller.util;

import android.os.Bundle;
import android.util.Log;
import com.inmobi.androidsdk.IMAdRequest;
import com.inmobi.androidsdk.IMAdRequest.IMIDType;
import com.inmobi.androidsdk.impl.Constants;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPublicKeySpec;
import javax.crypto.Cipher;

public class Utils
{
  private static final String a = "ISO-8859-1";
  private static final String b = "C10F7968CFE2C76AC6F0650C877806D4514DE58FC239592D2385BCE5609A84B2A0FBDAF29B05505EAD1FDFEF3D7209ACBF34B5D0A806DF18147EA9C0337D6B5B";
  private static final String c = "010001";
  private static int d = 0;

  private static String a(String paramString)
  {
    try
    {
      BigInteger localBigInteger1 = new BigInteger("C10F7968CFE2C76AC6F0650C877806D4514DE58FC239592D2385BCE5609A84B2A0FBDAF29B05505EAD1FDFEF3D7209ACBF34B5D0A806DF18147EA9C0337D6B5B", 16);
      BigInteger localBigInteger2 = new BigInteger("010001", 16);
      RSAPublicKey localRSAPublicKey = (RSAPublicKey)KeyFactory.getInstance("RSA").generatePublic(new RSAPublicKeySpec(localBigInteger1, localBigInteger2));
      Cipher localCipher = Cipher.getInstance("RSA/ECB/nopadding");
      localCipher.init(1, localRSAPublicKey);
      String str = new String(Base64.encode(a(paramString.getBytes("UTF-8"), 1, localCipher), 0));
      return str;
    }
    catch (Exception localException)
    {
      if (Constants.DEBUG)
        Log.d("InMobiAndroidSDK_3.5.2", "Exception in encryptRSA", localException);
    }
    return null;
  }

  private static String a(String paramString1, String paramString2)
  {
    try
    {
      byte[] arrayOfByte1 = paramString1.getBytes("UTF-8");
      byte[] arrayOfByte2 = new byte[arrayOfByte1.length];
      byte[] arrayOfByte3 = paramString2.getBytes("UTF-8");
      for (int i = 0; ; i++)
      {
        if (i >= arrayOfByte1.length)
          return new String(Base64.encode(arrayOfByte2, 2), "UTF-8");
        arrayOfByte2[i] = ((byte)(arrayOfByte1[i] ^ arrayOfByte3[(i % arrayOfByte3.length)]));
      }
    }
    catch (Exception localException)
    {
      if (Constants.DEBUG)
        Log.d("InMobiAndroidSDK_3.5.2", "Exception in xor with random integer", localException);
    }
    return "";
  }

  private static byte[] a(byte[] paramArrayOfByte, int paramInt, Cipher paramCipher)
  {
    byte[] arrayOfByte1 = new byte[0];
    int i = paramArrayOfByte.length;
    byte[] arrayOfByte2 = new byte[64];
    Object localObject = arrayOfByte1;
    byte[] arrayOfByte3 = arrayOfByte2;
    int j = 0;
    if (j >= i)
      return a((byte[])localObject, paramCipher.doFinal(arrayOfByte3));
    byte[] arrayOfByte4;
    if ((j > 0) && (j % 64 == 0))
    {
      arrayOfByte4 = a((byte[])localObject, paramCipher.doFinal(arrayOfByte3));
      if (j + 64 <= i)
        break label114;
    }
    label114: for (int k = i - j; ; k = 64)
    {
      arrayOfByte3 = new byte[k];
      localObject = arrayOfByte4;
      arrayOfByte3[(j % 64)] = paramArrayOfByte[j];
      j++;
      break;
    }
  }

  private static byte[] a(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
  {
    byte[] arrayOfByte = new byte[paramArrayOfByte1.length + paramArrayOfByte2.length];
    int i = 0;
    int j = paramArrayOfByte1.length;
    int k = 0;
    if (i >= j);
    while (true)
    {
      if (k >= paramArrayOfByte2.length)
      {
        return arrayOfByte;
        arrayOfByte[i] = paramArrayOfByte1[i];
        i++;
        break;
      }
      arrayOfByte[(k + paramArrayOfByte1.length)] = paramArrayOfByte2[k];
      k++;
    }
  }

  public static String byteToHex(byte paramByte)
  {
    char[] arrayOfChar1 = { 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102 };
    char[] arrayOfChar2 = new char[2];
    arrayOfChar2[0] = arrayOfChar1[(0xF & paramByte >> 4)];
    arrayOfChar2[1] = arrayOfChar1[(paramByte & 0xF)];
    return new String(arrayOfChar2);
  }

  public static String convert(String paramString)
  {
    while (true)
    {
      int i;
      try
      {
        byte[] arrayOfByte = paramString.getBytes();
        StringBuffer localStringBuffer = new StringBuffer();
        i = 0;
        if (i >= arrayOfByte.length)
          return new String(localStringBuffer.toString().getBytes(), "ISO-8859-1");
        if ((0x80 & arrayOfByte[i]) > 0)
          localStringBuffer.append("%" + byteToHex(arrayOfByte[i]));
        else
          localStringBuffer.append((char)arrayOfByte[i]);
      }
      catch (Exception localException)
      {
        return null;
      }
      i++;
    }
  }

  public static String createMessageDigest(String paramString1, String paramString2)
  {
    try
    {
      MessageDigest localMessageDigest = MessageDigest.getInstance(paramString2);
      localMessageDigest.update(paramString1.getBytes());
      byte[] arrayOfByte = localMessageDigest.digest();
      StringBuffer localStringBuffer = new StringBuffer();
      for (int i = 0; ; i++)
      {
        if (i >= arrayOfByte.length)
          return localStringBuffer.toString();
        String str = Integer.toHexString(0xFF & arrayOfByte[i]);
        if (str.length() == 1)
          localStringBuffer.append('0');
        localStringBuffer.append(str);
      }
    }
    catch (Exception localException)
    {
      if (Constants.DEBUG)
        localException.printStackTrace();
    }
    return null;
  }

  public static String getData(String paramString, Bundle paramBundle)
  {
    return paramBundle.getString(paramString);
  }

  public static String getODIN1(String paramString)
  {
    try
    {
      MessageDigest localMessageDigest = MessageDigest.getInstance("SHA-1");
      localMessageDigest.update(paramString.getBytes());
      byte[] arrayOfByte = localMessageDigest.digest();
      StringBuffer localStringBuffer = new StringBuffer();
      for (int i = 0; ; i++)
      {
        if (i >= arrayOfByte.length)
          return localStringBuffer.toString();
        localStringBuffer.append(Integer.toString(256 + (0xFF & arrayOfByte[i]), 16).substring(1));
      }
    }
    catch (Exception localException)
    {
      if (Constants.DEBUG)
        Log.d("InMobiAndroidSDK_3.5.2", "Exception in getting ODIN-1", localException);
    }
    return null;
  }

  public static String getUIDMap(IMAdRequest paramIMAdRequest, String paramString1, String paramString2, boolean paramBoolean)
  {
    String str2;
    String str1;
    int i;
    if (paramIMAdRequest != null)
    {
      str2 = paramIMAdRequest.getIDType(IMAdRequest.IMIDType.ID_LOGIN);
      str1 = paramIMAdRequest.getIDType(IMAdRequest.IMIDType.ID_SESSION);
      i = paramIMAdRequest.getDeviceIdMask();
    }
    while (true)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("{");
      if (str2 != null)
      {
        String str7 = a(str2, paramString2);
        localStringBuilder.append("LID:'");
        localStringBuilder.append(str7);
        localStringBuilder.append("'");
      }
      for (int j = 1; ; j = 0)
      {
        if (str1 != null)
        {
          if (j == 1)
            localStringBuilder.append(",");
          String str6 = a(str1, paramString2);
          localStringBuilder.append("SID:'");
          localStringBuilder.append(str6);
          localStringBuilder.append("'");
        }
        for (int k = 1; ; k = j)
        {
          int m;
          if (paramIMAdRequest == null)
          {
            if (paramString1 == null)
              break label296;
            if (k == 1)
              localStringBuilder.append(",");
            String str5 = a(paramString1, paramString2);
            localStringBuilder.append("O1:'");
            localStringBuilder.append(str5);
            localStringBuilder.append("'");
            m = 1;
          }
          while (true)
          {
            localStringBuilder.append("}");
            String str3 = null;
            if (m == 1)
              str3 = a(localStringBuilder.toString());
            return str3;
            if ((paramString1 != null) && ((i & 0x1) != 1) && ((i & 0x2) == 2))
            {
              if (k == 1)
                localStringBuilder.append(",");
              String str4 = a(paramString1, paramString2);
              localStringBuilder.append("O1:'");
              localStringBuilder.append(str4);
              localStringBuilder.append("'");
              m = 1;
            }
            else
            {
              label296: m = k;
            }
          }
        }
      }
      i = 2;
      str1 = null;
      str2 = null;
    }
  }

  public static int incrementBaseUrl()
  {
    try
    {
      int i = 1 + d;
      d = i;
      return i;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  // ERROR //
  public static java.util.Properties readFileFromJar(String paramString)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: new 250	java/util/Properties
    //   5: dup
    //   6: invokespecial 251	java/util/Properties:<init>	()V
    //   9: astore_2
    //   10: ldc 253
    //   12: invokevirtual 259	java/lang/Class:getClassLoader	()Ljava/lang/ClassLoader;
    //   15: aload_0
    //   16: invokevirtual 265	java/lang/ClassLoader:getResource	(Ljava/lang/String;)Ljava/net/URL;
    //   19: invokevirtual 270	java/net/URL:getFile	()Ljava/lang/String;
    //   22: astore 9
    //   24: aload 9
    //   26: ldc_w 272
    //   29: invokevirtual 276	java/lang/String:startsWith	(Ljava/lang/String;)Z
    //   32: ifeq +11 -> 43
    //   35: aload 9
    //   37: iconst_5
    //   38: invokevirtual 204	java/lang/String:substring	(I)Ljava/lang/String;
    //   41: astore 9
    //   43: aload 9
    //   45: ldc_w 278
    //   48: invokevirtual 282	java/lang/String:indexOf	(Ljava/lang/String;)I
    //   51: istore 10
    //   53: iload 10
    //   55: ifle +13 -> 68
    //   58: aload 9
    //   60: iconst_0
    //   61: iload 10
    //   63: invokevirtual 284	java/lang/String:substring	(II)Ljava/lang/String;
    //   66: astore 9
    //   68: new 286	java/util/jar/JarFile
    //   71: dup
    //   72: aload 9
    //   74: invokespecial 287	java/util/jar/JarFile:<init>	(Ljava/lang/String;)V
    //   77: astore 11
    //   79: aload 11
    //   81: aload 11
    //   83: aload_0
    //   84: invokevirtual 291	java/util/jar/JarFile:getJarEntry	(Ljava/lang/String;)Ljava/util/jar/JarEntry;
    //   87: invokevirtual 295	java/util/jar/JarFile:getInputStream	(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;
    //   90: astore 12
    //   92: aload 12
    //   94: astore 6
    //   96: aload_2
    //   97: aload 6
    //   99: invokevirtual 299	java/util/Properties:load	(Ljava/io/InputStream;)V
    //   102: aload 6
    //   104: ifnull +8 -> 112
    //   107: aload 6
    //   109: invokevirtual 304	java/io/InputStream:close	()V
    //   112: aload_2
    //   113: areturn
    //   114: astore 5
    //   116: aconst_null
    //   117: astore 6
    //   119: getstatic 87	com/inmobi/androidsdk/impl/Constants:DEBUG	Z
    //   122: ifeq +14 -> 136
    //   125: ldc 89
    //   127: ldc_w 306
    //   130: aload 5
    //   132: invokestatic 96	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   135: pop
    //   136: aload 6
    //   138: ifnull +8 -> 146
    //   141: aload 6
    //   143: invokevirtual 304	java/io/InputStream:close	()V
    //   146: aconst_null
    //   147: areturn
    //   148: astore_3
    //   149: aload_1
    //   150: ifnull +7 -> 157
    //   153: aload_1
    //   154: invokevirtual 304	java/io/InputStream:close	()V
    //   157: aload_3
    //   158: athrow
    //   159: astore 13
    //   161: aload_2
    //   162: areturn
    //   163: astore 7
    //   165: goto -19 -> 146
    //   168: astore 4
    //   170: goto -13 -> 157
    //   173: astore_3
    //   174: aload 6
    //   176: astore_1
    //   177: goto -28 -> 149
    //   180: astore 5
    //   182: goto -63 -> 119
    //
    // Exception table:
    //   from	to	target	type
    //   10	43	114	java/lang/Exception
    //   43	53	114	java/lang/Exception
    //   58	68	114	java/lang/Exception
    //   68	92	114	java/lang/Exception
    //   10	43	148	finally
    //   43	53	148	finally
    //   58	68	148	finally
    //   68	92	148	finally
    //   107	112	159	java/lang/Exception
    //   141	146	163	java/lang/Exception
    //   153	157	168	java/lang/Exception
    //   96	102	173	finally
    //   119	136	173	finally
    //   96	102	180	java/lang/Exception
  }

  public static boolean validateAppId(String paramString)
  {
    if (paramString == null)
    {
      Log.v("InMobiAndroidSDK_3.5.2", "Cannot load ad because appId is null. Please provide a valid appId.");
      return false;
    }
    if (paramString.matches("(x)+"))
    {
      Log.v("InMobiAndroidSDK_3.5.2", "Cannot load ad because appId is null. Please provide a valid appId.");
      return false;
    }
    return true;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.ai.controller.util.Utils
 * JD-Core Version:    0.6.2
 */