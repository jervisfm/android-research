package com.inmobi.androidsdk.impl;

public final class AdException extends Exception
{
  public static final int INVALID_REQUEST = 300;
  public static final int NO_FILL = 100;
  public static final int PARSE_ERROR = 200;
  public static final int SANDBOX_BADIP = 500;
  public static final int SANDBOX_OOF = 400;
  public static final int SANDBOX_UA = 700;
  public static final int SANDBOX_UAND = 600;
  private static final long serialVersionUID = -3924043691624251411L;
  private int a = 300;

  public AdException(String paramString, int paramInt)
  {
    super(paramString);
    this.a = paramInt;
  }

  public AdException(String paramString, Exception paramException, int paramInt)
  {
    super(paramString, paramException);
    this.a = paramInt;
  }

  public final int getCode()
  {
    return this.a;
  }

  public final void setCode(int paramInt)
  {
    this.a = paramInt;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.impl.AdException
 * JD-Core Version:    0.6.2
 */