package com.inmobi.androidsdk.impl;

import java.io.Serializable;

public class AdUnit
  implements Serializable
{
  private static final long serialVersionUID = 7987544297386338802L;
  AdActionNames a = AdActionNames.AdActionName_Web;
  AdTypes b = AdTypes.NONE;
  String c;
  String d;
  boolean e;
  String f;
  String g;
  int h;
  int i;

  public static AdActionNames adActionNamefromString(String paramString)
  {
    AdActionNames localAdActionNames = AdActionNames.AdActionName_None;
    if (paramString != null)
    {
      if (paramString.equalsIgnoreCase("call"))
        localAdActionNames = AdActionNames.AdActionName_Call;
    }
    else
      return localAdActionNames;
    if (paramString.equalsIgnoreCase("sms"))
      return AdActionNames.AdActionName_SMS;
    if (paramString.equalsIgnoreCase("search"))
      return AdActionNames.AdActionName_Search;
    if (paramString.equalsIgnoreCase("android"))
      return AdActionNames.AdActionName_Android;
    if (paramString.equalsIgnoreCase("web"))
      return AdActionNames.AdActionName_Web;
    if (paramString.equalsIgnoreCase("map"))
      return AdActionNames.AdActionName_Map;
    if (paramString.equalsIgnoreCase("audio"))
      return AdActionNames.AdActionName_Audio;
    if (paramString.equalsIgnoreCase("video"))
      return AdActionNames.AdActionName_Video;
    return AdActionNames.AdActionName_None;
  }

  public static AdTypes adTypefromString(String paramString)
  {
    AdTypes localAdTypes = AdTypes.NONE;
    if (paramString != null)
    {
      if (!paramString.equalsIgnoreCase("banner"))
        break label23;
      localAdTypes = AdTypes.BANNER;
    }
    label23: 
    do
    {
      return localAdTypes;
      if (paramString.equalsIgnoreCase("text"))
        return AdTypes.TEXT;
      if (paramString.equalsIgnoreCase("search"))
        return AdTypes.SEARCH;
    }
    while (!paramString.equalsIgnoreCase("rm"));
    return AdTypes.RICH_MEDIA;
  }

  public AdActionNames getAdActionName()
  {
    return this.a;
  }

  public AdTypes getAdType()
  {
    return this.b;
  }

  public String getCDATABlock()
  {
    return this.g;
  }

  public String getDefaultTargetUrl()
  {
    return this.d;
  }

  public String getDeviceInfoUploadUrl()
  {
    return this.f;
  }

  public int getHeight()
  {
    return this.i;
  }

  public String getTargetUrl()
  {
    return this.c;
  }

  public int getWidth()
  {
    return this.h;
  }

  public boolean isSendDeviceInfo()
  {
    return this.e;
  }

  public void setAdActionName(AdActionNames paramAdActionNames)
  {
    this.a = paramAdActionNames;
  }

  public void setAdType(AdTypes paramAdTypes)
  {
    this.b = paramAdTypes;
  }

  public void setCDATABlock(String paramString)
  {
    this.g = paramString;
  }

  public void setDefaultTargetUrl(String paramString)
  {
    this.d = paramString;
  }

  public void setDeviceInfoUploadUrl(String paramString)
  {
    this.f = paramString;
  }

  public void setHeight(int paramInt)
  {
    this.i = paramInt;
  }

  public void setSendDeviceInfo(boolean paramBoolean)
  {
    this.e = paramBoolean;
  }

  public void setTargetUrl(String paramString)
  {
    this.c = paramString;
  }

  public void setWidth(int paramInt)
  {
    this.h = paramInt;
  }

  public String toString()
  {
    StringBuffer localStringBuffer = new StringBuffer();
    localStringBuffer.append("AdUnit: ");
    localStringBuffer.append(" adActionName: " + this.a);
    localStringBuffer.append(" adType: " + this.b);
    localStringBuffer.append(" targetUrl: " + this.c);
    localStringBuffer.append(" width: " + this.h);
    localStringBuffer.append(" height: " + this.i);
    return localStringBuffer.toString();
  }

  public static enum AdActionNames
  {
    static
    {
      AdActionName_SMS = new AdActionNames("AdActionName_SMS", 2);
      AdActionName_Search = new AdActionNames("AdActionName_Search", 3);
      AdActionName_Call = new AdActionNames("AdActionName_Call", 4);
      AdActionName_Android = new AdActionNames("AdActionName_Android", 5);
      AdActionName_Map = new AdActionNames("AdActionName_Map", 6);
      AdActionName_Audio = new AdActionNames("AdActionName_Audio", 7);
      AdActionName_Video = new AdActionNames("AdActionName_Video", 8);
      AdActionNames[] arrayOfAdActionNames = new AdActionNames[9];
      arrayOfAdActionNames[0] = AdActionName_None;
      arrayOfAdActionNames[1] = AdActionName_Web;
      arrayOfAdActionNames[2] = AdActionName_SMS;
      arrayOfAdActionNames[3] = AdActionName_Search;
      arrayOfAdActionNames[4] = AdActionName_Call;
      arrayOfAdActionNames[5] = AdActionName_Android;
      arrayOfAdActionNames[6] = AdActionName_Map;
      arrayOfAdActionNames[7] = AdActionName_Audio;
      arrayOfAdActionNames[8] = AdActionName_Video;
    }

    public final String toString()
    {
      return super.toString().replaceFirst("AdActionName_", "").toLowerCase();
    }
  }

  public static enum AdTypes
  {
    static
    {
      BANNER = new AdTypes("BANNER", 2);
      SEARCH = new AdTypes("SEARCH", 3);
      RICH_MEDIA = new AdTypes("RICH_MEDIA", 4);
      AdTypes[] arrayOfAdTypes = new AdTypes[5];
      arrayOfAdTypes[0] = NONE;
      arrayOfAdTypes[1] = TEXT;
      arrayOfAdTypes[2] = BANNER;
      arrayOfAdTypes[3] = SEARCH;
      arrayOfAdTypes[4] = RICH_MEDIA;
    }

    public final String toString()
    {
      return super.toString().replaceFirst("AdType_", "").toLowerCase();
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.impl.AdUnit
 * JD-Core Version:    0.6.2
 */