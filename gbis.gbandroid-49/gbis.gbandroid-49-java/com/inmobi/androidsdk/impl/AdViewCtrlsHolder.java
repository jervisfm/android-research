package com.inmobi.androidsdk.impl;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

class AdViewCtrlsHolder extends RelativeLayout
{
  public AdViewCtrlsHolder(Context paramContext)
  {
    super(paramContext);
  }

  public AdViewCtrlsHolder(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public AdViewCtrlsHolder(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.impl.AdViewCtrlsHolder
 * JD-Core Version:    0.6.2
 */