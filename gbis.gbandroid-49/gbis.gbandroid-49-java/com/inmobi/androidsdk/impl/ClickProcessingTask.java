package com.inmobi.androidsdk.impl;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Message;
import android.util.Log;
import com.inmobi.androidsdk.IMBrowserActivity;
import com.inmobi.androidsdk.ai.container.IMWebView.IMWebViewListener;
import com.inmobi.androidsdk.impl.net.HttpRequestBuilder;

public class ClickProcessingTask extends AsyncTask<Void, Void, String>
{
  private final AdUnit a;
  private final UserInfo b;
  private final Context c;
  private Message d;
  private Message e;
  private Message f;
  private IMWebView.IMWebViewListener g;

  public ClickProcessingTask(AdUnit paramAdUnit, UserInfo paramUserInfo, Context paramContext, Message paramMessage1, Message paramMessage2, Message paramMessage3, IMWebView.IMWebViewListener paramIMWebViewListener)
  {
    this.a = paramAdUnit;
    this.b = paramUserInfo;
    this.c = paramContext;
    this.d = paramMessage1;
    this.e = paramMessage2;
    this.f = paramMessage3;
    this.g = paramIMWebViewListener;
  }

  private void a(String paramString)
  {
    if (paramString == null);
    label330: 
    do
    {
      do
      {
        while (true)
        {
          return;
          try
          {
            if (Constants.DEBUG)
            {
              Log.d("InMobiAndroidSDK_3.5.2", "Click target URL: " + paramString);
              Log.d("InMobiAndroidSDK_3.5.2", "AdActionName: " + this.a.getAdActionName());
            }
            if (this.a.getAdActionName() == AdUnit.AdActionNames.AdActionName_SMS)
            {
              b(paramString);
              if (this.f == null)
                continue;
              this.f.sendToTarget();
            }
          }
          catch (ActivityNotFoundException localActivityNotFoundException)
          {
            Log.w("InMobiAndroidSDK_3.5.2", "Operation could not be performed : " + paramString, localActivityNotFoundException);
            return;
            if ((this.a.getAdActionName() != AdUnit.AdActionNames.AdActionName_Web) && (this.a.getAdActionName() != AdUnit.AdActionNames.AdActionName_Search))
              break label330;
            if ((paramString.startsWith("https:")) || (paramString.startsWith("market:")) || (paramString.contains("play.google.com")) || (paramString.contains("market.android.com")))
            {
              Intent localIntent1 = new Intent("android.intent.action.VIEW", Uri.parse(paramString));
              localIntent1.addFlags(268435456);
              this.c.startActivity(localIntent1);
              if (this.f == null)
                continue;
              this.f.sendToTarget();
            }
          }
          catch (Exception localException)
          {
            Log.w("InMobiAndroidSDK_3.5.2", "Error executing post click actions on URL : " + paramString, localException);
            return;
          }
        }
        Intent localIntent2 = new Intent(this.c, IMBrowserActivity.class);
        localIntent2.putExtra("extra_url", paramString);
        localIntent2.putExtra("extra_browser_type", 101);
        localIntent2.putExtra("FIRST_INSTANCE", "yes");
        IMBrowserActivity.setWebViewListener(this.g);
        this.c.startActivity(localIntent2);
      }
      while (this.e == null);
      this.e.sendToTarget();
      return;
      Intent localIntent3 = new Intent("android.intent.action.VIEW", Uri.parse(paramString));
      localIntent3.addFlags(268435456);
      this.c.startActivity(localIntent3);
    }
    while (this.f == null);
    this.f.sendToTarget();
  }

  private void b(String paramString)
  {
    int i = paramString.indexOf("&Body=", 0);
    String str = null;
    if (i > 0)
    {
      str = paramString.substring(i + 6);
      paramString = paramString.substring(0, i);
    }
    Intent localIntent = new Intent("android.intent.action.SENDTO", Uri.parse(paramString));
    localIntent.addFlags(268435456);
    localIntent.putExtra("compose_mode", true);
    if (str != null)
      localIntent.putExtra("sms_body", HttpRequestBuilder.getURLDecoded(str, "UTF-8"));
    this.c.startActivity(localIntent);
  }

  // ERROR //
  protected String doInBackground(Void[] paramArrayOfVoid)
  {
    // Byte code:
    //   0: new 211	com/inmobi/androidsdk/impl/net/RequestResponseManager
    //   3: dup
    //   4: aload_0
    //   5: getfield 28	com/inmobi/androidsdk/impl/ClickProcessingTask:c	Landroid/content/Context;
    //   8: invokespecial 214	com/inmobi/androidsdk/impl/net/RequestResponseManager:<init>	(Landroid/content/Context;)V
    //   11: astore_2
    //   12: new 216	java/util/ArrayList
    //   15: dup
    //   16: invokespecial 217	java/util/ArrayList:<init>	()V
    //   19: astore_3
    //   20: aload_3
    //   21: ldc 219
    //   23: invokeinterface 225 2 0
    //   28: pop
    //   29: aload_3
    //   30: aload_0
    //   31: getfield 24	com/inmobi/androidsdk/impl/ClickProcessingTask:a	Lcom/inmobi/androidsdk/impl/AdUnit;
    //   34: invokevirtual 76	com/inmobi/androidsdk/impl/AdUnit:getAdActionName	()Lcom/inmobi/androidsdk/impl/AdUnit$AdActionNames;
    //   37: invokevirtual 226	com/inmobi/androidsdk/impl/AdUnit$AdActionNames:toString	()Ljava/lang/String;
    //   40: invokeinterface 225 2 0
    //   45: pop
    //   46: aload_2
    //   47: aload_0
    //   48: getfield 24	com/inmobi/androidsdk/impl/ClickProcessingTask:a	Lcom/inmobi/androidsdk/impl/AdUnit;
    //   51: invokevirtual 229	com/inmobi/androidsdk/impl/AdUnit:getTargetUrl	()Ljava/lang/String;
    //   54: aload_0
    //   55: getfield 26	com/inmobi/androidsdk/impl/ClickProcessingTask:b	Lcom/inmobi/androidsdk/impl/UserInfo;
    //   58: aload_3
    //   59: invokevirtual 233	com/inmobi/androidsdk/impl/net/RequestResponseManager:initiateClick	(Ljava/lang/String;Lcom/inmobi/androidsdk/impl/UserInfo;Ljava/util/List;)Ljava/lang/String;
    //   62: astore 10
    //   64: aload 10
    //   66: astore 5
    //   68: aload_2
    //   69: invokevirtual 236	com/inmobi/androidsdk/impl/net/RequestResponseManager:getNewAdActionType	()Ljava/lang/String;
    //   72: astore 11
    //   74: aload 11
    //   76: ifnull +15 -> 91
    //   79: aload_0
    //   80: getfield 24	com/inmobi/androidsdk/impl/ClickProcessingTask:a	Lcom/inmobi/androidsdk/impl/AdUnit;
    //   83: aload 11
    //   85: invokestatic 240	com/inmobi/androidsdk/impl/AdUnit:adActionNamefromString	(Ljava/lang/String;)Lcom/inmobi/androidsdk/impl/AdUnit$AdActionNames;
    //   88: invokevirtual 244	com/inmobi/androidsdk/impl/AdUnit:setAdActionName	(Lcom/inmobi/androidsdk/impl/AdUnit$AdActionNames;)V
    //   91: aload 5
    //   93: areturn
    //   94: astore 4
    //   96: aconst_null
    //   97: astore 5
    //   99: aload 4
    //   101: astore 6
    //   103: ldc 49
    //   105: ldc 246
    //   107: aload 6
    //   109: invokestatic 98	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   112: pop
    //   113: aload 5
    //   115: areturn
    //   116: astore 6
    //   118: goto -15 -> 103
    //
    // Exception table:
    //   from	to	target	type
    //   0	64	94	java/lang/Exception
    //   68	74	116	java/lang/Exception
    //   79	91	116	java/lang/Exception
  }

  protected void onPostExecute(String paramString)
  {
    a(paramString);
    try
    {
      if (this.d != null)
        this.d.sendToTarget();
      return;
    }
    catch (Exception localException)
    {
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.impl.ClickProcessingTask
 * JD-Core Version:    0.6.2
 */