package com.inmobi.androidsdk.impl;

public final class Constants
{
  public static final String AD_SERVER_CACHED_LIFE = "inmobicachedlife";
  public static final String AD_SERVER_CACHED_URL = "inmobicachedserver";
  public static final String AD_SERVER_URL = "http://i.w.inmobi.com/showad.asm";
  public static final String AD_TEST_SERVER_URL = "http://i.w.sandbox.inmobi.com/showad.asm";
  public static final String APP_TRACKER_URL = "http://ma.inmobi.com/downloads/trackerV1";
  public static final String BASE_URL = "http://www.inmobi.com";
  public static final long CACHED_AD_SERVER_LIFE = 43200000L;
  public static final String CACHED_AD_SERVER_TIMESTAMP = "inmobi_cached_timestamp";
  public static boolean DEBUG = false;
  public static final int HTTP_SUCCESS_CODE = 200;
  public static final String LOGGING_TAG = "InMobiAndroidSDK_3.5.2";
  public static final boolean QA_MODE = false;
  public static final String QA_SERVER_URL = "";
  public static final String SDK_VERSION = "3.5.2";

  public static enum playerState
  {
    static
    {
      PAUSED = new playerState("PAUSED", 2);
      HIDDEN = new playerState("HIDDEN", 3);
      SHOWING = new playerState("SHOWING", 4);
      COMPLETED = new playerState("COMPLETED", 5);
      RELEASED = new playerState("RELEASED", 6);
      playerState[] arrayOfplayerState = new playerState[7];
      arrayOfplayerState[0] = INIT;
      arrayOfplayerState[1] = PLAYING;
      arrayOfplayerState[2] = PAUSED;
      arrayOfplayerState[3] = HIDDEN;
      arrayOfplayerState[4] = SHOWING;
      arrayOfplayerState[5] = COMPLETED;
      arrayOfplayerState[6] = RELEASED;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.impl.Constants
 * JD-Core Version:    0.6.2
 */