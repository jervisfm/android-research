package com.inmobi.androidsdk.impl;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.provider.Settings.Secure;
import android.provider.Settings.System;
import com.inmobi.androidsdk.ai.controller.util.Utils;
import java.net.URLEncoder;
import java.util.Random;

public class InMobiAndroidTrackerHTTPRequest
{
  private static final String h = "android";
  private final Context a;
  private final String b;
  private String c;
  private String d;
  private String e;
  private String f;
  private Random g;

  public InMobiAndroidTrackerHTTPRequest(Context paramContext, String paramString)
  {
    this.a = paramContext;
    this.b = paramString;
    this.c = Utils.createMessageDigest(a(), "MD5");
    if (this.c == null)
      this.c = "";
    this.g = new Random();
    this.f = "1";
  }

  private String a()
  {
    try
    {
      String str2 = Settings.Secure.getString(this.a.getApplicationContext().getContentResolver(), "android_id");
      localObject = str2;
      if (localObject == null);
      try
      {
        String str1 = Settings.System.getString(this.a.getApplicationContext().getContentResolver(), "android_id");
        localObject = str1;
        return localObject;
      }
      catch (Exception localException2)
      {
        return "";
      }
    }
    catch (Exception localException1)
    {
      while (true)
        Object localObject = null;
    }
  }

  private String a(String paramString)
  {
    try
    {
      String str = URLEncoder.encode(paramString, "UTF-8");
      return str;
    }
    catch (Exception localException)
    {
    }
    return "";
  }

  private void a(int paramInt)
  {
    this.e = Integer.toString(paramInt);
  }

  private String b()
  {
    try
    {
      ApplicationInfo localApplicationInfo = this.a.getPackageManager().getApplicationInfo(this.a.getPackageName(), 128);
      if (localApplicationInfo != null)
      {
        String str = localApplicationInfo.packageName;
        return str;
      }
      return "";
    }
    catch (Exception localException)
    {
    }
    return "";
  }

  // ERROR //
  public boolean setupConnection()
  {
    // Byte code:
    //   0: iconst_0
    //   1: istore_1
    //   2: aload_0
    //   3: aload_0
    //   4: getfield 46	com/inmobi/androidsdk/impl/InMobiAndroidTrackerHTTPRequest:g	Ljava/util/Random;
    //   7: invokevirtual 118	java/util/Random:nextInt	()I
    //   10: invokespecial 120	com/inmobi/androidsdk/impl/InMobiAndroidTrackerHTTPRequest:a	(I)V
    //   13: aload_0
    //   14: aconst_null
    //   15: aload_0
    //   16: invokespecial 29	com/inmobi/androidsdk/impl/InMobiAndroidTrackerHTTPRequest:a	()Ljava/lang/String;
    //   19: invokestatic 123	com/inmobi/androidsdk/ai/controller/util/Utils:getODIN1	(Ljava/lang/String;)Ljava/lang/String;
    //   22: aload_0
    //   23: getfield 90	com/inmobi/androidsdk/impl/InMobiAndroidTrackerHTTPRequest:e	Ljava/lang/String;
    //   26: iconst_1
    //   27: invokestatic 127	com/inmobi/androidsdk/ai/controller/util/Utils:getUIDMap	(Lcom/inmobi/androidsdk/IMAdRequest;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    //   30: putfield 129	com/inmobi/androidsdk/impl/InMobiAndroidTrackerHTTPRequest:d	Ljava/lang/String;
    //   33: new 131	java/text/SimpleDateFormat
    //   36: dup
    //   37: ldc 133
    //   39: invokespecial 136	java/text/SimpleDateFormat:<init>	(Ljava/lang/String;)V
    //   42: new 138	java/util/Date
    //   45: dup
    //   46: invokespecial 139	java/util/Date:<init>	()V
    //   49: invokevirtual 145	java/text/DateFormat:format	(Ljava/util/Date;)Ljava/lang/String;
    //   52: ldc 147
    //   54: ldc 149
    //   56: invokevirtual 155	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   59: astore 5
    //   61: new 157	java/lang/StringBuilder
    //   64: dup
    //   65: ldc 159
    //   67: invokespecial 160	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   70: aload_0
    //   71: getfield 26	com/inmobi/androidsdk/impl/InMobiAndroidTrackerHTTPRequest:b	Ljava/lang/String;
    //   74: invokevirtual 164	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   77: invokevirtual 166	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   80: astore 6
    //   82: aload_0
    //   83: getfield 129	com/inmobi/androidsdk/impl/InMobiAndroidTrackerHTTPRequest:d	Ljava/lang/String;
    //   86: astore 7
    //   88: iconst_0
    //   89: istore_1
    //   90: aload 7
    //   92: ifnull +60 -> 152
    //   95: new 157	java/lang/StringBuilder
    //   98: dup
    //   99: aload 6
    //   101: invokestatic 170	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   104: invokespecial 160	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   107: ldc 172
    //   109: invokevirtual 164	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   112: aload_0
    //   113: aload_0
    //   114: getfield 129	com/inmobi/androidsdk/impl/InMobiAndroidTrackerHTTPRequest:d	Ljava/lang/String;
    //   117: invokespecial 174	com/inmobi/androidsdk/impl/InMobiAndroidTrackerHTTPRequest:a	(Ljava/lang/String;)Ljava/lang/String;
    //   120: invokevirtual 164	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   123: ldc 176
    //   125: invokevirtual 164	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   128: aload_0
    //   129: getfield 90	com/inmobi/androidsdk/impl/InMobiAndroidTrackerHTTPRequest:e	Ljava/lang/String;
    //   132: invokevirtual 164	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   135: ldc 178
    //   137: invokevirtual 164	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   140: aload_0
    //   141: getfield 50	com/inmobi/androidsdk/impl/InMobiAndroidTrackerHTTPRequest:f	Ljava/lang/String;
    //   144: invokevirtual 164	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   147: invokevirtual 166	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   150: astore 6
    //   152: new 157	java/lang/StringBuilder
    //   155: dup
    //   156: aload 6
    //   158: invokestatic 170	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   161: invokespecial 160	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   164: ldc 180
    //   166: invokevirtual 164	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   169: aload_0
    //   170: invokespecial 182	com/inmobi/androidsdk/impl/InMobiAndroidTrackerHTTPRequest:b	()Ljava/lang/String;
    //   173: invokevirtual 164	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   176: ldc 184
    //   178: invokevirtual 164	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   181: aload 5
    //   183: invokevirtual 164	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   186: ldc 186
    //   188: invokevirtual 164	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   191: invokevirtual 166	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   194: astore 8
    //   196: getstatic 192	com/inmobi/androidsdk/impl/Constants:DEBUG	Z
    //   199: ifeq +26 -> 225
    //   202: ldc 194
    //   204: new 157	java/lang/StringBuilder
    //   207: dup
    //   208: ldc 196
    //   210: invokespecial 160	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   213: aload 8
    //   215: invokevirtual 164	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   218: invokevirtual 166	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   221: invokestatic 201	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
    //   224: pop
    //   225: new 203	java/net/URL
    //   228: dup
    //   229: aload 8
    //   231: invokespecial 204	java/net/URL:<init>	(Ljava/lang/String;)V
    //   234: invokevirtual 208	java/net/URL:openConnection	()Ljava/net/URLConnection;
    //   237: checkcast 210	java/net/HttpURLConnection
    //   240: astore 9
    //   242: aload 9
    //   244: iconst_1
    //   245: invokevirtual 214	java/net/HttpURLConnection:setDoOutput	(Z)V
    //   248: aload 9
    //   250: iconst_1
    //   251: invokevirtual 217	java/net/HttpURLConnection:setDoInput	(Z)V
    //   254: aload 9
    //   256: sipush 30000
    //   259: invokevirtual 220	java/net/HttpURLConnection:setConnectTimeout	(I)V
    //   262: aload 9
    //   264: sipush 30000
    //   267: invokevirtual 223	java/net/HttpURLConnection:setReadTimeout	(I)V
    //   270: new 225	java/io/BufferedReader
    //   273: dup
    //   274: new 227	java/io/InputStreamReader
    //   277: dup
    //   278: aload 9
    //   280: invokevirtual 231	java/net/HttpURLConnection:getInputStream	()Ljava/io/InputStream;
    //   283: invokespecial 234	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
    //   286: invokespecial 237	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   289: astore 10
    //   291: getstatic 192	com/inmobi/androidsdk/impl/Constants:DEBUG	Z
    //   294: ifeq +29 -> 323
    //   297: ldc 194
    //   299: new 157	java/lang/StringBuilder
    //   302: dup
    //   303: ldc 239
    //   305: invokespecial 160	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   308: aload 9
    //   310: invokevirtual 242	java/net/HttpURLConnection:getResponseCode	()I
    //   313: invokevirtual 245	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   316: invokevirtual 166	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   319: invokestatic 201	android/util/Log:d	(Ljava/lang/String;Ljava/lang/String;)I
    //   322: pop
    //   323: aload 9
    //   325: invokevirtual 242	java/net/HttpURLConnection:getResponseCode	()I
    //   328: istore 11
    //   330: iload 11
    //   332: sipush 200
    //   335: if_icmpne +219 -> 554
    //   338: invokestatic 251	org/xmlpull/v1/XmlPullParserFactory:newInstance	()Lorg/xmlpull/v1/XmlPullParserFactory;
    //   341: astore 14
    //   343: aload 14
    //   345: iconst_1
    //   346: invokevirtual 254	org/xmlpull/v1/XmlPullParserFactory:setNamespaceAware	(Z)V
    //   349: aload 14
    //   351: invokevirtual 258	org/xmlpull/v1/XmlPullParserFactory:newPullParser	()Lorg/xmlpull/v1/XmlPullParser;
    //   354: astore 15
    //   356: aload 15
    //   358: aload 10
    //   360: invokeinterface 263 2 0
    //   365: aload 15
    //   367: invokeinterface 266 1 0
    //   372: istore 16
    //   374: iload 16
    //   376: istore 17
    //   378: iconst_0
    //   379: istore_3
    //   380: aconst_null
    //   381: astore 18
    //   383: iload 17
    //   385: iconst_1
    //   386: if_icmpne +5 -> 391
    //   389: iload_3
    //   390: ireturn
    //   391: iload 17
    //   393: ifeq +148 -> 541
    //   396: iload 17
    //   398: iconst_1
    //   399: if_icmpeq +142 -> 541
    //   402: iload 17
    //   404: iconst_2
    //   405: if_icmpne +44 -> 449
    //   408: aload 15
    //   410: invokeinterface 269 1 0
    //   415: astore 25
    //   417: iload_3
    //   418: istore_1
    //   419: aload 25
    //   421: astore 20
    //   423: aload 15
    //   425: invokeinterface 272 1 0
    //   430: istore 21
    //   432: iload 21
    //   434: istore 17
    //   436: aload 20
    //   438: astore 22
    //   440: iload_1
    //   441: istore_3
    //   442: aload 22
    //   444: astore 18
    //   446: goto -63 -> 383
    //   449: iload 17
    //   451: iconst_3
    //   452: if_icmpeq +89 -> 541
    //   455: iload 17
    //   457: iconst_4
    //   458: if_icmpne +83 -> 541
    //   461: aload 18
    //   463: ifnull +78 -> 541
    //   466: aload 18
    //   468: ldc_w 274
    //   471: invokevirtual 278	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
    //   474: ifeq +67 -> 541
    //   477: aload 15
    //   479: invokeinterface 281 1 0
    //   484: invokestatic 286	java/lang/Boolean:parseBoolean	(Ljava/lang/String;)Z
    //   487: istore 23
    //   489: aload 18
    //   491: astore 24
    //   493: iload 23
    //   495: istore_1
    //   496: aload 24
    //   498: astore 20
    //   500: goto -77 -> 423
    //   503: astore 12
    //   505: iload_1
    //   506: istore_3
    //   507: aload 12
    //   509: astore 13
    //   511: aload 13
    //   513: invokevirtual 289	org/xmlpull/v1/XmlPullParserException:printStackTrace	()V
    //   516: iload_3
    //   517: ireturn
    //   518: astore 4
    //   520: aload 4
    //   522: invokevirtual 290	java/io/IOException:printStackTrace	()V
    //   525: iload_3
    //   526: ireturn
    //   527: astore_2
    //   528: iload_1
    //   529: istore_3
    //   530: aload_2
    //   531: astore 4
    //   533: goto -13 -> 520
    //   536: astore 13
    //   538: goto -27 -> 511
    //   541: aload 18
    //   543: astore 19
    //   545: iload_3
    //   546: istore_1
    //   547: aload 19
    //   549: astore 20
    //   551: goto -128 -> 423
    //   554: iconst_0
    //   555: ireturn
    //
    // Exception table:
    //   from	to	target	type
    //   338	374	503	org/xmlpull/v1/XmlPullParserException
    //   423	432	503	org/xmlpull/v1/XmlPullParserException
    //   408	417	518	java/io/IOException
    //   466	489	518	java/io/IOException
    //   511	516	518	java/io/IOException
    //   2	88	527	java/io/IOException
    //   95	152	527	java/io/IOException
    //   152	225	527	java/io/IOException
    //   225	323	527	java/io/IOException
    //   323	330	527	java/io/IOException
    //   338	374	527	java/io/IOException
    //   423	432	527	java/io/IOException
    //   408	417	536	org/xmlpull/v1/XmlPullParserException
    //   466	489	536	org/xmlpull/v1/XmlPullParserException
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.impl.InMobiAndroidTrackerHTTPRequest
 * JD-Core Version:    0.6.2
 */