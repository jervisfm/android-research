package com.inmobi.androidsdk.impl;

import android.util.Log;
import java.io.Reader;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public final class XMLParser
{
  public final AdUnit buildAdUnitFromResponseData(Reader paramReader)
  {
    AdUnit localAdUnit = new AdUnit();
    XmlPullParser localXmlPullParser;
    int j;
    int k;
    String str1;
    String str2;
    try
    {
      XmlPullParserFactory localXmlPullParserFactory = XmlPullParserFactory.newInstance();
      localXmlPullParserFactory.setNamespaceAware(true);
      localXmlPullParser = localXmlPullParserFactory.newPullParser();
      localXmlPullParser.setInput(paramReader);
      int i = localXmlPullParser.getEventType();
      j = 0;
      k = i;
      str1 = null;
      str2 = null;
      if (k == 1)
      {
        if (j != 0)
          break label290;
        throw new AdException("No Ads present", 100);
      }
    }
    catch (XmlPullParserException localXmlPullParserException)
    {
      throw new AdException("Exception constructing Ad", localXmlPullParserException, 200);
    }
    if (k != 0)
    {
      if (k != 2)
        break label421;
      str2 = localXmlPullParser.getName();
      if ((str2 != null) && (str2.equalsIgnoreCase("Ad")))
      {
        localAdUnit.setWidth(Integer.parseInt(localXmlPullParser.getAttributeValue(null, "width")));
        localAdUnit.setHeight(Integer.parseInt(localXmlPullParser.getAttributeValue(null, "height")));
        localAdUnit.setAdActionName(AdUnit.adActionNamefromString(localXmlPullParser.getAttributeValue(null, "actionName")));
        localAdUnit.setAdType(AdUnit.adTypefromString(localXmlPullParser.getAttributeValue(null, "type")));
        str1 = localXmlPullParser.getAttributeValue(null, "errorcode");
        j = 1;
      }
    }
    while (true)
    {
      k = localXmlPullParser.nextToken();
      break;
      label290: label421: 
      do
      {
        if (k == 5)
        {
          localAdUnit.setCDATABlock(localXmlPullParser.getText());
          break;
        }
        if ((k != 4) || (str2 == null) || (!str2.equalsIgnoreCase("AdURL")))
          break;
        localAdUnit.setTargetUrl(localXmlPullParser.getText());
        localAdUnit.setDefaultTargetUrl(localXmlPullParser.getText());
        break;
        if (str1 != null)
        {
          if (str1.equals("OOF"))
          {
            Log.v("InMobiAndroidSDK_3.5.2", "IP Address not found in CCID File");
            throw new AdException("IP Address not found in CCID File", 400);
          }
          if (str1.equals("BADIP"))
          {
            Log.v("InMobiAndroidSDK_3.5.2", "Invalid IP Address");
            throw new AdException("Invalid IP Address", 500);
          }
          if (str1.equals("UAND"))
          {
            Log.v("InMobiAndroidSDK_3.5.2", "User Agent not detected through using wurfl");
            throw new AdException("User Agent not detected through using wurfl", 600);
          }
          if (str1.equals("-UA"))
          {
            Log.v("InMobiAndroidSDK_3.5.2", "No User Agent found");
            throw new AdException("No User Agent found", 700);
          }
        }
        return localAdUnit;
      }
      while (k != 3);
      str2 = null;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.impl.XMLParser
 * JD-Core Version:    0.6.2
 */