package com.inmobi.androidsdk.impl.net;

import android.util.Log;
import com.inmobi.androidsdk.IMAdRequest.EducationType;
import com.inmobi.androidsdk.IMAdRequest.EthnicityType;
import com.inmobi.androidsdk.IMAdRequest.GenderType;
import com.inmobi.androidsdk.impl.UserInfo;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class HttpRequestBuilder
{
  private static String a(UserInfo paramUserInfo)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if (paramUserInfo.getPostalCode() != null)
    {
      localStringBuilder.append("u-postalCode=");
      localStringBuilder.append(getURLEncoded(paramUserInfo.getPostalCode()));
    }
    Iterator localIterator;
    if (paramUserInfo.getRequestParams() != null)
      localIterator = paramUserInfo.getRequestParams().entrySet().iterator();
    Object localObject;
    while (true)
    {
      String str2;
      if (!localIterator.hasNext())
      {
        if (paramUserInfo.getAreaCode() != null)
        {
          localStringBuilder.append("&u-areaCode=");
          localStringBuilder.append(getURLEncoded(paramUserInfo.getAreaCode()));
        }
        if (paramUserInfo.getDateOfBirth() != null)
        {
          localStringBuilder.append("&u-dateOfBirth=");
          localStringBuilder.append(getURLEncoded(paramUserInfo.getDateOfBirth()));
        }
        if ((paramUserInfo.getGender() != IMAdRequest.GenderType.NONE) && (paramUserInfo.getGender() != null))
        {
          localStringBuilder.append("&u-gender=");
          if (paramUserInfo.getGender() != IMAdRequest.GenderType.MALE)
            break label472;
          str2 = "M";
          localStringBuilder.append(str2);
        }
        if (paramUserInfo.getKeywords() != null)
        {
          localStringBuilder.append("&p-keywords=");
          localStringBuilder.append(getURLEncoded(paramUserInfo.getKeywords()));
        }
        if (paramUserInfo.getSearchString() != null)
        {
          localStringBuilder.append("&p-type=");
          localStringBuilder.append(getURLEncoded(paramUserInfo.getSearchString()));
        }
        if (paramUserInfo.getIncome() > 0)
        {
          localStringBuilder.append("&u-income=");
          localStringBuilder.append(paramUserInfo.getIncome());
        }
        if ((paramUserInfo.getEducation() != IMAdRequest.EducationType.Edu_None) && (paramUserInfo.getEducation() != null))
        {
          localStringBuilder.append("&u-education=");
          localStringBuilder.append(paramUserInfo.getEducation());
        }
        if ((paramUserInfo.getEthnicity() != IMAdRequest.EthnicityType.Eth_None) && (paramUserInfo.getEthnicity() != null))
        {
          localStringBuilder.append("&u-ethnicity=");
          localStringBuilder.append(paramUserInfo.getEthnicity());
        }
        if (paramUserInfo.getAge() > 0)
        {
          localStringBuilder.append("&u-age=");
          localStringBuilder.append(paramUserInfo.getAge());
        }
        if (paramUserInfo.getInterests() != null)
        {
          localStringBuilder.append("&u-interests=");
          localStringBuilder.append(getURLEncoded(paramUserInfo.getInterests()));
        }
        if (paramUserInfo.getLocationWithCityStateCountry() != null)
        {
          localStringBuilder.append("&u-location=");
          localStringBuilder.append(getURLEncoded(paramUserInfo.getLocationWithCityStateCountry()));
        }
        localObject = localStringBuilder.toString();
      }
      try
      {
        if (((String)localObject).charAt(0) == '&')
        {
          String str1 = ((String)localObject).substring(1);
          localObject = str1;
        }
        return localObject;
        Map.Entry localEntry = (Map.Entry)localIterator.next();
        localStringBuilder.append("&").append(getURLEncoded(((String)localEntry.getKey()).toString())).append("=").append(getURLEncoded(((String)localEntry.getValue()).toString()));
        continue;
        label472: str2 = "F";
      }
      catch (Exception localException)
      {
      }
    }
    return localObject;
  }

  static String a(UserInfo paramUserInfo, RequestResponseManager.ActionType paramActionType)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    try
    {
      if (RequestResponseManager.ActionType.AdRequest == paramActionType)
      {
        String str2 = a(paramUserInfo);
        localStringBuffer.append("requestactivity=AdRequest");
        if ((str2 != null) && (!str2.equalsIgnoreCase("")))
          localStringBuffer.append("&" + str2);
      }
      while (true)
      {
        localStringBuffer.append(b(paramUserInfo));
        localStringBuffer.append("&" + c(paramUserInfo));
        return localStringBuffer.toString();
        if (RequestResponseManager.ActionType.AdRequest_Interstitial != paramActionType)
          break;
        String str1 = a(paramUserInfo);
        localStringBuffer.append("adtype=int");
        if ((str1 != null) && (!str1.equalsIgnoreCase("")))
          localStringBuffer.append("&" + str1);
      }
    }
    catch (Exception localException)
    {
      while (true)
      {
        Log.w("InMobiAndroidSDK_3.5.2", "Exception occured in an ad request" + localException);
        continue;
        if (RequestResponseManager.ActionType.DeviceInfoUpload == paramActionType)
          localStringBuffer.append("requestactivity=DeviceInfo");
        else
          localStringBuffer.append("requestactivity=AdClicked");
      }
    }
  }

  private static String b(UserInfo paramUserInfo)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if (paramUserInfo.getScreenDensity() != null)
      localStringBuilder.append("&d-device-screen-density=").append(getURLEncoded(paramUserInfo.getScreenDensity()));
    if (paramUserInfo.getScreenSize() != null)
      localStringBuilder.append("&d-device-screen-size=").append(getURLEncoded(paramUserInfo.getScreenSize()));
    return localStringBuilder.toString();
  }

  private static String c(UserInfo paramUserInfo)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if (paramUserInfo.getSiteId() != null)
    {
      localStringBuilder.append("mk-siteid=");
      localStringBuilder.append(getURLEncoded(paramUserInfo.getSiteId()));
    }
    if (paramUserInfo.getUIDMapEncrypted() != null)
    {
      localStringBuilder.append("&u-id-map=");
      localStringBuilder.append(getURLEncoded(paramUserInfo.getUIDMapEncrypted()));
      localStringBuilder.append("&u-id-key=");
      localStringBuilder.append(paramUserInfo.getRandomKey());
      localStringBuilder.append("&u-key-ver=");
      localStringBuilder.append(paramUserInfo.getRsakeyVersion());
    }
    if (paramUserInfo.getAid() != null)
    {
      localStringBuilder.append("&aid=");
      localStringBuilder.append(getURLEncoded(paramUserInfo.getAid()));
    }
    localStringBuilder.append("&mk-version=");
    localStringBuilder.append(getURLEncoded("pr-SAND-DTFTB-20120224"));
    localStringBuilder.append("&format=xhtml");
    localStringBuilder.append("&mk-ads=1");
    localStringBuilder.append("&h-user-agent=");
    localStringBuilder.append(getURLEncoded(paramUserInfo.getPhoneDefaultUserAgent()));
    localStringBuilder.append("&u-InMobi_androidwebsdkVersion=");
    localStringBuilder.append(getURLEncoded("3.5.2"));
    localStringBuilder.append("&u-appBId=");
    localStringBuilder.append(getURLEncoded(paramUserInfo.getAppBId()));
    localStringBuilder.append("&u-appDNM=");
    localStringBuilder.append(getURLEncoded(paramUserInfo.getAppDisplayName()));
    localStringBuilder.append("&u-appVer=");
    localStringBuilder.append(getURLEncoded(paramUserInfo.getAppVer()));
    localStringBuilder.append("&d-localization=");
    localStringBuilder.append(getURLEncoded(paramUserInfo.getLocalization()));
    if (paramUserInfo.getNetworkType() != null)
    {
      localStringBuilder.append("&d-netType=");
      localStringBuilder.append(getURLEncoded(paramUserInfo.getNetworkType()));
    }
    if (paramUserInfo.getOrientation() != 0)
    {
      localStringBuilder.append("&d-orientation=");
      localStringBuilder.append(paramUserInfo.getOrientation());
    }
    localStringBuilder.append("&mk-ad-slot=");
    localStringBuilder.append(getURLEncoded(paramUserInfo.getAdUnitSlot()));
    if (paramUserInfo.isValidGeoInfo())
    {
      localStringBuilder.append("&u-latlong-accu=");
      localStringBuilder.append(getURLEncoded(currentLocationStr(paramUserInfo)));
    }
    if ((paramUserInfo.getRefTagKey() != null) && (paramUserInfo.getRefTagValue() != null))
      localStringBuilder.append("&").append(getURLEncoded(paramUserInfo.getRefTagKey())).append("=").append(getURLEncoded(paramUserInfo.getRefTagValue()));
    return localStringBuilder.toString();
  }

  public static String currentLocationStr(UserInfo paramUserInfo)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if (paramUserInfo.isValidGeoInfo())
    {
      localStringBuilder.append(paramUserInfo.getLat());
      localStringBuilder.append(",");
      localStringBuilder.append(paramUserInfo.getLon());
      localStringBuilder.append(",");
      localStringBuilder.append((int)paramUserInfo.getLocAccuracy());
      return localStringBuilder.toString();
    }
    return "";
  }

  public static String getURLDecoded(String paramString1, String paramString2)
  {
    try
    {
      String str = URLDecoder.decode(paramString1, paramString2);
      return str;
    }
    catch (Exception localException)
    {
    }
    return "";
  }

  public static String getURLEncoded(String paramString)
  {
    try
    {
      String str = URLEncoder.encode(paramString, "UTF-8");
      return str;
    }
    catch (Exception localException)
    {
    }
    return "";
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.impl.net.HttpRequestBuilder
 * JD-Core Version:    0.6.2
 */