package com.inmobi.androidsdk.impl.net;

public abstract interface HttpRequestCallback
{
  public static final int HTTP_FAILURE = 1;
  public static final int HTTP_SUCCESS;

  public abstract void notifyResult(int paramInt, Object paramObject);
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.impl.net.HttpRequestCallback
 * JD-Core Version:    0.6.2
 */