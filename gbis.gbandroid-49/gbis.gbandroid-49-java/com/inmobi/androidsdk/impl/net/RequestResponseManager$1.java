package com.inmobi.androidsdk.impl.net;

import android.util.Log;
import com.inmobi.androidsdk.IMAdRequest.ErrorCode;
import com.inmobi.androidsdk.impl.AdException;
import com.inmobi.androidsdk.impl.AdUnit;
import com.inmobi.androidsdk.impl.Constants;
import com.inmobi.androidsdk.impl.UserInfo;
import java.io.IOException;
import java.net.HttpURLConnection;

class RequestResponseManager$1 extends Thread
{
  RequestResponseManager$1(RequestResponseManager paramRequestResponseManager, UserInfo paramUserInfo, RequestResponseManager.ActionType paramActionType, HttpRequestCallback paramHttpRequestCallback)
  {
  }

  public void run()
  {
    try
    {
      String str1 = RequestResponseManager.a(this.a, this.b);
      if (Constants.DEBUG)
        Log.d("InMobiAndroidSDK_3.5.2", "Ad Serving URL: " + str1);
      RequestResponseManager.a(this.a, this.b.getPhoneDefaultUserAgent());
      String str2 = HttpRequestBuilder.a(this.b, this.c);
      Log.v("InMobiAndroidSDK_3.5.2", str2);
      HttpURLConnection localHttpURLConnection = RequestResponseManager.a(this.a, str1, this.b, this.c);
      RequestResponseManager.a(this.a, localHttpURLConnection, str2);
      AdUnit localAdUnit = RequestResponseManager.a(this.a, localHttpURLConnection, this.b);
      this.d.notifyResult(0, localAdUnit);
      return;
    }
    catch (AdException localAdException)
    {
      if (Constants.DEBUG)
        Log.d("InMobiAndroidSDK_3.5.2", "Exception Retriving Ad", localAdException);
      switch (localAdException.getCode())
      {
      default:
        return;
      case 100:
        this.d.notifyResult(1, IMAdRequest.ErrorCode.NO_FILL);
        return;
      case 300:
        this.d.notifyResult(1, IMAdRequest.ErrorCode.INVALID_REQUEST);
        return;
      case 200:
        this.d.notifyResult(1, IMAdRequest.ErrorCode.INTERNAL_ERROR);
        return;
      case 400:
        this.d.notifyResult(1, IMAdRequest.ErrorCode.INVALID_REQUEST);
        return;
      case 500:
        this.d.notifyResult(1, IMAdRequest.ErrorCode.INVALID_REQUEST);
        return;
      case 600:
        this.d.notifyResult(1, IMAdRequest.ErrorCode.INVALID_REQUEST);
        return;
      case 700:
      }
      this.d.notifyResult(1, IMAdRequest.ErrorCode.INVALID_REQUEST);
      return;
    }
    catch (IOException localIOException)
    {
      if (Constants.DEBUG)
        Log.d("InMobiAndroidSDK_3.5.2", "Exception Retriving Ad", localIOException);
      this.d.notifyResult(1, IMAdRequest.ErrorCode.NETWORK_ERROR);
      return;
    }
    catch (Exception localException)
    {
      if (Constants.DEBUG)
        Log.d("InMobiAndroidSDK_3.5.2", "Exception Retriving Ad", localException);
      this.d.notifyResult(1, IMAdRequest.ErrorCode.INTERNAL_ERROR);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.impl.net.RequestResponseManager.1
 * JD-Core Version:    0.6.2
 */