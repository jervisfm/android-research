package com.inmobi.androidsdk.impl.net;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import com.inmobi.androidsdk.impl.AdException;
import com.inmobi.androidsdk.impl.AdUnit;
import com.inmobi.androidsdk.impl.Constants;
import com.inmobi.androidsdk.impl.UserInfo;
import com.inmobi.androidsdk.impl.XMLParser;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

public final class RequestResponseManager
{
  private Context a;
  private String b = null;
  private String c;

  public RequestResponseManager(Context paramContext)
  {
    this.a = paramContext;
  }

  private AdUnit a(HttpURLConnection paramHttpURLConnection, UserInfo paramUserInfo)
  {
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", "Http Status Code: " + paramHttpURLConnection.getResponseCode());
    if (paramHttpURLConnection.getResponseCode() == 200);
    try
    {
      localBufferedReader1 = new BufferedReader(new InputStreamReader(paramHttpURLConnection.getInputStream(), "UTF-8"));
      try
      {
        StringBuilder localStringBuilder = new StringBuilder();
        while (true)
        {
          String str1 = localBufferedReader1.readLine();
          if (str1 == null)
          {
            String str2 = localStringBuilder.toString();
            if (Constants.DEBUG)
              Log.d("InMobiAndroidSDK_3.5.2", "Ad Response: " + str2);
            Map localMap = paramHttpURLConnection.getHeaderFields();
            localObject3 = null;
            if (localMap != null)
            {
              if (!localMap.containsKey("x-mkhoj-ph"))
                break label497;
              List localList3 = (List)localMap.get("x-mkhoj-ph");
              if ((localList3 == null) || (localList3.size() != 1))
                break label497;
              str3 = ((String)localList3.get(0)).trim();
              if (!localMap.containsKey("inmobicachedserver"))
                break label503;
              List localList1 = (List)localMap.get("inmobicachedserver");
              if ((localList1 == null) || (localList1.size() != 1))
                break label488;
              str6 = ((String)localList1.get(0)).trim();
              if (!localMap.containsKey("inmobicachedlife"))
                break;
              List localList2 = (List)localMap.get("inmobicachedlife");
              if ((localList2 == null) || (localList2.size() != 1))
                break;
              str4 = ((String)localList2.get(0)).trim();
              str5 = str6;
              a(str5, str4);
              break label503;
            }
            BufferedReader localBufferedReader2 = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(str2.getBytes()), "UTF-8"));
            AdUnit localAdUnit = new XMLParser().buildAdUnitFromResponseData(localBufferedReader2);
            if (Constants.DEBUG)
              Log.d("InMobiAndroidSDK_3.5.2", "Retrived AdUnit: " + localAdUnit);
            a(localAdUnit, paramUserInfo, (String)localObject3);
            paramHttpURLConnection.disconnect();
            a(localBufferedReader1);
            return localAdUnit;
          }
          localStringBuilder.append(str1).append("\n");
        }
      }
      finally
      {
      }
      paramHttpURLConnection.disconnect();
      a(localBufferedReader1);
      throw localObject1;
      Log.v("InMobiAndroidSDK_3.5.2", "Invalid Request. This may be because of invalid appId or appId might not be in 'active' state.");
      throw new AdException("Server did not return 200.", 300);
    }
    finally
    {
      String str3;
      while (true)
      {
        String str6;
        BufferedReader localBufferedReader1 = null;
        continue;
        String str5 = str6;
        String str4 = null;
        continue;
        label488: str4 = null;
        str5 = null;
        continue;
        label497: str3 = null;
      }
      label503: Object localObject3 = str3;
    }
  }

  private String a()
  {
    SharedPreferences localSharedPreferences = this.a.getSharedPreferences("InMobi_Prefs_key", 0);
    String str = localSharedPreferences.getString("inmobicachedserver", null);
    if (str != null)
    {
      long l1 = Calendar.getInstance().getTimeInMillis();
      long l2 = localSharedPreferences.getLong("inmobi_cached_timestamp", 0L);
      long l3 = localSharedPreferences.getLong("inmobicachedlife", 0L);
      if (l1 - l2 <= l3)
        return str;
    }
    return null;
  }

  private String a(UserInfo paramUserInfo)
  {
    String str;
    if (paramUserInfo.isTestMode())
      str = "http://i.w.sandbox.inmobi.com/showad.asm";
    do
    {
      return str;
      str = a();
    }
    while (str != null);
    return "http://i.w.inmobi.com/showad.asm";
  }

  private HttpURLConnection a(String paramString, UserInfo paramUserInfo, ActionType paramActionType)
  {
    HttpURLConnection localHttpURLConnection = (HttpURLConnection)new URL(paramString).openConnection();
    a(localHttpURLConnection, paramUserInfo, paramActionType);
    return localHttpURLConnection;
  }

  private void a(AdUnit paramAdUnit, UserInfo paramUserInfo, String paramString)
  {
    if (paramAdUnit == null)
      return;
    paramAdUnit.setSendDeviceInfo(true);
    paramAdUnit.setDeviceInfoUploadUrl(paramString);
  }

  private void a(Closeable paramCloseable)
  {
    if (paramCloseable != null);
    try
    {
      paramCloseable.close();
      return;
    }
    catch (IOException localIOException)
    {
      Log.d("InMobiAndroidSDK_3.5.2", "Exception closing resource: " + paramCloseable, localIOException);
    }
  }

  private void a(String paramString1, String paramString2)
  {
    long l1 = 43200000L;
    SharedPreferences.Editor localEditor = this.a.getSharedPreferences("InMobi_Prefs_key", 0).edit();
    if (paramString1 != null)
      localEditor.putString("inmobicachedserver", paramString1);
    if (paramString2 != null);
    try
    {
      long l2 = Long.parseLong(paramString2);
      l1 = l2 * 1000L;
      label53: localEditor.putLong("inmobicachedlife", l1);
      while (true)
      {
        localEditor.putLong("inmobi_cached_timestamp", Calendar.getInstance().getTimeInMillis());
        localEditor.commit();
        return;
        localEditor.putLong("inmobicachedlife", l1);
      }
    }
    catch (NumberFormatException localNumberFormatException)
    {
      break label53;
    }
  }

  private static void a(HttpURLConnection paramHttpURLConnection, UserInfo paramUserInfo, ActionType paramActionType)
  {
    paramHttpURLConnection.setDoOutput(true);
    paramHttpURLConnection.setDoInput(true);
    paramHttpURLConnection.setConnectTimeout(30000);
    paramHttpURLConnection.setReadTimeout(30000);
    paramHttpURLConnection.setRequestProperty("user-agent", paramUserInfo.getPhoneDefaultUserAgent());
    String str1;
    if (paramUserInfo.isTestMode())
    {
      str1 = "YES";
      paramHttpURLConnection.setRequestProperty("x-mkhoj-testmode", str1);
      paramHttpURLConnection.setUseCaches(false);
      if (paramActionType != ActionType.AdClick)
        break label120;
      paramHttpURLConnection.setRequestMethod("GET");
      label73: paramHttpURLConnection.setRequestProperty("content-type", "application/x-www-form-urlencoded");
      if (paramUserInfo.isTestMode())
        if (paramUserInfo.getTestModeAdActionType() == null)
          break label130;
    }
    label130: for (String str2 = paramUserInfo.getTestModeAdActionType(); ; str2 = "web")
    {
      paramHttpURLConnection.setRequestProperty("x-mkhoj-adactiontype", str2);
      return;
      str1 = "NO";
      break;
      label120: paramHttpURLConnection.setRequestMethod("POST");
      break label73;
    }
  }

  // ERROR //
  private void a(HttpURLConnection paramHttpURLConnection, String paramString)
  {
    // Byte code:
    //   0: aload_1
    //   1: ldc_w 336
    //   4: aload_2
    //   5: invokevirtual 339	java/lang/String:length	()I
    //   8: invokestatic 344	java/lang/Integer:toString	(I)Ljava/lang/String;
    //   11: invokevirtual 301	java/net/HttpURLConnection:setRequestProperty	(Ljava/lang/String;Ljava/lang/String;)V
    //   14: new 346	java/io/BufferedWriter
    //   17: dup
    //   18: new 348	java/io/OutputStreamWriter
    //   21: dup
    //   22: aload_1
    //   23: invokevirtual 352	java/net/HttpURLConnection:getOutputStream	()Ljava/io/OutputStream;
    //   26: invokespecial 355	java/io/OutputStreamWriter:<init>	(Ljava/io/OutputStream;)V
    //   29: invokespecial 358	java/io/BufferedWriter:<init>	(Ljava/io/Writer;)V
    //   32: astore_3
    //   33: aload_3
    //   34: aload_2
    //   35: invokevirtual 361	java/io/BufferedWriter:write	(Ljava/lang/String;)V
    //   38: aload_0
    //   39: aload_3
    //   40: invokespecial 148	com/inmobi/androidsdk/impl/net/RequestResponseManager:a	(Ljava/io/Closeable;)V
    //   43: return
    //   44: astore 4
    //   46: aconst_null
    //   47: astore_3
    //   48: aload_0
    //   49: aload_3
    //   50: invokespecial 148	com/inmobi/androidsdk/impl/net/RequestResponseManager:a	(Ljava/io/Closeable;)V
    //   53: aload 4
    //   55: athrow
    //   56: astore 4
    //   58: goto -10 -> 48
    //
    // Exception table:
    //   from	to	target	type
    //   14	33	44	finally
    //   33	38	56	finally
  }

  private String b(HttpURLConnection paramHttpURLConnection, String paramString)
  {
    HttpURLConnection.setFollowRedirects(false);
    try
    {
      if (Constants.DEBUG)
        Log.d("InMobiAndroidSDK_3.5.2", "HTTP Response code: " + paramHttpURLConnection.getResponseCode());
      String str3 = paramHttpURLConnection.getURL().toString();
      str1 = str3;
      if ((str1 == null) || (paramString.equalsIgnoreCase(str1)))
      {
        str2 = paramHttpURLConnection.getHeaderField("location");
        Map localMap = paramHttpURLConnection.getHeaderFields();
        if ((localMap != null) && (localMap.containsKey("action-name")))
        {
          List localList = (List)localMap.get("action-name");
          if ((localList != null) && (localList.size() == 1))
            setNewAdActionType(((String)localList.get(0)).trim());
        }
        if (Constants.DEBUG)
          Log.d("InMobiAndroidSDK_3.5.2", "Redirection URL: " + str2);
        return str2;
      }
    }
    catch (IOException localIOException)
    {
      while (true)
      {
        Log.d("InMobiAndroidSDK_3.5.2", "Exception getting response code for redirection URL", localIOException);
        String str1 = null;
        continue;
        String str2 = str1;
      }
    }
  }

  public final void asyncPing(String paramString)
  {
    new RequestResponseManager.2(this, paramString).start();
  }

  public final void asyncRequestAd(UserInfo paramUserInfo, ActionType paramActionType, HttpRequestCallback paramHttpRequestCallback)
  {
    new RequestResponseManager.1(this, paramUserInfo, paramActionType, paramHttpRequestCallback).start();
  }

  public final String getNewAdActionType()
  {
    return this.b;
  }

  public final String initiateClick(String paramString, UserInfo paramUserInfo, List<?> paramList)
  {
    if (Constants.DEBUG)
      Log.d("InMobiAndroidSDK_3.5.2", ">>> Enter initiateClick, clickURL: " + paramString);
    String str = null;
    if (paramList != null)
    {
      boolean bool1 = paramList.isEmpty();
      str = null;
      if (!bool1)
      {
        boolean bool2 = "x-mkhoj-adactiontype".equals(paramList.get(0));
        str = null;
        if (bool2)
          str = (String)paramList.get(1);
      }
    }
    HttpURLConnection localHttpURLConnection = a(paramString, paramUserInfo, ActionType.AdClick);
    if ((str != null) && (!paramUserInfo.isTestMode()))
      localHttpURLConnection.setRequestProperty("x-mkhoj-adactionType", str);
    return b(localHttpURLConnection, paramString);
  }

  public final AdUnit requestAd(String paramString, UserInfo paramUserInfo, ActionType paramActionType)
  {
    try
    {
      String str = HttpRequestBuilder.a(paramUserInfo, paramActionType);
      Log.v("InMobiAndroidSDK_3.5.2", URLDecoder.decode(str));
      HttpURLConnection localHttpURLConnection = a(paramString, paramUserInfo, paramActionType);
      a(localHttpURLConnection, str);
      AdUnit localAdUnit = a(localHttpURLConnection, paramUserInfo);
      return localAdUnit;
    }
    catch (IOException localIOException)
    {
      Log.w("InMobiAndroidSDK_3.5.2", "Exception occured while requesting an ad" + localIOException);
      localIOException.printStackTrace();
    }
    return null;
  }

  public final void setNewAdActionType(String paramString)
  {
    this.b = paramString;
  }

  // ERROR //
  public final void uploadDeviceInfo(String paramString, UserInfo paramUserInfo, ActionType paramActionType)
  {
    // Byte code:
    //   0: aload_2
    //   1: aload_3
    //   2: invokestatic 429	com/inmobi/androidsdk/impl/net/HttpRequestBuilder:a	(Lcom/inmobi/androidsdk/impl/UserInfo;Lcom/inmobi/androidsdk/impl/net/RequestResponseManager$ActionType;)Ljava/lang/String;
    //   5: astore 4
    //   7: aload_0
    //   8: aload_1
    //   9: aload_2
    //   10: aload_3
    //   11: invokespecial 215	com/inmobi/androidsdk/impl/net/RequestResponseManager:a	(Ljava/lang/String;Lcom/inmobi/androidsdk/impl/UserInfo;Lcom/inmobi/androidsdk/impl/net/RequestResponseManager$ActionType;)Ljava/net/HttpURLConnection;
    //   14: astore 5
    //   16: aload_0
    //   17: aload 5
    //   19: aload 4
    //   21: invokespecial 239	com/inmobi/androidsdk/impl/net/RequestResponseManager:a	(Ljava/net/HttpURLConnection;Ljava/lang/String;)V
    //   24: new 59	java/io/BufferedReader
    //   27: dup
    //   28: new 61	java/io/InputStreamReader
    //   31: dup
    //   32: aload 5
    //   34: invokevirtual 65	java/net/HttpURLConnection:getInputStream	()Ljava/io/InputStream;
    //   37: invokespecial 447	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
    //   40: invokespecial 73	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   43: astore 6
    //   45: aload 5
    //   47: invokevirtual 43	java/net/HttpURLConnection:getResponseCode	()I
    //   50: pop
    //   51: aload_0
    //   52: aload 6
    //   54: invokespecial 148	com/inmobi/androidsdk/impl/net/RequestResponseManager:a	(Ljava/io/Closeable;)V
    //   57: return
    //   58: astore 7
    //   60: aconst_null
    //   61: astore 6
    //   63: aload_0
    //   64: aload 6
    //   66: invokespecial 148	com/inmobi/androidsdk/impl/net/RequestResponseManager:a	(Ljava/io/Closeable;)V
    //   69: aload 7
    //   71: athrow
    //   72: astore 7
    //   74: goto -11 -> 63
    //
    // Exception table:
    //   from	to	target	type
    //   24	45	58	finally
    //   45	51	72	finally
  }

  public static enum ActionType
  {
    static
    {
      AdClick = new ActionType("AdClick", 3);
      ActionType[] arrayOfActionType = new ActionType[4];
      arrayOfActionType[0] = AdRequest;
      arrayOfActionType[1] = AdRequest_Interstitial;
      arrayOfActionType[2] = DeviceInfoUpload;
      arrayOfActionType[3] = AdClick;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.inmobi.androidsdk.impl.net.RequestResponseManager
 * JD-Core Version:    0.6.2
 */