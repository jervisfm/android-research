package com.millennialmedia.android;

import java.io.IOException;

public final class Base64
{
  public static byte[] decode(String paramString)
  {
    if (paramString.length() % 4 != 0)
      throw new IOException();
    int i = 3 * (paramString.length() / 4);
    byte[] arrayOfByte1;
    if (paramString.charAt(-2 + paramString.length()) == '=')
    {
      i -= 2;
      arrayOfByte1 = new byte[i];
    }
    for (int j = 0; ; j++)
    {
      if (j * 4 >= paramString.length())
        break label135;
      byte[] arrayOfByte2 = mapChars(paramString.substring(j * 4, 4 + j * 4));
      int k = 0;
      while (true)
        if (k < arrayOfByte2.length)
        {
          arrayOfByte1[(k + j * 3)] = arrayOfByte2[k];
          k++;
          continue;
          if (paramString.charAt(-1 + paramString.length()) != '=')
            break;
          i--;
          break;
        }
    }
    label135: return arrayOfByte1;
  }

  public static String encode(byte[] paramArrayOfByte)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    byte[] arrayOfByte1 = new byte[3];
    for (int i = 0; i < paramArrayOfByte.length - paramArrayOfByte.length % 3; i += 3)
    {
      arrayOfByte1[0] = paramArrayOfByte[i];
      arrayOfByte1[1] = paramArrayOfByte[(i + 1)];
      arrayOfByte1[2] = paramArrayOfByte[(i + 2)];
      localStringBuilder.append(mapBits(arrayOfByte1));
    }
    if (paramArrayOfByte.length % 3 == 1)
    {
      byte[] arrayOfByte3 = new byte[1];
      arrayOfByte3[0] = paramArrayOfByte[(-1 + paramArrayOfByte.length)];
      localStringBuilder.append(mapBits(arrayOfByte3));
    }
    while (true)
    {
      return localStringBuilder.toString();
      if (paramArrayOfByte.length % 3 == 2)
      {
        byte[] arrayOfByte2 = new byte[2];
        arrayOfByte2[0] = paramArrayOfByte[(-2 + paramArrayOfByte.length)];
        arrayOfByte2[1] = paramArrayOfByte[(-1 + paramArrayOfByte.length)];
        localStringBuilder.append(mapBits(arrayOfByte2));
      }
    }
  }

  private static String mapBits(byte[] paramArrayOfByte)
  {
    int i = 0;
    StringBuilder localStringBuilder = new StringBuilder();
    for (int j = 0; j < paramArrayOfByte.length; j++)
      i = i << 8 | 0xFF & paramArrayOfByte[j];
    if (paramArrayOfByte.length == 2)
      i <<= 2;
    while (true)
    {
      for (int k = 6 * paramArrayOfByte.length; k >= 0; k -= 6)
        localStringBuilder.append(mapValue(0x3F & i >> k));
      if (paramArrayOfByte.length == 1)
        i <<= 4;
    }
    if (paramArrayOfByte.length == 2)
      localStringBuilder.append('=');
    while (true)
    {
      return localStringBuilder.toString();
      if (paramArrayOfByte.length == 1)
        localStringBuilder.append("==");
    }
  }

  private static int mapChar(char paramChar)
  {
    if ((paramChar >= 'A') && (paramChar <= 'Z'))
      return paramChar - 'A';
    if ((paramChar >= 'a') && (paramChar <= 'z'))
      return 26 + (paramChar - 'a');
    if ((paramChar >= '0') && (paramChar <= '9'))
      return 52 + (paramChar - '0');
    if (paramChar == '+')
      return 62;
    if (paramChar == '/')
      return 63;
    throw new IOException();
  }

  private static byte[] mapChars(String paramString)
  {
    int i = 0;
    for (int j = 0; (j < paramString.length()) && (paramString.charAt(j) != '='); j++)
      i = i << 6 | mapChar(paramString.charAt(j));
    if (j == 2)
    {
      byte[] arrayOfByte3 = new byte[1];
      arrayOfByte3[0] = ((byte)(0xFF & i >> 4));
      return arrayOfByte3;
    }
    if (j == 3)
    {
      byte[] arrayOfByte2 = new byte[2];
      arrayOfByte2[1] = ((byte)(0xFF & i >> 2));
      arrayOfByte2[0] = ((byte)(0xFF & i >> 10));
      return arrayOfByte2;
    }
    byte[] arrayOfByte1 = new byte[3];
    arrayOfByte1[2] = ((byte)(i & 0xFF));
    arrayOfByte1[1] = ((byte)(0xFF & i >> 8));
    arrayOfByte1[0] = ((byte)(0xFF & i >> 16));
    return arrayOfByte1;
  }

  private static char mapValue(int paramInt)
  {
    if (paramInt < 26)
      return (char)(paramInt + 65);
    if (paramInt < 52)
      return (char)(97 + (paramInt - 26));
    if (paramInt < 62)
      return (char)(48 + (paramInt - 52));
    if (paramInt == 62)
      return '+';
    return '/';
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.Base64
 * JD-Core Version:    0.6.2
 */