package com.millennialmedia.android;

import android.content.Context;
import java.util.Date;

abstract class BasicCachedAd
{
  String acid;
  String contentUrl;
  long deferredViewStart;
  boolean downloaded;
  Date expiration;
  String id;
  int type;
  int views;

  BasicCachedAd()
  {
  }

  BasicCachedAd(String paramString)
  {
    this.id = paramString;
  }

  static boolean isOnDisk(Context paramContext, String paramString)
  {
    BasicCachedAd localBasicCachedAd = load(paramContext, paramString);
    if (localBasicCachedAd != null)
      return localBasicCachedAd.isOnDisk(paramContext);
    return false;
  }

  static BasicCachedAd load(Context paramContext, String paramString)
  {
    return null;
  }

  abstract boolean download(Context paramContext);

  abstract boolean isExpired();

  abstract boolean isOnDisk(Context paramContext);

  boolean isValid()
  {
    return (this.id != null) && (this.id.length() > 0) && (this.contentUrl != null) && (this.contentUrl.length() > 0);
  }

  void parseJSON(String paramString)
  {
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.BasicCachedAd
 * JD-Core Version:    0.6.2
 */