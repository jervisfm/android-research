package com.millennialmedia.android;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class EventLogSet$1
  implements Parcelable.Creator<EventLogSet>
{
  public final EventLogSet createFromParcel(Parcel paramParcel)
  {
    return new EventLogSet(paramParcel);
  }

  public final EventLogSet[] newArray(int paramInt)
  {
    return new EventLogSet[paramInt];
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.EventLogSet.1
 * JD-Core Version:    0.6.2
 */