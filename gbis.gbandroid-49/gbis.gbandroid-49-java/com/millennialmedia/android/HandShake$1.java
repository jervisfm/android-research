package com.millennialmedia.android;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Handler;
import android.util.DisplayMetrics;
import java.lang.ref.WeakReference;
import java.net.URLEncoder;
import java.util.Locale;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;

class HandShake$1
  implements Runnable
{
  HandShake$1(HandShake paramHandShake)
  {
  }

  public void run()
  {
    HttpGetRequest localHttpGetRequest = new HttpGetRequest();
    Context localContext = (Context)HandShake.access$000(this.this$0).get();
    if (localContext == null)
      return;
    while (true)
    {
      StringBuilder localStringBuilder;
      try
      {
        DisplayMetrics localDisplayMetrics = localContext.getResources().getDisplayMetrics();
        float f = localDisplayMetrics.density;
        int i = localDisplayMetrics.heightPixels;
        int j = localDisplayMetrics.widthPixels;
        localStringBuilder = new StringBuilder();
        if (this.this$0.hdid)
        {
          str1 = MMAdViewSDK.getHdid(localContext);
          if (str1 != null)
          {
            if (!str1.startsWith("mmh_"))
              break label591;
            localStringBuilder.append("&hdid=" + URLEncoder.encode(str1, "UTF-8"));
          }
          String str2 = MMAdViewSDK.getMMdid(localContext);
          if (str2 != null)
            localStringBuilder.append("&mmdid=" + URLEncoder.encode(str2, "UTF-8"));
          if (Build.MODEL != null)
            localStringBuilder.append("&dm=" + URLEncoder.encode(Build.MODEL, "UTF-8"));
          if (Build.VERSION.RELEASE != null)
            localStringBuilder.append("&dv=Android" + URLEncoder.encode(Build.VERSION.RELEASE, "UTF-8"));
          localStringBuilder.append("&density=" + Float.toString(f));
          localStringBuilder.append("&hpx=" + i);
          localStringBuilder.append("&wpx=" + j);
          String str3 = MMAdViewController.getURLDeviceValues(localContext);
          localStringBuilder.append("&mmisdk=" + URLEncoder.encode("4.5.1-12.2.2.a", "UTF-8") + str3);
          Locale localLocale = Locale.getDefault();
          if (localLocale != null)
            localStringBuilder.append("&language=" + localLocale.getLanguage() + "&country=" + localLocale.getCountry());
          String str4 = HandShake.access$100().getSchemesList(localContext);
          if (str4 != null)
            localStringBuilder.append("&appsids=" + str4);
          String str5 = MMAdViewController.getCachedVideoList(localContext);
          if (str5 != null)
            localStringBuilder.append("&vid=" + URLEncoder.encode(str5, "UTF-8"));
          MMAdViewController.getURLDeviceValues(localContext);
          HttpResponse localHttpResponse = localHttpGetRequest.get("http://ads.mp.mydas.mobi/appConfigServlet?apid=" + HandShake.apid + localStringBuilder.toString());
          if (localHttpResponse == null)
            break;
          HandShake.access$300(this.this$0, HandShake.access$200(this.this$0, HttpGetRequest.convertStreamToString(localHttpResponse.getEntity().getContent())), localContext);
          HandShake.access$400(this.this$0, localContext);
          HandShake.access$700(this.this$0).postDelayed(HandShake.access$500(this.this$0), HandShake.access$600(this.this$0));
          return;
        }
      }
      catch (Exception localException)
      {
        localException.printStackTrace();
        return;
      }
      String str1 = MMAdViewSDK.getAuid(localContext);
      continue;
      label591: localStringBuilder.append("&auid=" + URLEncoder.encode(str1, "UTF-8"));
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.HandShake.1
 * JD-Core Version:    0.6.2
 */