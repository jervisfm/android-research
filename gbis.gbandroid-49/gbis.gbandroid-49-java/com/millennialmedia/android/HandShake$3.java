package com.millennialmedia.android;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.view.WindowManager.BadTokenException;

class HandShake$3
  implements Runnable
{
  HandShake$3(HandShake paramHandShake, Context paramContext, String paramString)
  {
  }

  public void run()
  {
    try
    {
      AlertDialog localAlertDialog = new AlertDialog.Builder(this.val$context).create();
      localAlertDialog.setTitle("Error");
      localAlertDialog.setMessage(this.val$message);
      localAlertDialog.setButton(-3, "OK", new HandShake.3.1(this, localAlertDialog));
      localAlertDialog.show();
      return;
    }
    catch (WindowManager.BadTokenException localBadTokenException)
    {
      localBadTokenException.printStackTrace();
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.HandShake.3
 * JD-Core Version:    0.6.2
 */