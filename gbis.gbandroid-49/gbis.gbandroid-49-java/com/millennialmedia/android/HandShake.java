package com.millennialmedia.android;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class HandShake
{
  static String apid;
  private static final String handShakeURL = "http://ads.mp.mydas.mobi/appConfigServlet?apid=";
  private static HandShake sharedInstance;
  private LinkedHashMap<String, AdTypeHandShake> adTypeHandShakes = new LinkedHashMap();
  private String[] cachedVideos;
  private WeakReference<Context> contextRef;
  long creativeCacheTimeout = 259200000L;
  private long deferredViewTimeout = 3600000L;
  private long handShakeCallback = 86400000L;
  private final Handler handler = new Handler(Looper.getMainLooper());
  boolean hdid = true;
  boolean kill = false;
  private long lastHandShake;
  String mmdid;
  private ArrayList<Scheme> schemes = new ArrayList();
  private String schemesList;
  private Runnable updateHandShakeRunnable = new HandShake.2(this);

  private HandShake()
  {
  }

  private HandShake(Context paramContext)
  {
    this.contextRef = new WeakReference(paramContext);
    if ((!loadHandShake(paramContext)) || (System.currentTimeMillis() - this.lastHandShake > this.handShakeCallback))
    {
      this.lastHandShake = System.currentTimeMillis();
      new Thread(new HandShake.1(this)).start();
    }
  }

  private void deserializeFromObj(JSONObject paramJSONObject, Context paramContext)
  {
    int i = 0;
    if (paramJSONObject == null)
      return;
    int m;
    try
    {
      JSONArray localJSONArray1 = paramJSONObject.optJSONArray("errors");
      if (localJSONArray1 != null)
      {
        m = 0;
        label23: if (m < localJSONArray1.length())
        {
          JSONObject localJSONObject4 = localJSONArray1.optJSONObject(m);
          if (localJSONObject4 == null)
            break label463;
          String str1 = localJSONObject4.optString("message", null);
          String str2 = localJSONObject4.optString("type", null);
          if ((str1 == null) || (str2 == null))
            break label463;
          if (str2.equalsIgnoreCase("log"))
            Log.e("MillennialMediaSDK", str1);
          else if (str2.equalsIgnoreCase("prompt"))
            this.handler.post(new HandShake.3(this, paramContext, str1));
        }
      }
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
      return;
    }
    JSONObject localJSONObject1 = paramJSONObject.optJSONObject("adtypes");
    int j;
    if (localJSONObject1 != null)
    {
      String[] arrayOfString = MMAdView.getAdTypes();
      j = 0;
      label159: if (j < arrayOfString.length)
      {
        JSONObject localJSONObject3 = localJSONObject1.optJSONObject(arrayOfString[j]);
        if (localJSONObject3 == null)
          break label469;
        AdTypeHandShake localAdTypeHandShake = new AdTypeHandShake();
        localAdTypeHandShake.deserializeFromObj(localJSONObject3);
        localAdTypeHandShake.loadLastVideo(paramContext, arrayOfString[j]);
        this.adTypeHandShakes.put(arrayOfString[j], localAdTypeHandShake);
        break label469;
      }
    }
    while (true)
    {
      int k;
      try
      {
        JSONArray localJSONArray2 = paramJSONObject.optJSONArray("schemes");
        if (localJSONArray2 != null)
        {
          k = 0;
          if (k < localJSONArray2.length())
          {
            JSONObject localJSONObject2 = localJSONArray2.optJSONObject(k);
            if (localJSONObject2 == null)
              break label475;
            Scheme localScheme = new Scheme();
            localScheme.deserializeFromObj(localJSONObject2);
            this.schemes.add(localScheme);
            break label475;
          }
        }
        this.deferredViewTimeout = (1000L * paramJSONObject.optLong("deferredviewtimeout", 3600L));
        this.kill = paramJSONObject.optBoolean("kill");
        this.handShakeCallback = (1000L * paramJSONObject.optLong("handshakecallback", 86400L));
        this.hdid = paramJSONObject.optBoolean("hdid", true);
        this.creativeCacheTimeout = (1000L * paramJSONObject.optLong("creativeCacheTimeout", 259200L));
        JSONArray localJSONArray3 = paramJSONObject.optJSONArray("cachedVideos");
        if (localJSONArray3 != null)
        {
          this.cachedVideos = new String[localJSONArray3.length()];
          if (i >= localJSONArray3.length())
            break label446;
          this.cachedVideos[i] = localJSONArray3.optString(i);
          i++;
          continue;
        }
      }
      finally
      {
      }
      this.cachedVideos = new String[0];
      label446: if (this.cachedVideos.length <= 0)
        break;
      PreCacheWorker.preCacheVideos(this.cachedVideos, paramContext);
      return;
      label463: m++;
      break label23;
      label469: j++;
      break label159;
      label475: k++;
    }
  }

  private boolean loadHandShake(Context paramContext)
  {
    SharedPreferences localSharedPreferences = paramContext.getSharedPreferences("MillennialMediaSettings", 0);
    int i = 0;
    if (localSharedPreferences == null)
      return i;
    int j;
    label49: int k;
    if (localSharedPreferences.contains("handshake_deferredviewtimeout"))
    {
      this.deferredViewTimeout = localSharedPreferences.getLong("handshake_deferredviewtimeout", this.deferredViewTimeout);
      j = 1;
      if (localSharedPreferences.contains("handshake_kill"))
      {
        this.kill = localSharedPreferences.getBoolean("handshake_kill", this.kill);
        j = 1;
      }
      if (localSharedPreferences.contains("handshake_callback"))
      {
        this.handShakeCallback = localSharedPreferences.getLong("handshake_callback", this.handShakeCallback);
        j = 1;
      }
      if (localSharedPreferences.contains("handshake_hdid"))
      {
        this.hdid = localSharedPreferences.getBoolean("handshake_hdid", this.hdid);
        j = 1;
      }
      if (localSharedPreferences.contains("handshake_mmdid"))
      {
        setMMdid(paramContext, localSharedPreferences.getString("handshake_mmdid", this.mmdid), false);
        j = 1;
      }
      if (localSharedPreferences.contains("handshake_creativecachetimeout"))
      {
        this.creativeCacheTimeout = localSharedPreferences.getLong("handshake_creativecachetimeout", this.creativeCacheTimeout);
        j = 1;
      }
      String[] arrayOfString1 = MMAdView.getAdTypes();
      k = j;
      for (int m = 0; m < arrayOfString1.length; m++)
      {
        AdTypeHandShake localAdTypeHandShake = new AdTypeHandShake();
        if (localAdTypeHandShake.load(localSharedPreferences, arrayOfString1[m]))
        {
          this.adTypeHandShakes.put(arrayOfString1[m], localAdTypeHandShake);
          k = 1;
        }
      }
    }
    while (true)
    {
      try
      {
        if (localSharedPreferences.contains("handshake_schemes"))
        {
          String str = localSharedPreferences.getString("handshake_schemes", "");
          if (str.length() > 0)
          {
            String[] arrayOfString2 = str.split("/");
            int n = arrayOfString2.length;
            if (i >= n)
              break label504;
            String[] arrayOfString3 = arrayOfString2[i].split(":");
            if (arrayOfString3.length < 2)
              break label498;
            Scheme localScheme = new Scheme(arrayOfString3[0], Integer.parseInt(arrayOfString3[1]));
            this.schemes.add(localScheme);
            break label498;
            if (localSharedPreferences.contains("handshake_lasthandshake"))
            {
              this.lastHandShake = localSharedPreferences.getLong("handshake_lasthandshake", this.lastHandShake);
              i = 1;
            }
            if ((i != 0) && (System.currentTimeMillis() - this.lastHandShake < this.handShakeCallback))
              this.handler.postDelayed(this.updateHandShakeRunnable, this.handShakeCallback - (System.currentTimeMillis() - this.lastHandShake));
            if (i == 0)
              break;
            MMAdViewSDK.Log.d("Handshake successfully loaded from shared preferences.");
            return i;
          }
        }
      }
      finally
      {
      }
      i = k;
      continue;
      j = 0;
      break label49;
      label498: i++;
      continue;
      label504: i = 1;
    }
  }

  private JSONObject parseJson(String paramString)
  {
    MMAdViewSDK.Log.d("JSON String: " + paramString);
    if (paramString != null)
      try
      {
        JSONObject localJSONObject1 = new JSONObject(paramString);
        MMAdViewSDK.Log.v(localJSONObject1.toString());
        if (localJSONObject1.has("mmishake"))
        {
          JSONObject localJSONObject2 = localJSONObject1.getJSONObject("mmishake");
          return localJSONObject2;
        }
      }
      catch (JSONException localJSONException)
      {
        localJSONException.printStackTrace();
      }
    return null;
  }

  private void saveHandShake(Context paramContext)
  {
    SharedPreferences.Editor localEditor = paramContext.getSharedPreferences("MillennialMediaSettings", 0).edit();
    localEditor.putLong("handshake_deferredviewtimeout", this.deferredViewTimeout);
    localEditor.putBoolean("handshake_kill", this.kill);
    localEditor.putLong("handshake_callback", this.handShakeCallback);
    localEditor.putBoolean("handshake_hdid", this.hdid);
    localEditor.putLong("handshake_creativecachetimeout", this.creativeCacheTimeout);
    Iterator localIterator = this.adTypeHandShakes.keySet().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      ((AdTypeHandShake)this.adTypeHandShakes.get(str)).save(localEditor, (String)str);
    }
    try
    {
      StringBuilder localStringBuilder = new StringBuilder();
      for (int i = 0; i < this.schemes.size(); i++)
      {
        Scheme localScheme = (Scheme)this.schemes.get(i);
        if (i > 0)
          localStringBuilder.append("/");
        localStringBuilder.append(localScheme.scheme + ":" + localScheme.id);
      }
      localEditor.putString("handshake_schemes", localStringBuilder.toString());
      localEditor.putLong("handshake_lasthandshake", this.lastHandShake);
      localEditor.commit();
      return;
    }
    finally
    {
    }
  }

  static HandShake sharedHandShake(Context paramContext)
  {
    try
    {
      HandShake localHandShake;
      if (apid == null)
      {
        Log.e("MillennialMediaSDK", "No apid set for the handshake.");
        localHandShake = null;
        return localHandShake;
      }
      if (sharedInstance == null)
        sharedInstance = new HandShake(paramContext);
      while (true)
      {
        localHandShake = sharedInstance;
        break;
        if (System.currentTimeMillis() - sharedInstance.lastHandShake > sharedInstance.handShakeCallback)
        {
          MMAdViewSDK.Log.d("Handshake expired, requesting new handshake from the server.");
          sharedInstance = new HandShake(paramContext);
        }
      }
    }
    finally
    {
    }
  }

  boolean canDisplayCachedAd(String paramString, long paramLong)
  {
    try
    {
      AdTypeHandShake localAdTypeHandShake = (AdTypeHandShake)this.adTypeHandShakes.get(paramString);
      boolean bool1;
      if (localAdTypeHandShake != null)
        bool1 = localAdTypeHandShake.canDisplayCachedAd(paramLong);
      for (boolean bool2 = bool1; ; bool2 = false)
        return bool2;
    }
    finally
    {
    }
  }

  boolean canRequestVideo(Context paramContext, String paramString)
  {
    try
    {
      AdTypeHandShake localAdTypeHandShake = (AdTypeHandShake)this.adTypeHandShakes.get(paramString);
      boolean bool1;
      if (localAdTypeHandShake != null)
        bool1 = localAdTypeHandShake.canRequestVideo(paramContext, paramString);
      for (boolean bool2 = bool1; ; bool2 = true)
        return bool2;
    }
    finally
    {
    }
  }

  boolean canWatchVideoAd(Context paramContext, String paramString1, String paramString2)
  {
    try
    {
      AdTypeHandShake localAdTypeHandShake = (AdTypeHandShake)this.adTypeHandShakes.get(paramString1);
      boolean bool1;
      if (localAdTypeHandShake != null)
        bool1 = localAdTypeHandShake.canWatchVideoAd(paramContext, paramString1, paramString2);
      for (boolean bool2 = bool1; ; bool2 = false)
        return bool2;
    }
    finally
    {
    }
  }

  void didReceiveVideoXHeader(Context paramContext, String paramString)
  {
    try
    {
      AdTypeHandShake localAdTypeHandShake = (AdTypeHandShake)this.adTypeHandShakes.get(paramString);
      if (localAdTypeHandShake != null)
        localAdTypeHandShake.didReceiveVideoXHeader(paramContext, paramString);
      return;
    }
    finally
    {
    }
  }

  JSONArray getSchemesJSONArray(Context paramContext)
  {
    try
    {
      JSONArray localJSONArray = new JSONArray();
      if (this.schemes.size() > 0)
      {
        Iterator localIterator = this.schemes.iterator();
        while (localIterator.hasNext())
        {
          Scheme localScheme = (Scheme)localIterator.next();
          boolean bool = localScheme.checkAvailability(paramContext);
          if (bool)
            try
            {
              JSONObject localJSONObject = new JSONObject();
              localJSONObject.put("scheme", localScheme.scheme);
              localJSONObject.put("schemeid", localScheme.id);
              localJSONArray.put(localJSONObject);
            }
            catch (JSONException localJSONException)
            {
            }
        }
      }
      return localJSONArray;
    }
    finally
    {
    }
  }

  String getSchemesList(Context paramContext)
  {
    StringBuilder localStringBuilder;
    while (true)
    {
      Scheme localScheme;
      try
      {
        if ((this.schemesList != null) || (this.schemes.size() <= 0))
          break label145;
        localStringBuilder = new StringBuilder();
        Iterator localIterator = this.schemes.iterator();
        if (!localIterator.hasNext())
          break;
        localScheme = (Scheme)localIterator.next();
        if (!localScheme.checkAvailability(paramContext))
          continue;
        if (localStringBuilder.length() > 0)
        {
          localStringBuilder.append("," + localScheme.id);
          continue;
        }
      }
      finally
      {
      }
      localStringBuilder.append(Integer.toString(localScheme.id));
    }
    if (localStringBuilder.length() > 0)
      this.schemesList = localStringBuilder.toString();
    label145: String str = this.schemesList;
    return str;
  }

  boolean isAdTypeDownloading(String paramString)
  {
    try
    {
      AdTypeHandShake localAdTypeHandShake = (AdTypeHandShake)this.adTypeHandShakes.get(paramString);
      if (localAdTypeHandShake != null);
      for (boolean bool = localAdTypeHandShake.downloading; ; bool = false)
        return bool;
    }
    finally
    {
    }
  }

  void lockAdTypeDownload(String paramString)
  {
    try
    {
      AdTypeHandShake localAdTypeHandShake = (AdTypeHandShake)this.adTypeHandShakes.get(paramString);
      if (localAdTypeHandShake != null)
        localAdTypeHandShake.downloading = true;
      return;
    }
    finally
    {
    }
  }

  void overrideAdRefresh(MMAdView paramMMAdView)
  {
    if (paramMMAdView.adType != null)
    {
      AdTypeHandShake localAdTypeHandShake = (AdTypeHandShake)this.adTypeHandShakes.get(paramMMAdView.adType);
      if ((localAdTypeHandShake != null) && (localAdTypeHandShake.refreshInterval != null))
        paramMMAdView.refreshInterval = Integer.parseInt(localAdTypeHandShake.refreshInterval);
    }
  }

  void setMMdid(Context paramContext, String paramString)
  {
    setMMdid(paramContext, paramString, true);
  }

  void setMMdid(Context paramContext, String paramString, boolean paramBoolean)
  {
    if (paramString != null);
    try
    {
      if ((paramString.length() == 0) || (paramString.equals("NULL")));
      for (this.mmdid = null; ; this.mmdid = paramString)
      {
        MMAdViewSDK.setMMdid(this.mmdid);
        if (paramBoolean)
        {
          SharedPreferences.Editor localEditor = paramContext.getSharedPreferences("MillennialMediaSettings", 0).edit();
          localEditor.putString("handshake_mmdid", this.mmdid);
          localEditor.commit();
        }
        return;
      }
    }
    finally
    {
    }
  }

  void unlockAdTypeDownload(String paramString)
  {
    try
    {
      AdTypeHandShake localAdTypeHandShake = (AdTypeHandShake)this.adTypeHandShakes.get(paramString);
      if (localAdTypeHandShake != null)
        localAdTypeHandShake.downloading = false;
      return;
    }
    finally
    {
    }
  }

  void updateLastVideoViewedTime(Context paramContext, String paramString)
  {
    try
    {
      AdTypeHandShake localAdTypeHandShake = (AdTypeHandShake)this.adTypeHandShakes.get(paramString);
      if (localAdTypeHandShake != null)
        localAdTypeHandShake.updateLastVideoViewedTime(paramContext, paramString);
      return;
    }
    finally
    {
    }
  }

  private class AdTypeHandShake
  {
    boolean downloading;
    long lastVideo = 0L;
    String refreshInterval;
    long videoInterval = 0L;

    AdTypeHandShake()
    {
    }

    boolean canDisplayCachedAd(long paramLong)
    {
      return System.currentTimeMillis() - paramLong < HandShake.this.deferredViewTimeout;
    }

    boolean canRequestVideo(Context paramContext, String paramString)
    {
      MMAdViewSDK.Log.d("canRequestVideo() Current Time: " + System.currentTimeMillis() + " last video: " + this.lastVideo / 1000L + " Diff: " + (System.currentTimeMillis() - this.lastVideo) / 1000L + " Video interval: " + this.videoInterval / 1000L);
      return System.currentTimeMillis() - this.lastVideo > this.videoInterval;
    }

    // ERROR //
    boolean canWatchVideoAd(Context paramContext, String paramString1, String paramString2)
    {
      // Byte code:
      //   0: new 77	com/millennialmedia/android/AdDatabaseHelper
      //   3: dup
      //   4: aload_1
      //   5: invokespecial 80	com/millennialmedia/android/AdDatabaseHelper:<init>	(Landroid/content/Context;)V
      //   8: astore 4
      //   10: aload 4
      //   12: aload_3
      //   13: invokevirtual 84	com/millennialmedia/android/AdDatabaseHelper:getDeferredViewStart	(Ljava/lang/String;)J
      //   16: lstore 7
      //   18: aload 4
      //   20: invokevirtual 87	com/millennialmedia/android/AdDatabaseHelper:close	()V
      //   23: invokestatic 32	java/lang/System:currentTimeMillis	()J
      //   26: lload 7
      //   28: lsub
      //   29: aload_0
      //   30: getfield 17	com/millennialmedia/android/HandShake$AdTypeHandShake:this$0	Lcom/millennialmedia/android/HandShake;
      //   33: invokestatic 38	com/millennialmedia/android/HandShake:access$800	(Lcom/millennialmedia/android/HandShake;)J
      //   36: lcmp
      //   37: istore 9
      //   39: iconst_0
      //   40: istore 10
      //   42: iload 9
      //   44: ifge +19 -> 63
      //   47: aload_0
      //   48: invokestatic 32	java/lang/System:currentTimeMillis	()J
      //   51: putfield 22	com/millennialmedia/android/HandShake$AdTypeHandShake:lastVideo	J
      //   54: aload_0
      //   55: aload_1
      //   56: aload_2
      //   57: invokevirtual 91	com/millennialmedia/android/HandShake$AdTypeHandShake:save	(Landroid/content/Context;Ljava/lang/String;)V
      //   60: iconst_1
      //   61: istore 10
      //   63: iload 10
      //   65: ireturn
      //   66: astore 11
      //   68: aconst_null
      //   69: astore 4
      //   71: aload 4
      //   73: ifnull +8 -> 81
      //   76: aload 4
      //   78: invokevirtual 87	com/millennialmedia/android/AdDatabaseHelper:close	()V
      //   81: ldc 93
      //   83: ldc 95
      //   85: invokestatic 101	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
      //   88: pop
      //   89: iconst_0
      //   90: ireturn
      //   91: astore 5
      //   93: goto -22 -> 71
      //
      // Exception table:
      //   from	to	target	type
      //   0	10	66	android/database/sqlite/SQLiteException
      //   10	23	91	android/database/sqlite/SQLiteException
    }

    void deserializeFromObj(JSONObject paramJSONObject)
    {
      if (paramJSONObject == null);
      do
      {
        return;
        this.videoInterval = (1000L * paramJSONObject.optLong("videointerval"));
        this.refreshInterval = paramJSONObject.optString("adrefresh", null);
      }
      while ((this.refreshInterval == null) || (!this.refreshInterval.equalsIgnoreCase("sdk")));
      this.refreshInterval = null;
    }

    void didReceiveVideoXHeader(Context paramContext, String paramString)
    {
      this.lastVideo = System.currentTimeMillis();
      save(paramContext, paramString);
    }

    boolean load(SharedPreferences paramSharedPreferences, String paramString)
    {
      boolean bool1 = paramSharedPreferences.contains("handshake_lastvideo_" + paramString);
      boolean bool2 = false;
      if (bool1)
      {
        this.lastVideo = paramSharedPreferences.getLong("handshake_lastvideo_" + paramString, this.lastVideo);
        bool2 = true;
      }
      if (paramSharedPreferences.contains("handshake_videointerval_" + paramString))
      {
        this.videoInterval = paramSharedPreferences.getLong("handshake_videointerval_" + paramString, this.videoInterval);
        bool2 = true;
      }
      if (paramSharedPreferences.contains("handshake_adrefresh_" + paramString))
      {
        this.refreshInterval = paramSharedPreferences.getString("handshake_adrefresh_" + paramString, null);
        return true;
      }
      return bool2;
    }

    void loadLastVideo(Context paramContext, String paramString)
    {
      SharedPreferences localSharedPreferences = paramContext.getSharedPreferences("MillennialMediaSettings", 0);
      if ((localSharedPreferences != null) && (localSharedPreferences.contains("handshake_lastvideo_" + paramString)))
        this.lastVideo = localSharedPreferences.getLong("handshake_lastvideo_" + paramString, this.lastVideo);
    }

    void save(Context paramContext, String paramString)
    {
      SharedPreferences.Editor localEditor = paramContext.getSharedPreferences("MillennialMediaSettings", 0).edit();
      save(localEditor, paramString);
      localEditor.commit();
    }

    void save(SharedPreferences.Editor paramEditor, String paramString)
    {
      paramEditor.putLong("handshake_lastvideo_" + paramString, this.lastVideo);
      paramEditor.putLong("handshake_videointerval_" + paramString, this.videoInterval);
      if (this.refreshInterval != null)
        paramEditor.putString("handshake_adrefresh_" + paramString, this.refreshInterval);
    }

    void updateLastVideoViewedTime(Context paramContext, String paramString)
    {
      this.lastVideo = System.currentTimeMillis();
      save(paramContext, paramString);
    }
  }

  private class Scheme
  {
    int id;
    String scheme;

    Scheme()
    {
    }

    Scheme(String paramInt, int arg3)
    {
      this.scheme = paramInt;
      int i;
      this.id = i;
    }

    boolean checkAvailability(Context paramContext)
    {
      if (this.scheme.contains("://"));
      for (Intent localIntent = new Intent("android.intent.action.VIEW", Uri.parse(this.scheme)); paramContext.getPackageManager().queryIntentActivities(localIntent, 65536).size() > 0; localIntent = new Intent("android.intent.action.VIEW", Uri.parse(this.scheme + "://")))
        return true;
      return false;
    }

    void deserializeFromObj(JSONObject paramJSONObject)
    {
      if (paramJSONObject == null)
        return;
      this.scheme = paramJSONObject.optString("scheme", null);
      this.id = paramJSONObject.optInt("schemeid");
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.HandShake
 * JD-Core Version:    0.6.2
 */