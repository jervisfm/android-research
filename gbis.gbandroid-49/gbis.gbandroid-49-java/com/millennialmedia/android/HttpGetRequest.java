package com.millennialmedia.android;

import android.util.Log;
import java.io.IOException;
import java.net.URI;
import java.net.URLEncoder;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

class HttpGetRequest
{
  private HttpClient client = new DefaultHttpClient();
  private HttpGet getRequest = new HttpGet();

  // ERROR //
  static String convertStreamToString(java.io.InputStream paramInputStream)
  {
    // Byte code:
    //   0: aload_0
    //   1: ifnonnull +13 -> 14
    //   4: new 28	java/io/IOException
    //   7: dup
    //   8: ldc 30
    //   10: invokespecial 33	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   13: athrow
    //   14: new 35	java/io/BufferedReader
    //   17: dup
    //   18: new 37	java/io/InputStreamReader
    //   21: dup
    //   22: aload_0
    //   23: invokespecial 40	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
    //   26: invokespecial 43	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   29: astore_1
    //   30: new 45	java/lang/StringBuilder
    //   33: dup
    //   34: invokespecial 46	java/lang/StringBuilder:<init>	()V
    //   37: astore_2
    //   38: aload_1
    //   39: invokevirtual 50	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   42: astore 6
    //   44: aload 6
    //   46: ifnull +59 -> 105
    //   49: aload_2
    //   50: new 45	java/lang/StringBuilder
    //   53: dup
    //   54: invokespecial 46	java/lang/StringBuilder:<init>	()V
    //   57: aload 6
    //   59: invokevirtual 54	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   62: ldc 56
    //   64: invokevirtual 54	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   67: invokevirtual 59	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   70: invokevirtual 54	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   73: pop
    //   74: goto -36 -> 38
    //   77: astore 5
    //   79: aload 5
    //   81: invokevirtual 62	java/lang/OutOfMemoryError:printStackTrace	()V
    //   84: new 28	java/io/IOException
    //   87: dup
    //   88: ldc 64
    //   90: invokespecial 33	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   93: athrow
    //   94: astore_3
    //   95: aload_1
    //   96: ifnull +7 -> 103
    //   99: aload_1
    //   100: invokevirtual 67	java/io/BufferedReader:close	()V
    //   103: aload_3
    //   104: athrow
    //   105: aload_1
    //   106: invokevirtual 67	java/io/BufferedReader:close	()V
    //   109: aload_2
    //   110: invokevirtual 59	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   113: areturn
    //   114: astore 8
    //   116: aload 8
    //   118: invokevirtual 68	java/io/IOException:printStackTrace	()V
    //   121: goto -12 -> 109
    //   124: astore 4
    //   126: aload 4
    //   128: invokevirtual 68	java/io/IOException:printStackTrace	()V
    //   131: goto -28 -> 103
    //   134: astore_3
    //   135: aconst_null
    //   136: astore_1
    //   137: goto -42 -> 95
    //   140: astore 5
    //   142: aconst_null
    //   143: astore_1
    //   144: goto -65 -> 79
    //
    // Exception table:
    //   from	to	target	type
    //   30	38	77	java/lang/OutOfMemoryError
    //   38	44	77	java/lang/OutOfMemoryError
    //   49	74	77	java/lang/OutOfMemoryError
    //   30	38	94	finally
    //   38	44	94	finally
    //   49	74	94	finally
    //   79	94	94	finally
    //   105	109	114	java/io/IOException
    //   99	103	124	java/io/IOException
    //   14	30	134	finally
    //   14	30	140	java/lang/OutOfMemoryError
  }

  static void log(String paramString)
  {
    if ((paramString != null) && (paramString.length() > 0))
      new Thread(new HttpGetRequest.1(paramString)).start();
  }

  static void log(String[] paramArrayOfString)
  {
    if ((paramArrayOfString != null) && (paramArrayOfString.length > 0))
      new Thread(new HttpGetRequest.2(paramArrayOfString)).start();
  }

  HttpResponse get(String paramString)
  {
    try
    {
      this.getRequest.setURI(new URI(paramString));
      HttpResponse localHttpResponse = this.client.execute(this.getRequest);
      return localHttpResponse;
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
      Log.e("MillennialMediaSDK", "Out of memory!");
    }
    return null;
  }

  void trackConversion(String paramString1, String paramString2, boolean paramBoolean)
  {
    if (paramBoolean);
    for (int i = 1; ; i = 0)
    {
      HttpResponse localHttpResponse;
      try
      {
        StringBuilder localStringBuilder = new StringBuilder("http://cvt.mydas.mobi/handleConversion?goalId=" + paramString1 + "&firstlaunch=" + i);
        if (paramString2 != null)
        {
          if (!paramString2.startsWith("mmh_"))
            break label187;
          localStringBuilder.append("&hdid=" + URLEncoder.encode(paramString2));
        }
        while (true)
        {
          String str = localStringBuilder.toString();
          MMAdViewSDK.Log.d("Sending conversion tracker report: " + str);
          this.getRequest.setURI(new URI(str));
          localHttpResponse = this.client.execute(this.getRequest);
          if (localHttpResponse.getStatusLine().getStatusCode() != 200)
            break;
          MMAdViewSDK.Log.v("Conversion tracker reponse code: " + localHttpResponse.getStatusLine().getStatusCode());
          return;
          label187: localStringBuilder.append("&auid=" + URLEncoder.encode(paramString2));
        }
      }
      catch (IOException localIOException)
      {
        localIOException.printStackTrace();
        return;
      }
      Log.e("MillennialMediaSDK", "Conversion tracker unable to complete report: " + localHttpResponse.getStatusLine().getStatusCode());
      return;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.HttpGetRequest
 * JD-Core Version:    0.6.2
 */