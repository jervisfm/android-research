package com.millennialmedia.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import java.util.Hashtable;

public class MMAdView extends FrameLayout
  implements View.OnClickListener
{
  public static final String BANNER_AD_BOTTOM = "MMBannerAdBottom";
  public static final String BANNER_AD_RECTANGLE = "MMBannerAdRectangle";
  public static final String BANNER_AD_TOP = "MMBannerAdTop";
  public static final String FULLSCREEN_AD_LAUNCH = "MMFullScreenAdLaunch";
  public static final String FULLSCREEN_AD_TRANSITION = "MMFullScreenAdTransition";
  public static final String KEY_AGE = "age";
  public static final String KEY_CHILDREN = "children";
  public static final String KEY_EDUCATION = "education";
  public static final String KEY_ETHNICITY = "ethnicity";
  public static final String KEY_GENDER = "gender";
  public static final String KEY_HEIGHT = "height";
  public static final String KEY_INCOME = "income";
  public static final String KEY_KEYWORDS = "keywords";
  public static final String KEY_LATITUDE = "latitude";
  public static final String KEY_LONGITUDE = "longitude";
  public static final String KEY_MARITAL_STATUS = "marital";
  public static final String KEY_ORIENTATION = "orientation";
  public static final String KEY_POLITICS = "politics";
  public static final String KEY_VENDOR = "vendor";
  public static final String KEY_WIDTH = "width";
  public static final String KEY_ZIP_CODE = "zip";
  public static final int REFRESH_INTERVAL_OFF = -1;
  private static long nextAdViewId = 1L;
  String _goalId;
  boolean accelerate = true;
  String acid = null;
  String adType;
  Long adViewId;
  String age = null;
  String apid = "28911";
  String children = null;
  MMAdViewController controller;
  boolean deferedCallForAd;
  boolean deferedFetch;
  String education = null;
  String ethnicity = null;
  String gender = null;
  String height = null;
  boolean ignoreDensityScaling = false;
  String income = null;
  String keywords = null;
  String latitude = null;
  MMAdListener listener;
  Location location;
  String longitude = null;
  String marital = null;
  String mxsdk = null;
  String orientation = null;
  String politics = null;
  int refreshInterval = 60;
  boolean testMode = false;
  String vendor = null;
  boolean vibrate = true;
  String width = null;
  boolean xmlLayout = false;
  String zip = null;

  public MMAdView(Activity paramActivity)
  {
    super(paramActivity);
    MMAdViewSDK.Log.d("Creating new MMAdView for conversion tracking.");
    checkPermissions(paramActivity);
  }

  public MMAdView(Activity paramActivity, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramActivity, paramAttributeSet, paramInt);
    MMAdViewSDK.Log.d("Creating MMAdView from XML layout.");
    if (paramAttributeSet != null)
    {
      this.apid = paramAttributeSet.getAttributeValue("http://millennialmedia.com/android/schema", "apid");
      this.acid = paramAttributeSet.getAttributeValue("http://millennialmedia.com/android/schema", "acid");
      this.adType = paramAttributeSet.getAttributeValue("http://millennialmedia.com/android/schema", "adType");
      this.refreshInterval = paramAttributeSet.getAttributeIntValue("http://millennialmedia.com/android/schema", "refreshInterval", 60);
      this.accelerate = paramAttributeSet.getAttributeBooleanValue("http://millennialmedia.com/android/schema", "accelerate", true);
      this.ignoreDensityScaling = paramAttributeSet.getAttributeBooleanValue("http://millennialmedia.com/android/schema", "ignoreDensityScaling", false);
      this.height = paramAttributeSet.getAttributeValue("http://millennialmedia.com/android/schema", "height");
      this.width = paramAttributeSet.getAttributeValue("http://millennialmedia.com/android/schema", "width");
      this.age = paramAttributeSet.getAttributeValue("http://millennialmedia.com/android/schema", "age");
      this.gender = paramAttributeSet.getAttributeValue("http://millennialmedia.com/android/schema", "gender");
      this.zip = paramAttributeSet.getAttributeValue("http://millennialmedia.com/android/schema", "zip");
      this.income = paramAttributeSet.getAttributeValue("http://millennialmedia.com/android/schema", "income");
      this.keywords = paramAttributeSet.getAttributeValue("http://millennialmedia.com/android/schema", "keywords");
      this.ethnicity = paramAttributeSet.getAttributeValue("http://millennialmedia.com/android/schema", "ethnicity");
      this.orientation = paramAttributeSet.getAttributeValue("http://millennialmedia.com/android/schema", "orientation");
      this.marital = paramAttributeSet.getAttributeValue("http://millennialmedia.com/android/schema", "marital");
      this.children = paramAttributeSet.getAttributeValue("http://millennialmedia.com/android/schema", "children");
      this.education = paramAttributeSet.getAttributeValue("http://millennialmedia.com/android/schema", "education");
      this.politics = paramAttributeSet.getAttributeValue("http://millennialmedia.com/android/schema", "politics");
      this.vendor = paramAttributeSet.getAttributeValue("http://millennialmedia.com/android/schema", "vendor");
      this._goalId = paramAttributeSet.getAttributeValue("http://millennialmedia.com/android/schema", "goalId");
    }
    this.xmlLayout = true;
    init(paramActivity);
  }

  public MMAdView(Activity paramActivity, String paramString1, String paramString2, int paramInt)
  {
    super(paramActivity);
    MMAdViewSDK.Log.d("Creating new MMAdView.");
    this.apid = paramString1;
    this.adType = paramString2;
    this.refreshInterval = paramInt;
    init(paramActivity);
  }

  public MMAdView(Activity paramActivity, String paramString1, String paramString2, int paramInt, Hashtable<String, String> paramHashtable)
  {
    super(paramActivity);
    MMAdViewSDK.Log.d("Creating new MMAdView.");
    this.apid = paramString1;
    this.adType = paramString2;
    this.refreshInterval = paramInt;
    setMetaValues(paramHashtable);
    init(paramActivity);
  }

  public MMAdView(Activity paramActivity, String paramString1, String paramString2, int paramInt, Hashtable<String, String> paramHashtable, boolean paramBoolean)
  {
    super(paramActivity);
    MMAdViewSDK.Log.d("Creating new MMAdView.");
    this.apid = paramString1;
    this.adType = paramString2;
    this.refreshInterval = paramInt;
    setMetaValues(paramHashtable);
    this.accelerate = paramBoolean;
    init(paramActivity);
  }

  public MMAdView(Activity paramActivity, String paramString1, String paramString2, int paramInt, boolean paramBoolean)
  {
    super(paramActivity);
    MMAdViewSDK.Log.d("Creating new MMAdView.");
    this.apid = paramString1;
    this.adType = paramString2;
    this.refreshInterval = paramInt;
    this.testMode = paramBoolean;
    init(paramActivity);
  }

  public MMAdView(Activity paramActivity, String paramString1, String paramString2, int paramInt, boolean paramBoolean, Hashtable<String, String> paramHashtable)
  {
    super(paramActivity);
    MMAdViewSDK.Log.d("Creating new MMAdView.");
    this.apid = paramString1;
    this.adType = paramString2;
    this.testMode = paramBoolean;
    this.refreshInterval = paramInt;
    setMetaValues(paramHashtable);
    init(paramActivity);
  }

  public MMAdView(Activity paramActivity, String paramString1, String paramString2, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
  {
    super(paramActivity);
    MMAdViewSDK.Log.d("Creating new MMAdView.");
    this.apid = paramString1;
    this.adType = paramString2;
    this.refreshInterval = paramInt;
    this.testMode = paramBoolean1;
    this.accelerate = paramBoolean2;
    init(paramActivity);
  }

  public MMAdView(Activity paramActivity, String paramString1, String paramString2, int paramInt, boolean paramBoolean1, boolean paramBoolean2, Hashtable<String, String> paramHashtable)
  {
    super(paramActivity);
    MMAdViewSDK.Log.d("Creating new MMAdView.");
    this.apid = paramString1;
    this.adType = paramString2;
    this.refreshInterval = paramInt;
    this.testMode = paramBoolean1;
    this.accelerate = paramBoolean2;
    setMetaValues(paramHashtable);
    init(paramActivity);
  }

  public MMAdView(Activity paramActivity, String paramString1, String paramString2, boolean paramBoolean, Hashtable<String, String> paramHashtable)
  {
    super(paramActivity);
    MMAdViewSDK.Log.d("Creating new MMAdView.");
    this.apid = paramString1;
    this.adType = paramString2;
    this.refreshInterval = -1;
    this.accelerate = paramBoolean;
    setMetaValues(paramHashtable);
    init(paramActivity);
  }

  public MMAdView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this((Activity)paramContext, paramAttributeSet, 0);
  }

  private void _callForAd(boolean paramBoolean)
  {
    try
    {
      MMAdViewSDK.Log.d("callForAd");
      if ((getWindowToken() == null) && (this.xmlLayout))
      {
        this.deferedCallForAd = true;
        this.deferedFetch = paramBoolean;
        return;
      }
      if (this.refreshInterval < 0)
      {
        MMAdViewController.assignAdViewController(this);
        if (this.controller != null)
          this.controller.chooseCachedAdOrAdCall(paramBoolean);
      }
      this.deferedCallForAd = false;
      this.deferedFetch = false;
      return;
    }
    catch (Exception localException)
    {
      Log.e("MillennialMediaSDK", "There was an exception with the ad request. " + localException.getMessage());
      localException.printStackTrace();
    }
  }

  private static void checkPermissions(Activity paramActivity)
  {
    if (paramActivity.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == -1)
    {
      AlertDialog localAlertDialog3 = new AlertDialog.Builder(paramActivity).create();
      localAlertDialog3.setTitle("Whoops!");
      localAlertDialog3.setMessage("The developer has forgot to declare the READ_PHONE_STATE permission in the manifest file. Please reach out to the developer to remove this error.");
      localAlertDialog3.setButton(-3, "OK", new MMAdView.3(localAlertDialog3));
      localAlertDialog3.show();
    }
    if (paramActivity.checkCallingOrSelfPermission("android.permission.INTERNET") == -1)
    {
      AlertDialog localAlertDialog2 = new AlertDialog.Builder(paramActivity).create();
      localAlertDialog2.setTitle("Whoops!");
      localAlertDialog2.setMessage("The developer has forgot to declare the INTERNET permission in the manifest file. Please reach out to the developer to remove this error.");
      localAlertDialog2.setButton(-3, "OK", new MMAdView.4(localAlertDialog2));
      localAlertDialog2.show();
    }
    if (paramActivity.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") == -1)
    {
      AlertDialog localAlertDialog1 = new AlertDialog.Builder(paramActivity).create();
      localAlertDialog1.setTitle("Whoops!");
      localAlertDialog1.setMessage("The developer has forgot to declare the ACCESS_NETWORK_STATE permission in the manifest file. Please reach out to the developer to remove this error.");
      localAlertDialog1.setButton(-3, "OK", new MMAdView.5(localAlertDialog1));
      localAlertDialog1.show();
    }
  }

  static String[] getAdTypes()
  {
    return new String[] { "MMBannerAdTop", "MMBannerAdBottom", "MMBannerAdRectangle", "MMFullScreenAdLaunch", "MMFullScreenAdTransition" };
  }

  // ERROR //
  private void init(Activity paramActivity)
  {
    // Byte code:
    //   0: ldc_w 340
    //   3: invokestatic 166	com/millennialmedia/android/MMAdViewSDK$Log:d	(Ljava/lang/String;)V
    //   6: ldc 2
    //   8: monitorenter
    //   9: aload_0
    //   10: new 342	java/lang/Long
    //   13: dup
    //   14: getstatic 101	com/millennialmedia/android/MMAdView:nextAdViewId	J
    //   17: invokespecial 345	java/lang/Long:<init>	(J)V
    //   20: putfield 347	com/millennialmedia/android/MMAdView:adViewId	Ljava/lang/Long;
    //   23: lconst_1
    //   24: getstatic 101	com/millennialmedia/android/MMAdView:nextAdViewId	J
    //   27: ladd
    //   28: putstatic 101	com/millennialmedia/android/MMAdView:nextAdViewId	J
    //   31: new 254	java/lang/StringBuilder
    //   34: dup
    //   35: ldc_w 349
    //   38: invokespecial 258	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   41: aload_0
    //   42: getfield 347	com/millennialmedia/android/MMAdView:adViewId	Ljava/lang/Long;
    //   45: invokevirtual 352	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   48: invokevirtual 269	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   51: invokestatic 355	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
    //   54: ldc 2
    //   56: monitorexit
    //   57: aload_1
    //   58: invokestatic 169	com/millennialmedia/android/MMAdView:checkPermissions	(Landroid/app/Activity;)V
    //   61: aload_1
    //   62: ldc_w 357
    //   65: invokevirtual 284	android/app/Activity:checkCallingOrSelfPermission	(Ljava/lang/String;)I
    //   68: iconst_m1
    //   69: if_icmpne +8 -> 77
    //   72: aload_0
    //   73: iconst_0
    //   74: putfield 152	com/millennialmedia/android/MMAdView:vibrate	Z
    //   77: aload_1
    //   78: invokevirtual 361	android/app/Activity:getPackageManager	()Landroid/content/pm/PackageManager;
    //   81: astore 5
    //   83: aload 5
    //   85: new 363	android/content/ComponentName
    //   88: dup
    //   89: aload_1
    //   90: ldc_w 365
    //   93: invokespecial 368	android/content/ComponentName:<init>	(Landroid/content/Context;Ljava/lang/String;)V
    //   96: sipush 128
    //   99: invokevirtual 374	android/content/pm/PackageManager:getActivityInfo	(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    //   102: pop
    //   103: aload 5
    //   105: new 363	android/content/ComponentName
    //   108: dup
    //   109: aload_1
    //   110: ldc_w 376
    //   113: invokespecial 368	android/content/ComponentName:<init>	(Landroid/content/Context;Ljava/lang/String;)V
    //   116: sipush 128
    //   119: invokevirtual 374	android/content/pm/PackageManager:getActivityInfo	(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    //   122: pop
    //   123: aload_0
    //   124: iconst_0
    //   125: invokevirtual 380	com/millennialmedia/android/MMAdView:setBackgroundColor	(I)V
    //   128: aload_0
    //   129: aload_0
    //   130: invokevirtual 384	com/millennialmedia/android/MMAdView:setOnClickListener	(Landroid/view/View$OnClickListener;)V
    //   133: aload_0
    //   134: iconst_1
    //   135: invokevirtual 387	com/millennialmedia/android/MMAdView:setFocusable	(Z)V
    //   138: aload_0
    //   139: ldc_w 388
    //   142: invokevirtual 391	com/millennialmedia/android/MMAdView:setDescendantFocusability	(I)V
    //   145: aload_0
    //   146: getfield 112	com/millennialmedia/android/MMAdView:apid	Ljava/lang/String;
    //   149: ifnonnull +252 -> 401
    //   152: ldc 252
    //   154: ldc_w 393
    //   157: invokestatic 275	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   160: pop
    //   161: ldc 110
    //   163: putstatic 396	com/millennialmedia/android/HandShake:apid	Ljava/lang/String;
    //   166: aload_1
    //   167: invokestatic 400	com/millennialmedia/android/HandShake:sharedHandShake	(Landroid/content/Context;)Lcom/millennialmedia/android/HandShake;
    //   170: aload_0
    //   171: invokevirtual 403	com/millennialmedia/android/HandShake:overrideAdRefresh	(Lcom/millennialmedia/android/MMAdView;)V
    //   174: aload_0
    //   175: getfield 188	com/millennialmedia/android/MMAdView:adType	Ljava/lang/String;
    //   178: ldc 22
    //   180: if_acmpeq +12 -> 192
    //   183: aload_0
    //   184: getfield 188	com/millennialmedia/android/MMAdView:adType	Ljava/lang/String;
    //   187: ldc 19
    //   189: if_acmpne +222 -> 411
    //   192: aload_0
    //   193: getfield 108	com/millennialmedia/android/MMAdView:refreshInterval	I
    //   196: iconst_m1
    //   197: if_icmpeq +214 -> 411
    //   200: ldc 252
    //   202: ldc_w 405
    //   205: invokestatic 408	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;)I
    //   208: pop
    //   209: aload_0
    //   210: iconst_m1
    //   211: putfield 108	com/millennialmedia/android/MMAdView:refreshInterval	I
    //   214: return
    //   215: astore 4
    //   217: ldc 2
    //   219: monitorexit
    //   220: aload 4
    //   222: athrow
    //   223: astore_2
    //   224: ldc 252
    //   226: new 254	java/lang/StringBuilder
    //   229: dup
    //   230: ldc_w 410
    //   233: invokespecial 258	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   236: aload_2
    //   237: invokevirtual 262	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   240: invokevirtual 266	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   243: invokevirtual 269	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   246: invokestatic 275	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   249: pop
    //   250: aload_2
    //   251: invokevirtual 278	java/lang/Exception:printStackTrace	()V
    //   254: return
    //   255: astore 6
    //   257: ldc 252
    //   259: ldc_w 412
    //   262: invokestatic 275	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   265: pop
    //   266: aload 6
    //   268: invokevirtual 413	android/content/pm/PackageManager$NameNotFoundException:printStackTrace	()V
    //   271: new 286	android/app/AlertDialog$Builder
    //   274: dup
    //   275: aload_1
    //   276: invokespecial 287	android/app/AlertDialog$Builder:<init>	(Landroid/content/Context;)V
    //   279: invokevirtual 291	android/app/AlertDialog$Builder:create	()Landroid/app/AlertDialog;
    //   282: astore 8
    //   284: aload 8
    //   286: ldc_w 293
    //   289: invokevirtual 299	android/app/AlertDialog:setTitle	(Ljava/lang/CharSequence;)V
    //   292: aload 8
    //   294: ldc_w 415
    //   297: invokevirtual 304	android/app/AlertDialog:setMessage	(Ljava/lang/CharSequence;)V
    //   300: aload 8
    //   302: bipush 253
    //   304: ldc_w 306
    //   307: new 417	com/millennialmedia/android/MMAdView$1
    //   310: dup
    //   311: aload_0
    //   312: aload 8
    //   314: invokespecial 420	com/millennialmedia/android/MMAdView$1:<init>	(Lcom/millennialmedia/android/MMAdView;Landroid/app/AlertDialog;)V
    //   317: invokevirtual 315	android/app/AlertDialog:setButton	(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
    //   320: aload 8
    //   322: invokevirtual 318	android/app/AlertDialog:show	()V
    //   325: goto -222 -> 103
    //   328: astore 9
    //   330: ldc 252
    //   332: ldc_w 422
    //   335: invokestatic 275	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   338: pop
    //   339: aload 9
    //   341: invokevirtual 413	android/content/pm/PackageManager$NameNotFoundException:printStackTrace	()V
    //   344: new 286	android/app/AlertDialog$Builder
    //   347: dup
    //   348: aload_1
    //   349: invokespecial 287	android/app/AlertDialog$Builder:<init>	(Landroid/content/Context;)V
    //   352: invokevirtual 291	android/app/AlertDialog$Builder:create	()Landroid/app/AlertDialog;
    //   355: astore 11
    //   357: aload 11
    //   359: ldc_w 293
    //   362: invokevirtual 299	android/app/AlertDialog:setTitle	(Ljava/lang/CharSequence;)V
    //   365: aload 11
    //   367: ldc_w 424
    //   370: invokevirtual 304	android/app/AlertDialog:setMessage	(Ljava/lang/CharSequence;)V
    //   373: aload 11
    //   375: bipush 253
    //   377: ldc_w 306
    //   380: new 426	com/millennialmedia/android/MMAdView$2
    //   383: dup
    //   384: aload_0
    //   385: aload 11
    //   387: invokespecial 427	com/millennialmedia/android/MMAdView$2:<init>	(Lcom/millennialmedia/android/MMAdView;Landroid/app/AlertDialog;)V
    //   390: invokevirtual 315	android/app/AlertDialog:setButton	(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
    //   393: aload 11
    //   395: invokevirtual 318	android/app/AlertDialog:show	()V
    //   398: goto -275 -> 123
    //   401: aload_0
    //   402: getfield 112	com/millennialmedia/android/MMAdView:apid	Ljava/lang/String;
    //   405: putstatic 396	com/millennialmedia/android/HandShake:apid	Ljava/lang/String;
    //   408: goto -242 -> 166
    //   411: return
    //
    // Exception table:
    //   from	to	target	type
    //   9	57	215	finally
    //   0	9	223	java/lang/Exception
    //   57	77	223	java/lang/Exception
    //   77	83	223	java/lang/Exception
    //   83	103	223	java/lang/Exception
    //   103	123	223	java/lang/Exception
    //   123	166	223	java/lang/Exception
    //   166	192	223	java/lang/Exception
    //   192	214	223	java/lang/Exception
    //   217	223	223	java/lang/Exception
    //   257	325	223	java/lang/Exception
    //   330	398	223	java/lang/Exception
    //   401	408	223	java/lang/Exception
    //   83	103	255	android/content/pm/PackageManager$NameNotFoundException
    //   103	123	328	android/content/pm/PackageManager$NameNotFoundException
  }

  public static void startConversionTrackerWithGoalId(Context paramContext, String paramString)
  {
    if ((paramContext == null) || (paramString == null) || (paramString.length() == 0))
      return;
    while (true)
    {
      try
      {
        SharedPreferences localSharedPreferences = paramContext.getSharedPreferences("MillennialMediaSettings", 0);
        boolean bool = localSharedPreferences.getBoolean("firstLaunch_" + paramString, true);
        if (bool)
        {
          SharedPreferences.Editor localEditor = localSharedPreferences.edit();
          localEditor.putBoolean("firstLaunch_" + paramString, false);
          localEditor.commit();
        }
        checkPermissions((Activity)paramContext);
        String str = MMAdViewSDK.getHdid(paramContext);
        if (MMAdViewSDK.isConnected(paramContext))
        {
          new MMAdView.7(paramString, str, bool).start();
          return;
        }
      }
      finally
      {
      }
      Log.w("MillennialMediaSDK", "No network available for conversion tracking.");
    }
  }

  public void callForAd()
  {
    _callForAd(false);
  }

  public boolean check()
  {
    try
    {
      MMAdViewController localMMAdViewController = this.controller;
      boolean bool1 = false;
      if (localMMAdViewController != null)
      {
        boolean bool2 = this.controller.check(this);
        bool1 = bool2;
      }
      return bool1;
    }
    catch (Exception localException)
    {
      Log.e("MillennialMediaSDK", "There was an exception checking for a cached ad. " + localException.getMessage());
      localException.printStackTrace();
    }
    return false;
  }

  public boolean display()
  {
    try
    {
      MMAdViewController localMMAdViewController = this.controller;
      boolean bool1 = false;
      if (localMMAdViewController != null)
      {
        boolean bool2 = this.controller.display(this);
        bool1 = bool2;
      }
      return bool1;
    }
    catch (Exception localException)
    {
      Log.e("MillennialMediaSDK", "There was an exception displaying a cached ad. " + localException.getMessage());
      localException.printStackTrace();
    }
    return false;
  }

  public void fetch()
  {
    if (check())
    {
      MMAdViewSDK.Log.d("Ad already fetched and ready for display...");
      if (this.listener != null);
      try
      {
        this.listener.MMAdFailed(this);
        return;
      }
      catch (Exception localException)
      {
        Log.w("MillennialMediaSDK", "Exception raised in your MMAdListener: " + localException.getMessage());
        return;
      }
    }
    MMAdViewSDK.Log.d("Fetching new ad...");
    _callForAd(true);
  }

  public MMAdListener getListener()
  {
    return this.listener;
  }

  public void halt()
  {
  }

  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    MMAdViewSDK.Log.d("onAttachedToWindow");
    MMAdViewController.assignAdViewController(this);
    if (this.deferedCallForAd)
      _callForAd(this.deferedFetch);
  }

  public void onClick(View paramView)
  {
    onTouchEvent(MotionEvent.obtain(0L, System.currentTimeMillis(), 1, 0.0F, 0.0F, 0));
  }

  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    MMAdViewSDK.Log.d("onDetachedFromWindow");
    MMAdViewController.removeAdViewController(this, false);
  }

  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    return true;
  }

  protected void onRestoreInstanceState(Parcelable paramParcelable)
  {
    Bundle localBundle = (Bundle)paramParcelable;
    MMAdViewSDK.Log.d("onRestoreInstanceState");
    this.adViewId = new Long(localBundle.getLong("MMAdView"));
    super.onRestoreInstanceState(localBundle.getParcelable("super"));
  }

  protected Parcelable onSaveInstanceState()
  {
    MMAdViewSDK.Log.d("onSaveInstanceState");
    Bundle localBundle = new Bundle();
    localBundle.putParcelable("super", super.onSaveInstanceState());
    localBundle.putLong("MMAdView", this.adViewId.longValue());
    return localBundle;
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    int i = paramMotionEvent.getAction();
    MMAdViewSDK.Log.v(paramMotionEvent.toString());
    if ((this.controller == null) || (!isClickable()));
    while (true)
    {
      return true;
      if (i == 1)
      {
        MMAdViewSDK.Log.v("Ad clicked: action=" + i + " x=" + paramMotionEvent.getX() + " y=" + paramMotionEvent.getY());
        if ((this.controller.nextUrl.startsWith("mmvideo")) || (this.controller.nextUrl.endsWith("content.once")))
          this.controller.shouldLaunchToOverlay = true;
        if (this.controller.shouldLaunchToOverlay == true)
        {
          if (this.listener != null);
          try
          {
            this.listener.MMAdClickedToOverlay(this);
            if (this.controller.nextUrl != null)
            {
              this.controller.handleClick(this.controller.nextUrl);
              return true;
            }
          }
          catch (Exception localException2)
          {
            while (true)
              Log.w("MillennialMediaSDK", "Exception raised in your MMAdListener: ", localException2);
            Log.e("MillennialMediaSDK", "No ad returned, no overlay launched");
            return true;
          }
        }
        if (this.controller.nextUrl != null)
        {
          MMAdViewSDK.Log.d("Ad clicked, launching new browser");
          Activity localActivity = (Activity)getContext();
          if (localActivity == null)
          {
            Log.e("MillennialMediaSDK", "The reference to the ad view was broken.");
            return true;
          }
          try
          {
            Intent localIntent = new Intent("android.intent.action.VIEW", Uri.parse(this.controller.nextUrl));
            localIntent.setFlags(603979776);
            localActivity.startActivity(localIntent);
            if (this.listener != null)
              try
              {
                this.listener.MMAdClickedToNewBrowser(this);
                return true;
              }
              catch (Exception localException1)
              {
                Log.w("MillennialMediaSDK", "Exception raised in your MMAdListener: ", localException1);
                return true;
              }
          }
          catch (ActivityNotFoundException localActivityNotFoundException)
          {
            while (true)
              MMAdViewSDK.Log.d("Could not find activity for: " + this.controller.nextUrl);
          }
        }
      }
    }
    Log.e("MillennialMediaSDK", "No ad returned, no new browser launched");
    return true;
  }

  protected void onVisibilityChanged(View paramView, int paramInt)
  {
    if (this.controller != null)
    {
      if (paramInt != 0)
        break label56;
      this.controller.resumeTimer(false);
    }
    while (true)
    {
      StringBuilder localStringBuilder = new StringBuilder("Window Visibility Changed. Window is visible?: ");
      boolean bool = false;
      if (paramInt == 0)
        bool = true;
      MMAdViewSDK.Log.d(Boolean.toString(bool));
      return;
      label56: this.controller.pauseTimer(false);
    }
  }

  public void onWindowFocusChanged(boolean paramBoolean)
  {
    super.onWindowFocusChanged(paramBoolean);
    if (this.controller != null)
    {
      if (!paramBoolean)
        break label73;
      this.controller.resumeTimer(false);
    }
    while (true)
    {
      MMAdViewSDK.Log.d("Window Focus Changed. Window in focus?: " + paramBoolean);
      if (!paramBoolean)
      {
        Activity localActivity = (Activity)getContext();
        if ((localActivity == null) || (localActivity.isFinishing()))
          MMAdViewController.removeAdViewController(this, true);
      }
      return;
      label73: this.controller.pauseTimer(false);
    }
  }

  public void pause()
  {
    if (this.controller != null)
      this.controller.pauseTimer(true);
  }

  public void resume()
  {
    if (this.controller != null)
      this.controller.resumeTimer(true);
  }

  public void setAcid(String paramString)
  {
    this.acid = paramString;
  }

  public void setAdType(String paramString)
  {
    this.adType = paramString;
  }

  public void setAge(String paramString)
  {
    this.age = paramString;
  }

  public void setApid(String paramString)
  {
    this.apid = paramString;
  }

  public void setEducation(String paramString)
  {
    this.education = paramString;
  }

  public void setEthnicity(String paramString)
  {
    this.ethnicity = paramString;
  }

  public void setGender(String paramString)
  {
    this.gender = paramString;
  }

  public void setHeight(String paramString)
  {
    this.height = paramString;
  }

  public void setIgnoresDensityScaling(boolean paramBoolean)
  {
    try
    {
      this.ignoreDensityScaling = paramBoolean;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void setIncome(String paramString)
  {
    this.income = paramString;
  }

  public void setLatitude(String paramString)
  {
    this.latitude = paramString;
  }

  public void setListener(MMAdListener paramMMAdListener)
  {
    try
    {
      this.listener = paramMMAdListener;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void setLongitude(String paramString)
  {
    this.longitude = paramString;
  }

  public void setMarital(String paramString)
  {
    this.marital = paramString;
  }

  public void setMetaValues(Hashtable<String, String> paramHashtable)
  {
    if (paramHashtable == null);
    do
    {
      return;
      if (paramHashtable.containsKey("age"))
        this.age = ((String)paramHashtable.get("age"));
      if (paramHashtable.containsKey("gender"))
        this.gender = ((String)paramHashtable.get("gender"));
      if (paramHashtable.containsKey("zip"))
        this.zip = ((String)paramHashtable.get("zip"));
      if (paramHashtable.containsKey("marital"))
        this.marital = ((String)paramHashtable.get("marital"));
      if (paramHashtable.containsKey("income"))
        this.income = ((String)paramHashtable.get("income"));
      if (paramHashtable.containsKey("keywords"))
        this.keywords = ((String)paramHashtable.get("keywords"));
      if (paramHashtable.containsKey("mmacid"))
        this.acid = ((String)paramHashtable.get("mmacid"));
      if (paramHashtable.containsKey("mxsdk"))
        this.mxsdk = ((String)paramHashtable.get("mxsdk"));
      if (paramHashtable.containsKey("height"))
        this.height = ((String)paramHashtable.get("height"));
      if (paramHashtable.containsKey("width"))
        this.width = ((String)paramHashtable.get("width"));
      if (paramHashtable.containsKey("ethnicity"))
        this.ethnicity = ((String)paramHashtable.get("ethnicity"));
      if (paramHashtable.containsKey("orientation"))
        this.orientation = ((String)paramHashtable.get("orientation"));
      if (paramHashtable.containsKey("education"))
        this.education = ((String)paramHashtable.get("education"));
      if (paramHashtable.containsKey("children"))
        this.children = ((String)paramHashtable.get("children"));
      if (paramHashtable.containsKey("politics"))
        this.politics = ((String)paramHashtable.get("politics"));
    }
    while (!paramHashtable.containsKey("vendor"));
    this.vendor = ((String)paramHashtable.get("vendor"));
  }

  public void setMxsdk(String paramString)
  {
    this.mxsdk = paramString;
  }

  public void setOrientation(String paramString)
  {
    this.orientation = paramString;
  }

  public void setPolitics(String paramString)
  {
    this.politics = paramString;
  }

  public void setVendor(String paramString)
  {
    this.vendor = paramString;
  }

  public void setWidth(String paramString)
  {
    this.width = paramString;
  }

  public void setZip(String paramString)
  {
    this.zip = paramString;
  }

  public void startConversionTrackerWithGoalId(String paramString)
  {
    if ((paramString == null) || (paramString.length() == 0))
      return;
    this._goalId = paramString;
    SharedPreferences localSharedPreferences = getContext().getSharedPreferences("MillennialMediaSettings", 0);
    boolean bool = localSharedPreferences.getBoolean("firstLaunch_" + this._goalId, true);
    if (bool)
    {
      SharedPreferences.Editor localEditor = localSharedPreferences.edit();
      localEditor.putBoolean("firstLaunch_" + this._goalId, false);
      localEditor.commit();
    }
    if (MMAdViewSDK.isConnected(getContext()))
    {
      new MMAdView.6(this, MMAdViewSDK.getAuidOrHdid(getContext()), bool).start();
      return;
    }
    Log.w("MillennialMediaSDK", "No network available for conversion tracking.");
  }

  public void updateUserLocation(Location paramLocation)
  {
    if (paramLocation == null)
      return;
    this.latitude = String.valueOf(paramLocation.getLatitude());
    this.longitude = String.valueOf(paramLocation.getLongitude());
    this.location = paramLocation;
  }

  public static abstract interface MMAdListener
  {
    public abstract void MMAdCachingCompleted(MMAdView paramMMAdView, boolean paramBoolean);

    public abstract void MMAdClickedToNewBrowser(MMAdView paramMMAdView);

    public abstract void MMAdClickedToOverlay(MMAdView paramMMAdView);

    public abstract void MMAdFailed(MMAdView paramMMAdView);

    public abstract void MMAdOverlayLaunched(MMAdView paramMMAdView);

    public abstract void MMAdRequestIsCaching(MMAdView paramMMAdView);

    public abstract void MMAdReturned(MMAdView paramMMAdView);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.MMAdView
 * JD-Core Version:    0.6.2
 */