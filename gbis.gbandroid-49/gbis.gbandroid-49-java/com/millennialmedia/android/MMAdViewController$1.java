package com.millennialmedia.android;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.net.URLEncoder;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;

class MMAdViewController$1 extends Thread
{
  MMAdViewController$1(MMAdViewController paramMMAdViewController, boolean paramBoolean)
  {
  }

  public void run()
  {
    MMAdView localMMAdView = (MMAdView)MMAdViewController.access$100(this.this$0).get();
    if (localMMAdView == null)
    {
      Log.e("MillennialMediaSDK", "The reference to the ad view was broken.");
      this.this$0.requestInProgress = false;
      return;
    }
    if (localMMAdView.apid == null)
    {
      MMAdViewController.access$200(this.this$0, localMMAdView);
      Log.e("MillennialMediaSDK", "MMAdView found with a null apid. New ad requests on this MMAdView are disabled until an apid has been assigned.");
      this.this$0.requestInProgress = false;
      return;
    }
    if (MMAdViewSDK.isConnected(localMMAdView.getContext()));
    try
    {
      str5 = MMAdViewController.access$300(this.this$0, localMMAdView);
      str6 = MMAdViewController.getURLDeviceValues(localMMAdView.getContext());
      if (localMMAdView.testMode)
        Log.w("MillennialMediaSDK", "*********** Advertising test mode is deprecated. Refer to wiki.millennialmedia.com for testing information. ");
      String str7 = MMAdViewController.access$400(this.this$0, localMMAdView.adType);
      str1 = str7;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException1)
    {
      while (true)
      {
        String str1;
        try
        {
          String str5;
          String str6;
          DisplayMetrics localDisplayMetrics = localMMAdView.getContext().getResources().getDisplayMetrics();
          float f = localDisplayMetrics.density;
          int n = localDisplayMetrics.heightPixels;
          int i1 = localDisplayMetrics.widthPixels;
          StringBuilder localStringBuilder = new StringBuilder("http://androidsdk.ads.mp.mydas.mobi/getAd.php5?sdkapid=" + URLEncoder.encode(localMMAdView.apid, "UTF-8"));
          String str8 = MMAdViewSDK.getAuidOrHdid(localMMAdView.getContext());
          HttpResponse localHttpResponse;
          if (str8 != null)
          {
            if (str8.startsWith("mmh_"))
              localStringBuilder.append("&hdid=" + URLEncoder.encode(str8, "UTF-8"));
          }
          else
          {
            String str9 = MMAdViewSDK.getMMdid(localMMAdView.getContext());
            if (str9 != null)
              localStringBuilder.append("&mmdid=" + URLEncoder.encode(str9, "UTF-8"));
            if (MMAdViewController.access$500(this.this$0) == null)
              continue;
            localStringBuilder.append("&ua=" + URLEncoder.encode(MMAdViewController.access$500(this.this$0), "UTF-8"));
            if (Build.MODEL != null)
              localStringBuilder.append("&dm=" + URLEncoder.encode(Build.MODEL, "UTF-8"));
            if (Build.VERSION.RELEASE != null)
              localStringBuilder.append("&dv=Android" + URLEncoder.encode(Build.VERSION.RELEASE, "UTF-8"));
            localStringBuilder.append("&density=" + Float.toString(f));
            localStringBuilder.append("&hpx=" + n);
            localStringBuilder.append("&wpx=" + i1);
            localStringBuilder.append("&mmisdk=" + URLEncoder.encode("4.5.1-12.2.2.a", "UTF-8") + str5 + str6);
            Context localContext2 = localMMAdView.getContext();
            if (!HandShake.sharedHandShake(localContext2).canRequestVideo(localContext2, localMMAdView.adType))
              continue;
            localStringBuilder.append("&video=true");
            if ((localMMAdView.getContext().checkCallingOrSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE") != -1) && ((!Build.VERSION.SDK.equalsIgnoreCase("8")) || (Environment.getExternalStorageState().equals("mounted"))))
              continue;
            localStringBuilder.append("&cachedvideo=false");
            String str10 = localStringBuilder.toString();
            str2 = str1;
            str3 = str10;
            if (str2 != null)
              str3 = str3 + str2;
            Log.i("MillennialMediaSDK", "Calling for an advertisement: " + str3);
            try
            {
              localHttpResponse = new HttpGetRequest().get(str3);
              if (localHttpResponse != null)
                continue;
              Log.e("MillennialMediaSDK", "HTTP response is null");
              this.this$0.requestInProgress = false;
              MMAdViewController.access$200(this.this$0, localMMAdView);
              return;
            }
            catch (Exception localException)
            {
              Log.e("MillennialMediaSDK", "HTTP error: ", localException);
              this.this$0.requestInProgress = false;
              MMAdViewController.access$200(this.this$0, localMMAdView);
              return;
            }
          }
          localStringBuilder.append("&auid=" + URLEncoder.encode(str8, "UTF-8"));
          continue;
          localStringBuilder.append("&ua=UNKNOWN");
          continue;
          localStringBuilder.append("&video=false");
          continue;
          localStringBuilder.append("&cachedvideo=true");
          continue;
          HttpEntity localHttpEntity = localHttpResponse.getEntity();
          if (localHttpEntity == null)
          {
            Log.i("MillennialMediaSDK", "Null HTTP entity");
            this.this$0.requestInProgress = false;
            MMAdViewController.access$200(this.this$0, localMMAdView);
            return;
          }
          if (localHttpEntity.getContentLength() == 0L)
          {
            Log.i("MillennialMediaSDK", "Millennial ad return failed. Zero content length returned.");
            this.this$0.requestInProgress = false;
            MMAdViewController.access$200(this.this$0, localMMAdView);
            return;
          }
          Header[] arrayOfHeader = localHttpResponse.getHeaders("Set-Cookie");
          int i = arrayOfHeader.length;
          int j = 0;
          if (j < i)
          {
            String str4 = arrayOfHeader[j].getValue();
            int k = str4.indexOf("MAC-ID=");
            if (k >= 0)
            {
              int m = str4.indexOf(';', k);
              if (m > k)
                MMAdViewSDK.macId = str4.substring(k + 7, m);
            }
            j++;
            continue;
          }
          Header localHeader1 = localHttpEntity.getContentType();
          if (localHeader1 != null)
          {
            if (localHeader1.getValue() != null)
            {
              if (localHeader1.getValue().equalsIgnoreCase("application/json"))
                try
                {
                  VideoAd localVideoAd = new VideoAd(HttpGetRequest.convertStreamToString(localHttpEntity.getContent()));
                  Log.i("MillennialMediaSDK", "Current environment: " + Environment.getExternalStorageState());
                  if (Environment.getExternalStorageState().equals("mounted"))
                    localVideoAd.storedOnSdCard = true;
                  if (localVideoAd.isValid())
                  {
                    Log.i("MillennialMediaSDK", "Cached video ad JSON received: " + localVideoAd.id);
                    MMAdViewController.access$600(this.this$0, localVideoAd, this.val$fetch);
                  }
                  this.this$0.requestInProgress = false;
                  return;
                }
                catch (IllegalStateException localIllegalStateException)
                {
                  localIllegalStateException.printStackTrace();
                  Log.i("MillennialMediaSDK", "Millennial ad return failed. Invalid response data.");
                  this.this$0.requestInProgress = false;
                  MMAdViewController.access$200(this.this$0, localMMAdView);
                  return;
                }
                catch (IOException localIOException2)
                {
                  localIOException2.printStackTrace();
                  Log.i("MillennialMediaSDK", "Millennial ad return failed. " + localIOException2.getMessage());
                  this.this$0.requestInProgress = false;
                  MMAdViewController.access$200(this.this$0, localMMAdView);
                  return;
                }
              if (localHeader1.getValue().equalsIgnoreCase("text/html"))
              {
                Header localHeader2 = localHttpResponse.getFirstHeader("X-MM-Video");
                if ((localHeader2 != null) && (localHeader2.getValue().equalsIgnoreCase("true")))
                {
                  Context localContext1 = localMMAdView.getContext();
                  HandShake.sharedHandShake(localContext1).didReceiveVideoXHeader(localContext1, localMMAdView.adType);
                }
                try
                {
                  if ((!this.val$fetch) || (!localMMAdView.adType.equalsIgnoreCase("MMFullScreenAdLaunch")))
                    continue;
                  MMAdViewController.access$702(this.this$0, HttpGetRequest.convertStreamToString(localHttpEntity.getContent()));
                  MMAdViewController.access$802(this.this$0, str3.substring(0, 1 + str3.lastIndexOf("/")));
                  MMAdViewController.access$902(this.this$0, System.currentTimeMillis());
                  MMAdViewController.access$1000(this.this$0, localMMAdView);
                  MMAdViewController.access$1100(this.this$0, localMMAdView, true);
                }
                catch (IOException localIOException1)
                {
                  Log.e("MillennialMediaSDK", "Exception raised in HTTP stream: ", localIOException1);
                  MMAdViewController.access$200(this.this$0, localMMAdView);
                }
                continue;
                if ((this.val$fetch) && (localMMAdView.adType.equalsIgnoreCase("MMFullScreenAdTransition")))
                {
                  MMAdViewController.access$1202(this.this$0, HttpGetRequest.convertStreamToString(localHttpEntity.getContent()));
                  MMAdViewController.access$1302(this.this$0, str3.substring(0, 1 + str3.lastIndexOf("/")));
                  MMAdViewController.access$1402(this.this$0, System.currentTimeMillis());
                  MMAdViewController.access$1000(this.this$0, localMMAdView);
                  MMAdViewController.access$1100(this.this$0, localMMAdView, true);
                  continue;
                }
                MMAdViewController.access$1500(this.this$0, HttpGetRequest.convertStreamToString(localHttpEntity.getContent()), str3.substring(0, 1 + str3.lastIndexOf("/")), localMMAdView);
                continue;
              }
              Log.i("MillennialMediaSDK", "Millennial ad return failed. Invalid mime type returned.");
              MMAdViewController.access$200(this.this$0, localMMAdView);
              continue;
            }
            Log.i("MillennialMediaSDK", "Millennial ad return failed. HTTP Header value null.");
            MMAdViewController.access$200(this.this$0, localMMAdView);
            continue;
          }
          Log.i("MillennialMediaSDK", "Millennial ad return failed. HTTP Header is null.");
          MMAdViewController.access$200(this.this$0, localMMAdView);
          continue;
          Log.i("MillennialMediaSDK", "No network available, can't call for ads.");
          MMAdViewController.access$200(this.this$0, localMMAdView);
          continue;
          localUnsupportedEncodingException1 = localUnsupportedEncodingException1;
          str1 = null;
        }
        catch (UnsupportedEncodingException localUnsupportedEncodingException2)
        {
        }
        String str2 = str1;
        String str3 = null;
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.MMAdViewController.1
 * JD-Core Version:    0.6.2
 */