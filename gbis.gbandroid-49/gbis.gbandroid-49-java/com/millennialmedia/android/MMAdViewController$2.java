package com.millennialmedia.android;

import android.util.Log;
import android.webkit.WebView;
import java.lang.ref.WeakReference;

class MMAdViewController$2 extends MMWebViewClient
{
  MMAdViewController$2(MMAdViewController paramMMAdViewController)
  {
  }

  public void onPageFinished(WebView paramWebView, String paramString)
  {
    paramWebView.loadUrl("javascript:window.interface.setLoaded(true);");
    paramWebView.loadUrl("javascript:window.interface.getUrl(document.links[0].href);");
    MMAdView localMMAdView = (MMAdView)MMAdViewController.access$100(this.this$0).get();
    if (localMMAdView != null)
      localMMAdView.setClickable(true);
    if (paramWebView != null)
      paramWebView.clearCache(true);
  }

  public void onScaleChanged(WebView paramWebView, float paramFloat1, float paramFloat2)
  {
    Log.e("MillennialMediaSDK", "Scale Changed");
  }

  public boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
  {
    super.shouldOverrideUrlLoading(paramWebView, paramString);
    return true;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.MMAdViewController.2
 * JD-Core Version:    0.6.2
 */