package com.millennialmedia.android;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

class MMAdViewController$5 extends Thread
{
  MMAdViewController$5(MMAdViewController paramMMAdViewController)
  {
  }

  public void run()
  {
    MMAdViewSDK.Log.d("Touch occured, opening ad...");
    if (MMAdViewController.access$1700(this.this$0) == null);
    while (true)
    {
      return;
      MMAdView localMMAdView = (MMAdView)MMAdViewController.access$100(this.this$0).get();
      if (localMMAdView == null)
      {
        Log.e("MillennialMediaSDK", "The reference to the ad view was broken.");
        return;
      }
      Activity localActivity = (Activity)localMMAdView.getContext();
      if (localActivity == null)
      {
        Log.e("MillennialMediaSDK", "The ad view does not have a parent activity.");
        return;
      }
      String str1 = null;
      label66: String str2 = MMAdViewController.access$1700(this.this$0);
      try
      {
        URL localURL = new URL(MMAdViewController.access$1700(this.this$0));
        HttpURLConnection.setFollowRedirects(false);
        HttpURLConnection localHttpURLConnection = (HttpURLConnection)localURL.openConnection();
        localHttpURLConnection.setRequestMethod("GET");
        localHttpURLConnection.connect();
        MMAdViewController.access$1702(this.this$0, localHttpURLConnection.getHeaderField("Location"));
        str1 = localHttpURLConnection.getHeaderField("Content-Type");
        int i = localHttpURLConnection.getResponseCode();
        MMAdViewSDK.Log.d("Response: " + localHttpURLConnection.getResponseCode() + " " + localHttpURLConnection.getResponseMessage());
        MMAdViewSDK.Log.d("urlString: " + MMAdViewController.access$1700(this.this$0));
        if ((i >= 300) && (i < 400))
          break label66;
        str3 = str1;
        if (str2 != null)
        {
          if (str3 == null)
            str3 = "";
          MMAdViewSDK.Log.d(str2);
          localUri = Uri.parse(str2);
          if ((localUri != null) && (localUri.getScheme() != null) && (str3 != null))
            try
            {
              if (((localUri.getScheme().equalsIgnoreCase("http")) || (localUri.getScheme().equalsIgnoreCase("https"))) && (str3.equalsIgnoreCase("text/html")))
              {
                Intent localIntent1 = new Intent(localActivity, MMAdViewOverlayActivity.class);
                localIntent1.setFlags(603979776);
                localIntent1.putExtra("canAccelerate", this.this$0.canAccelerate);
                localIntent1.putExtra("overlayTransition", this.this$0.overlayTransition);
                localIntent1.putExtra("transitionTime", this.this$0.transitionTime);
                localIntent1.putExtra("shouldResizeOverlay", this.this$0.shouldResizeOverlay);
                localIntent1.putExtra("shouldShowTitlebar", this.this$0.shouldShowTitlebar);
                localIntent1.putExtra("shouldShowBottomBar", this.this$0.shouldShowBottomBar);
                localIntent1.putExtra("shouldEnableBottomBar", this.this$0.shouldEnableBottomBar);
                localIntent1.putExtra("shouldMakeOverlayTransparent", this.this$0.shouldMakeOverlayTransparent);
                localIntent1.putExtra("overlayTitle", this.this$0.overlayTitle);
                MMAdViewSDK.Log.v("Accelerometer on?: " + this.this$0.canAccelerate);
                localIntent1.setData(localUri);
                localActivity.startActivityForResult(localIntent1, 0);
                return;
              }
            }
            catch (ActivityNotFoundException localActivityNotFoundException)
            {
              Log.e("MillennialMediaSDK", localActivityNotFoundException.getMessage());
              return;
            }
        }
      }
      catch (MalformedURLException localMalformedURLException)
      {
        while (true)
          str3 = str1;
      }
      catch (IOException localIOException)
      {
        String str3;
        Uri localUri;
        while (true)
          str3 = str1;
        if (localUri.getScheme().equalsIgnoreCase("mmvideo"))
        {
          String str4 = localUri.getHost();
          if (str4 != null)
          {
            MMAdViewSDK.Log.v("mmvideo: attempting to play video " + str4);
            if (MMAdViewController.access$1800(str4, localMMAdView))
            {
              if (MMAdViewController.access$1900(localMMAdView.getContext(), str4, null))
              {
                if (!MMAdViewController.access$2000(this.this$0, str4, localMMAdView))
                {
                  HandShake.sharedHandShake(localMMAdView.getContext()).updateLastVideoViewedTime(localMMAdView.getContext(), localMMAdView.adType);
                  MMAdViewController.access$2100(this.this$0, str4, localMMAdView);
                  return;
                }
                MMAdViewSDK.Log.v("mmvideo: Ad is expired.");
                return;
              }
              MMAdViewSDK.Log.v("mmvideo: Ad is not in the filesystem.");
              return;
            }
            MMAdViewSDK.Log.v("mmvideo: Ad is not in the database.");
          }
        }
        else
        {
          if (localUri.getScheme().equalsIgnoreCase("market"))
          {
            MMAdViewSDK.Log.v("Android Market URL, launch the Market Application");
            Intent localIntent9 = new Intent("android.intent.action.VIEW", localUri);
            localIntent9.setFlags(603979776);
            localActivity.startActivity(localIntent9);
            return;
          }
          if ((localUri.getScheme().equalsIgnoreCase("rtsp")) || ((localUri.getScheme().equalsIgnoreCase("http")) && ((str3.equalsIgnoreCase("video/mp4")) || (str3.equalsIgnoreCase("video/3gpp")))))
          {
            MMAdViewSDK.Log.v("Video, launch the video player for video at: " + localUri);
            Intent localIntent2 = new Intent(localActivity, VideoPlayer.class);
            localIntent2.setFlags(603979776);
            localIntent2.setData(localUri);
            localActivity.startActivityForResult(localIntent2, 0);
            return;
          }
          if (localUri.getScheme().equalsIgnoreCase("tel"))
          {
            MMAdViewSDK.Log.v("Telephone Number, launch the phone");
            Intent localIntent8 = new Intent("android.intent.action.DIAL", localUri);
            localIntent8.setFlags(603979776);
            localActivity.startActivity(localIntent8);
            return;
          }
          if (localUri.getScheme().equalsIgnoreCase("geo"))
          {
            MMAdViewSDK.Log.v("Google Maps");
            Intent localIntent7 = new Intent("android.intent.action.VIEW", localUri);
            localIntent7.setFlags(603979776);
            localActivity.startActivity(localIntent7);
            return;
          }
          if ((localUri.getScheme().equalsIgnoreCase("http")) && (localUri.getLastPathSegment() != null))
          {
            if ((localUri.getLastPathSegment().endsWith(".mp4")) || (localUri.getLastPathSegment().endsWith(".3gp")))
            {
              MMAdViewSDK.Log.v("Video, launch the video player for video at: " + localUri);
              Intent localIntent5 = new Intent(localActivity, VideoPlayer.class);
              localIntent5.setFlags(603979776);
              localIntent5.setData(localUri);
              localActivity.startActivityForResult(localIntent5, 0);
              return;
            }
            Intent localIntent6 = new Intent(localActivity, MMAdViewOverlayActivity.class);
            localIntent6.setFlags(603979776);
            localIntent6.putExtra("canAccelerate", this.this$0.canAccelerate);
            localIntent6.putExtra("overlayTransition", this.this$0.overlayTransition);
            localIntent6.putExtra("transitionTime", this.this$0.transitionTime);
            localIntent6.putExtra("shouldResizeOverlay", this.this$0.shouldResizeOverlay);
            localIntent6.putExtra("shouldShowTitlebar", this.this$0.shouldShowTitlebar);
            localIntent6.putExtra("shouldShowBottomBar", this.this$0.shouldShowBottomBar);
            localIntent6.putExtra("shouldEnableBottomBar", this.this$0.shouldEnableBottomBar);
            localIntent6.putExtra("shouldMakeOverlayTransparent", this.this$0.shouldMakeOverlayTransparent);
            localIntent6.putExtra("overlayTitle", this.this$0.overlayTitle);
            MMAdViewSDK.Log.v("Accelerometer on?: " + this.this$0.canAccelerate);
            localIntent6.setData(localUri);
            localActivity.startActivityForResult(localIntent6, 0);
            return;
          }
          if (localUri.getScheme().equalsIgnoreCase("http"))
          {
            Intent localIntent3 = new Intent(localActivity, MMAdViewOverlayActivity.class);
            localIntent3.setFlags(603979776);
            localIntent3.putExtra("canAccelerate", this.this$0.canAccelerate);
            localIntent3.putExtra("overlayTransition", this.this$0.overlayTransition);
            localIntent3.putExtra("transitionTime", this.this$0.transitionTime);
            localIntent3.putExtra("shouldResizeOverlay", this.this$0.shouldResizeOverlay);
            localIntent3.putExtra("shouldShowTitlebar", this.this$0.shouldShowTitlebar);
            localIntent3.putExtra("shouldShowBottomBar", this.this$0.shouldShowBottomBar);
            localIntent3.putExtra("shouldEnableBottomBar", this.this$0.shouldEnableBottomBar);
            localIntent3.putExtra("shouldMakeOverlayTransparent", this.this$0.shouldMakeOverlayTransparent);
            localIntent3.putExtra("overlayTitle", this.this$0.overlayTitle);
            MMAdViewSDK.Log.v("Accelerometer on?: " + this.this$0.canAccelerate);
            localIntent3.setData(localUri);
            localActivity.startActivityForResult(localIntent3, 0);
            return;
          }
          Intent localIntent4 = new Intent("android.intent.action.VIEW", localUri);
          localIntent4.setFlags(603979776);
          localActivity.startActivity(localIntent4);
        }
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.MMAdViewController.5
 * JD-Core Version:    0.6.2
 */