package com.millennialmedia.android;

import android.os.Handler;
import android.util.Log;
import java.lang.ref.WeakReference;

class MMAdViewController$6
  implements Runnable
{
  MMAdViewController$6(MMAdViewController paramMMAdViewController)
  {
  }

  public void run()
  {
    this.this$0.chooseCachedAdOrAdCall(false);
    MMAdView localMMAdView = (MMAdView)MMAdViewController.access$100(this.this$0).get();
    if (localMMAdView == null)
    {
      Log.e("MillennialMediaSDK", "The reference to the ad view was broken.");
      return;
    }
    MMAdViewController.access$2200(this.this$0).postDelayed(this, 1000 * localMMAdView.refreshInterval);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.MMAdViewController.6
 * JD-Core Version:    0.6.2
 */