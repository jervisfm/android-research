package com.millennialmedia.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.StatFs;
import android.os.SystemClock;
import android.os.Vibrator;
import android.util.Log;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebSettings;
import android.webkit.WebView;
import java.io.File;
import java.lang.ref.WeakReference;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;

class MMAdViewController
{
  private static String cachedVideoList;
  private static boolean cachedVideoListLoaded;
  private static HashSet cachedVideoSet;
  private static final HashMap<Long, MMAdViewController> controllers = new HashMap();
  private WeakReference<MMAdView> adViewRef;
  private boolean appPaused;
  private Handler cacheHandler = new Handler(Looper.getMainLooper());
  boolean canAccelerate;
  private String fetchedBaseUrlLaunch;
  private String fetchedBaseUrlTransition;
  private String fetchedContentLaunch;
  private String fetchedContentTransition;
  private long fetchedTimeLaunch;
  private long fetchedTimeTransition;
  private Handler handler;
  String nextUrl;
  String overlayTitle;
  String overlayTransition;
  private boolean paused = true;
  private boolean refreshTimerOn;
  boolean requestInProgress;
  private Runnable runnable = new MMAdViewController.6(this);
  boolean shouldEnableBottomBar;
  boolean shouldLaunchToOverlay;
  boolean shouldMakeOverlayTransparent;
  int shouldResizeOverlay;
  boolean shouldShowBottomBar;
  boolean shouldShowTitlebar;
  private long timeRemaining;
  private long timeResumed;
  long transitionTime;
  private String urlString;
  private String useragent;
  private WebView webView;

  private MMAdViewController(MMAdView paramMMAdView)
  {
    resetAdViewSettings();
    this.adViewRef = new WeakReference(paramMMAdView);
    this.webView = new WebView(paramMMAdView.getContext().getApplicationContext());
    this.webView.getSettings().setJavaScriptEnabled(true);
    this.webView.getSettings().setCacheMode(2);
    this.webView.setBackgroundColor(0);
    this.webView.setWillNotDraw(false);
    this.webView.addJavascriptInterface(new MMJSInterface(null), "interface");
    this.webView.setId(15063);
    this.useragent = (this.webView.getSettings().getUserAgentString() + Build.MODEL);
  }

  // ERROR //
  private void DownloadLastAd(MMAdView paramMMAdView, boolean paramBoolean)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: aload_1
    //   3: invokevirtual 101	com/millennialmedia/android/MMAdView:getContext	()Landroid/content/Context;
    //   6: ldc 172
    //   8: iconst_0
    //   9: invokevirtual 176	android/content/Context:getSharedPreferences	(Ljava/lang/String;I)Landroid/content/SharedPreferences;
    //   12: ldc 178
    //   14: aconst_null
    //   15: invokeinterface 184 3 0
    //   20: astore 4
    //   22: new 147	java/lang/StringBuilder
    //   25: dup
    //   26: ldc 186
    //   28: invokespecial 189	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   31: aload 4
    //   33: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   36: invokevirtual 164	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   39: invokestatic 194	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
    //   42: new 196	com/millennialmedia/android/AdDatabaseHelper
    //   45: dup
    //   46: aload_1
    //   47: invokevirtual 101	com/millennialmedia/android/MMAdView:getContext	()Landroid/content/Context;
    //   50: invokespecial 197	com/millennialmedia/android/AdDatabaseHelper:<init>	(Landroid/content/Context;)V
    //   53: astore 5
    //   55: aload 5
    //   57: aload 4
    //   59: invokevirtual 201	com/millennialmedia/android/AdDatabaseHelper:getVideoAd	(Ljava/lang/String;)Lcom/millennialmedia/android/VideoAd;
    //   62: astore 11
    //   64: aload 11
    //   66: astore 8
    //   68: aload 5
    //   70: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   73: aload 8
    //   75: ifnull +77 -> 152
    //   78: aload_0
    //   79: aload_1
    //   80: invokespecial 207	com/millennialmedia/android/MMAdViewController:adIsCaching	(Lcom/millennialmedia/android/MMAdView;)V
    //   83: ldc 209
    //   85: ldc 211
    //   87: invokestatic 217	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
    //   90: pop
    //   91: aload_0
    //   92: getfield 74	com/millennialmedia/android/MMAdViewController:cacheHandler	Landroid/os/Handler;
    //   95: new 219	com/millennialmedia/android/MMAdViewController$7
    //   98: dup
    //   99: aload_0
    //   100: aload 8
    //   102: invokespecial 222	com/millennialmedia/android/MMAdViewController$7:<init>	(Lcom/millennialmedia/android/MMAdViewController;Lcom/millennialmedia/android/VideoAd;)V
    //   105: invokevirtual 226	android/os/Handler:post	(Ljava/lang/Runnable;)Z
    //   108: pop
    //   109: return
    //   110: astore 6
    //   112: aconst_null
    //   113: astore 5
    //   115: aload 6
    //   117: invokevirtual 229	android/database/sqlite/SQLiteException:printStackTrace	()V
    //   120: aconst_null
    //   121: astore 8
    //   123: aload 5
    //   125: ifnull -52 -> 73
    //   128: aload 5
    //   130: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   133: aconst_null
    //   134: astore 8
    //   136: goto -63 -> 73
    //   139: astore 7
    //   141: aload_3
    //   142: ifnull +7 -> 149
    //   145: aload_3
    //   146: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   149: aload 7
    //   151: athrow
    //   152: ldc 231
    //   154: invokestatic 234	com/millennialmedia/android/MMAdViewSDK$Log:d	(Ljava/lang/String;)V
    //   157: aload_0
    //   158: iload_2
    //   159: invokespecial 237	com/millennialmedia/android/MMAdViewController:getNextAd	(Z)V
    //   162: return
    //   163: astore 7
    //   165: aload 5
    //   167: astore_3
    //   168: goto -27 -> 141
    //   171: astore 6
    //   173: goto -58 -> 115
    //
    // Exception table:
    //   from	to	target	type
    //   42	55	110	android/database/sqlite/SQLiteException
    //   42	55	139	finally
    //   55	64	163	finally
    //   115	120	163	finally
    //   55	64	171	android/database/sqlite/SQLiteException
  }

  private void adDoneCaching(MMAdView paramMMAdView, boolean paramBoolean)
  {
    if ((paramMMAdView != null) && (paramMMAdView.listener != null));
    try
    {
      paramMMAdView.listener.MMAdCachingCompleted(paramMMAdView, paramBoolean);
      return;
    }
    catch (Exception localException)
    {
      Log.w("MillennialMediaSDK", "Exception raised in your MMAdListener: ", localException);
    }
  }

  private void adFailed(MMAdView paramMMAdView)
  {
    if ((paramMMAdView != null) && (paramMMAdView.listener != null));
    try
    {
      paramMMAdView.listener.MMAdFailed(paramMMAdView);
      return;
    }
    catch (Exception localException)
    {
      Log.w("MillennialMediaSDK", "Exception raised in your MMAdListener: ", localException);
    }
  }

  private void adIsCaching(MMAdView paramMMAdView)
  {
    if ((paramMMAdView != null) && (paramMMAdView.listener != null));
    try
    {
      paramMMAdView.listener.MMAdRequestIsCaching(paramMMAdView);
      return;
    }
    catch (Exception localException)
    {
      Log.w("MillennialMediaSDK", "Exception raised in your MMAdListener: ", localException);
    }
  }

  private void adSuccess(MMAdView paramMMAdView)
  {
    if ((paramMMAdView != null) && (paramMMAdView.listener != null));
    try
    {
      paramMMAdView.listener.MMAdReturned(paramMMAdView);
      return;
    }
    catch (Exception localException)
    {
      Log.w("MillennialMediaSDK", "Exception raised in your MMAdListener: ", localException);
    }
  }

  static void assignAdViewController(MMAdView paramMMAdView)
  {
    int i = 1;
    try
    {
      MMAdViewController localMMAdViewController1 = paramMMAdView.controller;
      if (localMMAdViewController1 != null);
      while (true)
      {
        return;
        if (paramMMAdView.getId() != -1)
          break;
        Log.e("MillennialMediaSDK", "MMAdView found without a view id. Ad requests on this MMAdView are disabled.");
      }
    }
    finally
    {
    }
    MMAdViewController localMMAdViewController2 = (MMAdViewController)controllers.get(paramMMAdView.adViewId);
    MMAdViewController localMMAdViewController3;
    if (localMMAdViewController2 == null)
    {
      localMMAdViewController3 = new MMAdViewController(paramMMAdView);
      controllers.put(paramMMAdView.adViewId, localMMAdViewController3);
      i = 0;
    }
    for (MMAdViewController localMMAdViewController4 = localMMAdViewController3; ; localMMAdViewController4 = localMMAdViewController2)
    {
      localMMAdViewController4.adViewRef = new WeakReference(paramMMAdView);
      paramMMAdView.controller = localMMAdViewController4;
      if (localMMAdViewController4.webView.getParent() != null)
        ((ViewGroup)localMMAdViewController4.webView.getParent()).removeView(localMMAdViewController4.webView);
      paramMMAdView.addView(localMMAdViewController4.webView, new ViewGroup.LayoutParams(-1, -1));
      if ((paramMMAdView.refreshInterval >= 0) && (paramMMAdView.refreshInterval < 15))
      {
        localMMAdViewController4.refreshTimerOn = false;
        MMAdViewSDK.Log.d("Refresh interval is " + paramMMAdView.refreshInterval + ". Change to at least 15 to refresh ads.");
      }
      while (true)
      {
        if ((paramMMAdView.refreshInterval < 0) || (i != 0))
          break label289;
        localMMAdViewController4.chooseCachedAdOrAdCall(false);
        break;
        if (paramMMAdView.refreshInterval < 0)
        {
          localMMAdViewController4.refreshTimerOn = false;
          MMAdViewSDK.Log.d("Automatic ad fetching is off with " + paramMMAdView.refreshInterval + ". You must manually call for ads.");
        }
        else
        {
          localMMAdViewController4.refreshTimerOn = true;
          localMMAdViewController4.resumeTimer(false);
        }
      }
      label289: break;
    }
  }

  static void cachedVideoWasAdded(Context paramContext, String paramString)
  {
    if (paramString != null);
    try
    {
      if (!cachedVideoListLoaded)
        getCachedVideoList(paramContext);
      if (cachedVideoSet == null)
        cachedVideoSet = new HashSet();
      cachedVideoSet.add(paramString);
      cachedVideoList = null;
      return;
    }
    finally
    {
    }
  }

  static void cachedVideoWasRemoved(Context paramContext, String paramString)
  {
    try
    {
      if (!cachedVideoListLoaded)
        getCachedVideoList(paramContext);
      if (cachedVideoSet != null)
      {
        cachedVideoSet.remove(paramString);
        cachedVideoList = null;
      }
      return;
    }
    finally
    {
    }
  }

  private static boolean checkForAdNotDownloaded(Context paramContext)
  {
    SharedPreferences localSharedPreferences = paramContext.getSharedPreferences("MillennialMediaSettings", 0);
    boolean bool = localSharedPreferences.getBoolean("pendingDownload", false);
    MMAdViewSDK.Log.v("Pending download?: " + bool);
    if (localSharedPreferences.getInt("downloadAttempts", 0) >= 3)
    {
      MMAdViewSDK.Log.v("Cached ad download failed too many times. Purging it from the database.");
      deleteAd(paramContext, localSharedPreferences.getString("lastDownloadedAdName", null));
      SharedPreferences.Editor localEditor = localSharedPreferences.edit();
      localEditor.putInt("downloadAttempts", 0);
      localEditor.commit();
      return false;
    }
    return bool;
  }

  // ERROR //
  private static boolean checkIfAdExistsInDb(String paramString, MMAdView paramMMAdView)
  {
    // Byte code:
    //   0: new 196	com/millennialmedia/android/AdDatabaseHelper
    //   3: dup
    //   4: aload_1
    //   5: invokevirtual 101	com/millennialmedia/android/MMAdView:getContext	()Landroid/content/Context;
    //   8: invokespecial 197	com/millennialmedia/android/AdDatabaseHelper:<init>	(Landroid/content/Context;)V
    //   11: astore_2
    //   12: aload_2
    //   13: aload_0
    //   14: invokevirtual 487	com/millennialmedia/android/AdDatabaseHelper:checkIfAdExists	(Ljava/lang/String;)Z
    //   17: istore 7
    //   19: iload 7
    //   21: istore 5
    //   23: new 147	java/lang/StringBuilder
    //   26: dup
    //   27: ldc_w 489
    //   30: invokespecial 189	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   33: aload_0
    //   34: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   37: ldc_w 491
    //   40: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   43: iload 5
    //   45: invokevirtual 458	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
    //   48: invokevirtual 164	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   51: invokestatic 194	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
    //   54: aload_2
    //   55: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   58: iload 5
    //   60: ireturn
    //   61: astore 8
    //   63: aconst_null
    //   64: astore_2
    //   65: aload 8
    //   67: astore 4
    //   69: iconst_0
    //   70: istore 5
    //   72: aload 4
    //   74: invokevirtual 229	android/database/sqlite/SQLiteException:printStackTrace	()V
    //   77: aload_2
    //   78: ifnull -20 -> 58
    //   81: aload_2
    //   82: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   85: iload 5
    //   87: ireturn
    //   88: astore 6
    //   90: aconst_null
    //   91: astore_2
    //   92: aload_2
    //   93: ifnull +7 -> 100
    //   96: aload_2
    //   97: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   100: aload 6
    //   102: athrow
    //   103: astore 6
    //   105: goto -13 -> 92
    //   108: astore_3
    //   109: aload_3
    //   110: astore 4
    //   112: iconst_0
    //   113: istore 5
    //   115: goto -43 -> 72
    //   118: astore 4
    //   120: goto -48 -> 72
    //
    // Exception table:
    //   from	to	target	type
    //   0	12	61	android/database/sqlite/SQLiteException
    //   0	12	88	finally
    //   12	19	103	finally
    //   23	54	103	finally
    //   72	77	103	finally
    //   12	19	108	android/database/sqlite/SQLiteException
    //   23	54	118	android/database/sqlite/SQLiteException
  }

  // ERROR //
  private static boolean checkIfAdExistsInFilesystem(Context paramContext, String paramString, VideoAd paramVideoAd)
  {
    // Byte code:
    //   0: new 196	com/millennialmedia/android/AdDatabaseHelper
    //   3: dup
    //   4: aload_0
    //   5: invokespecial 197	com/millennialmedia/android/AdDatabaseHelper:<init>	(Landroid/content/Context;)V
    //   8: astore_3
    //   9: iconst_1
    //   10: aload_3
    //   11: aload_1
    //   12: invokevirtual 495	com/millennialmedia/android/AdDatabaseHelper:getButtonCountForAd	(Ljava/lang/String;)I
    //   15: iadd
    //   16: istore 6
    //   18: aload_3
    //   19: aload_1
    //   20: invokevirtual 498	com/millennialmedia/android/AdDatabaseHelper:isAdOnSDCard	(Ljava/lang/String;)Z
    //   23: istore 7
    //   25: aload_3
    //   26: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   29: iload 7
    //   31: ifeq +203 -> 234
    //   34: invokestatic 503	android/os/Environment:getExternalStorageState	()Ljava/lang/String;
    //   37: ldc_w 505
    //   40: invokevirtual 510	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   43: ifeq +191 -> 234
    //   46: new 512	java/io/File
    //   49: dup
    //   50: new 147	java/lang/StringBuilder
    //   53: dup
    //   54: invokespecial 148	java/lang/StringBuilder:<init>	()V
    //   57: invokestatic 516	android/os/Environment:getExternalStorageDirectory	()Ljava/io/File;
    //   60: invokevirtual 519	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   63: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   66: ldc_w 521
    //   69: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   72: aload_1
    //   73: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   76: invokevirtual 164	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   79: invokespecial 522	java/io/File:<init>	(Ljava/lang/String;)V
    //   82: astore 8
    //   84: aload 8
    //   86: invokevirtual 525	java/io/File:exists	()Z
    //   89: ifeq +281 -> 370
    //   92: aload 8
    //   94: invokevirtual 529	java/io/File:list	()[Ljava/lang/String;
    //   97: astore 15
    //   99: aload 15
    //   101: ifnull +269 -> 370
    //   104: aload 15
    //   106: arraylength
    //   107: iload 6
    //   109: if_icmplt +261 -> 370
    //   112: iconst_1
    //   113: istore 9
    //   115: iload 9
    //   117: ifeq +246 -> 363
    //   120: aload_2
    //   121: ifnull +242 -> 363
    //   124: aload_2
    //   125: getfield 534	com/millennialmedia/android/VideoAd:contentLength	J
    //   128: lconst_0
    //   129: lcmp
    //   130: ifle +143 -> 273
    //   133: new 512	java/io/File
    //   136: dup
    //   137: aload 8
    //   139: ldc_w 536
    //   142: invokespecial 539	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   145: astore 11
    //   147: aload 11
    //   149: invokevirtual 525	java/io/File:exists	()Z
    //   152: ifeq +16 -> 168
    //   155: aload 11
    //   157: invokevirtual 543	java/io/File:length	()J
    //   160: aload_2
    //   161: getfield 534	com/millennialmedia/android/VideoAd:contentLength	J
    //   164: lcmp
    //   165: ifeq +108 -> 273
    //   168: iconst_0
    //   169: istore 10
    //   171: new 147	java/lang/StringBuilder
    //   174: dup
    //   175: ldc_w 489
    //   178: invokespecial 189	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   181: aload_1
    //   182: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   185: ldc_w 545
    //   188: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   191: iload 10
    //   193: invokevirtual 458	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
    //   196: invokevirtual 164	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   199: invokestatic 194	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
    //   202: iload 10
    //   204: ireturn
    //   205: astore 4
    //   207: aconst_null
    //   208: astore_3
    //   209: aload 4
    //   211: invokevirtual 229	android/database/sqlite/SQLiteException:printStackTrace	()V
    //   214: aload_3
    //   215: ifnull +7 -> 222
    //   218: aload_3
    //   219: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   222: ldc_w 547
    //   225: ldc_w 549
    //   228: invokestatic 367	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   231: pop
    //   232: iconst_0
    //   233: ireturn
    //   234: new 512	java/io/File
    //   237: dup
    //   238: new 147	java/lang/StringBuilder
    //   241: dup
    //   242: invokespecial 148	java/lang/StringBuilder:<init>	()V
    //   245: aload_0
    //   246: invokevirtual 552	android/content/Context:getCacheDir	()Ljava/io/File;
    //   249: invokevirtual 555	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   252: ldc_w 557
    //   255: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   258: aload_1
    //   259: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   262: invokevirtual 164	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   265: invokespecial 522	java/io/File:<init>	(Ljava/lang/String;)V
    //   268: astore 8
    //   270: goto -186 -> 84
    //   273: aload_2
    //   274: getfield 561	com/millennialmedia/android/VideoAd:buttons	Ljava/util/ArrayList;
    //   277: invokevirtual 567	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   280: astore 12
    //   282: aload 12
    //   284: invokeinterface 572 1 0
    //   289: ifeq +74 -> 363
    //   292: aload 12
    //   294: invokeinterface 576 1 0
    //   299: checkcast 578	com/millennialmedia/android/VideoImage
    //   302: astore 13
    //   304: aload 13
    //   306: getfield 579	com/millennialmedia/android/VideoImage:contentLength	J
    //   309: lconst_0
    //   310: lcmp
    //   311: ifle -29 -> 282
    //   314: new 512	java/io/File
    //   317: dup
    //   318: aload 8
    //   320: aload 13
    //   322: invokevirtual 582	com/millennialmedia/android/VideoImage:getImageName	()Ljava/lang/String;
    //   325: invokespecial 539	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   328: astore 14
    //   330: aload 14
    //   332: invokevirtual 525	java/io/File:exists	()Z
    //   335: ifeq +17 -> 352
    //   338: aload 14
    //   340: invokevirtual 543	java/io/File:length	()J
    //   343: aload 13
    //   345: getfield 579	com/millennialmedia/android/VideoImage:contentLength	J
    //   348: lcmp
    //   349: ifeq -67 -> 282
    //   352: iconst_0
    //   353: istore 10
    //   355: goto -184 -> 171
    //   358: astore 4
    //   360: goto -151 -> 209
    //   363: iload 9
    //   365: istore 10
    //   367: goto -196 -> 171
    //   370: iconst_0
    //   371: istore 9
    //   373: goto -258 -> 115
    //
    // Exception table:
    //   from	to	target	type
    //   0	9	205	android/database/sqlite/SQLiteException
    //   9	29	358	android/database/sqlite/SQLiteException
  }

  // ERROR //
  private boolean checkIfExpired(String paramString, MMAdView paramMMAdView)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: new 196	com/millennialmedia/android/AdDatabaseHelper
    //   5: dup
    //   6: aload_2
    //   7: invokevirtual 101	com/millennialmedia/android/MMAdView:getContext	()Landroid/content/Context;
    //   10: invokespecial 197	com/millennialmedia/android/AdDatabaseHelper:<init>	(Landroid/content/Context;)V
    //   13: astore 4
    //   15: aload 4
    //   17: aload_1
    //   18: invokevirtual 585	com/millennialmedia/android/AdDatabaseHelper:isAdExpired	(Ljava/lang/String;)Z
    //   21: istore 8
    //   23: iload 8
    //   25: istore 7
    //   27: aload 4
    //   29: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   32: iload 7
    //   34: ireturn
    //   35: astore 5
    //   37: aconst_null
    //   38: astore 4
    //   40: aload 5
    //   42: invokevirtual 229	android/database/sqlite/SQLiteException:printStackTrace	()V
    //   45: iconst_0
    //   46: istore 7
    //   48: aload 4
    //   50: ifnull -18 -> 32
    //   53: aload 4
    //   55: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   58: iconst_0
    //   59: ireturn
    //   60: astore 6
    //   62: aload_3
    //   63: ifnull +7 -> 70
    //   66: aload_3
    //   67: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   70: aload 6
    //   72: athrow
    //   73: astore 6
    //   75: aload 4
    //   77: astore_3
    //   78: goto -16 -> 62
    //   81: astore 5
    //   83: goto -43 -> 40
    //
    // Exception table:
    //   from	to	target	type
    //   2	15	35	android/database/sqlite/SQLiteException
    //   2	15	60	finally
    //   15	23	73	finally
    //   40	45	73	finally
    //   15	23	81	android/database/sqlite/SQLiteException
  }

  // ERROR //
  private static void cleanUpExpiredAds(Context paramContext)
  {
    // Byte code:
    //   0: new 196	com/millennialmedia/android/AdDatabaseHelper
    //   3: dup
    //   4: aload_0
    //   5: invokespecial 197	com/millennialmedia/android/AdDatabaseHelper:<init>	(Landroid/content/Context;)V
    //   8: astore_1
    //   9: aload_1
    //   10: invokevirtual 590	com/millennialmedia/android/AdDatabaseHelper:getAllExpiredAds	()Ljava/util/List;
    //   13: astore 4
    //   15: aload 4
    //   17: ifnull +56 -> 73
    //   20: aload 4
    //   22: invokeinterface 595 1 0
    //   27: ifle +46 -> 73
    //   30: ldc_w 597
    //   33: invokestatic 194	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
    //   36: iconst_0
    //   37: istore 5
    //   39: iload 5
    //   41: aload 4
    //   43: invokeinterface 595 1 0
    //   48: if_icmpge +25 -> 73
    //   51: aload_0
    //   52: aload 4
    //   54: iload 5
    //   56: invokeinterface 600 2 0
    //   61: checkcast 507	java/lang/String
    //   64: invokestatic 469	com/millennialmedia/android/MMAdViewController:deleteAd	(Landroid/content/Context;Ljava/lang/String;)V
    //   67: iinc 5 1
    //   70: goto -31 -> 39
    //   73: aload_1
    //   74: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   77: return
    //   78: astore_2
    //   79: aconst_null
    //   80: astore_1
    //   81: aload_2
    //   82: invokevirtual 229	android/database/sqlite/SQLiteException:printStackTrace	()V
    //   85: aload_1
    //   86: ifnull -9 -> 77
    //   89: aload_1
    //   90: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   93: return
    //   94: astore_3
    //   95: aconst_null
    //   96: astore_1
    //   97: aload_1
    //   98: ifnull +7 -> 105
    //   101: aload_1
    //   102: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   105: aload_3
    //   106: athrow
    //   107: astore_3
    //   108: goto -11 -> 97
    //   111: astore_2
    //   112: goto -31 -> 81
    //
    // Exception table:
    //   from	to	target	type
    //   0	9	78	android/database/sqlite/SQLiteException
    //   0	9	94	finally
    //   9	15	107	finally
    //   20	36	107	finally
    //   39	67	107	finally
    //   81	85	107	finally
    //   9	15	111	android/database/sqlite/SQLiteException
    //   20	36	111	android/database/sqlite/SQLiteException
    //   39	67	111	android/database/sqlite/SQLiteException
  }

  // ERROR //
  static void deleteAd(Context paramContext, String paramString)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: iconst_0
    //   3: istore_3
    //   4: aload_1
    //   5: ifnonnull +4 -> 9
    //   8: return
    //   9: invokestatic 503	android/os/Environment:getExternalStorageState	()Ljava/lang/String;
    //   12: ldc_w 505
    //   15: invokevirtual 510	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   18: ifeq +133 -> 151
    //   21: new 512	java/io/File
    //   24: dup
    //   25: new 147	java/lang/StringBuilder
    //   28: dup
    //   29: invokespecial 148	java/lang/StringBuilder:<init>	()V
    //   32: invokestatic 516	android/os/Environment:getExternalStorageDirectory	()Ljava/io/File;
    //   35: invokevirtual 519	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   38: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   41: ldc_w 521
    //   44: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   47: aload_1
    //   48: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   51: invokevirtual 164	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   54: invokespecial 522	java/io/File:<init>	(Ljava/lang/String;)V
    //   57: astore 4
    //   59: aload 4
    //   61: invokevirtual 525	java/io/File:exists	()Z
    //   64: ifeq +87 -> 151
    //   67: aload 4
    //   69: invokevirtual 604	java/io/File:listFiles	()[Ljava/io/File;
    //   72: astore 17
    //   74: new 147	java/lang/StringBuilder
    //   77: dup
    //   78: ldc_w 606
    //   81: invokespecial 189	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   84: aload 17
    //   86: arraylength
    //   87: invokevirtual 410	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   90: invokevirtual 164	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   93: invokestatic 194	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
    //   96: iconst_0
    //   97: istore 18
    //   99: iload 18
    //   101: aload 17
    //   103: arraylength
    //   104: if_icmpge +18 -> 122
    //   107: aload 17
    //   109: iload 18
    //   111: aaload
    //   112: invokevirtual 609	java/io/File:delete	()Z
    //   115: pop
    //   116: iinc 18 1
    //   119: goto -20 -> 99
    //   122: aload 4
    //   124: invokevirtual 609	java/io/File:delete	()Z
    //   127: pop
    //   128: new 147	java/lang/StringBuilder
    //   131: dup
    //   132: invokespecial 148	java/lang/StringBuilder:<init>	()V
    //   135: aload_1
    //   136: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   139: ldc_w 611
    //   142: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   145: invokevirtual 164	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   148: invokestatic 194	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
    //   151: new 512	java/io/File
    //   154: dup
    //   155: new 147	java/lang/StringBuilder
    //   158: dup
    //   159: invokespecial 148	java/lang/StringBuilder:<init>	()V
    //   162: aload_0
    //   163: invokevirtual 552	android/content/Context:getCacheDir	()Ljava/io/File;
    //   166: invokevirtual 555	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   169: ldc_w 557
    //   172: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   175: aload_1
    //   176: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   179: invokevirtual 164	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   182: invokespecial 522	java/io/File:<init>	(Ljava/lang/String;)V
    //   185: astore 5
    //   187: aload 5
    //   189: invokevirtual 525	java/io/File:exists	()Z
    //   192: ifeq +87 -> 279
    //   195: aload 5
    //   197: invokevirtual 604	java/io/File:listFiles	()[Ljava/io/File;
    //   200: astore 13
    //   202: new 147	java/lang/StringBuilder
    //   205: dup
    //   206: ldc_w 606
    //   209: invokespecial 189	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   212: aload 13
    //   214: arraylength
    //   215: invokevirtual 410	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   218: invokevirtual 164	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   221: invokestatic 194	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
    //   224: iconst_0
    //   225: istore 14
    //   227: iload 14
    //   229: aload 13
    //   231: arraylength
    //   232: if_icmpge +18 -> 250
    //   235: aload 13
    //   237: iload 14
    //   239: aaload
    //   240: invokevirtual 609	java/io/File:delete	()Z
    //   243: pop
    //   244: iinc 14 1
    //   247: goto -20 -> 227
    //   250: aload 5
    //   252: invokevirtual 609	java/io/File:delete	()Z
    //   255: pop
    //   256: new 147	java/lang/StringBuilder
    //   259: dup
    //   260: invokespecial 148	java/lang/StringBuilder:<init>	()V
    //   263: aload_1
    //   264: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   267: ldc_w 611
    //   270: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   273: invokevirtual 164	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   276: invokestatic 194	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
    //   279: new 196	com/millennialmedia/android/AdDatabaseHelper
    //   282: dup
    //   283: aload_0
    //   284: invokespecial 197	com/millennialmedia/android/AdDatabaseHelper:<init>	(Landroid/content/Context;)V
    //   287: astore 6
    //   289: aload 6
    //   291: aload_1
    //   292: invokevirtual 614	com/millennialmedia/android/AdDatabaseHelper:getCachedAdAcid	(Ljava/lang/String;)Ljava/lang/String;
    //   295: astore 10
    //   297: aload 10
    //   299: astore 8
    //   301: aload 6
    //   303: aload_1
    //   304: invokevirtual 617	com/millennialmedia/android/AdDatabaseHelper:purgeAdFromDb	(Ljava/lang/String;)Z
    //   307: istore_3
    //   308: aload 6
    //   310: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   313: aload 8
    //   315: ifnull +9 -> 324
    //   318: aload_0
    //   319: aload 8
    //   321: invokestatic 619	com/millennialmedia/android/MMAdViewController:cachedVideoWasRemoved	(Landroid/content/Context;Ljava/lang/String;)V
    //   324: new 147	java/lang/StringBuilder
    //   327: dup
    //   328: ldc_w 621
    //   331: invokespecial 189	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   334: aload_1
    //   335: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   338: ldc_w 623
    //   341: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   344: iload_3
    //   345: invokevirtual 458	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
    //   348: invokevirtual 164	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   351: invokestatic 194	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
    //   354: return
    //   355: astore 12
    //   357: aconst_null
    //   358: astore 8
    //   360: aload_2
    //   361: ifnull +7 -> 368
    //   364: aload_2
    //   365: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   368: ldc 209
    //   370: ldc_w 625
    //   373: invokestatic 367	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   376: pop
    //   377: goto -64 -> 313
    //   380: astore 7
    //   382: aload 6
    //   384: astore_2
    //   385: aconst_null
    //   386: astore 8
    //   388: iconst_0
    //   389: istore_3
    //   390: goto -30 -> 360
    //   393: astore 11
    //   395: aload 6
    //   397: astore_2
    //   398: goto -38 -> 360
    //
    // Exception table:
    //   from	to	target	type
    //   279	289	355	android/database/sqlite/SQLiteException
    //   289	297	380	android/database/sqlite/SQLiteException
    //   301	313	393	android/database/sqlite/SQLiteException
  }

  // ERROR //
  static boolean downloadComponent(String paramString1, String paramString2, File paramFile)
  {
    // Byte code:
    //   0: new 512	java/io/File
    //   3: dup
    //   4: aload_2
    //   5: aload_1
    //   6: invokespecial 539	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   9: astore_3
    //   10: new 147	java/lang/StringBuilder
    //   13: dup
    //   14: ldc_w 633
    //   17: invokespecial 189	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   20: aload_1
    //   21: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   24: ldc_w 635
    //   27: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   30: aload_0
    //   31: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   34: invokevirtual 164	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   37: invokestatic 194	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
    //   40: aload_3
    //   41: invokevirtual 525	java/io/File:exists	()Z
    //   44: ifeq +37 -> 81
    //   47: aload_3
    //   48: invokevirtual 543	java/io/File:length	()J
    //   51: lconst_0
    //   52: lcmp
    //   53: ifle +28 -> 81
    //   56: new 147	java/lang/StringBuilder
    //   59: dup
    //   60: invokespecial 148	java/lang/StringBuilder:<init>	()V
    //   63: aload_1
    //   64: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   67: ldc_w 637
    //   70: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   73: invokevirtual 164	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   76: invokestatic 194	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
    //   79: iconst_1
    //   80: ireturn
    //   81: new 639	java/net/URL
    //   84: dup
    //   85: aload_0
    //   86: invokespecial 640	java/net/URL:<init>	(Ljava/lang/String;)V
    //   89: astore 4
    //   91: iconst_0
    //   92: invokestatic 645	java/net/HttpURLConnection:setFollowRedirects	(Z)V
    //   95: aload 4
    //   97: invokevirtual 649	java/net/URL:openConnection	()Ljava/net/URLConnection;
    //   100: checkcast 642	java/net/HttpURLConnection
    //   103: astore 8
    //   105: aload 8
    //   107: ldc_w 651
    //   110: invokevirtual 654	java/net/HttpURLConnection:setRequestMethod	(Ljava/lang/String;)V
    //   113: aload 8
    //   115: invokevirtual 657	java/net/HttpURLConnection:connect	()V
    //   118: aload 8
    //   120: invokevirtual 661	java/net/HttpURLConnection:getInputStream	()Ljava/io/InputStream;
    //   123: astore 9
    //   125: aload 8
    //   127: ldc_w 663
    //   130: invokevirtual 666	java/net/HttpURLConnection:getHeaderField	(Ljava/lang/String;)Ljava/lang/String;
    //   133: astore 10
    //   135: aload 10
    //   137: ifnull +257 -> 394
    //   140: aload 10
    //   142: invokestatic 672	java/lang/Long:parseLong	(Ljava/lang/String;)J
    //   145: lstore 11
    //   147: aload 9
    //   149: ifnonnull +28 -> 177
    //   152: ldc 209
    //   154: new 147	java/lang/StringBuilder
    //   157: dup
    //   158: ldc_w 674
    //   161: invokespecial 189	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   164: aload_1
    //   165: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   168: invokevirtual 164	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   171: invokestatic 367	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   174: pop
    //   175: iconst_0
    //   176: ireturn
    //   177: new 676	java/io/FileOutputStream
    //   180: dup
    //   181: aload_3
    //   182: invokespecial 679	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   185: astore 14
    //   187: sipush 1024
    //   190: newarray byte
    //   192: astore 15
    //   194: aload 9
    //   196: aload 15
    //   198: invokevirtual 685	java/io/InputStream:read	([B)I
    //   201: istore 16
    //   203: iload 16
    //   205: ifle +62 -> 267
    //   208: aload 14
    //   210: aload 15
    //   212: iconst_0
    //   213: iload 16
    //   215: invokevirtual 689	java/io/FileOutputStream:write	([BII)V
    //   218: goto -24 -> 194
    //   221: astore 5
    //   223: ldc 209
    //   225: new 147	java/lang/StringBuilder
    //   228: dup
    //   229: ldc_w 691
    //   232: invokespecial 189	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   235: aload_1
    //   236: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   239: ldc_w 693
    //   242: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   245: aload 5
    //   247: invokevirtual 696	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   250: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   253: invokevirtual 164	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   256: invokestatic 367	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   259: pop
    //   260: aload_3
    //   261: invokevirtual 609	java/io/File:delete	()Z
    //   264: pop
    //   265: iconst_0
    //   266: ireturn
    //   267: aload 9
    //   269: invokevirtual 697	java/io/InputStream:close	()V
    //   272: aload 14
    //   274: invokevirtual 698	java/io/FileOutputStream:close	()V
    //   277: aload_3
    //   278: invokevirtual 543	java/io/File:length	()J
    //   281: lstore 22
    //   283: lload 22
    //   285: lload 11
    //   287: lcmp
    //   288: ifeq +12 -> 300
    //   291: lload 11
    //   293: ldc2_w 699
    //   296: lcmp
    //   297: ifne +43 -> 340
    //   300: iconst_1
    //   301: ireturn
    //   302: astore 17
    //   304: ldc 209
    //   306: new 147	java/lang/StringBuilder
    //   309: dup
    //   310: ldc_w 702
    //   313: invokespecial 189	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   316: aload 17
    //   318: invokevirtual 703	java/io/IOException:getMessage	()Ljava/lang/String;
    //   321: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   324: invokevirtual 164	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   327: aload 17
    //   329: invokestatic 705	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   332: pop
    //   333: aload_3
    //   334: invokevirtual 609	java/io/File:delete	()Z
    //   337: pop
    //   338: iconst_0
    //   339: ireturn
    //   340: ldc 209
    //   342: ldc_w 707
    //   345: invokestatic 367	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   348: pop
    //   349: goto -89 -> 260
    //   352: astore 20
    //   354: ldc 209
    //   356: new 147	java/lang/StringBuilder
    //   359: dup
    //   360: ldc_w 691
    //   363: invokespecial 189	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   366: aload_1
    //   367: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   370: ldc_w 693
    //   373: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   376: aload 20
    //   378: invokevirtual 708	java/lang/SecurityException:getMessage	()Ljava/lang/String;
    //   381: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   384: invokevirtual 164	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   387: invokestatic 367	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   390: pop
    //   391: goto -131 -> 260
    //   394: ldc2_w 699
    //   397: lstore 11
    //   399: goto -252 -> 147
    //
    // Exception table:
    //   from	to	target	type
    //   81	135	221	java/lang/Exception
    //   140	147	221	java/lang/Exception
    //   152	175	221	java/lang/Exception
    //   177	194	221	java/lang/Exception
    //   194	203	221	java/lang/Exception
    //   208	218	221	java/lang/Exception
    //   267	277	221	java/lang/Exception
    //   277	283	221	java/lang/Exception
    //   304	338	221	java/lang/Exception
    //   340	349	221	java/lang/Exception
    //   354	391	221	java/lang/Exception
    //   267	277	302	java/io/IOException
    //   277	283	352	java/lang/SecurityException
    //   340	349	352	java/lang/SecurityException
  }

  private static boolean freeMemoryOnDisk(MMAdView paramMMAdView)
  {
    boolean bool = true;
    try
    {
      if (Environment.getExternalStorageState().equals("mounted"))
      {
        File localFile1 = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/.mmsyscache");
        if (localFile1.exists())
        {
          if (localFile1.length() >= 12582912L)
            break label128;
          return bool;
        }
      }
      File localFile2 = paramMMAdView.getContext().getCacheDir();
      if (localFile2 != null)
      {
        long l = localFile2.length();
        Log.i("MillennialMediaSDK", "Cache: " + l);
        if (l < 12582912L)
          break label126;
        return false;
      }
    }
    catch (Exception localException)
    {
      bool = false;
    }
    label126: return bool;
    label128: return false;
  }

  private String getAdType(String paramString)
  {
    if (paramString != null)
    {
      if (paramString.equals("MMBannerAdTop"))
        return "&adtype=" + "MMBannerAdTop";
      if (paramString.equals("MMBannerAdBottom"))
        return "&adtype=" + "MMBannerAdBottom";
      if (paramString.equals("MMBannerAdRectangle"))
        return "&adtype=" + "MMBannerAdRectangle";
      if (paramString.equals("MMFullScreenAdLaunch"))
        return "&adtype=" + "MMFullScreenAdLaunch";
      if (paramString.equals("MMFullScreenAdTransition"))
        return "&adtype=" + "MMFullScreenAdTransition";
    }
    Log.e("MillennialMediaSDK", "******* ERROR: INCORRECT AD TYPE IN MMADVIEW OBJECT PARAMETERS (" + paramString + ") **********");
    Log.e("MillennialMediaSDK", "******* SDK DEFAULTED TO MMBannerAdTop. THIS MAY AFFECT THE ADS YOU RECIEVE!!! **********");
    return "&adtype=" + "MMBannerAdTop";
  }

  // ERROR //
  static String getCachedVideoList(Context paramContext)
  {
    // Byte code:
    //   0: iconst_0
    //   1: istore_1
    //   2: ldc 2
    //   4: monitorenter
    //   5: getstatic 441	com/millennialmedia/android/MMAdViewController:cachedVideoList	Ljava/lang/String;
    //   8: ifnonnull +402 -> 410
    //   11: getstatic 426	com/millennialmedia/android/MMAdViewController:cachedVideoListLoaded	Z
    //   14: istore 4
    //   16: iload 4
    //   18: ifne +244 -> 262
    //   21: new 434	java/util/HashSet
    //   24: dup
    //   25: invokespecial 435	java/util/HashSet:<init>	()V
    //   28: putstatic 432	com/millennialmedia/android/MMAdViewController:cachedVideoSet	Ljava/util/HashSet;
    //   31: new 196	com/millennialmedia/android/AdDatabaseHelper
    //   34: dup
    //   35: aload_0
    //   36: invokespecial 197	com/millennialmedia/android/AdDatabaseHelper:<init>	(Landroid/content/Context;)V
    //   39: astore 11
    //   41: invokestatic 503	android/os/Environment:getExternalStorageState	()Ljava/lang/String;
    //   44: ldc_w 505
    //   47: invokevirtual 510	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   50: ifeq +117 -> 167
    //   53: new 512	java/io/File
    //   56: dup
    //   57: new 147	java/lang/StringBuilder
    //   60: dup
    //   61: invokespecial 148	java/lang/StringBuilder:<init>	()V
    //   64: invokestatic 516	android/os/Environment:getExternalStorageDirectory	()Ljava/io/File;
    //   67: invokevirtual 519	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   70: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   73: ldc_w 712
    //   76: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   79: invokevirtual 164	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   82: invokespecial 522	java/io/File:<init>	(Ljava/lang/String;)V
    //   85: astore 14
    //   87: aload 14
    //   89: invokevirtual 525	java/io/File:exists	()Z
    //   92: ifeq +161 -> 253
    //   95: aload 14
    //   97: invokevirtual 604	java/io/File:listFiles	()[Ljava/io/File;
    //   100: astore 15
    //   102: iload_1
    //   103: aload 15
    //   105: arraylength
    //   106: if_icmpge +147 -> 253
    //   109: aload 11
    //   111: aload 15
    //   113: iload_1
    //   114: aaload
    //   115: invokevirtual 740	java/io/File:getName	()Ljava/lang/String;
    //   118: invokevirtual 201	com/millennialmedia/android/AdDatabaseHelper:getVideoAd	(Ljava/lang/String;)Lcom/millennialmedia/android/VideoAd;
    //   121: astore 16
    //   123: aload 16
    //   125: ifnull +297 -> 422
    //   128: aload 16
    //   130: getfield 743	com/millennialmedia/android/VideoAd:acid	Ljava/lang/String;
    //   133: ifnull +289 -> 422
    //   136: aload_0
    //   137: aload 15
    //   139: iload_1
    //   140: aaload
    //   141: invokevirtual 740	java/io/File:getName	()Ljava/lang/String;
    //   144: aload 16
    //   146: invokestatic 280	com/millennialmedia/android/MMAdViewController:checkIfAdExistsInFilesystem	(Landroid/content/Context;Ljava/lang/String;Lcom/millennialmedia/android/VideoAd;)Z
    //   149: ifeq +273 -> 422
    //   152: getstatic 432	com/millennialmedia/android/MMAdViewController:cachedVideoSet	Ljava/util/HashSet;
    //   155: aload 16
    //   157: getfield 743	com/millennialmedia/android/VideoAd:acid	Ljava/lang/String;
    //   160: invokevirtual 439	java/util/HashSet:add	(Ljava/lang/Object;)Z
    //   163: pop
    //   164: goto +258 -> 422
    //   167: aload_0
    //   168: invokevirtual 552	android/content/Context:getCacheDir	()Ljava/io/File;
    //   171: astore 18
    //   173: aload 18
    //   175: invokevirtual 525	java/io/File:exists	()Z
    //   178: ifeq +75 -> 253
    //   181: aload 18
    //   183: invokevirtual 604	java/io/File:listFiles	()[Ljava/io/File;
    //   186: astore 19
    //   188: iload_1
    //   189: aload 19
    //   191: arraylength
    //   192: if_icmpge +61 -> 253
    //   195: aload 11
    //   197: aload 19
    //   199: iload_1
    //   200: aaload
    //   201: invokevirtual 740	java/io/File:getName	()Ljava/lang/String;
    //   204: invokevirtual 201	com/millennialmedia/android/AdDatabaseHelper:getVideoAd	(Ljava/lang/String;)Lcom/millennialmedia/android/VideoAd;
    //   207: astore 20
    //   209: aload 20
    //   211: ifnull +217 -> 428
    //   214: aload 20
    //   216: getfield 743	com/millennialmedia/android/VideoAd:acid	Ljava/lang/String;
    //   219: ifnull +209 -> 428
    //   222: aload_0
    //   223: aload 19
    //   225: iload_1
    //   226: aaload
    //   227: invokevirtual 740	java/io/File:getName	()Ljava/lang/String;
    //   230: aload 20
    //   232: invokestatic 280	com/millennialmedia/android/MMAdViewController:checkIfAdExistsInFilesystem	(Landroid/content/Context;Ljava/lang/String;Lcom/millennialmedia/android/VideoAd;)Z
    //   235: ifeq +193 -> 428
    //   238: getstatic 432	com/millennialmedia/android/MMAdViewController:cachedVideoSet	Ljava/util/HashSet;
    //   241: aload 20
    //   243: getfield 743	com/millennialmedia/android/VideoAd:acid	Ljava/lang/String;
    //   246: invokevirtual 439	java/util/HashSet:add	(Ljava/lang/Object;)Z
    //   249: pop
    //   250: goto +178 -> 428
    //   253: aload 11
    //   255: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   258: iconst_1
    //   259: putstatic 426	com/millennialmedia/android/MMAdViewController:cachedVideoListLoaded	Z
    //   262: getstatic 432	com/millennialmedia/android/MMAdViewController:cachedVideoSet	Ljava/util/HashSet;
    //   265: ifnull +145 -> 410
    //   268: getstatic 432	com/millennialmedia/android/MMAdViewController:cachedVideoSet	Ljava/util/HashSet;
    //   271: invokevirtual 744	java/util/HashSet:size	()I
    //   274: ifle +136 -> 410
    //   277: new 147	java/lang/StringBuilder
    //   280: dup
    //   281: invokespecial 148	java/lang/StringBuilder:<init>	()V
    //   284: astore 5
    //   286: getstatic 432	com/millennialmedia/android/MMAdViewController:cachedVideoSet	Ljava/util/HashSet;
    //   289: invokevirtual 745	java/util/HashSet:iterator	()Ljava/util/Iterator;
    //   292: astore 6
    //   294: aload 6
    //   296: invokeinterface 572 1 0
    //   301: ifeq +101 -> 402
    //   304: aload 6
    //   306: invokeinterface 576 1 0
    //   311: astore 7
    //   313: aload 5
    //   315: invokevirtual 747	java/lang/StringBuilder:length	()I
    //   318: ifle +70 -> 388
    //   321: aload 5
    //   323: new 147	java/lang/StringBuilder
    //   326: dup
    //   327: ldc_w 749
    //   330: invokespecial 189	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   333: aload 7
    //   335: checkcast 507	java/lang/String
    //   338: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   341: invokevirtual 164	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   344: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   347: pop
    //   348: goto -54 -> 294
    //   351: astore_2
    //   352: ldc 2
    //   354: monitorexit
    //   355: aload_2
    //   356: athrow
    //   357: astore 10
    //   359: aconst_null
    //   360: astore 11
    //   362: ldc 209
    //   364: ldc_w 751
    //   367: invokestatic 367	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   370: pop
    //   371: aconst_null
    //   372: astore_3
    //   373: aload 11
    //   375: ifnull +8 -> 383
    //   378: aload 11
    //   380: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   383: ldc 2
    //   385: monitorexit
    //   386: aload_3
    //   387: areturn
    //   388: aload 5
    //   390: aload 7
    //   392: checkcast 507	java/lang/String
    //   395: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   398: pop
    //   399: goto -105 -> 294
    //   402: aload 5
    //   404: invokevirtual 164	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   407: putstatic 441	com/millennialmedia/android/MMAdViewController:cachedVideoList	Ljava/lang/String;
    //   410: getstatic 441	com/millennialmedia/android/MMAdViewController:cachedVideoList	Ljava/lang/String;
    //   413: astore_3
    //   414: goto -31 -> 383
    //   417: astore 13
    //   419: goto -57 -> 362
    //   422: iinc 1 1
    //   425: goto -323 -> 102
    //   428: iinc 1 1
    //   431: goto -243 -> 188
    //
    // Exception table:
    //   from	to	target	type
    //   5	16	351	finally
    //   21	41	351	finally
    //   41	102	351	finally
    //   102	123	351	finally
    //   128	164	351	finally
    //   167	188	351	finally
    //   188	209	351	finally
    //   214	250	351	finally
    //   253	262	351	finally
    //   262	294	351	finally
    //   294	348	351	finally
    //   362	371	351	finally
    //   378	383	351	finally
    //   388	399	351	finally
    //   402	410	351	finally
    //   410	414	351	finally
    //   21	41	357	android/database/sqlite/SQLiteException
    //   41	102	417	android/database/sqlite/SQLiteException
    //   102	123	417	android/database/sqlite/SQLiteException
    //   128	164	417	android/database/sqlite/SQLiteException
    //   167	188	417	android/database/sqlite/SQLiteException
    //   188	209	417	android/database/sqlite/SQLiteException
    //   214	250	417	android/database/sqlite/SQLiteException
    //   253	262	417	android/database/sqlite/SQLiteException
  }

  private void getNextAd(boolean paramBoolean)
  {
    this.requestInProgress = true;
    MMAdViewController.1 local1 = new MMAdViewController.1(this, paramBoolean);
    local1.setPriority(10);
    local1.start();
  }

  static String getURLDeviceValues(Context paramContext)
  {
    while (true)
    {
      try
      {
        StringBuilder localStringBuilder = new StringBuilder();
        String str1 = MMAdViewSDK.getConnectionType(paramContext);
        StatFs localStatFs;
        if (Environment.getExternalStorageState().equals("mounted"))
        {
          localStatFs = new StatFs(Environment.getExternalStorageDirectory().getAbsolutePath());
          String str2 = Long.toString(localStatFs.getAvailableBlocks() * localStatFs.getBlockSize());
          Intent localIntent = paramContext.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
          if (localIntent == null)
            break label302;
          if (localIntent.getIntExtra("plugged", 0) == 0)
          {
            str3 = "false";
            str4 = Integer.toString((int)(100.0F / localIntent.getIntExtra("scale", 100) * localIntent.getIntExtra("level", 0)));
            if ((str4 != null) && (str4.length() > 0))
              localStringBuilder.append("&bl=" + str4);
            if ((str3 != null) && (str3.length() > 0))
              localStringBuilder.append("&plugged=" + str3);
            if (str2.length() > 0)
              localStringBuilder.append("&space=" + str2);
            if (str1 != null)
              localStringBuilder.append("&conn=" + str1);
            return localStringBuilder.toString();
          }
        }
        else
        {
          localStatFs = new StatFs(paramContext.getCacheDir().getPath());
          continue;
        }
      }
      catch (Exception localException)
      {
        MMAdViewSDK.Log.v(Log.getStackTraceString(localException));
        return "";
      }
      String str3 = "true";
      continue;
      label302: String str4 = null;
      str3 = null;
    }
  }

  private String getURLMetaValues(MMAdView paramMMAdView)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    Locale localLocale = Locale.getDefault();
    if (localLocale != null)
    {
      localStringBuilder.append("&language=" + localLocale.getLanguage());
      localStringBuilder.append("&country=" + localLocale.getCountry());
    }
    if (paramMMAdView.age != null)
      localStringBuilder.append("&age=" + URLEncoder.encode(paramMMAdView.age, "UTF-8"));
    if (paramMMAdView.gender != null)
      localStringBuilder.append("&gender=" + URLEncoder.encode(paramMMAdView.gender, "UTF-8"));
    if (paramMMAdView.zip != null)
      localStringBuilder.append("&zip=" + URLEncoder.encode(paramMMAdView.zip, "UTF-8"));
    if ((paramMMAdView.marital != null) && ((paramMMAdView.marital.equals("single")) || (paramMMAdView.marital.equals("married")) || (paramMMAdView.marital.equals("divorced")) || (paramMMAdView.marital.equals("swinger")) || (paramMMAdView.marital.equals("relationship")) || (paramMMAdView.marital.equals("engaged"))))
      localStringBuilder.append("&marital=" + paramMMAdView.marital);
    if (paramMMAdView.income != null)
      localStringBuilder.append("&income=" + URLEncoder.encode(paramMMAdView.income, "UTF-8"));
    if (paramMMAdView.keywords != null)
      localStringBuilder.append("&kw=" + URLEncoder.encode(paramMMAdView.keywords, "UTF-8"));
    if (paramMMAdView.latitude != null)
      localStringBuilder.append("&lat=" + URLEncoder.encode(paramMMAdView.latitude, "UTF-8"));
    if (paramMMAdView.longitude != null)
      localStringBuilder.append("&long=" + URLEncoder.encode(paramMMAdView.longitude, "UTF-8"));
    if (paramMMAdView.location != null)
    {
      if (paramMMAdView.location.hasAccuracy())
      {
        localStringBuilder.append("&ha=" + paramMMAdView.location.getAccuracy());
        localStringBuilder.append("&va=" + paramMMAdView.location.getAccuracy());
      }
      if (paramMMAdView.location.hasSpeed())
        localStringBuilder.append("&spd=" + paramMMAdView.location.getSpeed());
      if (paramMMAdView.location.hasBearing())
        localStringBuilder.append("&brg=" + paramMMAdView.location.getBearing());
      if (paramMMAdView.location.hasAltitude())
        localStringBuilder.append("&alt=" + paramMMAdView.location.getAltitude());
      localStringBuilder.append("&tslr=" + paramMMAdView.location.getTime());
    }
    if (paramMMAdView.acid != null)
      localStringBuilder.append("&acid=" + URLEncoder.encode(paramMMAdView.acid, "UTF-8"));
    if (paramMMAdView.mxsdk != null)
      localStringBuilder.append("&mxsdk=" + URLEncoder.encode(paramMMAdView.mxsdk, "UTF-8"));
    if (paramMMAdView.height != null)
      localStringBuilder.append("&hsht=" + URLEncoder.encode(paramMMAdView.height, "UTF-8"));
    if (paramMMAdView.width != null)
      localStringBuilder.append("&hswd=" + URLEncoder.encode(paramMMAdView.width, "UTF-8"));
    if (paramMMAdView.ethnicity != null)
      localStringBuilder.append("&ethnicity=" + URLEncoder.encode(paramMMAdView.ethnicity, "UTF-8"));
    if ((paramMMAdView.orientation != null) && ((paramMMAdView.orientation.equals("straight")) || (paramMMAdView.orientation.equals("gay")) || (paramMMAdView.orientation.equals("bisexual")) || (paramMMAdView.orientation.equals("notsure"))))
      localStringBuilder.append("&orientation=" + paramMMAdView.orientation);
    if (paramMMAdView.education != null)
      localStringBuilder.append("&edu=" + URLEncoder.encode(paramMMAdView.education, "UTF-8"));
    if (paramMMAdView.children != null)
      localStringBuilder.append("&children=" + URLEncoder.encode(paramMMAdView.children, "UTF-8"));
    if (paramMMAdView.politics != null)
      localStringBuilder.append("&politics=" + URLEncoder.encode(paramMMAdView.politics, "UTF-8"));
    if (paramMMAdView.vendor != null)
      localStringBuilder.append("&vendor=" + URLEncoder.encode(paramMMAdView.vendor, "UTF-8"));
    if (this.refreshTimerOn)
      localStringBuilder.append("&ar=" + paramMMAdView.refreshInterval);
    while (true)
    {
      String str1 = HandShake.sharedHandShake(paramMMAdView.getContext()).getSchemesList(paramMMAdView.getContext());
      if (str1 != null)
        localStringBuilder.append("&appsids=" + str1);
      String str2 = getCachedVideoList(paramMMAdView.getContext());
      if (str2 != null)
        localStringBuilder.append("&vid=" + URLEncoder.encode(str2, "UTF-8"));
      return localStringBuilder.toString();
      localStringBuilder.append("&ar=manual");
    }
  }

  // ERROR //
  private void handleCachedAdResponse(VideoAd paramVideoAd, boolean paramBoolean)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: aload_0
    //   3: getfield 93	com/millennialmedia/android/MMAdViewController:adViewRef	Ljava/lang/ref/WeakReference;
    //   6: invokevirtual 1035	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
    //   9: checkcast 97	com/millennialmedia/android/MMAdView
    //   12: astore 4
    //   14: aload 4
    //   16: ifnonnull +13 -> 29
    //   19: ldc 209
    //   21: ldc_w 1037
    //   24: invokestatic 367	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   27: pop
    //   28: return
    //   29: aload_1
    //   30: getfield 1040	com/millennialmedia/android/VideoAd:id	Ljava/lang/String;
    //   33: invokestatic 1045	com/millennialmedia/android/PreCacheWorker:isCurrentlyDownloading	(Ljava/lang/String;)Z
    //   36: ifeq +16 -> 52
    //   39: ldc_w 1047
    //   42: invokestatic 234	com/millennialmedia/android/MMAdViewSDK$Log:d	(Ljava/lang/String;)V
    //   45: aload_0
    //   46: aload 4
    //   48: invokespecial 207	com/millennialmedia/android/MMAdViewController:adIsCaching	(Lcom/millennialmedia/android/MMAdView;)V
    //   51: return
    //   52: aload_1
    //   53: invokevirtual 1050	com/millennialmedia/android/VideoAd:isExpired	()Z
    //   56: ifeq +31 -> 87
    //   59: ldc 209
    //   61: ldc_w 1052
    //   64: invokestatic 217	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
    //   67: pop
    //   68: aload 4
    //   70: invokevirtual 101	com/millennialmedia/android/MMAdView:getContext	()Landroid/content/Context;
    //   73: aload_1
    //   74: getfield 1040	com/millennialmedia/android/VideoAd:id	Ljava/lang/String;
    //   77: invokestatic 469	com/millennialmedia/android/MMAdViewController:deleteAd	(Landroid/content/Context;Ljava/lang/String;)V
    //   80: aload_0
    //   81: aload 4
    //   83: invokespecial 284	com/millennialmedia/android/MMAdViewController:adFailed	(Lcom/millennialmedia/android/MMAdView;)V
    //   86: return
    //   87: aload_1
    //   88: getfield 1040	com/millennialmedia/android/VideoAd:id	Ljava/lang/String;
    //   91: aload 4
    //   93: invokestatic 275	com/millennialmedia/android/MMAdViewController:checkIfAdExistsInDb	(Ljava/lang/String;Lcom/millennialmedia/android/MMAdView;)Z
    //   96: ifne +116 -> 212
    //   99: ldc_w 1054
    //   102: invokestatic 234	com/millennialmedia/android/MMAdViewSDK$Log:d	(Ljava/lang/String;)V
    //   105: new 196	com/millennialmedia/android/AdDatabaseHelper
    //   108: dup
    //   109: aload 4
    //   111: invokevirtual 101	com/millennialmedia/android/MMAdView:getContext	()Landroid/content/Context;
    //   114: invokespecial 197	com/millennialmedia/android/AdDatabaseHelper:<init>	(Landroid/content/Context;)V
    //   117: astore 15
    //   119: aload 15
    //   121: aload_1
    //   122: invokevirtual 1058	com/millennialmedia/android/AdDatabaseHelper:storeAd	(Lcom/millennialmedia/android/VideoAd;)V
    //   125: aload 15
    //   127: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   130: aload 4
    //   132: invokevirtual 101	com/millennialmedia/android/MMAdView:getContext	()Landroid/content/Context;
    //   135: aload_1
    //   136: getfield 1040	com/millennialmedia/android/VideoAd:id	Ljava/lang/String;
    //   139: aload_1
    //   140: invokestatic 280	com/millennialmedia/android/MMAdViewController:checkIfAdExistsInFilesystem	(Landroid/content/Context;Ljava/lang/String;Lcom/millennialmedia/android/VideoAd;)Z
    //   143: ifne +134 -> 277
    //   146: ldc_w 1060
    //   149: invokestatic 234	com/millennialmedia/android/MMAdViewSDK$Log:d	(Ljava/lang/String;)V
    //   152: aload_0
    //   153: aload 4
    //   155: invokespecial 207	com/millennialmedia/android/MMAdViewController:adIsCaching	(Lcom/millennialmedia/android/MMAdView;)V
    //   158: aload_0
    //   159: getfield 74	com/millennialmedia/android/MMAdViewController:cacheHandler	Landroid/os/Handler;
    //   162: new 1062	com/millennialmedia/android/MMAdViewController$4
    //   165: dup
    //   166: aload_0
    //   167: aload_1
    //   168: invokespecial 1063	com/millennialmedia/android/MMAdViewController$4:<init>	(Lcom/millennialmedia/android/MMAdViewController;Lcom/millennialmedia/android/VideoAd;)V
    //   171: invokevirtual 226	android/os/Handler:post	(Ljava/lang/Runnable;)Z
    //   174: pop
    //   175: return
    //   176: astore 16
    //   178: aconst_null
    //   179: astore 15
    //   181: aload 16
    //   183: invokevirtual 229	android/database/sqlite/SQLiteException:printStackTrace	()V
    //   186: aload 15
    //   188: ifnull -58 -> 130
    //   191: aload 15
    //   193: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   196: goto -66 -> 130
    //   199: astore 17
    //   201: aload_3
    //   202: ifnull +7 -> 209
    //   205: aload_3
    //   206: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   209: aload 17
    //   211: athrow
    //   212: ldc_w 1065
    //   215: invokestatic 234	com/millennialmedia/android/MMAdViewSDK$Log:d	(Ljava/lang/String;)V
    //   218: new 196	com/millennialmedia/android/AdDatabaseHelper
    //   221: dup
    //   222: aload 4
    //   224: invokevirtual 101	com/millennialmedia/android/MMAdView:getContext	()Landroid/content/Context;
    //   227: invokespecial 197	com/millennialmedia/android/AdDatabaseHelper:<init>	(Landroid/content/Context;)V
    //   230: astore 5
    //   232: aload 5
    //   234: aload_1
    //   235: invokevirtual 1068	com/millennialmedia/android/AdDatabaseHelper:updateAdData	(Lcom/millennialmedia/android/VideoAd;)V
    //   238: aload 5
    //   240: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   243: goto -113 -> 130
    //   246: astore 6
    //   248: aload 6
    //   250: invokevirtual 229	android/database/sqlite/SQLiteException:printStackTrace	()V
    //   253: aload_3
    //   254: ifnull -124 -> 130
    //   257: aload_3
    //   258: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   261: goto -131 -> 130
    //   264: astore 7
    //   266: aload_3
    //   267: ifnull +7 -> 274
    //   270: aload_3
    //   271: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   274: aload 7
    //   276: athrow
    //   277: iload_2
    //   278: ifne +71 -> 349
    //   281: ldc 209
    //   283: ldc_w 1070
    //   286: invokestatic 217	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
    //   289: pop
    //   290: aload 4
    //   292: invokevirtual 101	com/millennialmedia/android/MMAdView:getContext	()Landroid/content/Context;
    //   295: invokestatic 1024	com/millennialmedia/android/HandShake:sharedHandShake	(Landroid/content/Context;)Lcom/millennialmedia/android/HandShake;
    //   298: aload 4
    //   300: invokevirtual 101	com/millennialmedia/android/MMAdView:getContext	()Landroid/content/Context;
    //   303: aload 4
    //   305: getfield 1073	com/millennialmedia/android/MMAdView:adType	Ljava/lang/String;
    //   308: invokevirtual 1076	com/millennialmedia/android/HandShake:updateLastVideoViewedTime	(Landroid/content/Context;Ljava/lang/String;)V
    //   311: aload_0
    //   312: aload 4
    //   314: invokespecial 1078	com/millennialmedia/android/MMAdViewController:adSuccess	(Lcom/millennialmedia/android/MMAdView;)V
    //   317: ldc 209
    //   319: ldc_w 1080
    //   322: invokestatic 217	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
    //   325: pop
    //   326: aload_0
    //   327: aload_1
    //   328: getfield 1040	com/millennialmedia/android/VideoAd:id	Ljava/lang/String;
    //   331: aload 4
    //   333: invokespecial 295	com/millennialmedia/android/MMAdViewController:playVideo	(Ljava/lang/String;Lcom/millennialmedia/android/MMAdView;)V
    //   336: aload 4
    //   338: invokevirtual 101	com/millennialmedia/android/MMAdView:getContext	()Landroid/content/Context;
    //   341: aload_1
    //   342: getfield 743	com/millennialmedia/android/VideoAd:acid	Ljava/lang/String;
    //   345: invokestatic 1082	com/millennialmedia/android/MMAdViewController:cachedVideoWasAdded	(Landroid/content/Context;Ljava/lang/String;)V
    //   348: return
    //   349: aload 4
    //   351: invokevirtual 101	com/millennialmedia/android/MMAdView:getContext	()Landroid/content/Context;
    //   354: ldc 172
    //   356: iconst_0
    //   357: invokevirtual 176	android/content/Context:getSharedPreferences	(Ljava/lang/String;I)Landroid/content/SharedPreferences;
    //   360: invokeinterface 473 1 0
    //   365: astore 8
    //   367: aload 8
    //   369: ldc 178
    //   371: aload_1
    //   372: getfield 1040	com/millennialmedia/android/VideoAd:id	Ljava/lang/String;
    //   375: invokeinterface 1086 3 0
    //   380: pop
    //   381: aload 8
    //   383: ldc_w 1088
    //   386: iconst_0
    //   387: invokeinterface 1092 3 0
    //   392: pop
    //   393: aload 8
    //   395: invokeinterface 483 1 0
    //   400: pop
    //   401: ldc_w 1094
    //   404: invokestatic 234	com/millennialmedia/android/MMAdViewSDK$Log:d	(Ljava/lang/String;)V
    //   407: return
    //   408: astore 7
    //   410: aload 5
    //   412: astore_3
    //   413: goto -147 -> 266
    //   416: astore 6
    //   418: aload 5
    //   420: astore_3
    //   421: goto -173 -> 248
    //   424: astore 17
    //   426: aload 15
    //   428: astore_3
    //   429: goto -228 -> 201
    //   432: astore 16
    //   434: goto -253 -> 181
    //
    // Exception table:
    //   from	to	target	type
    //   105	119	176	android/database/sqlite/SQLiteException
    //   105	119	199	finally
    //   218	232	246	android/database/sqlite/SQLiteException
    //   218	232	264	finally
    //   248	253	264	finally
    //   232	238	408	finally
    //   232	238	416	android/database/sqlite/SQLiteException
    //   119	125	424	finally
    //   181	186	424	finally
    //   119	125	432	android/database/sqlite/SQLiteException
  }

  // ERROR //
  static File initCachedAdDirectory(VideoAd paramVideoAd, Context paramContext)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 1099	com/millennialmedia/android/VideoAd:storedOnSdCard	Z
    //   4: ifeq +259 -> 263
    //   7: invokestatic 503	android/os/Environment:getExternalStorageState	()Ljava/lang/String;
    //   10: ldc_w 505
    //   13: invokevirtual 510	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   16: ifeq +156 -> 172
    //   19: new 512	java/io/File
    //   22: dup
    //   23: invokestatic 516	android/os/Environment:getExternalStorageDirectory	()Ljava/io/File;
    //   26: ldc_w 1101
    //   29: invokespecial 539	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   32: astore_2
    //   33: aload_2
    //   34: invokevirtual 525	java/io/File:exists	()Z
    //   37: ifne +10 -> 47
    //   40: aload_2
    //   41: invokevirtual 1104	java/io/File:mkdirs	()Z
    //   44: ifeq +37 -> 81
    //   47: aload_2
    //   48: ifnull +223 -> 271
    //   51: aload_2
    //   52: invokevirtual 1107	java/io/File:isDirectory	()Z
    //   55: ifeq +216 -> 271
    //   58: new 512	java/io/File
    //   61: dup
    //   62: aload_2
    //   63: aload_0
    //   64: getfield 1040	com/millennialmedia/android/VideoAd:id	Ljava/lang/String;
    //   67: invokespecial 539	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   70: astore 4
    //   72: aload 4
    //   74: invokevirtual 1110	java/io/File:mkdir	()Z
    //   77: pop
    //   78: aload 4
    //   80: areturn
    //   81: aload_1
    //   82: invokevirtual 552	android/content/Context:getCacheDir	()Ljava/io/File;
    //   85: astore 6
    //   87: new 196	com/millennialmedia/android/AdDatabaseHelper
    //   90: dup
    //   91: aload_1
    //   92: invokespecial 197	com/millennialmedia/android/AdDatabaseHelper:<init>	(Landroid/content/Context;)V
    //   95: astore 7
    //   97: aload 7
    //   99: aload_0
    //   100: getfield 1040	com/millennialmedia/android/VideoAd:id	Ljava/lang/String;
    //   103: iconst_0
    //   104: invokevirtual 1114	com/millennialmedia/android/AdDatabaseHelper:updateAdOnSDCard	(Ljava/lang/String;I)V
    //   107: aload 7
    //   109: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   112: aload 6
    //   114: astore_2
    //   115: goto -68 -> 47
    //   118: astore 8
    //   120: aconst_null
    //   121: astore 7
    //   123: aload 8
    //   125: invokevirtual 229	android/database/sqlite/SQLiteException:printStackTrace	()V
    //   128: aload 7
    //   130: ifnull +36 -> 166
    //   133: aload 7
    //   135: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   138: aload 6
    //   140: astore_2
    //   141: goto -94 -> 47
    //   144: astore 10
    //   146: aconst_null
    //   147: astore 7
    //   149: aload 10
    //   151: astore 9
    //   153: aload 7
    //   155: ifnull +8 -> 163
    //   158: aload 7
    //   160: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   163: aload 9
    //   165: athrow
    //   166: aload 6
    //   168: astore_2
    //   169: goto -122 -> 47
    //   172: aload_1
    //   173: invokevirtual 552	android/content/Context:getCacheDir	()Ljava/io/File;
    //   176: astore 11
    //   178: new 196	com/millennialmedia/android/AdDatabaseHelper
    //   181: dup
    //   182: aload_1
    //   183: invokespecial 197	com/millennialmedia/android/AdDatabaseHelper:<init>	(Landroid/content/Context;)V
    //   186: astore 12
    //   188: aload 12
    //   190: aload_0
    //   191: getfield 1040	com/millennialmedia/android/VideoAd:id	Ljava/lang/String;
    //   194: iconst_0
    //   195: invokevirtual 1114	com/millennialmedia/android/AdDatabaseHelper:updateAdOnSDCard	(Ljava/lang/String;I)V
    //   198: aload 12
    //   200: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   203: aload 11
    //   205: astore_2
    //   206: goto -159 -> 47
    //   209: astore 13
    //   211: aconst_null
    //   212: astore 12
    //   214: aload 13
    //   216: invokevirtual 229	android/database/sqlite/SQLiteException:printStackTrace	()V
    //   219: aload 12
    //   221: ifnull +36 -> 257
    //   224: aload 12
    //   226: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   229: aload 11
    //   231: astore_2
    //   232: goto -185 -> 47
    //   235: astore 15
    //   237: aconst_null
    //   238: astore 12
    //   240: aload 15
    //   242: astore 14
    //   244: aload 12
    //   246: ifnull +8 -> 254
    //   249: aload 12
    //   251: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   254: aload 14
    //   256: athrow
    //   257: aload 11
    //   259: astore_2
    //   260: goto -213 -> 47
    //   263: aload_1
    //   264: invokevirtual 552	android/content/Context:getCacheDir	()Ljava/io/File;
    //   267: astore_2
    //   268: goto -221 -> 47
    //   271: ldc 209
    //   273: ldc_w 1116
    //   276: invokestatic 367	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   279: pop
    //   280: aconst_null
    //   281: areturn
    //   282: astore 14
    //   284: goto -40 -> 244
    //   287: astore 13
    //   289: goto -75 -> 214
    //   292: astore 9
    //   294: goto -141 -> 153
    //   297: astore 8
    //   299: goto -176 -> 123
    //
    // Exception table:
    //   from	to	target	type
    //   87	97	118	android/database/sqlite/SQLiteException
    //   87	97	144	finally
    //   178	188	209	android/database/sqlite/SQLiteException
    //   178	188	235	finally
    //   188	198	282	finally
    //   214	219	282	finally
    //   188	198	287	android/database/sqlite/SQLiteException
    //   97	107	292	finally
    //   123	128	292	finally
    //   97	107	297	android/database/sqlite/SQLiteException
  }

  // ERROR //
  private void playVideo(String paramString, MMAdView paramMMAdView)
  {
    // Byte code:
    //   0: aload_1
    //   1: ifnull +218 -> 219
    //   4: aload_2
    //   5: invokevirtual 101	com/millennialmedia/android/MMAdView:getContext	()Landroid/content/Context;
    //   8: ldc 172
    //   10: iconst_0
    //   11: invokevirtual 176	android/content/Context:getSharedPreferences	(Ljava/lang/String;I)Landroid/content/SharedPreferences;
    //   14: invokeinterface 473 1 0
    //   19: astore_3
    //   20: aload_3
    //   21: ldc_w 1088
    //   24: iconst_1
    //   25: invokeinterface 1092 3 0
    //   30: pop
    //   31: aload_3
    //   32: invokeinterface 483 1 0
    //   37: pop
    //   38: aload_2
    //   39: getfield 332	com/millennialmedia/android/MMAdView:listener	Lcom/millennialmedia/android/MMAdView$MMAdListener;
    //   42: ifnull +13 -> 55
    //   45: aload_2
    //   46: getfield 332	com/millennialmedia/android/MMAdView:listener	Lcom/millennialmedia/android/MMAdView$MMAdListener;
    //   49: aload_2
    //   50: invokeinterface 1119 2 0
    //   55: new 147	java/lang/StringBuilder
    //   58: dup
    //   59: ldc_w 1121
    //   62: invokespecial 189	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   65: aload_1
    //   66: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   69: invokevirtual 164	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   72: invokestatic 234	com/millennialmedia/android/MMAdViewSDK$Log:d	(Ljava/lang/String;)V
    //   75: new 797	android/content/Intent
    //   78: dup
    //   79: invokespecial 1122	android/content/Intent:<init>	()V
    //   82: aload_2
    //   83: invokevirtual 101	com/millennialmedia/android/MMAdView:getContext	()Landroid/content/Context;
    //   86: ldc_w 1124
    //   89: invokevirtual 1128	android/content/Intent:setClass	(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;
    //   92: astore 6
    //   94: aload 6
    //   96: ldc_w 1129
    //   99: invokevirtual 1133	android/content/Intent:setFlags	(I)Landroid/content/Intent;
    //   102: pop
    //   103: aload 6
    //   105: ldc_w 1135
    //   108: iconst_1
    //   109: invokevirtual 1139	android/content/Intent:putExtra	(Ljava/lang/String;Z)Landroid/content/Intent;
    //   112: pop
    //   113: aload 6
    //   115: ldc_w 1141
    //   118: aload_1
    //   119: invokevirtual 1144	android/content/Intent:putExtra	(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    //   122: pop
    //   123: new 196	com/millennialmedia/android/AdDatabaseHelper
    //   126: dup
    //   127: aload_2
    //   128: invokevirtual 101	com/millennialmedia/android/MMAdView:getContext	()Landroid/content/Context;
    //   131: invokespecial 197	com/millennialmedia/android/AdDatabaseHelper:<init>	(Landroid/content/Context;)V
    //   134: astore 10
    //   136: aload 10
    //   138: aload_1
    //   139: invokevirtual 498	com/millennialmedia/android/AdDatabaseHelper:isAdOnSDCard	(Ljava/lang/String;)Z
    //   142: istore 13
    //   144: aload 10
    //   146: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   149: iload 13
    //   151: ifeq +144 -> 295
    //   154: invokestatic 503	android/os/Environment:getExternalStorageState	()Ljava/lang/String;
    //   157: ldc_w 505
    //   160: invokevirtual 510	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   163: ifeq +132 -> 295
    //   166: aload 6
    //   168: new 147	java/lang/StringBuilder
    //   171: dup
    //   172: invokespecial 148	java/lang/StringBuilder:<init>	()V
    //   175: invokestatic 516	android/os/Environment:getExternalStorageDirectory	()Ljava/io/File;
    //   178: invokevirtual 519	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   181: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   184: ldc_w 521
    //   187: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   190: aload_1
    //   191: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   194: ldc_w 1146
    //   197: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   200: invokevirtual 164	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   203: invokestatic 1152	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
    //   206: invokevirtual 1156	android/content/Intent:setData	(Landroid/net/Uri;)Landroid/content/Intent;
    //   209: pop
    //   210: aload_2
    //   211: invokevirtual 101	com/millennialmedia/android/MMAdView:getContext	()Landroid/content/Context;
    //   214: aload 6
    //   216: invokevirtual 1160	android/content/Context:startActivity	(Landroid/content/Intent;)V
    //   219: return
    //   220: astore 16
    //   222: ldc 209
    //   224: ldc_w 339
    //   227: aload 16
    //   229: invokestatic 343	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   232: pop
    //   233: goto -178 -> 55
    //   236: astore 11
    //   238: aconst_null
    //   239: astore 10
    //   241: aload 11
    //   243: invokevirtual 229	android/database/sqlite/SQLiteException:printStackTrace	()V
    //   246: new 147	java/lang/StringBuilder
    //   249: dup
    //   250: ldc_w 1162
    //   253: invokespecial 189	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   256: aload_1
    //   257: invokevirtual 156	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   260: invokevirtual 164	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   263: invokestatic 234	com/millennialmedia/android/MMAdViewSDK$Log:d	(Ljava/lang/String;)V
    //   266: aload 10
    //   268: ifnull -49 -> 219
    //   271: aload 10
    //   273: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   276: return
    //   277: astore 12
    //   279: aconst_null
    //   280: astore 10
    //   282: aload 10
    //   284: ifnull +8 -> 292
    //   287: aload 10
    //   289: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   292: aload 12
    //   294: athrow
    //   295: aload 6
    //   297: aload_1
    //   298: invokestatic 1152	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
    //   301: invokevirtual 1156	android/content/Intent:setData	(Landroid/net/Uri;)Landroid/content/Intent;
    //   304: pop
    //   305: goto -95 -> 210
    //   308: astore 12
    //   310: goto -28 -> 282
    //   313: astore 11
    //   315: goto -74 -> 241
    //
    // Exception table:
    //   from	to	target	type
    //   45	55	220	java/lang/Exception
    //   123	136	236	android/database/sqlite/SQLiteException
    //   123	136	277	finally
    //   136	144	308	finally
    //   241	266	308	finally
    //   136	144	313	android/database/sqlite/SQLiteException
  }

  static void removeAdViewController(MMAdView paramMMAdView, boolean paramBoolean)
  {
    while (true)
    {
      try
      {
        MMAdViewController localMMAdViewController1 = paramMMAdView.controller;
        if (localMMAdViewController1 == null)
          return;
        if (paramBoolean)
        {
          localMMAdViewController2 = (MMAdViewController)controllers.put(paramMMAdView.adViewId, null);
          paramMMAdView.controller = null;
          if (localMMAdViewController2 == null)
            continue;
          localMMAdViewController2.pauseTimer(false);
          if (paramBoolean)
            localMMAdViewController2.handler = null;
          paramMMAdView.removeView(localMMAdViewController2.webView);
          continue;
        }
      }
      finally
      {
      }
      MMAdViewController localMMAdViewController2 = (MMAdViewController)controllers.get(paramMMAdView.adViewId);
    }
  }

  private void resetAdViewSettings()
  {
    this.shouldLaunchToOverlay = false;
    this.shouldShowTitlebar = false;
    this.shouldShowBottomBar = true;
    this.shouldEnableBottomBar = true;
    this.shouldMakeOverlayTransparent = false;
    this.shouldResizeOverlay = 0;
    this.overlayTitle = "Advertisement";
    this.overlayTransition = "bottomtotop";
    this.transitionTime = 600L;
    this.canAccelerate = false;
  }

  private void setWebViewContent(String paramString1, String paramString2, MMAdView paramMMAdView)
  {
    Activity localActivity = (Activity)paramMMAdView.getContext();
    if ((paramString1 == null) || (localActivity == null))
      return;
    MMAdViewController.2 local2 = new MMAdViewController.2(this);
    if (paramMMAdView.ignoreDensityScaling)
      paramString1 = "<head><meta name=\"viewport\" content=\"target-densitydpi=device-dpi\" /></head>" + paramString1;
    resetAdViewSettings();
    paramMMAdView.setClickable(false);
    localActivity.runOnUiThread(new MMAdViewController.3(this, local2, paramString2, paramString1));
  }

  // ERROR //
  boolean check(MMAdView paramMMAdView)
  {
    // Byte code:
    //   0: lconst_0
    //   1: lstore_2
    //   2: aconst_null
    //   3: astore 4
    //   5: aload_1
    //   6: getfield 1073	com/millennialmedia/android/MMAdView:adType	Ljava/lang/String;
    //   9: ldc_w 729
    //   12: invokevirtual 1219	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
    //   15: ifeq +50 -> 65
    //   18: aload_0
    //   19: getfield 320	com/millennialmedia/android/MMAdViewController:fetchedContentLaunch	Ljava/lang/String;
    //   22: ifnull +43 -> 65
    //   25: aload_1
    //   26: invokevirtual 101	com/millennialmedia/android/MMAdView:getContext	()Landroid/content/Context;
    //   29: invokestatic 1024	com/millennialmedia/android/HandShake:sharedHandShake	(Landroid/content/Context;)Lcom/millennialmedia/android/HandShake;
    //   32: aload_1
    //   33: getfield 1073	com/millennialmedia/android/MMAdView:adType	Ljava/lang/String;
    //   36: aload_0
    //   37: getfield 326	com/millennialmedia/android/MMAdViewController:fetchedTimeLaunch	J
    //   40: invokevirtual 1223	com/millennialmedia/android/HandShake:canDisplayCachedAd	(Ljava/lang/String;J)Z
    //   43: ifne +20 -> 63
    //   46: aload_0
    //   47: aconst_null
    //   48: putfield 320	com/millennialmedia/android/MMAdViewController:fetchedContentLaunch	Ljava/lang/String;
    //   51: aload_0
    //   52: aconst_null
    //   53: putfield 323	com/millennialmedia/android/MMAdViewController:fetchedBaseUrlLaunch	Ljava/lang/String;
    //   56: aload_0
    //   57: lload_2
    //   58: putfield 326	com/millennialmedia/android/MMAdViewController:fetchedTimeLaunch	J
    //   61: iconst_0
    //   62: ireturn
    //   63: iconst_1
    //   64: ireturn
    //   65: aload_1
    //   66: getfield 1073	com/millennialmedia/android/MMAdView:adType	Ljava/lang/String;
    //   69: ldc_w 731
    //   72: invokevirtual 1219	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
    //   75: ifeq +50 -> 125
    //   78: aload_0
    //   79: getfield 250	com/millennialmedia/android/MMAdViewController:fetchedContentTransition	Ljava/lang/String;
    //   82: ifnull +43 -> 125
    //   85: aload_1
    //   86: invokevirtual 101	com/millennialmedia/android/MMAdView:getContext	()Landroid/content/Context;
    //   89: invokestatic 1024	com/millennialmedia/android/HandShake:sharedHandShake	(Landroid/content/Context;)Lcom/millennialmedia/android/HandShake;
    //   92: aload_1
    //   93: getfield 1073	com/millennialmedia/android/MMAdView:adType	Ljava/lang/String;
    //   96: aload_0
    //   97: getfield 257	com/millennialmedia/android/MMAdViewController:fetchedTimeTransition	J
    //   100: invokevirtual 1223	com/millennialmedia/android/HandShake:canDisplayCachedAd	(Ljava/lang/String;J)Z
    //   103: ifne +20 -> 123
    //   106: aload_0
    //   107: aconst_null
    //   108: putfield 250	com/millennialmedia/android/MMAdViewController:fetchedContentTransition	Ljava/lang/String;
    //   111: aload_0
    //   112: aconst_null
    //   113: putfield 253	com/millennialmedia/android/MMAdViewController:fetchedBaseUrlTransition	Ljava/lang/String;
    //   116: aload_0
    //   117: lload_2
    //   118: putfield 257	com/millennialmedia/android/MMAdViewController:fetchedTimeTransition	J
    //   121: iconst_0
    //   122: ireturn
    //   123: iconst_1
    //   124: ireturn
    //   125: aload_1
    //   126: invokevirtual 101	com/millennialmedia/android/MMAdView:getContext	()Landroid/content/Context;
    //   129: ldc 172
    //   131: iconst_0
    //   132: invokevirtual 176	android/content/Context:getSharedPreferences	(Ljava/lang/String;I)Landroid/content/SharedPreferences;
    //   135: astore 5
    //   137: aload 5
    //   139: ldc 178
    //   141: aconst_null
    //   142: invokeinterface 184 3 0
    //   147: astore 6
    //   149: aload 5
    //   151: ldc_w 1088
    //   154: iconst_0
    //   155: invokeinterface 453 3 0
    //   160: istore 7
    //   162: aload 6
    //   164: ifnull +137 -> 301
    //   167: aload 6
    //   169: aload_1
    //   170: invokestatic 275	com/millennialmedia/android/MMAdViewController:checkIfAdExistsInDb	(Ljava/lang/String;Lcom/millennialmedia/android/MMAdView;)Z
    //   173: ifeq +166 -> 339
    //   176: aload_1
    //   177: invokevirtual 101	com/millennialmedia/android/MMAdView:getContext	()Landroid/content/Context;
    //   180: aload 6
    //   182: aconst_null
    //   183: invokestatic 280	com/millennialmedia/android/MMAdViewController:checkIfAdExistsInFilesystem	(Landroid/content/Context;Ljava/lang/String;Lcom/millennialmedia/android/VideoAd;)Z
    //   186: ifeq +141 -> 327
    //   189: aload_0
    //   190: aload 6
    //   192: aload_1
    //   193: invokespecial 289	com/millennialmedia/android/MMAdViewController:checkIfExpired	(Ljava/lang/String;Lcom/millennialmedia/android/MMAdView;)Z
    //   196: ifne +119 -> 315
    //   199: iload 7
    //   201: ifne +102 -> 303
    //   204: new 196	com/millennialmedia/android/AdDatabaseHelper
    //   207: dup
    //   208: aload_1
    //   209: invokevirtual 101	com/millennialmedia/android/MMAdView:getContext	()Landroid/content/Context;
    //   212: invokespecial 197	com/millennialmedia/android/AdDatabaseHelper:<init>	(Landroid/content/Context;)V
    //   215: astore 12
    //   217: aload 12
    //   219: aload 6
    //   221: invokevirtual 1226	com/millennialmedia/android/AdDatabaseHelper:getDeferredViewStart	(Ljava/lang/String;)J
    //   224: lstore 16
    //   226: lload 16
    //   228: lstore_2
    //   229: aload 12
    //   231: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   234: aload_1
    //   235: invokevirtual 101	com/millennialmedia/android/MMAdView:getContext	()Landroid/content/Context;
    //   238: invokestatic 1024	com/millennialmedia/android/HandShake:sharedHandShake	(Landroid/content/Context;)Lcom/millennialmedia/android/HandShake;
    //   241: aload_1
    //   242: getfield 1073	com/millennialmedia/android/MMAdView:adType	Ljava/lang/String;
    //   245: lload_2
    //   246: invokevirtual 1223	com/millennialmedia/android/HandShake:canDisplayCachedAd	(Ljava/lang/String;J)Z
    //   249: ifeq +43 -> 292
    //   252: iconst_1
    //   253: ireturn
    //   254: astore 13
    //   256: aconst_null
    //   257: astore 12
    //   259: aload 13
    //   261: invokevirtual 229	android/database/sqlite/SQLiteException:printStackTrace	()V
    //   264: aload 12
    //   266: ifnull -32 -> 234
    //   269: aload 12
    //   271: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   274: goto -40 -> 234
    //   277: astore 14
    //   279: aload 4
    //   281: ifnull +8 -> 289
    //   284: aload 4
    //   286: invokevirtual 204	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   289: aload 14
    //   291: athrow
    //   292: ldc 209
    //   294: ldc_w 1228
    //   297: invokestatic 217	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
    //   300: pop
    //   301: iconst_0
    //   302: ireturn
    //   303: ldc 209
    //   305: ldc_w 1230
    //   308: invokestatic 217	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
    //   311: pop
    //   312: goto -11 -> 301
    //   315: ldc 209
    //   317: ldc_w 1232
    //   320: invokestatic 217	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
    //   323: pop
    //   324: goto -23 -> 301
    //   327: ldc 209
    //   329: ldc_w 1234
    //   332: invokestatic 217	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
    //   335: pop
    //   336: goto -35 -> 301
    //   339: ldc 209
    //   341: ldc_w 1236
    //   344: invokestatic 217	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
    //   347: pop
    //   348: goto -47 -> 301
    //   351: astore 14
    //   353: aload 12
    //   355: astore 4
    //   357: goto -78 -> 279
    //   360: astore 13
    //   362: goto -103 -> 259
    //
    // Exception table:
    //   from	to	target	type
    //   204	217	254	android/database/sqlite/SQLiteException
    //   204	217	277	finally
    //   217	226	351	finally
    //   259	264	351	finally
    //   217	226	360	android/database/sqlite/SQLiteException
  }

  void chooseCachedAdOrAdCall(boolean paramBoolean)
  {
    while (true)
    {
      MMAdView localMMAdView;
      Context localContext;
      try
      {
        localMMAdView = (MMAdView)this.adViewRef.get();
        if (localMMAdView == null)
        {
          Log.e("MillennialMediaSDK", "The reference to the ad view was broken.");
          return;
        }
        localContext = localMMAdView.getContext();
        if (HandShake.sharedHandShake(localContext).kill)
        {
          Log.i("MillennialMediaSDK", "The server is no longer allowing ads.");
          adFailed(localMMAdView);
          continue;
        }
      }
      finally
      {
      }
      if (this.requestInProgress)
      {
        Log.i("MillennialMediaSDK", "There is already an ad request in progress. Defering call for new ad");
        adFailed(localMMAdView);
      }
      else if (HandShake.sharedHandShake(localContext).isAdTypeDownloading(localMMAdView.adType))
      {
        Log.i("MillennialMediaSDK", "There is a download in progress. Defering call for new ad");
        adFailed(localMMAdView);
      }
      else
      {
        MMAdViewSDK.Log.d("No download in progress.");
        if (checkForAdNotDownloaded(localMMAdView.getContext()))
        {
          Log.i("MillennialMediaSDK", "Last ad wasn't fully downloaded. Download again.");
          adIsCaching(localMMAdView);
          DownloadLastAd(localMMAdView, paramBoolean);
        }
        else
        {
          Log.i("MillennialMediaSDK", "No incomplete downloads.");
          cleanUpExpiredAds(localMMAdView.getContext());
          if (paramBoolean)
          {
            getNextAd(paramBoolean);
          }
          else
          {
            SharedPreferences localSharedPreferences = localMMAdView.getContext().getSharedPreferences("MillennialMediaSettings", 0);
            String str = localSharedPreferences.getString("lastDownloadedAdName", null);
            if (str != null)
            {
              if (checkIfAdExistsInDb(str, localMMAdView))
              {
                Log.i("MillennialMediaSDK", "Ad found in the database");
                if (checkIfAdExistsInFilesystem(localMMAdView.getContext(), str, null))
                {
                  if (!checkIfExpired(str, localMMAdView))
                  {
                    boolean bool = localSharedPreferences.getBoolean("lastAdViewed", false);
                    Log.i("MillennialMediaSDK", "Last ad viewed?: " + bool);
                    if (!bool)
                    {
                      if (HandShake.sharedHandShake(localContext).canWatchVideoAd(localContext, localMMAdView.adType, str))
                      {
                        adSuccess(localMMAdView);
                        Log.i("MillennialMediaSDK", "Millennial ad return success");
                        playVideo(str, localMMAdView);
                      }
                      else
                      {
                        Log.i("MillennialMediaSDK", "Outside of the timeout window. Call for a new ad");
                        getNextAd(false);
                      }
                    }
                    else
                    {
                      Log.i("MillennialMediaSDK", "Existing ad has been viewed. Call for a new ad");
                      getNextAd(false);
                    }
                  }
                  else
                  {
                    Log.i("MillennialMediaSDK", "Existing ad is expired. Delete and call for a new ad");
                    deleteAd(localMMAdView.getContext(), str);
                    SharedPreferences.Editor localEditor2 = localMMAdView.getContext().getSharedPreferences("MillennialMediaSettings", 0).edit();
                    localEditor2.putString("lastDownloadedAdName", null);
                    localEditor2.commit();
                    MMAdViewSDK.Log.v("Setting last ad name to NULL");
                    getNextAd(false);
                  }
                }
                else
                {
                  Log.i("MillennialMediaSDK", "Last ad can't be found in the file system. Download again.");
                  DownloadLastAd(localMMAdView, false);
                }
              }
              else
              {
                Log.i("MillennialMediaSDK", "Last ad can't be found in the database. Remove any files from the filesystem and call for new ad.");
                deleteAd(localMMAdView.getContext(), str);
                SharedPreferences.Editor localEditor1 = localMMAdView.getContext().getSharedPreferences("MillennialMediaSettings", 0).edit();
                localEditor1.putString("lastDownloadedAdName", null);
                localEditor1.commit();
                MMAdViewSDK.Log.v("Setting last ad name to NULL");
                getNextAd(false);
              }
            }
            else
            {
              Log.i("MillennialMediaSDK", "Last ad name is null. Call for new ad.");
              getNextAd(false);
            }
          }
        }
      }
    }
  }

  boolean display(MMAdView paramMMAdView)
  {
    if (check(paramMMAdView))
    {
      if ((paramMMAdView.adType.equalsIgnoreCase("MMFullScreenAdLaunch")) && (this.fetchedContentLaunch != null))
      {
        setWebViewContent(this.fetchedContentLaunch, this.fetchedBaseUrlLaunch, paramMMAdView);
        this.fetchedContentLaunch = null;
        this.fetchedBaseUrlLaunch = null;
        this.fetchedTimeLaunch = 0L;
        return true;
      }
      if ((paramMMAdView.adType.equalsIgnoreCase("MMFullScreenAdTransition")) && (this.fetchedContentTransition != null))
      {
        setWebViewContent(this.fetchedContentTransition, this.fetchedBaseUrlTransition, paramMMAdView);
        this.fetchedContentTransition = null;
        this.fetchedBaseUrlTransition = null;
        this.fetchedTimeTransition = 0L;
        return true;
      }
      String str = paramMMAdView.getContext().getSharedPreferences("MillennialMediaSettings", 0).getString("lastDownloadedAdName", null);
      HandShake.sharedHandShake(paramMMAdView.getContext()).updateLastVideoViewedTime(paramMMAdView.getContext(), paramMMAdView.adType);
      playVideo(str, paramMMAdView);
      return true;
    }
    return false;
  }

  void handleClick(String paramString)
  {
    this.urlString = paramString;
    MMAdViewController.5 local5 = new MMAdViewController.5(this);
    local5.setPriority(10);
    local5.start();
  }

  void pauseTimer(boolean paramBoolean)
  {
    try
    {
      if (!this.refreshTimerOn)
        return;
      if (this.paused)
      {
        if (paramBoolean)
          this.appPaused = true;
        return;
      }
    }
    finally
    {
    }
    this.handler.removeCallbacks(this.runnable);
    this.timeRemaining = (SystemClock.uptimeMillis() - this.timeResumed);
    this.paused = true;
    this.appPaused = paramBoolean;
  }

  void resumeTimer(boolean paramBoolean)
  {
    try
    {
      if (!this.refreshTimerOn)
        return;
      if (!this.paused)
        return;
    }
    finally
    {
    }
    if ((this.appPaused) && (!paramBoolean))
      return;
    MMAdView localMMAdView = (MMAdView)this.adViewRef.get();
    if (localMMAdView == null)
    {
      Log.e("MillennialMediaSDK", "The reference to the ad view was broken.");
      return;
    }
    if (this.handler == null)
      this.handler = new Handler(Looper.getMainLooper());
    if ((this.timeRemaining <= 0L) || (this.timeRemaining > 1000 * localMMAdView.refreshInterval))
      this.timeRemaining = (1000 * localMMAdView.refreshInterval);
    this.handler.postDelayed(this.runnable, this.timeRemaining);
    this.timeResumed = SystemClock.uptimeMillis();
    this.appPaused = false;
    this.paused = false;
  }

  private class DownloadAdTask extends AsyncTask<VideoAd, Void, String>
  {
    DownloadAdTask()
    {
      MMAdView localMMAdView = (MMAdView)MMAdViewController.this.adViewRef.get();
      if (localMMAdView == null)
      {
        Log.e("MillennialMediaSDK", "The reference to the ad view was broken.");
        return;
      }
      HandShake.sharedHandShake(localMMAdView.getContext()).lockAdTypeDownload(localMMAdView.adType);
    }

    protected String doInBackground(VideoAd[] paramArrayOfVideoAd)
    {
      MMAdView localMMAdView = (MMAdView)MMAdViewController.this.adViewRef.get();
      if (localMMAdView == null)
      {
        Log.e("MillennialMediaSDK", "The reference to the ad view was broken.");
        return null;
      }
      if ((paramArrayOfVideoAd == null) || (paramArrayOfVideoAd.length == 0))
        return null;
      File localFile = MMAdViewController.initCachedAdDirectory(paramArrayOfVideoAd[0], localMMAdView.getContext());
      if (localFile == null)
        return null;
      MMAdViewSDK.Log.v("Downloading content to " + localFile);
      if (!MMAdViewController.downloadComponent(paramArrayOfVideoAd[0].contentUrl, "video.dat", localFile))
      {
        SharedPreferences.Editor localEditor3 = localMMAdView.getContext().getSharedPreferences("MillennialMediaSettings", 0).edit();
        localEditor3.putBoolean("pendingDownload", true);
        localEditor3.commit();
        return paramArrayOfVideoAd[0].id;
      }
      for (int i = 0; i < paramArrayOfVideoAd[0].buttons.size(); i++)
      {
        VideoImage localVideoImage = (VideoImage)paramArrayOfVideoAd[0].buttons.get(i);
        if (!MMAdViewController.downloadComponent(localVideoImage.imageUrl, localVideoImage.getImageName(), localFile))
        {
          SharedPreferences.Editor localEditor2 = localMMAdView.getContext().getSharedPreferences("MillennialMediaSettings", 0).edit();
          localEditor2.putBoolean("pendingDownload", true);
          localEditor2.commit();
          return paramArrayOfVideoAd[0].id;
        }
      }
      SharedPreferences.Editor localEditor1 = localMMAdView.getContext().getSharedPreferences("MillennialMediaSettings", 0).edit();
      localEditor1.putBoolean("pendingDownload", false);
      localEditor1.commit();
      return paramArrayOfVideoAd[0].id;
    }

    // ERROR //
    protected void onPostExecute(String paramString)
    {
      // Byte code:
      //   0: iconst_1
      //   1: istore_2
      //   2: aload_0
      //   3: getfield 11	com/millennialmedia/android/MMAdViewController$DownloadAdTask:this$0	Lcom/millennialmedia/android/MMAdViewController;
      //   6: invokestatic 20	com/millennialmedia/android/MMAdViewController:access$100	(Lcom/millennialmedia/android/MMAdViewController;)Ljava/lang/ref/WeakReference;
      //   9: invokevirtual 26	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
      //   12: checkcast 28	com/millennialmedia/android/MMAdView
      //   15: astore_3
      //   16: aload_3
      //   17: ifnonnull +12 -> 29
      //   20: ldc 30
      //   22: ldc 32
      //   24: invokestatic 38	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
      //   27: pop
      //   28: return
      //   29: aload_3
      //   30: invokevirtual 42	com/millennialmedia/android/MMAdView:getContext	()Landroid/content/Context;
      //   33: ldc 99
      //   35: iconst_0
      //   36: invokevirtual 105	android/content/Context:getSharedPreferences	(Ljava/lang/String;I)Landroid/content/SharedPreferences;
      //   39: astore 4
      //   41: aload 4
      //   43: invokeinterface 111 1 0
      //   48: astore 5
      //   50: aload_1
      //   51: ifnull +162 -> 213
      //   54: aload 5
      //   56: ldc 157
      //   58: aload_1
      //   59: invokeinterface 161 3 0
      //   64: pop
      //   65: new 69	java/lang/StringBuilder
      //   68: dup
      //   69: ldc 163
      //   71: invokespecial 73	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
      //   74: aload_1
      //   75: invokevirtual 166	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   78: invokevirtual 81	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   81: invokestatic 86	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
      //   84: aload 4
      //   86: ldc 113
      //   88: iload_2
      //   89: invokeinterface 170 3 0
      //   94: ifeq +5 -> 99
      //   97: iconst_0
      //   98: istore_2
      //   99: aload_3
      //   100: invokevirtual 42	com/millennialmedia/android/MMAdView:getContext	()Landroid/content/Context;
      //   103: invokestatic 48	com/millennialmedia/android/HandShake:sharedHandShake	(Landroid/content/Context;)Lcom/millennialmedia/android/HandShake;
      //   106: aload_3
      //   107: getfield 52	com/millennialmedia/android/MMAdView:adType	Ljava/lang/String;
      //   110: invokevirtual 173	com/millennialmedia/android/HandShake:unlockAdTypeDownload	(Ljava/lang/String;)V
      //   113: new 175	com/millennialmedia/android/AdDatabaseHelper
      //   116: dup
      //   117: aload_3
      //   118: invokevirtual 42	com/millennialmedia/android/MMAdView:getContext	()Landroid/content/Context;
      //   121: invokespecial 178	com/millennialmedia/android/AdDatabaseHelper:<init>	(Landroid/content/Context;)V
      //   124: astore 7
      //   126: aload 7
      //   128: aload_1
      //   129: invokevirtual 182	com/millennialmedia/android/AdDatabaseHelper:getVideoAd	(Ljava/lang/String;)Lcom/millennialmedia/android/VideoAd;
      //   132: astore 15
      //   134: iload_2
      //   135: ifeq +94 -> 229
      //   138: aload 5
      //   140: ldc 184
      //   142: iconst_0
      //   143: invokeinterface 188 3 0
      //   148: pop
      //   149: ldc 190
      //   151: invokestatic 193	com/millennialmedia/android/MMAdViewSDK$Log:d	(Ljava/lang/String;)V
      //   154: aload 5
      //   156: ldc 195
      //   158: iconst_0
      //   159: invokeinterface 119 3 0
      //   164: pop
      //   165: ldc 197
      //   167: invokestatic 86	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
      //   170: aload_3
      //   171: invokevirtual 42	com/millennialmedia/android/MMAdView:getContext	()Landroid/content/Context;
      //   174: aload 15
      //   176: getfield 200	com/millennialmedia/android/VideoAd:acid	Ljava/lang/String;
      //   179: invokestatic 204	com/millennialmedia/android/MMAdViewController:cachedVideoWasAdded	(Landroid/content/Context;Ljava/lang/String;)V
      //   182: aload 15
      //   184: getfield 208	com/millennialmedia/android/VideoAd:cacheComplete	[Ljava/lang/String;
      //   187: invokestatic 214	com/millennialmedia/android/HttpGetRequest:log	([Ljava/lang/String;)V
      //   190: aload 7
      //   192: invokevirtual 217	com/millennialmedia/android/AdDatabaseHelper:close	()V
      //   195: aload 5
      //   197: invokeinterface 123 1 0
      //   202: pop
      //   203: aload_0
      //   204: getfield 11	com/millennialmedia/android/MMAdViewController$DownloadAdTask:this$0	Lcom/millennialmedia/android/MMAdViewController;
      //   207: aload_3
      //   208: iconst_0
      //   209: invokestatic 221	com/millennialmedia/android/MMAdViewController:access$1100	(Lcom/millennialmedia/android/MMAdViewController;Lcom/millennialmedia/android/MMAdView;Z)V
      //   212: return
      //   213: aload 5
      //   215: ldc 157
      //   217: aconst_null
      //   218: invokeinterface 161 3 0
      //   223: pop
      //   224: iconst_0
      //   225: istore_2
      //   226: goto -127 -> 99
      //   229: aload 5
      //   231: ldc 184
      //   233: iconst_1
      //   234: aload 4
      //   236: ldc 184
      //   238: iconst_0
      //   239: invokeinterface 225 3 0
      //   244: iadd
      //   245: invokeinterface 188 3 0
      //   250: pop
      //   251: ldc 227
      //   253: invokestatic 193	com/millennialmedia/android/MMAdViewSDK$Log:d	(Ljava/lang/String;)V
      //   256: aload 15
      //   258: getfield 230	com/millennialmedia/android/VideoAd:cacheFailed	[Ljava/lang/String;
      //   261: invokestatic 214	com/millennialmedia/android/HttpGetRequest:log	([Ljava/lang/String;)V
      //   264: goto -74 -> 190
      //   267: astore 10
      //   269: aload 7
      //   271: astore 11
      //   273: ldc 30
      //   275: ldc 232
      //   277: invokestatic 38	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
      //   280: pop
      //   281: aload 11
      //   283: ifnull +8 -> 291
      //   286: aload 11
      //   288: invokevirtual 217	com/millennialmedia/android/AdDatabaseHelper:close	()V
      //   291: aload 5
      //   293: invokeinterface 123 1 0
      //   298: pop
      //   299: aload_0
      //   300: getfield 11	com/millennialmedia/android/MMAdViewController$DownloadAdTask:this$0	Lcom/millennialmedia/android/MMAdViewController;
      //   303: aload_3
      //   304: iconst_0
      //   305: invokestatic 221	com/millennialmedia/android/MMAdViewController:access$1100	(Lcom/millennialmedia/android/MMAdViewController;Lcom/millennialmedia/android/MMAdView;Z)V
      //   308: return
      //   309: astore 8
      //   311: aconst_null
      //   312: astore 7
      //   314: aload 7
      //   316: ifnull +8 -> 324
      //   319: aload 7
      //   321: invokevirtual 217	com/millennialmedia/android/AdDatabaseHelper:close	()V
      //   324: aload 5
      //   326: invokeinterface 123 1 0
      //   331: pop
      //   332: aload_0
      //   333: getfield 11	com/millennialmedia/android/MMAdViewController$DownloadAdTask:this$0	Lcom/millennialmedia/android/MMAdViewController;
      //   336: aload_3
      //   337: iconst_0
      //   338: invokestatic 221	com/millennialmedia/android/MMAdViewController:access$1100	(Lcom/millennialmedia/android/MMAdViewController;Lcom/millennialmedia/android/MMAdView;Z)V
      //   341: aload 8
      //   343: athrow
      //   344: astore 8
      //   346: goto -32 -> 314
      //   349: astore 12
      //   351: aload 11
      //   353: astore 7
      //   355: aload 12
      //   357: astore 8
      //   359: goto -45 -> 314
      //   362: astore 20
      //   364: aconst_null
      //   365: astore 11
      //   367: goto -94 -> 273
      //
      // Exception table:
      //   from	to	target	type
      //   126	134	267	android/database/sqlite/SQLiteException
      //   138	190	267	android/database/sqlite/SQLiteException
      //   229	264	267	android/database/sqlite/SQLiteException
      //   113	126	309	finally
      //   126	134	344	finally
      //   138	190	344	finally
      //   229	264	344	finally
      //   273	281	349	finally
      //   113	126	362	android/database/sqlite/SQLiteException
    }

    protected void onPreExecute()
    {
      MMAdView localMMAdView = (MMAdView)MMAdViewController.this.adViewRef.get();
      if (localMMAdView == null)
      {
        Log.e("MillennialMediaSDK", "The reference to the ad view was broken.");
        return;
      }
      MMAdViewSDK.Log.v("DownloadAdTask onPreExecute");
      SharedPreferences.Editor localEditor = localMMAdView.getContext().getSharedPreferences("MillennialMediaSettings", 0).edit();
      localEditor.putBoolean("pendingDownload", true);
      localEditor.commit();
      MMAdViewSDK.Log.v("Setting pendingDownload to TRUE");
    }

    protected void onProgressUpdate()
    {
    }
  }

  private class MMJSInterface
  {
    private MMJSInterface()
    {
    }

    public void countImages(String paramString)
    {
      MMAdView localMMAdView = (MMAdView)MMAdViewController.this.adViewRef.get();
      if (localMMAdView == null)
      {
        Log.e("MillennialMediaSDK", "The reference to the ad view was broken.");
        return;
      }
      MMAdViewSDK.Log.d("size: " + paramString);
      int i;
      if (paramString != null)
        i = new Integer(paramString).intValue();
      while (true)
      {
        MMAdViewSDK.Log.d("num: " + i);
        if (i > 0)
        {
          if (localMMAdView.listener != null);
          try
          {
            localMMAdView.listener.MMAdReturned(localMMAdView);
            Log.i("MillennialMediaSDK", "Millennial ad return success");
            MMAdViewSDK.Log.v("View height: " + localMMAdView.getHeight());
            return;
            Log.e("MillennialMediaSDK", "Image count is null");
            i = 0;
          }
          catch (Exception localException2)
          {
            while (true)
              Log.w("MillennialMediaSDK", "Exception raised in your MMAdListener: ", localException2);
          }
        }
      }
      if (localMMAdView.listener != null);
      try
      {
        localMMAdView.listener.MMAdFailed(localMMAdView);
        Log.i("MillennialMediaSDK", "Millennial ad return failed");
        return;
      }
      catch (Exception localException1)
      {
        while (true)
          Log.w("MillennialMediaSDK", "Exception raised in your MMAdListener: ", localException1);
      }
    }

    public void getUrl(String paramString)
    {
      MMAdViewController.this.nextUrl = paramString;
      MMAdViewSDK.Log.v("nextUrl: " + MMAdViewController.this.nextUrl);
      if (MMAdViewController.this.nextUrl.toLowerCase().startsWith("mmvideo"))
        MMAdViewController.this.shouldLaunchToOverlay = true;
    }

    public void log(String paramString)
    {
      MMAdViewSDK.Log.d(paramString);
    }

    public void overlayTitle(String paramString)
    {
      MMAdViewController.this.overlayTitle = paramString;
    }

    public void overlayTransition(String paramString, long paramLong)
    {
      MMAdViewController.this.overlayTransition = paramString;
      MMAdViewController.this.transitionTime = paramLong;
    }

    public void setLoaded(boolean paramBoolean)
    {
      MMAdView localMMAdView = (MMAdView)MMAdViewController.this.adViewRef.get();
      if (localMMAdView == null)
      {
        Log.e("MillennialMediaSDK", "The reference to the ad view was broken.");
        return;
      }
      if ((localMMAdView.listener == null) || (paramBoolean));
      while (true)
      {
        try
        {
          localMMAdView.listener.MMAdReturned(localMMAdView);
          if (!paramBoolean)
            break;
          Log.i("MillennialMediaSDK", "Millennial ad return success");
          return;
        }
        catch (Exception localException2)
        {
          Log.w("MillennialMediaSDK", "Exception raised in your MMAdListener: ", localException2);
          continue;
        }
        try
        {
          localMMAdView.listener.MMAdFailed(localMMAdView);
        }
        catch (Exception localException1)
        {
          Log.w("MillennialMediaSDK", "Exception raised in your MMAdListener: ", localException1);
        }
      }
      Log.i("MillennialMediaSDK", "Millennial ad return failed");
    }

    public void shouldAccelerate(boolean paramBoolean)
    {
      MMAdView localMMAdView = (MMAdView)MMAdViewController.this.adViewRef.get();
      if (localMMAdView != null)
      {
        if (localMMAdView.accelerate == true)
          MMAdViewController.this.canAccelerate = paramBoolean;
      }
      else
        return;
      MMAdViewController.this.canAccelerate = false;
    }

    public void shouldEnableBottomBar(boolean paramBoolean)
    {
      MMAdViewController.this.shouldEnableBottomBar = paramBoolean;
    }

    public void shouldMakeOverlayTransparent(boolean paramBoolean)
    {
      MMAdViewController.this.shouldMakeOverlayTransparent = paramBoolean;
    }

    public void shouldOpen(String paramString)
    {
      MMAdView localMMAdView = (MMAdView)MMAdViewController.this.adViewRef.get();
      if (localMMAdView == null)
        Log.e("MillennialMediaSDK", "The reference to the ad view was broken.");
      do
      {
        return;
        MMAdViewController.this.shouldLaunchToOverlay = true;
        MMAdViewController.this.handleClick(paramString);
      }
      while (localMMAdView.listener == null);
      try
      {
        localMMAdView.listener.MMAdOverlayLaunched(localMMAdView);
        return;
      }
      catch (Exception localException)
      {
        Log.w("MillennialMediaSDK", "Exception raised in your MMAdListener: ", localException);
      }
    }

    public void shouldOverlay(boolean paramBoolean)
    {
      MMAdViewController.this.shouldLaunchToOverlay = paramBoolean;
    }

    public void shouldResizeOverlay(int paramInt)
    {
      MMAdViewController.this.shouldResizeOverlay = paramInt;
    }

    public void shouldShowBottomBar(boolean paramBoolean)
    {
      MMAdViewController.this.shouldShowBottomBar = paramBoolean;
    }

    public void shouldShowTitlebar(boolean paramBoolean)
    {
      MMAdViewController.this.shouldShowTitlebar = paramBoolean;
    }

    public void vibrate(int paramInt)
    {
      MMAdView localMMAdView = (MMAdView)MMAdViewController.this.adViewRef.get();
      if ((localMMAdView != null) && (localMMAdView.vibrate))
      {
        Activity localActivity = (Activity)localMMAdView.getContext();
        if (localActivity.getPackageManager().checkPermission("android.permission.VIBRATE", localActivity.getPackageName()) == 0)
          ((Vibrator)localActivity.getSystemService("vibrator")).vibrate(paramInt);
      }
      else
      {
        return;
      }
      Log.w("MillennialMediaSDK", "Advertisement is trying to use vibrator but permissions are missing.");
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.MMAdViewController
 * JD-Core Version:    0.6.2
 */