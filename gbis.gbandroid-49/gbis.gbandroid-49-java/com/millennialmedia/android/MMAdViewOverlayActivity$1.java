package com.millennialmedia.android;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

class MMAdViewOverlayActivity$1
  implements SensorEventListener
{
  private long currentTime = 0L;
  private float force = 0.0F;
  private float lastX = 0.0F;
  private float lastY = 0.0F;
  private float lastZ = 0.0F;
  private long prevShakeTime = 0L;
  private long prevTime = 0L;
  private long timeDifference = 0L;
  private float x = 0.0F;
  private float y = 0.0F;
  private float z = 0.0F;

  MMAdViewOverlayActivity$1(MMAdViewOverlayActivity paramMMAdViewOverlayActivity)
  {
  }

  public void onAccuracyChanged(Sensor paramSensor, int paramInt)
  {
  }

  public void onSensorChanged(SensorEvent paramSensorEvent)
  {
    this.currentTime = paramSensorEvent.timestamp;
    this.x = paramSensorEvent.values[0];
    this.y = paramSensorEvent.values[1];
    this.z = paramSensorEvent.values[2];
    this.timeDifference = (this.currentTime - this.prevTime);
    if (this.timeDifference > 500L)
    {
      MMAdViewOverlayActivity.access$000(this.this$0, this.x, this.y, this.z);
      this.force = (Math.abs(this.x + this.y + this.z - this.lastX - this.lastY - this.lastZ) / (float)this.timeDifference);
      if (this.force > 0.2F)
      {
        if (this.currentTime - this.prevShakeTime >= 1000L)
          MMAdViewOverlayActivity.access$100(this.this$0, this.force);
        this.prevShakeTime = this.currentTime;
      }
      this.lastX = this.x;
      this.lastY = this.y;
      this.lastZ = this.z;
      this.prevTime = this.currentTime;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.MMAdViewOverlayActivity.1
 * JD-Core Version:    0.6.2
 */