package com.millennialmedia.android;

import android.app.Activity;
import android.content.Intent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;

public class MMAdViewOverlayActivity extends Activity
{
  private SensorEventListener accelerometerEventListener = new MMAdViewOverlayActivity.1(this);
  private final int interval = 1000;
  private MMAdViewWebOverlay mmOverlay;
  private SensorManager sensorManager;
  protected Boolean shouldAccelerate;
  private final float threshold = 0.2F;

  private void didAccelerate(float paramFloat1, float paramFloat2, float paramFloat3)
  {
    MMAdViewSDK.Log.v("Accelerometer x:" + paramFloat1 + " y:" + paramFloat2 + " z:" + paramFloat3);
    this.mmOverlay.injectJS("javascript:didAccelerate(" + paramFloat1 + "," + paramFloat2 + "," + paramFloat3 + ")");
  }

  private void didShake(float paramFloat)
  {
    MMAdViewSDK.Log.v("Phone shaken: " + paramFloat);
    this.mmOverlay.injectJS("javascript:didShake(" + paramFloat + ")");
  }

  private void startAccelerating()
  {
    this.sensorManager = ((SensorManager)getSystemService("sensor"));
    if (!Boolean.valueOf(this.sensorManager.registerListener(this.accelerometerEventListener, this.sensorManager.getDefaultSensor(1), 1)).booleanValue())
    {
      Log.w("MillennialMediaSDK", "Accelerometer not supported by this device. Unregistering listener.");
      this.sensorManager.unregisterListener(this.accelerometerEventListener, this.sensorManager.getDefaultSensor(1));
      this.accelerometerEventListener = null;
      this.sensorManager = null;
    }
  }

  private void stopAccelerating()
  {
    try
    {
      if ((this.sensorManager != null) && (this.accelerometerEventListener != null))
        this.sensorManager.unregisterListener(this.accelerometerEventListener, this.sensorManager.getDefaultSensor(1));
      return;
    }
    catch (Exception localException)
    {
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    setTheme(16973840);
    super.onCreate(paramBundle);
    long l = 600L;
    boolean bool1 = true;
    boolean bool2 = true;
    Bundle localBundle = getIntent().getExtras();
    String str1;
    int i;
    boolean bool3;
    String str2;
    boolean bool4;
    boolean bool6;
    String str3;
    if (localBundle != null)
    {
      l = localBundle.getLong("transitionTime", 600L);
      str1 = localBundle.getString("overlayTransition");
      i = localBundle.getInt("shouldResizeOverlay", 0);
      bool3 = localBundle.getBoolean("shouldShowTitlebar", false);
      str2 = localBundle.getString("overlayTitle");
      bool1 = localBundle.getBoolean("shouldShowBottomBar", true);
      bool2 = localBundle.getBoolean("shouldEnableBottomBar", true);
      bool4 = localBundle.getBoolean("shouldMakeOverlayTransparent", false);
      bool6 = localBundle.getBoolean("cachedAdView", false);
      str3 = localBundle.getString("overlayOrientation");
    }
    for (boolean bool5 = bool6; ; bool5 = false)
    {
      MMAdViewSDK.Log.v("Padding: " + i + " Time: " + l + " Transition: " + str1 + " Title: " + str2 + " Bottom bar: " + bool2 + " Should accelerate: " + this.shouldAccelerate + " Tranparent: " + bool4 + " Cached Ad: " + bool5);
      String str4 = getIntent().getData().getLastPathSegment();
      MMAdViewSDK.Log.v("Path: " + str4);
      this.mmOverlay = new MMAdViewWebOverlay(this, i, l, str1, bool3, str2, bool1, bool2, bool4);
      setContentView(this.mmOverlay);
      if (getLastNonConfigurationInstance() == null)
        this.mmOverlay.loadWebContent(getIntent().getDataString());
      this.shouldAccelerate = Boolean.valueOf(getIntent().getBooleanExtra("canAccelerate", false));
      if ((bool5) && (str3 != null))
      {
        if (str3.equalsIgnoreCase("landscape"))
          setRequestedOrientation(0);
      }
      else
        return;
      if (str3.equalsIgnoreCase("portrait"))
      {
        setRequestedOrientation(1);
        return;
      }
      setRequestedOrientation(2);
      return;
      i = 0;
      str1 = null;
      bool3 = false;
      str2 = null;
      bool4 = false;
      str3 = null;
    }
  }

  protected void onDestroy()
  {
    super.onDestroy();
    MMAdViewSDK.Log.d("Overlay onDestroy");
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 4) && (paramKeyEvent.getRepeatCount() == 0) && (this.mmOverlay.goBack()))
      return true;
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  protected void onPause()
  {
    super.onPause();
    MMAdViewSDK.Log.d("Overlay onPause");
    if (this.shouldAccelerate.booleanValue())
      stopAccelerating();
    setResult(0);
  }

  protected void onResume()
  {
    super.onResume();
    MMAdViewSDK.Log.d("Overlay onResume");
    if (this.shouldAccelerate.booleanValue())
      startAccelerating();
  }

  public Object onRetainNonConfigurationInstance()
  {
    return this.mmOverlay.getNonConfigurationInstance();
  }

  protected void onStop()
  {
    super.onStop();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.MMAdViewOverlayActivity
 * JD-Core Version:    0.6.2
 */