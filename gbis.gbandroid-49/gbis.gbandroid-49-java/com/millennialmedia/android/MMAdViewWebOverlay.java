package com.millennialmedia.android;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import java.io.IOException;

class MMAdViewWebOverlay extends FrameLayout
{
  private static final int kTitleMarginX = 8;
  private static final int kTitleMarginY = 9;
  private static final int kTransitionDuration = 200;
  private Button backButton;
  private Drawable close;
  private Drawable closeDisabled;
  private LinearLayout content;
  private Button forwardButton;
  private Drawable leftArrow;
  private Drawable leftArrowDisabled;
  private RelativeLayout navBar;
  private Button navCloseButton;
  private String overlayUrl;
  private Drawable rightArrow;
  private Drawable rightArrowDisabled;
  private TextView title;
  Handler viewHandler = new MMAdViewWebOverlay.8(this);
  protected WebView webView;

  MMAdViewWebOverlay(Context paramContext, int paramInt, long paramLong, String paramString1, boolean paramBoolean1, String paramString2, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4)
  {
    super(paramContext);
    setId(15062);
    if (paramContext == null);
    while (true)
    {
      return;
      NonConfigurationInstance localNonConfigurationInstance = (NonConfigurationInstance)((Activity)paramContext).getLastNonConfigurationInstance();
      if (localNonConfigurationInstance != null)
      {
        paramBoolean2 = localNonConfigurationInstance.bottomBarVisible;
        paramBoolean3 = localNonConfigurationInstance.bottomBarEnabled;
        this.webView = localNonConfigurationInstance.webView;
      }
      setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
      float f = paramContext.getResources().getDisplayMetrics().density;
      Integer localInteger = Integer.valueOf((int)(0.0625F * f * paramInt));
      setPadding(localInteger.intValue(), localInteger.intValue(), localInteger.intValue(), localInteger.intValue());
      this.content = new LinearLayout(paramContext);
      this.content.setOrientation(1);
      this.content.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
      addView(this.content);
      if (paramBoolean1)
      {
        RelativeLayout localRelativeLayout = new RelativeLayout(paramContext);
        localRelativeLayout.setLayoutParams(new FrameLayout.LayoutParams(-1, -2));
        localRelativeLayout.setBackgroundColor(-16777216);
        localRelativeLayout.setId(100);
        this.title = new TextView(paramContext);
        this.title.setText(paramString2);
        this.title.setTextColor(-1);
        this.title.setBackgroundColor(-16777216);
        this.title.setTypeface(Typeface.DEFAULT_BOLD);
        this.title.setPadding(8, 9, 8, 9);
        this.title.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
        localRelativeLayout.addView(this.title);
        Button localButton = new Button(paramContext);
        localButton.setBackgroundColor(-16777216);
        localButton.setText("Close");
        localButton.setTextColor(-1);
        localButton.setOnTouchListener(new MMAdViewWebOverlay.1(this));
        RelativeLayout.LayoutParams localLayoutParams1 = new RelativeLayout.LayoutParams(-2, -2);
        localLayoutParams1.addRule(11);
        localRelativeLayout.addView(localButton, localLayoutParams1);
        this.content.addView(localRelativeLayout);
      }
      this.webView = new WebView(paramContext);
      this.webView.setId(200);
      LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(-1, -1);
      localLayoutParams.weight = 1.0F;
      this.webView.setLayoutParams(localLayoutParams);
      this.webView.setWebViewClient(new OverlayWebViewClient());
      this.webView.addJavascriptInterface(new OverlayJSInterface(), "interface");
      WebSettings localWebSettings = this.webView.getSettings();
      localWebSettings.setJavaScriptEnabled(true);
      localWebSettings.setDefaultTextEncodingName("UTF-8");
      int i;
      AssetManager localAssetManager;
      if (paramBoolean4)
      {
        this.webView.setBackgroundColor(0);
        this.content.setBackgroundColor(0);
        this.content.addView(this.webView);
        i = (int)(0.5F + f * 50.0F);
        this.navBar = new RelativeLayout(paramContext);
        this.navBar.setLayoutParams(new FrameLayout.LayoutParams(-1, -2));
        this.navBar.setBackgroundColor(-3355444);
        this.navBar.setId(300);
        this.navCloseButton = new Button(paramContext);
        this.navCloseButton.setBackgroundColor(-16777216);
        localAssetManager = paramContext.getAssets();
      }
      try
      {
        this.close = Drawable.createFromStream(localAssetManager.open("millennial_close.png"), "millennial_close.png");
        this.closeDisabled = Drawable.createFromStream(localAssetManager.open("millennial_close_disabled.png"), "millennial_close_disabled.png");
        setCloseButtonListener(paramBoolean3);
        RelativeLayout.LayoutParams localLayoutParams2 = new RelativeLayout.LayoutParams(i, i);
        localLayoutParams2.addRule(11);
        localLayoutParams2.addRule(15);
        this.navBar.addView(this.navCloseButton, localLayoutParams2);
        this.content.addView(this.navBar);
        if (paramBoolean2)
        {
          this.navBar.setVisibility(0);
          if (localNonConfigurationInstance != null)
            continue;
          animateView(paramString1, paramLong);
          return;
          this.webView.setBackgroundColor(-1);
          this.content.setBackgroundColor(-1);
        }
      }
      catch (IOException localIOException)
      {
        while (true)
        {
          localIOException.printStackTrace();
          continue;
          this.navBar.setVisibility(8);
        }
      }
    }
  }

  private void animateView(String paramString, long paramLong)
  {
    if (paramString == null)
      paramString = "bottomtotop";
    if (paramString.equals("toptobottom"))
    {
      TranslateAnimation localTranslateAnimation1 = new TranslateAnimation(1, 0.0F, 1, 0.0F, 1, -1.0F, 1, 0.0F);
      localTranslateAnimation1.setDuration(paramLong);
      localTranslateAnimation1.setAnimationListener(new MMAdViewWebOverlay.2(this));
      MMAdViewSDK.Log.v("Translate down");
      startAnimation(localTranslateAnimation1);
      return;
    }
    if (paramString.equals("explode"))
    {
      ScaleAnimation localScaleAnimation = new ScaleAnimation(1.1F, 0.9F, 0.1F, 0.9F, 1, 0.5F, 1, 0.5F);
      localScaleAnimation.setDuration(paramLong);
      localScaleAnimation.setAnimationListener(new MMAdViewWebOverlay.3(this));
      MMAdViewSDK.Log.v("Explode");
      startAnimation(localScaleAnimation);
      return;
    }
    TranslateAnimation localTranslateAnimation2 = new TranslateAnimation(1, 0.0F, 1, 0.0F, 1, 1.0F, 1, 0.0F);
    localTranslateAnimation2.setDuration(paramLong);
    localTranslateAnimation2.setAnimationListener(new MMAdViewWebOverlay.4(this));
    MMAdViewSDK.Log.v("Translate up");
    startAnimation(localTranslateAnimation2);
  }

  private void dismiss(boolean paramBoolean)
  {
    MMAdViewSDK.Log.d("Ad overlay closed");
    Activity localActivity = (Activity)getContext();
    if (localActivity == null)
      return;
    if (paramBoolean)
    {
      AlphaAnimation localAlphaAnimation = new AlphaAnimation(1.0F, 0.0F);
      localAlphaAnimation.setDuration(200L);
      localActivity.finish();
      startAnimation(localAlphaAnimation);
      return;
    }
    localActivity.finish();
  }

  Object getNonConfigurationInstance()
  {
    NonConfigurationInstance localNonConfigurationInstance = new NonConfigurationInstance(null);
    this.webView.setWebViewClient(null);
    ((ViewGroup)this.webView.getParent()).removeView(this.webView);
    if (this.navBar.getVisibility() == 0);
    for (boolean bool = true; ; bool = false)
    {
      localNonConfigurationInstance.bottomBarVisible = bool;
      localNonConfigurationInstance.bottomBarEnabled = this.navBar.isEnabled();
      localNonConfigurationInstance.webView = this.webView;
      return localNonConfigurationInstance;
    }
  }

  boolean goBack()
  {
    if (this.webView.canGoBack())
    {
      this.webView.goBack();
      return true;
    }
    return false;
  }

  void injectJS(String paramString)
  {
    this.webView.loadUrl(paramString);
  }

  void loadWebContent(String paramString)
  {
    this.overlayUrl = paramString;
    if (MMAdViewSDK.isConnected(getContext()))
    {
      this.webView.loadUrl(this.overlayUrl);
      return;
    }
    Log.e("MillennialMediaSDK", "No network available, can't load overlay.");
  }

  protected void setBackButtonListener(boolean paramBoolean)
  {
    if (this.backButton != null)
    {
      if (paramBoolean)
      {
        this.backButton.setBackgroundDrawable(this.leftArrow);
        this.backButton.setOnClickListener(new MMAdViewWebOverlay.6(this));
        this.backButton.setEnabled(true);
      }
    }
    else
      return;
    this.backButton.setBackgroundDrawable(this.leftArrowDisabled);
    this.backButton.setEnabled(false);
  }

  protected void setCloseButtonListener(boolean paramBoolean)
  {
    if (this.navCloseButton != null)
    {
      if (paramBoolean)
      {
        this.navCloseButton.setBackgroundDrawable(this.close);
        this.navCloseButton.setOnClickListener(new MMAdViewWebOverlay.7(this));
        this.navCloseButton.setEnabled(true);
      }
    }
    else
      return;
    this.navCloseButton.setBackgroundDrawable(this.closeDisabled);
    this.navCloseButton.setEnabled(false);
  }

  protected void setForwardButtonListener(boolean paramBoolean)
  {
    if (this.forwardButton != null)
    {
      if (paramBoolean)
      {
        this.forwardButton.setBackgroundDrawable(this.rightArrow);
        this.forwardButton.setOnClickListener(new MMAdViewWebOverlay.5(this));
        this.forwardButton.setEnabled(true);
      }
    }
    else
      return;
    this.forwardButton.setBackgroundDrawable(this.rightArrowDisabled);
    this.forwardButton.setEnabled(false);
  }

  private static final class NonConfigurationInstance
  {
    boolean bottomBarEnabled;
    boolean bottomBarVisible;
    WebView webView;
  }

  class OverlayJSInterface
  {
    OverlayJSInterface()
    {
    }

    public void shouldCloseOverlay()
    {
      MMAdViewWebOverlay.this.viewHandler.sendEmptyMessage(2);
    }

    public void shouldEnableBottomBar(boolean paramBoolean)
    {
      MMAdViewSDK.Log.d("Should Enable Bottom Bar: " + paramBoolean);
      MMAdViewWebOverlay.this.viewHandler.post(new MMAdViewWebOverlay.OverlayJSInterface.1(this, paramBoolean));
    }

    public void shouldShowBottomBar(boolean paramBoolean)
    {
      MMAdViewSDK.Log.d("Should show Bottom Bar: " + paramBoolean);
      MMAdViewWebOverlay.this.viewHandler.post(new MMAdViewWebOverlay.OverlayJSInterface.2(this, paramBoolean));
    }

    public void shouldVibrate(long paramLong)
    {
      if (MMAdViewWebOverlay.this.getContext().checkCallingOrSelfPermission("android.permission.VIBRATE") == 0)
        ((Vibrator)MMAdViewWebOverlay.this.getContext().getSystemService("vibrator")).vibrate(paramLong);
    }
  }

  final class OverlayWebViewClient extends MMWebViewClient
  {
    OverlayWebViewClient()
    {
    }

    // ERROR //
    public final void onPageStarted(WebView paramWebView, String paramString, android.graphics.Bitmap paramBitmap)
    {
      // Byte code:
      //   0: aconst_null
      //   1: astore 4
      //   3: new 21	java/lang/StringBuilder
      //   6: dup
      //   7: ldc 23
      //   9: invokespecial 26	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
      //   12: aload_2
      //   13: invokevirtual 30	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   16: invokevirtual 34	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   19: invokestatic 39	com/millennialmedia/android/MMAdViewSDK$Log:d	(Ljava/lang/String;)V
      //   22: aload_2
      //   23: ifnull +25 -> 48
      //   26: aload_0
      //   27: getfield 10	com/millennialmedia/android/MMAdViewWebOverlay$OverlayWebViewClient:this$0	Lcom/millennialmedia/android/MMAdViewWebOverlay;
      //   30: invokevirtual 45	com/millennialmedia/android/MMAdViewWebOverlay:getContext	()Landroid/content/Context;
      //   33: checkcast 47	android/app/Activity
      //   36: astore 5
      //   38: aload 5
      //   40: ifnonnull +9 -> 49
      //   43: ldc 49
      //   45: invokestatic 39	com/millennialmedia/android/MMAdViewSDK$Log:d	(Ljava/lang/String;)V
      //   48: return
      //   49: aload_2
      //   50: invokestatic 55	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
      //   53: astore 6
      //   55: aload 6
      //   57: ifnull -9 -> 48
      //   60: aload 6
      //   62: invokevirtual 58	android/net/Uri:getScheme	()Ljava/lang/String;
      //   65: ifnull -17 -> 48
      //   68: aload 6
      //   70: invokevirtual 58	android/net/Uri:getScheme	()Ljava/lang/String;
      //   73: ldc 60
      //   75: invokevirtual 66	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
      //   78: ifeq +242 -> 320
      //   81: aload 6
      //   83: invokevirtual 69	android/net/Uri:getHost	()Ljava/lang/String;
      //   86: astore 21
      //   88: aload 21
      //   90: ifnull +173 -> 263
      //   93: new 71	com/millennialmedia/android/AdDatabaseHelper
      //   96: dup
      //   97: aload 5
      //   99: invokespecial 74	com/millennialmedia/android/AdDatabaseHelper:<init>	(Landroid/content/Context;)V
      //   102: astore 22
      //   104: aload 22
      //   106: aload 21
      //   108: invokevirtual 78	com/millennialmedia/android/AdDatabaseHelper:getVideoAd	(Ljava/lang/String;)Lcom/millennialmedia/android/VideoAd;
      //   111: astore 4
      //   113: aload 22
      //   115: invokevirtual 81	com/millennialmedia/android/AdDatabaseHelper:close	()V
      //   118: aload 4
      //   120: ifnull +143 -> 263
      //   123: aload 4
      //   125: aload 5
      //   127: invokevirtual 87	com/millennialmedia/android/VideoAd:isOnDisk	(Landroid/content/Context;)Z
      //   130: ifeq +133 -> 263
      //   133: aload 4
      //   135: invokevirtual 91	com/millennialmedia/android/VideoAd:isExpired	()Z
      //   138: ifne +125 -> 263
      //   141: new 93	android/content/Intent
      //   144: dup
      //   145: invokespecial 94	android/content/Intent:<init>	()V
      //   148: aload 5
      //   150: ldc 96
      //   152: invokevirtual 100	android/content/Intent:setClass	(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;
      //   155: astore 24
      //   157: aload 24
      //   159: ldc 101
      //   161: invokevirtual 105	android/content/Intent:setFlags	(I)Landroid/content/Intent;
      //   164: pop
      //   165: aload 24
      //   167: ldc 107
      //   169: iconst_1
      //   170: invokevirtual 111	android/content/Intent:putExtra	(Ljava/lang/String;Z)Landroid/content/Intent;
      //   173: pop
      //   174: aload 24
      //   176: ldc 113
      //   178: aload 21
      //   180: invokevirtual 116	android/content/Intent:putExtra	(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
      //   183: pop
      //   184: aload 24
      //   186: ldc 118
      //   188: aload 21
      //   190: invokevirtual 116	android/content/Intent:putExtra	(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
      //   193: pop
      //   194: aload 4
      //   196: getfield 122	com/millennialmedia/android/VideoAd:storedOnSdCard	Z
      //   199: ifeq +107 -> 306
      //   202: invokestatic 127	android/os/Environment:getExternalStorageState	()Ljava/lang/String;
      //   205: ldc 129
      //   207: invokevirtual 133	java/lang/String:equals	(Ljava/lang/Object;)Z
      //   210: ifeq +96 -> 306
      //   213: aload 24
      //   215: new 21	java/lang/StringBuilder
      //   218: dup
      //   219: invokespecial 134	java/lang/StringBuilder:<init>	()V
      //   222: invokestatic 138	android/os/Environment:getExternalStorageDirectory	()Ljava/io/File;
      //   225: invokevirtual 143	java/io/File:getAbsolutePath	()Ljava/lang/String;
      //   228: invokevirtual 30	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   231: ldc 145
      //   233: invokevirtual 30	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   236: aload 21
      //   238: invokevirtual 30	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   241: ldc 147
      //   243: invokevirtual 30	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   246: invokevirtual 34	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   249: invokestatic 55	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
      //   252: invokevirtual 151	android/content/Intent:setData	(Landroid/net/Uri;)Landroid/content/Intent;
      //   255: pop
      //   256: aload 5
      //   258: aload 24
      //   260: invokevirtual 155	android/app/Activity:startActivity	(Landroid/content/Intent;)V
      //   263: aload_0
      //   264: getfield 10	com/millennialmedia/android/MMAdViewWebOverlay$OverlayWebViewClient:this$0	Lcom/millennialmedia/android/MMAdViewWebOverlay;
      //   267: getfield 159	com/millennialmedia/android/MMAdViewWebOverlay:webView	Landroid/webkit/WebView;
      //   270: invokevirtual 164	android/webkit/WebView:goBack	()V
      //   273: return
      //   274: astore 7
      //   276: ldc 166
      //   278: aload 7
      //   280: invokevirtual 169	android/content/ActivityNotFoundException:getMessage	()Ljava/lang/String;
      //   283: invokestatic 175	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
      //   286: pop
      //   287: return
      //   288: astore 31
      //   290: aconst_null
      //   291: astore 22
      //   293: aload 22
      //   295: ifnull -177 -> 118
      //   298: aload 22
      //   300: invokevirtual 81	com/millennialmedia/android/AdDatabaseHelper:close	()V
      //   303: goto -185 -> 118
      //   306: aload 24
      //   308: aload 21
      //   310: invokestatic 55	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
      //   313: invokevirtual 151	android/content/Intent:setData	(Landroid/net/Uri;)Landroid/content/Intent;
      //   316: pop
      //   317: goto -61 -> 256
      //   320: aload 6
      //   322: invokevirtual 58	android/net/Uri:getScheme	()Ljava/lang/String;
      //   325: ldc 177
      //   327: invokevirtual 66	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
      //   330: ifeq +47 -> 377
      //   333: ldc 179
      //   335: invokestatic 39	com/millennialmedia/android/MMAdViewSDK$Log:d	(Ljava/lang/String;)V
      //   338: new 93	android/content/Intent
      //   341: dup
      //   342: ldc 181
      //   344: aload 6
      //   346: invokespecial 184	android/content/Intent:<init>	(Ljava/lang/String;Landroid/net/Uri;)V
      //   349: astore 19
      //   351: aload 19
      //   353: ldc 101
      //   355: invokevirtual 105	android/content/Intent:setFlags	(I)Landroid/content/Intent;
      //   358: pop
      //   359: aload 5
      //   361: aload 19
      //   363: invokevirtual 155	android/app/Activity:startActivity	(Landroid/content/Intent;)V
      //   366: aload_0
      //   367: getfield 10	com/millennialmedia/android/MMAdViewWebOverlay$OverlayWebViewClient:this$0	Lcom/millennialmedia/android/MMAdViewWebOverlay;
      //   370: getfield 159	com/millennialmedia/android/MMAdViewWebOverlay:webView	Landroid/webkit/WebView;
      //   373: invokevirtual 164	android/webkit/WebView:goBack	()V
      //   376: return
      //   377: aload 6
      //   379: invokevirtual 58	android/net/Uri:getScheme	()Ljava/lang/String;
      //   382: ldc 186
      //   384: invokevirtual 66	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
      //   387: ifeq +68 -> 455
      //   390: ldc 188
      //   392: invokestatic 39	com/millennialmedia/android/MMAdViewSDK$Log:d	(Ljava/lang/String;)V
      //   395: new 21	java/lang/StringBuilder
      //   398: dup
      //   399: ldc 190
      //   401: invokespecial 26	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
      //   404: aload 6
      //   406: invokevirtual 193	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   409: invokevirtual 34	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   412: invokestatic 39	com/millennialmedia/android/MMAdViewSDK$Log:d	(Ljava/lang/String;)V
      //   415: new 93	android/content/Intent
      //   418: dup
      //   419: aload 5
      //   421: ldc 96
      //   423: invokespecial 196	android/content/Intent:<init>	(Landroid/content/Context;Ljava/lang/Class;)V
      //   426: astore 17
      //   428: aload 17
      //   430: aload 6
      //   432: invokevirtual 151	android/content/Intent:setData	(Landroid/net/Uri;)Landroid/content/Intent;
      //   435: pop
      //   436: aload 5
      //   438: aload 17
      //   440: iconst_0
      //   441: invokevirtual 200	android/app/Activity:startActivityForResult	(Landroid/content/Intent;I)V
      //   444: aload_0
      //   445: getfield 10	com/millennialmedia/android/MMAdViewWebOverlay$OverlayWebViewClient:this$0	Lcom/millennialmedia/android/MMAdViewWebOverlay;
      //   448: getfield 159	com/millennialmedia/android/MMAdViewWebOverlay:webView	Landroid/webkit/WebView;
      //   451: invokevirtual 164	android/webkit/WebView:goBack	()V
      //   454: return
      //   455: aload 6
      //   457: invokevirtual 58	android/net/Uri:getScheme	()Ljava/lang/String;
      //   460: ldc 202
      //   462: invokevirtual 66	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
      //   465: ifeq +47 -> 512
      //   468: ldc 204
      //   470: invokestatic 39	com/millennialmedia/android/MMAdViewSDK$Log:d	(Ljava/lang/String;)V
      //   473: new 93	android/content/Intent
      //   476: dup
      //   477: ldc 206
      //   479: aload 6
      //   481: invokespecial 184	android/content/Intent:<init>	(Ljava/lang/String;Landroid/net/Uri;)V
      //   484: astore 15
      //   486: aload 15
      //   488: ldc 101
      //   490: invokevirtual 105	android/content/Intent:setFlags	(I)Landroid/content/Intent;
      //   493: pop
      //   494: aload 5
      //   496: aload 15
      //   498: invokevirtual 155	android/app/Activity:startActivity	(Landroid/content/Intent;)V
      //   501: aload_0
      //   502: getfield 10	com/millennialmedia/android/MMAdViewWebOverlay$OverlayWebViewClient:this$0	Lcom/millennialmedia/android/MMAdViewWebOverlay;
      //   505: getfield 159	com/millennialmedia/android/MMAdViewWebOverlay:webView	Landroid/webkit/WebView;
      //   508: invokevirtual 164	android/webkit/WebView:goBack	()V
      //   511: return
      //   512: aload 6
      //   514: invokevirtual 58	android/net/Uri:getScheme	()Ljava/lang/String;
      //   517: ldc 208
      //   519: invokevirtual 66	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
      //   522: ifeq +47 -> 569
      //   525: ldc 210
      //   527: invokestatic 39	com/millennialmedia/android/MMAdViewSDK$Log:d	(Ljava/lang/String;)V
      //   530: new 93	android/content/Intent
      //   533: dup
      //   534: ldc 181
      //   536: aload 6
      //   538: invokespecial 184	android/content/Intent:<init>	(Ljava/lang/String;Landroid/net/Uri;)V
      //   541: astore 13
      //   543: aload 13
      //   545: ldc 101
      //   547: invokevirtual 105	android/content/Intent:setFlags	(I)Landroid/content/Intent;
      //   550: pop
      //   551: aload 5
      //   553: aload 13
      //   555: invokevirtual 155	android/app/Activity:startActivity	(Landroid/content/Intent;)V
      //   558: aload_0
      //   559: getfield 10	com/millennialmedia/android/MMAdViewWebOverlay$OverlayWebViewClient:this$0	Lcom/millennialmedia/android/MMAdViewWebOverlay;
      //   562: getfield 159	com/millennialmedia/android/MMAdViewWebOverlay:webView	Landroid/webkit/WebView;
      //   565: invokevirtual 164	android/webkit/WebView:goBack	()V
      //   568: return
      //   569: aload 6
      //   571: invokevirtual 58	android/net/Uri:getScheme	()Ljava/lang/String;
      //   574: ldc 212
      //   576: invokevirtual 66	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
      //   579: ifeq +47 -> 626
      //   582: ldc 214
      //   584: invokestatic 39	com/millennialmedia/android/MMAdViewSDK$Log:d	(Ljava/lang/String;)V
      //   587: new 93	android/content/Intent
      //   590: dup
      //   591: ldc 181
      //   593: aload 6
      //   595: invokespecial 184	android/content/Intent:<init>	(Ljava/lang/String;Landroid/net/Uri;)V
      //   598: astore 11
      //   600: aload 11
      //   602: ldc 101
      //   604: invokevirtual 105	android/content/Intent:setFlags	(I)Landroid/content/Intent;
      //   607: pop
      //   608: aload 5
      //   610: aload 11
      //   612: invokevirtual 155	android/app/Activity:startActivity	(Landroid/content/Intent;)V
      //   615: aload_0
      //   616: getfield 10	com/millennialmedia/android/MMAdViewWebOverlay$OverlayWebViewClient:this$0	Lcom/millennialmedia/android/MMAdViewWebOverlay;
      //   619: getfield 159	com/millennialmedia/android/MMAdViewWebOverlay:webView	Landroid/webkit/WebView;
      //   622: invokevirtual 164	android/webkit/WebView:goBack	()V
      //   625: return
      //   626: aload 6
      //   628: invokevirtual 58	android/net/Uri:getScheme	()Ljava/lang/String;
      //   631: ldc 216
      //   633: invokevirtual 66	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
      //   636: ifeq +102 -> 738
      //   639: aload 6
      //   641: invokevirtual 219	android/net/Uri:getLastPathSegment	()Ljava/lang/String;
      //   644: ifnull +94 -> 738
      //   647: aload 6
      //   649: invokevirtual 219	android/net/Uri:getLastPathSegment	()Ljava/lang/String;
      //   652: ldc 221
      //   654: invokevirtual 224	java/lang/String:endsWith	(Ljava/lang/String;)Z
      //   657: ifne +16 -> 673
      //   660: aload 6
      //   662: invokevirtual 219	android/net/Uri:getLastPathSegment	()Ljava/lang/String;
      //   665: ldc 226
      //   667: invokevirtual 224	java/lang/String:endsWith	(Ljava/lang/String;)Z
      //   670: ifeq +63 -> 733
      //   673: new 21	java/lang/StringBuilder
      //   676: dup
      //   677: ldc 190
      //   679: invokespecial 26	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
      //   682: aload 6
      //   684: invokevirtual 193	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   687: invokevirtual 34	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   690: invokestatic 39	com/millennialmedia/android/MMAdViewSDK$Log:d	(Ljava/lang/String;)V
      //   693: new 93	android/content/Intent
      //   696: dup
      //   697: aload 5
      //   699: ldc 96
      //   701: invokespecial 196	android/content/Intent:<init>	(Landroid/content/Context;Ljava/lang/Class;)V
      //   704: astore 9
      //   706: aload 9
      //   708: aload 6
      //   710: invokevirtual 151	android/content/Intent:setData	(Landroid/net/Uri;)Landroid/content/Intent;
      //   713: pop
      //   714: aload 5
      //   716: aload 9
      //   718: iconst_0
      //   719: invokevirtual 200	android/app/Activity:startActivityForResult	(Landroid/content/Intent;I)V
      //   722: aload_0
      //   723: getfield 10	com/millennialmedia/android/MMAdViewWebOverlay$OverlayWebViewClient:this$0	Lcom/millennialmedia/android/MMAdViewWebOverlay;
      //   726: getfield 159	com/millennialmedia/android/MMAdViewWebOverlay:webView	Landroid/webkit/WebView;
      //   729: invokevirtual 164	android/webkit/WebView:goBack	()V
      //   732: return
      //   733: aload_0
      //   734: invokevirtual 229	com/millennialmedia/android/MMAdViewWebOverlay$OverlayWebViewClient:shouldShowAndEnableBottomBar	()V
      //   737: return
      //   738: ldc 231
      //   740: invokestatic 39	com/millennialmedia/android/MMAdViewSDK$Log:d	(Ljava/lang/String;)V
      //   743: aload_0
      //   744: invokevirtual 229	com/millennialmedia/android/MMAdViewWebOverlay$OverlayWebViewClient:shouldShowAndEnableBottomBar	()V
      //   747: return
      //   748: astore 23
      //   750: goto -457 -> 293
      //
      // Exception table:
      //   from	to	target	type
      //   68	88	274	android/content/ActivityNotFoundException
      //   93	104	274	android/content/ActivityNotFoundException
      //   104	118	274	android/content/ActivityNotFoundException
      //   123	256	274	android/content/ActivityNotFoundException
      //   256	263	274	android/content/ActivityNotFoundException
      //   263	273	274	android/content/ActivityNotFoundException
      //   298	303	274	android/content/ActivityNotFoundException
      //   306	317	274	android/content/ActivityNotFoundException
      //   320	376	274	android/content/ActivityNotFoundException
      //   377	454	274	android/content/ActivityNotFoundException
      //   455	511	274	android/content/ActivityNotFoundException
      //   512	568	274	android/content/ActivityNotFoundException
      //   569	625	274	android/content/ActivityNotFoundException
      //   626	673	274	android/content/ActivityNotFoundException
      //   673	732	274	android/content/ActivityNotFoundException
      //   733	737	274	android/content/ActivityNotFoundException
      //   738	747	274	android/content/ActivityNotFoundException
      //   93	104	288	android/database/sqlite/SQLiteException
      //   104	118	748	android/database/sqlite/SQLiteException
    }

    public final void onReceivedError(WebView paramWebView, Error paramError, String paramString1, String paramString2)
    {
      Log.e("MillennialMediaSDK", "Error: " + paramError + "  " + paramString1);
    }

    public final boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
    {
      MMAdViewSDK.Log.d("shouldOverrideUrlLoading: " + paramString);
      return super.shouldOverrideUrlLoading(paramWebView, paramString);
    }

    public final void shouldShowAndEnableBottomBar()
    {
      MMAdViewSDK.Log.v("Showing and enabling bottom bar");
      if (MMAdViewWebOverlay.this.navBar != null)
      {
        MMAdViewWebOverlay.this.navBar.setVisibility(0);
        MMAdViewWebOverlay.this.setCloseButtonListener(true);
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.MMAdViewWebOverlay
 * JD-Core Version:    0.6.2
 */