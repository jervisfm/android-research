package com.millennialmedia.android;

import android.content.Context;
import android.os.Environment;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;

class MMCache extends MMJSObject
{
  // ERROR //
  public MMJSResponse availableCachedVideos(HashMap<String, String> paramHashMap)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 16	com/millennialmedia/android/MMCache:contextRef	Ljava/lang/ref/WeakReference;
    //   4: invokevirtual 22	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
    //   7: checkcast 24	android/content/Context
    //   10: astore_2
    //   11: aload_2
    //   12: ifnull +143 -> 155
    //   15: new 26	com/millennialmedia/android/AdDatabaseHelper
    //   18: dup
    //   19: aload_2
    //   20: invokespecial 29	com/millennialmedia/android/AdDatabaseHelper:<init>	(Landroid/content/Context;)V
    //   23: astore_3
    //   24: aload_3
    //   25: invokevirtual 33	com/millennialmedia/android/AdDatabaseHelper:getAllVideoAds	()Ljava/util/ArrayList;
    //   28: astore 11
    //   30: aload 11
    //   32: astore 5
    //   34: aload_3
    //   35: invokevirtual 36	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   38: aload 5
    //   40: ifnull +115 -> 155
    //   43: new 38	org/json/JSONArray
    //   46: dup
    //   47: invokespecial 39	org/json/JSONArray:<init>	()V
    //   50: astore 6
    //   52: aload 5
    //   54: invokevirtual 45	java/util/ArrayList:iterator	()Ljava/util/Iterator;
    //   57: astore 7
    //   59: aload 7
    //   61: invokeinterface 51 1 0
    //   66: ifeq +64 -> 130
    //   69: aload 7
    //   71: invokeinterface 54 1 0
    //   76: checkcast 56	com/millennialmedia/android/VideoAd
    //   79: astore 9
    //   81: aload 9
    //   83: aload_2
    //   84: invokevirtual 60	com/millennialmedia/android/VideoAd:isOnDisk	(Landroid/content/Context;)Z
    //   87: ifeq -28 -> 59
    //   90: aload 9
    //   92: invokevirtual 63	com/millennialmedia/android/VideoAd:isExpired	()Z
    //   95: ifne -36 -> 59
    //   98: aload 6
    //   100: aload 9
    //   102: getfield 67	com/millennialmedia/android/VideoAd:id	Ljava/lang/String;
    //   105: invokevirtual 71	org/json/JSONArray:put	(Ljava/lang/Object;)Lorg/json/JSONArray;
    //   108: pop
    //   109: goto -50 -> 59
    //   112: astore 13
    //   114: aconst_null
    //   115: astore 5
    //   117: aconst_null
    //   118: astore_3
    //   119: aload_3
    //   120: ifnull -82 -> 38
    //   123: aload_3
    //   124: invokevirtual 36	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   127: goto -89 -> 38
    //   130: new 73	com/millennialmedia/android/MMJSResponse
    //   133: dup
    //   134: invokespecial 74	com/millennialmedia/android/MMJSResponse:<init>	()V
    //   137: astore 8
    //   139: aload 8
    //   141: iconst_1
    //   142: putfield 78	com/millennialmedia/android/MMJSResponse:result	I
    //   145: aload 8
    //   147: aload 6
    //   149: putfield 82	com/millennialmedia/android/MMJSResponse:response	Ljava/lang/Object;
    //   152: aload 8
    //   154: areturn
    //   155: aconst_null
    //   156: areturn
    //   157: astore 4
    //   159: aconst_null
    //   160: astore 5
    //   162: goto -43 -> 119
    //   165: astore 12
    //   167: goto -48 -> 119
    //
    // Exception table:
    //   from	to	target	type
    //   15	24	112	android/database/sqlite/SQLiteException
    //   24	30	157	android/database/sqlite/SQLiteException
    //   34	38	165	android/database/sqlite/SQLiteException
  }

  public MMJSResponse cacheVideo(HashMap<String, String> paramHashMap)
  {
    Context localContext = (Context)this.contextRef.get();
    String str = (String)paramHashMap.get("url");
    if ((str != null) && (localContext != null))
    {
      System.out.println(str);
      HttpEntity localHttpEntity;
      try
      {
        HttpResponse localHttpResponse = new HttpGetRequest().get(str);
        if (localHttpResponse == null)
        {
          MMAdViewSDK.Log.d("HTTP response is null");
          return null;
        }
        localHttpEntity = localHttpResponse.getEntity();
        if (localHttpEntity == null)
        {
          MMAdViewSDK.Log.d("Null HTTP entity");
          return null;
        }
      }
      catch (Exception localException)
      {
        MMAdViewSDK.Log.d("HTTP error: " + localException.getMessage());
        return null;
      }
      if (localHttpEntity.getContentLength() == 0L)
      {
        MMAdViewSDK.Log.d("Millennial ad return failed. Zero content length returned.");
        return null;
      }
      Header localHeader = localHttpEntity.getContentType();
      if ((localHeader != null) && (localHeader.getValue() != null) && (localHeader.getValue().equalsIgnoreCase("application/json")))
        try
        {
          VideoAd localVideoAd = new VideoAd(HttpGetRequest.convertStreamToString(localHttpEntity.getContent()));
          if (Environment.getExternalStorageState().equals("mounted"))
            localVideoAd.storedOnSdCard = true;
          if ((localVideoAd.isValid()) && (localVideoAd.download(localContext)))
            return MMJSResponse.responseWithSuccess();
        }
        catch (IllegalStateException localIllegalStateException)
        {
          localIllegalStateException.printStackTrace();
          MMAdViewSDK.Log.d("Millennial ad return failed. Invalid response data.");
          return null;
        }
        catch (IOException localIOException)
        {
          localIOException.printStackTrace();
          MMAdViewSDK.Log.d("Millennial ad return failed. Invalid response data.");
          return null;
        }
    }
    return null;
  }

  // ERROR //
  public MMJSResponse playCachedVideo(HashMap<String, String> paramHashMap)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 16	com/millennialmedia/android/MMCache:contextRef	Ljava/lang/ref/WeakReference;
    //   4: invokevirtual 22	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
    //   7: checkcast 24	android/content/Context
    //   10: astore_2
    //   11: aload_1
    //   12: ldc 214
    //   14: invokevirtual 96	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   17: checkcast 98	java/lang/String
    //   20: astore_3
    //   21: aload_3
    //   22: ifnull +195 -> 217
    //   25: aload_2
    //   26: ifnull +191 -> 217
    //   29: new 26	com/millennialmedia/android/AdDatabaseHelper
    //   32: dup
    //   33: aload_2
    //   34: invokespecial 29	com/millennialmedia/android/AdDatabaseHelper:<init>	(Landroid/content/Context;)V
    //   37: astore 4
    //   39: aload 4
    //   41: aload_3
    //   42: invokevirtual 218	com/millennialmedia/android/AdDatabaseHelper:getVideoAd	(Ljava/lang/String;)Lcom/millennialmedia/android/VideoAd;
    //   45: astore 7
    //   47: aload 4
    //   49: invokevirtual 36	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   52: aload 7
    //   54: ifnull +163 -> 217
    //   57: aload 7
    //   59: aload_2
    //   60: invokevirtual 60	com/millennialmedia/android/VideoAd:isOnDisk	(Landroid/content/Context;)Z
    //   63: ifeq +154 -> 217
    //   66: aload 7
    //   68: invokevirtual 63	com/millennialmedia/android/VideoAd:isExpired	()Z
    //   71: ifne +146 -> 217
    //   74: new 220	android/content/Intent
    //   77: dup
    //   78: invokespecial 221	android/content/Intent:<init>	()V
    //   81: aload_2
    //   82: ldc 223
    //   84: invokevirtual 227	android/content/Intent:setClass	(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;
    //   87: astore 8
    //   89: aload 8
    //   91: ldc 228
    //   93: invokevirtual 232	android/content/Intent:setFlags	(I)Landroid/content/Intent;
    //   96: pop
    //   97: aload 8
    //   99: ldc 234
    //   101: iconst_1
    //   102: invokevirtual 238	android/content/Intent:putExtra	(Ljava/lang/String;Z)Landroid/content/Intent;
    //   105: pop
    //   106: aload 8
    //   108: ldc 240
    //   110: aload_3
    //   111: invokevirtual 243	android/content/Intent:putExtra	(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    //   114: pop
    //   115: aload 7
    //   117: getfield 195	com/millennialmedia/android/VideoAd:storedOnSdCard	Z
    //   120: ifeq +84 -> 204
    //   123: invokestatic 185	android/os/Environment:getExternalStorageState	()Ljava/lang/String;
    //   126: ldc 187
    //   128: invokevirtual 191	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   131: ifeq +73 -> 204
    //   134: aload 8
    //   136: new 133	java/lang/StringBuilder
    //   139: dup
    //   140: invokespecial 244	java/lang/StringBuilder:<init>	()V
    //   143: invokestatic 248	android/os/Environment:getExternalStorageDirectory	()Ljava/io/File;
    //   146: invokevirtual 253	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   149: invokevirtual 145	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   152: ldc 255
    //   154: invokevirtual 145	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   157: aload_3
    //   158: invokevirtual 145	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   161: ldc_w 257
    //   164: invokevirtual 145	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   167: invokevirtual 148	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   170: invokestatic 263	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
    //   173: invokevirtual 267	android/content/Intent:setData	(Landroid/net/Uri;)Landroid/content/Intent;
    //   176: pop
    //   177: aload_2
    //   178: aload 8
    //   180: invokevirtual 271	android/content/Context:startActivity	(Landroid/content/Intent;)V
    //   183: invokestatic 205	com/millennialmedia/android/MMJSResponse:responseWithSuccess	()Lcom/millennialmedia/android/MMJSResponse;
    //   186: areturn
    //   187: astore 14
    //   189: aconst_null
    //   190: astore 6
    //   192: aload 6
    //   194: ifnull +8 -> 202
    //   197: aload 6
    //   199: invokevirtual 36	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   202: aconst_null
    //   203: areturn
    //   204: aload 8
    //   206: aload_3
    //   207: invokestatic 263	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
    //   210: invokevirtual 267	android/content/Intent:setData	(Landroid/net/Uri;)Landroid/content/Intent;
    //   213: pop
    //   214: goto -37 -> 177
    //   217: aconst_null
    //   218: areturn
    //   219: astore 5
    //   221: aload 4
    //   223: astore 6
    //   225: goto -33 -> 192
    //
    // Exception table:
    //   from	to	target	type
    //   29	39	187	android/database/sqlite/SQLiteException
    //   39	52	219	android/database/sqlite/SQLiteException
  }

  // ERROR //
  public MMJSResponse videoIdExists(HashMap<String, String> paramHashMap)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 16	com/millennialmedia/android/MMCache:contextRef	Ljava/lang/ref/WeakReference;
    //   4: invokevirtual 22	java/lang/ref/WeakReference:get	()Ljava/lang/Object;
    //   7: checkcast 24	android/content/Context
    //   10: astore_2
    //   11: aload_1
    //   12: ldc 214
    //   14: invokevirtual 96	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   17: checkcast 98	java/lang/String
    //   20: astore_3
    //   21: aload_3
    //   22: ifnull +81 -> 103
    //   25: aload_2
    //   26: ifnull +77 -> 103
    //   29: new 26	com/millennialmedia/android/AdDatabaseHelper
    //   32: dup
    //   33: aload_2
    //   34: invokespecial 29	com/millennialmedia/android/AdDatabaseHelper:<init>	(Landroid/content/Context;)V
    //   37: astore 4
    //   39: aload 4
    //   41: aload_3
    //   42: invokevirtual 218	com/millennialmedia/android/AdDatabaseHelper:getVideoAd	(Ljava/lang/String;)Lcom/millennialmedia/android/VideoAd;
    //   45: astore 7
    //   47: aload 7
    //   49: astore 6
    //   51: aload 4
    //   53: invokevirtual 36	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   56: aload 6
    //   58: ifnull +45 -> 103
    //   61: aload 6
    //   63: aload_2
    //   64: invokevirtual 60	com/millennialmedia/android/VideoAd:isOnDisk	(Landroid/content/Context;)Z
    //   67: ifeq +36 -> 103
    //   70: aload 6
    //   72: invokevirtual 63	com/millennialmedia/android/VideoAd:isExpired	()Z
    //   75: ifne +28 -> 103
    //   78: invokestatic 205	com/millennialmedia/android/MMJSResponse:responseWithSuccess	()Lcom/millennialmedia/android/MMJSResponse;
    //   81: areturn
    //   82: astore 9
    //   84: aconst_null
    //   85: astore 6
    //   87: aconst_null
    //   88: astore 4
    //   90: aload 4
    //   92: ifnull -36 -> 56
    //   95: aload 4
    //   97: invokevirtual 36	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   100: goto -44 -> 56
    //   103: aconst_null
    //   104: areturn
    //   105: astore 5
    //   107: aconst_null
    //   108: astore 6
    //   110: goto -20 -> 90
    //   113: astore 8
    //   115: goto -25 -> 90
    //
    // Exception table:
    //   from	to	target	type
    //   29	39	82	android/database/sqlite/SQLiteException
    //   39	47	105	android/database/sqlite/SQLiteException
    //   51	56	113	android/database/sqlite/SQLiteException
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.MMCache
 * JD-Core Version:    0.6.2
 */