package com.millennialmedia.android;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class MMDevice extends MMJSObject
{
  public MMJSResponse getAvailableSchemes(HashMap<String, String> paramHashMap)
  {
    Context localContext = (Context)this.contextRef.get();
    if (localContext != null)
    {
      HandShake localHandShake = HandShake.sharedHandShake(localContext);
      MMJSResponse localMMJSResponse = new MMJSResponse();
      localMMJSResponse.result = 1;
      localMMJSResponse.response = localHandShake.getSchemesJSONArray(localContext);
      return localMMJSResponse;
    }
    return null;
  }

  public MMJSResponse getInfo(HashMap<String, String> paramHashMap)
  {
    Context localContext = (Context)this.contextRef.get();
    if (localContext != null);
    try
    {
      JSONObject localJSONObject1 = new JSONObject();
      try
      {
        localJSONObject1.put("sdkVersion", "4.5.1-12.2.2.a");
        localJSONObject1.put("connection", MMAdViewSDK.getConnectionType(localContext));
        localJSONObject1.put("platform", "Android");
        if (Build.VERSION.RELEASE != null)
          localJSONObject1.put("version", Build.VERSION.RELEASE);
        if (Build.MODEL != null)
          localJSONObject1.put("device", Build.MODEL);
        if (HandShake.sharedHandShake(localContext).hdid)
          localJSONObject1.put("hdid", MMAdViewSDK.getHdid(localContext));
        while (true)
        {
          localJSONObject1.put("mmdid", MMAdViewSDK.getMMdid(localContext));
          DisplayMetrics localDisplayMetrics = localContext.getResources().getDisplayMetrics();
          localJSONObject1.put("density", new Float(localDisplayMetrics.density));
          localJSONObject1.put("height", new Integer(localDisplayMetrics.heightPixels));
          localJSONObject1.put("width", new Integer(localDisplayMetrics.widthPixels));
          Locale localLocale = Locale.getDefault();
          if (localLocale != null)
          {
            localJSONObject1.put("language", localLocale.getLanguage());
            localJSONObject1.put("country", localLocale.getCountry());
          }
          JSONObject localJSONObject3 = new JSONObject();
          localJSONObject3.put("name", "MAC-ID");
          localJSONObject3.put("path", "/");
          localJSONObject3.put("value", MMAdViewSDK.macId);
          JSONArray localJSONArray = new JSONArray();
          localJSONArray.put(localJSONObject3);
          localJSONObject1.put("cookies", localJSONArray);
          localJSONObject2 = localJSONObject1;
          MMJSResponse localMMJSResponse = new MMJSResponse();
          localMMJSResponse.result = 1;
          localMMJSResponse.response = localJSONObject2;
          return localMMJSResponse;
          localJSONObject1.put("auid", MMAdViewSDK.getAuid(localContext));
        }
      }
      catch (JSONException localJSONException1)
      {
        while (true)
          localJSONObject2 = localJSONObject1;
      }
      return null;
    }
    catch (JSONException localJSONException2)
    {
      while (true)
        JSONObject localJSONObject2 = null;
    }
  }

  public MMJSResponse getOrientation(HashMap<String, String> paramHashMap)
  {
    Context localContext = (Context)this.contextRef.get();
    int i;
    if (localContext != null)
    {
      i = localContext.getResources().getConfiguration().orientation;
      if (i != 0)
        break label106;
    }
    label106: for (int j = ((WindowManager)localContext.getSystemService("window")).getDefaultDisplay().getOrientation(); ; j = i)
    {
      MMJSResponse localMMJSResponse = new MMJSResponse();
      localMMJSResponse.result = 1;
      switch (j)
      {
      default:
      case 2:
      }
      for (localMMJSResponse.response = "portrait"; ; localMMJSResponse.response = "landscape")
        return localMMJSResponse;
      return null;
    }
  }

  public MMJSResponse isSchemeAvailable(HashMap<String, String> paramHashMap)
  {
    String str = (String)paramHashMap.get("scheme");
    Context localContext = (Context)this.contextRef.get();
    if ((str != null) && (localContext != null))
    {
      Intent localIntent = new Intent("android.intent.action.VIEW", Uri.parse(str));
      if (localContext.getPackageManager().queryIntentActivities(localIntent, 65536).size() > 0)
        return MMJSResponse.responseWithSuccess();
    }
    return null;
  }

  public MMJSResponse setMMDID(HashMap<String, String> paramHashMap)
  {
    String str = (String)paramHashMap.get("mmdid");
    Context localContext = (Context)this.contextRef.get();
    if (localContext != null)
      HandShake.sharedHandShake(localContext).setMMdid(localContext, str);
    return null;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.MMDevice
 * JD-Core Version:    0.6.2
 */