package com.millennialmedia.android;

import android.content.Context;
import android.os.Environment;
import android.os.StatFs;
import android.webkit.MimeTypeMap;
import java.io.File;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import org.json.JSONArray;

class MMFileManager extends MMJSObject
{
  static final String CREATIVE_CACHE_DIR = "creativecache";
  private boolean isExternal;
  private boolean isInitialized;
  private File root;

  // ERROR //
  static void cleanupCache(Context paramContext, boolean paramBoolean)
  {
    // Byte code:
    //   0: invokestatic 29	android/os/Environment:getExternalStorageState	()Ljava/lang/String;
    //   3: ldc 31
    //   5: invokevirtual 37	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   8: ifeq +68 -> 76
    //   11: new 39	java/io/File
    //   14: dup
    //   15: new 41	java/lang/StringBuilder
    //   18: dup
    //   19: invokespecial 42	java/lang/StringBuilder:<init>	()V
    //   22: invokestatic 46	android/os/Environment:getExternalStorageDirectory	()Ljava/io/File;
    //   25: invokevirtual 49	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   28: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   31: ldc 55
    //   33: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   36: ldc 57
    //   38: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   41: invokevirtual 60	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   44: invokespecial 63	java/io/File:<init>	(Ljava/lang/String;)V
    //   47: astore 4
    //   49: aload 4
    //   51: invokevirtual 67	java/io/File:exists	()Z
    //   54: istore 5
    //   56: iload 5
    //   58: ifeq +17 -> 75
    //   61: iload_1
    //   62: ifeq +34 -> 96
    //   65: lconst_0
    //   66: lstore 7
    //   68: aload 4
    //   70: lload 7
    //   72: invokestatic 71	com/millennialmedia/android/MMFileManager:cleanupDirectory	(Ljava/io/File;J)V
    //   75: return
    //   76: new 39	java/io/File
    //   79: dup
    //   80: aload_0
    //   81: invokevirtual 76	android/content/Context:getCacheDir	()Ljava/io/File;
    //   84: ldc 78
    //   86: invokespecial 81	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   89: astore_3
    //   90: aload_3
    //   91: astore 4
    //   93: goto -44 -> 49
    //   96: aload_0
    //   97: invokestatic 87	com/millennialmedia/android/HandShake:sharedHandShake	(Landroid/content/Context;)Lcom/millennialmedia/android/HandShake;
    //   100: getfield 91	com/millennialmedia/android/HandShake:creativeCacheTimeout	J
    //   103: lstore 7
    //   105: goto -37 -> 68
    //   108: astore_2
    //   109: return
    //   110: astore 6
    //   112: return
    //
    // Exception table:
    //   from	to	target	type
    //   0	49	108	java/lang/Exception
    //   49	56	108	java/lang/Exception
    //   68	75	108	java/lang/Exception
    //   76	90	108	java/lang/Exception
    //   96	105	108	java/lang/Exception
    //   68	75	110	java/lang/SecurityException
    //   96	105	110	java/lang/SecurityException
  }

  private static void cleanupDirectory(File paramFile, long paramLong)
  {
    File[] arrayOfFile = paramFile.listFiles();
    int i = arrayOfFile.length;
    int j = 0;
    if (j < i)
    {
      File localFile = arrayOfFile[j];
      if (localFile.isFile())
        if (System.currentTimeMillis() - localFile.lastModified() > paramLong)
          localFile.delete();
      while (true)
      {
        j++;
        break;
        if (localFile.isDirectory())
          try
          {
            cleanupDirectory(localFile, paramLong);
            if (localFile.list().length == 0)
              localFile.delete();
          }
          catch (SecurityException localSecurityException)
          {
          }
      }
    }
  }

  private boolean initialize()
  {
    if (!this.isInitialized);
    try
    {
      Context localContext = (Context)this.contextRef.get();
      if (localContext != null)
      {
        boolean bool = Environment.getExternalStorageState().equals("mounted");
        this.isExternal = bool;
        if (!bool)
          break label110;
        this.root = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/.mmsyscache/creativecache" + "/");
        if (this.root.exists())
          break label130;
        if (!this.root.mkdirs());
      }
      label130: for (this.isInitialized = true; ; this.isInitialized = true)
      {
        label105: return this.isInitialized;
        label110: this.root = new File(localContext.getCacheDir(), "creativecache/");
        break;
      }
    }
    catch (Exception localException)
    {
      break label105;
    }
  }

  public MMJSResponse cleanupCache(HashMap<String, String> paramHashMap)
  {
    long l;
    if (initialize())
    {
      boolean bool1 = paramHashMap.containsKey("clear");
      boolean bool2 = false;
      if (bool1)
        bool2 = Boolean.parseBoolean((String)paramHashMap.get("clear"));
      if (!bool2)
        break label51;
      l = 0L;
    }
    while (true)
      try
      {
        cleanupDirectory(this.root, l);
        return null;
        label51: Context localContext = (Context)this.contextRef.get();
        if (localContext != null)
          l = HandShake.sharedHandShake(localContext).creativeCacheTimeout;
      }
      catch (SecurityException localSecurityException)
      {
        continue;
        l = 259200000L;
      }
  }

  // ERROR //
  public MMJSResponse downloadFile(HashMap<String, String> paramHashMap)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: aload_0
    //   3: invokespecial 140	com/millennialmedia/android/MMFileManager:initialize	()Z
    //   6: ifeq +212 -> 218
    //   9: aload_1
    //   10: ldc 161
    //   12: invokevirtual 147	java/util/HashMap:containsKey	(Ljava/lang/Object;)Z
    //   15: ifeq +203 -> 218
    //   18: aload_1
    //   19: ldc 163
    //   21: invokevirtual 147	java/util/HashMap:containsKey	(Ljava/lang/Object;)Z
    //   24: ifeq +194 -> 218
    //   27: new 165	java/net/URL
    //   30: dup
    //   31: aload_1
    //   32: ldc 163
    //   34: invokevirtual 150	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   37: checkcast 33	java/lang/String
    //   40: invokespecial 166	java/net/URL:<init>	(Ljava/lang/String;)V
    //   43: invokevirtual 170	java/net/URL:openConnection	()Ljava/net/URLConnection;
    //   46: checkcast 172	java/net/HttpURLConnection
    //   49: astore 4
    //   51: aload 4
    //   53: invokevirtual 176	java/net/HttpURLConnection:getInputStream	()Ljava/io/InputStream;
    //   56: astore 12
    //   58: sipush 4096
    //   61: newarray byte
    //   63: astore 13
    //   65: new 178	java/io/FileOutputStream
    //   68: dup
    //   69: new 39	java/io/File
    //   72: dup
    //   73: aload_0
    //   74: getfield 134	com/millennialmedia/android/MMFileManager:root	Ljava/io/File;
    //   77: aload_1
    //   78: ldc 161
    //   80: invokevirtual 150	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   83: checkcast 33	java/lang/String
    //   86: invokespecial 81	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   89: invokespecial 181	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   92: astore 5
    //   94: aload 12
    //   96: aload 13
    //   98: invokevirtual 187	java/io/InputStream:read	([B)I
    //   101: istore 16
    //   103: iload 16
    //   105: ifle +50 -> 155
    //   108: aload 5
    //   110: aload 13
    //   112: iconst_0
    //   113: iload 16
    //   115: invokevirtual 191	java/io/FileOutputStream:write	([BII)V
    //   118: goto -24 -> 94
    //   121: astore 15
    //   123: aload 4
    //   125: ifnull +8 -> 133
    //   128: aload 4
    //   130: invokevirtual 194	java/net/HttpURLConnection:disconnect	()V
    //   133: aload 5
    //   135: ifnull +8 -> 143
    //   138: aload 5
    //   140: invokevirtual 197	java/io/FileOutputStream:close	()V
    //   143: iconst_0
    //   144: istore 6
    //   146: iload 6
    //   148: ifeq +70 -> 218
    //   151: invokestatic 203	com/millennialmedia/android/MMJSResponse:responseWithSuccess	()Lcom/millennialmedia/android/MMJSResponse;
    //   154: areturn
    //   155: aload 4
    //   157: ifnull +8 -> 165
    //   160: aload 4
    //   162: invokevirtual 194	java/net/HttpURLConnection:disconnect	()V
    //   165: aload 5
    //   167: invokevirtual 197	java/io/FileOutputStream:close	()V
    //   170: iconst_1
    //   171: istore 6
    //   173: goto -27 -> 146
    //   176: astore 17
    //   178: iconst_1
    //   179: istore 6
    //   181: goto -35 -> 146
    //   184: astore 7
    //   186: iconst_0
    //   187: istore 6
    //   189: goto -43 -> 146
    //   192: astore 8
    //   194: aconst_null
    //   195: astore 5
    //   197: aload_2
    //   198: ifnull +7 -> 205
    //   201: aload_2
    //   202: invokevirtual 194	java/net/HttpURLConnection:disconnect	()V
    //   205: aload 5
    //   207: ifnull +8 -> 215
    //   210: aload 5
    //   212: invokevirtual 197	java/io/FileOutputStream:close	()V
    //   215: aload 8
    //   217: athrow
    //   218: aconst_null
    //   219: areturn
    //   220: astore 9
    //   222: goto -7 -> 215
    //   225: astore 11
    //   227: aload 4
    //   229: astore_2
    //   230: aload 11
    //   232: astore 8
    //   234: aconst_null
    //   235: astore 5
    //   237: goto -40 -> 197
    //   240: astore 14
    //   242: aload 4
    //   244: astore_2
    //   245: aload 14
    //   247: astore 8
    //   249: goto -52 -> 197
    //   252: astore_3
    //   253: aconst_null
    //   254: astore 4
    //   256: aconst_null
    //   257: astore 5
    //   259: goto -136 -> 123
    //   262: astore 10
    //   264: aconst_null
    //   265: astore 5
    //   267: goto -144 -> 123
    //
    // Exception table:
    //   from	to	target	type
    //   94	103	121	java/lang/Exception
    //   108	118	121	java/lang/Exception
    //   165	170	176	java/lang/Exception
    //   138	143	184	java/lang/Exception
    //   27	51	192	finally
    //   210	215	220	java/lang/Exception
    //   51	94	225	finally
    //   94	103	240	finally
    //   108	118	240	finally
    //   27	51	252	java/lang/Exception
    //   51	94	262	java/lang/Exception
  }

  public MMJSResponse getDirectoryContents(HashMap<String, String> paramHashMap)
  {
    if (initialize())
    {
      JSONArray localJSONArray = new JSONArray();
      String[] arrayOfString = this.root.list();
      int i = arrayOfString.length;
      for (int j = 0; j < i; j++)
        localJSONArray.put(arrayOfString[j]);
      MMJSResponse localMMJSResponse = new MMJSResponse();
      localMMJSResponse.result = 1;
      localMMJSResponse.response = localJSONArray;
      return localMMJSResponse;
    }
    return null;
  }

  // ERROR //
  public MMJSResponse getFileContents(HashMap<String, String> paramHashMap)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: aload_0
    //   3: invokespecial 140	com/millennialmedia/android/MMFileManager:initialize	()Z
    //   6: ifeq +137 -> 143
    //   9: aload_1
    //   10: ldc 161
    //   12: invokevirtual 147	java/util/HashMap:containsKey	(Ljava/lang/Object;)Z
    //   15: ifeq +128 -> 143
    //   18: new 39	java/io/File
    //   21: dup
    //   22: aload_0
    //   23: getfield 134	com/millennialmedia/android/MMFileManager:root	Ljava/io/File;
    //   26: aload_1
    //   27: ldc 161
    //   29: invokevirtual 150	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   32: checkcast 33	java/lang/String
    //   35: invokespecial 81	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   38: astore_3
    //   39: new 223	java/io/FileInputStream
    //   42: dup
    //   43: aload_3
    //   44: invokespecial 224	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   47: astore 4
    //   49: aload_3
    //   50: invokevirtual 227	java/io/File:length	()J
    //   53: l2i
    //   54: newarray byte
    //   56: astore 6
    //   58: aload 4
    //   60: aload 6
    //   62: invokevirtual 228	java/io/FileInputStream:read	([B)I
    //   65: pop
    //   66: aload 4
    //   68: invokevirtual 229	java/io/FileInputStream:close	()V
    //   71: aload 6
    //   73: ifnull +70 -> 143
    //   76: new 199	com/millennialmedia/android/MMJSResponse
    //   79: dup
    //   80: invokespecial 212	com/millennialmedia/android/MMJSResponse:<init>	()V
    //   83: astore 7
    //   85: aload 7
    //   87: iconst_1
    //   88: putfield 216	com/millennialmedia/android/MMJSResponse:result	I
    //   91: aload 7
    //   93: aload 6
    //   95: putfield 233	com/millennialmedia/android/MMJSResponse:dataResponse	[B
    //   98: aload 7
    //   100: areturn
    //   101: astore 14
    //   103: aconst_null
    //   104: astore 4
    //   106: aload 4
    //   108: ifnull +8 -> 116
    //   111: aload 4
    //   113: invokevirtual 229	java/io/FileInputStream:close	()V
    //   116: aconst_null
    //   117: astore 6
    //   119: goto -48 -> 71
    //   122: astore 8
    //   124: aconst_null
    //   125: astore 6
    //   127: goto -56 -> 71
    //   130: astore 10
    //   132: aload_2
    //   133: ifnull +7 -> 140
    //   136: aload_2
    //   137: invokevirtual 229	java/io/FileInputStream:close	()V
    //   140: aload 10
    //   142: athrow
    //   143: aconst_null
    //   144: areturn
    //   145: astore 13
    //   147: goto -76 -> 71
    //   150: astore 11
    //   152: goto -12 -> 140
    //   155: astore 9
    //   157: aload 4
    //   159: astore_2
    //   160: aload 9
    //   162: astore 10
    //   164: goto -32 -> 132
    //   167: astore 5
    //   169: goto -63 -> 106
    //
    // Exception table:
    //   from	to	target	type
    //   18	49	101	java/lang/Exception
    //   111	116	122	java/lang/Exception
    //   18	49	130	finally
    //   66	71	145	java/lang/Exception
    //   136	140	150	java/lang/Exception
    //   49	66	155	finally
    //   49	66	167	java/lang/Exception
  }

  public MMJSResponse getFreeDiskSpace(HashMap<String, String> paramHashMap)
  {
    if (initialize())
    {
      MMJSResponse localMMJSResponse = new MMJSResponse();
      localMMJSResponse.result = 1;
      StatFs localStatFs = new StatFs(this.root.getAbsolutePath());
      localMMJSResponse.response = new Long(localStatFs.getAvailableBlocks() * localStatFs.getBlockSize());
      return localMMJSResponse;
    }
    return null;
  }

  public MMJSResponse getMimeType(HashMap<String, String> paramHashMap)
  {
    if (initialize())
    {
      String[] arrayOfString = ((String)paramHashMap.get("path")).split("\\.");
      String str = MimeTypeMap.getSingleton().getMimeTypeFromExtension(arrayOfString[(-1 + arrayOfString.length)]);
      if (str != null)
      {
        MMJSResponse localMMJSResponse = new MMJSResponse();
        localMMJSResponse.result = 1;
        localMMJSResponse.response = str;
        return localMMJSResponse;
      }
    }
    return null;
  }

  public MMJSResponse moveFile(HashMap<String, String> paramHashMap)
  {
    if (initialize())
      try
      {
        String str1 = (String)paramHashMap.get("fromPath");
        String str2 = (String)paramHashMap.get("toPath");
        if ((str1 != null) && (str2 != null) && (new File(this.root, str1).renameTo(new File(this.root, str2))))
        {
          MMJSResponse localMMJSResponse = MMJSResponse.responseWithSuccess();
          return localMMJSResponse;
        }
      }
      catch (Exception localException)
      {
      }
    return null;
  }

  public MMJSResponse removeAtPath(HashMap<String, String> paramHashMap)
  {
    if (initialize())
      try
      {
        String str = (String)paramHashMap.get("path");
        if ((str != null) && (new File(this.root, str).delete()))
        {
          MMJSResponse localMMJSResponse = MMJSResponse.responseWithSuccess();
          return localMMJSResponse;
        }
      }
      catch (Exception localException)
      {
      }
    return null;
  }

  // ERROR //
  public MMJSResponse writeData(HashMap<String, String> paramHashMap)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: aload_0
    //   3: invokespecial 140	com/millennialmedia/android/MMFileManager:initialize	()Z
    //   6: ifeq +142 -> 148
    //   9: aload_1
    //   10: ldc 161
    //   12: invokevirtual 147	java/util/HashMap:containsKey	(Ljava/lang/Object;)Z
    //   15: ifeq +133 -> 148
    //   18: aload_1
    //   19: ldc_w 279
    //   22: invokevirtual 147	java/util/HashMap:containsKey	(Ljava/lang/Object;)Z
    //   25: ifeq +123 -> 148
    //   28: new 39	java/io/File
    //   31: dup
    //   32: aload_0
    //   33: getfield 134	com/millennialmedia/android/MMFileManager:root	Ljava/io/File;
    //   36: aload_1
    //   37: ldc 161
    //   39: invokevirtual 150	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   42: checkcast 33	java/lang/String
    //   45: invokespecial 81	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   48: astore_3
    //   49: aload_1
    //   50: ldc_w 279
    //   53: invokevirtual 150	java/util/HashMap:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   56: checkcast 33	java/lang/String
    //   59: invokestatic 285	com/millennialmedia/android/Base64:decode	(Ljava/lang/String;)[B
    //   62: astore 10
    //   64: new 178	java/io/FileOutputStream
    //   67: dup
    //   68: aload_3
    //   69: invokespecial 181	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   72: astore 7
    //   74: aload 7
    //   76: aload 10
    //   78: invokevirtual 288	java/io/FileOutputStream:write	([B)V
    //   81: aload 7
    //   83: invokevirtual 197	java/io/FileOutputStream:close	()V
    //   86: iconst_1
    //   87: istore 8
    //   89: iload 8
    //   91: ifeq +57 -> 148
    //   94: invokestatic 203	com/millennialmedia/android/MMJSResponse:responseWithSuccess	()Lcom/millennialmedia/android/MMJSResponse;
    //   97: areturn
    //   98: astore 13
    //   100: iconst_1
    //   101: istore 8
    //   103: goto -14 -> 89
    //   106: astore 6
    //   108: aconst_null
    //   109: astore 7
    //   111: aload 7
    //   113: ifnull +8 -> 121
    //   116: aload 7
    //   118: invokevirtual 197	java/io/FileOutputStream:close	()V
    //   121: iconst_0
    //   122: istore 8
    //   124: goto -35 -> 89
    //   127: astore 9
    //   129: iconst_0
    //   130: istore 8
    //   132: goto -43 -> 89
    //   135: astore 4
    //   137: aload_2
    //   138: ifnull +7 -> 145
    //   141: aload_2
    //   142: invokevirtual 197	java/io/FileOutputStream:close	()V
    //   145: aload 4
    //   147: athrow
    //   148: aconst_null
    //   149: areturn
    //   150: astore 5
    //   152: goto -7 -> 145
    //   155: astore 12
    //   157: aload 7
    //   159: astore_2
    //   160: aload 12
    //   162: astore 4
    //   164: goto -27 -> 137
    //   167: astore 11
    //   169: goto -58 -> 111
    //
    // Exception table:
    //   from	to	target	type
    //   81	86	98	java/lang/Exception
    //   28	74	106	java/lang/Exception
    //   116	121	127	java/lang/Exception
    //   28	74	135	finally
    //   141	145	150	java/lang/Exception
    //   74	81	155	finally
    //   74	81	167	java/lang/Exception
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.MMFileManager
 * JD-Core Version:    0.6.2
 */