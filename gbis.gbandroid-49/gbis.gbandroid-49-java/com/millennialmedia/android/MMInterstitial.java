package com.millennialmedia.android;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import java.lang.ref.WeakReference;
import java.util.HashMap;

class MMInterstitial extends MMJSObject
{
  public MMJSResponse close(HashMap<String, String> paramHashMap)
  {
    Activity localActivity = (Activity)this.contextRef.get();
    if (localActivity != null)
    {
      localActivity.finish();
      return MMJSResponse.responseWithSuccess();
    }
    return null;
  }

  public MMJSResponse show(HashMap<String, String> paramHashMap)
  {
    String str = (String)paramHashMap.get("url");
    Activity localActivity = (Activity)this.contextRef.get();
    if ((str != null) && (localActivity != null))
    {
      Intent localIntent = new Intent(localActivity, MMAdViewOverlayActivity.class);
      localIntent.setData(Uri.parse(str));
      localActivity.startActivityForResult(localIntent, 0);
      return MMJSResponse.responseWithSuccess();
    }
    return null;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.MMInterstitial
 * JD-Core Version:    0.6.2
 */