package com.millennialmedia.android;

import android.content.Context;
import java.lang.ref.WeakReference;

class MMJSObject
{
  protected WeakReference<Context> contextRef;

  void setContext(Context paramContext)
  {
    this.contextRef = new WeakReference(paramContext);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.MMJSObject
 * JD-Core Version:    0.6.2
 */