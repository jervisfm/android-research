package com.millennialmedia.android;

import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

class MMJSResponse
{
  static final int ERROR = 0;
  static final int SUCCESS = 1;
  String className;
  byte[] dataResponse;
  String methodName;
  Object response;
  int result;

  static MMJSResponse responseWithError()
  {
    return responseWithError("An unknown error occured.");
  }

  static MMJSResponse responseWithError(String paramString)
  {
    MMJSResponse localMMJSResponse = new MMJSResponse();
    localMMJSResponse.result = 0;
    localMMJSResponse.response = paramString;
    return localMMJSResponse;
  }

  static MMJSResponse responseWithSuccess()
  {
    return responseWithSuccess("Success.");
  }

  static MMJSResponse responseWithSuccess(String paramString)
  {
    MMJSResponse localMMJSResponse = new MMJSResponse();
    localMMJSResponse.result = 1;
    localMMJSResponse.response = paramString;
    return localMMJSResponse;
  }

  String toJSONString()
  {
    try
    {
      if ((this.className == null) || (this.methodName == null))
        break label122;
      JSONObject localJSONObject = new JSONObject();
      localJSONObject.put("class", this.className);
      localJSONObject.put("call", this.methodName);
      localJSONObject.put("result", this.result);
      if (this.response != null)
        localJSONObject.put("response", this.response);
      while (true)
      {
        return localJSONObject.toString();
        if (this.dataResponse == null)
          break;
        localJSONObject.put("response", Base64.encode(this.dataResponse));
      }
    }
    catch (JSONException localJSONException)
    {
      Log.e("MillennialMediaSDK", localJSONException.getMessage());
      return "";
    }
    return "";
    label122: return "";
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.MMJSResponse
 * JD-Core Version:    0.6.2
 */