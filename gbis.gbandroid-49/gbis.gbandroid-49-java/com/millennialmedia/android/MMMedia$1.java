package com.millennialmedia.android;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import java.io.File;

class MMMedia$1
  implements Runnable
{
  MMMedia$1(MMMedia paramMMMedia, Activity paramActivity, Object paramObject)
  {
  }

  public void run()
  {
    Intent localIntent = new Intent("android.media.action.IMAGE_CAPTURE");
    MMMedia.access$002(this.this$0, new File(this.val$activity.getCacheDir(), "tmp_mm_" + String.valueOf(System.currentTimeMillis()) + ".jpg"));
    localIntent.putExtra("output", Uri.fromFile(MMMedia.access$000(this.this$0)));
    localIntent.putExtra("return-data", true);
    MMMedia.access$102(this.this$0, new MMMedia.1.1(this));
    MMMedia.access$100(this.this$0).startActivityForResult(localIntent, 0);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.MMMedia.1
 * JD-Core Version:    0.6.2
 */