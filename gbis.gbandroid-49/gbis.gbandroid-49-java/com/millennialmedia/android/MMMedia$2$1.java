package com.millennialmedia.android;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import java.io.File;

class MMMedia$2$1 extends Activity
{
  MMMedia$2$1(MMMedia.2 param2)
  {
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    Cursor localCursor = managedQuery(paramIntent.getData(), new String[] { "_data" }, null, null, null);
    if (localCursor != null)
    {
      int i = localCursor.getColumnIndexOrThrow("_data");
      localCursor.moveToFirst();
      MMMedia.access$002(this.this$1.this$0, new File(localCursor.getString(i)));
      localCursor.close();
    }
    synchronized (this.this$1.val$lock)
    {
      this.this$1.val$lock.notify();
      return;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.MMMedia.2.1
 * JD-Core Version:    0.6.2
 */