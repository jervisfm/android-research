package com.millennialmedia.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import java.io.File;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;

class MMMedia extends MMJSObject
{
  private Activity tmpActivity;
  private File tmpFile;

  private boolean isCameraAvailable()
  {
    Context localContext = (Context)this.contextRef.get();
    if ((localContext != null) && (localContext.getPackageManager().checkPermission("android.permission.CAMERA", localContext.getPackageName()) == 0))
    {
      Intent localIntent = new Intent("android.media.action.IMAGE_CAPTURE");
      return localContext.getPackageManager().queryIntentActivities(localIntent, 65536).size() > 0;
    }
    return false;
  }

  private boolean isPictureChooserAvailable()
  {
    Context localContext = (Context)this.contextRef.get();
    if (localContext != null)
    {
      Intent localIntent = new Intent();
      localIntent.setType("image/*");
      localIntent.setAction("android.intent.action.GET_CONTENT");
      return localContext.getPackageManager().queryIntentActivities(localIntent, 65536).size() > 0;
    }
    return false;
  }

  public MMJSResponse availableSourceTypes(HashMap<String, String> paramHashMap)
  {
    JSONArray localJSONArray = new JSONArray();
    if (isCameraAvailable())
      localJSONArray.put("Camera");
    if (isPictureChooserAvailable())
      localJSONArray.put("Photo Library");
    MMJSResponse localMMJSResponse = new MMJSResponse();
    localMMJSResponse.result = 1;
    localMMJSResponse.response = localJSONArray;
    return localMMJSResponse;
  }

  public MMJSResponse getPicture(HashMap<String, String> paramHashMap)
  {
    return null;
  }

  public MMJSResponse isSourceTypeAvailable(HashMap<String, String> paramHashMap)
  {
    String str = (String)paramHashMap.get("sourceType");
    if (str != null)
    {
      if ((str.equalsIgnoreCase("Camera")) && (isCameraAvailable()))
        return MMJSResponse.responseWithSuccess();
      if ((str.equalsIgnoreCase("Photo Library")) && (isPictureChooserAvailable()))
        return MMJSResponse.responseWithSuccess();
    }
    return null;
  }

  public MMJSResponse playAudio(HashMap<String, String> paramHashMap)
  {
    Activity localActivity = (Activity)this.contextRef.get();
    String str = (String)paramHashMap.get("path");
    if ((localActivity != null) && (str != null))
    {
      if (Environment.getExternalStorageState().equals("mounted"));
      for (File localFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/.mmsyscache/creativecache" + "/" + str); localFile.exists(); localFile = new File(localActivity.getCacheDir(), "creativecache/" + str))
      {
        MediaPlayer localMediaPlayer = MediaPlayer.create(localActivity, Uri.fromFile(localFile));
        localMediaPlayer.start();
        localMediaPlayer.setOnCompletionListener(new MMMedia.3(this));
        return MMJSResponse.responseWithSuccess();
      }
    }
    return null;
  }

  public MMJSResponse playVideo(HashMap<String, String> paramHashMap)
  {
    return null;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.MMMedia
 * JD-Core Version:    0.6.2
 */