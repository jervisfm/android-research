package com.millennialmedia.android;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import java.util.HashMap;

class MMNotification$1
  implements Runnable
{
  MMNotification$1(MMNotification paramMMNotification, Context paramContext, HashMap paramHashMap)
  {
  }

  public void run()
  {
    AlertDialog localAlertDialog = new AlertDialog.Builder(this.val$context).create();
    if (this.val$finalArguments.containsKey("title"))
      localAlertDialog.setTitle((CharSequence)this.val$finalArguments.get("title"));
    if (this.val$finalArguments.containsKey("message"))
      localAlertDialog.setMessage((CharSequence)this.val$finalArguments.get("message"));
    if (this.val$finalArguments.containsKey("cancelButton"))
      localAlertDialog.setButton(-2, (CharSequence)this.val$finalArguments.get("cancelButton"), this.this$0);
    if (this.val$finalArguments.containsKey("buttons"))
    {
      String[] arrayOfString = ((String)this.val$finalArguments.get("buttons")).split(",");
      if (arrayOfString.length > 0)
        localAlertDialog.setButton(-3, arrayOfString[0], this.this$0);
      if (arrayOfString.length > 1)
        localAlertDialog.setButton(-1, arrayOfString[1], this.this$0);
    }
    localAlertDialog.show();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.MMNotification.1
 * JD-Core Version:    0.6.2
 */