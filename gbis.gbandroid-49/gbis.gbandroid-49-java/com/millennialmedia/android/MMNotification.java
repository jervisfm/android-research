package com.millennialmedia.android;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.pm.PackageManager;
import android.os.Vibrator;
import java.lang.ref.WeakReference;
import java.util.HashMap;

class MMNotification extends MMJSObject
  implements DialogInterface.OnClickListener
{
  private int index;

  public MMJSResponse alert(HashMap<String, String> paramHashMap)
  {
    try
    {
      Context localContext = (Context)this.contextRef.get();
      if (localContext != null)
        ((Activity)localContext).runOnUiThread(new MMNotification.1(this, localContext, paramHashMap));
      while (true)
      {
        try
        {
          wait();
          localMMJSResponse = new MMJSResponse();
          localMMJSResponse.result = 1;
          localMMJSResponse.response = new Integer(this.index);
          return localMMJSResponse;
        }
        catch (Exception localException)
        {
          localException.printStackTrace();
        }
        MMJSResponse localMMJSResponse = null;
      }
    }
    finally
    {
    }
  }

  public void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    if (paramInt == -2);
    try
    {
      this.index = 0;
      if (paramInt == -3)
        this.index = 1;
      if (paramInt == -1)
        this.index = 2;
      paramDialogInterface.cancel();
      notify();
      return;
    }
    finally
    {
    }
  }

  public MMJSResponse vibrate(HashMap<String, String> paramHashMap)
  {
    Context localContext = (Context)this.contextRef.get();
    if (paramHashMap.containsKey("duration"));
    for (long l = ()(1000.0D * Float.parseFloat((String)paramHashMap.get("duration"))); ; l = 0L)
    {
      if ((localContext != null) && (l > 0L))
      {
        if (localContext.getPackageManager().checkPermission("android.permission.VIBRATE", localContext.getPackageName()) == 0)
        {
          ((Vibrator)localContext.getSystemService("vibrator")).vibrate(l);
          return MMJSResponse.responseWithSuccess();
        }
        return MMJSResponse.responseWithError("The required permissions to vibrate are not set.");
      }
      return null;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.MMNotification
 * JD-Core Version:    0.6.2
 */