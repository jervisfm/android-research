package com.millennialmedia.android;

import android.webkit.WebView;
import android.webkit.WebViewClient;

class MMWebViewClient extends WebViewClient
{
  public boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
  {
    boolean bool1 = paramString.substring(0, 6).equalsIgnoreCase("mmsdk:");
    boolean bool2 = false;
    if (bool1)
    {
      MMAdViewSDK.Log.v("Running JS bridge command: " + paramString);
      new Thread(new MMCommand(paramWebView, paramString)).start();
      bool2 = true;
    }
    return bool2;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.MMWebViewClient
 * JD-Core Version:    0.6.2
 */