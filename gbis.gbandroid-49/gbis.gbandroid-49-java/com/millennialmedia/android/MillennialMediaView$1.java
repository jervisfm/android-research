package com.millennialmedia.android;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.view.SurfaceHolder;

class MillennialMediaView$1
  implements MediaPlayer.OnVideoSizeChangedListener
{
  MillennialMediaView$1(MillennialMediaView paramMillennialMediaView)
  {
  }

  public void onVideoSizeChanged(MediaPlayer paramMediaPlayer, int paramInt1, int paramInt2)
  {
    MillennialMediaView.access$002(this.this$0, paramMediaPlayer.getVideoWidth());
    MillennialMediaView.access$102(this.this$0, paramMediaPlayer.getVideoHeight());
    if ((MillennialMediaView.access$000(this.this$0) != 0) && (MillennialMediaView.access$100(this.this$0) != 0))
      this.this$0.getHolder().setFixedSize(MillennialMediaView.access$000(this.this$0), MillennialMediaView.access$100(this.this$0));
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.MillennialMediaView.1
 * JD-Core Version:    0.6.2
 */