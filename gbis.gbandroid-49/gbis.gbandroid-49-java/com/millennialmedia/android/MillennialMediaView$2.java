package com.millennialmedia.android;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.view.SurfaceHolder;
import android.widget.MediaController;

class MillennialMediaView$2
  implements MediaPlayer.OnPreparedListener
{
  MillennialMediaView$2(MillennialMediaView paramMillennialMediaView)
  {
  }

  public void onPrepared(MediaPlayer paramMediaPlayer)
  {
    MillennialMediaView.access$202(this.this$0, 2);
    MillennialMediaView.access$302(this.this$0, MillennialMediaView.access$402(this.this$0, MillennialMediaView.access$502(this.this$0, true)));
    if (MillennialMediaView.access$600(this.this$0) != null)
      MillennialMediaView.access$600(this.this$0).onPrepared(MillennialMediaView.access$700(this.this$0));
    if (MillennialMediaView.access$800(this.this$0) != null)
      MillennialMediaView.access$800(this.this$0).setEnabled(true);
    MillennialMediaView.access$002(this.this$0, paramMediaPlayer.getVideoWidth());
    MillennialMediaView.access$102(this.this$0, paramMediaPlayer.getVideoHeight());
    int i = MillennialMediaView.access$900(this.this$0);
    if (i != 0)
      this.this$0.seekTo(i);
    if ((MillennialMediaView.access$000(this.this$0) != 0) && (MillennialMediaView.access$100(this.this$0) != 0))
    {
      this.this$0.getHolder().setFixedSize(MillennialMediaView.access$000(this.this$0), MillennialMediaView.access$100(this.this$0));
      if ((MillennialMediaView.access$1000(this.this$0) == MillennialMediaView.access$000(this.this$0)) && (MillennialMediaView.access$1100(this.this$0) == MillennialMediaView.access$100(this.this$0)))
      {
        if (MillennialMediaView.access$1200(this.this$0) != 3)
          break label248;
        this.this$0.start();
        if (MillennialMediaView.access$800(this.this$0) != null)
          MillennialMediaView.access$800(this.this$0).show();
      }
    }
    label248: 
    while (MillennialMediaView.access$1200(this.this$0) != 3)
    {
      do
        return;
      while ((this.this$0.isPlaying()) || ((i == 0) && (this.this$0.getCurrentPosition() <= 0)) || (MillennialMediaView.access$800(this.this$0) == null));
      MillennialMediaView.access$800(this.this$0).show(0);
      return;
    }
    this.this$0.start();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.MillennialMediaView.2
 * JD-Core Version:    0.6.2
 */