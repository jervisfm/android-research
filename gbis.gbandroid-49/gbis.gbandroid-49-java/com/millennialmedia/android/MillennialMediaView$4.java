package com.millennialmedia.android;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.widget.MediaController;

class MillennialMediaView$4
  implements MediaPlayer.OnCompletionListener
{
  MillennialMediaView$4(MillennialMediaView paramMillennialMediaView)
  {
  }

  public void onCompletion(MediaPlayer paramMediaPlayer)
  {
    MillennialMediaView.access$202(this.this$0, 5);
    MillennialMediaView.access$1202(this.this$0, 5);
    if (MillennialMediaView.access$800(this.this$0) != null)
      MillennialMediaView.access$800(this.this$0).hide();
    if (MillennialMediaView.access$1300(this.this$0) != null)
      MillennialMediaView.access$1300(this.this$0).onCompletion(MillennialMediaView.access$700(this.this$0));
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.MillennialMediaView.4
 * JD-Core Version:    0.6.2
 */