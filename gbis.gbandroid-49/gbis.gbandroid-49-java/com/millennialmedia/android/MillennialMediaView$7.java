package com.millennialmedia.android;

import android.media.MediaPlayer;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.widget.MediaController;

class MillennialMediaView$7
  implements SurfaceHolder.Callback
{
  MillennialMediaView$7(MillennialMediaView paramMillennialMediaView)
  {
  }

  public void surfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3)
  {
    int i = 1;
    MillennialMediaView.access$1002(this.this$0, paramInt2);
    MillennialMediaView.access$1102(this.this$0, paramInt3);
    int j;
    if (MillennialMediaView.access$1200(this.this$0) == 3)
    {
      j = i;
      if ((MillennialMediaView.access$000(this.this$0) != paramInt2) || (MillennialMediaView.access$100(this.this$0) != paramInt3))
        break label161;
    }
    while (true)
    {
      if ((MillennialMediaView.access$700(this.this$0) != null) && (j != 0) && (i != 0))
      {
        if (MillennialMediaView.access$900(this.this$0) != 0)
          this.this$0.seekTo(MillennialMediaView.access$900(this.this$0));
        this.this$0.start();
        if (MillennialMediaView.access$800(this.this$0) != null)
        {
          if (MillennialMediaView.access$800(this.this$0).isShowing())
            MillennialMediaView.access$800(this.this$0).hide();
          MillennialMediaView.access$800(this.this$0).show();
        }
      }
      return;
      j = 0;
      break;
      label161: i = 0;
    }
  }

  public void surfaceCreated(SurfaceHolder paramSurfaceHolder)
  {
    MillennialMediaView.access$1702(this.this$0, paramSurfaceHolder);
    if ((MillennialMediaView.access$700(this.this$0) != null) && (MillennialMediaView.access$200(this.this$0) == 6) && (MillennialMediaView.access$1200(this.this$0) == 7))
    {
      MillennialMediaView.access$700(this.this$0).setDisplay(MillennialMediaView.access$1700(this.this$0));
      return;
    }
    this.this$0.openVideo();
  }

  public void surfaceDestroyed(SurfaceHolder paramSurfaceHolder)
  {
    MillennialMediaView.access$1702(this.this$0, null);
    if (MillennialMediaView.access$800(this.this$0) != null)
      MillennialMediaView.access$800(this.this$0).hide();
    if (MillennialMediaView.access$200(this.this$0) != 6)
      MillennialMediaView.access$1800(this.this$0, true);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.MillennialMediaView.7
 * JD-Core Version:    0.6.2
 */