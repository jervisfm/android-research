package com.millennialmedia.android;

import android.content.Context;

class PreCacheWorker extends Thread
{
  private static boolean busy;
  private static String currentDownload = "";
  private Context appContext;
  private String[] cachedVideos;

  private PreCacheWorker(String[] paramArrayOfString, Context paramContext)
  {
    this.cachedVideos = paramArrayOfString;
    this.appContext = paramContext.getApplicationContext();
  }

  static boolean isCurrentlyDownloading(String paramString)
  {
    return currentDownload.equalsIgnoreCase(paramString);
  }

  static void preCacheVideos(String[] paramArrayOfString, Context paramContext)
  {
    try
    {
      if (!busy)
        new PreCacheWorker(paramArrayOfString, paramContext).start();
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  // ERROR //
  public void run()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 24	com/millennialmedia/android/PreCacheWorker:cachedVideos	[Ljava/lang/String;
    //   4: astore_1
    //   5: aload_1
    //   6: arraylength
    //   7: istore_2
    //   8: iconst_0
    //   9: istore_3
    //   10: iload_3
    //   11: iload_2
    //   12: if_icmpge +496 -> 508
    //   15: aload_1
    //   16: iload_3
    //   17: aaload
    //   18: astore 4
    //   20: new 58	com/millennialmedia/android/HttpGetRequest
    //   23: dup
    //   24: invokespecial 59	com/millennialmedia/android/HttpGetRequest:<init>	()V
    //   27: aload 4
    //   29: invokevirtual 63	com/millennialmedia/android/HttpGetRequest:get	(Ljava/lang/String;)Lorg/apache/http/HttpResponse;
    //   32: astore 6
    //   34: aload 6
    //   36: ifnonnull +11 -> 47
    //   39: ldc 65
    //   41: invokestatic 71	com/millennialmedia/android/MMAdViewSDK$Log:d	(Ljava/lang/String;)V
    //   44: goto +486 -> 530
    //   47: aload 6
    //   49: invokeinterface 77 1 0
    //   54: astore 7
    //   56: aload 7
    //   58: ifnonnull +39 -> 97
    //   61: ldc 79
    //   63: invokestatic 71	com/millennialmedia/android/MMAdViewSDK$Log:d	(Ljava/lang/String;)V
    //   66: goto +464 -> 530
    //   69: astore 5
    //   71: new 81	java/lang/StringBuilder
    //   74: dup
    //   75: ldc 83
    //   77: invokespecial 85	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   80: aload 5
    //   82: invokevirtual 89	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   85: invokevirtual 93	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   88: invokevirtual 96	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   91: invokestatic 71	com/millennialmedia/android/MMAdViewSDK$Log:d	(Ljava/lang/String;)V
    //   94: goto +436 -> 530
    //   97: aload 7
    //   99: invokeinterface 102 1 0
    //   104: lconst_0
    //   105: lcmp
    //   106: ifne +11 -> 117
    //   109: ldc 104
    //   111: invokestatic 71	com/millennialmedia/android/MMAdViewSDK$Log:d	(Ljava/lang/String;)V
    //   114: goto +416 -> 530
    //   117: aload 7
    //   119: invokeinterface 108 1 0
    //   124: astore 8
    //   126: aload 8
    //   128: ifnull +402 -> 530
    //   131: aload 8
    //   133: invokeinterface 113 1 0
    //   138: ifnull +392 -> 530
    //   141: aload 8
    //   143: invokeinterface 113 1 0
    //   148: ldc 115
    //   150: invokevirtual 39	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
    //   153: ifeq +377 -> 530
    //   156: new 117	com/millennialmedia/android/VideoAd
    //   159: dup
    //   160: aload 7
    //   162: invokeinterface 121 1 0
    //   167: invokestatic 125	com/millennialmedia/android/HttpGetRequest:convertStreamToString	(Ljava/io/InputStream;)Ljava/lang/String;
    //   170: invokespecial 126	com/millennialmedia/android/VideoAd:<init>	(Ljava/lang/String;)V
    //   173: astore 9
    //   175: invokestatic 131	android/os/Environment:getExternalStorageState	()Ljava/lang/String;
    //   178: ldc 133
    //   180: invokevirtual 137	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   183: ifeq +9 -> 192
    //   186: aload 9
    //   188: iconst_1
    //   189: putfield 140	com/millennialmedia/android/VideoAd:storedOnSdCard	Z
    //   192: aload 9
    //   194: invokevirtual 144	com/millennialmedia/android/VideoAd:isValid	()Z
    //   197: ifeq +333 -> 530
    //   200: aload 9
    //   202: aload_0
    //   203: getfield 32	com/millennialmedia/android/PreCacheWorker:appContext	Landroid/content/Context;
    //   206: invokestatic 150	com/millennialmedia/android/MMAdViewController:initCachedAdDirectory	(Lcom/millennialmedia/android/VideoAd;Landroid/content/Context;)Ljava/io/File;
    //   209: astore 12
    //   211: new 81	java/lang/StringBuilder
    //   214: dup
    //   215: ldc 152
    //   217: invokespecial 85	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   220: aload 12
    //   222: invokevirtual 155	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   225: invokevirtual 96	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   228: invokestatic 158	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
    //   231: aload 9
    //   233: getfield 161	com/millennialmedia/android/VideoAd:contentUrl	Ljava/lang/String;
    //   236: ldc 163
    //   238: aload 12
    //   240: invokestatic 167	com/millennialmedia/android/MMAdViewController:downloadComponent	(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Z
    //   243: istore 13
    //   245: iload 13
    //   247: ifeq +276 -> 523
    //   250: iconst_0
    //   251: istore 18
    //   253: iload 18
    //   255: aload 9
    //   257: getfield 171	com/millennialmedia/android/VideoAd:buttons	Ljava/util/ArrayList;
    //   260: invokevirtual 177	java/util/ArrayList:size	()I
    //   263: if_icmpge +260 -> 523
    //   266: aload 9
    //   268: getfield 171	com/millennialmedia/android/VideoAd:buttons	Ljava/util/ArrayList;
    //   271: iload 18
    //   273: invokevirtual 180	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   276: checkcast 182	com/millennialmedia/android/VideoImage
    //   279: astore 19
    //   281: aload 19
    //   283: getfield 185	com/millennialmedia/android/VideoImage:imageUrl	Ljava/lang/String;
    //   286: aload 19
    //   288: invokevirtual 188	com/millennialmedia/android/VideoImage:getImageName	()Ljava/lang/String;
    //   291: aload 12
    //   293: invokestatic 167	com/millennialmedia/android/MMAdViewController:downloadComponent	(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Z
    //   296: istore 20
    //   298: iload 20
    //   300: ifeq +43 -> 343
    //   303: iinc 18 1
    //   306: iload 20
    //   308: istore 13
    //   310: goto -57 -> 253
    //   313: astore 11
    //   315: aload 11
    //   317: invokevirtual 191	java/lang/IllegalStateException:printStackTrace	()V
    //   320: ldc 193
    //   322: invokestatic 71	com/millennialmedia/android/MMAdViewSDK$Log:d	(Ljava/lang/String;)V
    //   325: goto +205 -> 530
    //   328: astore 10
    //   330: aload 10
    //   332: invokevirtual 194	java/io/IOException:printStackTrace	()V
    //   335: ldc 193
    //   337: invokestatic 71	com/millennialmedia/android/MMAdViewSDK$Log:d	(Ljava/lang/String;)V
    //   340: goto +190 -> 530
    //   343: iload 20
    //   345: istore 14
    //   347: iload 14
    //   349: ifne +46 -> 395
    //   352: aload_0
    //   353: getfield 32	com/millennialmedia/android/PreCacheWorker:appContext	Landroid/content/Context;
    //   356: aload 9
    //   358: getfield 197	com/millennialmedia/android/VideoAd:id	Ljava/lang/String;
    //   361: invokestatic 201	com/millennialmedia/android/MMAdViewController:deleteAd	(Landroid/content/Context;Ljava/lang/String;)V
    //   364: aload 9
    //   366: getfield 204	com/millennialmedia/android/VideoAd:cacheFailed	[Ljava/lang/String;
    //   369: invokestatic 208	com/millennialmedia/android/HttpGetRequest:log	([Ljava/lang/String;)V
    //   372: new 81	java/lang/StringBuilder
    //   375: dup
    //   376: ldc 210
    //   378: invokespecial 85	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   381: iload 14
    //   383: invokevirtual 213	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
    //   386: invokevirtual 96	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   389: invokestatic 158	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
    //   392: goto +138 -> 530
    //   395: ldc 215
    //   397: invokestatic 158	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
    //   400: new 217	com/millennialmedia/android/AdDatabaseHelper
    //   403: dup
    //   404: aload_0
    //   405: getfield 32	com/millennialmedia/android/PreCacheWorker:appContext	Landroid/content/Context;
    //   408: invokespecial 220	com/millennialmedia/android/AdDatabaseHelper:<init>	(Landroid/content/Context;)V
    //   411: astore 15
    //   413: aload 15
    //   415: aload 9
    //   417: invokevirtual 224	com/millennialmedia/android/AdDatabaseHelper:storeAd	(Lcom/millennialmedia/android/VideoAd;)V
    //   420: aload 9
    //   422: getfield 227	com/millennialmedia/android/VideoAd:acid	Ljava/lang/String;
    //   425: ifnull +26 -> 451
    //   428: aload 9
    //   430: getfield 227	com/millennialmedia/android/VideoAd:acid	Ljava/lang/String;
    //   433: invokevirtual 230	java/lang/String:length	()I
    //   436: ifle +15 -> 451
    //   439: aload_0
    //   440: getfield 32	com/millennialmedia/android/PreCacheWorker:appContext	Landroid/content/Context;
    //   443: aload 9
    //   445: getfield 227	com/millennialmedia/android/VideoAd:acid	Ljava/lang/String;
    //   448: invokestatic 233	com/millennialmedia/android/MMAdViewController:cachedVideoWasAdded	(Landroid/content/Context;Ljava/lang/String;)V
    //   451: aload 15
    //   453: invokevirtual 236	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   456: aload 9
    //   458: getfield 239	com/millennialmedia/android/VideoAd:cacheComplete	[Ljava/lang/String;
    //   461: invokestatic 208	com/millennialmedia/android/HttpGetRequest:log	([Ljava/lang/String;)V
    //   464: goto -92 -> 372
    //   467: astore 16
    //   469: aconst_null
    //   470: astore 15
    //   472: aload 16
    //   474: invokevirtual 240	android/database/sqlite/SQLiteException:printStackTrace	()V
    //   477: aload 15
    //   479: ifnull -23 -> 456
    //   482: aload 15
    //   484: invokevirtual 236	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   487: goto -31 -> 456
    //   490: astore 17
    //   492: aconst_null
    //   493: astore 15
    //   495: aload 15
    //   497: ifnull +8 -> 505
    //   500: aload 15
    //   502: invokevirtual 236	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   505: aload 17
    //   507: athrow
    //   508: iconst_0
    //   509: putstatic 42	com/millennialmedia/android/PreCacheWorker:busy	Z
    //   512: return
    //   513: astore 17
    //   515: goto -20 -> 495
    //   518: astore 16
    //   520: goto -48 -> 472
    //   523: iload 13
    //   525: istore 14
    //   527: goto -180 -> 347
    //   530: iinc 3 1
    //   533: goto -523 -> 10
    //
    // Exception table:
    //   from	to	target	type
    //   20	34	69	java/lang/Exception
    //   39	44	69	java/lang/Exception
    //   47	56	69	java/lang/Exception
    //   156	192	313	java/lang/IllegalStateException
    //   156	192	328	java/io/IOException
    //   400	413	467	android/database/sqlite/SQLiteException
    //   400	413	490	finally
    //   413	451	513	finally
    //   472	477	513	finally
    //   413	451	518	android/database/sqlite/SQLiteException
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.PreCacheWorker
 * JD-Core Version:    0.6.2
 */