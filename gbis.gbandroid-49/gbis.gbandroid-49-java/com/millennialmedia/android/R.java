package com.millennialmedia.android;

public final class R
{
  public static final class attr
  {
    public static int accelerate = 2130771972;
    public static int acid = 2130771969;
    public static int adType = 2130771970;
    public static int age = 2130771974;
    public static int apid = 2130771968;
    public static int children = 2130771982;
    public static int education = 2130771983;
    public static int ethnicity = 2130771979;
    public static int gender = 2130771975;
    public static int goalId = 2130771985;
    public static int ignoreDensityScaling = 2130771973;
    public static int income = 2130771977;
    public static int keywords = 2130771978;
    public static int marital = 2130771981;
    public static int orientation = 2130771980;
    public static int politics = 2130771984;
    public static int refreshInterval = 2130771971;
    public static int zip = 2130771976;
  }

  public static final class styleable
  {
    public static final int[] MMAdView = { 2130771968, 2130771969, 2130771970, 2130771971, 2130771972, 2130771973, 2130771974, 2130771975, 2130771976, 2130771977, 2130771978, 2130771979, 2130771980, 2130771981, 2130771982, 2130771983, 2130771984, 2130771985 };
    public static final int MMAdView_accelerate = 4;
    public static final int MMAdView_acid = 1;
    public static final int MMAdView_adType = 2;
    public static final int MMAdView_age = 6;
    public static final int MMAdView_apid = 0;
    public static final int MMAdView_children = 14;
    public static final int MMAdView_education = 15;
    public static final int MMAdView_ethnicity = 11;
    public static final int MMAdView_gender = 7;
    public static final int MMAdView_goalId = 17;
    public static final int MMAdView_ignoreDensityScaling = 5;
    public static final int MMAdView_income = 9;
    public static final int MMAdView_keywords = 10;
    public static final int MMAdView_marital = 13;
    public static final int MMAdView_orientation = 12;
    public static final int MMAdView_politics = 16;
    public static final int MMAdView_refreshInterval = 3;
    public static final int MMAdView_zip = 8;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.R
 * JD-Core Version:    0.6.2
 */