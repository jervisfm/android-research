package com.millennialmedia.android;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class VideoAd$1
  implements Parcelable.Creator<VideoAd>
{
  public final VideoAd createFromParcel(Parcel paramParcel)
  {
    return new VideoAd(paramParcel);
  }

  public final VideoAd[] newArray(int paramInt)
  {
    return new VideoAd[paramInt];
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.VideoAd.1
 * JD-Core Version:    0.6.2
 */