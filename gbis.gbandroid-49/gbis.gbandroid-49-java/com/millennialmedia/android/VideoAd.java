package com.millennialmedia.android;

import android.content.Context;
import android.os.Environment;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class VideoAd extends BasicCachedAd
  implements Parcelable
{
  public static final Parcelable.Creator<VideoAd> CREATOR = new VideoAd.1();
  ArrayList<VideoLogEvent> activities = new ArrayList();
  ArrayList<VideoImage> buttons = new ArrayList();
  String[] cacheComplete;
  String[] cacheFailed;
  long contentLength;
  long duration;
  String[] endActivity;
  String onCompletionUrl;
  boolean showControls;
  boolean showCountdown;
  String[] startActivity;
  boolean stayInPlayer;
  boolean storedOnSdCard;
  String[] videoError;

  VideoAd()
  {
  }

  VideoAd(Parcel paramParcel)
  {
    try
    {
      this.id = paramParcel.readString();
      this.acid = paramParcel.readString();
      this.contentUrl = paramParcel.readString();
      this.startActivity = new String[paramParcel.readInt()];
      paramParcel.readStringArray(this.startActivity);
      this.endActivity = new String[paramParcel.readInt()];
      paramParcel.readStringArray(this.endActivity);
      boolean[] arrayOfBoolean = new boolean[4];
      paramParcel.readBooleanArray(arrayOfBoolean);
      this.showControls = arrayOfBoolean[0];
      this.stayInPlayer = arrayOfBoolean[1];
      this.showCountdown = arrayOfBoolean[2];
      this.storedOnSdCard = arrayOfBoolean[3];
      String str = paramParcel.readString();
      SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
      try
      {
        this.expiration = localSimpleDateFormat.parse(str);
        this.onCompletionUrl = paramParcel.readString();
        this.duration = paramParcel.readLong();
        this.contentLength = paramParcel.readLong();
        this.buttons = paramParcel.readArrayList(VideoImage.class.getClassLoader());
        this.activities = paramParcel.readArrayList(VideoLogEvent.class.getClassLoader());
        this.deferredViewStart = paramParcel.readLong();
        this.cacheComplete = new String[paramParcel.readInt()];
        paramParcel.readStringArray(this.cacheComplete);
        this.cacheFailed = new String[paramParcel.readInt()];
        paramParcel.readStringArray(this.cacheFailed);
        this.videoError = new String[paramParcel.readInt()];
        paramParcel.readStringArray(this.videoError);
        return;
      }
      catch (ParseException localParseException)
      {
        while (true)
          localParseException.printStackTrace();
      }
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }

  VideoAd(String paramString)
  {
    if (paramString != null);
    try
    {
      localJSONObject1 = new JSONObject(paramString);
      if (localJSONObject1 != null)
      {
        JSONObject localJSONObject2 = localJSONObject1.optJSONObject("video");
        if (localJSONObject2 != null)
          deserializeFromObj(localJSONObject2);
      }
      return;
    }
    catch (JSONException localJSONException)
    {
      while (true)
      {
        localJSONException.printStackTrace();
        JSONObject localJSONObject1 = null;
      }
    }
  }

  private void deserializeFromObj(JSONObject paramJSONObject)
  {
    this.id = paramJSONObject.optString("id", null);
    this.acid = paramJSONObject.optString("vid", null);
    this.contentUrl = paramJSONObject.optString("content-url", null);
    JSONArray localJSONArray1 = paramJSONObject.optJSONArray("startActivity");
    if (localJSONArray1 != null)
    {
      this.startActivity = new String[localJSONArray1.length()];
      for (int i2 = 0; i2 < localJSONArray1.length(); i2++)
        this.startActivity[i2] = localJSONArray1.optString(i2);
    }
    this.startActivity = new String[0];
    JSONArray localJSONArray2 = paramJSONObject.optJSONArray("endActivity");
    if (localJSONArray2 != null)
    {
      this.endActivity = new String[localJSONArray2.length()];
      for (int i1 = 0; i1 < localJSONArray2.length(); i1++)
        this.endActivity[i1] = localJSONArray2.optString(i1);
    }
    this.endActivity = new String[0];
    JSONArray localJSONArray3 = paramJSONObject.optJSONArray("cacheComplete");
    if (localJSONArray3 != null)
    {
      this.cacheComplete = new String[localJSONArray3.length()];
      for (int n = 0; n < localJSONArray3.length(); n++)
        this.cacheComplete[n] = localJSONArray3.optString(n);
    }
    this.cacheComplete = new String[0];
    JSONArray localJSONArray4 = paramJSONObject.optJSONArray("cacheFailed");
    if (localJSONArray4 != null)
    {
      this.cacheFailed = new String[localJSONArray4.length()];
      for (int m = 0; m < localJSONArray4.length(); m++)
        this.cacheFailed[m] = localJSONArray4.optString(m);
    }
    this.cacheFailed = new String[0];
    JSONArray localJSONArray5 = paramJSONObject.optJSONArray("videoError");
    if (localJSONArray5 != null)
    {
      this.videoError = new String[localJSONArray5.length()];
      for (int k = 0; k < localJSONArray5.length(); k++)
        this.videoError[k] = localJSONArray5.optString(k);
    }
    this.videoError = new String[0];
    this.showControls = paramJSONObject.optBoolean("showVideoPlayerControls");
    this.showCountdown = paramJSONObject.optBoolean("showCountdownHUD");
    String str = paramJSONObject.optString("expiration", null);
    SimpleDateFormat localSimpleDateFormat;
    if (str != null)
      localSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ZZZZ");
    try
    {
      this.expiration = localSimpleDateFormat.parse(str);
      JSONObject localJSONObject1 = paramJSONObject.optJSONObject("onCompletion");
      if (localJSONObject1 != null)
      {
        this.onCompletionUrl = localJSONObject1.optString("url", null);
        this.stayInPlayer = localJSONObject1.optBoolean("stayInPlayer");
      }
      this.duration = (1000L * ()paramJSONObject.optDouble("duration", 0.0D));
      this.contentLength = paramJSONObject.optLong("contentLength");
      JSONArray localJSONArray6 = paramJSONObject.optJSONArray("buttons");
      if (localJSONArray6 != null)
        for (int j = 0; j < localJSONArray6.length(); j++)
        {
          JSONObject localJSONObject3 = localJSONArray6.optJSONObject(j);
          if (localJSONObject3 != null)
          {
            VideoImage localVideoImage = new VideoImage(localJSONObject3);
            this.buttons.add(localVideoImage);
          }
        }
    }
    catch (ParseException localParseException)
    {
      while (true)
        localParseException.printStackTrace();
      JSONArray localJSONArray7 = paramJSONObject.optJSONArray("log");
      int i = 0;
      if (localJSONArray7 != null)
        while (i < localJSONArray7.length())
        {
          JSONObject localJSONObject2 = localJSONArray7.optJSONObject(i);
          if (localJSONObject2 != null)
          {
            VideoLogEvent localVideoLogEvent = new VideoLogEvent(localJSONObject2);
            this.activities.add(localVideoLogEvent);
          }
          i++;
        }
      this.deferredViewStart = System.currentTimeMillis();
    }
  }

  public int describeContents()
  {
    return 0;
  }

  // ERROR //
  boolean download(Context paramContext)
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: invokestatic 244	com/millennialmedia/android/MMAdViewController:initCachedAdDirectory	(Lcom/millennialmedia/android/VideoAd;Landroid/content/Context;)Ljava/io/File;
    //   5: astore_2
    //   6: new 246	java/lang/StringBuilder
    //   9: dup
    //   10: ldc 248
    //   12: invokespecial 249	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   15: aload_2
    //   16: invokevirtual 253	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   19: invokevirtual 256	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   22: invokestatic 261	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
    //   25: aload_0
    //   26: getfield 67	com/millennialmedia/android/VideoAd:contentUrl	Ljava/lang/String;
    //   29: ldc_w 263
    //   32: aload_2
    //   33: invokestatic 267	com/millennialmedia/android/MMAdViewController:downloadComponent	(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Z
    //   36: istore_3
    //   37: iload_3
    //   38: ifeq +261 -> 299
    //   41: iload_3
    //   42: istore 8
    //   44: iconst_0
    //   45: istore 9
    //   47: iload 9
    //   49: aload_0
    //   50: getfield 45	com/millennialmedia/android/VideoAd:buttons	Ljava/util/ArrayList;
    //   53: invokevirtual 270	java/util/ArrayList:size	()I
    //   56: if_icmpge +236 -> 292
    //   59: aload_0
    //   60: getfield 45	com/millennialmedia/android/VideoAd:buttons	Ljava/util/ArrayList;
    //   63: iload 9
    //   65: invokevirtual 274	java/util/ArrayList:get	(I)Ljava/lang/Object;
    //   68: checkcast 120	com/millennialmedia/android/VideoImage
    //   71: astore 10
    //   73: aload 10
    //   75: getfield 277	com/millennialmedia/android/VideoImage:imageUrl	Ljava/lang/String;
    //   78: aload 10
    //   80: invokevirtual 280	com/millennialmedia/android/VideoImage:getImageName	()Ljava/lang/String;
    //   83: aload_2
    //   84: invokestatic 267	com/millennialmedia/android/MMAdViewController:downloadComponent	(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Z
    //   87: istore 11
    //   89: iload 11
    //   91: ifeq +13 -> 104
    //   94: iinc 9 1
    //   97: iload 11
    //   99: istore 8
    //   101: goto -54 -> 47
    //   104: iload 11
    //   106: istore 4
    //   108: iload 4
    //   110: ifne +42 -> 152
    //   113: aload_1
    //   114: aload_0
    //   115: getfield 61	com/millennialmedia/android/VideoAd:id	Ljava/lang/String;
    //   118: invokestatic 284	com/millennialmedia/android/MMAdViewController:deleteAd	(Landroid/content/Context;Ljava/lang/String;)V
    //   121: aload_0
    //   122: getfield 139	com/millennialmedia/android/VideoAd:cacheFailed	[Ljava/lang/String;
    //   125: invokestatic 288	com/millennialmedia/android/HttpGetRequest:log	([Ljava/lang/String;)V
    //   128: new 246	java/lang/StringBuilder
    //   131: dup
    //   132: ldc_w 290
    //   135: invokespecial 249	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   138: iload 4
    //   140: invokevirtual 293	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
    //   143: invokevirtual 256	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   146: invokestatic 261	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
    //   149: iload 4
    //   151: ireturn
    //   152: new 295	com/millennialmedia/android/AdDatabaseHelper
    //   155: dup
    //   156: aload_1
    //   157: invokespecial 298	com/millennialmedia/android/AdDatabaseHelper:<init>	(Landroid/content/Context;)V
    //   160: astore 5
    //   162: aload 5
    //   164: aload_0
    //   165: getfield 61	com/millennialmedia/android/VideoAd:id	Ljava/lang/String;
    //   168: invokevirtual 301	com/millennialmedia/android/AdDatabaseHelper:checkIfAdExists	(Ljava/lang/String;)Z
    //   171: ifeq +55 -> 226
    //   174: ldc_w 303
    //   177: invokestatic 306	com/millennialmedia/android/MMAdViewSDK$Log:d	(Ljava/lang/String;)V
    //   180: aload 5
    //   182: aload_0
    //   183: invokevirtual 310	com/millennialmedia/android/AdDatabaseHelper:updateAdData	(Lcom/millennialmedia/android/VideoAd;)V
    //   186: aload_0
    //   187: getfield 64	com/millennialmedia/android/VideoAd:acid	Ljava/lang/String;
    //   190: ifnull +21 -> 211
    //   193: aload_0
    //   194: getfield 64	com/millennialmedia/android/VideoAd:acid	Ljava/lang/String;
    //   197: invokevirtual 311	java/lang/String:length	()I
    //   200: ifle +11 -> 211
    //   203: aload_1
    //   204: aload_0
    //   205: getfield 64	com/millennialmedia/android/VideoAd:acid	Ljava/lang/String;
    //   208: invokestatic 314	com/millennialmedia/android/MMAdViewController:cachedVideoWasAdded	(Landroid/content/Context;Ljava/lang/String;)V
    //   211: aload 5
    //   213: invokevirtual 317	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   216: aload_0
    //   217: getfield 137	com/millennialmedia/android/VideoAd:cacheComplete	[Ljava/lang/String;
    //   220: invokestatic 288	com/millennialmedia/android/HttpGetRequest:log	([Ljava/lang/String;)V
    //   223: goto -95 -> 128
    //   226: ldc_w 319
    //   229: invokestatic 261	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
    //   232: aload 5
    //   234: aload_0
    //   235: invokevirtual 322	com/millennialmedia/android/AdDatabaseHelper:storeAd	(Lcom/millennialmedia/android/VideoAd;)V
    //   238: goto -52 -> 186
    //   241: astore 7
    //   243: aload 7
    //   245: invokevirtual 323	android/database/sqlite/SQLiteException:printStackTrace	()V
    //   248: aload 5
    //   250: ifnull -34 -> 216
    //   253: aload 5
    //   255: invokevirtual 317	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   258: goto -42 -> 216
    //   261: astore 6
    //   263: aconst_null
    //   264: astore 5
    //   266: aload 5
    //   268: ifnull +8 -> 276
    //   271: aload 5
    //   273: invokevirtual 317	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   276: aload 6
    //   278: athrow
    //   279: astore 6
    //   281: goto -15 -> 266
    //   284: astore 7
    //   286: aconst_null
    //   287: astore 5
    //   289: goto -46 -> 243
    //   292: iload 8
    //   294: istore 4
    //   296: goto -188 -> 108
    //   299: iload_3
    //   300: istore 4
    //   302: goto -194 -> 108
    //
    // Exception table:
    //   from	to	target	type
    //   162	186	241	android/database/sqlite/SQLiteException
    //   186	211	241	android/database/sqlite/SQLiteException
    //   226	238	241	android/database/sqlite/SQLiteException
    //   152	162	261	finally
    //   162	186	279	finally
    //   186	211	279	finally
    //   226	238	279	finally
    //   243	248	279	finally
    //   152	162	284	android/database/sqlite/SQLiteException
  }

  boolean isExpired()
  {
    return (this.expiration != null) && (this.expiration.getTime() <= System.currentTimeMillis());
  }

  boolean isOnDisk(Context paramContext)
  {
    File localFile1;
    if ((this.storedOnSdCard) && (Environment.getExternalStorageState().equals("mounted")))
    {
      localFile1 = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/.mmsyscache/" + this.id);
      if (!localFile1.exists())
        break label266;
      String[] arrayOfString = localFile1.list();
      if ((arrayOfString == null) || (arrayOfString.length < 1 + this.buttons.size()))
        break label266;
    }
    label266: for (boolean bool = true; ; bool = false)
    {
      if (bool)
      {
        if (this.contentLength > 0L)
        {
          File localFile2 = new File(localFile1, "video.dat");
          if ((!localFile2.exists()) || (localFile2.length() != this.contentLength))
          {
            return false;
            localFile1 = new File(paramContext.getCacheDir() + "/" + this.id);
            break;
          }
        }
        Iterator localIterator = this.buttons.iterator();
        while (localIterator.hasNext())
        {
          VideoImage localVideoImage = (VideoImage)localIterator.next();
          if (localVideoImage.contentLength > 0L)
          {
            File localFile3 = new File(localFile1, localVideoImage.getImageName());
            if ((!localFile3.exists()) || (localFile3.length() != localVideoImage.contentLength))
              return false;
          }
        }
      }
      return bool;
    }
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeString(this.id);
    paramParcel.writeString(this.acid);
    paramParcel.writeString(this.contentUrl);
    paramParcel.writeInt(this.startActivity.length);
    paramParcel.writeStringArray(this.startActivity);
    paramParcel.writeInt(this.endActivity.length);
    paramParcel.writeStringArray(this.endActivity);
    boolean[] arrayOfBoolean = new boolean[4];
    arrayOfBoolean[0] = this.showControls;
    arrayOfBoolean[1] = this.stayInPlayer;
    arrayOfBoolean[2] = this.showCountdown;
    arrayOfBoolean[3] = this.storedOnSdCard;
    paramParcel.writeBooleanArray(arrayOfBoolean);
    if (this.expiration != null)
      paramParcel.writeString(this.expiration.toString());
    paramParcel.writeString(this.onCompletionUrl);
    paramParcel.writeLong(this.duration);
    paramParcel.writeLong(this.contentLength);
    paramParcel.writeList(this.buttons);
    paramParcel.writeList(this.activities);
    paramParcel.writeLong(this.deferredViewStart);
    paramParcel.writeInt(this.cacheComplete.length);
    paramParcel.writeStringArray(this.cacheComplete);
    paramParcel.writeInt(this.cacheFailed.length);
    paramParcel.writeStringArray(this.cacheFailed);
    paramParcel.writeInt(this.videoError.length);
    paramParcel.writeStringArray(this.videoError);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.VideoAd
 * JD-Core Version:    0.6.2
 */