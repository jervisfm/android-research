package com.millennialmedia.android;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class VideoImage$1
  implements Parcelable.Creator<VideoImage>
{
  public final VideoImage createFromParcel(Parcel paramParcel)
  {
    return new VideoImage(paramParcel);
  }

  public final VideoImage[] newArray(int paramInt)
  {
    return new VideoImage[paramInt];
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.VideoImage.1
 * JD-Core Version:    0.6.2
 */