package com.millennialmedia.android;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class VideoLogEvent$1
  implements Parcelable.Creator<VideoLogEvent>
{
  public final VideoLogEvent createFromParcel(Parcel paramParcel)
  {
    return new VideoLogEvent(paramParcel);
  }

  public final VideoLogEvent[] newArray(int paramInt)
  {
    return new VideoLogEvent[paramInt];
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.VideoLogEvent.1
 * JD-Core Version:    0.6.2
 */