package com.millennialmedia.android;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.io.Serializable;
import org.json.JSONArray;
import org.json.JSONObject;

class VideoLogEvent
  implements Parcelable, Serializable
{
  public static final Parcelable.Creator<VideoLogEvent> CREATOR = new VideoLogEvent.1();
  String[] activities;
  long position;

  VideoLogEvent()
  {
  }

  VideoLogEvent(Parcel paramParcel)
  {
    try
    {
      this.position = paramParcel.readLong();
      this.activities = new String[paramParcel.readInt()];
      paramParcel.readStringArray(this.activities);
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }

  VideoLogEvent(JSONObject paramJSONObject)
  {
    deserializeFromObj(paramJSONObject);
  }

  private void deserializeFromObj(JSONObject paramJSONObject)
  {
    int i = 0;
    if (paramJSONObject == null);
    while (true)
    {
      return;
      this.position = (1000 * paramJSONObject.optInt("time"));
      JSONArray localJSONArray = paramJSONObject.optJSONArray("urls");
      if (localJSONArray == null)
        break;
      this.activities = new String[localJSONArray.length()];
      while (i < localJSONArray.length())
      {
        this.activities[i] = localJSONArray.optString(i);
        i++;
      }
    }
    this.activities = new String[0];
  }

  public int describeContents()
  {
    return 0;
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeLong(this.position);
    paramParcel.writeInt(this.activities.length);
    paramParcel.writeStringArray(this.activities);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.VideoLogEvent
 * JD-Core Version:    0.6.2
 */