package com.millennialmedia.android;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

class VideoPlayer$3
  implements View.OnClickListener
{
  VideoPlayer$3(VideoPlayer paramVideoPlayer)
  {
  }

  public void onClick(View paramView)
  {
    if (VideoPlayer.access$300(this.this$0))
    {
      if (VideoPlayer.access$200(this.this$0) != null)
        VideoPlayer.access$400(this.this$0, VideoPlayer.access$200(this.this$0).getCurrentPosition());
      VideoPlayer.access$500(this.this$0).setBackgroundResource(17301539);
      VideoPlayer.access$302(this.this$0, false);
    }
    while (VideoPlayer.access$200(this.this$0) == null)
      return;
    VideoPlayer.access$200(this.this$0).pause();
    VideoPlayer.access$500(this.this$0).setBackgroundResource(17301540);
    VideoPlayer.access$302(this.this$0, true);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.VideoPlayer.3
 * JD-Core Version:    0.6.2
 */