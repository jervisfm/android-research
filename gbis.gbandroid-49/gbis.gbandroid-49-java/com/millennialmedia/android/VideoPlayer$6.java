package com.millennialmedia.android;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;

class VideoPlayer$6
  implements MediaPlayer.OnCompletionListener
{
  VideoPlayer$6(VideoPlayer paramVideoPlayer)
  {
  }

  public void onCompletion(MediaPlayer paramMediaPlayer)
  {
    MMAdViewSDK.Log.d("Video Playing Complete");
    if (VideoPlayer.access$800(this.this$0))
      VideoPlayer.access$900(this.this$0);
    if (VideoPlayer.access$1000(this.this$0) != null)
      VideoPlayer.access$1100(this.this$0, VideoPlayer.access$1000(this.this$0).onCompletionUrl);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.VideoPlayer.6
 * JD-Core Version:    0.6.2
 */