package com.millennialmedia.android;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class VideoPlayer extends Activity
  implements Handler.Callback
{
  private static final int MESSAGE_DELAYED_BUTTON = 3;
  private static final int MESSAGE_EVENTLOG_CHECK = 2;
  private static final int MESSAGE_INACTIVITY_ANIMATION = 1;
  private RelativeLayout buttonsLayout;
  private RelativeLayout controlsLayout;
  private String current;
  private int currentVideoPosition = 0;
  private Handler handler;
  private TextView hudSeconds;
  private TextView hudStaticText;
  private boolean isCachedAd;
  private int lastVideoPosition;
  private EventLogSet logSet;
  private Button mPausePlay;
  private Button mRewind;
  private Button mStop;
  private MillennialMediaView mVideoView;
  private boolean paused = false;
  private ScreenReceiver receiver;
  private RelativeLayout relLayout;
  private boolean showBottomBar = true;
  private boolean showCountdownHud = true;
  private VideoAd videoAd;
  private boolean videoCompleted;
  private boolean videoCompletedOnce;
  protected VideoServer videoServer;
  private boolean waitForUserPresent;

  private boolean canFadeButtons()
  {
    return (!this.videoAd.stayInPlayer) || (!this.videoCompleted);
  }

  private void dismiss()
  {
    MMAdViewSDK.Log.d("Video ad player closed");
    if (this.mVideoView != null)
      this.mVideoView.stopPlayback();
    finish();
  }

  private void dispatchButtonClick(String paramString1, String paramString2)
  {
    MMAdViewSDK.Log.d("Button Clicked: " + paramString1);
    String str6;
    if (paramString1 != null)
    {
      if (!paramString1.startsWith("mmsdk"))
        break label542;
      str6 = paramString1.substring(8);
      if (str6 == null)
        break label383;
      if (!str6.equalsIgnoreCase("restartVideo"))
        break label332;
      if ((this.isCachedAd) && (this.videoAd != null))
      {
        ArrayList localArrayList = this.videoAd.buttons;
        if ((this.buttonsLayout != null) && (localArrayList != null))
        {
          this.handler.removeMessages(1);
          this.handler.removeMessages(2);
          this.handler.removeMessages(3);
          this.lastVideoPosition = 0;
          for (int i = 0; i < localArrayList.size(); i++)
          {
            MMAdViewSDK.Log.d("i: " + i);
            VideoImage localVideoImage = (VideoImage)localArrayList.get(i);
            if (localVideoImage != null)
            {
              if (localVideoImage.appearanceDelay > 0L)
              {
                ImageButton localImageButton = localVideoImage.button;
                this.buttonsLayout.removeView(localImageButton);
                Message localMessage2 = Message.obtain(this.handler, 3, localVideoImage);
                this.handler.sendMessageDelayed(localMessage2, localVideoImage.appearanceDelay);
              }
              if (localVideoImage.inactivityTimeout > 0L)
              {
                Message localMessage1 = Message.obtain(this.handler, 1, localVideoImage);
                this.handler.sendMessageDelayed(localMessage1, localVideoImage.inactivityTimeout + localVideoImage.appearanceDelay + localVideoImage.fadeDuration);
              }
              if (this.showCountdownHud)
                showHud(true);
              if (this.handler != null)
                this.handler.sendMessageDelayed(Message.obtain(this.handler, 2), 1000L);
            }
          }
        }
      }
      if (this.mVideoView == null)
        break label383;
      playVideo(0);
    }
    label542: 
    while (true)
    {
      return;
      label332: if (str6.equalsIgnoreCase("endVideo"))
      {
        MMAdViewSDK.Log.d("End");
        if (this.mVideoView != null)
        {
          this.current = null;
          this.mVideoView.stopPlayback();
          if (this.videoAd == null)
            continue;
          dismiss();
        }
      }
      else
      {
        MMAdViewSDK.Log.v("Unrecognized mmsdk:// URL");
        try
        {
          label383: String str1;
          do
          {
            do
              while (true)
              {
                NetworkingThread localNetworkingThread = new NetworkingThread();
                localNetworkingThread.urlString = paramString1;
                localNetworkingThread.setPriority(10);
                localNetworkingThread.start();
                localNetworkingThread.join();
                String str5 = localNetworkingThread.mimeTypeString;
                str2 = localNetworkingThread.locationString;
                str3 = str5;
                MMAdViewSDK.Log.v("locationString: " + str2);
                if (str2 == null)
                  break;
                localUri = Uri.parse(str2);
                if (str3 == null)
                  str3 = "";
                try
                {
                  if (!localUri.getScheme().equalsIgnoreCase("mmsdk"))
                    break label620;
                  if ((!localUri.getHost().equalsIgnoreCase("endVideo")) || (this.mVideoView == null))
                    break;
                  this.current = null;
                  this.mVideoView.stopPlayback();
                  dismiss();
                  return;
                }
                catch (ActivityNotFoundException localActivityNotFoundException2)
                {
                  Log.e("MillennialMediaSDK", localActivityNotFoundException2.getMessage());
                  return;
                }
              }
            while (!paramString1.startsWith("mmbrowser"));
            str1 = paramString1.substring(12);
          }
          while (str1 == null);
          MMAdViewSDK.Log.v("Launch browser");
          try
          {
            Intent localIntent1 = new Intent("android.intent.action.VIEW", Uri.parse(str1));
            localIntent1.setFlags(603979776);
            startActivity(localIntent1);
            return;
          }
          catch (ActivityNotFoundException localActivityNotFoundException1)
          {
            localActivityNotFoundException1.printStackTrace();
            return;
          }
        }
        catch (InterruptedException localInterruptedException)
        {
          String str3;
          Uri localUri;
          while (true)
          {
            String str2 = null;
            str3 = null;
          }
          label620: if (((localUri.getScheme().equalsIgnoreCase("http")) || (localUri.getScheme().equalsIgnoreCase("https"))) && (str3.equalsIgnoreCase("text/html")))
          {
            Intent localIntent2 = new Intent(this, MMAdViewOverlayActivity.class);
            localIntent2.setFlags(603979776);
            localIntent2.setData(localUri);
            localIntent2.putExtra("cachedAdView", true);
            localIntent2.putExtra("overlayOrientation", paramString2);
            startActivityForResult(localIntent2, 0);
            return;
          }
          if (localUri.getScheme().equalsIgnoreCase("market"))
          {
            MMAdViewSDK.Log.v("Android Market URL, launch the Market Application");
            Intent localIntent7 = new Intent("android.intent.action.VIEW", localUri);
            localIntent7.setFlags(603979776);
            startActivity(localIntent7);
            return;
          }
          if ((localUri.getScheme().equalsIgnoreCase("rtsp")) || ((localUri.getScheme().equalsIgnoreCase("http")) && ((str3.equalsIgnoreCase("video/mp4")) || (str3.equalsIgnoreCase("video/3gpp")))))
          {
            playVideo(0);
            return;
          }
          if (localUri.getScheme().equalsIgnoreCase("tel"))
          {
            MMAdViewSDK.Log.v("Telephone Number, launch the phone");
            Intent localIntent6 = new Intent("android.intent.action.DIAL", localUri);
            localIntent6.setFlags(603979776);
            startActivity(localIntent6);
            return;
          }
          if (localUri.getScheme().equalsIgnoreCase("http"))
          {
            Intent localIntent3 = new Intent(this, MMAdViewOverlayActivity.class);
            localIntent3.setFlags(603979776);
            localIntent3.setData(localUri);
            localIntent3.putExtra("cachedAdView", true);
            localIntent3.putExtra("overlayOrientation", paramString2);
            startActivityForResult(localIntent3, 0);
            return;
          }
          if (localUri.getScheme().equalsIgnoreCase("mmbrowser"))
          {
            String str4 = paramString1.substring(12);
            if (str4 != null)
            {
              MMAdViewSDK.Log.v("Launch browser");
              Intent localIntent5 = new Intent("android.intent.action.VIEW", Uri.parse(str4));
              localIntent5.setFlags(603979776);
              startActivity(localIntent5);
            }
          }
          else
          {
            MMAdViewSDK.Log.v("Uncertain about content, launch to browser");
            Intent localIntent4 = new Intent("android.intent.action.VIEW", localUri);
            localIntent4.setFlags(603979776);
            startActivity(localIntent4);
          }
        }
      }
    }
  }

  private void hideHud()
  {
    if (this.hudStaticText != null)
      this.hudStaticText.setVisibility(4);
    if (this.hudSeconds != null)
      this.hudSeconds.setVisibility(4);
  }

  private void pauseVideo()
  {
    if (this.mVideoView != null)
    {
      if (this.mVideoView.isPlaying())
      {
        this.mVideoView.pause();
        this.paused = true;
        MMAdViewSDK.Log.v("Video paused");
      }
      this.currentVideoPosition = this.mVideoView.getCurrentPosition();
    }
    if (this.isCachedAd)
    {
      this.handler.removeMessages(1);
      this.handler.removeMessages(2);
      this.handler.removeMessages(3);
      stopServer();
    }
  }

  private void playVideo(int paramInt)
  {
    try
    {
      str = getIntent().getData().toString();
      MMAdViewSDK.Log.d("playVideo path: " + str);
      if ((str == null) || (str.length() == 0))
      {
        Toast.makeText(this, "Sorry. There was a problem playing the video", 1).show();
        if (this.videoAd != null)
          HttpGetRequest.log(this.videoAd.videoError);
      }
      else
      {
        SharedPreferences.Editor localEditor2 = getSharedPreferences("MillennialMediaSettings", 0).edit();
        localEditor2.putBoolean("lastAdViewed", true);
        localEditor2.commit();
        this.videoCompleted = false;
        if ((str.equals(this.current)) && (this.mVideoView != null))
          if (this.isCachedAd)
          {
            if (this.videoAd == null)
              return;
            if (this.videoAd.storedOnSdCard)
            {
              this.mVideoView.setOnCompletionListener(new VideoPlayer.6(this));
              this.mVideoView.setOnPreparedListener(new VideoPlayer.7(this));
              this.mVideoView.setOnErrorListener(new VideoPlayer.8(this));
              this.mVideoView.setVideoURI(Uri.parse(str));
              this.mVideoView.requestFocus();
              this.mVideoView.seekTo(paramInt);
              this.mVideoView.start();
              this.paused = false;
              return;
            }
          }
      }
    }
    catch (Exception localException)
    {
      String str;
      Log.e("MillennialMediaSDK", "error: " + localException.getMessage(), localException);
      SharedPreferences.Editor localEditor1 = getSharedPreferences("MillennialMediaSettings", 0).edit();
      localEditor1.putBoolean("lastAdViewed", true);
      localEditor1.commit();
      Toast.makeText(this, "Sorry. There was a problem playing the video", 1).show();
      if (this.mVideoView != null)
        this.mVideoView.stopPlayback();
      if (this.videoAd != null)
      {
        HttpGetRequest.log(this.videoAd.videoError);
        return;
        startServer(str, paramInt, false);
        return;
        this.mVideoView.requestFocus();
        this.mVideoView.seekTo(paramInt);
        this.mVideoView.start();
        this.paused = false;
        return;
        this.current = str;
        if (this.mVideoView != null)
        {
          if (this.isCachedAd)
          {
            if (this.videoAd != null)
            {
              if (!this.videoAd.storedOnSdCard)
              {
                MMAdViewSDK.Log.d("Cached Ad. Starting Server");
                startServer(str, paramInt, false);
                return;
              }
              this.mVideoView.setOnCompletionListener(new VideoPlayer.9(this));
              this.mVideoView.setOnPreparedListener(new VideoPlayer.10(this));
              this.mVideoView.setOnErrorListener(new VideoPlayer.11(this));
              this.mVideoView.setVideoURI(Uri.parse(str));
              this.mVideoView.requestFocus();
              this.mVideoView.seekTo(paramInt);
              this.mVideoView.start();
              this.paused = false;
            }
          }
          else
          {
            this.mVideoView.setVideoURI(Uri.parse(str));
            this.mVideoView.requestFocus();
            this.mVideoView.seekTo(paramInt);
            this.mVideoView.start();
            this.paused = false;
          }
        }
        else
          Log.e("MillennialMediaSDK", "Video Player is Null");
      }
    }
  }

  private void resumeVideo()
  {
    int i;
    label122: VideoImage localVideoImage;
    long l1;
    if ((this.mVideoView != null) && (!this.mVideoView.isPlaying()) && (!this.videoCompleted))
      if ((this.isCachedAd) && (!this.handler.hasMessages(2)) && (this.videoAd != null))
      {
        this.handler.sendMessageDelayed(Message.obtain(this.handler, 2), 1000L);
        if (this.showCountdownHud)
        {
          long l2 = (this.videoAd.duration - this.currentVideoPosition) / 1000L;
          if (l2 <= 0L)
            break label270;
          if (this.hudSeconds != null)
            this.hudSeconds.setText(String.valueOf(l2));
        }
        i = 0;
        if (i < this.videoAd.buttons.size())
        {
          localVideoImage = (VideoImage)this.videoAd.buttons.get(i);
          if ((localVideoImage.appearanceDelay <= 0L) || (this.buttonsLayout.indexOfChild(localVideoImage.button) != -1))
            break label286;
          Message localMessage2 = Message.obtain(this.handler, 3, localVideoImage);
          l1 = localVideoImage.appearanceDelay - this.currentVideoPosition;
          if (l1 < 0L)
            l1 = 500L;
          this.handler.sendMessageDelayed(localMessage2, l1);
        }
      }
    while (true)
    {
      if (localVideoImage.inactivityTimeout > 0L)
      {
        Message localMessage1 = Message.obtain(this.handler, 1, localVideoImage);
        this.handler.sendMessageDelayed(localMessage1, l1 + localVideoImage.inactivityTimeout + localVideoImage.fadeDuration);
      }
      i++;
      break label122;
      label270: hideHud();
      break;
      playVideo(this.currentVideoPosition);
      return;
      label286: l1 = 0L;
    }
  }

  private void setButtonAlpha(ImageButton paramImageButton, float paramFloat)
  {
    AlphaAnimation localAlphaAnimation = new AlphaAnimation(paramFloat, paramFloat);
    localAlphaAnimation.setDuration(0L);
    localAlphaAnimation.setFillEnabled(true);
    localAlphaAnimation.setFillBefore(true);
    localAlphaAnimation.setFillAfter(true);
    paramImageButton.startAnimation(localAlphaAnimation);
  }

  private void showHud(boolean paramBoolean)
  {
    if ((this.hudStaticText == null) || (this.hudSeconds == null))
    {
      RelativeLayout.LayoutParams localLayoutParams1 = new RelativeLayout.LayoutParams(-2, -2);
      RelativeLayout.LayoutParams localLayoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
      this.hudStaticText = new TextView(this);
      this.hudStaticText.setText(" seconds remaining ...");
      this.hudStaticText.setTextColor(-1);
      this.hudStaticText.setPadding(0, 0, 5, 0);
      this.hudSeconds = new TextView(this);
      if (paramBoolean)
        if (this.videoAd != null)
          this.hudSeconds.setText(String.valueOf(this.videoAd.duration / 1000L));
      while (true)
      {
        this.hudSeconds.setTextColor(-1);
        this.hudSeconds.setId(401);
        this.hudStaticText.setId(402);
        localLayoutParams1.addRule(10);
        localLayoutParams1.addRule(11);
        this.buttonsLayout.addView(this.hudStaticText, localLayoutParams1);
        localLayoutParams2.addRule(10);
        localLayoutParams2.addRule(0, this.hudStaticText.getId());
        this.buttonsLayout.addView(this.hudSeconds, localLayoutParams2);
        return;
        if (this.currentVideoPosition != 0)
          this.hudSeconds.setText(String.valueOf(this.currentVideoPosition / 1000));
        else if (this.videoAd != null)
          this.hudSeconds.setText(String.valueOf(this.videoAd.duration / 1000L));
      }
    }
    if (paramBoolean)
    {
      if (this.videoAd == null)
        break label314;
      this.hudSeconds.setText(String.valueOf(this.videoAd.duration / 1000L));
    }
    while (true)
    {
      this.hudStaticText.setVisibility(0);
      this.hudSeconds.setVisibility(0);
      return;
      label314: this.hudSeconds.setText("");
    }
  }

  private void videoPlayerOnCompletion(String paramString)
  {
    this.videoCompletedOnce = true;
    this.videoCompleted = true;
    if (!this.mVideoView.isInErrorState())
      logEndEvent(this.logSet);
    stopServer();
    MMAdViewSDK.Log.v("Video player on complete");
    if (paramString != null)
      dispatchButtonClick(paramString, null);
    if (this.videoAd != null)
    {
      if (!this.videoAd.stayInPlayer)
        dismiss();
    }
    else
      return;
    if (this.videoAd.buttons != null)
      for (int i = 0; i < this.videoAd.buttons.size(); i++)
      {
        VideoImage localVideoImage = (VideoImage)this.videoAd.buttons.get(i);
        setButtonAlpha(localVideoImage.button, localVideoImage.fromAlpha);
        if (localVideoImage.button.getParent() == null)
          this.buttonsLayout.addView(localVideoImage.button, localVideoImage.layoutParams);
        for (int j = 0; j < this.videoAd.buttons.size(); j++)
          this.buttonsLayout.bringChildToFront(((VideoImage)this.videoAd.buttons.get(j)).button);
        MMAdViewSDK.Log.v("Button: " + i + " alpha: " + localVideoImage.fromAlpha);
      }
    this.handler.removeMessages(1);
    this.handler.removeMessages(2);
    this.handler.removeMessages(3);
  }

  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
  {
    if (this.videoAd != null)
    {
      if (this.handler != null)
        this.handler.removeMessages(1);
      int i = 0;
      if (i < this.videoAd.buttons.size())
      {
        VideoImage localVideoImage = (VideoImage)this.videoAd.buttons.get(i);
        setButtonAlpha(localVideoImage.button, localVideoImage.fromAlpha);
        if (localVideoImage.inactivityTimeout > 0L)
        {
          Message localMessage = Message.obtain(this.handler, 1, localVideoImage);
          this.handler.sendMessageDelayed(localMessage, localVideoImage.inactivityTimeout);
        }
        while (true)
        {
          i++;
          break;
          if (paramMotionEvent.getAction() == 1)
          {
            if (canFadeButtons())
            {
              AlphaAnimation localAlphaAnimation = new AlphaAnimation(localVideoImage.fromAlpha, localVideoImage.toAlpha);
              localAlphaAnimation.setDuration(localVideoImage.fadeDuration);
              localAlphaAnimation.setFillEnabled(true);
              localAlphaAnimation.setFillBefore(true);
              localAlphaAnimation.setFillAfter(true);
              localVideoImage.button.startAnimation(localAlphaAnimation);
            }
          }
          else if (paramMotionEvent.getAction() == 0)
            setButtonAlpha(localVideoImage.button, localVideoImage.fromAlpha);
        }
      }
    }
    return super.dispatchTouchEvent(paramMotionEvent);
  }

  public boolean handleMessage(Message paramMessage)
  {
    switch (paramMessage.what)
    {
    default:
    case 1:
      do
        return true;
      while (!canFadeButtons());
      VideoImage localVideoImage2 = (VideoImage)paramMessage.obj;
      AlphaAnimation localAlphaAnimation2 = new AlphaAnimation(localVideoImage2.fromAlpha, localVideoImage2.toAlpha);
      localAlphaAnimation2.setDuration(localVideoImage2.fadeDuration);
      localAlphaAnimation2.setFillEnabled(true);
      localAlphaAnimation2.setFillBefore(true);
      localAlphaAnimation2.setFillAfter(true);
      localVideoImage2.button.startAnimation(localAlphaAnimation2);
      return true;
    case 3:
      VideoImage localVideoImage1 = (VideoImage)paramMessage.obj;
      try
      {
        if (this.buttonsLayout.indexOfChild(localVideoImage1.button) == -1)
          this.buttonsLayout.addView(localVideoImage1.button, localVideoImage1.layoutParams);
        AlphaAnimation localAlphaAnimation1 = new AlphaAnimation(localVideoImage1.toAlpha, localVideoImage1.fromAlpha);
        localAlphaAnimation1.setDuration(localVideoImage1.fadeDuration);
        localAlphaAnimation1.setAnimationListener(new DelayedAnimationListener(localVideoImage1.button, localVideoImage1.layoutParams));
        localAlphaAnimation1.setFillEnabled(true);
        localAlphaAnimation1.setFillBefore(true);
        localAlphaAnimation1.setFillAfter(true);
        MMAdViewSDK.Log.v("Beginning animation to visibility. Fade duration: " + localVideoImage1.fadeDuration + " Button: " + localVideoImage1.name + " Time: " + System.currentTimeMillis());
        localVideoImage1.button.startAnimation(localAlphaAnimation1);
        return true;
      }
      catch (IllegalStateException localIllegalStateException2)
      {
        while (true)
          localIllegalStateException2.printStackTrace();
      }
    case 2:
    }
    while (true)
    {
      int i;
      try
      {
        if (!this.mVideoView.isPlaying())
          break label524;
        i = this.mVideoView.getCurrentPosition();
        if (i <= this.lastVideoPosition)
          break label474;
        if (this.videoAd == null)
          break label468;
        if (this.lastVideoPosition != 0)
          break label552;
        logBeginEvent(this.logSet);
        break label552;
        if (j >= this.videoAd.activities.size())
          break label468;
        VideoLogEvent localVideoLogEvent = (VideoLogEvent)this.videoAd.activities.get(j);
        if ((localVideoLogEvent != null) && (localVideoLogEvent.position >= this.lastVideoPosition) && (localVideoLogEvent.position < i))
        {
          int k = 0;
          int m = localVideoLogEvent.activities.length;
          if (k < m)
            try
            {
              logEvent(localVideoLogEvent.activities[k]);
              k++;
            }
            catch (UnsupportedEncodingException localUnsupportedEncodingException)
            {
              localUnsupportedEncodingException.printStackTrace();
              continue;
            }
        }
      }
      catch (IllegalStateException localIllegalStateException1)
      {
        localIllegalStateException1.printStackTrace();
        return true;
      }
      j++;
      continue;
      label468: this.lastVideoPosition = i;
      label474: if (this.showCountdownHud)
      {
        long l = (this.videoAd.duration - i) / 1000L;
        if (l <= 0L)
          break label545;
        if (this.hudSeconds != null)
          this.hudSeconds.setText(String.valueOf(l));
      }
      while (true)
      {
        label524: this.handler.sendMessageDelayed(Message.obtain(this.handler, 2), 1000L);
        return true;
        label545: hideHud();
      }
      label552: int j = 0;
    }
  }

  protected void logBeginEvent(EventLogSet paramEventLogSet)
  {
    if ((paramEventLogSet != null) && (paramEventLogSet.startActivity != null))
      try
      {
        MMAdViewSDK.Log.d("Cached video begin event logged");
        for (int i = 0; i < paramEventLogSet.startActivity.length; i++)
          logEvent(paramEventLogSet.startActivity[i]);
      }
      catch (UnsupportedEncodingException localUnsupportedEncodingException)
      {
        localUnsupportedEncodingException.printStackTrace();
      }
  }

  protected void logButtonEvent(VideoImage paramVideoImage)
  {
    MMAdViewSDK.Log.d("Cached video button event logged");
    int i = 0;
    try
    {
      while (i < paramVideoImage.activity.length)
      {
        logEvent(paramVideoImage.activity[i]);
        i++;
      }
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      localUnsupportedEncodingException.printStackTrace();
    }
  }

  protected void logEndEvent(EventLogSet paramEventLogSet)
  {
    MMAdViewSDK.Log.d("Cached video end event logged");
    int i = 0;
    try
    {
      while (i < paramEventLogSet.endActivity.length)
      {
        logEvent(paramEventLogSet.endActivity[i]);
        i++;
      }
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      localUnsupportedEncodingException.printStackTrace();
    }
  }

  protected void logEvent(String paramString)
  {
    MMAdViewSDK.Log.d("Logging event to: " + paramString);
    new Thread(new VideoPlayer.5(this, paramString)).start();
  }

  // ERROR //
  public void onCreate(Bundle paramBundle)
  {
    // Byte code:
    //   0: aload_0
    //   1: ldc_w 751
    //   4: invokevirtual 754	com/millennialmedia/android/VideoPlayer:setTheme	(I)V
    //   7: aload_0
    //   8: aload_1
    //   9: invokespecial 756	android/app/Activity:onCreate	(Landroid/os/Bundle;)V
    //   12: ldc_w 758
    //   15: invokestatic 131	com/millennialmedia/android/MMAdViewSDK$Log:d	(Ljava/lang/String;)V
    //   18: aload_1
    //   19: ifnull +1106 -> 1125
    //   22: aload_0
    //   23: aload_1
    //   24: ldc_w 759
    //   27: invokevirtual 764	android/os/Bundle:getBoolean	(Ljava/lang/String;)Z
    //   30: putfield 176	com/millennialmedia/android/VideoPlayer:isCachedAd	Z
    //   33: aload_0
    //   34: aload_1
    //   35: ldc_w 765
    //   38: invokevirtual 764	android/os/Bundle:getBoolean	(Ljava/lang/String;)Z
    //   41: putfield 124	com/millennialmedia/android/VideoPlayer:videoCompleted	Z
    //   44: aload_0
    //   45: aload_1
    //   46: ldc_w 766
    //   49: invokevirtual 764	android/os/Bundle:getBoolean	(Ljava/lang/String;)Z
    //   52: putfield 609	com/millennialmedia/android/VideoPlayer:videoCompletedOnce	Z
    //   55: aload_0
    //   56: aload_1
    //   57: ldc_w 768
    //   60: invokevirtual 772	android/os/Bundle:getInt	(Ljava/lang/String;)I
    //   63: putfield 58	com/millennialmedia/android/VideoPlayer:currentVideoPosition	I
    //   66: aload_0
    //   67: aload_1
    //   68: ldc_w 773
    //   71: invokevirtual 772	android/os/Bundle:getInt	(Ljava/lang/String;)I
    //   74: putfield 191	com/millennialmedia/android/VideoPlayer:lastVideoPosition	I
    //   77: aload_0
    //   78: new 775	com/millennialmedia/android/VideoPlayer$ScreenReceiver
    //   81: dup
    //   82: aload_0
    //   83: aconst_null
    //   84: invokespecial 778	com/millennialmedia/android/VideoPlayer$ScreenReceiver:<init>	(Lcom/millennialmedia/android/VideoPlayer;Lcom/millennialmedia/android/VideoPlayer$1;)V
    //   87: putfield 780	com/millennialmedia/android/VideoPlayer:receiver	Lcom/millennialmedia/android/VideoPlayer$ScreenReceiver;
    //   90: new 782	android/content/IntentFilter
    //   93: dup
    //   94: invokespecial 783	android/content/IntentFilter:<init>	()V
    //   97: astore_2
    //   98: aload_2
    //   99: ldc_w 785
    //   102: invokevirtual 788	android/content/IntentFilter:addAction	(Ljava/lang/String;)V
    //   105: aload_2
    //   106: ldc_w 790
    //   109: invokevirtual 788	android/content/IntentFilter:addAction	(Ljava/lang/String;)V
    //   112: aload_2
    //   113: ldc_w 792
    //   116: invokevirtual 788	android/content/IntentFilter:addAction	(Ljava/lang/String;)V
    //   119: aload_0
    //   120: aload_0
    //   121: getfield 780	com/millennialmedia/android/VideoPlayer:receiver	Lcom/millennialmedia/android/VideoPlayer$ScreenReceiver;
    //   124: aload_2
    //   125: invokevirtual 796	com/millennialmedia/android/VideoPlayer:registerReceiver	(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    //   128: pop
    //   129: aload_0
    //   130: new 218	android/widget/RelativeLayout
    //   133: dup
    //   134: aload_0
    //   135: invokespecial 797	android/widget/RelativeLayout:<init>	(Landroid/content/Context;)V
    //   138: putfield 799	com/millennialmedia/android/VideoPlayer:relLayout	Landroid/widget/RelativeLayout;
    //   141: aload_0
    //   142: getfield 799	com/millennialmedia/android/VideoPlayer:relLayout	Landroid/widget/RelativeLayout;
    //   145: sipush 400
    //   148: invokevirtual 800	android/widget/RelativeLayout:setId	(I)V
    //   151: aload_0
    //   152: getfield 799	com/millennialmedia/android/VideoPlayer:relLayout	Landroid/widget/RelativeLayout;
    //   155: new 802	android/view/ViewGroup$LayoutParams
    //   158: dup
    //   159: iconst_m1
    //   160: iconst_m1
    //   161: invokespecial 803	android/view/ViewGroup$LayoutParams:<init>	(II)V
    //   164: invokevirtual 807	android/widget/RelativeLayout:setLayoutParams	(Landroid/view/ViewGroup$LayoutParams;)V
    //   167: aload_0
    //   168: getfield 799	com/millennialmedia/android/VideoPlayer:relLayout	Landroid/widget/RelativeLayout;
    //   171: ldc_w 808
    //   174: invokevirtual 811	android/widget/RelativeLayout:setBackgroundColor	(I)V
    //   177: aload_0
    //   178: getfield 799	com/millennialmedia/android/VideoPlayer:relLayout	Landroid/widget/RelativeLayout;
    //   181: ldc_w 808
    //   184: invokevirtual 814	android/widget/RelativeLayout:setDrawingCacheBackgroundColor	(I)V
    //   187: new 218	android/widget/RelativeLayout
    //   190: dup
    //   191: aload_0
    //   192: invokespecial 797	android/widget/RelativeLayout:<init>	(Landroid/content/Context;)V
    //   195: astore 4
    //   197: aload 4
    //   199: ldc_w 808
    //   202: invokevirtual 811	android/widget/RelativeLayout:setBackgroundColor	(I)V
    //   205: aload 4
    //   207: sipush 410
    //   210: invokevirtual 800	android/widget/RelativeLayout:setId	(I)V
    //   213: new 575	android/widget/RelativeLayout$LayoutParams
    //   216: dup
    //   217: iconst_m1
    //   218: iconst_m1
    //   219: invokespecial 578	android/widget/RelativeLayout$LayoutParams:<init>	(II)V
    //   222: astore 5
    //   224: aload 5
    //   226: bipush 13
    //   228: invokevirtual 596	android/widget/RelativeLayout$LayoutParams:addRule	(I)V
    //   231: aload 4
    //   233: aload 5
    //   235: invokevirtual 807	android/widget/RelativeLayout:setLayoutParams	(Landroid/view/ViewGroup$LayoutParams;)V
    //   238: aload 4
    //   240: ldc_w 808
    //   243: invokevirtual 814	android/widget/RelativeLayout:setDrawingCacheBackgroundColor	(I)V
    //   246: new 575	android/widget/RelativeLayout$LayoutParams
    //   249: dup
    //   250: iconst_m1
    //   251: iconst_m1
    //   252: invokespecial 578	android/widget/RelativeLayout$LayoutParams:<init>	(II)V
    //   255: astore 6
    //   257: aload 6
    //   259: bipush 13
    //   261: invokevirtual 596	android/widget/RelativeLayout$LayoutParams:addRule	(I)V
    //   264: aload_0
    //   265: new 133	com/millennialmedia/android/MillennialMediaView
    //   268: dup
    //   269: aload_0
    //   270: invokespecial 815	com/millennialmedia/android/MillennialMediaView:<init>	(Landroid/content/Context;)V
    //   273: putfield 89	com/millennialmedia/android/VideoPlayer:mVideoView	Lcom/millennialmedia/android/MillennialMediaView;
    //   276: aload_0
    //   277: getfield 89	com/millennialmedia/android/VideoPlayer:mVideoView	Lcom/millennialmedia/android/MillennialMediaView;
    //   280: sipush 411
    //   283: invokevirtual 816	com/millennialmedia/android/MillennialMediaView:setId	(I)V
    //   286: aload_0
    //   287: getfield 89	com/millennialmedia/android/VideoPlayer:mVideoView	Lcom/millennialmedia/android/MillennialMediaView;
    //   290: invokevirtual 820	com/millennialmedia/android/MillennialMediaView:getHolder	()Landroid/view/SurfaceHolder;
    //   293: bipush 254
    //   295: invokeinterface 825 2 0
    //   300: aload 4
    //   302: aload_0
    //   303: getfield 89	com/millennialmedia/android/VideoPlayer:mVideoView	Lcom/millennialmedia/android/MillennialMediaView;
    //   306: aload 6
    //   308: invokevirtual 600	android/widget/RelativeLayout:addView	(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    //   311: aload_0
    //   312: getfield 89	com/millennialmedia/android/VideoPlayer:mVideoView	Lcom/millennialmedia/android/MillennialMediaView;
    //   315: ldc_w 808
    //   318: invokevirtual 826	com/millennialmedia/android/MillennialMediaView:setDrawingCacheBackgroundColor	(I)V
    //   321: aload_0
    //   322: getfield 799	com/millennialmedia/android/VideoPlayer:relLayout	Landroid/widget/RelativeLayout;
    //   325: aload 4
    //   327: aload 5
    //   329: invokevirtual 600	android/widget/RelativeLayout:addView	(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    //   332: new 575	android/widget/RelativeLayout$LayoutParams
    //   335: dup
    //   336: iconst_m1
    //   337: iconst_m1
    //   338: invokespecial 578	android/widget/RelativeLayout$LayoutParams:<init>	(II)V
    //   341: astore 7
    //   343: new 145	java/lang/StringBuilder
    //   346: dup
    //   347: ldc_w 828
    //   350: invokespecial 149	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   353: aload_0
    //   354: getfield 176	com/millennialmedia/android/VideoPlayer:isCachedAd	Z
    //   357: invokevirtual 831	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
    //   360: invokevirtual 157	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   363: invokestatic 256	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
    //   366: aload_0
    //   367: getfield 176	com/millennialmedia/android/VideoPlayer:isCachedAd	Z
    //   370: ifeq +918 -> 1288
    //   373: aload_0
    //   374: new 186	android/os/Handler
    //   377: dup
    //   378: aload_0
    //   379: invokespecial 834	android/os/Handler:<init>	(Landroid/os/Handler$Callback;)V
    //   382: putfield 184	com/millennialmedia/android/VideoPlayer:handler	Landroid/os/Handler;
    //   385: aload_0
    //   386: iconst_0
    //   387: invokevirtual 837	com/millennialmedia/android/VideoPlayer:setRequestedOrientation	(I)V
    //   390: aload_1
    //   391: ifnonnull +808 -> 1199
    //   394: aload_0
    //   395: invokevirtual 406	com/millennialmedia/android/VideoPlayer:getIntent	()Landroid/content/Intent;
    //   398: ldc_w 839
    //   401: invokevirtual 843	android/content/Intent:getStringExtra	(Ljava/lang/String;)Ljava/lang/String;
    //   404: astore 22
    //   406: new 845	com/millennialmedia/android/AdDatabaseHelper
    //   409: dup
    //   410: aload_0
    //   411: invokespecial 846	com/millennialmedia/android/AdDatabaseHelper:<init>	(Landroid/content/Context;)V
    //   414: astore 23
    //   416: aload_0
    //   417: aload 23
    //   419: aload 22
    //   421: invokevirtual 850	com/millennialmedia/android/AdDatabaseHelper:getVideoAd	(Ljava/lang/String;)Lcom/millennialmedia/android/VideoAd;
    //   424: putfield 70	com/millennialmedia/android/VideoPlayer:videoAd	Lcom/millennialmedia/android/VideoAd;
    //   427: aload 23
    //   429: invokevirtual 853	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   432: aload_0
    //   433: new 718	com/millennialmedia/android/EventLogSet
    //   436: dup
    //   437: aload_0
    //   438: getfield 70	com/millennialmedia/android/VideoPlayer:videoAd	Lcom/millennialmedia/android/VideoAd;
    //   441: invokespecial 856	com/millennialmedia/android/EventLogSet:<init>	(Lcom/millennialmedia/android/VideoAd;)V
    //   444: putfield 614	com/millennialmedia/android/VideoPlayer:logSet	Lcom/millennialmedia/android/EventLogSet;
    //   447: aload_0
    //   448: getfield 70	com/millennialmedia/android/VideoPlayer:videoAd	Lcom/millennialmedia/android/VideoAd;
    //   451: ifnull +25 -> 476
    //   454: aload_0
    //   455: aload_0
    //   456: getfield 70	com/millennialmedia/android/VideoPlayer:videoAd	Lcom/millennialmedia/android/VideoAd;
    //   459: getfield 859	com/millennialmedia/android/VideoAd:showControls	Z
    //   462: putfield 56	com/millennialmedia/android/VideoPlayer:showBottomBar	Z
    //   465: aload_0
    //   466: aload_0
    //   467: getfield 70	com/millennialmedia/android/VideoPlayer:videoAd	Lcom/millennialmedia/android/VideoAd;
    //   470: getfield 862	com/millennialmedia/android/VideoAd:showCountdown	Z
    //   473: putfield 60	com/millennialmedia/android/VideoPlayer:showCountdownHud	Z
    //   476: aload_0
    //   477: new 218	android/widget/RelativeLayout
    //   480: dup
    //   481: aload_0
    //   482: invokespecial 797	android/widget/RelativeLayout:<init>	(Landroid/content/Context;)V
    //   485: putfield 182	com/millennialmedia/android/VideoPlayer:buttonsLayout	Landroid/widget/RelativeLayout;
    //   488: aload_0
    //   489: getfield 182	com/millennialmedia/android/VideoPlayer:buttonsLayout	Landroid/widget/RelativeLayout;
    //   492: sipush 420
    //   495: invokevirtual 800	android/widget/RelativeLayout:setId	(I)V
    //   498: aload_0
    //   499: getfield 60	com/millennialmedia/android/VideoPlayer:showCountdownHud	Z
    //   502: ifeq +8 -> 510
    //   505: aload_0
    //   506: iconst_0
    //   507: invokespecial 242	com/millennialmedia/android/VideoPlayer:showHud	(Z)V
    //   510: aload_0
    //   511: getfield 70	com/millennialmedia/android/VideoPlayer:videoAd	Lcom/millennialmedia/android/VideoAd;
    //   514: ifnull +1109 -> 1623
    //   517: aload_0
    //   518: getfield 70	com/millennialmedia/android/VideoPlayer:videoAd	Lcom/millennialmedia/android/VideoAd;
    //   521: getfield 180	com/millennialmedia/android/VideoAd:buttons	Ljava/util/ArrayList;
    //   524: astore 12
    //   526: aload 12
    //   528: ifnull +760 -> 1288
    //   531: iconst_0
    //   532: istore 13
    //   534: iload 13
    //   536: aload 12
    //   538: invokeinterface 197 1 0
    //   543: if_icmpge +732 -> 1275
    //   546: aload_0
    //   547: getfield 70	com/millennialmedia/android/VideoPlayer:videoAd	Lcom/millennialmedia/android/VideoAd;
    //   550: getfield 467	com/millennialmedia/android/VideoAd:storedOnSdCard	Z
    //   553: ifeq +699 -> 1252
    //   556: new 864	java/io/File
    //   559: dup
    //   560: new 145	java/lang/StringBuilder
    //   563: dup
    //   564: invokespecial 865	java/lang/StringBuilder:<init>	()V
    //   567: invokestatic 871	android/os/Environment:getExternalStorageDirectory	()Ljava/io/File;
    //   570: invokevirtual 874	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   573: invokevirtual 153	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   576: ldc_w 876
    //   579: invokevirtual 153	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   582: invokevirtual 157	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   585: invokespecial 877	java/io/File:<init>	(Ljava/lang/String;)V
    //   588: astore 14
    //   590: new 569	android/widget/ImageButton
    //   593: dup
    //   594: aload_0
    //   595: invokespecial 878	android/widget/ImageButton:<init>	(Landroid/content/Context;)V
    //   598: astore 15
    //   600: aload 12
    //   602: iload 13
    //   604: invokeinterface 206 2 0
    //   609: checkcast 208	com/millennialmedia/android/VideoImage
    //   612: aload 15
    //   614: putfield 216	com/millennialmedia/android/VideoImage:button	Landroid/widget/ImageButton;
    //   617: aload 15
    //   619: new 145	java/lang/StringBuilder
    //   622: dup
    //   623: invokespecial 865	java/lang/StringBuilder:<init>	()V
    //   626: aload 14
    //   628: invokevirtual 881	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   631: ldc_w 883
    //   634: invokevirtual 153	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   637: aload_0
    //   638: getfield 70	com/millennialmedia/android/VideoPlayer:videoAd	Lcom/millennialmedia/android/VideoAd;
    //   641: getfield 886	com/millennialmedia/android/VideoAd:id	Ljava/lang/String;
    //   644: invokevirtual 153	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   647: ldc_w 883
    //   650: invokevirtual 153	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   653: aload 12
    //   655: iload 13
    //   657: invokeinterface 206 2 0
    //   662: checkcast 208	com/millennialmedia/android/VideoImage
    //   665: getfield 889	com/millennialmedia/android/VideoImage:imageUrl	Ljava/lang/String;
    //   668: invokestatic 286	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
    //   671: invokevirtual 892	android/net/Uri:getLastPathSegment	()Ljava/lang/String;
    //   674: ldc_w 894
    //   677: ldc_w 896
    //   680: invokevirtual 900	java/lang/String:replaceFirst	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   683: invokevirtual 153	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   686: invokevirtual 157	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   689: invokestatic 286	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
    //   692: invokevirtual 903	android/widget/ImageButton:setImageURI	(Landroid/net/Uri;)V
    //   695: aload 15
    //   697: iconst_0
    //   698: iconst_0
    //   699: iconst_0
    //   700: iconst_0
    //   701: invokevirtual 904	android/widget/ImageButton:setPadding	(IIII)V
    //   704: aload 15
    //   706: iconst_0
    //   707: invokevirtual 905	android/widget/ImageButton:setBackgroundColor	(I)V
    //   710: aload_0
    //   711: aload 15
    //   713: aload 12
    //   715: iload 13
    //   717: invokeinterface 206 2 0
    //   722: checkcast 208	com/millennialmedia/android/VideoImage
    //   725: getfield 624	com/millennialmedia/android/VideoImage:fromAlpha	F
    //   728: invokespecial 626	com/millennialmedia/android/VideoPlayer:setButtonAlpha	(Landroid/widget/ImageButton;F)V
    //   731: aload 15
    //   733: iload 13
    //   735: iconst_1
    //   736: iadd
    //   737: invokevirtual 906	android/widget/ImageButton:setId	(I)V
    //   740: new 575	android/widget/RelativeLayout$LayoutParams
    //   743: dup
    //   744: bipush 254
    //   746: bipush 254
    //   748: invokespecial 578	android/widget/RelativeLayout$LayoutParams:<init>	(II)V
    //   751: astore 16
    //   753: new 145	java/lang/StringBuilder
    //   756: dup
    //   757: ldc_w 908
    //   760: invokespecial 149	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   763: iconst_m1
    //   764: aload 15
    //   766: invokevirtual 909	android/widget/ImageButton:getId	()I
    //   769: iadd
    //   770: invokevirtual 202	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   773: ldc_w 911
    //   776: invokevirtual 153	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   779: aload 15
    //   781: invokevirtual 909	android/widget/ImageButton:getId	()I
    //   784: invokevirtual 202	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   787: ldc_w 913
    //   790: invokevirtual 153	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   793: aload 12
    //   795: iload 13
    //   797: invokeinterface 206 2 0
    //   802: checkcast 208	com/millennialmedia/android/VideoImage
    //   805: getfield 916	com/millennialmedia/android/VideoImage:anchor	I
    //   808: invokevirtual 202	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   811: ldc_w 918
    //   814: invokevirtual 153	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   817: aload 12
    //   819: iload 13
    //   821: invokeinterface 206 2 0
    //   826: checkcast 208	com/millennialmedia/android/VideoImage
    //   829: getfield 920	com/millennialmedia/android/VideoImage:position	I
    //   832: invokevirtual 202	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   835: invokevirtual 157	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   838: invokestatic 256	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
    //   841: aload 16
    //   843: aload 12
    //   845: iload 13
    //   847: invokeinterface 206 2 0
    //   852: checkcast 208	com/millennialmedia/android/VideoImage
    //   855: getfield 920	com/millennialmedia/android/VideoImage:position	I
    //   858: aload 12
    //   860: iload 13
    //   862: invokeinterface 206 2 0
    //   867: checkcast 208	com/millennialmedia/android/VideoImage
    //   870: getfield 916	com/millennialmedia/android/VideoImage:anchor	I
    //   873: invokevirtual 605	android/widget/RelativeLayout$LayoutParams:addRule	(II)V
    //   876: aload 16
    //   878: aload 12
    //   880: iload 13
    //   882: invokeinterface 206 2 0
    //   887: checkcast 208	com/millennialmedia/android/VideoImage
    //   890: getfield 923	com/millennialmedia/android/VideoImage:position2	I
    //   893: aload 12
    //   895: iload 13
    //   897: invokeinterface 206 2 0
    //   902: checkcast 208	com/millennialmedia/android/VideoImage
    //   905: getfield 926	com/millennialmedia/android/VideoImage:anchor2	I
    //   908: invokevirtual 605	android/widget/RelativeLayout$LayoutParams:addRule	(II)V
    //   911: aload 16
    //   913: aload 12
    //   915: iload 13
    //   917: invokeinterface 206 2 0
    //   922: checkcast 208	com/millennialmedia/android/VideoImage
    //   925: getfield 929	com/millennialmedia/android/VideoImage:paddingLeft	I
    //   928: aload 12
    //   930: iload 13
    //   932: invokeinterface 206 2 0
    //   937: checkcast 208	com/millennialmedia/android/VideoImage
    //   940: getfield 932	com/millennialmedia/android/VideoImage:paddingTop	I
    //   943: aload 12
    //   945: iload 13
    //   947: invokeinterface 206 2 0
    //   952: checkcast 208	com/millennialmedia/android/VideoImage
    //   955: getfield 935	com/millennialmedia/android/VideoImage:paddingRight	I
    //   958: aload 12
    //   960: iload 13
    //   962: invokeinterface 206 2 0
    //   967: checkcast 208	com/millennialmedia/android/VideoImage
    //   970: getfield 938	com/millennialmedia/android/VideoImage:paddingBottom	I
    //   973: invokevirtual 941	android/widget/RelativeLayout$LayoutParams:setMargins	(IIII)V
    //   976: aload 12
    //   978: iload 13
    //   980: invokeinterface 206 2 0
    //   985: checkcast 208	com/millennialmedia/android/VideoImage
    //   988: astore 17
    //   990: aload 17
    //   992: getfield 944	com/millennialmedia/android/VideoImage:linkUrl	Ljava/lang/String;
    //   995: invokestatic 950	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   998: ifne +18 -> 1016
    //   1001: aload 15
    //   1003: new 952	com/millennialmedia/android/VideoPlayer$1
    //   1006: dup
    //   1007: aload_0
    //   1008: aload 17
    //   1010: invokespecial 955	com/millennialmedia/android/VideoPlayer$1:<init>	(Lcom/millennialmedia/android/VideoPlayer;Lcom/millennialmedia/android/VideoImage;)V
    //   1013: invokevirtual 959	android/widget/ImageButton:setOnClickListener	(Landroid/view/View$OnClickListener;)V
    //   1016: aload 17
    //   1018: getfield 212	com/millennialmedia/android/VideoImage:appearanceDelay	J
    //   1021: lconst_0
    //   1022: lcmp
    //   1023: ifle +238 -> 1261
    //   1026: aload 12
    //   1028: iload 13
    //   1030: invokeinterface 206 2 0
    //   1035: checkcast 208	com/millennialmedia/android/VideoImage
    //   1038: aload 16
    //   1040: putfield 634	com/millennialmedia/android/VideoImage:layoutParams	Landroid/widget/RelativeLayout$LayoutParams;
    //   1043: aload_0
    //   1044: getfield 184	com/millennialmedia/android/VideoPlayer:handler	Landroid/os/Handler;
    //   1047: iconst_3
    //   1048: aload 17
    //   1050: invokestatic 228	android/os/Message:obtain	(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;
    //   1053: astore 20
    //   1055: aload_0
    //   1056: getfield 184	com/millennialmedia/android/VideoPlayer:handler	Landroid/os/Handler;
    //   1059: aload 20
    //   1061: aload 17
    //   1063: getfield 212	com/millennialmedia/android/VideoImage:appearanceDelay	J
    //   1066: invokevirtual 232	android/os/Handler:sendMessageDelayed	(Landroid/os/Message;J)Z
    //   1069: pop
    //   1070: aload 17
    //   1072: getfield 235	com/millennialmedia/android/VideoImage:inactivityTimeout	J
    //   1075: lconst_0
    //   1076: lcmp
    //   1077: ifle +42 -> 1119
    //   1080: aload_0
    //   1081: getfield 184	com/millennialmedia/android/VideoPlayer:handler	Landroid/os/Handler;
    //   1084: iconst_1
    //   1085: aload 17
    //   1087: invokestatic 228	android/os/Message:obtain	(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;
    //   1090: astore 18
    //   1092: aload_0
    //   1093: getfield 184	com/millennialmedia/android/VideoPlayer:handler	Landroid/os/Handler;
    //   1096: aload 18
    //   1098: aload 17
    //   1100: getfield 235	com/millennialmedia/android/VideoImage:inactivityTimeout	J
    //   1103: aload 17
    //   1105: getfield 212	com/millennialmedia/android/VideoImage:appearanceDelay	J
    //   1108: ladd
    //   1109: aload 17
    //   1111: getfield 238	com/millennialmedia/android/VideoImage:fadeDuration	J
    //   1114: ladd
    //   1115: invokevirtual 232	android/os/Handler:sendMessageDelayed	(Landroid/os/Message;J)Z
    //   1118: pop
    //   1119: iinc 13 1
    //   1122: goto -588 -> 534
    //   1125: aload_0
    //   1126: aload_0
    //   1127: invokevirtual 406	com/millennialmedia/android/VideoPlayer:getIntent	()Landroid/content/Intent;
    //   1130: ldc_w 961
    //   1133: iconst_0
    //   1134: invokevirtual 965	android/content/Intent:getBooleanExtra	(Ljava/lang/String;Z)Z
    //   1137: putfield 176	com/millennialmedia/android/VideoPlayer:isCachedAd	Z
    //   1140: aload_0
    //   1141: iconst_0
    //   1142: putfield 58	com/millennialmedia/android/VideoPlayer:currentVideoPosition	I
    //   1145: aload_0
    //   1146: iconst_0
    //   1147: putfield 609	com/millennialmedia/android/VideoPlayer:videoCompletedOnce	Z
    //   1150: aload_0
    //   1151: iconst_0
    //   1152: putfield 124	com/millennialmedia/android/VideoPlayer:videoCompleted	Z
    //   1155: goto -1078 -> 77
    //   1158: astore 24
    //   1160: aconst_null
    //   1161: astore 23
    //   1163: aload 24
    //   1165: invokevirtual 966	android/database/sqlite/SQLiteException:printStackTrace	()V
    //   1168: aload 23
    //   1170: ifnull -738 -> 432
    //   1173: aload 23
    //   1175: invokevirtual 853	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   1178: goto -746 -> 432
    //   1181: astore 25
    //   1183: aconst_null
    //   1184: astore 23
    //   1186: aload 23
    //   1188: ifnull +8 -> 1196
    //   1191: aload 23
    //   1193: invokevirtual 853	com/millennialmedia/android/AdDatabaseHelper:close	()V
    //   1196: aload 25
    //   1198: athrow
    //   1199: aload_0
    //   1200: aload_1
    //   1201: ldc_w 967
    //   1204: invokevirtual 971	android/os/Bundle:getParcelable	(Ljava/lang/String;)Landroid/os/Parcelable;
    //   1207: checkcast 119	com/millennialmedia/android/VideoAd
    //   1210: putfield 70	com/millennialmedia/android/VideoPlayer:videoAd	Lcom/millennialmedia/android/VideoAd;
    //   1213: aload_0
    //   1214: aload_1
    //   1215: ldc_w 972
    //   1218: invokevirtual 971	android/os/Bundle:getParcelable	(Ljava/lang/String;)Landroid/os/Parcelable;
    //   1221: checkcast 718	com/millennialmedia/android/EventLogSet
    //   1224: putfield 614	com/millennialmedia/android/VideoPlayer:logSet	Lcom/millennialmedia/android/EventLogSet;
    //   1227: aload_0
    //   1228: aload_1
    //   1229: ldc_w 974
    //   1232: invokevirtual 764	android/os/Bundle:getBoolean	(Ljava/lang/String;)Z
    //   1235: putfield 56	com/millennialmedia/android/VideoPlayer:showBottomBar	Z
    //   1238: aload_0
    //   1239: aload_0
    //   1240: getfield 70	com/millennialmedia/android/VideoPlayer:videoAd	Lcom/millennialmedia/android/VideoAd;
    //   1243: getfield 862	com/millennialmedia/android/VideoAd:showCountdown	Z
    //   1246: putfield 60	com/millennialmedia/android/VideoPlayer:showCountdownHud	Z
    //   1249: goto -773 -> 476
    //   1252: aload_0
    //   1253: invokevirtual 977	com/millennialmedia/android/VideoPlayer:getCacheDir	()Ljava/io/File;
    //   1256: astore 14
    //   1258: goto -668 -> 590
    //   1261: aload_0
    //   1262: getfield 182	com/millennialmedia/android/VideoPlayer:buttonsLayout	Landroid/widget/RelativeLayout;
    //   1265: aload 15
    //   1267: aload 16
    //   1269: invokevirtual 600	android/widget/RelativeLayout:addView	(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    //   1272: goto -202 -> 1070
    //   1275: aload_0
    //   1276: getfield 799	com/millennialmedia/android/VideoPlayer:relLayout	Landroid/widget/RelativeLayout;
    //   1279: aload_0
    //   1280: getfield 182	com/millennialmedia/android/VideoPlayer:buttonsLayout	Landroid/widget/RelativeLayout;
    //   1283: aload 7
    //   1285: invokevirtual 600	android/widget/RelativeLayout:addView	(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    //   1288: aload_0
    //   1289: getfield 56	com/millennialmedia/android/VideoPlayer:showBottomBar	Z
    //   1292: ifeq +280 -> 1572
    //   1295: aload_0
    //   1296: new 218	android/widget/RelativeLayout
    //   1299: dup
    //   1300: aload_0
    //   1301: invokespecial 797	android/widget/RelativeLayout:<init>	(Landroid/content/Context;)V
    //   1304: putfield 979	com/millennialmedia/android/VideoPlayer:controlsLayout	Landroid/widget/RelativeLayout;
    //   1307: aload_0
    //   1308: getfield 979	com/millennialmedia/android/VideoPlayer:controlsLayout	Landroid/widget/RelativeLayout;
    //   1311: ldc_w 808
    //   1314: invokevirtual 811	android/widget/RelativeLayout:setBackgroundColor	(I)V
    //   1317: new 575	android/widget/RelativeLayout$LayoutParams
    //   1320: dup
    //   1321: iconst_m1
    //   1322: bipush 254
    //   1324: invokespecial 578	android/widget/RelativeLayout$LayoutParams:<init>	(II)V
    //   1327: astore 8
    //   1329: aload_0
    //   1330: getfield 979	com/millennialmedia/android/VideoPlayer:controlsLayout	Landroid/widget/RelativeLayout;
    //   1333: aload 8
    //   1335: invokevirtual 807	android/widget/RelativeLayout:setLayoutParams	(Landroid/view/ViewGroup$LayoutParams;)V
    //   1338: aload 8
    //   1340: bipush 12
    //   1342: invokevirtual 596	android/widget/RelativeLayout$LayoutParams:addRule	(I)V
    //   1345: aload_0
    //   1346: new 981	android/widget/Button
    //   1349: dup
    //   1350: aload_0
    //   1351: invokespecial 982	android/widget/Button:<init>	(Landroid/content/Context;)V
    //   1354: putfield 984	com/millennialmedia/android/VideoPlayer:mRewind	Landroid/widget/Button;
    //   1357: aload_0
    //   1358: new 981	android/widget/Button
    //   1361: dup
    //   1362: aload_0
    //   1363: invokespecial 982	android/widget/Button:<init>	(Landroid/content/Context;)V
    //   1366: putfield 102	com/millennialmedia/android/VideoPlayer:mPausePlay	Landroid/widget/Button;
    //   1369: aload_0
    //   1370: new 981	android/widget/Button
    //   1373: dup
    //   1374: aload_0
    //   1375: invokespecial 982	android/widget/Button:<init>	(Landroid/content/Context;)V
    //   1378: putfield 986	com/millennialmedia/android/VideoPlayer:mStop	Landroid/widget/Button;
    //   1381: aload_0
    //   1382: getfield 984	com/millennialmedia/android/VideoPlayer:mRewind	Landroid/widget/Button;
    //   1385: ldc_w 987
    //   1388: invokevirtual 990	android/widget/Button:setBackgroundResource	(I)V
    //   1391: aload_0
    //   1392: getfield 102	com/millennialmedia/android/VideoPlayer:mPausePlay	Landroid/widget/Button;
    //   1395: ldc_w 991
    //   1398: invokevirtual 990	android/widget/Button:setBackgroundResource	(I)V
    //   1401: aload_0
    //   1402: getfield 986	com/millennialmedia/android/VideoPlayer:mStop	Landroid/widget/Button;
    //   1405: ldc_w 992
    //   1408: invokevirtual 990	android/widget/Button:setBackgroundResource	(I)V
    //   1411: new 575	android/widget/RelativeLayout$LayoutParams
    //   1414: dup
    //   1415: bipush 254
    //   1417: bipush 254
    //   1419: invokespecial 578	android/widget/RelativeLayout$LayoutParams:<init>	(II)V
    //   1422: astore 9
    //   1424: new 575	android/widget/RelativeLayout$LayoutParams
    //   1427: dup
    //   1428: bipush 254
    //   1430: bipush 254
    //   1432: invokespecial 578	android/widget/RelativeLayout$LayoutParams:<init>	(II)V
    //   1435: astore 10
    //   1437: new 575	android/widget/RelativeLayout$LayoutParams
    //   1440: dup
    //   1441: bipush 254
    //   1443: bipush 254
    //   1445: invokespecial 578	android/widget/RelativeLayout$LayoutParams:<init>	(II)V
    //   1448: astore 11
    //   1450: aload 9
    //   1452: bipush 14
    //   1454: invokevirtual 596	android/widget/RelativeLayout$LayoutParams:addRule	(I)V
    //   1457: aload_0
    //   1458: getfield 979	com/millennialmedia/android/VideoPlayer:controlsLayout	Landroid/widget/RelativeLayout;
    //   1461: aload_0
    //   1462: getfield 102	com/millennialmedia/android/VideoPlayer:mPausePlay	Landroid/widget/Button;
    //   1465: aload 9
    //   1467: invokevirtual 600	android/widget/RelativeLayout:addView	(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    //   1470: aload 11
    //   1472: iconst_0
    //   1473: aload_0
    //   1474: getfield 102	com/millennialmedia/android/VideoPlayer:mPausePlay	Landroid/widget/Button;
    //   1477: invokevirtual 993	android/widget/Button:getId	()I
    //   1480: invokevirtual 605	android/widget/RelativeLayout$LayoutParams:addRule	(II)V
    //   1483: aload_0
    //   1484: getfield 979	com/millennialmedia/android/VideoPlayer:controlsLayout	Landroid/widget/RelativeLayout;
    //   1487: aload_0
    //   1488: getfield 984	com/millennialmedia/android/VideoPlayer:mRewind	Landroid/widget/Button;
    //   1491: invokevirtual 995	android/widget/RelativeLayout:addView	(Landroid/view/View;)V
    //   1494: aload 10
    //   1496: bipush 11
    //   1498: invokevirtual 596	android/widget/RelativeLayout$LayoutParams:addRule	(I)V
    //   1501: aload_0
    //   1502: getfield 979	com/millennialmedia/android/VideoPlayer:controlsLayout	Landroid/widget/RelativeLayout;
    //   1505: aload_0
    //   1506: getfield 986	com/millennialmedia/android/VideoPlayer:mStop	Landroid/widget/Button;
    //   1509: aload 10
    //   1511: invokevirtual 600	android/widget/RelativeLayout:addView	(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    //   1514: aload_0
    //   1515: getfield 984	com/millennialmedia/android/VideoPlayer:mRewind	Landroid/widget/Button;
    //   1518: new 997	com/millennialmedia/android/VideoPlayer$2
    //   1521: dup
    //   1522: aload_0
    //   1523: invokespecial 998	com/millennialmedia/android/VideoPlayer$2:<init>	(Lcom/millennialmedia/android/VideoPlayer;)V
    //   1526: invokevirtual 999	android/widget/Button:setOnClickListener	(Landroid/view/View$OnClickListener;)V
    //   1529: aload_0
    //   1530: getfield 102	com/millennialmedia/android/VideoPlayer:mPausePlay	Landroid/widget/Button;
    //   1533: new 1001	com/millennialmedia/android/VideoPlayer$3
    //   1536: dup
    //   1537: aload_0
    //   1538: invokespecial 1002	com/millennialmedia/android/VideoPlayer$3:<init>	(Lcom/millennialmedia/android/VideoPlayer;)V
    //   1541: invokevirtual 999	android/widget/Button:setOnClickListener	(Landroid/view/View$OnClickListener;)V
    //   1544: aload_0
    //   1545: getfield 986	com/millennialmedia/android/VideoPlayer:mStop	Landroid/widget/Button;
    //   1548: new 1004	com/millennialmedia/android/VideoPlayer$4
    //   1551: dup
    //   1552: aload_0
    //   1553: invokespecial 1005	com/millennialmedia/android/VideoPlayer$4:<init>	(Lcom/millennialmedia/android/VideoPlayer;)V
    //   1556: invokevirtual 999	android/widget/Button:setOnClickListener	(Landroid/view/View$OnClickListener;)V
    //   1559: aload_0
    //   1560: getfield 799	com/millennialmedia/android/VideoPlayer:relLayout	Landroid/widget/RelativeLayout;
    //   1563: aload_0
    //   1564: getfield 979	com/millennialmedia/android/VideoPlayer:controlsLayout	Landroid/widget/RelativeLayout;
    //   1567: aload 8
    //   1569: invokevirtual 600	android/widget/RelativeLayout:addView	(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    //   1572: aload_0
    //   1573: getfield 979	com/millennialmedia/android/VideoPlayer:controlsLayout	Landroid/widget/RelativeLayout;
    //   1576: ifnull +10 -> 1586
    //   1579: aload_0
    //   1580: getfield 979	com/millennialmedia/android/VideoPlayer:controlsLayout	Landroid/widget/RelativeLayout;
    //   1583: invokevirtual 1008	android/widget/RelativeLayout:bringToFront	()V
    //   1586: aload_0
    //   1587: getfield 182	com/millennialmedia/android/VideoPlayer:buttonsLayout	Landroid/widget/RelativeLayout;
    //   1590: ifnull +14 -> 1604
    //   1593: aload_0
    //   1594: getfield 799	com/millennialmedia/android/VideoPlayer:relLayout	Landroid/widget/RelativeLayout;
    //   1597: aload_0
    //   1598: getfield 182	com/millennialmedia/android/VideoPlayer:buttonsLayout	Landroid/widget/RelativeLayout;
    //   1601: invokevirtual 637	android/widget/RelativeLayout:bringChildToFront	(Landroid/view/View;)V
    //   1604: aload_0
    //   1605: aload_0
    //   1606: getfield 799	com/millennialmedia/android/VideoPlayer:relLayout	Landroid/widget/RelativeLayout;
    //   1609: invokevirtual 1011	com/millennialmedia/android/VideoPlayer:setContentView	(Landroid/view/View;)V
    //   1612: return
    //   1613: astore 25
    //   1615: goto -429 -> 1186
    //   1618: astore 24
    //   1620: goto -457 -> 1163
    //   1623: aconst_null
    //   1624: astore 12
    //   1626: goto -1100 -> 526
    //
    // Exception table:
    //   from	to	target	type
    //   406	416	1158	android/database/sqlite/SQLiteException
    //   406	416	1181	finally
    //   416	427	1613	finally
    //   1163	1168	1613	finally
    //   416	427	1618	android/database/sqlite/SQLiteException
  }

  public void onDestroy()
  {
    super.onDestroy();
    unregisterReceiver(this.receiver);
    if (this.isCachedAd)
      stopServer();
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 4) && (paramKeyEvent.getRepeatCount() == 0) && (!this.videoCompletedOnce))
      return true;
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  public void onPause()
  {
    super.onPause();
    Log.v("MillennialMediaSDK", "VideoPlayer - onPause");
    if (!this.waitForUserPresent)
      pauseVideo();
  }

  public void onResume()
  {
    super.onResume();
    Log.v("MillennialMediaSDK", "VideoPlayer - onResume");
    if (!this.waitForUserPresent)
      resumeVideo();
  }

  public void onSaveInstanceState(Bundle paramBundle)
  {
    if (this.mVideoView != null)
    {
      paramBundle.putInt("videoPosition", this.mVideoView.getCurrentPosition());
      paramBundle.putInt("lastVideoPosition", this.lastVideoPosition);
    }
    paramBundle.putBoolean("isCachedAd", this.isCachedAd);
    paramBundle.putBoolean("videoCompleted", this.videoCompleted);
    paramBundle.putBoolean("videoCompletedOnce", this.videoCompletedOnce);
    paramBundle.putParcelable("logSet", this.logSet);
    paramBundle.putBoolean("shouldShowBottomBar", this.showBottomBar);
    paramBundle.putParcelable("videoAd", this.videoAd);
    super.onSaveInstanceState(paramBundle);
  }

  public void onStart()
  {
    super.onStart();
    if ((this.videoAd != null) && (this.videoAd.stayInPlayer == true) && (this.videoCompleted == true) && (this.videoAd.buttons != null))
      for (int i = 0; i < this.videoAd.buttons.size(); i++)
      {
        VideoImage localVideoImage = (VideoImage)this.videoAd.buttons.get(i);
        setButtonAlpha(localVideoImage.button, localVideoImage.fromAlpha);
        if (localVideoImage.button.getParent() == null)
          this.buttonsLayout.addView(localVideoImage.button, localVideoImage.layoutParams);
        for (int j = 0; j < this.videoAd.buttons.size(); j++)
          this.buttonsLayout.bringChildToFront(((VideoImage)this.videoAd.buttons.get(j)).button);
      }
  }

  public void startServer(String paramString, int paramInt, boolean paramBoolean)
  {
    try
    {
      if (this.videoServer == null)
      {
        this.videoServer = new VideoServer(paramString, paramBoolean);
        Thread localThread = new Thread(this.videoServer);
        localThread.start();
        localThread.getId();
        if (this.mVideoView == null)
          break label179;
        this.mVideoView.setVideoURI(Uri.parse("http://localhost:" + this.videoServer.port + "/" + paramString + "/video.dat"));
        this.mVideoView.setOnCompletionListener(new VideoPlayer.12(this));
        this.mVideoView.setOnPreparedListener(new VideoPlayer.13(this));
        this.mVideoView.setOnErrorListener(new VideoPlayer.14(this));
        this.mVideoView.requestFocus();
        this.mVideoView.seekTo(paramInt);
        this.mVideoView.start();
        this.paused = false;
      }
      while (true)
      {
        return;
        label179: Log.e("MillennialMediaSDK", "Null Video View");
      }
    }
    finally
    {
    }
  }

  public void stopServer()
  {
    try
    {
      if (this.videoServer != null)
      {
        MMAdViewSDK.Log.d("Stop video server");
        if (this.mVideoView != null)
          this.mVideoView.stopPlayback();
        this.videoServer.requestStop();
        this.videoServer = null;
      }
      return;
    }
    finally
    {
    }
  }

  private class DelayedAnimationListener
    implements Animation.AnimationListener
  {
    private ImageButton button;
    private RelativeLayout.LayoutParams layoutParams;

    DelayedAnimationListener(ImageButton paramLayoutParams, RelativeLayout.LayoutParams arg3)
    {
      this.button = paramLayoutParams;
      Object localObject;
      this.layoutParams = localObject;
    }

    public void onAnimationEnd(Animation paramAnimation)
    {
    }

    public void onAnimationRepeat(Animation paramAnimation)
    {
    }

    public void onAnimationStart(Animation paramAnimation)
    {
    }
  }

  final class NetworkingThread extends Thread
  {
    String locationString;
    String mimeTypeString;
    String urlString;
    String userAgent = null;

    NetworkingThread()
    {
    }

    public final void run()
    {
      this.mimeTypeString = null;
      this.locationString = this.urlString;
      try
      {
        int i;
        do
        {
          this.locationString = this.urlString;
          URL localURL = new URL(this.urlString);
          HttpURLConnection.setFollowRedirects(false);
          HttpURLConnection localHttpURLConnection = (HttpURLConnection)localURL.openConnection();
          localHttpURLConnection.setRequestMethod("GET");
          if (this.userAgent != null)
            localHttpURLConnection.setRequestProperty("User-Agent", this.userAgent);
          localHttpURLConnection.connect();
          this.urlString = localHttpURLConnection.getHeaderField("Location");
          this.mimeTypeString = localHttpURLConnection.getHeaderField("Content-Type");
          i = localHttpURLConnection.getResponseCode();
          MMAdViewSDK.Log.d("Response Code:" + localHttpURLConnection.getResponseCode() + " Response Message:" + localHttpURLConnection.getResponseMessage());
          MMAdViewSDK.Log.d("urlString: " + this.urlString);
        }
        while ((i >= 300) && (i < 400));
        return;
      }
      catch (IOException localIOException)
      {
        localIOException.printStackTrace();
        return;
      }
      catch (MalformedURLException localMalformedURLException)
      {
      }
    }
  }

  private class ScreenReceiver extends BroadcastReceiver
  {
    private ScreenReceiver()
    {
    }

    public void onReceive(Context paramContext, Intent paramIntent)
    {
      if (paramIntent.getAction().equals("android.intent.action.SCREEN_OFF"))
      {
        Log.v("MillennialMediaSDK", "VideoPlayer - Screen off");
        VideoPlayer.access$1202(VideoPlayer.this, true);
      }
      do
      {
        return;
        if (paramIntent.getAction().equals("android.intent.action.SCREEN_ON"))
        {
          Log.v("MillennialMediaSDK", "VideoPlayer - Screen on");
          return;
        }
      }
      while (!paramIntent.getAction().equals("android.intent.action.USER_PRESENT"));
      Log.v("MillennialMediaSDK", "VideoPlayer - Screen unlocked");
      VideoPlayer.this.resumeVideo();
      VideoPlayer.access$1202(VideoPlayer.this, false);
    }
  }

  private class VideoServer
    implements Runnable
  {
    private String cacheDir;
    boolean done = false;
    private final String filePath;
    Integer port;
    private ServerSocket serverSocket = null;

    public VideoServer(String paramBoolean, boolean arg3)
    {
      this.filePath = paramBoolean;
      int i;
      if (i != 0)
        this.cacheDir = (Environment.getExternalStorageDirectory().getPath() + "/.mmsyscache/");
      try
      {
        while (true)
        {
          this.serverSocket = new ServerSocket();
          this.serverSocket.bind(null);
          this.serverSocket.setSoTimeout(0);
          this.port = new Integer(this.serverSocket.getLocalPort());
          MMAdViewSDK.Log.v("Video Server Port: " + this.port);
          return;
          this.cacheDir = (VideoPlayer.this.getCacheDir() + "/");
        }
      }
      catch (IOException localIOException)
      {
        localIOException.printStackTrace();
      }
    }

    public void requestStop()
    {
      try
      {
        this.done = true;
        MMAdViewSDK.Log.v("Requested video server stop. Done: " + this.done);
        return;
      }
      finally
      {
        localObject = finally;
        throw localObject;
      }
    }

    // ERROR //
    public void run()
    {
      // Byte code:
      //   0: aconst_null
      //   1: astore_1
      //   2: aconst_null
      //   3: astore_2
      //   4: aconst_null
      //   5: astore_3
      //   6: sipush 1024
      //   9: newarray byte
      //   11: astore 4
      //   13: aload_0
      //   14: getfield 30	com/millennialmedia/android/VideoPlayer$VideoServer:done	Z
      //   17: ifne +998 -> 1015
      //   20: aload_0
      //   21: getfield 28	com/millennialmedia/android/VideoPlayer$VideoServer:serverSocket	Ljava/net/ServerSocket;
      //   24: invokevirtual 117	java/net/ServerSocket:accept	()Ljava/net/Socket;
      //   27: astore_1
      //   28: ldc 119
      //   30: invokestatic 92	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
      //   33: aload_1
      //   34: invokevirtual 125	java/net/Socket:getInputStream	()Ljava/io/InputStream;
      //   37: astore 18
      //   39: aload_1
      //   40: invokevirtual 129	java/net/Socket:getOutputStream	()Ljava/io/OutputStream;
      //   43: astore_2
      //   44: new 34	java/lang/StringBuilder
      //   47: dup
      //   48: invokespecial 35	java/lang/StringBuilder:<init>	()V
      //   51: astore 19
      //   53: aload 18
      //   55: aload 4
      //   57: invokevirtual 135	java/io/InputStream:read	([B)I
      //   60: pop
      //   61: aload 19
      //   63: new 137	java/lang/String
      //   66: dup
      //   67: aload 4
      //   69: invokespecial 140	java/lang/String:<init>	([B)V
      //   72: invokevirtual 51	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   75: pop
      //   76: aload 19
      //   78: invokevirtual 56	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   81: astore 22
      //   83: aload 22
      //   85: ldc 142
      //   87: invokevirtual 146	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
      //   90: ifeq -37 -> 53
      //   93: new 34	java/lang/StringBuilder
      //   96: dup
      //   97: ldc 148
      //   99: invokespecial 84	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
      //   102: aload 22
      //   104: invokevirtual 51	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   107: invokevirtual 56	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   110: invokestatic 92	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
      //   113: ldc 150
      //   115: invokestatic 92	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
      //   118: aload 22
      //   120: new 34	java/lang/StringBuilder
      //   123: dup
      //   124: ldc 152
      //   126: invokespecial 84	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
      //   129: aload_0
      //   130: getfield 32	com/millennialmedia/android/VideoPlayer$VideoServer:filePath	Ljava/lang/String;
      //   133: invokevirtual 51	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   136: ldc 154
      //   138: invokevirtual 51	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   141: invokevirtual 56	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   144: invokevirtual 158	java/lang/String:startsWith	(Ljava/lang/String;)Z
      //   147: ifeq +158 -> 305
      //   150: new 43	java/io/File
      //   153: dup
      //   154: new 34	java/lang/StringBuilder
      //   157: dup
      //   158: invokespecial 35	java/lang/StringBuilder:<init>	()V
      //   161: aload_0
      //   162: getfield 58	com/millennialmedia/android/VideoPlayer$VideoServer:cacheDir	Ljava/lang/String;
      //   165: invokevirtual 51	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   168: aload_0
      //   169: getfield 32	com/millennialmedia/android/VideoPlayer$VideoServer:filePath	Ljava/lang/String;
      //   172: invokevirtual 51	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   175: ldc 154
      //   177: invokevirtual 51	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   180: invokevirtual 56	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   183: invokespecial 159	java/io/File:<init>	(Ljava/lang/String;)V
      //   186: astore 23
      //   188: aload_2
      //   189: ldc 161
      //   191: invokevirtual 165	java/lang/String:getBytes	()[B
      //   194: invokevirtual 170	java/io/OutputStream:write	([B)V
      //   197: aload_2
      //   198: ldc 172
      //   200: invokevirtual 165	java/lang/String:getBytes	()[B
      //   203: invokevirtual 170	java/io/OutputStream:write	([B)V
      //   206: aload_2
      //   207: new 137	java/lang/String
      //   210: dup
      //   211: new 34	java/lang/StringBuilder
      //   214: dup
      //   215: ldc 174
      //   217: invokespecial 84	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
      //   220: aload 23
      //   222: invokevirtual 178	java/io/File:length	()J
      //   225: invokevirtual 181	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
      //   228: ldc 183
      //   230: invokevirtual 51	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   233: invokevirtual 56	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   236: invokespecial 184	java/lang/String:<init>	(Ljava/lang/String;)V
      //   239: invokevirtual 165	java/lang/String:getBytes	()[B
      //   242: invokevirtual 170	java/io/OutputStream:write	([B)V
      //   245: aload_2
      //   246: ldc 186
      //   248: invokevirtual 165	java/lang/String:getBytes	()[B
      //   251: invokevirtual 170	java/io/OutputStream:write	([B)V
      //   254: aload_2
      //   255: ldc 188
      //   257: invokevirtual 165	java/lang/String:getBytes	()[B
      //   260: invokevirtual 170	java/io/OutputStream:write	([B)V
      //   263: ldc 190
      //   265: invokestatic 92	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
      //   268: aload_1
      //   269: ifnull +7 -> 276
      //   272: aload_1
      //   273: invokevirtual 193	java/net/Socket:close	()V
      //   276: aload_3
      //   277: ifnull +7 -> 284
      //   280: aload_3
      //   281: invokevirtual 196	java/io/FileInputStream:close	()V
      //   284: aload_2
      //   285: ifnull -272 -> 13
      //   288: aload_2
      //   289: invokevirtual 197	java/io/OutputStream:close	()V
      //   292: goto -279 -> 13
      //   295: astore 24
      //   297: aload 24
      //   299: invokevirtual 102	java/io/IOException:printStackTrace	()V
      //   302: goto -289 -> 13
      //   305: aload 22
      //   307: new 34	java/lang/StringBuilder
      //   310: dup
      //   311: ldc 199
      //   313: invokespecial 84	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
      //   316: aload_0
      //   317: getfield 32	com/millennialmedia/android/VideoPlayer$VideoServer:filePath	Ljava/lang/String;
      //   320: invokevirtual 51	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   323: ldc 154
      //   325: invokevirtual 51	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   328: invokevirtual 56	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   331: invokevirtual 158	java/lang/String:startsWith	(Ljava/lang/String;)Z
      //   334: ifeq +864 -> 1198
      //   337: new 43	java/io/File
      //   340: dup
      //   341: new 34	java/lang/StringBuilder
      //   344: dup
      //   345: invokespecial 35	java/lang/StringBuilder:<init>	()V
      //   348: aload_0
      //   349: getfield 58	com/millennialmedia/android/VideoPlayer$VideoServer:cacheDir	Ljava/lang/String;
      //   352: invokevirtual 51	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   355: aload_0
      //   356: getfield 32	com/millennialmedia/android/VideoPlayer$VideoServer:filePath	Ljava/lang/String;
      //   359: invokevirtual 51	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   362: ldc 154
      //   364: invokevirtual 51	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   367: invokevirtual 56	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   370: invokespecial 159	java/io/File:<init>	(Ljava/lang/String;)V
      //   373: astore 25
      //   375: aload_1
      //   376: invokevirtual 129	java/net/Socket:getOutputStream	()Ljava/io/OutputStream;
      //   379: astore_2
      //   380: aload 22
      //   382: ldc 201
      //   384: invokevirtual 146	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
      //   387: ifeq +672 -> 1059
      //   390: ldc 203
      //   392: invokestatic 92	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
      //   395: ldc 205
      //   397: invokestatic 211	java/util/regex/Pattern:compile	(Ljava/lang/String;)Ljava/util/regex/Pattern;
      //   400: aload 22
      //   402: invokevirtual 215	java/util/regex/Pattern:matcher	(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
      //   405: astore 30
      //   407: lconst_1
      //   408: lstore 31
      //   410: lconst_1
      //   411: lstore 33
      //   413: aload 30
      //   415: invokevirtual 221	java/util/regex/Matcher:find	()Z
      //   418: ifeq +32 -> 450
      //   421: new 71	java/lang/Integer
      //   424: dup
      //   425: aload 30
      //   427: iconst_1
      //   428: invokevirtual 225	java/util/regex/Matcher:group	(I)Ljava/lang/String;
      //   431: invokespecial 226	java/lang/Integer:<init>	(Ljava/lang/String;)V
      //   434: invokevirtual 229	java/lang/Integer:intValue	()I
      //   437: i2l
      //   438: lstore 31
      //   440: aload 25
      //   442: invokevirtual 178	java/io/File:length	()J
      //   445: lstore 33
      //   447: goto -34 -> 413
      //   450: ldc 231
      //   452: invokestatic 211	java/util/regex/Pattern:compile	(Ljava/lang/String;)Ljava/util/regex/Pattern;
      //   455: aload 22
      //   457: invokevirtual 215	java/util/regex/Pattern:matcher	(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
      //   460: astore 35
      //   462: lload 33
      //   464: lstore 36
      //   466: lload 31
      //   468: lstore 38
      //   470: lload 36
      //   472: lstore 40
      //   474: aload 35
      //   476: invokevirtual 221	java/util/regex/Matcher:find	()Z
      //   479: ifeq +85 -> 564
      //   482: aload 35
      //   484: iconst_1
      //   485: invokevirtual 225	java/util/regex/Matcher:group	(I)Ljava/lang/String;
      //   488: astore 52
      //   490: aload 35
      //   492: iconst_2
      //   493: invokevirtual 225	java/util/regex/Matcher:group	(I)Ljava/lang/String;
      //   496: astore 53
      //   498: new 71	java/lang/Integer
      //   501: dup
      //   502: aload 52
      //   504: invokespecial 226	java/lang/Integer:<init>	(Ljava/lang/String;)V
      //   507: invokevirtual 229	java/lang/Integer:intValue	()I
      //   510: i2l
      //   511: lstore 54
      //   513: aload 53
      //   515: ifnull +29 -> 544
      //   518: new 71	java/lang/Integer
      //   521: dup
      //   522: aload 53
      //   524: invokespecial 226	java/lang/Integer:<init>	(Ljava/lang/String;)V
      //   527: invokevirtual 229	java/lang/Integer:intValue	()I
      //   530: i2l
      //   531: lstore 56
      //   533: lload 54
      //   535: lstore 38
      //   537: lload 56
      //   539: lstore 40
      //   541: goto -67 -> 474
      //   544: aload 25
      //   546: invokevirtual 178	java/io/File:length	()J
      //   549: lconst_1
      //   550: lsub
      //   551: lstore 58
      //   553: lload 54
      //   555: lstore 38
      //   557: lload 58
      //   559: lstore 40
      //   561: goto -87 -> 474
      //   564: ldc 233
      //   566: new 34	java/lang/StringBuilder
      //   569: dup
      //   570: ldc 235
      //   572: invokespecial 84	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
      //   575: lload 38
      //   577: invokevirtual 181	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
      //   580: ldc 237
      //   582: invokevirtual 51	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   585: lload 40
      //   587: invokevirtual 181	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
      //   590: invokevirtual 56	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   593: invokestatic 243	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
      //   596: pop
      //   597: new 195	java/io/FileInputStream
      //   600: dup
      //   601: aload 25
      //   603: invokespecial 246	java/io/FileInputStream:<init>	(Ljava/io/File;)V
      //   606: astore 9
      //   608: aload 9
      //   610: lload 38
      //   612: invokevirtual 250	java/io/FileInputStream:skip	(J)J
      //   615: lstore 45
      //   617: new 34	java/lang/StringBuilder
      //   620: dup
      //   621: ldc 252
      //   623: invokespecial 84	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
      //   626: lload 45
      //   628: invokevirtual 181	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
      //   631: invokevirtual 56	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   634: invokestatic 92	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
      //   637: lconst_1
      //   638: lload 40
      //   640: lload 45
      //   642: lsub
      //   643: ladd
      //   644: lstore 47
      //   646: aload_2
      //   647: ldc 254
      //   649: invokevirtual 165	java/lang/String:getBytes	()[B
      //   652: invokevirtual 170	java/io/OutputStream:write	([B)V
      //   655: aload_2
      //   656: ldc_w 256
      //   659: invokevirtual 165	java/lang/String:getBytes	()[B
      //   662: invokevirtual 170	java/io/OutputStream:write	([B)V
      //   665: aload_2
      //   666: ldc_w 258
      //   669: invokevirtual 165	java/lang/String:getBytes	()[B
      //   672: invokevirtual 170	java/io/OutputStream:write	([B)V
      //   675: lload 40
      //   677: lconst_1
      //   678: lcmp
      //   679: ifle +211 -> 890
      //   682: aload_2
      //   683: new 137	java/lang/String
      //   686: dup
      //   687: new 34	java/lang/StringBuilder
      //   690: dup
      //   691: ldc_w 260
      //   694: invokespecial 84	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
      //   697: lload 38
      //   699: invokevirtual 181	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
      //   702: ldc 237
      //   704: invokevirtual 51	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   707: lload 40
      //   709: invokevirtual 181	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
      //   712: ldc 183
      //   714: invokevirtual 51	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   717: invokevirtual 56	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   720: invokespecial 184	java/lang/String:<init>	(Ljava/lang/String;)V
      //   723: invokevirtual 165	java/lang/String:getBytes	()[B
      //   726: invokevirtual 170	java/io/OutputStream:write	([B)V
      //   729: aload_2
      //   730: ldc 172
      //   732: invokevirtual 165	java/lang/String:getBytes	()[B
      //   735: invokevirtual 170	java/io/OutputStream:write	([B)V
      //   738: aload_2
      //   739: new 137	java/lang/String
      //   742: dup
      //   743: new 34	java/lang/StringBuilder
      //   746: dup
      //   747: ldc 174
      //   749: invokespecial 84	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
      //   752: lload 47
      //   754: invokevirtual 181	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
      //   757: ldc 183
      //   759: invokevirtual 51	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   762: invokevirtual 56	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   765: invokespecial 184	java/lang/String:<init>	(Ljava/lang/String;)V
      //   768: invokevirtual 165	java/lang/String:getBytes	()[B
      //   771: invokevirtual 170	java/io/OutputStream:write	([B)V
      //   774: aload_2
      //   775: ldc 188
      //   777: invokevirtual 165	java/lang/String:getBytes	()[B
      //   780: invokevirtual 170	java/io/OutputStream:write	([B)V
      //   783: lload 47
      //   785: lstore 49
      //   787: aload 9
      //   789: aload 4
      //   791: invokevirtual 261	java/io/FileInputStream:read	([B)I
      //   794: istore 51
      //   796: iload 51
      //   798: ifgt +145 -> 943
      //   801: ldc_w 263
      //   804: invokestatic 92	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
      //   807: aload 9
      //   809: astore_3
      //   810: aload_2
      //   811: ldc 183
      //   813: invokevirtual 165	java/lang/String:getBytes	()[B
      //   816: invokevirtual 170	java/io/OutputStream:write	([B)V
      //   819: ldc_w 265
      //   822: invokestatic 92	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
      //   825: aload_0
      //   826: iconst_1
      //   827: putfield 30	com/millennialmedia/android/VideoPlayer$VideoServer:done	Z
      //   830: goto -567 -> 263
      //   833: astore 15
      //   835: aload 15
      //   837: invokevirtual 266	java/net/SocketTimeoutException:printStackTrace	()V
      //   840: aload_1
      //   841: ifnull +7 -> 848
      //   844: aload_1
      //   845: invokevirtual 193	java/net/Socket:close	()V
      //   848: ldc 190
      //   850: invokestatic 92	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
      //   853: aload_1
      //   854: ifnull +7 -> 861
      //   857: aload_1
      //   858: invokevirtual 193	java/net/Socket:close	()V
      //   861: aload_3
      //   862: ifnull +7 -> 869
      //   865: aload_3
      //   866: invokevirtual 196	java/io/FileInputStream:close	()V
      //   869: aload_2
      //   870: ifnull -857 -> 13
      //   873: aload_2
      //   874: invokevirtual 197	java/io/OutputStream:close	()V
      //   877: goto -864 -> 13
      //   880: astore 16
      //   882: aload 16
      //   884: invokevirtual 102	java/io/IOException:printStackTrace	()V
      //   887: goto -874 -> 13
      //   890: aload_2
      //   891: new 137	java/lang/String
      //   894: dup
      //   895: new 34	java/lang/StringBuilder
      //   898: dup
      //   899: ldc_w 260
      //   902: invokespecial 84	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
      //   905: lload 38
      //   907: invokevirtual 181	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
      //   910: ldc 237
      //   912: invokevirtual 51	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   915: aload 25
      //   917: invokevirtual 178	java/io/File:length	()J
      //   920: invokevirtual 181	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
      //   923: ldc 183
      //   925: invokevirtual 51	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   928: invokevirtual 56	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   931: invokespecial 184	java/lang/String:<init>	(Ljava/lang/String;)V
      //   934: invokevirtual 165	java/lang/String:getBytes	()[B
      //   937: invokevirtual 170	java/io/OutputStream:write	([B)V
      //   940: goto -211 -> 729
      //   943: lload 49
      //   945: iload 51
      //   947: i2l
      //   948: lcmp
      //   949: iflt +15 -> 964
      //   952: aload_2
      //   953: aload 4
      //   955: iconst_0
      //   956: iload 51
      //   958: invokevirtual 269	java/io/OutputStream:write	([BII)V
      //   961: goto +439 -> 1400
      //   964: aload_2
      //   965: aload 4
      //   967: iconst_0
      //   968: lload 49
      //   970: l2i
      //   971: invokevirtual 269	java/io/OutputStream:write	([BII)V
      //   974: goto +426 -> 1400
      //   977: astore 13
      //   979: aload 13
      //   981: invokevirtual 270	java/io/FileNotFoundException:printStackTrace	()V
      //   984: ldc 190
      //   986: invokestatic 92	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
      //   989: aload_1
      //   990: ifnull +7 -> 997
      //   993: aload_1
      //   994: invokevirtual 193	java/net/Socket:close	()V
      //   997: aload 9
      //   999: ifnull +8 -> 1007
      //   1002: aload 9
      //   1004: invokevirtual 196	java/io/FileInputStream:close	()V
      //   1007: aload_2
      //   1008: ifnull +7 -> 1015
      //   1011: aload_2
      //   1012: invokevirtual 197	java/io/OutputStream:close	()V
      //   1015: aload_0
      //   1016: getfield 30	com/millennialmedia/android/VideoPlayer$VideoServer:done	Z
      //   1019: ifeq +9 -> 1028
      //   1022: ldc_w 272
      //   1025: invokestatic 92	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
      //   1028: aload_0
      //   1029: getfield 28	com/millennialmedia/android/VideoPlayer$VideoServer:serverSocket	Ljava/net/ServerSocket;
      //   1032: ifnull +26 -> 1058
      //   1035: aload_0
      //   1036: getfield 28	com/millennialmedia/android/VideoPlayer$VideoServer:serverSocket	Ljava/net/ServerSocket;
      //   1039: invokevirtual 275	java/net/ServerSocket:isBound	()Z
      //   1042: ifeq +16 -> 1058
      //   1045: ldc_w 277
      //   1048: invokestatic 92	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
      //   1051: aload_0
      //   1052: getfield 28	com/millennialmedia/android/VideoPlayer$VideoServer:serverSocket	Ljava/net/ServerSocket;
      //   1055: invokevirtual 278	java/net/ServerSocket:close	()V
      //   1058: return
      //   1059: aload_2
      //   1060: ldc 161
      //   1062: invokevirtual 165	java/lang/String:getBytes	()[B
      //   1065: invokevirtual 170	java/io/OutputStream:write	([B)V
      //   1068: aload_2
      //   1069: ldc 172
      //   1071: invokevirtual 165	java/lang/String:getBytes	()[B
      //   1074: invokevirtual 170	java/io/OutputStream:write	([B)V
      //   1077: aload_2
      //   1078: new 137	java/lang/String
      //   1081: dup
      //   1082: new 34	java/lang/StringBuilder
      //   1085: dup
      //   1086: ldc 174
      //   1088: invokespecial 84	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
      //   1091: aload 25
      //   1093: invokevirtual 178	java/io/File:length	()J
      //   1096: invokevirtual 181	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
      //   1099: ldc 183
      //   1101: invokevirtual 51	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1104: invokevirtual 56	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   1107: invokespecial 184	java/lang/String:<init>	(Ljava/lang/String;)V
      //   1110: invokevirtual 165	java/lang/String:getBytes	()[B
      //   1113: invokevirtual 170	java/io/OutputStream:write	([B)V
      //   1116: aload_2
      //   1117: ldc 186
      //   1119: invokevirtual 165	java/lang/String:getBytes	()[B
      //   1122: invokevirtual 170	java/io/OutputStream:write	([B)V
      //   1125: aload_2
      //   1126: ldc 188
      //   1128: invokevirtual 165	java/lang/String:getBytes	()[B
      //   1131: invokevirtual 170	java/io/OutputStream:write	([B)V
      //   1134: new 195	java/io/FileInputStream
      //   1137: dup
      //   1138: aload 25
      //   1140: invokespecial 246	java/io/FileInputStream:<init>	(Ljava/io/File;)V
      //   1143: astore 9
      //   1145: aload 9
      //   1147: aload 4
      //   1149: invokevirtual 261	java/io/FileInputStream:read	([B)I
      //   1152: istore 28
      //   1154: iload 28
      //   1156: ifgt +18 -> 1174
      //   1159: ldc 233
      //   1161: ldc_w 263
      //   1164: invokestatic 243	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
      //   1167: pop
      //   1168: aload 9
      //   1170: astore_3
      //   1171: goto -361 -> 810
      //   1174: aload_2
      //   1175: aload 4
      //   1177: iconst_0
      //   1178: iload 28
      //   1180: invokevirtual 269	java/io/OutputStream:write	([BII)V
      //   1183: goto -38 -> 1145
      //   1186: astore 27
      //   1188: aload 9
      //   1190: astore_3
      //   1191: aload 27
      //   1193: astore 15
      //   1195: goto -360 -> 835
      //   1198: aload_2
      //   1199: ldc_w 280
      //   1202: invokevirtual 165	java/lang/String:getBytes	()[B
      //   1205: invokevirtual 170	java/io/OutputStream:write	([B)V
      //   1208: ldc_w 282
      //   1211: invokestatic 92	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
      //   1214: goto -951 -> 263
      //   1217: astore 12
      //   1219: aload_3
      //   1220: astore 9
      //   1222: aload 12
      //   1224: astore 13
      //   1226: goto -247 -> 979
      //   1229: astore 17
      //   1231: aload 17
      //   1233: invokevirtual 102	java/io/IOException:printStackTrace	()V
      //   1236: goto -388 -> 848
      //   1239: astore 8
      //   1241: aload_3
      //   1242: astore 9
      //   1244: aload 8
      //   1246: astore 10
      //   1248: ldc 190
      //   1250: invokestatic 92	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
      //   1253: aload_1
      //   1254: ifnull +7 -> 1261
      //   1257: aload_1
      //   1258: invokevirtual 193	java/net/Socket:close	()V
      //   1261: aload 9
      //   1263: ifnull +8 -> 1271
      //   1266: aload 9
      //   1268: invokevirtual 196	java/io/FileInputStream:close	()V
      //   1271: aload_2
      //   1272: ifnull +7 -> 1279
      //   1275: aload_2
      //   1276: invokevirtual 197	java/io/OutputStream:close	()V
      //   1279: aload 10
      //   1281: athrow
      //   1282: astore 14
      //   1284: aload 14
      //   1286: invokevirtual 102	java/io/IOException:printStackTrace	()V
      //   1289: goto -274 -> 1015
      //   1292: astore 43
      //   1294: aload 9
      //   1296: astore_3
      //   1297: aload 43
      //   1299: astore 6
      //   1301: aload 6
      //   1303: invokevirtual 102	java/io/IOException:printStackTrace	()V
      //   1306: ldc 190
      //   1308: invokestatic 92	com/millennialmedia/android/MMAdViewSDK$Log:v	(Ljava/lang/String;)V
      //   1311: aload_1
      //   1312: ifnull +7 -> 1319
      //   1315: aload_1
      //   1316: invokevirtual 193	java/net/Socket:close	()V
      //   1319: aload_3
      //   1320: ifnull +7 -> 1327
      //   1323: aload_3
      //   1324: invokevirtual 196	java/io/FileInputStream:close	()V
      //   1327: aload_2
      //   1328: ifnull -1315 -> 13
      //   1331: aload_2
      //   1332: invokevirtual 197	java/io/OutputStream:close	()V
      //   1335: goto -1322 -> 13
      //   1338: astore 7
      //   1340: aload 7
      //   1342: invokevirtual 102	java/io/IOException:printStackTrace	()V
      //   1345: goto -1332 -> 13
      //   1348: astore 11
      //   1350: aload 11
      //   1352: invokevirtual 102	java/io/IOException:printStackTrace	()V
      //   1355: goto -76 -> 1279
      //   1358: astore 5
      //   1360: aload 5
      //   1362: invokevirtual 102	java/io/IOException:printStackTrace	()V
      //   1365: return
      //   1366: astore 10
      //   1368: goto -120 -> 1248
      //   1371: astore 26
      //   1373: aload 9
      //   1375: astore_3
      //   1376: aload 26
      //   1378: astore 6
      //   1380: goto -79 -> 1301
      //   1383: astore 6
      //   1385: goto -84 -> 1301
      //   1388: astore 44
      //   1390: aload 9
      //   1392: astore_3
      //   1393: aload 44
      //   1395: astore 15
      //   1397: goto -562 -> 835
      //   1400: lload 49
      //   1402: iload 51
      //   1404: i2l
      //   1405: lsub
      //   1406: lstore 49
      //   1408: lload 49
      //   1410: lconst_0
      //   1411: lcmp
      //   1412: ifgt -625 -> 787
      //   1415: aload 9
      //   1417: astore_3
      //   1418: goto -608 -> 810
      //
      // Exception table:
      //   from	to	target	type
      //   272	276	295	java/io/IOException
      //   280	284	295	java/io/IOException
      //   288	292	295	java/io/IOException
      //   20	53	833	java/net/SocketTimeoutException
      //   53	263	833	java/net/SocketTimeoutException
      //   305	407	833	java/net/SocketTimeoutException
      //   413	447	833	java/net/SocketTimeoutException
      //   450	462	833	java/net/SocketTimeoutException
      //   474	513	833	java/net/SocketTimeoutException
      //   518	533	833	java/net/SocketTimeoutException
      //   544	553	833	java/net/SocketTimeoutException
      //   564	608	833	java/net/SocketTimeoutException
      //   810	830	833	java/net/SocketTimeoutException
      //   1059	1145	833	java/net/SocketTimeoutException
      //   1198	1214	833	java/net/SocketTimeoutException
      //   857	861	880	java/io/IOException
      //   865	869	880	java/io/IOException
      //   873	877	880	java/io/IOException
      //   608	637	977	java/io/FileNotFoundException
      //   646	675	977	java/io/FileNotFoundException
      //   682	729	977	java/io/FileNotFoundException
      //   729	783	977	java/io/FileNotFoundException
      //   787	796	977	java/io/FileNotFoundException
      //   801	807	977	java/io/FileNotFoundException
      //   890	940	977	java/io/FileNotFoundException
      //   952	961	977	java/io/FileNotFoundException
      //   964	974	977	java/io/FileNotFoundException
      //   1145	1154	977	java/io/FileNotFoundException
      //   1159	1168	977	java/io/FileNotFoundException
      //   1174	1183	977	java/io/FileNotFoundException
      //   1145	1154	1186	java/net/SocketTimeoutException
      //   1159	1168	1186	java/net/SocketTimeoutException
      //   1174	1183	1186	java/net/SocketTimeoutException
      //   20	53	1217	java/io/FileNotFoundException
      //   53	263	1217	java/io/FileNotFoundException
      //   305	407	1217	java/io/FileNotFoundException
      //   413	447	1217	java/io/FileNotFoundException
      //   450	462	1217	java/io/FileNotFoundException
      //   474	513	1217	java/io/FileNotFoundException
      //   518	533	1217	java/io/FileNotFoundException
      //   544	553	1217	java/io/FileNotFoundException
      //   564	608	1217	java/io/FileNotFoundException
      //   810	830	1217	java/io/FileNotFoundException
      //   1059	1145	1217	java/io/FileNotFoundException
      //   1198	1214	1217	java/io/FileNotFoundException
      //   844	848	1229	java/io/IOException
      //   20	53	1239	finally
      //   53	263	1239	finally
      //   305	407	1239	finally
      //   413	447	1239	finally
      //   450	462	1239	finally
      //   474	513	1239	finally
      //   518	533	1239	finally
      //   544	553	1239	finally
      //   564	608	1239	finally
      //   810	830	1239	finally
      //   835	840	1239	finally
      //   844	848	1239	finally
      //   1059	1145	1239	finally
      //   1198	1214	1239	finally
      //   1231	1236	1239	finally
      //   1301	1306	1239	finally
      //   993	997	1282	java/io/IOException
      //   1002	1007	1282	java/io/IOException
      //   1011	1015	1282	java/io/IOException
      //   608	637	1292	java/io/IOException
      //   646	675	1292	java/io/IOException
      //   682	729	1292	java/io/IOException
      //   729	783	1292	java/io/IOException
      //   787	796	1292	java/io/IOException
      //   801	807	1292	java/io/IOException
      //   890	940	1292	java/io/IOException
      //   952	961	1292	java/io/IOException
      //   964	974	1292	java/io/IOException
      //   1315	1319	1338	java/io/IOException
      //   1323	1327	1338	java/io/IOException
      //   1331	1335	1338	java/io/IOException
      //   1257	1261	1348	java/io/IOException
      //   1266	1271	1348	java/io/IOException
      //   1275	1279	1348	java/io/IOException
      //   1028	1058	1358	java/io/IOException
      //   608	637	1366	finally
      //   646	675	1366	finally
      //   682	729	1366	finally
      //   729	783	1366	finally
      //   787	796	1366	finally
      //   801	807	1366	finally
      //   890	940	1366	finally
      //   952	961	1366	finally
      //   964	974	1366	finally
      //   979	984	1366	finally
      //   1145	1154	1366	finally
      //   1159	1168	1366	finally
      //   1174	1183	1366	finally
      //   1145	1154	1371	java/io/IOException
      //   1159	1168	1371	java/io/IOException
      //   1174	1183	1371	java/io/IOException
      //   20	53	1383	java/io/IOException
      //   53	263	1383	java/io/IOException
      //   305	407	1383	java/io/IOException
      //   413	447	1383	java/io/IOException
      //   450	462	1383	java/io/IOException
      //   474	513	1383	java/io/IOException
      //   518	533	1383	java/io/IOException
      //   544	553	1383	java/io/IOException
      //   564	608	1383	java/io/IOException
      //   810	830	1383	java/io/IOException
      //   1059	1145	1383	java/io/IOException
      //   1198	1214	1383	java/io/IOException
      //   608	637	1388	java/net/SocketTimeoutException
      //   646	675	1388	java/net/SocketTimeoutException
      //   682	729	1388	java/net/SocketTimeoutException
      //   729	783	1388	java/net/SocketTimeoutException
      //   787	796	1388	java/net/SocketTimeoutException
      //   801	807	1388	java/net/SocketTimeoutException
      //   890	940	1388	java/net/SocketTimeoutException
      //   952	961	1388	java/net/SocketTimeoutException
      //   964	974	1388	java/net/SocketTimeoutException
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     com.millennialmedia.android.VideoPlayer
 * JD-Core Version:    0.6.2
 */