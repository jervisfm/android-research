package gbis.gbandroid;

import android.app.Application;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Environment;
import gbis.gbandroid.entities.AdsGSA;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class GBApplication extends Application
{
  public static final String ADS_DIRECTORY = "/Android/data/gbis.gbandroid/cache/Ads";
  public static final String ANALYTICS_EVENT_ACTION_BUTTON_AUTOCOMPLETE = "AutocompleteButton";
  public static final String ANALYTICS_EVENT_ACTION_BUTTON_CONTEXT_MENU = "ContextMenu";
  public static final String ANALYTICS_EVENT_ACTION_BUTTON_MENU = "MenuButton";
  public static final String ANALYTICS_EVENT_ACTION_BUTTON_PHONE = "PhoneButton";
  public static final String ANALYTICS_EVENT_ACTION_BUTTON_SCREEN = "ScreenButton";
  public static final String ANALYTICS_EVENT_ACTION_BUTTON_SEARCH = "SearchButton";
  public static final String BASE_DIRECTORY = "/Android/data/gbis.gbandroid/cache/";
  public static final String BRANDS_DIRECTORY = "/Android/data/gbis.gbandroid/cache/Brands";
  public static final int BRANDS_GLOBAL_VERSION = 1;
  public static final String TAG = "GasBuddy";
  private static GBApplication a;
  private Object b;
  private Location c;
  private String d;
  private AdsGSA e;

  private static String a(float paramFloat)
  {
    if (paramFloat < 1.0F)
      return "ldpi";
    if (paramFloat == 1.0F)
      return "mdpi";
    if (paramFloat == 1.5D)
      return "hdpi";
    if (paramFloat > 1.5D)
      return "xhdpi";
    return "mdpi";
  }

  public static GBApplication getInstance()
  {
    return a;
  }

  public AdsGSA getAdsGSA()
  {
    return this.e;
  }

  public Object getComplexObject()
  {
    return this.b;
  }

  public String getImageAdUrl(int paramInt, float paramFloat)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(getImageServer());
    localStringBuilder.append(1);
    localStringBuilder.append("/ads/");
    localStringBuilder.append(paramInt);
    localStringBuilder.append("/android/");
    localStringBuilder.append(a(paramFloat));
    localStringBuilder.append("/logo.png");
    return localStringBuilder.toString();
  }

  public String getImageServer()
  {
    return this.d;
  }

  public String getImageUrl(int paramInt, float paramFloat)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(getImageServer());
    localStringBuilder.append(1);
    localStringBuilder.append("/");
    localStringBuilder.append(paramInt);
    localStringBuilder.append("/android/");
    localStringBuilder.append(a(paramFloat));
    localStringBuilder.append("/logo.png");
    return localStringBuilder.toString();
  }

  public Location getLastKnownLocation()
  {
    return this.c;
  }

  public Bitmap getLogo(int paramInt, String paramString)
  {
    String str = Environment.getExternalStorageState();
    Object localObject;
    File localFile;
    if (!"mounted".equals(str))
    {
      boolean bool2 = "mounted_ro".equals(str);
      localObject = null;
      if (!bool2);
    }
    else
    {
      localFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + paramString, String.valueOf(paramInt));
      boolean bool1 = localFile.exists();
      localObject = null;
      if (!bool1);
    }
    try
    {
      Bitmap localBitmap = BitmapFactory.decodeStream(new FileInputStream(localFile));
      localObject = localBitmap;
      return localObject;
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
    }
    return null;
  }

  public void onCreate()
  {
    super.onCreate();
    a = this;
  }

  public void setAdsGSA(AdsGSA paramAdsGSA)
  {
    this.e = paramAdsGSA;
  }

  public void setComplexObject(Object paramObject)
  {
    this.b = paramObject;
  }

  public void setImageServer(String paramString)
  {
    this.d = paramString;
  }

  public void setLastKnowLocation(Location paramLocation)
  {
    this.c = paramLocation;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.GBApplication
 * JD-Core Version:    0.6.2
 */