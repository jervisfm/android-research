package gbis.gbandroid;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.location.Location;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import com.google.android.maps.GeoPoint;
import gbis.gbandroid.activities.base.GBActivity;
import gbis.gbandroid.activities.search.MainScreen;
import gbis.gbandroid.content.GBIntent;
import gbis.gbandroid.entities.Ad;
import gbis.gbandroid.entities.Ads;
import gbis.gbandroid.entities.InitializeMessage;
import gbis.gbandroid.entities.ListResults;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.queries.InitQuery;
import gbis.gbandroid.queries.ListQuery;
import gbis.gbandroid.services.BrandsDownloader;
import gbis.gbandroid.utils.CustomAsyncTask;
import gbis.gbandroid.utils.DateUtils;
import gbis.gbandroid.utils.FieldEncryption;
import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.Date;

public class InitScreen extends GBActivity
{
  private long a = 1L;
  private InitializeMessage b;
  private String c;
  private long d;
  private boolean e;

  private void a()
  {
    new a(this).execute(new Object[0]);
  }

  private void a(SharedPreferences.Editor paramEditor)
  {
    if ((this.b.getAuthId() != null) && (!this.b.getAuthId().equals("")))
      paramEditor.putString("auth_id", this.b.getAuthId());
  }

  private void a(Ads paramAds)
  {
    SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(getBaseContext()).edit();
    localEditor.putInt("adInit", paramAds.getInit().getNetworkId());
    localEditor.putString("adInitKey", paramAds.getInit().getNetworkKey());
    localEditor.putString("adInitUnit", paramAds.getInit().getUnitId());
    localEditor.putInt("adMain", paramAds.getHome().getNetworkId());
    localEditor.putString("adMainKey", paramAds.getHome().getNetworkKey());
    localEditor.putString("adMainUnit", paramAds.getHome().getUnitId());
    localEditor.putInt("adList", paramAds.getList().getNetworkId());
    localEditor.putString("adListKey", paramAds.getList().getNetworkKey());
    localEditor.putString("adListUnit", paramAds.getList().getUnitId());
    localEditor.putInt("adListTop", paramAds.getListTop().getNetworkId());
    localEditor.putString("adListTopKey", paramAds.getListTop().getNetworkKey());
    localEditor.putString("adListTopUnit", paramAds.getListTop().getUnitId());
    localEditor.putInt("adListBottom", paramAds.getListBottom().getNetworkId());
    localEditor.putString("adListBottomKey", paramAds.getListBottom().getNetworkKey());
    localEditor.putString("adListBottomUnit", paramAds.getListBottom().getUnitId());
    localEditor.putInt("adStation", paramAds.getDetails().getNetworkId());
    localEditor.putString("adStationKey", paramAds.getDetails().getNetworkKey());
    localEditor.putString("adStationUnit", paramAds.getDetails().getUnitId());
    localEditor.putInt("adProfile", paramAds.getProfile().getNetworkId());
    localEditor.putString("adProfileKey", paramAds.getProfile().getNetworkKey());
    localEditor.putString("adProfileUnit", paramAds.getProfile().getUnitId());
    localEditor.putInt("adFavorites", paramAds.getFavorites().getNetworkId());
    localEditor.putString("adFavoritesKey", paramAds.getFavorites().getNetworkKey());
    localEditor.putString("adFavoritesUnit", paramAds.getFavorites().getUnitId());
    localEditor.putInt("adInitTime", paramAds.getInitTime());
    localEditor.putInt("adListScrollTime", paramAds.getListScrollTime());
    localEditor.putInt("adListWaitTime", paramAds.getListWaitTime());
    localEditor.commit();
  }

  private void b()
  {
    Location localLocation = getLastKnownLocation();
    Ads localAds;
    if (localLocation != null)
    {
      this.myLocation = localLocation;
      Type localType = new a(this).getType();
      this.mResponseObject = new InitQuery(this, this.mPrefs, localType, localLocation).getResponseObject();
      this.b = ((InitializeMessage)this.mResponseObject.getPayload());
      if (this.b == null)
        break label179;
      SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(getBaseContext()).edit();
      a(localEditor);
      b(localEditor);
      localEditor.commit();
      if (this.b.getAds() == null)
        break label167;
      localAds = this.b.getAds();
      label117: a(localAds);
      if (this.b.getGSA() != null)
        GBApplication.getInstance().setAdsGSA(this.b.getGSA());
      GBApplication.getInstance().setImageServer("http://images.gasbuddy.com/b/");
    }
    while (true)
    {
      g();
      return;
      localLocation = getLastLocationStored();
      break;
      label167: localAds = new Ads();
      break label117;
      label179: if ((this.c == null) || (this.c.equals("")))
        d();
    }
  }

  private void b(SharedPreferences.Editor paramEditor)
  {
    if ((this.b.getHost() != null) && (!this.b.getHost().equals("")));
    for (String str = this.b.getHost(); ; str = getString(2131296733))
    {
      paramEditor.putString("host", str);
      return;
    }
  }

  private String c()
  {
    try
    {
      String str = FieldEncryption.encode64(FieldEncryption.encryptTripleDES(String.valueOf(-11)));
      return str;
    }
    catch (Exception localException)
    {
      showMessage(getString(2131296280));
      finish();
    }
    return "";
  }

  private void d()
  {
    SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(getBaseContext()).edit();
    localEditor.putString("auth_id", c());
    localEditor.commit();
  }

  private void e()
  {
    g();
    SharedPreferences.Editor localEditor = this.mPrefs.edit();
    if (!this.mPrefs.contains("fuelPreference"))
      localEditor.putString("fuelPreference", getString(2131296712));
    if (!this.mPrefs.contains("noPricesPreference"))
      localEditor.putBoolean("noPricesPreference", true);
    if (!this.mPrefs.contains("screenPreference"))
      localEditor.putString("screenPreference", "home");
    if (!this.mPrefs.contains("nearmePreference"))
      localEditor.putString("nearmePreference", "cheap");
    if (!this.mPrefs.contains("radiusPreference"))
      localEditor.putInt("radiusPreference", 5);
    if (!this.mPrefs.contains("auth_id"))
      localEditor.putString("auth_id", "");
    if (!this.mPrefs.contains("is_member"))
      localEditor.putBoolean("is_member", false);
    if (!this.mPrefs.contains("initial_screen_date"))
      localEditor.putLong("initial_screen_date", 0L);
    if (!this.mPrefs.contains("largeScreenBoolean"))
      localEditor.putBoolean("largeScreenBoolean", f());
    boolean bool2;
    if (!this.mPrefs.contains("customKeyboardPreference"))
    {
      boolean bool1 = f();
      bool2 = false;
      if (!bool1)
        break label314;
    }
    while (true)
    {
      localEditor.putBoolean("customKeyboardPreference", bool2);
      localEditor.commit();
      cleanImageCache("Awards");
      return;
      label314: bool2 = true;
    }
  }

  private boolean f()
  {
    if (Integer.parseInt(Build.VERSION.SDK) < 4);
    while (true)
    {
      return false;
      try
      {
        int i = Configuration.class.getField("screenLayout").getInt(getResources().getConfiguration());
        int j = Configuration.class.getField("SCREENLAYOUT_SIZE_MASK").getInt(null);
        if ((i & j) == 4)
          return true;
      }
      catch (Exception localException)
      {
        localException.printStackTrace();
      }
    }
    return false;
  }

  private void g()
  {
    this.c = this.mPrefs.getString("auth_id", "");
    this.a = (1000 * this.mPrefs.getInt("adInitTime", 1));
  }

  private void h()
  {
    String str = this.mPrefs.getString("screenPreference", "");
    if (str.equals("nearme"))
    {
      Location localLocation = getLastKnownLocation();
      if ((localLocation != null) && (localLocation.getLatitude() != 0.0D) && (localLocation.getLongitude() != 0.0D))
      {
        k();
        return;
      }
      i();
      showMessage(getString(2131296279));
    }
    while (true)
    {
      finish();
      return;
      if (str.equals("home"))
      {
        i();
      }
      else
      {
        if (!str.equals("favorites"))
          break;
        i();
        if (!this.mPrefs.getString("member_id", "").equals(""))
          showFavourites();
      }
    }
  }

  private void i()
  {
    startActivity(new GBIntent(this, MainScreen.class, this.myLocation));
  }

  private void j()
  {
    File[] arrayOfFile = new File(Environment.getExternalStorageDirectory() + "/Android/data/gbis.gbandroid/cache/Brands").listFiles();
    if ((arrayOfFile == null) || (arrayOfFile.length == 0))
      startService(new GBIntent(this, BrandsDownloader.class, this.myLocation));
  }

  private void k()
  {
    new b(this).execute(new Object[0]);
  }

  private void l()
  {
    Type localType = new b(this).getType();
    ListQuery localListQuery = new ListQuery(this, this.mPrefs, localType, this.myLocation);
    this.myLocation = getLastKnownLocation();
    if (this.myLocation != null);
    for (GeoPoint localGeoPoint = new GeoPoint((int)(1000000.0D * this.myLocation.getLatitude()), (int)(1000000.0D * this.myLocation.getLongitude())); ; localGeoPoint = new GeoPoint(0, 0))
    {
      this.mResponseObject = localListQuery.getResponseObject("", "", "", localGeoPoint);
      return;
    }
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    DateUtils.compareDateToNow(new Date(System.currentTimeMillis() - 3600000L), 0);
    setContentView(2130903092);
    this.d = System.currentTimeMillis();
    e();
    if (!this.mPrefs.getString("auth_id", "").equals(""))
    {
      j();
      this.e = true;
    }
    startGPSService();
    a();
  }

  protected void onResume()
  {
    super.onResume();
  }

  protected void setAdsAfterGPSServiceConnected()
  {
    int i = this.mPrefs.getInt("adInit", 0);
    setAdConfiguration(this.mPrefs.getString("adInitKey", ""), 1, this.mPrefs.getString("adInitUnit", ""), i);
  }

  protected String setAnalyticsPageName()
  {
    return getString(2131296810);
  }

  protected boolean trackAnalytics()
  {
    return false;
  }

  private final class a extends CustomAsyncTask
  {
    public a(GBActivity arg2)
    {
      super();
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      if (paramBoolean.booleanValue())
      {
        if (InitScreen.a(InitScreen.this).getAction() != 1)
        {
          if (!InitScreen.b(InitScreen.this))
            InitScreen.c(InitScreen.this);
          InitScreen.d(InitScreen.this);
          return;
        }
        InitScreen.this.finish();
        return;
      }
      InitScreen.this.finish();
    }

    protected final boolean queryWebService()
    {
      InitScreen.e(InitScreen.this);
      long l = System.currentTimeMillis();
      if (l - InitScreen.f(InitScreen.this) < InitScreen.g(InitScreen.this))
        SystemClock.sleep(InitScreen.g(InitScreen.this) - (l - InitScreen.f(InitScreen.this)));
      if (InitScreen.a(InitScreen.this) == null)
      {
        InitScreen.this.setMessage(InitScreen.this.getString(2131296282));
        return false;
      }
      return true;
    }
  }

  private final class b extends CustomAsyncTask
  {
    public b(GBActivity arg2)
    {
      super();
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      InitScreen.h(InitScreen.this);
      if (paramBoolean.booleanValue())
      {
        ListResults localListResults = (ListResults)InitScreen.i(InitScreen.this).getPayload();
        InitScreen.a(InitScreen.this, "", "", "", localListResults);
      }
      InitScreen.this.finish();
    }

    protected final boolean queryWebService()
    {
      InitScreen.j(InitScreen.this);
      return true;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.InitScreen
 * JD-Core Version:    0.6.2
 */