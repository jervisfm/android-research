package gbis.gbandroid.activities.awards;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableLayout.LayoutParams;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import gbis.gbandroid.activities.base.GBActivity;
import gbis.gbandroid.entities.AwardsMessage;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.queries.AwardDetailsQuery;
import gbis.gbandroid.utils.CustomAsyncTask;
import gbis.gbandroid.utils.DateUtils;
import gbis.gbandroid.utils.ImageLoader;
import gbis.gbandroid.utils.ImageUtils;
import java.lang.reflect.Type;
import java.util.List;

public class AwardDetails extends GBActivity
{
  private AwardsMessage a;
  private List<AwardsMessage> b;
  private float c;

  private void a()
  {
    ((TextView)findViewById(2131165218)).setText(this.a.getTitle());
  }

  private void b()
  {
    if ((this.b.size() == 1) || (d()))
      this.a = ((AwardsMessage)this.b.get(0));
    TextView localTextView1;
    ProgressBar localProgressBar;
    TextView localTextView2;
    while (true)
    {
      localTextView1 = (TextView)findViewById(2131165217);
      ImageView localImageView = (ImageView)findViewById(2131165215);
      View localView = findViewById(2131165214);
      localProgressBar = (ProgressBar)findViewById(2131165219);
      localTextView2 = (TextView)findViewById(2131165220);
      TextView localTextView3 = (TextView)findViewById(2131165221);
      ImageLoader localImageLoader = new ImageLoader(this, 2130837604, "/Android/data/gbis.gbandroid/cache/Awards");
      localView.setBackgroundDrawable(new ShapeDrawable(new a(this, localView)));
      localTextView3.setText(((AwardsMessage)this.b.get(0)).getMessage());
      String str1;
      label167: String str3;
      if (this.a.getAwarded() == 1)
      {
        str1 = "on";
        String str2 = str1 + "big.png";
        str3 = getString(2131296781) + ImageUtils.getResolutionInText(this.c) + "/" + this.a.getAwardId() + "/" + str2;
        localImageView.setTag(String.valueOf(str3.hashCode()));
      }
      try
      {
        if (!str3.equals(""))
          localImageLoader.displayImage(str3, this, localImageView, false, 0);
        while (true)
        {
          localImageView.setVisibility(0);
          if (this.a.getAwarded() != 1)
            break label630;
          ShapeDrawable localShapeDrawable1 = new ShapeDrawable(new b(this, localTextView1));
          String str4 = getString(2131296459);
          Object[] arrayOfObject1 = new Object[1];
          arrayOfObject1[0] = Integer.valueOf(this.a.getLevel());
          localTextView1.setText(String.format(str4, arrayOfObject1));
          localTextView1.setBackgroundDrawable(localShapeDrawable1);
          c();
          if (d())
            break label615;
          int i = Math.round(100.0F * ((AwardsMessage)this.b.get(0)).getTotalTimes() / ((AwardsMessage)this.b.get(0)).getActionCount());
          String str5 = getString(2131296456);
          Object[] arrayOfObject2 = new Object[2];
          arrayOfObject2[0] = Integer.valueOf(i);
          arrayOfObject2[1] = Integer.valueOf(((AwardsMessage)this.b.get(0)).getLevel());
          localTextView2.setText(String.format(str5, arrayOfObject2));
          localProgressBar.setProgress(i);
          ShapeDrawable localShapeDrawable2 = new ShapeDrawable(new c(this, localProgressBar));
          d locald = new d(this, localShapeDrawable2, localProgressBar);
          LayerDrawable localLayerDrawable = new LayerDrawable(new Drawable[] { localShapeDrawable2, locald, locald });
          localLayerDrawable.setId(0, 16908288);
          localLayerDrawable.setId(1, 16908303);
          localLayerDrawable.setId(2, 16908301);
          localProgressBar.setProgressDrawable(localLayerDrawable);
          return;
          this.a = ((AwardsMessage)this.b.get(1));
          break;
          str1 = "off";
          break label167;
          localImageView.setImageResource(2130837605);
        }
      }
      catch (Exception localException)
      {
        while (true)
          localException.printStackTrace();
        label615: localProgressBar.setVisibility(8);
        localTextView2.setVisibility(8);
        return;
      }
    }
    label630: localProgressBar.setVisibility(4);
    localTextView2.setVisibility(4);
    localTextView1.setVisibility(4);
  }

  private void c()
  {
    int i;
    int j;
    TableLayout localTableLayout;
    String str1;
    if (!d())
    {
      i = 1;
      j = this.b.size();
      if (j > i)
      {
        findViewById(2131165222).setVisibility(0);
        localTableLayout = (TableLayout)findViewById(2131165223);
        str1 = "";
      }
    }
    for (int k = i; ; k++)
    {
      if (k >= j)
      {
        return;
        i = 0;
        break;
      }
      AwardsMessage localAwardsMessage = (AwardsMessage)this.b.get(k);
      TableRow localTableRow = new TableRow(this);
      localTableRow.setLayoutParams(new TableLayout.LayoutParams(-1, -2));
      TextView localTextView1 = new TextView(this);
      TableRow.LayoutParams localLayoutParams = new TableRow.LayoutParams(0);
      localLayoutParams.setMargins(0, 10 * (int)this.c, 10 * (int)this.c, 0);
      localTextView1.setLayoutParams(localLayoutParams);
      localTextView1.setPadding(10 * (int)this.c, 2 * (int)this.c, 10 * (int)this.c, 10 * (int)this.c);
      localTextView1.setTextColor(this.mRes.getColor(2131230757));
      localTextView1.setTextSize(16.0F);
      localTextView1.setGravity(17);
      ShapeDrawable localShapeDrawable = new ShapeDrawable(new e(this, localAwardsMessage, localTextView1));
      String str2 = getString(2131296459);
      Object[] arrayOfObject1 = new Object[1];
      arrayOfObject1[0] = Integer.valueOf(localAwardsMessage.getLevel());
      localTextView1.setText(String.format(str2, arrayOfObject1));
      localTextView1.setBackgroundDrawable(localShapeDrawable);
      TextView localTextView2 = new TextView(this);
      localTextView2.setLayoutParams(new TableRow.LayoutParams(1));
      localTextView2.setTextColor(this.mRes.getColor(2131230757));
      localTextView2.setTextSize(14.0F);
      localTextView2.setSingleLine(false);
      localTextView2.setText(localAwardsMessage.getDescription() + ": " + DateUtils.getDateForWinners(DateUtils.toDateFormat(localAwardsMessage.getAddDate())));
      localTableLayout.addView(localTableRow);
      localTableRow.addView(localTextView1);
      localTableRow.addView(localTextView2);
      StringBuilder localStringBuilder = new StringBuilder(String.valueOf(str1));
      String str3 = getString(2131296457);
      Object[] arrayOfObject2 = new Object[2];
      arrayOfObject2[0] = Integer.valueOf(localAwardsMessage.getLevel());
      arrayOfObject2[1] = localAwardsMessage.getDescription();
      str1 = String.format(str3, arrayOfObject2) + DateUtils.getDateForWinners(DateUtils.toDateFormat(localAwardsMessage.getAddDate())) + "\n";
    }
  }

  private boolean d()
  {
    return ((AwardsMessage)this.b.get(0)).getActionCount() <= ((AwardsMessage)this.b.get(0)).getTotalTimes();
  }

  private void e()
  {
    Type localType = new f(this).getType();
    this.mResponseObject = new AwardDetailsQuery(this, this.mPrefs, localType, this.myLocation).getResponseObject(this.a.getAwardId());
    this.b = ((List)this.mResponseObject.getPayload());
  }

  private void f()
  {
    a locala = new a(this);
    locala.execute(new Object[0]);
    loadDialog(getString(2131296405), locala);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903048);
    this.c = getDensity();
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
      this.a = ((AwardsMessage)localBundle.getParcelable("award"));
    while (true)
    {
      f();
      a();
      return;
      finish();
    }
  }

  protected void setAdsAfterGPSServiceConnected()
  {
  }

  protected String setAnalyticsPageName()
  {
    return getString(2131296829);
  }

  private final class a extends CustomAsyncTask
  {
    public a(GBActivity arg2)
    {
      super();
    }

    protected final void onCancelled()
    {
      super.onCancelled();
      AwardDetails.this.finish();
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      try
      {
        AwardDetails.a(AwardDetails.this).dismiss();
        label15: if (paramBoolean.booleanValue())
        {
          AwardDetails.b(AwardDetails.this);
          return;
        }
        AwardDetails.this.finish();
        return;
      }
      catch (Exception localException)
      {
        break label15;
      }
    }

    protected final boolean queryWebService()
    {
      AwardDetails.c(AwardDetails.this);
      return true;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.awards.AwardDetails
 * JD-Core Version:    0.6.2
 */