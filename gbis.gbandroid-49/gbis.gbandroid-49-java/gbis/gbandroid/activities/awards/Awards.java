package gbis.gbandroid.activities.awards;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.drawable.ShapeDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import gbis.gbandroid.activities.base.GBActivity;
import gbis.gbandroid.content.GBIntent;
import gbis.gbandroid.entities.AwardsMessage;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.queries.AwardsListQuery;
import gbis.gbandroid.utils.CustomAsyncTask;
import gbis.gbandroid.utils.ImageLoader;
import gbis.gbandroid.utils.ImageUtils;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class Awards extends GBActivity
  implements AdapterView.OnItemClickListener
{
  public static final String AWARDS_DIRECTORY = "/Android/data/gbis.gbandroid/cache/Awards";
  private List<AwardsMessage> a;
  private float b;

  private void a()
  {
    ((TextView)findViewById(2131165226)).setText(getString(2131296653));
    GridView localGridView = (GridView)findViewById(2131165227);
    localGridView.setAdapter(new a(this, new ArrayList()));
    localGridView.setOnItemClickListener(this);
  }

  private void a(long paramLong)
  {
    SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(getBaseContext()).edit();
    localEditor.putLong("awards_cached_images_time", paramLong);
    localEditor.commit();
  }

  private void a(AwardsMessage paramAwardsMessage)
  {
    GBIntent localGBIntent = new GBIntent(this, AwardDetails.class, this.myLocation);
    localGBIntent.putExtra("award", paramAwardsMessage);
    startActivityForResult(localGBIntent, 1);
  }

  private void b()
  {
    GridView localGridView = (GridView)findViewById(2131165227);
    localGridView.setAdapter(new a(this, this.a));
    localGridView.setOnItemClickListener(this);
  }

  private void c()
  {
    Type localType = new g(this).getType();
    this.mResponseObject = new AwardsListQuery(this, this.mPrefs, localType, this.myLocation).getResponseObject();
    this.a = ((List)this.mResponseObject.getPayload());
  }

  private void d()
  {
    b localb = new b(this);
    localb.execute(new Object[0]);
    loadDialog(getString(2131296405), localb);
  }

  private boolean e()
  {
    Long localLong = Long.valueOf(System.currentTimeMillis());
    if (localLong.longValue() - this.mPrefs.getLong("awards_cached_images_time", localLong.longValue()) > 2592000000L)
    {
      a(localLong.longValue());
      return true;
    }
    return false;
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903050);
    this.b = getDensity();
    a();
    if (this.mPrefs.getLong("awards_cached_images_time", 0L) == 0L)
      a(System.currentTimeMillis());
    d();
  }

  protected void onDestroy()
  {
    super.onDestroy();
    if (e())
      cleanImageCache("/Android/data/gbis.gbandroid/cache/Awards");
  }

  public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    setAnalyticsTrackEventScreenButton(((AwardsMessage)paramAdapterView.getItemAtPosition(paramInt)).getTitle());
    a((AwardsMessage)paramAdapterView.getItemAtPosition(paramInt));
  }

  protected void setAdsAfterGPSServiceConnected()
  {
  }

  protected String setAnalyticsPageName()
  {
    return getString(2131296828);
  }

  private final class a extends ArrayAdapter<AwardsMessage>
  {
    private int b = 2130903047;
    private List<AwardsMessage> c;
    private ImageLoader d;

    public a(int arg2)
    {
      super(2130903047, localList);
      this.c = localList;
      this.d = new ImageLoader(Awards.this, 2130837604, "/Android/data/gbis.gbandroid/cache/Awards");
    }

    public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      Awards.c localc1;
      if (paramView == null)
      {
        paramView = Awards.a(Awards.this).inflate(this.b, null);
        Awards.c localc2 = new Awards.c();
        localc2.a = ((TextView)paramView.findViewById(2131165216));
        localc2.b = ((TextView)paramView.findViewById(2131165217));
        localc2.c = ((ImageView)paramView.findViewById(2131165215));
        localc2.d = paramView.findViewById(2131165214);
        paramView.setTag(localc2);
        localc1 = localc2;
      }
      while (true)
      {
        AwardsMessage localAwardsMessage = (AwardsMessage)this.c.get(paramInt);
        localc1.a.setText(localAwardsMessage.getTitle());
        ShapeDrawable localShapeDrawable1 = new ShapeDrawable(new h(this, localAwardsMessage, localc1));
        localc1.d.setBackgroundDrawable(localShapeDrawable1);
        label247: String str1;
        label259: String str3;
        if (localAwardsMessage.getLevel() > 0)
        {
          TextView localTextView = localc1.b;
          String str4 = Awards.this.getString(2131296459);
          Object[] arrayOfObject = new Object[1];
          arrayOfObject[0] = Integer.valueOf(localAwardsMessage.getLevel());
          localTextView.setText(String.format(str4, arrayOfObject));
          ShapeDrawable localShapeDrawable2 = new ShapeDrawable(new i(this, localAwardsMessage, localc1));
          localc1.b.setBackgroundDrawable(localShapeDrawable2);
          localc1.b.setVisibility(0);
          if (localAwardsMessage.getLevel() <= 0)
            break label413;
          str1 = "on";
          String str2 = str1 + "small.png";
          str3 = Awards.this.getString(2131296781) + ImageUtils.getResolutionInText(Awards.b(Awards.this)) + "/" + localAwardsMessage.getAwardId() + "/" + str2;
          localc1.c.setTag(String.valueOf(str3.hashCode()));
        }
        try
        {
          if (!str3.equals(""))
          {
            this.d.displayImage(str3, Awards.this, localc1.c, false, 0);
            return paramView;
            localc1 = (Awards.c)paramView.getTag();
            continue;
            localc1.b.setVisibility(4);
            break label247;
            label413: str1 = "off";
            break label259;
          }
          else
          {
            localc1.c.setImageResource(2130837604);
            return paramView;
          }
        }
        catch (Exception localException)
        {
          localException.printStackTrace();
        }
      }
      return paramView;
    }
  }

  private final class b extends CustomAsyncTask
  {
    public b(GBActivity arg2)
    {
      super();
    }

    protected final void onCancelled()
    {
      super.onCancelled();
      Awards.this.finish();
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      try
      {
        Awards.c(Awards.this).dismiss();
        label15: if (paramBoolean.booleanValue())
        {
          Awards.d(Awards.this);
          Awards.e(Awards.this);
          return;
        }
        Awards.this.finish();
        return;
      }
      catch (Exception localException)
      {
        break label15;
      }
    }

    protected final boolean queryWebService()
    {
      Awards.f(Awards.this);
      return true;
    }
  }

  static final class c
  {
    TextView a;
    TextView b;
    ImageView c;
    View d;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.awards.Awards
 * JD-Core Version:    0.6.2
 */