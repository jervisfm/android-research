package gbis.gbandroid.activities.awards;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.shapes.Shape;
import android.widget.TextView;
import gbis.gbandroid.entities.AwardsMessage;

final class b extends Shape
{
  b(AwardDetails paramAwardDetails, TextView paramTextView)
  {
  }

  public final void draw(Canvas paramCanvas, Paint paramPaint)
  {
    paramPaint.setColor(Color.parseColor(AwardDetails.d(this.a).getColor()));
    paramPaint.setAntiAlias(true);
    paramPaint.setShadowLayer(1.0F, 0.0F, 3.0F, -16777216);
    paramCanvas.drawRoundRect(new RectF(0.0F, 0.0F, this.b.getMeasuredWidth(), -3 + this.b.getMeasuredHeight()), 10.0F, 10.0F, paramPaint);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.awards.b
 * JD-Core Version:    0.6.2
 */