package gbis.gbandroid.activities.awards;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.shapes.Shape;
import android.widget.ProgressBar;

final class c extends Shape
{
  c(AwardDetails paramAwardDetails, ProgressBar paramProgressBar)
  {
  }

  public final void draw(Canvas paramCanvas, Paint paramPaint)
  {
    paramPaint.setColor(Color.parseColor("#6d6e71"));
    paramPaint.setAntiAlias(true);
    paramCanvas.drawRoundRect(new RectF(0.0F, 0.0F, this.b.getMeasuredWidth(), this.b.getMeasuredHeight()), 10.0F, 10.0F, paramPaint);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.awards.c
 * JD-Core Version:    0.6.2
 */