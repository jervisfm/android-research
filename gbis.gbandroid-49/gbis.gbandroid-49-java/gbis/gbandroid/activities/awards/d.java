package gbis.gbandroid.activities.awards;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.Drawable;
import android.widget.ProgressBar;
import gbis.gbandroid.entities.AwardsMessage;

final class d extends ClipDrawable
{
  d(AwardDetails paramAwardDetails, Drawable paramDrawable, ProgressBar paramProgressBar)
  {
    super(paramDrawable, 3, 1);
  }

  public final void draw(Canvas paramCanvas)
  {
    super.draw(paramCanvas);
    Paint localPaint = new Paint();
    localPaint.setColor(-16777216);
    localPaint.setAntiAlias(true);
    paramCanvas.drawRoundRect(new RectF(0.0F, 0.0F, this.b.getMeasuredWidth() * this.b.getProgress() / 100, this.b.getMeasuredHeight()), 10.0F, 10.0F, localPaint);
    localPaint.setColor(Color.parseColor(AwardDetails.d(this.a).getColor()));
    paramCanvas.drawRoundRect(new RectF(0.5F, 0.5F, -1 + this.b.getMeasuredWidth() * this.b.getProgress() / 100, -1 + this.b.getMeasuredHeight()), 9.5F, 9.5F, localPaint);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.awards.d
 * JD-Core Version:    0.6.2
 */