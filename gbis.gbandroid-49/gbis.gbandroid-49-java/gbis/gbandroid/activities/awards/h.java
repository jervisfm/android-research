package gbis.gbandroid.activities.awards;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.shapes.Shape;
import android.view.View;
import gbis.gbandroid.entities.AwardsMessage;

final class h extends Shape
{
  h(Awards.a parama, AwardsMessage paramAwardsMessage, Awards.c paramc)
  {
  }

  public final void draw(Canvas paramCanvas, Paint paramPaint)
  {
    paramPaint.setColor(Color.parseColor(this.b.getColor()));
    paramPaint.setAntiAlias(true);
    paramPaint.setShadowLayer(1.0F, 0.0F, 3.0F, -16777216);
    paramCanvas.drawCircle(this.c.d.getMeasuredWidth() / 2, this.c.d.getMeasuredHeight() / 2, 42.0F * Awards.b(Awards.a.a(this.a)), paramPaint);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.awards.h
 * JD-Core Version:    0.6.2
 */