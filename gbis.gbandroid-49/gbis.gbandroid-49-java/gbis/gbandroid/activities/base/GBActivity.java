package gbis.gbandroid.activities.base;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.amazon.device.ads.AdError;
import com.amazon.device.ads.AdLayout;
import com.amazon.device.ads.AdLayout.AdSize;
import com.amazon.device.ads.AdProperties;
import com.amazon.device.ads.AdRegistration;
import com.amazon.device.ads.AdTargetingOptions;
import com.google.ads.Ad;
import com.google.ads.AdRequest;
import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.google.ads.doubleclick.DfpExtras;
import com.google.ads.searchads.SearchAdRequest;
import com.google.ads.searchads.SearchAdRequest.BorderType;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import gbis.gbandroid.GBApplication;
import gbis.gbandroid.activities.favorites.Favourites;
import gbis.gbandroid.activities.favorites.PlacesList;
import gbis.gbandroid.activities.list.ListStations;
import gbis.gbandroid.activities.map.MapStationDialog;
import gbis.gbandroid.activities.members.MemberLogin;
import gbis.gbandroid.activities.members.MemberRegistration;
import gbis.gbandroid.activities.search.MainScreen;
import gbis.gbandroid.activities.search.SearchBar;
import gbis.gbandroid.activities.settings.Settings;
import gbis.gbandroid.activities.settings.ShareApp;
import gbis.gbandroid.activities.station.AddStationMap;
import gbis.gbandroid.activities.station.StationPhotoUploader;
import gbis.gbandroid.content.GBIntent;
import gbis.gbandroid.entities.AdsGSA;
import gbis.gbandroid.entities.AdsGSA.BackgroundGradient;
import gbis.gbandroid.entities.AwardsMessage;
import gbis.gbandroid.entities.Center;
import gbis.gbandroid.entities.ListResults;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.SmartPrompt;
import gbis.gbandroid.entities.Station;
import gbis.gbandroid.entities.requests.RequestMemberPreference;
import gbis.gbandroid.listeners.MyLocationChangedListener;
import gbis.gbandroid.queries.MemberPreferenceQuery;
import gbis.gbandroid.queries.SmartPromptResponseQuery;
import gbis.gbandroid.services.GPSService;
import gbis.gbandroid.utils.ConnectionUtils;
import gbis.gbandroid.utils.CustomAsyncTask;
import gbis.gbandroid.utils.DBHelper;
import gbis.gbandroid.views.CustomDialog;
import gbis.gbandroid.views.CustomDialog.Builder;
import gbis.gbandroid.views.CustomProgressDialog.Builder;
import gbis.gbandroid.views.CustomToast;
import gbis.gbandroid.views.CustomToastAward;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public abstract class GBActivity extends Activity
  implements com.amazon.device.ads.AdListener, com.google.ads.AdListener, MyLocationChangedListener
{
  protected static final int ACTIVITY_CREATE = 1;
  protected static final int ADS_SUPPLIER_ADMOB = 2;
  protected static final int ADS_SUPPLIER_AMAZON = 16;
  protected static final int ADS_SUPPLIER_CANWEST = 17;
  protected static final int ADS_SUPPLIER_DFP = 1;
  protected static final int ADS_SUPPLIER_DFP_AND_SEARCH_ADS = 7;
  protected static final int ADS_SUPPLIER_OFF = 0;
  protected static final int ADS_SUPPLIER_PONTIFLEX = 10;
  protected static final int ADS_SUPPLIER_SEARCH_ADS = 6;
  protected static final int ADS_SUPPLIER_XAD = 5;
  protected static final int AD_MEDIUM_BANNER = 1;
  protected static final int AD_STANDARD_BANNER = 0;
  protected static final int FILTER_STATIONS = 5;
  protected static final int MEMBER_LOGGED_IN = 6;
  protected static final int REPORT_PRICE = 3;
  public static final String RESULT = "result";
  protected static final int RESULT_REPORT = 3;
  protected static final int SEARCH_PERFORMED = 4;
  public static final String WIDGET_CALL = "WidgetCall";
  private DBHelper a;
  protected View adBanner;
  protected boolean adsVisible;
  private List<EditText> b;
  private GPSService c;
  private ServiceConnection d = new a(this);
  protected boolean gpsCorrupted;
  protected LayoutInflater mInflater;
  protected SharedPreferences mPrefs;
  protected Resources mRes;
  protected ResponseMessage<?> mResponseObject;
  protected GoogleAnalyticsTracker mTracker;
  protected Location myLocation;
  protected Dialog progress;
  protected CustomToast unstackedToast;
  protected String userMessage;

  private static AdSize a(int paramInt)
  {
    AdSize localAdSize = AdSize.BANNER;
    switch (paramInt)
    {
    default:
      return localAdSize;
    case 0:
      return AdSize.BANNER;
    case 1:
    }
    return AdSize.IAB_MRECT;
  }

  private void a()
  {
    new a(this).execute(new Object[0]);
  }

  private void a(int paramInt, String paramString, boolean paramBoolean)
  {
    new b(this, paramInt, paramString, paramBoolean).execute(new Object[0]);
    if (!this.mPrefs.getBoolean("smartPromptPreference", true))
      a();
  }

  private void a(AwardsMessage paramAwardsMessage)
  {
    new CustomToastAward(this, paramAwardsMessage, 1).show();
  }

  private void a(ListResults paramListResults)
  {
    try
    {
      Geocoder localGeocoder = new Geocoder(this, Locale.getDefault());
      Center localCenter = paramListResults.getCenter();
      List localList = localGeocoder.getFromLocation(localCenter.getLatitude(), localCenter.getLongitude(), 1);
      if (localList.size() > 0)
      {
        String str = ((Address)localList.get(0)).getCountryCode().toUpperCase();
        if ((!str.equals("US")) && (!str.equals("CA")))
          setMessage(getString(2131296288));
      }
      else
      {
        setMessage(getString(2131296288));
        return;
      }
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
  }

  private void a(String paramString, DfpExtras paramDfpExtras, RelativeLayout.LayoutParams paramLayoutParams, ViewGroup paramViewGroup, Location paramLocation, AdSize paramAdSize)
  {
    this.adBanner = new AdView(this, paramAdSize, paramString);
    paramLayoutParams.addRule(14, -1);
    this.adBanner.setVisibility(4);
    paramViewGroup.addView(this.adBanner, paramLayoutParams);
    AdRequest localAdRequest = new AdRequest();
    if (paramLocation != null)
      localAdRequest.setLocation(paramLocation);
    ((AdView)this.adBanner).loadAd(localAdRequest);
    ((AdView)this.adBanner).setAdListener(this);
    localAdRequest.setNetworkExtras(paramDfpExtras);
  }

  private void a(boolean paramBoolean)
  {
    int j;
    if (this.b != null)
    {
      int i = this.b.size();
      j = 0;
      if (j < i);
    }
    else
    {
      return;
    }
    if (paramBoolean)
      ((EditText)this.b.get(j)).setHintTextColor(this.mRes.getColor(2131230784));
    while (true)
    {
      j++;
      break;
      ((EditText)this.b.get(j)).setHintTextColor(this.mRes.getColor(2131230720));
    }
  }

  private void b()
  {
    AdTargetingOptions localAdTargetingOptions = new AdTargetingOptions().enableGeoLocation(true);
    ((AdLayout)this.adBanner).setListener(this);
    ((AdLayout)this.adBanner).loadAd(localAdTargetingOptions);
  }

  private void b(int paramInt, String paramString, boolean paramBoolean)
  {
    Type localType = new j(this).getType();
    this.mResponseObject = new SmartPromptResponseQuery(this, this.mPrefs, localType, this.myLocation).getResponseObject(paramInt, paramString, paramBoolean);
  }

  private void c()
  {
    if ((this.mTracker != null) && (trackAnalytics()))
    {
      this.mTracker.trackPageView(setAnalyticsPageName());
      Log.i("Analytics", setAnalyticsPageName());
      setAnalyticsCustomVariables();
    }
  }

  private void d()
  {
    Type localType = new b(this).getType();
    this.mResponseObject = new MemberPreferenceQuery(this, this.mPrefs, localType, this.myLocation).getResponseObject(e());
  }

  private List<RequestMemberPreference> e()
  {
    ArrayList localArrayList = new ArrayList();
    RequestMemberPreference localRequestMemberPreference = new RequestMemberPreference();
    localRequestMemberPreference.setName("SmartPrompt");
    localRequestMemberPreference.setValue(this.mPrefs.getBoolean("smartPromptPreference", true));
    localArrayList.add(localRequestMemberPreference);
    return localArrayList;
  }

  public static String getVersion(Context paramContext)
  {
    try
    {
      String str = paramContext.getPackageManager().getPackageInfo("gbis.gbandroid", 0).versionName;
      return str;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      localNameNotFoundException.printStackTrace();
    }
    return "";
  }

  protected void OnSmartPromptPressedNo(DialogInterface paramDialogInterface, int paramInt, boolean paramBoolean)
  {
    paramDialogInterface.dismiss();
    a(paramInt, "N", paramBoolean);
  }

  protected void OnSmartPromptPressedYes(DialogInterface paramDialogInterface, int paramInt, boolean paramBoolean)
  {
    paramDialogInterface.dismiss();
    a(paramInt, "Y", paramBoolean);
  }

  protected boolean addBrand(int paramInt1, int paramInt2)
  {
    return this.a.addBrandRow(paramInt1, paramInt2, "/Android/data/gbis.gbandroid/cache/Brands");
  }

  protected boolean addPlace(String paramString1, String paramString2, double paramDouble1, double paramDouble2)
  {
    return this.a.addSearchesRow(paramString1, paramString2, paramDouble1, paramDouble2);
  }

  protected void addToEditTextList(EditText paramEditText)
  {
    if (this.b == null)
      this.b = new ArrayList();
    this.b.add(paramEditText);
  }

  protected void adjustAdsInConfiguration(Configuration paramConfiguration)
  {
    if ((paramConfiguration.orientation == 2) || (paramConfiguration.orientation == 3))
      hideAds();
    while (paramConfiguration.orientation != 1)
      return;
    showAds();
  }

  public void checkAwardAndShow()
  {
    List localList;
    int i;
    if (this.mResponseObject != null)
    {
      localList = this.mResponseObject.getAwards();
      if (localList != null)
      {
        i = localList.size();
        if (i <= 0);
      }
    }
    for (int j = 0; ; j++)
    {
      if (j >= i)
        return;
      AwardsMessage localAwardsMessage = (AwardsMessage)localList.get(j);
      if (localAwardsMessage.getAwarded() == 1)
      {
        a(localAwardsMessage);
        setAwardBadgeFlag(true);
      }
    }
  }

  protected boolean checkConnection()
  {
    return ConnectionUtils.isConnectedToAny(this);
  }

  public void checkSmartPromptAndShow()
  {
    List localList;
    int i;
    if (this.mResponseObject != null)
    {
      localList = this.mResponseObject.getSmartPrompts();
      if ((localList != null) && (isSmartPromptOn()))
      {
        i = localList.size();
        if (i <= 0);
      }
    }
    for (int j = 0; ; j++)
    {
      if (j >= i)
        return;
      SmartPrompt localSmartPrompt = (SmartPrompt)localList.get(j);
      if (localSmartPrompt.getPromptId() >= 0)
        showSmartPrompt(localSmartPrompt);
    }
  }

  protected void cleanImageCache()
  {
    cleanImageCache("/Android/data/gbis.gbandroid/cache/LazyList");
  }

  protected void cleanImageCache(String paramString)
  {
    File localFile;
    String[] arrayOfString;
    int i;
    if (Environment.getExternalStorageState().equals("mounted"))
    {
      localFile = new File(Environment.getExternalStorageDirectory(), paramString);
      if (localFile.exists())
        if (localFile.isDirectory())
        {
          arrayOfString = localFile.list();
          i = arrayOfString.length;
        }
    }
    for (int j = 0; ; j++)
    {
      if (j >= i)
      {
        localFile.delete();
        return;
        localFile = getCacheDir();
        break;
      }
      new File(localFile, arrayOfString[j]).delete();
    }
  }

  protected void closeDB()
  {
    if (this.a != null)
      this.a.closeDB();
  }

  protected boolean deleteAllPlaces()
  {
    return this.a.deleteSearchesAllRows();
  }

  protected boolean deletePlace(String paramString)
  {
    return this.a.deleteSearchesRow(paramString);
  }

  protected void fillGSAAdRequestUIParameteres(SearchAdRequest paramSearchAdRequest, AdsGSA paramAdsGSA)
  {
    if (paramAdsGSA != null)
    {
      paramSearchAdRequest.setAnchorTextColor(Color.parseColor(paramAdsGSA.getAnchorTextColor()));
      paramSearchAdRequest.setBackgroundColor(Color.parseColor(paramAdsGSA.getBackgroundColor()));
      paramSearchAdRequest.setBackgroundGradient(Color.parseColor(paramAdsGSA.getBackgroundGradient().getFromColor()), Color.parseColor(paramAdsGSA.getBackgroundGradient().getToColor()));
      paramSearchAdRequest.setBorderColor(Color.parseColor(paramAdsGSA.getBorderColor()));
      paramSearchAdRequest.setBorderThickness(paramAdsGSA.getBorderThickness());
      paramSearchAdRequest.setBorderType(SearchAdRequest.BorderType.SOLID);
      paramSearchAdRequest.setDescriptionTextColor(Color.parseColor(paramAdsGSA.getDescriptionTextColor()));
      paramSearchAdRequest.setFontFace(paramAdsGSA.getFontFace());
      paramSearchAdRequest.setHeaderTextColor(Color.parseColor(paramAdsGSA.getHeaderTextColor()));
      paramSearchAdRequest.setHeaderTextSize(paramAdsGSA.getHeaderTextSize());
    }
  }

  protected GBActivity getActivity()
  {
    return this;
  }

  protected Cursor getAllPlaces(int paramInt)
  {
    return this.a.getAllSearchesRows(paramInt);
  }

  protected Cursor getBrand(int paramInt)
  {
    return this.a.getBrandRow(paramInt);
  }

  protected float getDensity()
  {
    DisplayMetrics localDisplayMetrics = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
    return localDisplayMetrics.density;
  }

  protected ServiceConnection getGPSServiceConnector()
  {
    return this.d;
  }

  protected String getGSAQuery(SearchAdRequest paramSearchAdRequest, AdsGSA paramAdsGSA, String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if (paramString != null)
      localStringBuilder.append(paramString);
    if ((paramAdsGSA != null) && (!paramAdsGSA.getKeywords().equals("")))
    {
      localStringBuilder.append(" ");
      localStringBuilder.append(paramAdsGSA.getKeywords());
      return localStringBuilder.toString();
    }
    String[] arrayOfString1 = this.mRes.getStringArray(2131099648);
    String[] arrayOfString2 = this.mRes.getStringArray(2131099649);
    for (int i = 0; ; i++)
    {
      if (i >= arrayOfString2.length);
      while (arrayOfString2[i].equals(this.mPrefs.getString("fuelPreference", "")))
      {
        localStringBuilder.append(" ");
        localStringBuilder.append(arrayOfString1[i]);
        localStringBuilder.append(" gas prices");
        break;
      }
    }
  }

  public String getHost()
  {
    return this.mPrefs.getString("host", getString(2131296733));
  }

  protected Location getLastKnownLocation()
  {
    if (this.c != null)
      return this.c.getLastKnownLocation();
    return ((GBApplication)getApplication()).getLastKnownLocation();
  }

  protected Location getLastLocationStored()
  {
    Location localLocation = new Location("NoLocation");
    localLocation.setLatitude(this.mPrefs.getFloat("lastLatitude", 0.0F));
    localLocation.setLongitude(this.mPrefs.getFloat("lastLongitude", 0.0F));
    return localLocation;
  }

  protected Cursor getPlace(String paramString)
  {
    return this.a.getSearchesRow(paramString);
  }

  protected Cursor getPlaceFromCityZip(String paramString)
  {
    return this.a.getSearchesRowFromCityZip(paramString);
  }

  public ResponseMessage<?> getResponseObject()
  {
    return this.mResponseObject;
  }

  protected void hideAds()
  {
    if (this.adBanner != null)
      this.adBanner.setVisibility(8);
  }

  protected boolean incrementFrequencyOfPlace(String paramString)
  {
    return this.a.incrementSearchesFrequency(paramString);
  }

  protected boolean isDBOpen()
  {
    if (this.a != null)
      return this.a.isDBOpen();
    return false;
  }

  protected boolean isSmartPromptOn()
  {
    return this.mPrefs.getBoolean("smartPromptPreference", true);
  }

  protected void launchSettings()
  {
    startActivity(new GBIntent(this, Settings.class, this.myLocation));
  }

  protected void loadDialog(String paramString)
  {
    if (!this.progress.isShowing())
    {
      CustomProgressDialog.Builder localBuilder = new CustomProgressDialog.Builder(this);
      localBuilder.setMessage(paramString);
      this.progress = localBuilder.create();
      this.progress.setCancelable(true);
      this.progress.show();
    }
  }

  protected void loadDialog(String paramString, CustomAsyncTask paramCustomAsyncTask)
  {
    if (!this.progress.isShowing())
    {
      CustomProgressDialog.Builder localBuilder = new CustomProgressDialog.Builder(this);
      localBuilder.setMessage(paramString);
      this.progress = localBuilder.create();
      this.progress.setCancelable(true);
      this.progress.setOnCancelListener(new d(this, paramCustomAsyncTask));
      this.progress.show();
    }
  }

  protected void loadDialog(String paramString, Thread paramThread)
  {
    if (!this.progress.isShowing())
    {
      CustomProgressDialog.Builder localBuilder = new CustomProgressDialog.Builder(this);
      localBuilder.setMessage(paramString);
      this.progress = localBuilder.create();
      this.progress.setCancelable(true);
      this.progress.setOnCancelListener(new c(this, paramThread));
      this.progress.show();
    }
  }

  public void onAdCollapsed(AdLayout paramAdLayout)
  {
  }

  public void onAdExpanded(AdLayout paramAdLayout)
  {
  }

  public void onAdFailedToLoad(AdLayout paramAdLayout, AdError paramAdError)
  {
    b();
  }

  public void onAdLoaded(AdLayout paramAdLayout, AdProperties paramAdProperties)
  {
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
    {
      Location localLocation = (Location)localBundle.getParcelable("my location");
      if (localLocation != null)
        this.myLocation = new Location(localLocation);
    }
    if (this.myLocation == null)
      setMyLocationToZeros();
    this.mInflater = ((LayoutInflater)getSystemService("layout_inflater"));
    this.progress = new Dialog(this);
    this.mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
    this.mRes = getResources();
    this.adsVisible = true;
    if (trackAnalytics())
    {
      this.mTracker = GoogleAnalyticsTracker.getInstance();
      if (this.mTracker != null)
        this.mTracker.startNewSession(getString(2131296806), this);
    }
    AdRegistration.setAppUniqueId(this, getString(2131296805));
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    setAnalyticsTrackEvent(setAnalyticsPageName(), "PhoneButton", getString(2131296660), 0);
    return true;
  }

  protected void onDestroy()
  {
    super.onDestroy();
    if (this.mTracker != null)
      this.mTracker.dispatch();
  }

  public void onDismissScreen(Ad paramAd)
  {
  }

  public void onFailedToReceiveAd(Ad paramAd, AdRequest.ErrorCode paramErrorCode)
  {
    hideAds();
  }

  public void onGPSServiceConnected()
  {
    this.myLocation = getLastKnownLocation();
    if (this.myLocation == null)
      setMyLocationToZeros();
    this.c.registerMyLocationChangedListener(this);
    setAdsAfterGPSServiceConnected();
  }

  public void onLeaveApplication(Ad paramAd)
  {
  }

  public void onMyLocationChanged(Location paramLocation)
  {
    if (paramLocation != null)
      this.myLocation = paramLocation;
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    setAnalyticsTrackEvent(setAnalyticsPageName(), "MenuButton", paramMenuItem.getTitle().toString(), 0);
    return true;
  }

  protected void onPause()
  {
    super.onPause();
    stopGPSService();
  }

  public void onPresentScreen(Ad paramAd)
  {
  }

  public void onReceiveAd(Ad paramAd)
  {
    showAds();
  }

  protected void onResume()
  {
    super.onResume();
    startGPSService();
    c();
  }

  public boolean onSearchRequested()
  {
    setAnalyticsTrackEvent(setAnalyticsPageName(), "PhoneButton", getString(2131296605), 0);
    showCityZipSearch();
    return true;
  }

  protected void onSmartPromptCancelled(DialogInterface paramDialogInterface, int paramInt, boolean paramBoolean)
  {
    a(paramInt, "", paramBoolean);
  }

  protected void onSmartPromptPressedDontKnow(DialogInterface paramDialogInterface, int paramInt, boolean paramBoolean)
  {
    paramDialogInterface.dismiss();
    a(paramInt, "?", paramBoolean);
  }

  protected void onStop()
  {
    super.onStop();
    closeDB();
    stopGPSService();
  }

  protected boolean openDB()
  {
    if (this.a == null)
      this.a = new DBHelper(this);
    return this.a.openDB();
  }

  protected void registerMyLocationChangedListener(MyLocationChangedListener paramMyLocationChangedListener)
  {
    this.c.registerMyLocationChangedListener(paramMyLocationChangedListener);
  }

  protected void setAdConfiguration(String paramString1, int paramInt1, String paramString2, int paramInt2)
  {
    setAdConfiguration(paramString1, paramInt1, paramString2, paramInt2, "");
  }

  protected void setAdConfiguration(String paramString1, int paramInt1, String paramString2, int paramInt2, ViewGroup paramViewGroup)
  {
    setAdConfiguration(paramString1, paramInt1, paramString2, paramInt2, paramViewGroup, "");
  }

  protected void setAdConfiguration(String paramString1, int paramInt1, String paramString2, int paramInt2, ViewGroup paramViewGroup, String paramString3)
  {
    String str;
    if (paramInt2 == 0)
    {
      this.adsVisible = false;
      str = paramString1;
    }
    while (true)
    {
      Location localLocation = getLastKnownLocation();
      if ((localLocation == null) || (localLocation.getLatitude() == 0.0D) || (localLocation.getLongitude() == 0.0D))
        localLocation = getLastLocationStored();
      RelativeLayout.LayoutParams localLayoutParams1 = new RelativeLayout.LayoutParams(-2, -2);
      if (this.adsVisible);
      switch (paramInt2)
      {
      case 3:
      case 4:
      case 8:
      case 9:
      case 11:
      case 12:
      case 13:
      case 14:
      case 15:
      default:
        this.adBanner = new View(this);
        RelativeLayout.LayoutParams localLayoutParams5 = new RelativeLayout.LayoutParams(0, 0);
        localLayoutParams5.addRule(14, -1);
        this.adBanner.setVisibility(4);
        paramViewGroup.addView(this.adBanner, localLayoutParams5);
        return;
        if (paramInt2 == 1)
          str = getString(2131296791) + paramString2;
        break;
      case 1:
      case 2:
        a(str, new DfpExtras(), localLayoutParams1, paramViewGroup, localLocation, a(paramInt1));
        return;
      case 17:
        DfpExtras localDfpExtras = new DfpExtras();
        localDfpExtras.addExtra("nk", "prtnr");
        localDfpExtras.addExtra("pr", "gb");
        localDfpExtras.addExtra("cnt", "auto");
        a(str, localDfpExtras, localLayoutParams1, paramViewGroup, localLocation, a(paramInt1));
        return;
      case 5:
        this.adBanner = new WebView(this);
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append(getString(2131296807));
        localStringBuilder.append("?v=1.1");
        localStringBuilder.append("&k=" + getString(2131296808));
        localStringBuilder.append("&uid=1234567890");
        localStringBuilder.append("&size=320x50");
        if (localLocation != null)
        {
          localStringBuilder.append("&lat=" + localLocation.getLatitude());
          localStringBuilder.append("&log=" + localLocation.getLongitude());
        }
        ((WebView)this.adBanner).loadUrl(localStringBuilder.toString());
        this.adBanner.setBackgroundColor(0);
        RelativeLayout.LayoutParams localLayoutParams4 = new RelativeLayout.LayoutParams((int)(320.0F * getDensity()), -2);
        paramViewGroup.addView(this.adBanner, localLayoutParams4);
        return;
      case 6:
        SearchAdRequest localSearchAdRequest = new SearchAdRequest();
        AdsGSA localAdsGSA = GBApplication.getInstance().getAdsGSA();
        localSearchAdRequest.setQuery("denver co shopping male");
        fillGSAAdRequestUIParameteres(localSearchAdRequest, localAdsGSA);
        localSearchAdRequest.setQuery(getGSAQuery(localSearchAdRequest, localAdsGSA, paramString3));
        this.adBanner = new AdView(this, new AdSize(320, 60), str);
        localLayoutParams1.addRule(14, -1);
        this.adBanner.setVisibility(4);
        paramViewGroup.addView(this.adBanner, localLayoutParams1);
        paramViewGroup.getLayoutParams().height = ((int)(60.0F * getDensity()));
        if (localLocation != null)
          localSearchAdRequest.setLocation(localLocation);
        ((AdView)this.adBanner).loadAd(localSearchAdRequest);
        ((AdView)this.adBanner).setAdListener(this);
        return;
      case 7:
        setAdConfiguration(paramString1, paramInt1, paramString2, 6, paramViewGroup, paramString3);
        return;
      case 10:
        this.adBanner = new WebView(this);
        ((WebView)this.adBanner).getSettings().setJavaScriptEnabled(true);
        ((WebView)this.adBanner).loadData("javascript:<script type=\"text/javascript\">\npontiflex_data = {};\npontiflex_ad = {pid: 3425, options: {}};\npontiflex_ad.options.view = \"320x50\";\npontiflex_ad.options.appName = \"Sample App\";\n</script>\n<script type=\"text/javascript\" src=\"http://hhymxdkkdt5c.pflexads.com/html-sdk/mobilewebAds.js\"></script>", "text/html", null);
        this.adBanner.setBackgroundColor(0);
        RelativeLayout.LayoutParams localLayoutParams3 = new RelativeLayout.LayoutParams((int)(320.0F * getDensity()), (int)(50.0F * getDensity()));
        paramViewGroup.addView(this.adBanner, localLayoutParams3);
        return;
      case 16:
        this.adBanner = new AdLayout(this, AdLayout.AdSize.AD_SIZE_320x50);
        RelativeLayout.LayoutParams localLayoutParams2 = new RelativeLayout.LayoutParams((int)(320.0F * getDensity()), -2);
        paramViewGroup.addView(this.adBanner, localLayoutParams2);
        b();
        return;
        str = paramString1;
      }
    }
  }

  protected void setAdConfiguration(String paramString1, int paramInt1, String paramString2, int paramInt2, String paramString3)
  {
    setAdConfiguration(paramString1, paramInt1, paramString2, paramInt2, (RelativeLayout)findViewById(2131165350), paramString3);
  }

  protected void setAdConfiguration(String paramString1, String paramString2, int paramInt)
  {
    setAdConfiguration(paramString1, paramString2, paramInt, "");
  }

  protected void setAdConfiguration(String paramString1, String paramString2, int paramInt, ViewGroup paramViewGroup)
  {
    setAdConfiguration(paramString1, 0, paramString2, paramInt, paramViewGroup);
  }

  protected void setAdConfiguration(String paramString1, String paramString2, int paramInt, String paramString3)
  {
    setAdConfiguration(paramString1, 0, paramString2, paramInt, paramString3);
  }

  protected abstract void setAdsAfterGPSServiceConnected();

  protected void setAnalyticsCustomVariables()
  {
    this.mTracker.setCustomVar(1, "AuthId", this.mPrefs.getString("auth_id", ""));
    this.mTracker.setCustomVar(2, "AppVersion", getVersion(this));
    this.mTracker.setCustomVar(3, "MemberId", this.mPrefs.getString("member_id", ""));
    this.mTracker.setCustomVar(4, "FuelType", this.mPrefs.getString("fuelPreference", ""));
  }

  protected abstract String setAnalyticsPageName();

  protected void setAnalyticsTrackEvent(String paramString1, String paramString2, String paramString3, int paramInt)
  {
    if ((this.mTracker != null) && (trackAnalytics()))
      this.mTracker.trackEvent(paramString1, paramString2, paramString3, paramInt);
  }

  protected void setAnalyticsTrackEventAutocomplete(String paramString)
  {
    setAnalyticsTrackEvent(setAnalyticsPageName(), "AutocompleteButton", paramString, 0);
  }

  protected void setAnalyticsTrackEventContextMenu(String paramString)
  {
    setAnalyticsTrackEvent(setAnalyticsPageName(), "ContextMenu", paramString, 0);
  }

  protected void setAnalyticsTrackEventScreenButton(String paramString)
  {
    setAnalyticsTrackEvent(setAnalyticsPageName(), "ScreenButton", paramString, 0);
  }

  protected void setAwardBadgeFlag(boolean paramBoolean)
  {
    SharedPreferences.Editor localEditor = this.mPrefs.edit();
    localEditor.putBoolean("awards_badge", paramBoolean);
    localEditor.commit();
  }

  protected void setEditBoxHintConfiguration()
  {
    if ((this.mRes.getConfiguration().orientation == 2) && ((this.mRes.getConfiguration().keyboard == 1) || (this.mRes.getConfiguration().hardKeyboardHidden == 2)) && (this.mRes.getConfiguration().keyboardHidden == 1) && ((((ViewGroup)getWindow().getDecorView()).getFocusedChild() instanceof EditText)))
    {
      a(true);
      return;
    }
    a(false);
  }

  public void setMessage(String paramString)
  {
    this.userMessage = paramString;
  }

  protected void setMyLocationToZeros()
  {
    this.myLocation = new Location("NoLocation");
  }

  protected void showAddStation(Location paramLocation, String paramString, int paramInt)
  {
    GBIntent localGBIntent = new GBIntent(this, AddStationMap.class, paramLocation);
    localGBIntent.putExtra("search terms", paramString);
    localGBIntent.putExtra("search type", paramInt);
    startActivityForResult(localGBIntent, 1);
  }

  protected void showAds()
  {
    if ((this.adBanner != null) && (this.adsVisible))
      this.adBanner.setVisibility(0);
  }

  protected void showCameraDialog(Station paramStation)
  {
    GBIntent localGBIntent = new GBIntent(this, StationPhotoUploader.class, this.myLocation);
    localGBIntent.putExtra("station", paramStation);
    startActivity(localGBIntent);
  }

  protected void showCityZipSearch()
  {
    startActivityForResult(new GBIntent(this, SearchBar.class, this.myLocation), 4);
  }

  protected void showDirections(double paramDouble1, double paramDouble2, Location paramLocation)
  {
    if ((paramLocation.getLatitude() != 0.0D) && (paramLocation.getLongitude() != 0.0D))
      startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://maps.google.com/maps?saddr=" + paramLocation.getLatitude() + "," + paramLocation.getLongitude() + "&daddr=" + paramDouble1 + "," + paramDouble2)));
  }

  protected void showFavourites()
  {
    startActivityForResult(new GBIntent(this, Favourites.class, this.myLocation), 1);
  }

  protected void showHome()
  {
    GBIntent localGBIntent = new GBIntent(this, MainScreen.class, this.myLocation);
    localGBIntent.setFlags(872415232);
    localGBIntent.addFlags(67108864);
    startActivity(localGBIntent);
    finish();
  }

  protected void showList(String paramString1, String paramString2, String paramString3, ListResults paramListResults)
  {
    if (paramListResults.getTotalStations() > 0)
    {
      GBIntent localGBIntent = new GBIntent(this, ListStations.class, this.myLocation);
      localGBIntent.putExtra("list", paramListResults);
      localGBIntent.putExtra("city", paramString1);
      localGBIntent.putExtra("state", paramString2);
      localGBIntent.putExtra("zip", paramString3);
      if ((paramString1.equals("")) && (paramString3.equals("")))
        localGBIntent.putExtra("list sort order", 1);
      while (true)
      {
        localGBIntent.putExtra("center latitude", (int)(1000000.0D * paramListResults.getCenter().getLatitude()));
        localGBIntent.putExtra("center longitude", (int)(1000000.0D * paramListResults.getCenter().getLongitude()));
        startActivityForResult(localGBIntent, 0);
        if ((this instanceof SearchBar))
        {
          setResult(-1);
          finish();
        }
        return;
        localGBIntent.putExtra("list sort order", 3);
      }
    }
    a(paramListResults);
  }

  protected void showLogin()
  {
    startActivityForResult(new GBIntent(this, MemberLogin.class, this.myLocation), 6);
  }

  public void showMessage()
  {
    if ((this.userMessage != null) && (this.unstackedToast != null) && (!this.userMessage.equals(this.unstackedToast.getMessage())))
    {
      showMessage(this.userMessage, true);
      return;
    }
    showMessage(this.userMessage, false);
  }

  public void showMessage(String paramString)
  {
    showMessage(paramString, true);
  }

  public void showMessage(String paramString, boolean paramBoolean)
  {
    if ((paramString == null) || (paramString.equals("")))
      return;
    if (!paramBoolean)
      if (this.unstackedToast == null)
        this.unstackedToast = new CustomToast(this, paramString, 1);
    for (CustomToast localCustomToast = this.unstackedToast; ; localCustomToast = new CustomToast(this, paramString, 1))
    {
      localCustomToast.show();
      this.userMessage = "";
      return;
    }
  }

  public void showMessage(boolean paramBoolean)
  {
    showMessage(this.userMessage, paramBoolean);
  }

  protected void showPlaces()
  {
    startActivity(new GBIntent(this, PlacesList.class, this.myLocation));
  }

  protected void showRegistration()
  {
    showRegistration(false);
  }

  protected void showRegistration(boolean paramBoolean)
  {
    GBIntent localGBIntent = new GBIntent(this, MemberRegistration.class, this.myLocation);
    localGBIntent.putExtra("register show skip", paramBoolean);
    startActivityForResult(localGBIntent, 6);
  }

  protected void showShare()
  {
    startActivity(new GBIntent(this, ShareApp.class, this.myLocation));
  }

  public void showSmartPrompt(SmartPrompt paramSmartPrompt)
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this, this.mTracker);
    View localView = this.mInflater.inflate(2130903073, null);
    CheckedTextView localCheckedTextView = (CheckedTextView)localView.findViewById(2131165329);
    TextView localTextView = (TextView)localView.findViewById(2131165234);
    localBuilder.setContentView(localView);
    localBuilder.setPositiveButton(getString(2131296666), new e(this, paramSmartPrompt, localCheckedTextView));
    localBuilder.setNegativeButton(getString(2131296669), new f(this, paramSmartPrompt, localCheckedTextView));
    localBuilder.setNeutralButton(getString(2131296667), new g(this, paramSmartPrompt, localCheckedTextView));
    localCheckedTextView.setOnClickListener(new h(this, localCheckedTextView));
    localBuilder.setTitle(getString(2131296554));
    localTextView.setText(paramSmartPrompt.getPrompt());
    CustomDialog localCustomDialog = localBuilder.create();
    localTextView.setTextSize(20.0F);
    localCustomDialog.setOnCancelListener(new i(this, paramSmartPrompt, localCheckedTextView));
    localCustomDialog.show();
  }

  protected void showStationOnMap(Station paramStation)
  {
    GBIntent localGBIntent = new GBIntent(this, MapStationDialog.class, this.myLocation);
    localGBIntent.putExtra("station", paramStation);
    startActivity(localGBIntent);
  }

  protected void startGPSService()
  {
    Intent localIntent = new Intent(this, GPSService.class);
    startService(localIntent);
    bindService(localIntent, this.d, 1);
  }

  protected void stopGPSService()
  {
    if (this.d != null);
    try
    {
      unbindService(this.d);
      return;
    }
    catch (Exception localException)
    {
    }
  }

  protected boolean trackAnalytics()
  {
    return true;
  }

  protected boolean updateBrandVersion(int paramInt1, int paramInt2)
  {
    return this.a.updateBrandVersionRow(paramInt1, paramInt2);
  }

  protected boolean updatePlace(String paramString1, String paramString2, double paramDouble1, double paramDouble2)
  {
    return this.a.updateSearchesRow(paramString1, paramString2, paramDouble1, paramDouble2);
  }

  protected boolean updatePlaceName(long paramLong, String paramString)
  {
    return this.a.updateSearchesName(paramLong, paramString);
  }

  private final class a extends CustomAsyncTask
  {
    public a(GBActivity arg2)
    {
      super();
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      paramBoolean.booleanValue();
    }

    protected final boolean queryWebService()
    {
      GBActivity.b(GBActivity.this);
      return true;
    }
  }

  private final class b extends CustomAsyncTask
  {
    private String b;
    private boolean c;
    private int d;

    public b(GBActivity paramInt, int paramString, String paramBoolean, boolean arg5)
    {
      super();
      this.b = paramBoolean;
      boolean bool;
      this.c = bool;
      this.d = paramString;
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      paramBoolean.booleanValue();
    }

    protected final boolean queryWebService()
    {
      GBActivity.a(GBActivity.this, this.d, this.b, this.c);
      return true;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.base.GBActivity
 * JD-Core Version:    0.6.2
 */