package gbis.gbandroid.activities.base;

import android.content.res.Configuration;

public abstract class GBActivityAds extends GBActivity
{
  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    adjustAdsInConfiguration(paramConfiguration);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.base.GBActivityAds
 * JD-Core Version:    0.6.2
 */