package gbis.gbandroid.activities.base;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import gbis.gbandroid.GBApplication;
import gbis.gbandroid.entities.Station;

public abstract class GBActivityDialog extends GBActivity
{
  private int a;
  private LinearLayout b;
  private Button c;
  private Button d;
  private Button e;
  private ImageView f;

  private void a()
  {
    setGBActivityDialogButtons();
    if (this.e == null)
      setGBActivityDialogPositiveButton(null, null);
    if (this.d == null)
      setGBActivityDialogNegativeButton(null, null);
    if (this.c == null)
      setGBActivityDialogNeutralButton(null, null);
    if (this.f == null)
      setGBActivityDialogIcon(null);
    if (this.a == 1)
      a(this.e);
    do
    {
      return;
      if (this.a == 4)
      {
        a(this.c);
        return;
      }
      if (this.a == 2)
      {
        a(this.d);
        return;
      }
    }
    while (this.a != 0);
    findViewById(2131165235).setVisibility(8);
  }

  private void a(Button paramButton)
  {
    LinearLayout.LayoutParams localLayoutParams = (LinearLayout.LayoutParams)paramButton.getLayoutParams();
    localLayoutParams.gravity = 1;
    localLayoutParams.weight = 0.5F;
    paramButton.setLayoutParams(localLayoutParams);
    findViewById(2131165236).setVisibility(0);
    findViewById(2131165240).setVisibility(0);
  }

  private void a(Button paramButton, String paramString, int paramInt, View.OnClickListener paramOnClickListener)
  {
    if (paramString != null)
    {
      paramButton.setText(paramString);
      this.a = (paramInt | this.a);
      if (paramOnClickListener != null)
        paramButton.setOnClickListener(new k(this, paramOnClickListener));
      return;
    }
    paramButton.setVisibility(8);
  }

  protected Button getGBActivityDialogNegativeButton()
  {
    return this.d;
  }

  protected Button getGBActivityDialogNeutralButton()
  {
    return this.c;
  }

  protected Button getGBActivityDialogPositiveButton()
  {
    return this.e;
  }

  protected LinearLayout getGBContentLayout()
  {
    return this.b;
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903055);
    this.b = ((LinearLayout)findViewById(2131165233));
    a();
  }

  protected abstract void setGBActivityDialogButtons();

  protected void setGBActivityDialogIcon(Drawable paramDrawable)
  {
    this.f = ((ImageView)findViewById(2131165231));
    if (paramDrawable != null)
    {
      this.f.setImageDrawable(paramDrawable);
      this.f.setVisibility(0);
      return;
    }
    this.f.setVisibility(8);
  }

  protected void setGBActivityDialogNegativeButton(int paramInt, View.OnClickListener paramOnClickListener)
  {
    setGBActivityDialogNegativeButton(getString(paramInt), paramOnClickListener);
  }

  protected void setGBActivityDialogNegativeButton(String paramString, View.OnClickListener paramOnClickListener)
  {
    this.d = ((Button)findViewById(2131165239));
    a(this.d, paramString, 2, paramOnClickListener);
  }

  protected void setGBActivityDialogNeutralButton(int paramInt, View.OnClickListener paramOnClickListener)
  {
    setGBActivityDialogNeutralButton(getString(paramInt), paramOnClickListener);
  }

  protected void setGBActivityDialogNeutralButton(String paramString, View.OnClickListener paramOnClickListener)
  {
    this.c = ((Button)findViewById(2131165238));
    a(this.c, paramString, 4, paramOnClickListener);
  }

  protected void setGBActivityDialogPositiveButton(int paramInt, View.OnClickListener paramOnClickListener)
  {
    setGBActivityDialogPositiveButton(getString(paramInt), paramOnClickListener);
  }

  protected void setGBActivityDialogPositiveButton(String paramString, View.OnClickListener paramOnClickListener)
  {
    this.e = ((Button)findViewById(2131165237));
    a(this.e, paramString, 1, paramOnClickListener);
  }

  protected void setGBActivityDialogStationIcon(Station paramStation)
  {
    this.f = ((ImageView)findViewById(2131165231));
    if (paramStation != null)
    {
      Bitmap localBitmap = GBApplication.getInstance().getLogo(paramStation.getGasBrandId(), "/Android/data/gbis.gbandroid/cache/Brands");
      if (localBitmap != null)
        this.f.setImageBitmap(localBitmap);
      while (true)
      {
        this.f.setVisibility(0);
        return;
        this.f.setImageDrawable(this.mRes.getDrawable(2130837622));
      }
    }
    this.f.setVisibility(8);
  }

  protected void setGBActivityDialogTitle(String paramString)
  {
    ((TextView)findViewById(2131165232)).setText(paramString);
  }

  protected void setGBViewInContentLayout(int paramInt, boolean paramBoolean)
  {
    ViewGroup localViewGroup = (ViewGroup)this.mInflater.inflate(paramInt, null);
    this.b.removeAllViews();
    this.b.addView(localViewGroup, new ViewGroup.LayoutParams(-1, -2));
    if (paramBoolean)
      this.b.getLayoutParams().width = -1;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.base.GBActivityDialog
 * JD-Core Version:    0.6.2
 */