package gbis.gbandroid.activities.base;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.google.android.maps.Projection;
import gbis.gbandroid.activities.favorites.Favourites;
import gbis.gbandroid.activities.favorites.PlacesList;
import gbis.gbandroid.activities.search.MainScreen;
import gbis.gbandroid.activities.search.SearchBar;
import gbis.gbandroid.activities.settings.Settings;
import gbis.gbandroid.content.GBIntent;
import gbis.gbandroid.entities.AwardsMessage;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.utils.ConnectionUtils;
import gbis.gbandroid.utils.CustomAsyncTask;
import gbis.gbandroid.views.CustomProgressDialog.Builder;
import gbis.gbandroid.views.CustomToast;
import gbis.gbandroid.views.CustomToastAward;
import java.util.List;

public abstract class GBActivityMap extends MapActivity
{
  public static final int ACTIVITY_CREATE = 1;
  protected static final int DEFAULT_ZOOM_LVL = 14;
  public static final int REPORT_PRICE = 3;
  protected static final int RESULT_REPORT = 3;
  public static final int SEARCH_PERFORMED = 4;
  protected LayoutInflater mInflater;
  protected SharedPreferences mPrefs;
  protected Resources mRes;
  protected ResponseMessage<?> mResponseObject;
  protected GoogleAnalyticsTracker mTracker;
  protected MyOwnLocationOverlay myLocationOverlay;
  protected Dialog progress;
  protected String userMessage;

  private void a()
  {
    this.mTracker.trackPageView(setAnalyticsPageName());
    Log.i("Analytics", setAnalyticsPageName());
    setAnalyticsCustomVariables();
  }

  public void checkAwardAndShow()
  {
    List localList;
    int i;
    if (this.mResponseObject != null)
    {
      localList = this.mResponseObject.getAwards();
      if (localList != null)
      {
        i = localList.size();
        if (i <= 0);
      }
    }
    for (int j = 0; ; j++)
    {
      if (j >= i)
        return;
      AwardsMessage localAwardsMessage = (AwardsMessage)localList.get(j);
      if (localAwardsMessage.getAwarded() == 1)
      {
        showAward(localAwardsMessage);
        setAwardBadgeFlag(true);
      }
    }
  }

  protected boolean checkConnection()
  {
    return ConnectionUtils.isConnectedToAny(this);
  }

  protected GBActivityMap getActivity()
  {
    return this;
  }

  protected float getDensity()
  {
    DisplayMetrics localDisplayMetrics = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
    return localDisplayMetrics.density;
  }

  public String getHost()
  {
    return this.mPrefs.getString("host", getString(2131296733));
  }

  protected Location getLocation()
  {
    if (this.myLocationOverlay != null)
    {
      Location localLocation = this.myLocationOverlay.getLastFix();
      if (localLocation == null)
        localLocation = new Location("NoLocation");
      return localLocation;
    }
    return new Location("NoLocation");
  }

  public ResponseMessage<?> getResponseObject()
  {
    return this.mResponseObject;
  }

  protected boolean isRouteDisplayed()
  {
    return false;
  }

  protected void launchSettings()
  {
    startActivity(new GBIntent(this, Settings.class, getLocation()));
  }

  protected void loadDialog(String paramString)
  {
    if (!this.progress.isShowing())
    {
      CustomProgressDialog.Builder localBuilder = new CustomProgressDialog.Builder(this);
      localBuilder.setMessage(paramString);
      this.progress = localBuilder.create();
      this.progress.setCancelable(true);
      this.progress.show();
    }
  }

  protected void loadDialog(String paramString, CustomAsyncTask paramCustomAsyncTask)
  {
    if (!this.progress.isShowing())
    {
      CustomProgressDialog.Builder localBuilder = new CustomProgressDialog.Builder(this);
      localBuilder.setMessage(paramString);
      this.progress = localBuilder.create();
      this.progress.setCancelable(true);
      this.progress.setOnCancelListener(new l(this, paramCustomAsyncTask));
      this.progress.show();
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.mInflater = ((LayoutInflater)getSystemService("layout_inflater"));
    this.progress = new Dialog(this);
    this.mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
    this.mRes = getResources();
    this.mTracker = GoogleAnalyticsTracker.getInstance();
    this.mTracker.startNewSession(getString(2131296806), this);
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    setAnalyticsTrackEvent(setAnalyticsPageName(), "PhoneButton", getString(2131296605), 0);
    return true;
  }

  protected void onDestroy()
  {
    super.onDestroy();
    this.mTracker.dispatch();
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    setAnalyticsTrackEvent(setAnalyticsPageName(), "MenuButton", paramMenuItem.getTitle().toString(), 0);
    return true;
  }

  protected void onResume()
  {
    super.onResume();
    a();
  }

  protected void setAnalyticsCustomVariables()
  {
    this.mTracker.setCustomVar(1, "AuthId", this.mPrefs.getString("auth_id", ""));
    this.mTracker.setCustomVar(2, "AppVersion", GBActivity.getVersion(this));
    this.mTracker.setCustomVar(3, "MemberId", this.mPrefs.getString("member_id", ""));
    this.mTracker.setCustomVar(4, "FuelType", this.mPrefs.getString("fuelPreference", ""));
  }

  protected abstract String setAnalyticsPageName();

  protected void setAnalyticsTrackEvent(String paramString1, String paramString2, String paramString3, int paramInt)
  {
    this.mTracker.trackEvent(paramString1, paramString2, paramString3, paramInt);
  }

  protected void setAnalyticsTrackEventScreenButton(String paramString)
  {
    this.mTracker.trackEvent(setAnalyticsPageName(), "ScreenButton", paramString, 0);
  }

  protected void setAwardBadgeFlag(boolean paramBoolean)
  {
    SharedPreferences.Editor localEditor = this.mPrefs.edit();
    localEditor.putBoolean("awards_badge", paramBoolean);
    localEditor.commit();
  }

  public void setMessage(String paramString)
  {
    this.userMessage = paramString;
  }

  public void showAward(AwardsMessage paramAwardsMessage)
  {
    new CustomToastAward(this, paramAwardsMessage, 1).show();
  }

  protected void showCityZipSearch()
  {
    startActivityForResult(new GBIntent(this, SearchBar.class, getLocation()), 4);
  }

  protected void showFavourites()
  {
    startActivityForResult(new GBIntent(this, Favourites.class, getLocation()), 1);
  }

  protected void showHome()
  {
    GBIntent localGBIntent = new GBIntent(this, MainScreen.class, getLocation());
    localGBIntent.setFlags(872415232);
    startActivity(localGBIntent);
    setResult(-1);
    finish();
  }

  public void showMessage()
  {
    if ((this.userMessage != null) && (!this.userMessage.equals("")))
    {
      new CustomToast(this, this.userMessage, 1).show();
      setMessage("");
    }
  }

  public void showMessage(String paramString)
  {
    if ((paramString != null) && (!paramString.equals("")))
      new CustomToast(this, paramString, 1).show();
  }

  protected void showPlaces()
  {
    startActivity(new GBIntent(this, PlacesList.class, getLocation()));
  }

  protected class MyOwnLocationOverlay extends MyLocationOverlay
  {
    private boolean b = false;
    private boolean c = false;
    private Paint d = new Paint();
    private Drawable e;
    private int f;
    private int g;

    public MyOwnLocationOverlay(Context paramMapView, MapView arg3)
    {
      super(localMapView);
    }

    protected void drawMyLocation(Canvas paramCanvas, MapView paramMapView, Location paramLocation, GeoPoint paramGeoPoint, long paramLong)
    {
      if (!this.c);
      try
      {
        super.drawMyLocation(paramCanvas, paramMapView, paramLocation, paramGeoPoint, paramLong);
        if (this.c)
        {
          if (this.e == null)
          {
            this.e = paramMapView.getContext().getResources().getDrawable(2130837657);
            this.f = this.e.getIntrinsicWidth();
            this.g = this.e.getIntrinsicHeight();
          }
          Projection localProjection = paramMapView.getProjection();
          float f1 = localProjection.metersToEquatorPixels(paramLocation.getAccuracy());
          Point localPoint = localProjection.toPixels(paramGeoPoint, null);
          this.d.setAntiAlias(true);
          this.d.setColor(-16776961);
          if (f1 > 10.0F)
          {
            this.d.setAlpha(50);
            paramCanvas.drawCircle(localPoint.x, localPoint.y, f1, this.d);
          }
          this.d.setAlpha(255);
          paramCanvas.drawCircle(localPoint.x, localPoint.y, 10.0F, this.d);
          this.e.setBounds(localPoint.x - this.f / 2, localPoint.y - this.g / 2, localPoint.x + this.f / 2, localPoint.y + this.g / 2);
          this.e.draw(paramCanvas);
        }
        return;
      }
      catch (Exception localException)
      {
        while (true)
          this.c = true;
      }
    }

    public boolean isFollowMe()
    {
      return this.b;
    }

    public void setFollowMe(boolean paramBoolean)
    {
      this.b = paramBoolean;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.base.GBActivityMap
 * JD-Core Version:    0.6.2
 */