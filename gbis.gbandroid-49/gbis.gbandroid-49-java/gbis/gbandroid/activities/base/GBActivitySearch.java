package gbis.gbandroid.activities.base;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filter.FilterResults;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.google.android.maps.GeoPoint;
import gbis.gbandroid.activities.favorites.PlacesList;
import gbis.gbandroid.activities.search.MainScreen;
import gbis.gbandroid.animations.MyScaleAnimation;
import gbis.gbandroid.entities.AutoCompMessage;
import gbis.gbandroid.entities.AutocompletePlaces;
import gbis.gbandroid.entities.ListResults;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.queries.AutoCompleteCityQuery;
import gbis.gbandroid.queries.ListQuery;
import gbis.gbandroid.utils.CustomAsyncTask;
import gbis.gbandroid.utils.VerifyFields;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public abstract class GBActivitySearch extends GBActivity
{
  public static final String AWARD = "award";
  public static final int BY_DISTANCE = 1;
  public static final int BY_LOCATION = 3;
  public static final int BY_PRICE = 2;
  public static final String CENTER_LAT = "center latitude";
  public static final String CENTER_LONG = "center longitude";
  public static final String CITY = "city";
  public static final int CONNECTION_ERROR = 9;
  public static final String COUNTRY = "country";
  public static final String CURRENCY = "currency";
  public static final String FAVOURITE_ID = "favourite id";
  public static final String FROM_MAP = "from map";
  public static final String FUEL_DIESEL = "fuel diesel";
  public static final String FUEL_MIDGRADE = "fuel midgrade";
  public static final String FUEL_PREMIUM = "fuel premium";
  public static final String FUEL_REGULAR = "fuel regular";
  public static final String GB_LIST = "list";
  public static final String GB_MAP = "map";
  public static final String LIST_SORT_ORDER = "list sort order";
  public static final String MAP_TOTAL_STATIONS = "map total stations";
  public static final int MILES = 50;
  public static final String MY_LAT = "my latitude";
  public static final String MY_LOCATION = "my location";
  public static final String MY_LONG = "my longitude";
  public static final String PLACE_NAME = "place name";
  public static final String PRICE = "price";
  public static final String REGISTER_SHOW_SKIP = "register show skip";
  public static final int RTN = 100;
  public static final String SEARCH_TERMS = "search terms";
  public static final String SEARCH_TYPE = "search type";
  public static final String SM_ID = "sm_id";
  public static final String STATE = "state";
  public static final String STATION = "station";
  public static final String STATION_ADDR = "station address";
  public static final String STATION_NM = "station_nm";
  public static final String STATION_PHOTOS = "station photos";
  public static final String STATION_PHOTOS_POSITION = "station photos position";
  public static final String TIME_OFFSET = "time offset";
  public static final String TIME_SPOTTED = "time spotted";
  public static final String TIME_SPOTTED_DIESEL = "time spotted diesel";
  public static final String TIME_SPOTTED_MIDGRADE = "time spotted midgrade";
  public static final String TIME_SPOTTED_PREMIUM = "time spotted premium";
  public static final String TIME_SPOTTED_REGULAR = "time spotted regular";
  public static final String TITLE = "title";
  public static final String TOTAL_NEAR = "total near";
  public static final String TOTAL_STATIONS = "total stations";
  public static final String ZIP = "zip";
  final Handler a = new m(this);
  protected boolean activityLoading;
  protected String authId;
  private GeoPoint b;
  protected View buttonNearMe;
  protected Button buttonSearch;
  private d c;
  private int d = 400;
  private boolean e;
  protected String fuelPreference;
  protected String memberId;
  protected int radiusPreference;
  protected String screenPreference;
  protected AutoCompleteTextView searchCityZip;

  private List<AutoCompMessage> a()
  {
    Type localType = new t(this).getType();
    this.mResponseObject = new AutoCompleteCityQuery(this, this.mPrefs, localType, this.myLocation).getResponseObject(this.searchCityZip.getText().toString());
    return (List)this.mResponseObject.getPayload();
  }

  private void a(int paramInt)
  {
    new b(this.a, 0).start();
  }

  private void a(String paramString1, String paramString2, String paramString3)
  {
    this.activityLoading = true;
    getPreferences();
    Type localType = new u(this).getType();
    ListQuery localListQuery = new ListQuery(this, this.mPrefs, localType, this.myLocation);
    if ((this.b == null) || ((this.b.getLatitudeE6() == 0) && (this.b.getLongitudeE6() == 0)))
    {
      this.myLocation = getLastKnownLocation();
      if (this.myLocation == null)
        break label163;
    }
    label163: for (this.b = new GeoPoint((int)(1000000.0D * this.myLocation.getLatitude()), (int)(1000000.0D * this.myLocation.getLongitude())); ; this.b = new GeoPoint(0, 0))
    {
      this.mResponseObject = localListQuery.getResponseObject(paramString1, paramString2, paramString3, this.b);
      if (!this.c.isCancelled())
        showList(paramString1, paramString2, paramString3, (ListResults)this.mResponseObject.getPayload());
      return;
    }
  }

  private boolean a(String paramString)
  {
    int i;
    String str2;
    try
    {
      Integer.parseInt(paramString);
      a("", "", paramString);
      bool = true;
      return bool;
    }
    catch (NumberFormatException localNumberFormatException)
    {
      do
      {
        while (true)
        {
          String str1 = VerifyFields.checkPostalCode(paramString);
          if (str1.equals(""))
            break;
          a("", "", str1);
        }
        i = paramString.indexOf(",");
        boolean bool = false;
      }
      while (i == -1);
      str2 = paramString.substring(0, i);
      if (paramString.length() <= i + 3)
        break label141;
    }
    Object localObject;
    if (paramString.substring(i + 1, i + 2).contains(" "))
    {
      String str4 = paramString.substring(i + 2);
      paramString = str2;
      localObject = str4;
    }
    while (true)
    {
      a(paramString, (String)localObject, "");
      break;
      label141: if ((paramString.length() > i + 2) && (!paramString.substring(i + 1, i + 2).contains(" ")))
      {
        String str3 = paramString.substring(i + 1);
        paramString = str2;
        localObject = str3;
      }
      else
      {
        localObject = "";
      }
    }
  }

  private void b()
  {
    ArrayList localArrayList = new ArrayList();
    if (!this.e)
    {
      Cursor localCursor = getAllPlaces(-1);
      if ((localCursor != null) && (localCursor.moveToFirst()))
        do
          localArrayList.add(new AutocompletePlaces(localCursor.getString(localCursor.getColumnIndex("table_column_name")), true));
        while (localCursor.moveToNext());
      localCursor.close();
    }
    a locala = new a(getApplicationContext(), localArrayList, true);
    this.searchCityZip.setAdapter(locala);
    this.searchCityZip.setOnItemClickListener(new p(this));
    try
    {
      this.searchCityZip.showDropDown();
      return;
    }
    catch (Exception localException)
    {
    }
  }

  private void b(int paramInt)
  {
    String str = "";
    switch (paramInt)
    {
    default:
    case 0:
    case 1:
    case 2:
    }
    while (true)
    {
      setAnalyticsTrackEventAutocomplete(str);
      return;
      str = getString(2131296623);
      if (!this.e)
      {
        showPlaces();
      }
      else
      {
        showMessage(getString(2131296258));
        continue;
        str = getString(2131296617);
        startNearMeThread();
        continue;
        getString(2131296619);
        showFavourites();
      }
    }
  }

  private void b(String paramString)
  {
    if (!a(paramString))
    {
      if ((this.searchCityZip.getAdapter() == null) || (this.searchCityZip.getAdapter().isEmpty()))
        break label109;
      String str = ((AutocompletePlaces)this.searchCityZip.getAdapter().getItem(0)).getText();
      int i = str.indexOf(",");
      if (i >= 0)
        a(str.substring(0, i), str.substring(i + 2), "");
    }
    else
    {
      return;
    }
    a(this.searchCityZip.getText().toString(), "", "");
    return;
    label109: a(this.searchCityZip.getText().toString(), "", "");
  }

  private static boolean b(List<AutocompletePlaces> paramList, String paramString)
  {
    int i = paramList.size();
    for (int j = 0; ; j++)
    {
      if (j >= i)
        return false;
      if (((AutocompletePlaces)paramList.get(j)).getText().equals(paramString))
        return true;
    }
  }

  private void c(int paramInt)
  {
    Cursor localCursor;
    if ((this.searchCityZip.getAdapter() != null) && (this.searchCityZip.getAdapter().getCount() > 0))
    {
      if (this.e)
        break label97;
      localCursor = getPlace(((AutocompletePlaces)this.searchCityZip.getAdapter().getItem(paramInt)).getText());
      if (localCursor != null)
      {
        if (localCursor.getCount() > 0)
          break label84;
        localCursor.close();
      }
    }
    startCityZipThread(getSearchText());
    return;
    label84: localCursor.moveToFirst();
    searchUsingPlaces(localCursor);
    return;
    label97: startCityZipThread(getSearchText());
  }

  protected void autoComplete()
  {
    Object[] arrayOfObject = (Object[])getLastNonConfigurationInstance();
    if (arrayOfObject != null);
    for (String str = (String)arrayOfObject[1]; ; str = null)
    {
      this.searchCityZip.setThreshold(0);
      this.searchCityZip.addTextChangedListener(new v(this));
      this.searchCityZip.setOnKeyListener(new w(this));
      this.searchCityZip.clearFocus();
      if (str != null)
      {
        this.searchCityZip.setText(str);
        this.searchCityZip.dismissDropDown();
      }
      this.searchCityZip.setOnFocusChangeListener(new x(this));
      this.searchCityZip.setOnTouchListener(new n(this));
      return;
    }
  }

  protected void getPreferences()
  {
    this.fuelPreference = this.mPrefs.getString("fuelPreference", "");
    this.memberId = this.mPrefs.getString("member_id", "");
    this.screenPreference = this.mPrefs.getString("screenPreference", "");
    this.radiusPreference = this.mPrefs.getInt("radiusPreference", 0);
    this.authId = this.mPrefs.getString("auth_id", "");
  }

  protected String getSearchText()
  {
    return this.searchCityZip.getText().toString();
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    getPreferences();
  }

  public void onResume()
  {
    super.onResume();
    if (!openDB())
      this.e = true;
    setAutocompleteWithQuickLinks();
  }

  public Object onRetainNonConfigurationInstance()
  {
    Object[] arrayOfObject = new Object[2];
    if ((this.searchCityZip != null) && (this.searchCityZip.getText() != null))
    {
      arrayOfObject[1] = this.searchCityZip.getText().toString();
      return arrayOfObject;
    }
    arrayOfObject[1] = "";
    return arrayOfObject;
  }

  protected void populateButtons()
  {
    this.buttonSearch = ((Button)findViewById(2131165446));
    this.searchCityZip = ((AutoCompleteTextView)findViewById(2131165447));
    this.buttonSearch.setOnClickListener(new q(this));
    if (this.buttonNearMe != null)
      this.buttonNearMe.setOnClickListener(new r(this));
    getWindow().getDecorView().setOnTouchListener(new s(this));
  }

  protected void resetFocus()
  {
    if (this.searchCityZip.hasFocus())
    {
      ((InputMethodManager)getSystemService("input_method")).hideSoftInputFromWindow(this.searchCityZip.getWindowToken(), 0);
      if ((this instanceof MainScreen))
      {
        ImageView localImageView = (ImageView)findViewById(2131165242);
        localImageView.setVisibility(0);
        localImageView.setFocusable(true);
        localImageView.setFocusableInTouchMode(true);
        this.searchCityZip.clearFocus();
        this.searchCityZip.dismissDropDown();
        localImageView.requestFocus();
        ((MainScreen)this).resizeElements(this.mRes.getConfiguration());
        localImageView.startAnimation(new MyScaleAnimation(1.0F, 1.0F, 0.0F, 1.0F, this.d, localImageView, false));
      }
    }
  }

  protected void searchUsingPlaces(Cursor paramCursor)
  {
    double d1 = paramCursor.getDouble(paramCursor.getColumnIndex("table_column_latitude"));
    double d2 = paramCursor.getDouble(paramCursor.getColumnIndex("table_column_longitude"));
    if ((d1 == 0.0D) && (d2 == 0.0D))
    {
      String str = paramCursor.getString(paramCursor.getColumnIndex("table_column_city_zip"));
      startPlacesThread(paramCursor.getString(paramCursor.getColumnIndex("table_column_name")), str);
    }
    while (true)
    {
      incrementFrequencyOfPlace(paramCursor.getString(paramCursor.getColumnIndex("table_column_name")));
      return;
      this.b = new GeoPoint((int)(d1 * 1000000.0D), (int)(d2 * 1000000.0D));
      startPlacesThread(paramCursor.getString(paramCursor.getColumnIndex("table_column_name")));
    }
  }

  protected void setAutocompleteWithQuickLinks()
  {
    setAutocompleteWithQuickLinks(false);
  }

  protected void setAutocompleteWithQuickLinks(boolean paramBoolean)
  {
    if ((this.searchCityZip != null) && ((this.searchCityZip.getAdapter() == null) || (paramBoolean) || ((this.searchCityZip.getAdapter() != null) && (a.b((a)this.searchCityZip.getAdapter())))))
    {
      ArrayList localArrayList = new ArrayList();
      localArrayList.add(new AutocompletePlaces(getString(2131296623), this.mRes.getDrawable(2130837592), true));
      localArrayList.add(new AutocompletePlaces(getString(2131296617), this.mRes.getDrawable(2130837591), true));
      if (!this.memberId.equals(""))
        localArrayList.add(new AutocompletePlaces(getString(2131296619), this.mRes.getDrawable(2130837590), true));
      a locala = new a(getApplicationContext(), localArrayList, false);
      this.searchCityZip.setAdapter(locala);
      this.searchCityZip.setOnItemClickListener(new o(this));
    }
  }

  protected void startCityZipThread(String paramString)
  {
    this.c = new d(this, paramString);
    this.c.execute(new Object[0]);
    loadDialog(getString(2131296390), this.c);
  }

  protected void startNearMeThread()
  {
    this.c = new d(this);
    this.c.execute(new Object[0]);
    loadDialog(getString(2131296389), this.c);
  }

  protected void startPlacesThread(String paramString)
  {
    this.c = new d(this);
    this.c.execute(new Object[0]);
    loadDialog(String.format(getString(2131296391), new Object[] { paramString }), this.c);
  }

  protected void startPlacesThread(String paramString1, String paramString2)
  {
    this.c = new d(this, paramString2);
    this.c.execute(new Object[0]);
    loadDialog(String.format(getString(2131296391), new Object[] { paramString1 }), this.c);
  }

  private final class a extends ArrayAdapter<AutocompletePlaces>
  {
    private List<AutocompletePlaces> b;
    private List<AutocompletePlaces> c;
    private int d;
    private a e;
    private boolean f;

    public a(int paramBoolean, boolean arg3)
    {
      super(2130903101, localList);
      this.b = localList;
      this.c = localList;
      this.d = 2130903101;
      boolean bool;
      this.f = bool;
      if (bool)
        this.e = new a((byte)0);
    }

    public final AutocompletePlaces a(int paramInt)
    {
      return (AutocompletePlaces)this.c.get(paramInt);
    }

    public final int getCount()
    {
      if (this.c != null)
        return this.c.size();
      return 0;
    }

    public final Filter getFilter()
    {
      if (this.f)
        return this.e;
      return super.getFilter();
    }

    public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      GBActivitySearch.e locale;
      if (paramView == null)
      {
        paramView = GBActivitySearch.this.mInflater.inflate(this.d, null);
        locale = new GBActivitySearch.e();
        locale.a = ((TextView)paramView.findViewById(2131165410));
        locale.b = ((ImageView)paramView.findViewById(2131165409));
        paramView.setTag(locale);
      }
      while (true)
      {
        AutocompletePlaces localAutocompletePlaces = (AutocompletePlaces)this.c.get(paramInt);
        if (localAutocompletePlaces != null)
        {
          locale.a.setText(localAutocompletePlaces.getText());
          if (!localAutocompletePlaces.isPlace())
            break;
          locale.b.setVisibility(0);
          if (localAutocompletePlaces.getIcon() != null)
            locale.b.setImageDrawable(localAutocompletePlaces.getIcon());
        }
        return paramView;
        locale = (GBActivitySearch.e)paramView.getTag();
      }
      locale.b.setVisibility(4);
      return paramView;
    }

    private final class a extends Filter
    {
      private a()
      {
      }

      protected final Filter.FilterResults performFiltering(CharSequence paramCharSequence)
      {
        Filter.FilterResults localFilterResults = new Filter.FilterResults();
        ArrayList localArrayList = new ArrayList();
        int i = GBActivitySearch.a.a(GBActivitySearch.a.this).size();
        for (int j = 0; ; j++)
        {
          if (j >= i)
          {
            localFilterResults.count = localArrayList.size();
            localFilterResults.values = localArrayList;
            return localFilterResults;
          }
          AutocompletePlaces localAutocompletePlaces = (AutocompletePlaces)GBActivitySearch.a.a(GBActivitySearch.a.this).get(j);
          if (localAutocompletePlaces.getText().toLowerCase().startsWith(((String)paramCharSequence).toLowerCase()))
            localArrayList.add(localAutocompletePlaces);
        }
      }

      protected final void publishResults(CharSequence paramCharSequence, Filter.FilterResults paramFilterResults)
      {
        GBActivitySearch.a.a(GBActivitySearch.a.this, (List)paramFilterResults.values);
        if (paramFilterResults.count > 0)
        {
          GBActivitySearch.a.this.notifyDataSetChanged();
          return;
        }
        GBActivitySearch.a.this.notifyDataSetInvalidated();
      }
    }
  }

  private final class b extends Thread
  {
    Handler a;
    int b;

    b(Handler paramInt, int arg3)
    {
      this.a = paramInt;
      int i;
      this.b = i;
    }

    public final void run()
    {
      try
      {
        sleep(this.b);
        this.a.sendEmptyMessage(0);
        return;
      }
      catch (InterruptedException localInterruptedException)
      {
        while (true)
          localInterruptedException.printStackTrace();
      }
    }
  }

  private final class c extends CustomAsyncTask
  {
    private List<AutoCompMessage> b;

    public c(GBActivity arg2)
    {
      super(false);
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      List localList;
      ArrayList localArrayList;
      GBActivitySearch.a locala1;
      int i;
      int j;
      int k;
      if ((paramBoolean.booleanValue()) && (this.b != null))
      {
        localList = this.b;
        localArrayList = new ArrayList();
        locala1 = (GBActivitySearch.a)GBActivitySearch.this.searchCityZip.getAdapter();
        i = locala1.getCount();
        j = localList.size();
        k = 0;
      }
      while (true)
      {
        int m;
        if (k >= i)
        {
          m = 0;
          if (m < j)
            break label170;
          GBActivitySearch.a locala2 = new GBActivitySearch.a(GBActivitySearch.this, GBActivitySearch.this.getApplicationContext(), localArrayList, true);
          GBActivitySearch.this.searchCityZip.setAdapter(locala2);
          locala2.getFilter().filter(GBActivitySearch.this.searchCityZip.getText().toString());
        }
        try
        {
          GBActivitySearch.this.searchCityZip.showDropDown();
          return;
          localArrayList.add(locala1.a(k));
          k++;
          continue;
          label170: AutoCompMessage localAutoCompMessage = (AutoCompMessage)localList.get(m);
          String str = localAutoCompMessage.getCity() + ", " + localAutoCompMessage.getState();
          if (!GBActivitySearch.a(localArrayList, str))
          {
            AutocompletePlaces localAutocompletePlaces = new AutocompletePlaces(str, false);
            localArrayList.add(localAutocompletePlaces);
            locala1.add(localAutocompletePlaces);
          }
          m++;
        }
        catch (Exception localException)
        {
        }
      }
    }

    protected final boolean queryWebService()
    {
      this.b = GBActivitySearch.b(GBActivitySearch.this);
      return true;
    }
  }

  private final class d extends CustomAsyncTask
  {
    private String b;

    public d(GBActivity arg2)
    {
      super();
    }

    public d(GBActivity paramString, String arg3)
    {
      super();
      Object localObject;
      this.b = localObject;
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      try
      {
        GBActivitySearch.this.progress.dismiss();
        label15: if (GBActivitySearch.this.searchCityZip != null)
          GBActivitySearch.this.searchCityZip.setText("");
        if (paramBoolean.booleanValue())
        {
          if ((GBActivitySearch.this.screenPreference.equals("home")) || (!(GBActivitySearch.this instanceof MainScreen)))
            break label77;
          GBActivitySearch.this.finish();
        }
        label77: 
        while ((!GBActivitySearch.this.screenPreference.equals("home")) || (!(GBActivitySearch.this instanceof MainScreen)))
          return;
        GBActivitySearch.this.autoComplete();
        return;
      }
      catch (Exception localException)
      {
        break label15;
      }
    }

    protected final boolean queryWebService()
    {
      Location localLocation = GBActivitySearch.this.getLastKnownLocation();
      if (this.b != null)
      {
        if ((GBActivitySearch.a(GBActivitySearch.this) == null) && (localLocation == null))
          GBActivitySearch.a(GBActivitySearch.this, new GeoPoint(0, 0));
        if ((GBActivitySearch.this instanceof PlacesList))
          GBActivitySearch.a(GBActivitySearch.this, this.b);
      }
      while (true)
      {
        return true;
        GBActivitySearch.b(GBActivitySearch.this, this.b);
        continue;
        if (((GBActivitySearch.a(GBActivitySearch.this) == null) || (GBActivitySearch.a(GBActivitySearch.this).getLatitudeE6() == 0) || (GBActivitySearch.a(GBActivitySearch.this).getLongitudeE6() == 0)) && (localLocation == null))
          break;
        GBActivitySearch.a(GBActivitySearch.this, "", "", "");
      }
      GBActivitySearch.this.setMessage(GBActivitySearch.this.getString(2131296279));
      return false;
    }
  }

  static final class e
  {
    TextView a;
    ImageView b;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.base.GBActivitySearch
 * JD-Core Version:    0.6.2
 */