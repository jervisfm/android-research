package gbis.gbandroid.activities.base;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.ListView;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.utils.CustomAsyncTask;
import gbis.gbandroid.views.CustomDialog;
import gbis.gbandroid.views.CustomDialog.Builder;
import gbis.gbandroid.views.CustomProgressDialog.Builder;
import gbis.gbandroid.views.CustomToast;

public abstract class GBPreferenceActivity extends PreferenceActivity
{
  public static final String AD_FAVORITES = "adFavorites";
  public static final String AD_FAVORITES_KEY = "adFavoritesKey";
  public static final String AD_FAVORITES_UNIT = "adFavoritesUnit";
  public static final String AD_INIT = "adInit";
  public static final String AD_INIT_KEY = "adInitKey";
  public static final String AD_INIT_TIME = "adInitTime";
  public static final String AD_INIT_UNIT = "adInitUnit";
  public static final String AD_LIST = "adList";
  public static final String AD_LIST_BOTTOM = "adListBottom";
  public static final String AD_LIST_BOTTOM_KEY = "adListBottomKey";
  public static final String AD_LIST_BOTTOM_UNIT = "adListBottomUnit";
  public static final String AD_LIST_KEY = "adListKey";
  public static final String AD_LIST_SCROLL_TIME = "adListScrollTime";
  public static final String AD_LIST_TOP = "adListTop";
  public static final String AD_LIST_TOP_KEY = "adListTopKey";
  public static final String AD_LIST_TOP_UNIT = "adListTopUnit";
  public static final String AD_LIST_UNIT = "adListUnit";
  public static final String AD_LIST_WAIT_TIME = "adListWaitTime";
  public static final String AD_MAIN = "adMain";
  public static final String AD_MAIN_KEY = "adMainKey";
  public static final String AD_MAIN_UNIT = "adMainUnit";
  public static final String AD_PROFILE = "adProfile";
  public static final String AD_PROFILE_KEY = "adProfileKey";
  public static final String AD_PROFILE_UNIT = "adProfileUnit";
  public static final String AD_STATION = "adStation";
  public static final String AD_STATION_KEY = "adStationKey";
  public static final String AD_STATION_UNIT = "adStationUnit";
  public static final String AUTH_ID = "auth_id";
  public static final String CAR = "car";
  public static final String CAR_ICON_ID = "car_icon_id";
  public static final String CUSTOM_KEYBOARD_PREFERENCE = "customKeyboardPreference";
  public static final String DEVICE_ID = "device_id";
  public static final String DISTANCE_PREFERENCE = "distancePreference";
  public static final String DISTRIBUTION_METHOD = "distributionMethod";
  public static final String FUEL_PREFERENCE = "fuelPreference";
  public static final String GB_APP_CATEGORY = "GbAppCategory";
  public static final String HOST = "host";
  public static final String INITIAL_SCREEN_DATE = "initial_screen_date";
  public static final String IS_MEMBER = "is_member";
  public static final String LARGE_SCREEN_BOOLEAN = "largeScreenBoolean";
  public static final String LAST_LATITUDE = "lastLatitude";
  public static final String LAST_LONGITUDE = "lastLongitude";
  public static final String LAST_REPORTED_PRICE_DATE = "lastReportedPriceDate";
  public static final String LIKE_APP = "like_app";
  public static final String LIKE_APP_DATE = "like_app_date";
  public static final String MEMBER = "member";
  public static final String MEMBER_ID = "member_id";
  public static final String MEMBER_PASSWORD = "password";
  public static final String NEARME_PREFERENCE = "nearmePreference";
  public static final String NO_PRICES_PREFERENCE = "noPricesPreference";
  public static final String RADIUS_PREFERENCE = "radiusPreference";
  public static final String REFERRER_STRING = "referralString";
  public static final String SCREEN_PREFERENCE = "screenPreference";
  public static final String SHARE_APP = "share_app";
  public static final String SHARE_APP_DATE = "share_app_date";
  public static final String SHOW_UPDATE_PRICE_DIALOG = "showUpdatePriceDialog";
  public static final String SMART_PROMPT_PREFERENCE = "smartPromptPreference";
  private String a;
  protected LayoutInflater mInflater;
  protected SharedPreferences mPrefs;
  protected ResponseMessage<?> mResponseObject;
  protected GoogleAnalyticsTracker mTracker;
  protected Location myLocation;
  protected Dialog progress;
  protected Resources res;

  private void a()
  {
    this.mTracker.trackPageView(setAnalyticsPageName());
    Log.i("Analytics", setAnalyticsPageName());
    setAnalyticsCustomVariables();
  }

  public String getHost()
  {
    return this.mPrefs.getString("host", getString(2131296733));
  }

  public ResponseMessage<?> getResponseObject()
  {
    return this.mResponseObject;
  }

  protected void loadDialog(String paramString, CustomAsyncTask paramCustomAsyncTask)
  {
    if (!this.progress.isShowing())
    {
      CustomProgressDialog.Builder localBuilder = new CustomProgressDialog.Builder(this);
      localBuilder.setMessage(paramString);
      this.progress = localBuilder.create();
      this.progress.setCancelable(true);
      this.progress.setOnCancelListener(new z(this, paramCustomAsyncTask));
      this.progress.show();
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.res = getResources();
    this.mInflater = ((LayoutInflater)getSystemService("layout_inflater"));
    this.progress = new Dialog(this);
    this.mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
    {
      if ((Location)localBundle.getParcelable("my location") == null)
        break label116;
      this.myLocation = new Location((Location)localBundle.getParcelable("my location"));
    }
    while (true)
    {
      this.mTracker = GoogleAnalyticsTracker.getInstance();
      this.mTracker.startNewSession(getString(2131296806), this);
      return;
      label116: setMyLocationToZeros();
    }
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    setAnalyticsTrackEvent(setAnalyticsPageName(), "PhoneButton", getString(2131296605), 0);
    return true;
  }

  protected void onDestroy()
  {
    super.onDestroy();
    this.mTracker.dispatch();
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    setAnalyticsTrackEvent(setAnalyticsPageName(), "MenuButton", paramMenuItem.getTitle().toString(), 0);
    return true;
  }

  protected void onResume()
  {
    super.onResume();
    a();
  }

  protected void setAnalyticsCustomVariables()
  {
    this.mTracker.setCustomVar(1, "AuthId", this.mPrefs.getString("auth_id", ""));
    this.mTracker.setCustomVar(2, "AppVersion", GBActivity.getVersion(this));
    this.mTracker.setCustomVar(3, "MemberId", this.mPrefs.getString("member_id", ""));
    this.mTracker.setCustomVar(4, "FuelType", this.mPrefs.getString("fuelPreference", ""));
  }

  protected abstract String setAnalyticsPageName();

  protected void setAnalyticsTrackEvent(String paramString1, String paramString2, String paramString3, int paramInt)
  {
    this.mTracker.trackEvent(paramString1, paramString2, paramString3, paramInt);
  }

  protected void setAnalyticsTrackEventScreenButton(String paramString)
  {
    this.mTracker.trackEvent(setAnalyticsPageName(), "ScreenButton", paramString, 0);
  }

  public void setMessage(String paramString)
  {
    this.a = paramString;
  }

  protected void setMyLocationToZeros()
  {
    this.myLocation = new Location("NoLocation");
  }

  protected void showListDialog(String[] paramArrayOfString1, String[] paramArrayOfString2, String paramString1, String paramString2)
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this);
    View localView = this.mInflater.inflate(2130903060, null);
    localBuilder.setTitle(paramString2);
    localBuilder.setContentView(localView);
    ListView localListView = (ListView)localView.findViewById(2131165255);
    localListView.setAdapter(new a(this, paramArrayOfString1, paramArrayOfString2, paramString1));
    try
    {
      Integer.parseInt(paramArrayOfString2[0]);
      i = 1;
      j = 0;
      if (j >= paramArrayOfString2.length)
      {
        localListView.setItemChecked(j, true);
        CustomDialog localCustomDialog = localBuilder.create();
        localListView.setOnItemClickListener(new aa(this, paramArrayOfString2, paramString1, localCustomDialog));
        localCustomDialog.show();
        return;
      }
    }
    catch (Exception localException)
    {
      while (true)
      {
        int i = 0;
        int j = 0;
        continue;
        if (i != 0)
        {
          if (Integer.parseInt(paramArrayOfString2[j]) != this.mPrefs.getInt(paramString1, 0))
            j++;
        }
        else if (!paramArrayOfString2[j].equals(this.mPrefs.getString(paramString1, "")))
          j++;
      }
    }
  }

  public void showMessage()
  {
    if ((this.a != null) && (!this.a.equals("")))
    {
      new CustomToast(this, this.a, 1).show();
      setMessage("");
    }
  }

  public void showMessage(String paramString)
  {
    new CustomToast(this, paramString, 1).show();
  }

  private final class a extends ArrayAdapter<String>
  {
    private String[] b;
    private String[] c;
    private String d;
    private int e;
    private boolean f;

    public a(Context paramArrayOfString1, String[] paramArrayOfString2, String[] paramString, String arg5)
    {
      super(2130903090, paramArrayOfString2);
      this.b = paramArrayOfString2;
      this.c = paramString;
      this.e = 2130903090;
      Object localObject;
      this.d = localObject;
      try
      {
        Integer.parseInt(paramString[0]);
        this.f = true;
        return;
      }
      catch (Exception localException)
      {
        this.f = false;
      }
    }

    public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if (paramView == null);
      for (View localView = GBPreferenceActivity.this.mInflater.inflate(this.e, null); ; localView = paramView)
      {
        CheckedTextView localCheckedTextView = (CheckedTextView)localView;
        localCheckedTextView.setText(this.b[paramInt]);
        if (this.f)
        {
          if (Integer.parseInt(this.c[paramInt]) == GBPreferenceActivity.this.mPrefs.getInt(this.d, 0))
          {
            localCheckedTextView.setChecked(true);
            return localView;
          }
          localCheckedTextView.setChecked(false);
          return localView;
        }
        if (this.c[paramInt].equals(GBPreferenceActivity.this.mPrefs.getString(this.d, "")))
        {
          localCheckedTextView.setChecked(true);
          return localView;
        }
        localCheckedTextView.setChecked(false);
        return localView;
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.base.GBPreferenceActivity
 * JD-Core Version:    0.6.2
 */