package gbis.gbandroid.activities.base;

import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public abstract class GBWebViewScreen extends GBActivityDialog
{
  protected WebView webView;

  private void a()
  {
    this.webView = ((WebView)findViewById(2131165334));
    this.webView.getSettings().setJavaScriptEnabled(true);
    this.webView.loadUrl(getUrl());
    this.webView.setWebViewClient(new a((byte)0));
  }

  protected abstract String getUrl();

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    a();
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 4)
    {
      if (this.webView.canGoBack())
        this.webView.goBack();
      while (true)
      {
        return true;
        finish();
      }
    }
    return false;
  }

  private final class a extends WebViewClient
  {
    private a()
    {
    }

    public final boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
    {
      paramWebView.loadUrl(paramString);
      return true;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.base.GBWebViewScreen
 * JD-Core Version:    0.6.2
 */