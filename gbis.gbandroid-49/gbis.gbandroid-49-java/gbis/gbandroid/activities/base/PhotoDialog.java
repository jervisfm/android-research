package gbis.gbandroid.activities.base;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import gbis.gbandroid.entities.Station;
import gbis.gbandroid.utils.CustomAsyncTask;
import gbis.gbandroid.utils.FeaturesUtils;
import gbis.gbandroid.views.CustomDialog;
import gbis.gbandroid.views.CustomDialog.Builder;
import java.io.ByteArrayOutputStream;
import java.io.File;

public abstract class PhotoDialog extends GBActivity
{
  private final int a = 0;
  private final int b = 1;
  private Uri c;
  private Bitmap d;
  private CustomDialog e;
  protected double latitude;
  protected double longitude;
  protected Station station;
  protected String title;

  private void a()
  {
    this.c = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), "temp" + String.valueOf(System.currentTimeMillis()) + ".jpg"));
    Intent localIntent = new Intent("android.media.action.IMAGE_CAPTURE");
    localIntent.putExtra("output", this.c);
    startActivityForResult(localIntent, 0);
  }

  private void a(int paramInt)
  {
    if (paramInt != 0)
    {
      Matrix localMatrix = new Matrix();
      localMatrix.preRotate(paramInt);
      this.d = Bitmap.createBitmap(this.d, 0, 0, this.d.getWidth(), this.d.getHeight(), localMatrix, true);
    }
  }

  private void a(byte[] paramArrayOfByte)
  {
    showMessage(getString(2131296312));
    new a(this, paramArrayOfByte).execute(new Object[0]);
  }

  private static byte[] a(Bitmap paramBitmap)
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    paramBitmap.compress(Bitmap.CompressFormat.PNG, 100, localByteArrayOutputStream);
    paramBitmap.recycle();
    return localByteArrayOutputStream.toByteArray();
  }

  private void b()
  {
    this.d = BitmapFactory.decodeFile(this.c.getPath(), c());
    if (this.d != null)
    {
      File localFile = new File(this.c.getPath());
      if (Integer.parseInt(Build.VERSION.SDK) >= 5)
        a(FeaturesUtils.rotateCameraPhoto(localFile));
      if (localFile.exists())
        localFile.delete();
      postPhotoResized();
      return;
    }
    getLastTakenPicture();
  }

  private static BitmapFactory.Options c()
  {
    BitmapFactory.Options localOptions = new BitmapFactory.Options();
    localOptions.inSampleSize = 4;
    return localOptions;
  }

  protected abstract void getIntentExtras();

  public void getLastTakenPicture()
  {
    while (true)
    {
      Object localObject1;
      try
      {
        this.c = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), "/dcim/100media"));
        File[] arrayOfFile = new File(this.c.getPath()).listFiles();
        localObject1 = arrayOfFile[0];
        int i = arrayOfFile.length;
        int j = 0;
        if (j >= i)
        {
          this.c = Uri.fromFile((File)localObject1);
          this.d = BitmapFactory.decodeFile(((File)localObject1).getAbsolutePath(), c());
          if (Integer.parseInt(Build.VERSION.SDK) >= 5)
            a(FeaturesUtils.rotateCameraPhoto((File)localObject1));
          if (((File)localObject1).exists())
            ((File)localObject1).delete();
          postPhotoResized();
          return;
        }
        localObject2 = arrayOfFile[j];
        long l1 = ((File)localObject1).lastModified();
        long l2 = ((File)localObject2).lastModified();
        if (l1 < l2)
        {
          j++;
          localObject1 = localObject2;
          continue;
        }
      }
      catch (Exception localException)
      {
        showMessage(getString(2131296314));
        return;
      }
      Object localObject2 = localObject1;
    }
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    switch (paramInt1)
    {
    default:
    case 0:
    case 1:
    }
    do
    {
      do
        return;
      while ((paramInt2 == 0) || (paramInt2 != -1));
      postPhotoTaken();
      try
      {
        b();
        return;
      }
      catch (Exception localException2)
      {
        try
        {
          getLastTakenPicture();
          return;
        }
        catch (Exception localException3)
        {
          showMessage(getString(2131296314));
          return;
        }
      }
    }
    while ((paramInt2 == 0) || (paramInt2 != -1));
    try
    {
      uploadPicture(paramIntent.getData());
      return;
    }
    catch (Exception localException1)
    {
      showMessage(getString(2131296314));
    }
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    getIntentExtras();
    showCameraDialog();
  }

  public void onDestroy()
  {
    super.onDestroy();
    if (this.e != null)
      this.e.dismiss();
  }

  protected abstract void postPhotoResized();

  protected abstract void postPhotoTaken();

  protected abstract void queryPhotoUploaderWebService(byte[] paramArrayOfByte);

  public void showCameraDialog()
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this);
    View localView = this.mInflater.inflate(2130903062, null);
    if (this.title == null)
    {
      localBuilder.setTitle(this.station.getStationName());
      localBuilder.setStationLogo(this.station.getGasBrandId(), "/Android/data/gbis.gbandroid/cache/Brands");
    }
    while (true)
    {
      localBuilder.setContentView(localView);
      ListView localListView = (ListView)localView.findViewById(2131165255);
      String[] arrayOfString = new String[2];
      arrayOfString[0] = getString(2131296682);
      arrayOfString[1] = getString(2131296683);
      localListView.setAdapter(new ArrayAdapter(this, 2130903090, arrayOfString));
      localListView.setOnItemClickListener(new ab(this, arrayOfString));
      this.e = localBuilder.create();
      this.e.setOnCancelListener(new ac(this));
      this.e.show();
      return;
      localBuilder.setTitle(this.title);
    }
  }

  protected void uploadPhoto()
  {
    this.e.cancel();
    a(a(this.d));
    this.d.recycle();
  }

  public void uploadPicture(Uri paramUri)
  {
    Cursor localCursor = managedQuery(paramUri, new String[] { "_data", "latitude", "longitude", "orientation" }, null, null, null);
    int i = localCursor.getColumnIndexOrThrow("_data");
    localCursor.moveToFirst();
    this.latitude = localCursor.getDouble(localCursor.getColumnIndexOrThrow("latitude"));
    this.longitude = localCursor.getDouble(localCursor.getColumnIndexOrThrow("longitude"));
    int j = localCursor.getInt(localCursor.getColumnIndexOrThrow("orientation"));
    this.d = BitmapFactory.decodeFile(localCursor.getString(i), c());
    a(j);
    File localFile = new File(paramUri.getPath());
    if (localFile.exists())
      localFile.delete();
    postPhotoResized();
  }

  private final class a extends CustomAsyncTask
  {
    private byte[] b;

    public a(GBActivity paramArrayOfByte, byte[] arg3)
    {
      super();
      Object localObject;
      this.b = localObject;
    }

    protected final void onCancelled()
    {
      super.onCancelled();
      PhotoDialog.this.finish();
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      try
      {
        PhotoDialog.this.progress.dismiss();
        label15: if (!paramBoolean.booleanValue())
          PhotoDialog.this.finish();
        return;
      }
      catch (Exception localException)
      {
        break label15;
      }
    }

    protected final boolean queryWebService()
    {
      PhotoDialog.this.queryPhotoUploaderWebService(this.b);
      return true;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.base.PhotoDialog
 * JD-Core Version:    0.6.2
 */