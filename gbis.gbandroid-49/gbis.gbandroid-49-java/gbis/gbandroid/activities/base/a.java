package gbis.gbandroid.activities.base;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import gbis.gbandroid.services.GPSService;
import gbis.gbandroid.services.GPSService.GPSBinder;

final class a
  implements ServiceConnection
{
  a(GBActivity paramGBActivity)
  {
  }

  public final void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
  {
    GBActivity.a(this.a, ((GPSService.GPSBinder)paramIBinder).getService());
    GBActivity.a(this.a).startLocationListener();
    this.a.onGPSServiceConnected();
  }

  public final void onServiceDisconnected(ComponentName paramComponentName)
  {
    GBActivity.a(this.a, null);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.base.a
 * JD-Core Version:    0.6.2
 */