package gbis.gbandroid.activities.base;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

final class ab
  implements AdapterView.OnItemClickListener
{
  ab(PhotoDialog paramPhotoDialog, String[] paramArrayOfString)
  {
  }

  public final void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    this.a.setAnalyticsTrackEventScreenButton(this.b[paramInt]);
    if (paramInt == 0)
    {
      PhotoDialog.a(this.a);
      return;
    }
    Intent localIntent = new Intent();
    localIntent.setType("image/*");
    localIntent.setAction("android.intent.action.GET_CONTENT");
    this.a.startActivityForResult(localIntent, 1);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.base.ab
 * JD-Core Version:    0.6.2
 */