package gbis.gbandroid.activities.base;

import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;

final class c
  implements DialogInterface.OnCancelListener
{
  c(GBActivity paramGBActivity, Thread paramThread)
  {
  }

  public final void onCancel(DialogInterface paramDialogInterface)
  {
    if (this.b != null)
      this.b.interrupt();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.base.c
 * JD-Core Version:    0.6.2
 */