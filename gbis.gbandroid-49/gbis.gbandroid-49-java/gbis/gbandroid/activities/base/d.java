package gbis.gbandroid.activities.base;

import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import gbis.gbandroid.utils.CustomAsyncTask;

final class d
  implements DialogInterface.OnCancelListener
{
  d(GBActivity paramGBActivity, CustomAsyncTask paramCustomAsyncTask)
  {
  }

  public final void onCancel(DialogInterface paramDialogInterface)
  {
    if (this.b != null)
      this.b.cancel(true);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.base.d
 * JD-Core Version:    0.6.2
 */