package gbis.gbandroid.activities.base;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckedTextView;

final class h
  implements View.OnClickListener
{
  h(GBActivity paramGBActivity, CheckedTextView paramCheckedTextView)
  {
  }

  public final void onClick(View paramView)
  {
    CheckedTextView localCheckedTextView = (CheckedTextView)paramView;
    boolean bool1;
    boolean bool3;
    if (localCheckedTextView.isChecked())
    {
      bool1 = false;
      localCheckedTextView.setChecked(bool1);
      boolean bool2 = this.b.isChecked();
      bool3 = false;
      if (!bool2)
        break label79;
    }
    while (true)
    {
      SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(this.a.getBaseContext()).edit();
      localEditor.putBoolean("smartPromptPreference", bool3);
      localEditor.commit();
      return;
      bool1 = true;
      break;
      label79: bool3 = true;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.base.h
 * JD-Core Version:    0.6.2
 */