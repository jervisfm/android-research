package gbis.gbandroid.activities.base;

import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.widget.CheckedTextView;
import gbis.gbandroid.entities.SmartPrompt;

final class i
  implements DialogInterface.OnCancelListener
{
  i(GBActivity paramGBActivity, SmartPrompt paramSmartPrompt, CheckedTextView paramCheckedTextView)
  {
  }

  public final void onCancel(DialogInterface paramDialogInterface)
  {
    GBActivity localGBActivity = this.a;
    int i = this.b.getPromptId();
    if (this.c.isChecked());
    for (boolean bool = false; ; bool = true)
    {
      localGBActivity.onSmartPromptCancelled(paramDialogInterface, i, bool);
      return;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.base.i
 * JD-Core Version:    0.6.2
 */