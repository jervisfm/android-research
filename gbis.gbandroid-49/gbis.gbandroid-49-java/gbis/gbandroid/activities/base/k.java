package gbis.gbandroid.activities.base;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

final class k
  implements View.OnClickListener
{
  k(GBActivityDialog paramGBActivityDialog, View.OnClickListener paramOnClickListener)
  {
  }

  public final void onClick(View paramView)
  {
    this.a.setAnalyticsTrackEventScreenButton(((Button)paramView).getText().toString());
    this.b.onClick(paramView);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.base.k
 * JD-Core Version:    0.6.2
 */