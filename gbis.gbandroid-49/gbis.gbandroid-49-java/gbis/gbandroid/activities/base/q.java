package gbis.gbandroid.activities.base;

import android.text.Editable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;

final class q
  implements View.OnClickListener
{
  q(GBActivitySearch paramGBActivitySearch)
  {
  }

  public final void onClick(View paramView)
  {
    this.a.setAnalyticsTrackEventScreenButton(this.a.getString(2131296605));
    if ((this.a.searchCityZip.getText() != null) && (!this.a.searchCityZip.getText().toString().equals("")))
    {
      this.a.resetFocus();
      GBActivitySearch.a(this.a, 0);
      return;
    }
    this.a.showMessage(this.a.getString(2131296285));
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.base.q
 * JD-Core Version:    0.6.2
 */