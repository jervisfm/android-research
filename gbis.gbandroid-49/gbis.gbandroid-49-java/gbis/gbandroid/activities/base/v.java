package gbis.gbandroid.activities.base;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.AutoCompleteTextView;
import java.util.regex.Pattern;

final class v
  implements TextWatcher
{
  v(GBActivitySearch paramGBActivitySearch)
  {
  }

  public final void afterTextChanged(Editable paramEditable)
  {
  }

  public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
  }

  public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
    if (!Pattern.matches(".*\\p{Digit}.*", paramCharSequence))
    {
      if (paramCharSequence.length() == 1)
        GBActivitySearch.c(this.a);
      if (paramCharSequence.length() != 2)
        break label63;
      new GBActivitySearch.c(this.a, this.a.getActivity()).execute(new Object[0]);
    }
    label63: 
    while ((!this.a.searchCityZip.hasFocus()) || (paramCharSequence.length() > 0))
      return;
    this.a.setAutocompleteWithQuickLinks();
    GBActivitySearch.d(this.a);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.base.v
 * JD-Core Version:    0.6.2
 */