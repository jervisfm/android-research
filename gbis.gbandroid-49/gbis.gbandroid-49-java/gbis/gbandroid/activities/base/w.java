package gbis.gbandroid.activities.base;

import android.text.Editable;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.AutoCompleteTextView;

final class w
  implements View.OnKeyListener
{
  w(GBActivitySearch paramGBActivitySearch)
  {
  }

  public final boolean onKey(View paramView, int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 66) && (paramKeyEvent.getAction() == 1))
    {
      if (!this.a.searchCityZip.getText().toString().equals(""))
      {
        GBActivitySearch.a(this.a, 0);
        this.a.resetFocus();
        return true;
      }
      this.a.showMessage(this.a.getString(2131296285));
      return true;
    }
    return false;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.base.w
 * JD-Core Version:    0.6.2
 */