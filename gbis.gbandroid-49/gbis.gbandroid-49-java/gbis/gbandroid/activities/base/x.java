package gbis.gbandroid.activities.base;

import android.content.res.Resources;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.animation.Animation;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import gbis.gbandroid.activities.search.MainScreen;
import gbis.gbandroid.animations.MyScaleAnimation;

final class x
  implements View.OnFocusChangeListener
{
  x(GBActivitySearch paramGBActivitySearch)
  {
  }

  public final void onFocusChange(View paramView, boolean paramBoolean)
  {
    if (paramBoolean)
    {
      this.a.searchCityZip.setHint("");
      if ((this.a instanceof MainScreen))
      {
        ImageView localImageView2 = (ImageView)this.a.findViewById(2131165242);
        localImageView2.setFocusable(false);
        localImageView2.startAnimation(new MyScaleAnimation(1.0F, 1.0F, 1.0F, 0.0F, GBActivitySearch.e(this.a), localImageView2, true));
        localImageView2.getAnimation().setAnimationListener(new y(this));
      }
    }
    do
    {
      return;
      this.a.searchCityZip.setHint(this.a.getString(2131296721));
    }
    while (!(this.a instanceof MainScreen));
    ImageView localImageView1 = (ImageView)this.a.findViewById(2131165242);
    localImageView1.setVisibility(0);
    ((MainScreen)this.a).resizeElements(this.a.mRes.getConfiguration());
    localImageView1.startAnimation(new MyScaleAnimation(1.0F, 1.0F, 0.0F, 1.0F, GBActivitySearch.e(this.a), localImageView1, false));
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.base.x
 * JD-Core Version:    0.6.2
 */