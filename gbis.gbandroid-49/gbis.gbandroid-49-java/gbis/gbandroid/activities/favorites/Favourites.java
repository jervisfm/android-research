package gbis.gbandroid.activities.favorites;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.location.Location;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputFilter.LengthFilter;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Spinner;
import android.widget.TextView;
import gbis.gbandroid.activities.base.GBActivity;
import gbis.gbandroid.activities.report.ReportPrices;
import gbis.gbandroid.activities.station.StationDetails;
import gbis.gbandroid.content.GBIntent;
import gbis.gbandroid.entities.FavListMessage;
import gbis.gbandroid.entities.FavStationMessage;
import gbis.gbandroid.entities.FavStationResults;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.queries.FavAddListQuery;
import gbis.gbandroid.queries.FavListQuery;
import gbis.gbandroid.queries.FavRemoveListQuery;
import gbis.gbandroid.queries.FavRemoveStationQuery;
import gbis.gbandroid.queries.FavStationListQuery;
import gbis.gbandroid.utils.CustomAsyncTask;
import gbis.gbandroid.utils.DateUtils;
import gbis.gbandroid.utils.VerifyFields;
import gbis.gbandroid.views.CustomDialog.Builder;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class Favourites extends GBActivity
  implements SharedPreferences.OnSharedPreferenceChangeListener, AbsListView.OnScrollListener
{
  private String a;
  private List<FavListMessage> b;
  private List<FavStationMessage> c;
  private int d;
  private int e;
  private int f;
  private Object g;
  private CustomAsyncTask h;
  private ListView i;
  private ListView j;
  private Dialog k;
  private ArrayAdapter<String> l;
  private Spinner m;
  private View n;
  private View o;

  private void a()
  {
    i locali = new i((byte)0);
    this.n = findViewById(2131165347);
    this.o = findViewById(2131165345);
    this.m = ((Spinner)findViewById(2131165337));
    this.n.setOnClickListener(new a(this));
    this.o.setOnClickListener(new g(this));
    this.m.setOnFocusChangeListener(locali);
    b();
  }

  private void a(int paramInt)
  {
    Type localType = new l(this).getType();
    this.mResponseObject = new FavRemoveListQuery(this, this.mPrefs, localType, this.myLocation).getResponseObject(paramInt);
  }

  private void a(Bundle paramBundle)
  {
    int i1 = paramBundle.getInt("sm_id");
    int i2 = this.c.size();
    for (int i3 = 0; ; i3++)
    {
      if (i3 >= i2);
      while (true)
      {
        this.j = ((ListView)findViewById(16908298));
        ((f)this.j.getAdapter()).notifyDataSetChanged();
        return;
        FavStationMessage localFavStationMessage = (FavStationMessage)this.c.get(i3);
        if (localFavStationMessage.getStationId() != i1)
          break;
        if (this.a.equals(this.mRes.getStringArray(2131099649)[0]))
        {
          if (paramBundle.getDouble("fuel regular") != 0.0D)
          {
            localFavStationMessage.setRegPrice(paramBundle.getDouble("fuel regular"));
            localFavStationMessage.setRegTimeSpotted(paramBundle.getString("time spotted"));
            localFavStationMessage.setTimeOffset(DateUtils.getNowOffset());
          }
        }
        else if (this.a.equals(this.mRes.getStringArray(2131099649)[1]))
        {
          if (paramBundle.getDouble("fuel midgrade") != 0.0D)
          {
            localFavStationMessage.setMidPrice(paramBundle.getDouble("fuel midgrade"));
            localFavStationMessage.setRegTimeSpotted(paramBundle.getString("time spotted"));
            localFavStationMessage.setTimeOffset(DateUtils.getNowOffset());
          }
        }
        else if (this.a.equals(this.mRes.getStringArray(2131099649)[2]))
        {
          if (paramBundle.getDouble("fuel premium") != 0.0D)
          {
            localFavStationMessage.setPremPrice(paramBundle.getDouble("fuel premium"));
            localFavStationMessage.setRegTimeSpotted(paramBundle.getString("time spotted"));
            localFavStationMessage.setTimeOffset(DateUtils.getNowOffset());
          }
        }
        else if ((this.a.equals(this.mRes.getStringArray(2131099649)[3])) && (paramBundle.getDouble("fuel diesel") != 0.0D))
        {
          localFavStationMessage.setDieselPrice(paramBundle.getDouble("fuel diesel"));
          localFavStationMessage.setRegTimeSpotted(paramBundle.getString("time spotted"));
          localFavStationMessage.setTimeOffset(DateUtils.getNowOffset());
        }
      }
    }
  }

  private void a(View paramView, int paramInt1, int paramInt2)
  {
    ((ProgressBar)paramView.findViewById(2131165355)).setVisibility(0);
    new e(this, paramInt1, paramInt2).execute(new Object[0]);
  }

  private void a(FavStationMessage paramFavStationMessage)
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this, this.mTracker, "ContextMenu");
    View localView = this.mInflater.inflate(2130903052, null);
    localBuilder.setTitle(paramFavStationMessage.getStationName());
    localBuilder.setStationLogo(paramFavStationMessage.getGasBrandId(), "/Android/data/gbis.gbandroid/cache/Brands");
    localBuilder.setContentView(localView);
    this.k = localBuilder.create();
    ListView localListView = (ListView)localView.findViewById(2131165229);
    String[] arrayOfString = this.mRes.getStringArray(2131099663);
    paramFavStationMessage.getStationId();
    if ((this.myLocation == null) || ((this.myLocation.getLatitude() == 0.0D) && (this.myLocation.getLongitude() == 0.0D)))
      arrayOfString[2] = getString(2131296698);
    localListView.setAdapter(new ArrayAdapter(this, 2130903053, arrayOfString));
    localListView.setOnItemClickListener(new h(this, paramFavStationMessage));
    this.k.show();
  }

  private void a(String paramString)
  {
    Type localType = new k(this).getType();
    this.mResponseObject = new FavAddListQuery(this, this.mPrefs, localType, this.myLocation).getResponseObject(paramString);
  }

  private void a(boolean paramBoolean)
  {
    this.m = ((Spinner)findViewById(2131165337));
    this.l = new ArrayAdapter(this, 2130903091, g());
    this.l.setDropDownViewResource(2130903090);
    this.m.setAdapter(this.l);
    this.m.setSelection(this.f);
    this.m.setOnItemSelectedListener(new n(this, paramBoolean));
  }

  private void b()
  {
    this.n.setVisibility(0);
    this.o.setVisibility(4);
    ((LinearLayout)findViewById(2131165344)).setVisibility(8);
    findViewById(2131165349).setVisibility(8);
    c(0);
    this.d = 1;
  }

  private void b(int paramInt)
  {
    Type localType = new m(this).getType();
    this.mResponseObject = new FavRemoveStationQuery(this, this.mPrefs, localType, this.myLocation).getResponseObject(paramInt);
  }

  private void b(FavStationMessage paramFavStationMessage)
  {
    GBIntent localGBIntent = new GBIntent(this, ReportPrices.class, this.myLocation);
    localGBIntent.putExtra("station", paramFavStationMessage);
    localGBIntent.putExtra("favourite id", paramFavStationMessage.getMemberFavId());
    localGBIntent.putExtra("fuel regular", paramFavStationMessage.getRegPrice());
    localGBIntent.putExtra("time spotted regular", paramFavStationMessage.getRegTimeSpotted());
    localGBIntent.putExtra("fuel midgrade", paramFavStationMessage.getMidPrice());
    localGBIntent.putExtra("time spotted midgrade", paramFavStationMessage.getMidTimeSpotted());
    localGBIntent.putExtra("fuel premium", paramFavStationMessage.getPremPrice());
    localGBIntent.putExtra("time spotted premium", paramFavStationMessage.getPremTimeSpotted());
    localGBIntent.putExtra("fuel diesel", paramFavStationMessage.getDieselPrice());
    localGBIntent.putExtra("time spotted diesel", paramFavStationMessage.getDieselTimeSpotted());
    localGBIntent.putExtra("time offset", paramFavStationMessage.getTimeOffset());
    startActivityForResult(localGBIntent, 3);
  }

  private void b(String paramString)
  {
    this.h = new a(this, paramString);
    this.h.execute(new Object[0]);
    loadDialog(getString(2131296402), this.h);
  }

  private void b(boolean paramBoolean)
  {
    this.h = new b(this, paramBoolean);
    this.h.execute(new Object[0]);
    loadDialog(getString(2131296402), this.h);
  }

  private void c()
  {
    Type localType = new i(this).getType();
    this.mResponseObject = new FavListQuery(this, this.mPrefs, localType, this.myLocation).getResponseObject();
    this.b = ((List)this.mResponseObject.getPayload());
  }

  private void c(int paramInt)
  {
    View localView = findViewById(2131165350);
    RelativeLayout.LayoutParams localLayoutParams = (RelativeLayout.LayoutParams)localView.getLayoutParams();
    if (paramInt == 1)
    {
      localLayoutParams.addRule(12, 0);
      localLayoutParams.addRule(2, 2131165349);
    }
    while (true)
    {
      localView.setLayoutParams(localLayoutParams);
      return;
      if (paramInt == 0)
        localLayoutParams.addRule(12, -1);
    }
  }

  private void c(FavStationMessage paramFavStationMessage)
  {
    GBIntent localGBIntent = new GBIntent(this, StationDetails.class, this.myLocation);
    localGBIntent.putExtra("station", paramFavStationMessage);
    localGBIntent.putExtra("favourite id", paramFavStationMessage.getMemberFavId());
    startActivityForResult(localGBIntent, 3);
  }

  private void d()
  {
    Type localType = new j(this).getType();
    this.mResponseObject = new FavStationListQuery(this, this.mPrefs, localType, this.myLocation).getResponseObject(((FavListMessage)this.b.get(this.f)).getListId(), this.d, 50);
    FavStationResults localFavStationResults = (FavStationResults)this.mResponseObject.getPayload();
    this.c = localFavStationResults.getFavStationMessage();
    this.e = localFavStationResults.getTotalStations();
  }

  private void d(int paramInt)
  {
    this.h = new c(this, paramInt);
    this.h.execute(new Object[0]);
    loadDialog(getString(2131296403), this.h);
  }

  private void e()
  {
    if (this.a.equals(getString(2131296713)))
      ((TextView)findViewById(2131165340)).setText(getString(2131296717));
    do
    {
      return;
      if (this.a.equals(getString(2131296714)))
      {
        ((TextView)findViewById(2131165340)).setText(getString(2131296718));
        return;
      }
      if (this.a.equals(getString(2131296715)))
      {
        ((TextView)findViewById(2131165340)).setText(getString(2131296719));
        return;
      }
    }
    while (!this.a.equals(getString(2131296716)));
    ((TextView)findViewById(2131165340)).setText(getString(2131296720));
  }

  private void f()
  {
    f localf = new f(this, this.c);
    e();
    this.j = ((ListView)findViewById(16908298));
    this.j.setAdapter(localf);
    this.j.setOnItemClickListener(new b(this));
    this.j.setOnItemLongClickListener(new c(this));
    int i1;
    int i2;
    if (this.e != 0)
    {
      if (this.e > 50)
      {
        ((LinearLayout)findViewById(2131165344)).setVisibility(0);
        findViewById(2131165349).setVisibility(0);
        c(1);
      }
      i1 = 1 + 50 * (-1 + this.d);
      if (i1 + 50 >= this.e)
      {
        i2 = this.e;
        TextView localTextView = (TextView)findViewById(2131165338);
        String str = getString(2131296366);
        Object[] arrayOfObject = new Object[3];
        arrayOfObject[0] = Integer.valueOf(i1);
        arrayOfObject[1] = Integer.valueOf(i2);
        arrayOfObject[2] = Integer.valueOf(this.e);
        localTextView.setText(String.format(str, arrayOfObject));
      }
    }
    while (true)
    {
      this.j.setOnScrollListener(this);
      return;
      i2 = -1 + (i1 + 50);
      break;
      ((TextView)findViewById(2131165338)).setText(getString(2131296367));
    }
  }

  private String[] g()
  {
    String[] arrayOfString = new String[this.b.size()];
    int i1 = this.b.size();
    for (int i2 = 0; ; i2++)
    {
      if (i2 >= i1)
        return arrayOfString;
      arrayOfString[i2] = ((FavListMessage)this.b.get(i2)).getListName();
    }
  }

  private void h()
  {
    b(true);
  }

  private void i()
  {
    this.h = new g(this);
    this.h.execute(new Object[0]);
    loadDialog(getString(2131296394), this.h);
  }

  private void j()
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this);
    View localView = this.mInflater.inflate(2130903057, null);
    EditText localEditText = (EditText)localView.findViewById(2131165247);
    InputFilter[] arrayOfInputFilter = new InputFilter[1];
    arrayOfInputFilter[0] = new InputFilter.LengthFilter(30);
    localEditText.setFilters(arrayOfInputFilter);
    localEditText.setImeOptions(6);
    localBuilder.setTitle(getString(2131296538));
    localBuilder.setContentView(localView);
    localBuilder.setPositiveButton(getString(2131296629), new d(this, localEditText));
    this.k = localBuilder.create();
    this.k.show();
  }

  private void k()
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this);
    View localView = this.mInflater.inflate(2130903060, null);
    ListView localListView = (ListView)localView.findViewById(2131165255);
    localBuilder.setTitle(getString(2131296539));
    localBuilder.setContentView(localView);
    localListView.setChoiceMode(1);
    localListView.setAdapter(new ArrayAdapter(this, 2130903090, g()));
    localBuilder.setPositiveButton(getString(2131296633), new e(this, localListView));
    this.k = localBuilder.create();
    this.k.show();
  }

  private void l()
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this);
    View localView = this.mInflater.inflate(2130903060, null);
    this.i = ((ListView)localView.findViewById(2131165255));
    localBuilder.setTitle(getString(2131296540));
    localBuilder.setContentView(localView);
    this.i.setChoiceMode(1);
    d locald = new d(this, this.c);
    this.i.setAdapter(locald);
    this.k = localBuilder.create();
    this.k.show();
    this.k.setOnCancelListener(new f(this));
  }

  private void m()
  {
    this.j = ((ListView)findViewById(16908298));
    if (this.j.getFooterViewsCount() == 0)
    {
      f localf = new f(this, new ArrayList());
      View localView = this.mInflater.inflate(2130903106, this.j, false);
      ((TextView)localView.findViewById(2131165422)).setText(getString(2131296364));
      ((TextView)localView.findViewById(2131165423)).setText(getString(2131296365));
      this.j.addFooterView(localView, null, false);
      this.j.setAdapter(localf);
    }
  }

  private void n()
  {
    this.a = this.mPrefs.getString("fuelPreference", "");
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    switch (paramInt1)
    {
    default:
    case 3:
    }
    do
      return;
    while ((paramInt2 == 0) || (paramInt2 != 3));
    if (this.c != null)
    {
      a(paramIntent.getExtras());
      return;
    }
    finish();
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    int i1 = this.mPrefs.getInt("adFavorites", 0);
    String str1 = this.mPrefs.getString("adFavoritesKey", "");
    String str2 = this.mPrefs.getString("adFavoritesUnit", "");
    if (paramConfiguration.orientation == 1)
    {
      setAdConfiguration(str1, str2, i1);
      showAds();
      int i2 = this.n.getVisibility();
      int i3 = this.o.getVisibility();
      int i4 = this.d;
      setContentView(2130903077);
      a();
      this.d = i4;
      this.n.setVisibility(i2);
      this.o.setVisibility(i3);
      if ((this.b == null) || (this.b.size() <= 0))
        break label217;
      a(false);
      if ((this.h != null) && ((this.h instanceof g)) && (this.h.getStatus() == AsyncTask.Status.FINISHED))
        f();
    }
    while (true)
    {
      if (paramConfiguration.orientation != 1)
        break label254;
      showAds();
      return;
      setAdConfiguration(str1, str2, i1);
      hideAds();
      break;
      label217: if ((this.h != null) && ((this.h instanceof b)) && (this.h.getStatus() == AsyncTask.Status.FINISHED))
        m();
    }
    label254: if (((LinearLayout)findViewById(2131165344)).getVisibility() == 0)
    {
      c(1);
      return;
    }
    c(0);
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.g = getLastNonConfigurationInstance();
    setContentView(2130903077);
    a();
    n();
    if (this.g != null)
    {
      this.f = ((Bundle)this.g).getInt("listPosition");
      this.d = ((Bundle)this.g).getInt("page");
    }
    while (true)
    {
      h();
      return;
      this.f = -1;
    }
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    super.onCreateOptionsMenu(paramMenu);
    getMenuInflater().inflate(2131427329, paramMenu);
    return true;
  }

  protected void onDestroy()
  {
    super.onDestroy();
    this.mPrefs.unregisterOnSharedPreferenceChangeListener(this);
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    super.onOptionsItemSelected(paramMenuItem);
    switch (paramMenuItem.getItemId())
    {
    default:
      return false;
    case 2131165576:
      j();
      return true;
    case 2131165577:
      k();
      return true;
    case 2131165578:
      l();
      return true;
    case 2131165579:
    }
    launchSettings();
    return true;
  }

  public boolean onPrepareOptionsMenu(Menu paramMenu)
  {
    if ((this.b == null) || (this.b.size() == 0))
    {
      paramMenu.getItem(1).setEnabled(false);
      paramMenu.getItem(2).setEnabled(false);
    }
    while ((this.c == null) || (this.c.size() == 0))
    {
      paramMenu.getItem(2).setEnabled(false);
      return true;
      paramMenu.getItem(1).setEnabled(true);
      paramMenu.getItem(2).setEnabled(true);
    }
    paramMenu.getItem(2).setEnabled(true);
    return true;
  }

  public void onResume()
  {
    super.onResume();
    n();
    this.mPrefs.registerOnSharedPreferenceChangeListener(this);
  }

  public Object onRetainNonConfigurationInstance()
  {
    Bundle localBundle = new Bundle();
    localBundle.putInt("listPosition", this.f);
    localBundle.putInt("page", this.d);
    return localBundle;
  }

  public void onScroll(AbsListView paramAbsListView, int paramInt1, int paramInt2, int paramInt3)
  {
    if ((paramInt1 == 0) && (this.mRes.getConfiguration().orientation == 1))
    {
      showAds();
      return;
    }
    hideAds();
  }

  public void onScrollStateChanged(AbsListView paramAbsListView, int paramInt)
  {
  }

  public void onSharedPreferenceChanged(SharedPreferences paramSharedPreferences, String paramString)
  {
    n();
    if (paramString.equals("fuelPreference"))
      if ((this.b != null) && (this.b.size() > 0))
        i();
    while (!paramString.equals("member_id"))
      return;
    finish();
  }

  protected void setAdsAfterGPSServiceConnected()
  {
    int i1 = this.mPrefs.getInt("adFavorites", 0);
    setAdConfiguration(this.mPrefs.getString("adFavoritesKey", ""), this.mPrefs.getString("adFavoritesUnit", ""), i1);
  }

  protected String setAnalyticsPageName()
  {
    return getString(2131296835);
  }

  private final class a extends CustomAsyncTask
  {
    private String b;

    public a(GBActivity paramString, String arg3)
    {
      super();
      Object localObject;
      this.b = localObject;
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      try
      {
        Favourites.c(Favourites.this).dismiss();
        label15: if (paramBoolean.booleanValue())
        {
          if (!((Boolean)Favourites.o(Favourites.this).getPayload()).booleanValue())
            break label75;
          Favourites.p(Favourites.this);
          Favourites.this.setMessage(Favourites.this.getString(2131296369));
        }
        while (true)
        {
          Favourites.q(Favourites.this).dismiss();
          return;
          label75: Favourites.this.setMessage(Favourites.this.getString(2131296370));
        }
      }
      catch (Exception localException)
      {
        break label15;
      }
    }

    protected final boolean queryWebService()
    {
      Favourites.a(Favourites.this, this.b);
      return true;
    }
  }

  private final class b extends CustomAsyncTask
  {
    private boolean b;

    public b(GBActivity paramBoolean, boolean arg3)
    {
      super();
      boolean bool;
      this.b = bool;
    }

    protected final void onCancelled()
    {
      super.onCancelled();
      Favourites.this.finish();
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      try
      {
        Favourites.c(Favourites.this).dismiss();
        label15: if ((paramBoolean.booleanValue()) && (Favourites.l(Favourites.this) != null))
        {
          if (Favourites.l(Favourites.this).size() <= 0)
          {
            Favourites.this.showMessage(Favourites.this.getString(2131296363));
            Favourites.m(Favourites.this);
            this.b = false;
          }
          Favourites.a(Favourites.this, this.b);
        }
        return;
      }
      catch (Exception localException)
      {
        break label15;
      }
    }

    protected final boolean queryWebService()
    {
      Favourites.n(Favourites.this);
      return true;
    }
  }

  private final class c extends CustomAsyncTask
  {
    private int b;

    public c(GBActivity paramInt, int arg3)
    {
      super();
      int i;
      this.b = i;
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      if (paramBoolean.booleanValue())
      {
        if (!((Boolean)Favourites.o(Favourites.this).getPayload()).booleanValue())
          break label76;
        Favourites.r(Favourites.this).setSelection(0);
        Favourites.s(Favourites.this);
        Favourites.this.setMessage(Favourites.this.getString(2131296371));
      }
      while (true)
      {
        Favourites.q(Favourites.this).dismiss();
        return;
        label76: Favourites.this.setMessage(Favourites.this.getString(2131296372));
      }
    }

    protected final boolean queryWebService()
    {
      Favourites.a(Favourites.this, this.b);
      return true;
    }
  }

  private final class d extends ArrayAdapter<FavStationMessage>
  {
    private List<FavStationMessage> b;
    private int c;

    public d(int arg2)
    {
      super(2130903078, localList);
      this.b = localList;
      this.c = 2130903078;
    }

    public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      Favourites.k localk;
      FavStationMessage localFavStationMessage;
      TextView localTextView;
      StringBuilder localStringBuilder;
      if (paramView == null)
      {
        paramView = Favourites.a(Favourites.this).inflate(this.c, null);
        localk = new Favourites.k();
        localk.a = ((TextView)paramView.findViewById(2131165351));
        localk.b = ((TextView)paramView.findViewById(2131165352));
        localk.c = ((TextView)paramView.findViewById(2131165353));
        localk.d = ((ProgressBar)paramView.findViewById(2131165355));
        localk.e = ((Button)paramView.findViewById(2131165357));
        paramView.setTag(localk);
        localFavStationMessage = (FavStationMessage)this.b.get(paramInt);
        if (localFavStationMessage != null)
        {
          localk.a.setText(localFavStationMessage.getStationName());
          localk.b.setText(localFavStationMessage.getAddress());
          localTextView = localk.c;
          localStringBuilder = new StringBuilder(String.valueOf(localFavStationMessage.getCity()));
          if ((localFavStationMessage.getState() == null) || (localFavStationMessage.getState().equals("")))
            break label276;
        }
      }
      label276: for (String str = ", " + localFavStationMessage.getState(); ; str = "")
      {
        localTextView.setText(str);
        localk.d.setVisibility(4);
        localk.e.setOnClickListener(new o(this, paramView, localFavStationMessage, paramInt));
        return paramView;
        localk = (Favourites.k)paramView.getTag();
        break;
      }
    }
  }

  private final class e extends CustomAsyncTask
  {
    private int b;
    private int c;

    public e(GBActivity paramInt1, int paramInt2, int arg4)
    {
      super();
      this.b = paramInt2;
      int i;
      this.c = i;
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      View localView = Favourites.t(Favourites.this).getChildAt(this.c);
      if (paramBoolean.booleanValue())
        if (((Boolean)Favourites.o(Favourites.this).getPayload()).booleanValue())
          locald = (Favourites.d)Favourites.t(Favourites.this).getAdapter();
      while (localView == null)
      {
        do
        {
          try
          {
            localFavStationMessage = (FavStationMessage)locald.getItem(this.c);
            locald.remove(localFavStationMessage);
            locald.notifyDataSetChanged();
            if (locald.isEmpty())
              Favourites.q(Favourites.this).cancel();
            return;
          }
          catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
          {
            while (true)
            {
              Favourites.d locald;
              FavStationMessage localFavStationMessage = (FavStationMessage)locald.getItem(-1 + Favourites.t(Favourites.this).getChildCount());
            }
          }
          Favourites.this.showMessage(Favourites.this.getString(2131296374));
        }
        while (localView == null);
        ((ProgressBar)localView.findViewById(2131165355)).setVisibility(4);
        return;
      }
      ((ProgressBar)localView.findViewById(2131165355)).setVisibility(4);
    }

    protected final boolean queryWebService()
    {
      Favourites.b(Favourites.this, this.b);
      return true;
    }
  }

  private final class f extends ArrayAdapter<FavStationMessage>
  {
    private List<FavStationMessage> b;
    private int c;
    private Favourites.h d;

    public f(int arg2)
    {
      super(2130903079, localList);
      this.b = localList;
      this.c = 2130903079;
    }

    public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      Favourites.j localj;
      String str;
      label291: double d1;
      if (paramView == null)
      {
        paramView = Favourites.a(Favourites.this).inflate(this.c, null);
        localj = new Favourites.j();
        localj.a = ((TextView)paramView.findViewById(2131165351));
        localj.b = ((TextView)paramView.findViewById(2131165352));
        localj.c = ((TextView)paramView.findViewById(2131165353));
        localj.d = ((TextView)paramView.findViewById(2131165360));
        localj.e = ((TextView)paramView.findViewById(2131165361));
        localj.f = ((TextView)paramView.findViewById(2131165363));
        localj.h = ((TextView)paramView.findViewById(2131165362));
        localj.g = ((TextView)paramView.findViewById(2131165364));
        localj.i = ((RelativeLayout)paramView.findViewById(2131165358));
        paramView.setTag(localj);
        FavStationMessage localFavStationMessage = (FavStationMessage)this.b.get(paramInt);
        if (localFavStationMessage != null)
        {
          this.d = new Favourites.h(Favourites.this, localFavStationMessage);
          localj.a.setText(localFavStationMessage.getStationName());
          localj.b.setText(localFavStationMessage.getAddress());
          TextView localTextView = localj.c;
          StringBuilder localStringBuilder = new StringBuilder(String.valueOf(localFavStationMessage.getCity()));
          if ((localFavStationMessage.getState() == null) || (localFavStationMessage.getState().equals("")))
            break label502;
          str = ", " + localFavStationMessage.getState();
          localTextView.setText(str);
          d1 = localFavStationMessage.getPrice(Favourites.this, Favourites.b(Favourites.this));
          if (d1 <= 0.0D)
            break label549;
          if (!localFavStationMessage.getCountry().equals("USA"))
            break label509;
          localj.d.setText(VerifyFields.doubleToScale(d1, 2));
          localj.e.setVisibility(0);
          label380: localj.f.setText(Favourites.this.getString(2131296302));
          localj.h.setText(localFavStationMessage.getTimeSpottedConverted(Favourites.this, Favourites.b(Favourites.this)));
          localj.h.setVisibility(0);
          label430: switch (localFavStationMessage.getMslMatchStatus())
          {
          default:
          case 1:
          case 2:
          case -1:
          case -4:
          case -3:
          case -2:
          case 0:
          }
        }
      }
      while (true)
      {
        localj.i.setOnClickListener(this.d);
        return paramView;
        localj = (Favourites.j)paramView.getTag();
        break;
        label502: str = "";
        break label291;
        label509: localj.d.setText(VerifyFields.doubleToScale(d1, 1));
        localj.e.setVisibility(8);
        break label380;
        label549: localj.d.setText("--");
        localj.f.setText(Favourites.this.getString(2131296301));
        localj.e.setVisibility(8);
        localj.h.setVisibility(4);
        break label430;
        localj.g.setText("Matched");
        continue;
        localj.g.setText("Changed");
        continue;
        localj.g.setText("Not Matched");
      }
    }
  }

  private final class g extends CustomAsyncTask
  {
    public g(GBActivity arg2)
    {
      super();
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      try
      {
        Favourites.c(Favourites.this).dismiss();
        label15: if (paramBoolean.booleanValue())
        {
          Favourites.d(Favourites.this);
          if (Favourites.e(Favourites.this) != null)
          {
            if (Favourites.f(Favourites.this) >= 50)
              break label90;
            ((LinearLayout)Favourites.this.findViewById(2131165344)).setVisibility(8);
            Favourites.this.findViewById(2131165349).setVisibility(8);
            Favourites.g(Favourites.this);
          }
        }
        label90: 
        do
        {
          return;
          if (Favourites.h(Favourites.this) == 1 + (-1 + Favourites.f(Favourites.this)) / 50)
            Favourites.i(Favourites.this).setVisibility(4);
        }
        while (Favourites.h(Favourites.this) == 1);
        Favourites.j(Favourites.this).setVisibility(0);
        return;
      }
      catch (Exception localException)
      {
        break label15;
      }
    }

    protected final boolean queryWebService()
    {
      Favourites.k(Favourites.this);
      return true;
    }
  }

  private final class h
    implements View.OnClickListener
  {
    private FavStationMessage b;

    public h(FavStationMessage arg2)
    {
      Object localObject;
      this.b = localObject;
    }

    public final void onClick(View paramView)
    {
      Favourites.b(Favourites.this, Favourites.this.getString(2131296647));
      Favourites.a(Favourites.this, this.b);
    }
  }

  private final class i
    implements View.OnFocusChangeListener
  {
    private i()
    {
    }

    public final void onFocusChange(View paramView, boolean paramBoolean)
    {
      if (paramBoolean);
      try
      {
        Favourites.u(Favourites.this).getChildAt(0).setFocusable(true);
        return;
        Favourites.u(Favourites.this).requestFocus();
        return;
      }
      catch (Exception localException)
      {
      }
    }
  }

  static final class j
  {
    TextView a;
    TextView b;
    TextView c;
    TextView d;
    TextView e;
    TextView f;
    TextView g;
    TextView h;
    RelativeLayout i;
  }

  static final class k
  {
    TextView a;
    TextView b;
    TextView c;
    ProgressBar d;
    Button e;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.favorites.Favourites
 * JD-Core Version:    0.6.2
 */