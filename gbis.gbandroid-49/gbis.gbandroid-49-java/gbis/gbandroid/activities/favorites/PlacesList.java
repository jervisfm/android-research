package gbis.gbandroid.activities.favorites;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import gbis.gbandroid.activities.base.GBActivitySearch;
import gbis.gbandroid.views.CustomDialog.Builder;

public class PlacesList extends GBActivitySearch
{
  private Cursor b;
  private ListView c;
  private boolean d;
  private a e;
  private boolean f;

  private void a()
  {
    ((TextView)findViewById(2131165395)).setText(getString(2131296544));
    ((Button)findViewById(2131165396)).setOnClickListener(new p(this));
  }

  private void a(String paramString, long paramLong)
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this);
    View localView = this.mInflater.inflate(2130903057, null);
    TextView localTextView = (TextView)localView.findViewById(2131165246);
    EditText localEditText = (EditText)localView.findViewById(2131165247);
    localBuilder.setTitle(getString(2131296544));
    localBuilder.setContentView(localView);
    localTextView.setText(getString(2131296573));
    localEditText.setText(paramString);
    localBuilder.setPositiveButton(getString(2131296610), new r(this, localEditText, paramLong));
    localBuilder.create().show();
  }

  private void b()
  {
    this.c = ((ListView)findViewById(2131165397));
    this.c.setOnItemClickListener(new q(this));
  }

  private void c()
  {
    this.b = getAllPlaces(0);
    this.e = new a(this, this.b);
    d();
    this.c.setAdapter(this.e);
  }

  private void d()
  {
    if ((this.b.getCount() <= 0) && (this.c.getFooterViewsCount() == 0))
    {
      View localView = this.mInflater.inflate(2130903106, this.c, false);
      ((TextView)localView.findViewById(2131165422)).setText(2131296441);
      ((TextView)localView.findViewById(2131165423)).setText(2131296440);
      this.c.addFooterView(localView, null, false);
    }
  }

  private void e()
  {
    if (this.d);
    for (boolean bool = false; ; bool = true)
    {
      this.d = bool;
      this.e.notifyDataSetChanged();
      if (!this.d)
        break;
      ((Button)findViewById(2131165396)).setVisibility(0);
      return;
    }
    ((Button)findViewById(2131165396)).setVisibility(4);
  }

  private void f()
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this);
    localBuilder.setPositiveButton(getString(2131296666), new s(this));
    localBuilder.setNegativeButton(getString(2131296667), new t(this));
    localBuilder.setTitle(getString(2131296547));
    localBuilder.setMessage(2131296574);
    localBuilder.create().show();
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903099);
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
      this.f = localBundle.getBoolean("WidgetCall", false);
    a();
    b();
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    super.onCreateOptionsMenu(paramMenu);
    getMenuInflater().inflate(2131427333, paramMenu);
    return true;
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    switch (paramInt)
    {
    default:
      return super.onKeyDown(paramInt, paramKeyEvent);
    case 4:
    }
    if (this.d)
      e();
    while (true)
    {
      return true;
      finish();
    }
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    super.onOptionsItemSelected(paramMenuItem);
    switch (paramMenuItem.getItemId())
    {
    default:
      return super.onOptionsItemSelected(paramMenuItem);
    case 2131165592:
      if (paramMenuItem.getTitle().equals(getString(2131296613)))
      {
        this.e.a(1);
        paramMenuItem.setTitle(2131296612);
        return true;
      }
      this.e.a(0);
      paramMenuItem.setTitle(2131296613);
      return true;
    case 2131165593:
      launchSettings();
      return true;
    case 2131165594:
      e();
      return true;
    case 2131165595:
    }
    f();
    return true;
  }

  public boolean onPrepareOptionsMenu(Menu paramMenu)
  {
    return this.b.getCount() > 0;
  }

  public void onResume()
  {
    super.onResume();
    c();
  }

  protected void onStop()
  {
    super.onStop();
    this.b.close();
  }

  protected void setAdsAfterGPSServiceConnected()
  {
  }

  protected String setAnalyticsPageName()
  {
    return getString(2131296851);
  }

  private final class a extends CursorAdapter
  {
    public a(Context paramCursor, Cursor arg3)
    {
      super(localCursor, true);
    }

    public final void a(int paramInt)
    {
      PlacesList.a(PlacesList.this, PlacesList.a(PlacesList.this, paramInt));
      PlacesList.d(PlacesList.this).changeCursor(PlacesList.c(PlacesList.this));
    }

    public final void bindView(View paramView, Context paramContext, Cursor paramCursor)
    {
      PlacesList.b localb = (PlacesList.b)paramView.getTag();
      String str1 = paramCursor.getString(paramCursor.getColumnIndex("table_column_name"));
      String str2 = paramCursor.getString(paramCursor.getColumnIndex("table_column_city_zip"));
      long l = paramCursor.getLong(paramCursor.getColumnIndex("_id"));
      localb.a.setText(str1);
      if ((str2 == null) || (str2.equals("")))
      {
        TextView localTextView = localb.b;
        String str3 = PlacesList.this.getString(2131296438);
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = PlacesList.this.getString(2131296439);
        localTextView.setText(String.format(str3, arrayOfObject));
      }
      while (PlacesList.b(PlacesList.this))
      {
        localb.c.setVisibility(0);
        localb.e.setOnClickListener(new u(this, str1, l));
        localb.d.setOnClickListener(new v(this, str1));
        return;
        localb.b.setText(String.format(PlacesList.this.getString(2131296438), new Object[] { str2 }));
      }
      localb.c.setVisibility(8);
    }

    public final View newView(Context paramContext, Cursor paramCursor, ViewGroup paramViewGroup)
    {
      View localView = PlacesList.a(PlacesList.this).inflate(2130903103, null);
      PlacesList.b localb = new PlacesList.b();
      localb.a = ((TextView)localView.findViewById(2131165412));
      localb.b = ((TextView)localView.findViewById(2131165413));
      localb.d = ((Button)localView.findViewById(2131165416));
      localb.e = ((Button)localView.findViewById(2131165415));
      localb.c = ((RelativeLayout)localView.findViewById(2131165414));
      localView.setTag(localb);
      return localView;
    }
  }

  static final class b
  {
    TextView a;
    TextView b;
    RelativeLayout c;
    Button d;
    Button e;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.favorites.PlacesList
 * JD-Core Version:    0.6.2
 */