package gbis.gbandroid.activities.favorites;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import gbis.gbandroid.entities.FavStationMessage;

final class b
  implements AdapterView.OnItemClickListener
{
  b(Favourites paramFavourites)
  {
  }

  public final void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    FavStationMessage localFavStationMessage = (FavStationMessage)paramAdapterView.getItemAtPosition(paramInt);
    if (localFavStationMessage.getStationId() > 0)
    {
      Favourites.b(this.a, localFavStationMessage);
      return;
    }
    this.a.showMessage(this.a.getString(2131296375));
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.favorites.b
 * JD-Core Version:    0.6.2
 */