package gbis.gbandroid.activities.favorites;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import gbis.gbandroid.entities.FavStationMessage;

final class c
  implements AdapterView.OnItemLongClickListener
{
  c(Favourites paramFavourites)
  {
  }

  public final boolean onItemLongClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    Favourites.c(this.a, (FavStationMessage)paramAdapterView.getItemAtPosition(paramInt));
    Favourites.b(this.a, "ContextMenu");
    return true;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.favorites.c
 * JD-Core Version:    0.6.2
 */