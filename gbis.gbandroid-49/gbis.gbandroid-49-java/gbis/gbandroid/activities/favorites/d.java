package gbis.gbandroid.activities.favorites;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.text.Editable;
import android.widget.EditText;

final class d
  implements DialogInterface.OnClickListener
{
  d(Favourites paramFavourites, EditText paramEditText)
  {
  }

  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    String str = this.b.getText().toString();
    if ((str.equals("")) || (str.replaceAll(" ", "").equals("")))
    {
      this.a.showMessage(this.a.getString(2131296368));
      return;
    }
    Favourites.c(this.a, str);
    paramDialogInterface.dismiss();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.favorites.d
 * JD-Core Version:    0.6.2
 */