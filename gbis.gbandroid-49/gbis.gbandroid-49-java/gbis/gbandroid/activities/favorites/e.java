package gbis.gbandroid.activities.favorites;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.widget.ListView;
import gbis.gbandroid.entities.FavListMessage;
import java.util.List;

final class e
  implements DialogInterface.OnClickListener
{
  e(Favourites paramFavourites, ListView paramListView)
  {
  }

  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    if (this.b.getCheckedItemPosition() != -1)
    {
      Favourites.e(this.a, ((FavListMessage)Favourites.l(this.a).get(this.b.getCheckedItemPosition())).getListId());
      paramDialogInterface.dismiss();
      return;
    }
    this.a.showMessage(this.a.getString(2131296373));
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.favorites.e
 * JD-Core Version:    0.6.2
 */