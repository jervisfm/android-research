package gbis.gbandroid.activities.favorites;

import android.view.View;
import android.view.View.OnClickListener;

final class g
  implements View.OnClickListener
{
  g(Favourites paramFavourites)
  {
  }

  public final void onClick(View paramView)
  {
    Favourites localFavourites = this.a;
    Favourites.c(localFavourites, -1 + Favourites.h(localFavourites));
    Favourites.v(this.a);
    if (Favourites.h(this.a) == 1)
      Favourites.j(this.a).setVisibility(4);
    if (Favourites.i(this.a).getVisibility() == 4)
      Favourites.i(this.a).setVisibility(0);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.favorites.g
 * JD-Core Version:    0.6.2
 */