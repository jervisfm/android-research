package gbis.gbandroid.activities.favorites;

import android.app.Dialog;
import android.location.Location;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import gbis.gbandroid.entities.FavStationMessage;

final class h
  implements AdapterView.OnItemClickListener
{
  h(Favourites paramFavourites, FavStationMessage paramFavStationMessage)
  {
  }

  public final void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    Favourites.q(this.a).dismiss();
    switch (paramInt)
    {
    default:
      return;
    case 0:
      Favourites.a(this.a, this.b);
      return;
    case 1:
      if (this.b.getStationId() > 0)
      {
        Favourites.b(this.a, this.b);
        return;
      }
      this.a.showMessage(this.a.getString(2131296375));
      return;
    case 2:
      if (this.b.getStationId() > 0)
      {
        Location localLocation = Favourites.w(this.a);
        if ((localLocation != null) && (localLocation.getLatitude() != 0.0D) && (localLocation.getLongitude() != 0.0D))
        {
          Favourites.a(this.a, this.b.getLatitude(), this.b.getLongitude(), localLocation);
          return;
        }
        Favourites.a(this.a, this.b);
        return;
      }
      this.a.showMessage(this.a.getString(2131296375));
      return;
    case 3:
      if (this.b.getStationId() > 0)
      {
        Favourites.b(this.a, this.b);
        return;
      }
      this.a.showMessage(this.a.getString(2131296375));
      return;
    case 4:
    }
    Favourites.x(this.a);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.favorites.h
 * JD-Core Version:    0.6.2
 */