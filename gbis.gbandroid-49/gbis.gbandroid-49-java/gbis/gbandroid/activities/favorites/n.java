package gbis.gbandroid.activities.favorites;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;

final class n
  implements AdapterView.OnItemSelectedListener
{
  n(Favourites paramFavourites, boolean paramBoolean)
  {
  }

  public final void onItemSelected(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    if (this.b)
    {
      if (Favourites.e(this.a) == null)
        Favourites.y(this.a);
      Favourites.d(this.a, paramInt);
      Favourites.v(this.a);
    }
  }

  public final void onNothingSelected(AdapterView<?> paramAdapterView)
  {
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.favorites.n
 * JD-Core Version:    0.6.2
 */