package gbis.gbandroid.activities.favorites;

import android.content.Intent;
import android.database.Cursor;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

final class q
  implements AdapterView.OnItemClickListener
{
  q(PlacesList paramPlacesList)
  {
  }

  public final void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    Cursor localCursor = (Cursor)PlacesList.d(this.a).getItem(paramInt);
    if (!PlacesList.g(this.a))
    {
      PlacesList.b(this.a, localCursor);
      return;
    }
    Intent localIntent = new Intent();
    localIntent.putExtra("center latitude", localCursor.getDouble(localCursor.getColumnIndex("table_column_latitude")));
    localIntent.putExtra("center longitude", localCursor.getDouble(localCursor.getColumnIndex("table_column_longitude")));
    localIntent.putExtra("city", localCursor.getString(localCursor.getColumnIndex("table_column_city_zip")));
    localIntent.putExtra("place name", localCursor.getString(localCursor.getColumnIndex("table_column_name")));
    this.a.setResult(-1, localIntent);
    this.a.finish();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.favorites.q
 * JD-Core Version:    0.6.2
 */