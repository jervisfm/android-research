package gbis.gbandroid.activities.favorites;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.database.Cursor;
import android.text.Editable;
import android.widget.EditText;

final class r
  implements DialogInterface.OnClickListener
{
  r(PlacesList paramPlacesList, EditText paramEditText, long paramLong)
  {
  }

  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    String str = this.b.getText().toString();
    if ((!str.equals("")) && (!str.replaceAll(" ", "").equals("")))
    {
      if (PlacesList.a(this.a, this.c, str))
      {
        PlacesList.c(this.a).requery();
        paramDialogInterface.dismiss();
        return;
      }
      this.a.showMessage(this.a.getString(2131296413));
      return;
    }
    this.a.showMessage(this.a.getString(2131296412));
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.favorites.r
 * JD-Core Version:    0.6.2
 */