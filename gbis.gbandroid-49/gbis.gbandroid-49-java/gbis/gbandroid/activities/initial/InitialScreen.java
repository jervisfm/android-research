package gbis.gbandroid.activities.initial;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;
import gbis.gbandroid.activities.base.GBActivity;
import java.util.Calendar;

public class InitialScreen extends GBActivity
{
  private Button a;
  private Button b;
  private int c;
  private int d;
  private b e;

  private void a()
  {
    this.a = ((Button)findViewById(2131165376));
    this.b = ((Button)findViewById(2131165377));
    a locala = new a((byte)0);
    this.a.setOnClickListener(locala);
    this.b.setOnClickListener(locala);
    b();
  }

  private void b()
  {
    RelativeLayout.LayoutParams localLayoutParams = (RelativeLayout.LayoutParams)((ImageView)findViewById(2131165242)).getLayoutParams();
    if (this.d == 0);
    for (int i = 119 * this.c / 760; ; i = 119 * this.c / 800)
    {
      localLayoutParams.height = i;
      localLayoutParams.width = (i * 480 / 119);
      ((ImageView)findViewById(2131165242)).setLayoutParams(localLayoutParams);
      return;
    }
  }

  private void c()
  {
    Calendar localCalendar = Calendar.getInstance();
    SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(getBaseContext()).edit();
    localEditor.putLong("initial_screen_date", localCalendar.getTimeInMillis());
    localEditor.commit();
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.e = new b((byte)0);
    DisplayMetrics localDisplayMetrics = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
    if (getWindowManager().getDefaultDisplay().getHeight() > getWindowManager().getDefaultDisplay().getWidth())
      this.c = localDisplayMetrics.heightPixels;
    for (this.d = 0; ; this.d = 1)
    {
      setContentView(2130903093);
      a();
      c();
      return;
      this.c = localDisplayMetrics.heightPixels;
    }
  }

  public void onDestroy()
  {
    super.onDestroy();
    try
    {
      this.mPrefs.unregisterOnSharedPreferenceChangeListener(this.e);
      return;
    }
    catch (Exception localException)
    {
    }
  }

  public void onStart()
  {
    super.onStart();
    this.mPrefs.registerOnSharedPreferenceChangeListener(this.e);
  }

  protected void setAdsAfterGPSServiceConnected()
  {
  }

  protected String setAnalyticsPageName()
  {
    return getString(2131296812);
  }

  private final class a
    implements View.OnClickListener
  {
    private a()
    {
    }

    public final void onClick(View paramView)
    {
      if (paramView == InitialScreen.a(InitialScreen.this))
        InitialScreen.b(InitialScreen.this);
      while (true)
      {
        InitialScreen.a(InitialScreen.this, ((Button)paramView).getText().toString());
        return;
        InitialScreen.c(InitialScreen.this);
        InitialScreen.this.finish();
      }
    }
  }

  private final class b
    implements SharedPreferences.OnSharedPreferenceChangeListener
  {
    private b()
    {
    }

    public final void onSharedPreferenceChanged(SharedPreferences paramSharedPreferences, String paramString)
    {
      if (paramString.equals("is_member"))
        InitialScreen.this.finish();
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.initial.InitialScreen
 * JD-Core Version:    0.6.2
 */