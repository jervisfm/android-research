package gbis.gbandroid.activities.list;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import gbis.gbandroid.activities.base.GBActivity;
import gbis.gbandroid.entities.ListMessage;
import gbis.gbandroid.entities.ListStationFilter;
import gbis.gbandroid.entities.ListStationTab;
import gbis.gbandroid.entities.StationNameFilter;
import gbis.gbandroid.utils.VerifyFields;
import gbis.gbandroid.views.CustomDialog;
import gbis.gbandroid.views.CustomDialog.Builder;
import gbis.gbandroid.views.FilterButton;
import java.util.ArrayList;
import java.util.List;

public class FilterStations extends GBActivity
  implements DialogInterface.OnDismissListener
{
  private List<StationNameFilter> a;
  private double b;
  private double c;
  private double d;
  private double e;

  private void a(SeekBar paramSeekBar, TextView paramTextView, double paramDouble1, double paramDouble2, double paramDouble3, int paramInt1, int paramInt2, String paramString, int paramInt3, ListStationTab paramListStationTab)
  {
    int i = (int)Math.ceil(paramDouble2 - paramDouble1);
    paramSeekBar.setMax(i);
    paramSeekBar.setProgress(i);
    ListStationFilter localListStationFilter = paramListStationTab.getStationFilter();
    if (paramInt3 == 0)
      if (localListStationFilter.getDistance() == 0.0D)
        localListStationFilter.setDistance(paramDouble2);
    while (true)
    {
      paramTextView.setText(VerifyFields.doubleToScale((paramDouble1 + paramSeekBar.getProgress()) / paramInt1, paramInt2) + " " + paramString);
      paramSeekBar.setOnSeekBarChangeListener(new a(this, paramInt3, localListStationFilter, paramDouble1, paramDouble3, paramInt1, paramTextView, paramInt2, paramString));
      return;
      paramSeekBar.setProgress((int)(Math.ceil(paramDouble3 * localListStationFilter.getDistance() * paramInt1) - paramDouble1));
      continue;
      if (localListStationFilter.getPrice() == 0.0D)
        localListStationFilter.setPrice(paramDouble2);
      else
        paramSeekBar.setProgress((int)(Math.ceil(paramDouble3 * localListStationFilter.getPrice() * paramInt1) - paramDouble1));
    }
  }

  private void a(SeekBar paramSeekBar, TextView paramTextView, int paramInt, ListStationTab paramListStationTab)
  {
    List localList = paramListStationTab.getListStations();
    String str2;
    double d4;
    label93: double d2;
    double d3;
    int i;
    int j;
    double d1;
    String str1;
    switch (paramInt)
    {
    default:
      return;
    case 0:
      if (this.mPrefs.getString("distancePreference", "").equals(""))
        if (((ListMessage)localList.get(0)).getCountry().equals("USA"))
        {
          str2 = "miles";
          if (!str2.equals("miles"))
            break label178;
          d4 = 1.0D;
          d2 = Math.ceil(d4 * this.b);
          d3 = Math.ceil(d4 * this.c);
          i = 1;
          j = 0;
          d1 = d4;
          str1 = str2;
        }
      break;
    case 1:
    }
    while (true)
    {
      a(paramSeekBar, paramTextView, d3, d2, d1, i, j, str1, paramInt, paramListStationTab);
      return;
      str2 = "km";
      break;
      str2 = this.mPrefs.getString("distancePreference", "");
      break;
      label178: d4 = 1.60934D;
      break label93;
      d1 = 1.0D;
      str1 = "";
      if (((ListMessage)localList.get(0)).getCountry().equals("USA"))
      {
        i = 100;
        d2 = 100.0D * this.d;
        d3 = 100.0D * this.e;
        j = 2;
      }
      else
      {
        i = 1;
        d2 = Math.ceil(this.d);
        d3 = Math.ceil(this.e);
        j = 1;
      }
    }
  }

  private void a(ListStationTab paramListStationTab)
  {
    this.a = new ArrayList();
    this.b = 0.0D;
    this.c = 0.0D;
    this.d = 0.0D;
    this.e = 0.0D;
    c(paramListStationTab);
    b(paramListStationTab);
  }

  private static boolean a(double paramDouble, ListMessage paramListMessage)
  {
    return paramListMessage.getPrice() <= paramDouble;
  }

  private static boolean a(ListMessage paramListMessage)
  {
    return paramListMessage.getPrice() > 0.0D;
  }

  private static boolean a(List<StationNameFilter> paramList, ListMessage paramListMessage)
  {
    int i = paramList.size();
    for (int j = 0; ; j++)
    {
      if (j >= i)
        return false;
      StationNameFilter localStationNameFilter = (StationNameFilter)paramList.get(j);
      if (localStationNameFilter.getStationName().toLowerCase().equals(paramListMessage.getStationName().toLowerCase()))
        return localStationNameFilter.isIncluded();
    }
  }

  private static boolean a(List<StationNameFilter> paramList, String paramString)
  {
    int i = paramList.size();
    for (int j = 0; ; j++)
    {
      if (j >= i)
        return false;
      if (((StationNameFilter)paramList.get(j)).getStationName().toLowerCase().equals(paramString.toLowerCase()))
        return true;
    }
  }

  private void b(ListStationTab paramListStationTab)
  {
    if (paramListStationTab.getStationFilter() == null)
      paramListStationTab.setStationFilter(new ListStationFilter(this.d, this.b, this.mPrefs.getBoolean("noPricesPreference", true), this.a));
  }

  private static boolean b(double paramDouble, ListMessage paramListMessage)
  {
    return paramListMessage.getDistance() <= paramDouble;
  }

  private void c(ListStationTab paramListStationTab)
  {
    List localList = paramListStationTab.getListStations();
    int i = localList.size();
    for (int j = 0; ; j++)
    {
      if (j >= i)
      {
        if (this.e == 0.0D)
          this.e = this.d;
        return;
      }
      ListMessage localListMessage = (ListMessage)localList.get(j);
      String str = localListMessage.getStationName();
      if (str != null)
      {
        if (!a(this.a, str))
          this.a.add(new StationNameFilter(str, true));
        double d1 = localListMessage.getDistance();
        if (d1 > this.b)
          this.b = d1;
        if ((d1 < this.c) || (this.c == 0.0D))
          this.c = d1;
        double d2 = localListMessage.getPrice();
        if (d2 > this.d)
          this.d = d2;
        if (((d2 < this.e) || (this.e == 0.0D)) && (d2 != 0.0D))
          this.e = d2;
      }
    }
  }

  public static ListStationFilter createFilter(ListStationTab paramListStationTab, boolean paramBoolean)
  {
    List localList1 = paramListStationTab.getListStations();
    ListStationFilter localListStationFilter = new ListStationFilter();
    List localList2 = localListStationFilter.getStationNames();
    double d1 = localListStationFilter.getDistance();
    double d2 = localListStationFilter.getPrice();
    int i = localList1.size();
    int j = 0;
    if (j >= i)
    {
      localListStationFilter.setDistance(d1);
      localListStationFilter.setPrice(d2);
      localListStationFilter.setHasPrices(paramBoolean);
      return localListStationFilter;
    }
    ListMessage localListMessage = (ListMessage)localList1.get(j);
    String str = localListMessage.getStationName();
    double d4;
    if (str != null)
    {
      if (!a(localList2, str))
        localList2.add(new StationNameFilter(str, true));
      d4 = localListMessage.getDistance();
      if (d4 <= d1)
        break label187;
    }
    while (true)
    {
      double d5 = localListMessage.getPrice();
      double d3;
      if (d5 > d2)
      {
        d1 = d4;
        d3 = d5;
      }
      while (true)
      {
        j++;
        d2 = d3;
        break;
        d3 = d2;
        continue;
        d1 = d4;
        d3 = d2;
      }
      label187: d4 = d1;
    }
  }

  private void d(ListStationTab paramListStationTab)
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this);
    View localView1 = this.mInflater.inflate(2130903059, null);
    FilterButton localFilterButton1 = (FilterButton)localView1.findViewById(2131165252);
    FilterButton localFilterButton2 = (FilterButton)localView1.findViewById(2131165253);
    FilterButton localFilterButton3 = (FilterButton)localView1.findViewById(2131165254);
    ListView localListView = (ListView)localView1.findViewById(2131165255);
    View localView2 = localView1.findViewById(2131165256);
    View localView3 = localView1.findViewById(2131165257);
    SeekBar localSeekBar1 = (SeekBar)localView2.findViewById(2131165290);
    TextView localTextView1 = (TextView)localView2.findViewById(2131165291);
    SeekBar localSeekBar2 = (SeekBar)localView3.findViewById(2131165290);
    TextView localTextView2 = (TextView)localView3.findViewById(2131165291);
    CheckedTextView localCheckedTextView = (CheckedTextView)localView1.findViewById(2131165369);
    b localb = new b();
    localb.a(localFilterButton1, localListView);
    localb.a(localFilterButton2, localView2);
    localb.a(localFilterButton3, localView3);
    localb.a(localFilterButton1);
    a locala = new a(localb);
    localFilterButton1.setOnClickListener(locala);
    localFilterButton2.setOnClickListener(locala);
    localFilterButton3.setOnClickListener(locala);
    localCheckedTextView.setOnClickListener(new b(this, localCheckedTextView, paramListStationTab));
    c localc = new c(this, paramListStationTab.getStationFilter().getStationNames());
    localListView.setAdapter(localc);
    localListView.setOnItemClickListener(new c(this, localc));
    a(localSeekBar1, localTextView1, 0, paramListStationTab);
    a(localSeekBar2, localTextView2, 1, paramListStationTab);
    localCheckedTextView.setChecked(paramListStationTab.getStationFilter().hasPrices());
    localBuilder.setTitle(getString(2131296545));
    localBuilder.setContentView(localView1);
    localBuilder.setPositiveButton(getString(2131296659), new d(this, paramListStationTab));
    CustomDialog localCustomDialog = localBuilder.create();
    localCustomDialog.setOnDismissListener(this);
    localCustomDialog.show();
  }

  public static List<ListMessage> filterStations(ListStationTab paramListStationTab)
  {
    ArrayList localArrayList = new ArrayList();
    List localList1 = paramListStationTab.getListStations();
    ListStationFilter localListStationFilter = paramListStationTab.getStationFilter();
    if (localListStationFilter != null)
    {
      List localList2 = localListStationFilter.getStationNames();
      double d1 = localListStationFilter.getDistance();
      double d2 = localListStationFilter.getPrice();
      int i = localList1.size();
      int k;
      if (!localListStationFilter.hasPrices())
      {
        k = 0;
        if (k < i);
      }
      while (true)
      {
        return localArrayList;
        ListMessage localListMessage2 = (ListMessage)localList1.get(k);
        if ((a(localList2, localListMessage2)) && ((d1 == 0.0D) || (b(d1, localListMessage2))) && ((d2 == 0.0D) || (a(d2, localListMessage2))) && (a(localListMessage2)))
          localArrayList.add(localListMessage2);
        k++;
        break;
        for (int j = 0; j < i; j++)
        {
          ListMessage localListMessage1 = (ListMessage)localList1.get(j);
          if ((a(localList2, localListMessage1)) && ((d1 == 0.0D) || (b(d1, localListMessage1))) && ((d2 == 0.0D) || (a(d2, localListMessage1))))
            localArrayList.add(localListMessage1);
        }
      }
    }
    return paramListStationTab.getListStationsFiltered();
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
    {
      ListStationTab localListStationTab = (ListStationTab)localBundle.getParcelable("list");
      if (localListStationTab != null)
      {
        a(localListStationTab);
        d(localListStationTab);
        return;
      }
      finish();
      return;
    }
    finish();
  }

  public void onDismiss(DialogInterface paramDialogInterface)
  {
    finish();
  }

  protected void setAdsAfterGPSServiceConnected()
  {
  }

  protected String setAnalyticsPageName()
  {
    return getString(2131296853);
  }

  private final class a
    implements View.OnClickListener
  {
    private FilterStations.b b;

    public a(FilterStations.b arg2)
    {
      Object localObject;
      this.b = localObject;
    }

    public final void onClick(View paramView)
    {
      this.b.a(paramView);
      FilterStations.a(FilterStations.this, ((FilterButton)paramView).getText());
    }
  }

  private final class b
  {
    private List<View> b = new ArrayList();
    private List<View> c = new ArrayList();

    public b()
    {
    }

    public final void a(View paramView)
    {
      int i = this.b.size();
      int j = 0;
      if (j >= i)
        return;
      View localView1 = (View)this.b.get(j);
      View localView2 = (View)this.c.get(j);
      if (localView1 == paramView)
      {
        localView1.setSelected(true);
        localView2.setVisibility(0);
      }
      while (true)
      {
        j++;
        break;
        localView1.setSelected(false);
        localView2.setVisibility(8);
      }
    }

    public final void a(View paramView1, View paramView2)
    {
      if (!this.b.contains(paramView1))
      {
        this.b.add(paramView1);
        this.c.add(paramView2);
      }
    }
  }

  private final class c extends ArrayAdapter<StationNameFilter>
  {
    private List<StationNameFilter> b;
    private int c;

    public c(int arg2)
    {
      super(2130903083, localList);
      this.b = localList;
      this.c = 2130903083;
    }

    public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      FilterStations.d locald;
      if (paramView == null)
      {
        paramView = FilterStations.a(FilterStations.this).inflate(this.c, null);
        locald = new FilterStations.d();
        locald.a = ((CheckedTextView)paramView.findViewById(2131165228));
        paramView.setTag(locald);
      }
      while (true)
      {
        StationNameFilter localStationNameFilter = (StationNameFilter)this.b.get(paramInt);
        if (localStationNameFilter != null)
        {
          locald.a.setText(localStationNameFilter.getStationName());
          locald.a.setChecked(localStationNameFilter.isIncluded());
        }
        return paramView;
        locald = (FilterStations.d)paramView.getTag();
      }
    }
  }

  static final class d
  {
    CheckedTextView a;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.list.FilterStations
 * JD-Core Version:    0.6.2
 */