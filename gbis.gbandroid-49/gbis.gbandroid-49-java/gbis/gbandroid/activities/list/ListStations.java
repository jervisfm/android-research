package gbis.gbandroid.activities.list;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.text.InputFilter;
import android.text.InputFilter.LengthFilter;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AbsListView;
import android.widget.AbsListView.LayoutParams;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.amazon.device.ads.AdLayout;
import com.amazon.device.ads.AdLayout.AdSize;
import com.amazon.device.ads.AdTargetingOptions;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.google.ads.doubleclick.DfpExtras;
import com.google.ads.searchads.SearchAdRequest;
import com.google.android.maps.GeoPoint;
import gbis.gbandroid.GBApplication;
import gbis.gbandroid.activities.base.GBActivity;
import gbis.gbandroid.activities.base.GBActivityAds;
import gbis.gbandroid.activities.map.MapStations;
import gbis.gbandroid.activities.report.ReportPrices;
import gbis.gbandroid.activities.station.FavAddStation;
import gbis.gbandroid.activities.station.StationDetails;
import gbis.gbandroid.content.GBIntent;
import gbis.gbandroid.entities.AdsGSA;
import gbis.gbandroid.entities.Center;
import gbis.gbandroid.entities.ListMessage;
import gbis.gbandroid.entities.ListResults;
import gbis.gbandroid.entities.ListStationFilter;
import gbis.gbandroid.entities.ListStationTab;
import gbis.gbandroid.entities.MapResults;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.Station;
import gbis.gbandroid.listeners.MyLocationChangedListener;
import gbis.gbandroid.queries.ListQuery;
import gbis.gbandroid.queries.MapQuery;
import gbis.gbandroid.utils.CustomAsyncTask;
import gbis.gbandroid.utils.DateUtils;
import gbis.gbandroid.utils.ImageLoader;
import gbis.gbandroid.utils.VerifyFields;
import gbis.gbandroid.views.CustomDialog.Builder;
import gbis.gbandroid.views.layouts.CustomListView;
import gbis.gbandroid.views.layouts.CustomListView.OnRefreshListener;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;

public class ListStations extends GBActivityAds
  implements View.OnClickListener, AbsListView.OnScrollListener, MyLocationChangedListener, CustomListView.OnRefreshListener
{
  private boolean A;
  private int B;
  private int C;
  private int D;
  private int E;
  private boolean F;
  private c G;
  private View H;
  private View I;
  private SensorManager J;
  private Sensor K;
  private h L;
  private float M;
  private RelativeLayout N;
  private RelativeLayout O;
  private RelativeLayout P;
  private RelativeLayout Q;
  private RelativeLayout R;
  private RelativeLayout S;
  private RelativeLayout T;
  final Handler a = new e(this);
  private List<ListStationTab> b;
  private ListResults c;
  private String d;
  private String e;
  private boolean f;
  private String g;
  private GeoPoint h;
  private String i;
  private String j;
  private String k;
  private TextView l;
  private ImageView m;
  private View n;
  private RelativeLayout[] o;
  private CustomListView p;
  private View q;
  private View r;
  private Dialog s;
  private j t;
  private g u;
  private e v;
  private CustomAsyncTask w;
  private int x;
  private int y;
  private boolean z;

  private void A()
  {
    ListStationTab localListStationTab = b();
    List localList = localListStationTab.getListStationsFiltered();
    this.c.setTotalNear(a(localList));
    ListMessage localListMessage1;
    ListMessage localListMessage3;
    if (localList.size() != 0)
    {
      localListMessage1 = new ListMessage();
      if (localList.size() >= localListStationTab.getListStations().size())
        break label301;
      localListMessage1.setStationId(-2);
      localListMessage1.setDistance(9999.0D);
      localListMessage1.setSortOrder(9999);
      if (!((ListMessage)localList.get(-1 + localList.size())).isNear())
      {
        ListMessage localListMessage2 = new ListMessage();
        localListMessage2.setStationId(-1);
        String str = getString(2131296307);
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = this.c.getSearchTerms();
        localListMessage2.setStationName(String.format(str, arrayOfObject));
        localList.add(0, localListMessage2);
        localListMessage3 = new ListMessage();
        localListMessage3.setStationId(-1);
        localListMessage3.setStationName(getString(2131296308));
        if (this.c.getTotalNear() != 0)
          break label310;
        if (((localListMessage1.getStationId() == -3) && (!this.g.equals(""))) || (localListMessage1.getStationId() == -2))
          localList.add(1, localListMessage1);
        localList.add(2, localListMessage3);
      }
    }
    while (true)
    {
      if (!this.g.equals(""))
      {
        ListMessage localListMessage4 = new ListMessage();
        localListMessage4.setStationId(-3);
        localListMessage4.setDistance(9999.0D);
        localListMessage4.setSortOrder(9999);
        localList.add(localListMessage4);
      }
      o();
      return;
      label301: localListMessage1.setStationId(-3);
      break;
      label310: if (((localListMessage1.getStationId() == -3) && (!this.g.equals(""))) || (localListMessage1.getStationId() == -2))
      {
        localList.add(1 + this.c.getTotalNear(), localListMessage1);
        localList.add(2 + this.c.getTotalNear(), localListMessage3);
      }
      else
      {
        localList.add(1 + this.c.getTotalNear(), localListMessage3);
      }
    }
  }

  private void B()
  {
    this.d = this.mPrefs.getString("fuelPreference", "");
    this.e = this.mPrefs.getString("distancePreference", "");
    this.f = this.mPrefs.getBoolean("noPricesPreference", true);
    this.g = this.mPrefs.getString("member_id", "");
  }

  private void C()
  {
    a(false);
    loadDialog(getString(2131296393), this.w);
    this.w = new d(this);
    this.w.execute(new Object[0]);
  }

  private void D()
  {
    a(false);
    loadDialog(getString(2131296393), this.w);
    this.w = new a(this);
    this.w.execute(new Object[0]);
  }

  private void E()
  {
    this.v = new e(getActivity());
    this.v.execute(new Object[0]);
    loadDialog(getString(2131296397), this.v);
  }

  private void F()
  {
    int i1 = getResources().getIntArray(2131099658)[0];
    int i2 = getResources().getIntArray(2131099658)[1];
    List localList = b().getListStationsFiltered();
    ListMessage localListMessage;
    if ((localList.size() != 0) && (((ListMessage)localList.get(0)).getStationId() <= 0))
      if ((localList.size() != 1) && (((ListMessage)localList.get(1)).getStationId() <= 0))
        if ((localList.size() != 2) && (((ListMessage)localList.get(2)).getStationId() <= 0))
          localListMessage = (ListMessage)this.p.getItemAtPosition(4);
    while (true)
    {
      if (localListMessage != null)
        this.h = new GeoPoint((int)(1000000.0D * localListMessage.getLatitude()), (int)(1000000.0D * localListMessage.getLongitude()));
      Type localType = new f(this).getType();
      this.mResponseObject = new MapQuery(this, this.mPrefs, localType, this.myLocation).getResponseObject(this.h, i1, i2);
      if (!this.v.isCancelled())
        a((MapResults)this.mResponseObject.getPayload());
      return;
      localListMessage = (ListMessage)this.p.getItemAtPosition(3);
      continue;
      localListMessage = (ListMessage)this.p.getItemAtPosition(2);
      continue;
      localListMessage = (ListMessage)this.p.getItemAtPosition(1);
    }
  }

  private void G()
  {
    B();
    Location localLocation = getLastKnownLocation();
    if (localLocation != null)
    {
      this.myLocation = localLocation;
      this.h = new GeoPoint((int)(1000000.0D * this.myLocation.getLatitude()), (int)(1000000.0D * this.myLocation.getLongitude()));
    }
    Type localType = new g(this).getType();
    ListQuery localListQuery = new ListQuery(this, this.mPrefs, localType, this.myLocation);
    if (this.h == null)
      this.h = new GeoPoint(0, 0);
    this.mResponseObject = localListQuery.getResponseObject(this.i, this.j, this.k, this.h);
    if (!this.w.isCancelled())
      this.c = ((ListResults)this.mResponseObject.getPayload());
  }

  private void H()
  {
    GBIntent localGBIntent = new GBIntent(this, FilterStations.class, this.myLocation);
    localGBIntent.putExtra("list", b());
    startActivityForResult(localGBIntent, 5);
  }

  private void I()
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this);
    View localView = this.mInflater.inflate(2130903057, null);
    TextView localTextView = (TextView)localView.findViewById(2131165246);
    EditText localEditText = (EditText)localView.findViewById(2131165247);
    InputFilter[] arrayOfInputFilter = new InputFilter[1];
    arrayOfInputFilter[0] = new InputFilter.LengthFilter(30);
    localEditText.setFilters(arrayOfInputFilter);
    localEditText.setImeOptions(6);
    localBuilder.setTitle(getString(2131296544));
    localBuilder.setContentView(localView);
    localTextView.setText(getString(2131296573));
    String str = this.c.getSearchTerms();
    if ((str != null) && (!str.equals("")))
      localEditText.setText(str);
    localBuilder.setPositiveButton(getString(2131296675), new h(this, localEditText));
    localBuilder.create().show();
  }

  private void J()
  {
    this.t.notifyDataSetChanged();
  }

  private static int a(List<ListMessage> paramList)
  {
    int i1 = 0;
    int i2 = 0;
    while (true)
    {
      if (i1 >= paramList.size());
      while (!((ListMessage)paramList.get(i1)).isNear())
        return i2;
      i2++;
      i1++;
    }
  }

  private View a(String paramString1, int paramInt, String paramString2)
  {
    View localView = this.mInflater.inflate(2130903040, null);
    localView.setLayoutParams(new AbsListView.LayoutParams(-1, (int)(50.0F * getDensity())));
    a(paramString1, paramString2, paramInt, (ViewGroup)localView);
    return localView;
  }

  private void a()
  {
    if (this.J != null)
    {
      if (this.L != null)
      {
        this.J.unregisterListener(this.L);
        this.L = null;
      }
      this.K = null;
      this.J = null;
    }
  }

  private void a(int paramInt)
  {
    if (paramInt == 0)
    {
      this.l.setText(getString(2131296600));
      this.m.setImageDrawable(this.mRes.getDrawable(2130837556));
      return;
    }
    this.l.setText(getString(2131296599));
    this.m.setImageDrawable(this.mRes.getDrawable(2130837555));
  }

  private void a(Bundle paramBundle)
  {
    int i1 = 0;
    int i2 = paramBundle.getInt("sm_id");
    double d1;
    if (this.d.equals(this.mRes.getStringArray(2131099649)[0]))
      d1 = paramBundle.getDouble("fuel regular");
    while (true)
    {
      List localList1;
      int i4;
      label74: label81: List localList2;
      int i5;
      if (d1 > 0.0D)
      {
        localList1 = b().getListStations();
        if (localList1 != null)
        {
          int i3 = localList1.size();
          i4 = 0;
          if (i4 < i3)
            break label215;
          localList2 = b().getListStationsFiltered();
          i5 = localList2.size();
        }
      }
      while (true)
      {
        if (i1 >= i5);
        while (true)
        {
          this.t.notifyDataSetChanged();
          return;
          if (this.d.equals(this.mRes.getStringArray(2131099649)[1]))
          {
            d1 = paramBundle.getDouble("fuel midgrade");
            break;
          }
          if (this.d.equals(this.mRes.getStringArray(2131099649)[2]))
          {
            d1 = paramBundle.getDouble("fuel premium");
            break;
          }
          if (!this.d.equals(this.mRes.getStringArray(2131099649)[3]))
            break label311;
          d1 = paramBundle.getDouble("fuel diesel");
          break;
          label215: ListMessage localListMessage = (ListMessage)localList1.get(i4);
          if (localListMessage.getStationId() == i2)
          {
            localListMessage.setPrice(d1);
            localListMessage.setTimeSpotted(paramBundle.getString("time spotted"));
            localListMessage.setTimeOffset(DateUtils.getNowOffset());
            break label81;
          }
          i4++;
          break label74;
          if (((ListMessage)localList2.get(i1)).getStationId() != -6)
            break label305;
          localList2.remove(i1);
        }
        label305: i1++;
      }
      label311: d1 = 0.0D;
    }
  }

  private void a(View paramView)
  {
    AdTargetingOptions localAdTargetingOptions = new AdTargetingOptions().enableGeoLocation(true);
    ((AdLayout)paramView).setListener(new p(this, paramView));
    ((AdLayout)paramView).loadAd(localAdTargetingOptions);
  }

  private void a(ListView paramListView)
  {
    if (this.q != null)
    {
      paramListView.removeFooterView(this.q);
      this.q = null;
    }
    if (paramListView.getFooterViewsCount() == 0)
    {
      this.r = this.mInflater.inflate(2130903106, paramListView, false);
      paramListView.addFooterView(this.r, null, true);
    }
  }

  private void a(RelativeLayout paramRelativeLayout)
  {
    RelativeLayout[] arrayOfRelativeLayout = this.o;
    int i1 = arrayOfRelativeLayout.length;
    int i2 = 0;
    if (i2 >= i1)
      return;
    RelativeLayout localRelativeLayout = arrayOfRelativeLayout[i2];
    if (localRelativeLayout.equals(paramRelativeLayout))
    {
      localRelativeLayout.findViewById(2131165557).setVisibility(8);
      localRelativeLayout.setSelected(true);
    }
    while (true)
    {
      i2++;
      break;
      localRelativeLayout.findViewById(2131165557).setVisibility(0);
      localRelativeLayout.setSelected(false);
    }
  }

  private void a(ListMessage paramListMessage)
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this, this.mTracker, "ContextMenu");
    View localView = this.mInflater.inflate(2130903052, null);
    localBuilder.setTitle(paramListMessage.getStationName());
    localBuilder.setStationLogo(paramListMessage.getGasBrandId(), "/Android/data/gbis.gbandroid/cache/Brands");
    localBuilder.setContentView(localView);
    this.s = localBuilder.create();
    ListView localListView = (ListView)localView.findViewById(2131165229);
    String[] arrayOfString = this.mRes.getStringArray(2131099662);
    if ((this.myLocation == null) || ((this.myLocation.getLatitude() == 0.0D) && (this.myLocation.getLongitude() == 0.0D)))
      arrayOfString[2] = getString(2131296698);
    localListView.setAdapter(new ArrayAdapter(this, 2130903053, arrayOfString));
    localListView.setOnItemClickListener(new i(this, paramListMessage, arrayOfString));
    this.s.show();
  }

  private void a(ListResults paramListResults, int paramInt)
  {
    a(paramListResults, 1, true);
  }

  private void a(ListResults paramListResults, int paramInt, boolean paramBoolean)
  {
    ListStationTab localListStationTab1 = b();
    ListStationTab localListStationTab2;
    if (localListStationTab1 == null)
    {
      localListStationTab2 = new ListStationTab();
      this.b.add(localListStationTab2);
    }
    for (ListStationTab localListStationTab3 = localListStationTab2; ; localListStationTab3 = localListStationTab1)
    {
      localListStationTab3.setListStations(paramListResults.getListMessages());
      localListStationTab3.setPage(paramInt);
      localListStationTab3.setTotalStations(paramListResults.getTotalStations());
      localListStationTab3.setTotalNear(paramListResults.getTotalNear());
      localListStationTab3.setFuelType(this.d);
      localListStationTab3.setSortOrder(this.x);
      List localList;
      ArrayList localArrayList;
      int i1;
      if ((paramBoolean) && (localListStationTab3.getListStationsFiltered() == null))
      {
        localList = localListStationTab3.getListStations();
        localArrayList = new ArrayList();
        i1 = localList.size();
      }
      for (int i2 = 0; ; i2++)
      {
        if (i2 >= i1)
        {
          localListStationTab3.setListStationsFiltered(localArrayList);
          if (localListStationTab3.getStationFilter() == null)
            localListStationTab3.setStationFilter(FilterStations.createFilter(localListStationTab3, this.f));
          return;
        }
        ListMessage localListMessage = (ListMessage)localList.get(i2);
        if (localListMessage.getStationId() > 0)
          localArrayList.add(localListMessage);
      }
    }
  }

  private void a(MapResults paramMapResults)
  {
    double d1 = 0.0D;
    double d2;
    GBIntent localGBIntent;
    if (this.myLocation == null)
    {
      d2 = d1;
      localGBIntent = new GBIntent(this, MapStations.class, this.myLocation);
      localGBIntent.putExtra("map", paramMapResults);
      localGBIntent.putExtra("center latitude", this.h.getLatitudeE6());
      localGBIntent.putExtra("center longitude", this.h.getLongitudeE6());
      localGBIntent.putExtra("my latitude", d2);
      localGBIntent.putExtra("my longitude", d1);
      if (!((TextView)findViewById(2131165388)).getText().equals(getString(2131296599)))
        break label169;
      localGBIntent.putExtra("list sort order", 2);
    }
    while (true)
    {
      this.mPrefs.unregisterOnSharedPreferenceChangeListener(this.u);
      startActivityForResult(localGBIntent, 4);
      return;
      d2 = this.myLocation.getLatitude();
      d1 = this.myLocation.getLongitude();
      break;
      label169: localGBIntent.putExtra("list sort order", 1);
    }
  }

  private void a(Station paramStation)
  {
    GBIntent localGBIntent = new GBIntent(this, FavAddStation.class, this.myLocation);
    localGBIntent.putExtra("station", paramStation);
    startActivity(localGBIntent);
  }

  private void a(String paramString)
  {
    SharedPreferences.Editor localEditor = this.mPrefs.edit();
    localEditor.putString("fuelPreference", paramString);
    localEditor.commit();
  }

  private void a(String paramString, DfpExtras paramDfpExtras, RelativeLayout.LayoutParams paramLayoutParams, ViewGroup paramViewGroup, Location paramLocation)
  {
    AdView localAdView = new AdView(this, AdSize.BANNER, paramString);
    paramLayoutParams.addRule(14, -1);
    paramViewGroup.addView(localAdView, paramLayoutParams);
    AdRequest localAdRequest = new AdRequest();
    if (paramLocation != null)
      localAdRequest.setLocation(paramLocation);
    ((AdView)localAdView).loadAd(localAdRequest);
    ((AdView)localAdView).setAdListener(new o(this, localAdRequest, paramViewGroup));
    localAdRequest.setNetworkExtras(paramDfpExtras);
  }

  private void a(String paramString1, String paramString2, int paramInt, ViewGroup paramViewGroup)
  {
    if (paramInt == 1);
    for (String str = getString(2131296791) + paramString2; ; str = paramString1)
    {
      Location localLocation = getLastKnownLocation();
      if ((localLocation == null) || (localLocation.getLatitude() == 0.0D) || (localLocation.getLongitude() == 0.0D))
        localLocation = getLastLocationStored();
      RelativeLayout.LayoutParams localLayoutParams1 = new RelativeLayout.LayoutParams(-2, -2);
      switch (paramInt)
      {
      case 3:
      case 4:
      case 8:
      case 9:
      case 11:
      case 12:
      case 13:
      case 14:
      case 15:
      default:
        View localView = new View(this);
        RelativeLayout.LayoutParams localLayoutParams3 = new RelativeLayout.LayoutParams(0, 0);
        localLayoutParams3.addRule(14, -1);
        localView.setVisibility(4);
        paramViewGroup.addView(localView, localLayoutParams3);
        return;
      case 1:
      case 2:
        a(str, new DfpExtras(), localLayoutParams1, paramViewGroup, localLocation);
        return;
      case 17:
        DfpExtras localDfpExtras = new DfpExtras();
        localDfpExtras.addExtra("nk", "prtnr");
        localDfpExtras.addExtra("pr", "gb");
        localDfpExtras.addExtra("cnt", "auto");
        a(str, localDfpExtras, localLayoutParams1, paramViewGroup, localLocation);
        return;
      case 5:
        WebView localWebView2 = new WebView(this);
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append(getString(2131296807));
        localStringBuilder.append("?v=1.1");
        localStringBuilder.append("&k=" + getString(2131296808));
        localStringBuilder.append("&uid=1234567890");
        localStringBuilder.append("&size=320x50");
        if (localLocation != null)
        {
          localStringBuilder.append("&lat=" + localLocation.getLatitude());
          localStringBuilder.append("&log=" + localLocation.getLongitude());
        }
        ((WebView)localWebView2).loadUrl(localStringBuilder.toString());
        localWebView2.setBackgroundColor(0);
        paramViewGroup.addView(localWebView2, new RelativeLayout.LayoutParams(-1, -2));
        return;
      case 6:
        AdsGSA localAdsGSA = GBApplication.getInstance().getAdsGSA();
        SearchAdRequest localSearchAdRequest = new SearchAdRequest();
        fillGSAAdRequestUIParameteres(localSearchAdRequest, localAdsGSA);
        localSearchAdRequest.setQuery(getGSAQuery(localSearchAdRequest, localAdsGSA, t()));
        AdView localAdView = new AdView(this, new AdSize(320, 60), str);
        RelativeLayout.LayoutParams localLayoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        localLayoutParams2.addRule(14, -1);
        paramViewGroup.addView(localAdView, localLayoutParams2);
        paramViewGroup.getLayoutParams().height = ((int)(60.0F * getDensity()));
        if (localLocation != null)
          localSearchAdRequest.setLocation(localLocation);
        ((AdView)localAdView).loadAd(localSearchAdRequest);
        ((AdView)localAdView).setAdListener(this);
        return;
      case 7:
        a(paramString1, paramString2, 6, paramViewGroup);
        return;
      case 10:
        WebView localWebView1 = new WebView(this);
        ((WebView)localWebView1).getSettings().setJavaScriptEnabled(true);
        ((WebView)localWebView1).loadData("javascript:<script type=\"text/javascript\">\npontiflex_data = {};\npontiflex_ad = {pid: 3425, options: {}};\npontiflex_ad.options.view = \"320x50\";\npontiflex_ad.options.appName = \"Sample App\";\n</script>\n<script type=\"text/javascript\" src=\"http://hhymxdkkdt5c.pflexads.com/html-sdk/mobilewebAds.js\"></script>", "text/html", null);
        localWebView1.setBackgroundColor(0);
        paramViewGroup.addView(localWebView1, new RelativeLayout.LayoutParams((int)(320.0F * getDensity()), (int)(50.0F * getDensity())));
        return;
      case 16:
      }
      AdLayout localAdLayout = new AdLayout(this, AdLayout.AdSize.AD_SIZE_320x50);
      paramViewGroup.addView(localAdLayout, new RelativeLayout.LayoutParams((int)(320.0F * getDensity()), -2));
      a(localAdLayout);
      return;
    }
  }

  private void a(boolean paramBoolean)
  {
    this.N.setEnabled(paramBoolean);
    this.O.setEnabled(paramBoolean);
    this.P.setEnabled(paramBoolean);
    this.Q.setEnabled(paramBoolean);
    this.R.setEnabled(paramBoolean);
    this.S.setEnabled(paramBoolean);
    this.T.setEnabled(paramBoolean);
  }

  private ListStationTab b()
  {
    int i1 = this.b.size();
    if (this.d == null)
    {
      if (this.mPrefs == null)
        this.mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
      this.d = this.mPrefs.getString("fuelPreference", "");
    }
    for (int i2 = 0; ; i2++)
    {
      ListStationTab localListStationTab;
      if (i2 >= i1)
        localListStationTab = null;
      do
      {
        return localListStationTab;
        localListStationTab = (ListStationTab)this.b.get(i2);
      }
      while (localListStationTab.getFuelType().equals(this.d));
    }
  }

  private void b(int paramInt)
  {
    if ((this.G != null) && (this.G.isAlive()))
      this.G.interrupt();
    this.G = new c(this.a, paramInt);
    this.G.start();
  }

  private void b(ListView paramListView)
  {
    if (paramListView.getFooterViewsCount() != 0)
    {
      paramListView.removeFooterView(this.q);
      paramListView.removeFooterView(this.r);
      this.q = null;
      this.r = null;
    }
  }

  private void b(ListMessage paramListMessage)
  {
    B();
    if (this.g.equals(""))
    {
      showMessage(getString(2131296317));
      showLogin();
      return;
    }
    showCameraDialog(paramListMessage);
  }

  private void b(boolean paramBoolean)
  {
    b().getStationFilter().setHasPrices(paramBoolean);
  }

  private boolean b(String paramString)
  {
    String str = this.c.getSearchTerms();
    if (str.equals(""))
      return addPlace(paramString, null, this.h.getLatitudeE6() / 1000000.0D, this.h.getLongitudeE6() / 1000000.0D);
    return addPlace(paramString, str, 0.0D, 0.0D);
  }

  private void c()
  {
    ListStationTab localListStationTab1 = b();
    ListStationFilter localListStationFilter = localListStationTab1.getStationFilter();
    int i1 = localListStationTab1.getSortOrder();
    this.b.clear();
    ListResults localListResults = this.c;
    if (localListStationFilter == null);
    for (boolean bool = true; ; bool = false)
    {
      a(localListResults, 1, bool);
      ListStationTab localListStationTab2 = b();
      localListStationTab2.setStationFilter(localListStationFilter);
      localListStationTab2.setSortOrder(i1);
      localListStationTab2.setListStationsFiltered(FilterStations.filterStations(localListStationTab2));
      return;
    }
  }

  private void c(ListView paramListView)
  {
    ListStationTab localListStationTab = b();
    List localList1 = localListStationTab.getListStationsFiltered();
    List localList2 = localListStationTab.getListStations();
    if (localList1.size() >= localList2.size())
    {
      if (localList2.size() < 10)
      {
        a(paramListView);
        ((TextView)this.r.findViewById(2131165423)).setVisibility(8);
        this.r.setOnClickListener(null);
        if (localList2.size() == 0)
          ((TextView)this.r.findViewById(2131165422)).setText(2131296297);
      }
      do
      {
        return;
        ((TextView)this.r.findViewById(2131165422)).setText(2131296291);
        return;
        if ((this.c.getTotalStations() / (100 * localListStationTab.getPage()) <= 1.0F) || (localListStationTab.getPage() >= 5))
          break;
        if (this.r != null)
        {
          paramListView.removeFooterView(this.r);
          this.r = null;
        }
      }
      while (paramListView.getFooterViewsCount() != 0);
      this.q = this.mInflater.inflate(2130903105, paramListView, false);
      paramListView.addFooterView(this.q, null, true);
      return;
      b(paramListView);
      return;
    }
    if (localList1.size() < 10)
    {
      a(paramListView);
      this.r.setOnClickListener(new k(this));
      ((TextView)this.r.findViewById(2131165423)).setVisibility(0);
      if (localList1.size() == 0)
      {
        ((TextView)this.r.findViewById(2131165422)).setText(2131296295);
        ((TextView)this.r.findViewById(2131165423)).setText(2131296296);
        return;
      }
      ((TextView)this.r.findViewById(2131165422)).setText(2131296291);
      ((TextView)this.r.findViewById(2131165423)).setText(2131296296);
      return;
    }
    b(paramListView);
  }

  private void c(ListMessage paramListMessage)
  {
    GBIntent localGBIntent = new GBIntent(this, ReportPrices.class, this.myLocation);
    localGBIntent.putExtra("station", paramListMessage);
    localGBIntent.putExtra("time offset", paramListMessage.getTimeOffset());
    if (this.d.equals(this.mRes.getStringArray(2131099649)[0]))
    {
      localGBIntent.putExtra("fuel regular", paramListMessage.getPrice());
      localGBIntent.putExtra("time spotted regular", paramListMessage.getTimeSpotted());
    }
    while (true)
    {
      this.mPrefs.unregisterOnSharedPreferenceChangeListener(this.u);
      startActivityForResult(localGBIntent, 3);
      return;
      if (this.d.equals(this.mRes.getStringArray(2131099649)[1]))
      {
        localGBIntent.putExtra("fuel midgrade", paramListMessage.getPrice());
        localGBIntent.putExtra("time spotted midgrade", paramListMessage.getTimeSpotted());
      }
      else if (this.d.equals(this.mRes.getStringArray(2131099649)[2]))
      {
        localGBIntent.putExtra("fuel premium", paramListMessage.getPrice());
        localGBIntent.putExtra("time spotted premium", paramListMessage.getTimeSpotted());
      }
      else if (this.d.equals(this.mRes.getStringArray(2131099649)[3]))
      {
        localGBIntent.putExtra("fuel diesel", paramListMessage.getPrice());
        localGBIntent.putExtra("time spotted diesel", paramListMessage.getTimeSpotted());
      }
    }
  }

  private void d()
  {
    if (this.y == 3)
      this.x = 2;
    while (true)
    {
      b().setSortOrder(this.x);
      a(this.x);
      return;
      if (this.y == 1)
      {
        f localf = new f(1);
        this.t.sort(localf);
        this.x = 0;
      }
      else
      {
        z();
        this.x = 1;
      }
    }
  }

  private void d(ListMessage paramListMessage)
  {
    GBIntent localGBIntent = new GBIntent(this, StationDetails.class, this.myLocation);
    localGBIntent.putExtra("station", paramListMessage);
    this.mPrefs.unregisterOnSharedPreferenceChangeListener(this.u);
    startActivityForResult(localGBIntent, 4);
  }

  private void e()
  {
    this.N = ((RelativeLayout)findViewById(2131165386));
    this.O = ((RelativeLayout)findViewById(2131165384));
    this.P = ((RelativeLayout)findViewById(2131165389));
    this.Q = ((RelativeLayout)View.inflate(this, 2130903127, null));
    this.R = ((RelativeLayout)View.inflate(this, 2130903127, null));
    this.S = ((RelativeLayout)View.inflate(this, 2130903127, null));
    this.T = ((RelativeLayout)View.inflate(this, 2130903127, null));
    LinearLayout localLinearLayout = (LinearLayout)findViewById(2131165381);
    this.l = ((TextView)findViewById(2131165388));
    this.m = ((ImageView)findViewById(2131165387));
    LinearLayout.LayoutParams localLayoutParams1 = new LinearLayout.LayoutParams(-1, (int)(40.0F * this.mRes.getDisplayMetrics().density));
    localLayoutParams1.weight = 1.0F;
    ((TextView)this.Q.findViewById(2131165556)).setText(getString(2131296717));
    this.Q.setClickable(true);
    this.Q.setFocusable(true);
    ((TextView)this.R.findViewById(2131165556)).setText(getString(2131296718));
    this.R.setClickable(true);
    this.R.setFocusable(true);
    ((TextView)this.S.findViewById(2131165556)).setText(getString(2131296719));
    this.S.setClickable(true);
    this.S.setFocusable(true);
    ((TextView)this.T.findViewById(2131165556)).setText(getString(2131296720));
    this.T.setClickable(true);
    this.T.setFocusable(true);
    RelativeLayout[] arrayOfRelativeLayout = new RelativeLayout[4];
    arrayOfRelativeLayout[0] = this.Q;
    arrayOfRelativeLayout[1] = this.R;
    arrayOfRelativeLayout[2] = this.S;
    arrayOfRelativeLayout[3] = this.T;
    this.o = arrayOfRelativeLayout;
    View localView1 = new View(this);
    LinearLayout.LayoutParams localLayoutParams2 = new LinearLayout.LayoutParams(7, 2);
    localLayoutParams2.gravity = 80;
    localView1.setBackgroundColor(this.mRes.getColor(2131230762));
    View localView2 = new View(this);
    localView2.setBackgroundColor(this.mRes.getColor(2131230762));
    View localView3 = new View(this);
    localView3.setBackgroundColor(this.mRes.getColor(2131230762));
    View localView4 = new View(this);
    LinearLayout.LayoutParams localLayoutParams3 = new LinearLayout.LayoutParams(5, 2);
    View localView5 = new View(this);
    localLayoutParams3.gravity = 80;
    localView4.setBackgroundColor(this.mRes.getColor(2131230792));
    localView5.setBackgroundColor(this.mRes.getColor(2131230792));
    localLinearLayout.addView(localView4, localLayoutParams3);
    localLinearLayout.addView(this.Q, localLayoutParams1);
    localLinearLayout.addView(localView1, localLayoutParams2);
    localLinearLayout.addView(this.R, localLayoutParams1);
    localLinearLayout.addView(localView2, localLayoutParams2);
    localLinearLayout.addView(this.S, localLayoutParams1);
    localLinearLayout.addView(localView3, localLayoutParams2);
    localLinearLayout.addView(this.T, localLayoutParams1);
    localLinearLayout.addView(localView5, localLayoutParams3);
    f();
    this.N.setOnClickListener(this);
    this.O.setOnClickListener(this);
    this.P.setOnClickListener(this);
    this.Q.setOnClickListener(this);
    this.R.setOnClickListener(this);
    this.S.setOnClickListener(this);
    this.T.setOnClickListener(this);
    b localb = new b((byte)0);
    k localk = new k((byte)0);
    this.N.setOnFocusChangeListener(localb);
    this.O.setOnFocusChangeListener(localb);
    this.P.setOnFocusChangeListener(localb);
    this.Q.setOnFocusChangeListener(localk);
    this.R.setOnFocusChangeListener(localk);
    this.S.setOnFocusChangeListener(localk);
    this.T.setOnFocusChangeListener(localk);
    this.N.setOnFocusChangeListener(new j(this));
  }

  private void f()
  {
    if (this.d.equals(this.mRes.getStringArray(2131099649)[0]))
      a(this.o[0]);
    do
    {
      return;
      if (this.d.equals(this.mRes.getStringArray(2131099649)[1]))
      {
        a(this.o[1]);
        return;
      }
      if (this.d.equals(this.mRes.getStringArray(2131099649)[2]))
      {
        a(this.o[2]);
        return;
      }
    }
    while (!this.d.equals(this.mRes.getStringArray(2131099649)[3]));
    a(this.o[3]);
  }

  private void g()
  {
    if (b() == null)
    {
      C();
      return;
    }
    j();
    h();
    m();
  }

  private void h()
  {
    try
    {
      a(b().getSortOrder());
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }

  private void i()
  {
    this.p = ((CustomListView)findViewById(16908298));
    this.p.setOnRefreshListener(this);
    this.p.setOnItemLongClickListener(new l(this));
    this.p.setTextFilterEnabled(true);
    this.p.setOnItemSelectedListener(new m(this));
    this.p.setOnItemClickListener(new n(this));
    this.p.setOnScrollListener(this);
  }

  private void j()
  {
    if (this.p == null)
      i();
    y();
    k();
  }

  private void k()
  {
    if ((this.myLocation == null) || (this.myLocation.getLatitude() == 0.0D) || (this.myLocation.getLongitude() == 0.0D))
    {
      Center localCenter = this.c.getCenter();
      SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(getBaseContext()).edit();
      localEditor.putFloat("lastLatitude", (float)localCenter.getLatitude());
      localEditor.putFloat("lastLongitude", (float)localCenter.getLongitude());
      localEditor.commit();
    }
  }

  private void l()
  {
    x();
    int i1 = b().getSortOrder();
    if ((i1 == 1) || (i1 == 2))
      if ((this.myLocation == null) || ((this.myLocation.getLatitude() == 0.0D) && (this.myLocation.getLongitude() == 0.0D)))
        showMessage(getString(2131296279), false);
    while (true)
    {
      o();
      return;
      f localf1 = new f(1);
      b().setSortOrder(0);
      a(0);
      this.t.sort(localf1);
      continue;
      f localf2 = new f(2);
      b().setSortOrder(1);
      a(1);
      this.t.sort(localf2);
    }
  }

  private void m()
  {
    int i1 = b().getSortOrder();
    if (i1 != 2)
    {
      x();
      if ((i1 != 1) && (i1 != 2))
        break label55;
    }
    label55: for (f localf = new f(2); ; localf = new f(1))
    {
      a(i1);
      this.t.sort(localf);
      o();
      return;
    }
  }

  private void n()
  {
    int i1 = b().getSortOrder();
    if (i1 != 2)
    {
      x();
      if (i1 != 1)
        break label45;
    }
    label45: for (f localf = new f(2); ; localf = new f(1))
    {
      this.t.sort(localf);
      o();
      return;
    }
  }

  private void o()
  {
    p();
    q();
  }

  private void p()
  {
    List localList = b().getListStationsFiltered();
    if (this.H != null)
    {
      ListMessage localListMessage1 = new ListMessage();
      localListMessage1.setStationId(-4);
      localList.add(0, localListMessage1);
    }
    if (this.I != null)
    {
      ListMessage localListMessage2 = new ListMessage();
      localListMessage2.setStationId(-5);
      localList.add(-1 + localList.size(), localListMessage2);
    }
  }

  private void q()
  {
    List localList = b().getListStationsFiltered();
    if ((r()) && (localList.size() > 0));
    for (int i1 = 0; ; i1++)
    {
      if (i1 >= localList.size())
        return;
      if (((ListMessage)localList.get(i1)).getStationId() > 0)
      {
        ListMessage localListMessage = new ListMessage();
        localListMessage.setStationId(-6);
        localList.add(i1, localListMessage);
        return;
      }
    }
  }

  private boolean r()
  {
    return this.mPrefs.getLong("lastReportedPriceDate", 0L) <= Calendar.getInstance().getTimeInMillis() - 1209600000L;
  }

  private int s()
  {
    if (!this.i.equals(""))
      return 2;
    if (!this.k.equals(""))
      return 3;
    return 1;
  }

  private String t()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if (this.c != null)
    {
      List localList = this.c.getListMessages();
      if ((localList != null) && (localList.size() > 0))
      {
        Station localStation = (Station)localList.get(0);
        localStringBuilder.append(localStation.getCity());
        localStringBuilder.append(" ");
        localStringBuilder.append(localStation.getState());
      }
    }
    return localStringBuilder.toString();
  }

  private void u()
  {
    int i1 = this.mPrefs.getInt("adList", 0);
    this.B = this.mPrefs.getInt("adListScrollTime", 1);
    this.C = this.mPrefs.getInt("adListWaitTime", 1);
    setAdConfiguration(this.mPrefs.getString("adListKey", ""), this.mPrefs.getString("adListUnit", ""), i1, t());
    v();
    w();
  }

  private void v()
  {
    this.D = this.mPrefs.getInt("adListTop", 0);
    String str1 = this.mPrefs.getString("adListTopKey", "");
    String str2 = this.mPrefs.getString("adListTopUnit", "");
    if (this.D > 0)
      this.H = a(str1, this.D, str2);
  }

  private void w()
  {
    this.E = this.mPrefs.getInt("adListBottom", 0);
    String str1 = this.mPrefs.getString("adListBottomKey", "");
    String str2 = this.mPrefs.getString("adListBottomUnit", "");
    if (this.E > 0)
      this.I = a(str1, this.E, str2);
  }

  private void x()
  {
    List localList = b().getListStationsFiltered();
    if ((localList != null) && (localList.size() != 0));
    for (int i1 = 0; ; i1++)
    {
      if (i1 >= -1 + localList.size())
        return;
      if (((ListMessage)localList.get(i1)).getStationId() <= 0)
      {
        localList.remove(i1);
        i1--;
      }
    }
  }

  private void y()
  {
    b().setListStationsFiltered(FilterStations.filterStations(b()));
    x();
    A();
    this.t = new j(this, b().getListStationsFiltered());
    c(this.p);
    this.p.setAdapter(this.t);
  }

  private void z()
  {
    List localList;
    int i1;
    if (b() != null)
    {
      localList = b().getListStations();
      if (localList != null)
        i1 = localList.size();
    }
    for (int i2 = 0; ; i2++)
    {
      if (i2 >= i1)
        return;
      ListMessage localListMessage = (ListMessage)localList.get(i2);
      if ((localListMessage != null) && (this.myLocation != null) && (this.myLocation.getLatitude() != 0.0D) && (this.myLocation.getLongitude() != 0.0D) && ((Math.abs(1000000.0D * this.myLocation.getLatitude() - this.h.getLatitudeE6()) > 5.0D) || (Math.abs(1000000.0D * this.myLocation.getLongitude() - this.h.getLongitudeE6()) > 5.0D)))
      {
        Location localLocation = new Location("Station");
        localLocation.setLatitude(localListMessage.getLatitude());
        localLocation.setLongitude(localListMessage.getLongitude());
        localListMessage.setDistance(this.myLocation.distanceTo(localLocation) / 1000.0F / 1.609344D);
      }
    }
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    switch (paramInt1)
    {
    default:
    case 4:
    case 3:
    case 5:
    }
    do
    {
      do
      {
        do
        {
          do
            return;
          while (paramInt2 == 0);
          if (paramInt2 == -1)
          {
            finish();
            return;
          }
          if (paramInt2 == 9)
          {
            showMessage(this.mRes.getString(2131296257));
            return;
          }
        }
        while (paramInt2 != 3);
        a(paramIntent.getExtras());
        return;
      }
      while ((paramInt2 == 0) || (paramInt2 != 3));
      a(paramIntent.getExtras());
      return;
    }
    while (paramInt2 != -1);
    ListStationTab localListStationTab1 = null;
    if (paramIntent != null)
      localListStationTab1 = (ListStationTab)paramIntent.getExtras().getParcelable("list");
    if (localListStationTab1 == null)
    {
      showMessage(getString(2131296300));
      return;
    }
    ListStationTab localListStationTab2 = b();
    localListStationTab2.setListStationsFiltered(localListStationTab1.getListStationsFiltered());
    localListStationTab2.setStationFilter(localListStationTab1.getStationFilter());
    y();
  }

  public void onClick(View paramView)
  {
    if (paramView == this.N)
    {
      l();
      setAnalyticsTrackEventScreenButton(this.l.getText().toString());
    }
    do
    {
      return;
      if (paramView == this.O)
      {
        setAnalyticsTrackEventScreenButton(getString(2131296602));
        D();
        return;
      }
      if (paramView == this.P)
      {
        setAnalyticsTrackEventScreenButton(getString(2131296601));
        E();
        return;
      }
      if (paramView == this.Q)
      {
        setAnalyticsTrackEventScreenButton(getString(2131296717));
        a(getString(2131296713));
        a(this.Q);
        g();
        return;
      }
      if (paramView == this.R)
      {
        setAnalyticsTrackEventScreenButton(getString(2131296718));
        a(getString(2131296714));
        a(this.R);
        g();
        return;
      }
      if (paramView == this.S)
      {
        setAnalyticsTrackEventScreenButton(getString(2131296719));
        a(getString(2131296715));
        a(this.S);
        g();
        return;
      }
    }
    while (paramView != this.T);
    setAnalyticsTrackEventScreenButton(getString(2131296720));
    a(getString(2131296716));
    a(this.T);
    g();
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    if ((this.G != null) && (!this.G.isAlive()))
      hideAds();
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.u = new g((byte)0);
    B();
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
    {
      this.b = new ArrayList();
      this.c = ((ListResults)localBundle.getParcelable("list"));
      if (this.c != null)
      {
        openDB();
        setContentView(2130903096);
        u();
        e();
        a(this.c, 1);
        this.h = new GeoPoint(localBundle.getInt("center latitude"), localBundle.getInt("center longitude"));
        this.i = localBundle.getString("city");
        this.j = localBundle.getString("state");
        this.k = localBundle.getString("zip");
        j();
        this.c.getTotalStations();
        this.y = localBundle.getInt("list sort order");
        if (this.y != 0)
          d();
        while (true)
        {
          Dialog localDialog = (Dialog)getLastNonConfigurationInstance();
          if (localDialog != null)
          {
            this.s = localDialog;
            this.s.show();
          }
          return;
          z();
        }
      }
      finish();
      return;
    }
    finish();
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    super.onCreateOptionsMenu(paramMenu);
    getMenuInflater().inflate(2131427330, paramMenu);
    return super.onCreateOptionsMenu(paramMenu);
  }

  public void onDestroy()
  {
    super.onDestroy();
    this.mPrefs.unregisterOnSharedPreferenceChangeListener(this.u);
  }

  public void onMyLocationChanged(Location paramLocation)
  {
    super.onMyLocationChanged(paramLocation);
    z();
    this.t.notifyDataSetChanged();
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    super.onOptionsItemSelected(paramMenuItem);
    switch (paramMenuItem.getItemId())
    {
    default:
      return super.onOptionsItemSelected(paramMenuItem);
    case 2131165581:
      E();
      return true;
    case 2131165579:
      launchSettings();
      return true;
    case 2131165583:
      showHome();
      return true;
    case 2131165584:
      showCityZipSearch();
      return true;
    case 2131165582:
      if (openDB())
      {
        String str = this.c.getSearchTerms();
        if ((str != null) && (!str.equals("")))
        {
          Cursor localCursor = getPlaceFromCityZip(str);
          if (localCursor.getCount() == 0)
            I();
          while (true)
          {
            localCursor.close();
            return true;
            showMessage(getString(2131296414));
          }
        }
        I();
        return true;
      }
      showMessage(getString(2131296258));
      return true;
    case 2131165580:
    }
    H();
    return true;
  }

  public void onPause()
  {
    super.onPause();
    a();
  }

  public void onRefresh()
  {
    setAnalyticsTrackEventScreenButton(getString(2131296602) + " Pull-to-refresh");
    D();
  }

  public void onResume()
  {
    super.onResume();
    openDB();
    if (this.c == null)
    {
      Bundle localBundle = getIntent().getExtras();
      if (localBundle != null)
        this.c = ((ListResults)localBundle.getParcelable("list"));
    }
    if (this.c == null)
    {
      finish();
      return;
    }
    B();
    this.A = false;
    this.mPrefs.registerOnSharedPreferenceChangeListener(this.u);
    if (this.mRes.getConfiguration().orientation == 1)
      showAds();
    b(this.C);
  }

  public Object onRetainNonConfigurationInstance()
  {
    Dialog localDialog1 = this.s;
    Dialog localDialog2 = null;
    if (localDialog1 != null)
    {
      boolean bool = this.s.isShowing();
      localDialog2 = null;
      if (bool)
      {
        localDialog2 = this.s;
        this.s.dismiss();
      }
    }
    return localDialog2;
  }

  public void onScroll(AbsListView paramAbsListView, int paramInt1, int paramInt2, int paramInt3)
  {
    int i1 = paramInt1 + paramInt2;
    if ((this.q != null) && (i1 == paramInt3) && (!this.z))
    {
      this.z = true;
      this.w = new d(this, (byte)0);
      this.w.execute(new Object[0]);
    }
    if ((paramInt1 != 0) && (!this.F))
    {
      this.F = true;
      b(this.B);
    }
  }

  public void onScrollStateChanged(AbsListView paramAbsListView, int paramInt)
  {
  }

  public void onStop()
  {
    super.onStop();
    this.A = true;
  }

  protected void setAdsAfterGPSServiceConnected()
  {
    registerMyLocationChangedListener(this);
  }

  protected String setAnalyticsPageName()
  {
    return getString(2131296849);
  }

  private final class a extends CustomAsyncTask
  {
    public a(GBActivity arg2)
    {
      super();
    }

    protected final void onCancelled()
    {
      super.onCancelled();
      ListStations.q(ListStations.this).onRefreshComplete();
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      try
      {
        ListStations.r(ListStations.this).dismiss();
        label15: if (paramBoolean.booleanValue())
        {
          ListStations.t(ListStations.this);
          if (ListStations.u(ListStations.this) != null)
            ListStations.u(ListStations.this).clear();
          ListStations.n(ListStations.this);
          ListStations.v(ListStations.this);
        }
        if (ListStations.q(ListStations.this) == null)
          ListStations.a(ListStations.this, (CustomListView)ListStations.this.findViewById(16908298));
        ListStations.q(ListStations.this).onRefreshComplete();
        ListStations.w(ListStations.this);
        return;
      }
      catch (Exception localException)
      {
        break label15;
      }
    }

    protected final boolean queryWebService()
    {
      try
      {
        ListStations.x(ListStations.this);
        return true;
      }
      catch (Exception localException)
      {
        ListStations.this.setMessage(ListStations.this.getString(2131296279));
      }
      return false;
    }
  }

  private final class b
    implements View.OnFocusChangeListener
  {
    private b()
    {
    }

    public final void onFocusChange(View paramView, boolean paramBoolean)
    {
      if (ListStations.q(ListStations.this) == null)
        ListStations.a(ListStations.this, (CustomListView)ListStations.this.findViewById(16908298));
      if (paramBoolean)
      {
        ListStations.q(ListStations.this).getChildAt(ListStations.q(ListStations.this).getLastVisiblePosition() - ListStations.q(ListStations.this).getFirstVisiblePosition()).setFocusable(true);
        return;
      }
      ListStations.q(ListStations.this).requestFocus();
    }
  }

  private final class c extends Thread
  {
    private Handler b;
    private long c;
    private long d;

    c(Handler paramInt, int arg3)
    {
      this.b = paramInt;
      this.c = System.currentTimeMillis();
      int i;
      this.d = (i * 1000);
    }

    public final void run()
    {
      long l = System.currentTimeMillis();
      if (l - this.c < this.d)
        SystemClock.sleep(this.d - (l - this.c));
      if (!Thread.currentThread().isInterrupted())
        this.b.sendEmptyMessage(0);
    }
  }

  private final class d extends CustomAsyncTask
  {
    private boolean b;

    public d(GBActivity arg2)
    {
      super();
    }

    public d(GBActivity paramByte, byte arg3)
    {
      super();
      this.b = true;
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      try
      {
        ListStations.r(ListStations.this).dismiss();
        label15: ListStationTab localListStationTab;
        if (paramBoolean.booleanValue())
        {
          ListStations.a(ListStations.this, ListStations.y(ListStations.this));
          localListStationTab = ListStations.z(ListStations.this);
          if (!this.b)
          {
            ListStations.n(ListStations.this);
            ListStations.A(ListStations.this);
            ListStations.B(ListStations.this);
          }
        }
        while (true)
        {
          ListStations.w(ListStations.this);
          return;
          List localList = localListStationTab.getListStationsFiltered();
          ListStations.p(ListStations.this);
          int i = localList.size();
          for (int j = 0; ; j++)
          {
            if (j >= i)
            {
              ListStations.C(ListStations.this);
              ListStations.a(ListStations.this, ListStations.q(ListStations.this));
              break;
            }
            ListStations.u(ListStations.this).add((ListMessage)localList.get(j));
          }
          if (this.b)
            ListStations.this.showMessage(ListStations.j(ListStations.this).getString(2131296257));
        }
      }
      catch (Exception localException)
      {
        break label15;
      }
    }

    protected final boolean queryWebService()
    {
      ListStations.x(ListStations.this);
      return true;
    }
  }

  private final class e extends CustomAsyncTask
  {
    public e(GBActivity arg2)
    {
      super();
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      try
      {
        ListStations.r(ListStations.this).dismiss();
        return;
      }
      catch (Exception localException)
      {
      }
    }

    protected final boolean queryWebService()
    {
      ListStations.s(ListStations.this);
      return true;
    }
  }

  private final class f
    implements Comparator<ListMessage>
  {
    private int b;

    public f(int arg2)
    {
      int i;
      this.b = i;
    }

    private int a(ListMessage paramListMessage1, ListMessage paramListMessage2)
    {
      if (paramListMessage1.getStationId() < 0);
      int i;
      do
      {
        do
        {
          return 1;
          if (this.b == 1)
            return Double.compare(paramListMessage1.getDistance(), paramListMessage2.getDistance());
          if (this.b != 2)
            break;
        }
        while ((paramListMessage1.getPrice() == 0.0D) && (paramListMessage2.getPrice() != 0.0D));
        if ((paramListMessage2.getPrice() == 0.0D) && (paramListMessage1.getPrice() != 0.0D))
          return -1;
        i = Double.compare(paramListMessage1.getPrice(), paramListMessage2.getPrice());
        if (i != 0)
          break;
        if (paramListMessage1.getSortOrder() < paramListMessage2.getSortOrder())
          return -1;
      }
      while (paramListMessage1.getSortOrder() > paramListMessage2.getSortOrder());
      return 0;
      return i;
      return 0;
    }
  }

  private final class g
    implements SharedPreferences.OnSharedPreferenceChangeListener
  {
    private g()
    {
    }

    public final void onSharedPreferenceChanged(SharedPreferences paramSharedPreferences, String paramString)
    {
      ListStations.k(ListStations.this);
      if ((paramString.equals("fuelPreference")) && (ListStations.l(ListStations.this)))
        ListStations.m(ListStations.this);
      do
      {
        return;
        if (paramString.equals("distancePreference"))
        {
          ListStations.n(ListStations.this);
          return;
        }
        if (paramString.equals("noPricesPreference"))
        {
          ListStations.a(ListStations.this, ListStations.o(ListStations.this));
          ListStations.p(ListStations.this);
          return;
        }
      }
      while (!paramString.equals("member_id"));
      ListStations.p(ListStations.this);
    }
  }

  private final class h
    implements SensorEventListener
  {
    public final void onAccuracyChanged(Sensor paramSensor, int paramInt)
    {
    }

    public final void onSensorChanged(SensorEvent paramSensorEvent)
    {
      if (paramSensorEvent.sensor == ListStations.D(this.a))
      {
        float f = ListStations.E(this.a);
        ListStations.a(this.a, paramSensorEvent.values[0]);
        if (Math.abs(f - ListStations.E(this.a)) > 2.0F)
          ListStations.F(this.a);
      }
    }
  }

  private final class i
    implements View.OnClickListener
  {
    private ListMessage b;

    public i(ListMessage arg2)
    {
      Object localObject;
      this.b = localObject;
    }

    public final void onClick(View paramView)
    {
      ListStations.a(ListStations.this, ListStations.this.getString(2131296647));
      ListStations.a(ListStations.this, this.b);
    }
  }

  private final class j extends ArrayAdapter<ListMessage>
  {
    private List<ListMessage> b;
    private int c;
    private ListStations.i d;
    private ImageLoader e;

    public j(int arg2)
    {
      super(2130903100, localList);
      this.b = localList;
      this.c = 2130903100;
      this.e = new ImageLoader(ListStations.this, 2130837622, "/Android/data/gbis.gbandroid/cache/Brands");
    }

    public final boolean areAllItemsEnabled()
    {
      return false;
    }

    public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if (((ListMessage)this.b.get(paramInt)).getStationId() < 0)
      {
        if (((ListMessage)this.b.get(paramInt)).getStationId() == -1)
        {
          View localView2 = ListStations.b(ListStations.this).inflate(2130903102, null);
          ((TextView)localView2.findViewById(2131165411)).setText(((ListMessage)this.b.get(paramInt)).getStationName());
          return localView2;
        }
        if (((ListMessage)this.b.get(paramInt)).getStationId() == -4)
          return ListStations.c(ListStations.this);
        if (((ListMessage)this.b.get(paramInt)).getStationId() == -5)
          return ListStations.d(ListStations.this);
        if (((ListMessage)this.b.get(paramInt)).getStationId() == -6)
          return ListStations.b(ListStations.this).inflate(2130903108, null);
        View localView1 = ListStations.b(ListStations.this).inflate(2130903106, null);
        TextView localTextView2 = (TextView)localView1.findViewById(2131165422);
        TextView localTextView3 = (TextView)localView1.findViewById(2131165423);
        if (((ListMessage)this.b.get(paramInt)).getStationId() == -2)
        {
          localTextView2.setText(ListStations.this.getString(2131296295));
          localTextView3.setText(ListStations.this.getString(2131296296));
          return localView1;
        }
        localTextView2.setText(ListStations.this.getString(2131296294));
        localTextView3.setText(ListStations.this.getString(2131296293));
        return localView1;
      }
      ListStations.l locall2;
      if ((paramView == null) || ((ListStations.l)paramView.getTag() == null))
      {
        paramView = ListStations.b(ListStations.this).inflate(this.c, null);
        ListStations.l locall1 = new ListStations.l();
        locall1.a = ((TextView)paramView.findViewById(2131165400));
        locall1.b = ((TextView)paramView.findViewById(2131165401));
        locall1.c = ((TextView)paramView.findViewById(2131165402));
        locall1.d = ((TextView)paramView.findViewById(2131165403));
        locall1.j = ((RelativeLayout.LayoutParams)locall1.d.getLayoutParams());
        locall1.e = ((TextView)paramView.findViewById(2131165405));
        locall1.k = ((RelativeLayout)paramView.findViewById(2131165398));
        locall1.f = ((TextView)paramView.findViewById(2131165406));
        locall1.g = ((TextView)paramView.findViewById(2131165407));
        locall1.h = ((TextView)paramView.findViewById(2131165408));
        locall1.i = ((ImageView)paramView.findViewById(2131165404));
        paramView.setTag(locall1);
        locall2 = locall1;
      }
      while (true)
      {
        ListMessage localListMessage = (ListMessage)this.b.get(paramInt);
        label618: String str1;
        label692: label716: String str2;
        if (localListMessage != null)
        {
          this.d = new ListStations.i(ListStations.this, localListMessage);
          locall2.e.setText(localListMessage.getStationName());
          if (localListMessage.getPrice() == 0.0D)
            break label1241;
          if (!localListMessage.getCountry().equals("USA"))
            break label1198;
          locall2.a.setText(VerifyFields.doubleToScale(localListMessage.getPrice(), 2));
          locall2.b.setVisibility(0);
          locall2.c.setText(localListMessage.getTimeSpottedConverted(ListStations.this, ListStations.e(ListStations.this)));
          locall2.c.setVisibility(0);
          locall2.d.setText(ListStations.this.getString(2131296302));
          locall2.j.addRule(3, 2131165402);
          locall2.d.setLayoutParams(locall2.j);
          locall2.k.setOnClickListener(this.d);
          if (!localListMessage.isNotIntersection())
            break label1317;
          str1 = "near";
          locall2.f.setText(localListMessage.getAddress() + " " + str1 + " " + localListMessage.getCrossStreet());
          TextView localTextView1 = locall2.g;
          StringBuilder localStringBuilder = new StringBuilder(String.valueOf(localListMessage.getCity()));
          if ((localListMessage.getState() == null) || (localListMessage.getState().equals("")))
            break label1325;
          str2 = ", " + localListMessage.getState();
          label834: localTextView1.setText(str2);
        }
        try
        {
          if ((localListMessage.getDistance() != 0.0D) && (ListStations.f(ListStations.this) != null) && (ListStations.f(ListStations.this).getLatitude() != 0.0D) && (ListStations.f(ListStations.this).getLongitude() != 0.0D))
            if (!ListStations.g(ListStations.this).equals(""))
              if (ListStations.g(ListStations.this).equals("miles"))
              {
                locall2.h.setText(VerifyFields.roundTwoDecimals(localListMessage.getDistance()) + " mi");
                label966: str3 = GBApplication.getInstance().getImageUrl(localListMessage.getGasBrandId(), ListStations.h(ListStations.this));
                locall2.i.setTag(String.valueOf(localListMessage.getGasBrandId()));
              }
        }
        catch (Exception localException1)
        {
          try
          {
            String str3;
            if (localListMessage.getGasBrandId() > 0)
            {
              boolean bool1 = ListStations.i(ListStations.this);
              boolean bool2 = false;
              if (bool1)
              {
                Cursor localCursor = ListStations.a(ListStations.this, localListMessage.getGasBrandId());
                if (localCursor.getCount() == 0)
                {
                  ListStations.a(ListStations.this, localListMessage.getGasBrandId(), localListMessage.getGasBrandVersion());
                  localCursor = ListStations.a(ListStations.this, localListMessage.getGasBrandId());
                }
                localCursor.moveToFirst();
                int i = localCursor.getInt(localCursor.getColumnIndex("version"));
                int j = localListMessage.getGasBrandVersion();
                bool2 = false;
                if (j > i)
                {
                  bool2 = true;
                  ListStations.b(ListStations.this, localListMessage.getGasBrandId(), localListMessage.getGasBrandVersion());
                }
                localCursor.close();
              }
              this.e.displayImage(str3, ListStations.this, locall2.i, bool2, 0, String.valueOf(localListMessage.getGasBrandId()));
            }
            while (true)
            {
              return paramView;
              locall2 = (ListStations.l)paramView.getTag();
              break;
              label1198: locall2.a.setText(VerifyFields.doubleToScale(localListMessage.getPrice(), 1));
              locall2.b.setVisibility(8);
              break label618;
              label1241: locall2.a.setText("--");
              locall2.b.setVisibility(8);
              locall2.d.setText(ListStations.this.getString(2131296301));
              locall2.c.setVisibility(4);
              locall2.j.addRule(3, 2131165399);
              locall2.d.setLayoutParams(locall2.j);
              break label692;
              label1317: str1 = "&";
              break label716;
              label1325: str2 = "";
              break label834;
              locall2.h.setText(VerifyFields.roundTwoDecimals(1.609344D * localListMessage.getDistance()) + " km");
              break label966;
              localException1 = localException1;
              locall2.h.setVisibility(8);
              break label966;
              if (localListMessage.getCountry().equals("USA"))
              {
                locall2.h.setText(VerifyFields.roundTwoDecimals(localListMessage.getDistance()) + " mi");
                break label966;
              }
              locall2.h.setText(VerifyFields.roundTwoDecimals(1.609344D * localListMessage.getDistance()) + " km");
              break label966;
              locall2.h.setVisibility(8);
              break label966;
              locall2.i.setImageDrawable(ListStations.j(ListStations.this).getDrawable(2130837622));
            }
          }
          catch (Exception localException2)
          {
            while (true)
            {
              locall2.i.setImageDrawable(ListStations.j(ListStations.this).getDrawable(2130837622));
              localException2.printStackTrace();
            }
          }
        }
      }
    }

    public final boolean isEnabled(int paramInt)
    {
      return true;
    }
  }

  private final class k
    implements View.OnFocusChangeListener
  {
    private k()
    {
    }

    public final void onFocusChange(View paramView, boolean paramBoolean)
    {
      if (ListStations.q(ListStations.this) == null)
        ListStations.a(ListStations.this, (CustomListView)ListStations.this.findViewById(16908298));
      if (paramBoolean)
      {
        ListStations.q(ListStations.this).getChildAt(0).setFocusable(true);
        return;
      }
      ListStations.q(ListStations.this).requestFocus(130);
    }
  }

  static final class l
  {
    TextView a;
    TextView b;
    TextView c;
    TextView d;
    TextView e;
    TextView f;
    TextView g;
    TextView h;
    ImageView i;
    RelativeLayout.LayoutParams j;
    RelativeLayout k;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.list.ListStations
 * JD-Core Version:    0.6.2
 */