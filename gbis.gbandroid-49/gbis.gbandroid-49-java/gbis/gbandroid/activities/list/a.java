package gbis.gbandroid.activities.list;

import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import gbis.gbandroid.entities.ListStationFilter;
import gbis.gbandroid.utils.VerifyFields;

final class a
  implements SeekBar.OnSeekBarChangeListener
{
  a(FilterStations paramFilterStations, int paramInt1, ListStationFilter paramListStationFilter, double paramDouble1, double paramDouble2, int paramInt2, TextView paramTextView, int paramInt3, String paramString)
  {
  }

  public final void onProgressChanged(SeekBar paramSeekBar, int paramInt, boolean paramBoolean)
  {
    if (this.b == 0)
      this.c.setDistance((paramInt + this.d) / (this.e * this.f));
    while (true)
    {
      this.g.setText(VerifyFields.doubleToScale((paramInt + this.d) / this.f, this.h) + " " + this.i);
      return;
      this.c.setPrice((paramInt + this.d) / (this.e * this.f));
    }
  }

  public final void onStartTrackingTouch(SeekBar paramSeekBar)
  {
  }

  public final void onStopTrackingTouch(SeekBar paramSeekBar)
  {
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.list.a
 * JD-Core Version:    0.6.2
 */