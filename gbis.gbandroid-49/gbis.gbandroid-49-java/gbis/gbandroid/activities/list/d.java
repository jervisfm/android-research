package gbis.gbandroid.activities.list;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import gbis.gbandroid.entities.ListStationTab;

final class d
  implements DialogInterface.OnClickListener
{
  d(FilterStations paramFilterStations, ListStationTab paramListStationTab)
  {
  }

  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    this.b.setListStationsFiltered(FilterStations.filterStations(this.b));
    Intent localIntent = new Intent();
    localIntent.putExtra("list", this.b);
    this.a.setResult(-1, localIntent);
    paramDialogInterface.dismiss();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.list.d
 * JD-Core Version:    0.6.2
 */