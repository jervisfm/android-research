package gbis.gbandroid.activities.list;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.text.Editable;
import android.widget.EditText;

final class h
  implements DialogInterface.OnClickListener
{
  h(ListStations paramListStations, EditText paramEditText)
  {
  }

  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    String str = this.b.getText().toString();
    if ((!str.equals("")) && (!str.replaceAll(" ", "").equals("")))
    {
      if (ListStations.c(this.a, str))
      {
        this.a.showMessage(this.a.getString(2131296415));
        paramDialogInterface.dismiss();
        return;
      }
      this.a.showMessage(this.a.getString(2131296413));
      return;
    }
    this.a.showMessage(this.a.getString(2131296412));
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.list.h
 * JD-Core Version:    0.6.2
 */