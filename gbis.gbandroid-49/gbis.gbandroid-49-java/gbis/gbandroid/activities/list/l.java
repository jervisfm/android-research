package gbis.gbandroid.activities.list;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import gbis.gbandroid.entities.ListMessage;

final class l
  implements AdapterView.OnItemLongClickListener
{
  l(ListStations paramListStations)
  {
  }

  public final boolean onItemLongClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    ListMessage localListMessage = (ListMessage)paramAdapterView.getItemAtPosition(paramInt);
    if (localListMessage.getStationId() > 0)
    {
      ListStations.d(this.a, localListMessage);
      ListStations.a(this.a, "ContextMenu");
      return true;
    }
    return false;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.list.l
 * JD-Core Version:    0.6.2
 */