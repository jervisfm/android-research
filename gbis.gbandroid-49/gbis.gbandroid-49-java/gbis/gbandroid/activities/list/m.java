package gbis.gbandroid.activities.list;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;

final class m
  implements AdapterView.OnItemSelectedListener
{
  m(ListStations paramListStations)
  {
  }

  public final void onItemSelected(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    if (paramView != null)
    {
      if (ListStations.J(this.a) != null)
        ListStations.J(this.a).setBackgroundColor(-1);
      ListStations.a(this.a, paramView);
      paramView.setBackgroundColor(0);
    }
  }

  public final void onNothingSelected(AdapterView<?> paramAdapterView)
  {
    if (ListStations.J(this.a) != null)
    {
      ListStations.J(this.a).setBackgroundColor(-1);
      ListStations.a(this.a, null);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.list.m
 * JD-Core Version:    0.6.2
 */