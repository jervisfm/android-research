package gbis.gbandroid.activities.list;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import gbis.gbandroid.entities.ListMessage;
import gbis.gbandroid.entities.ListResults;

final class n
  implements AdapterView.OnItemClickListener
{
  n(ListStations paramListStations)
  {
  }

  public final void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    ListMessage localListMessage = (ListMessage)paramAdapterView.getItemAtPosition(paramInt);
    if (localListMessage != null)
    {
      if (localListMessage.getStationId() <= 0)
        break label33;
      ListStations.b(this.a, localListMessage);
    }
    label33: 
    do
    {
      return;
      if (localListMessage.getStationId() == -2)
      {
        ListStations.K(this.a);
        return;
      }
    }
    while (localListMessage.getStationId() != -3);
    ListStations.a(this.a, ListStations.f(this.a), ListStations.y(this.a).getSearchTerms(), ListStations.L(this.a));
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.list.n
 * JD-Core Version:    0.6.2
 */