package gbis.gbandroid.activities.list;

import android.view.ViewGroup;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdRequest.ErrorCode;
import gbis.gbandroid.views.layouts.CustomListView;

final class o
  implements AdListener
{
  int a = 0;

  o(ListStations paramListStations, AdRequest paramAdRequest, ViewGroup paramViewGroup)
  {
  }

  public final void onDismissScreen(Ad paramAd)
  {
  }

  public final void onFailedToReceiveAd(Ad paramAd, AdRequest.ErrorCode paramErrorCode)
  {
    if (this.a <= 0)
    {
      paramAd.loadAd(this.c);
      this.a = (1 + this.a);
    }
    do
    {
      return;
      this.d.getLayoutParams().height = 0;
    }
    while (ListStations.q(this.b) == null);
    ListStations.q(this.b).invalidateViews();
  }

  public final void onLeaveApplication(Ad paramAd)
  {
  }

  public final void onPresentScreen(Ad paramAd)
  {
  }

  public final void onReceiveAd(Ad paramAd)
  {
    if (ListStations.u(this.b) != null)
      ListStations.u(this.b).notifyDataSetChanged();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.list.o
 * JD-Core Version:    0.6.2
 */