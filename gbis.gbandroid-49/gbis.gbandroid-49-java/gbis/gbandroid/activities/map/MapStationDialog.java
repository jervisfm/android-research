package gbis.gbandroid.activities.map;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;
import gbis.gbandroid.GBApplication;
import gbis.gbandroid.activities.base.GBActivityMap;
import gbis.gbandroid.entities.Station;
import gbis.gbandroid.utils.VerifyFields;
import gbis.gbandroid.views.CustomDialog;
import gbis.gbandroid.views.CustomDialog.Builder;
import gbis.gbandroid.views.CustomMapPin;
import gbis.gbandroid.views.MyItemizedOverlay;
import java.math.BigDecimal;
import java.util.List;

public class MapStationDialog extends GBActivityMap
{
  private MapView a;
  private BigDecimal b;
  private SharedPreferences c;
  private String d;
  private Station e;
  private Bitmap f;

  private void a()
  {
    BitmapFactory.Options localOptions = new BitmapFactory.Options();
    MapController localMapController = this.a.getController();
    localMapController.setZoom(16);
    DisplayMetrics localDisplayMetrics = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
    float f1 = localDisplayMetrics.density;
    CustomMapPin localCustomMapPin;
    if (this.f != null)
    {
      Bitmap localBitmap3 = BitmapFactory.decodeResource(getResources(), 2130837631, localOptions);
      Bitmap localBitmap4 = this.f;
      if (this.b.compareTo(BigDecimal.ZERO) > 0);
      for (String str2 = this.b.toString(); ; str2 = "")
      {
        localCustomMapPin = new CustomMapPin(localBitmap3, localBitmap4, str2, f1);
        a locala = new a(localCustomMapPin, this.mTracker);
        GeoPoint localGeoPoint = new GeoPoint((int)(1000000.0D * this.e.getLatitude()), (int)(1000000.0D * this.e.getLongitude()));
        localMapController.animateTo(localGeoPoint);
        locala.addOverlay(new OverlayItem(localGeoPoint, "", ""));
        this.a.getOverlays().add(locala);
        return;
      }
    }
    Bitmap localBitmap1 = BitmapFactory.decodeResource(getResources(), 2130837631, localOptions);
    Bitmap localBitmap2 = BitmapFactory.decodeResource(getResources(), 2130837622, localOptions);
    if (this.b.compareTo(BigDecimal.ZERO) > 0);
    for (String str1 = this.b.toString(); ; str1 = "")
    {
      localCustomMapPin = new CustomMapPin(localBitmap1, localBitmap2, str1, f1);
      break;
    }
  }

  private void b()
  {
    this.d = this.c.getString("fuelPreference", "");
  }

  private String c()
  {
    if (this.d.equals(getString(2131296713)))
      return getString(2131296717) + ": ";
    if (this.d.equals(getString(2131296714)))
      return getString(2131296718) + ": ";
    if (this.d.equals(getString(2131296715)))
      return getString(2131296719) + ": ";
    if (this.d.equals(getString(2131296716)))
      return getString(2131296720) + ": ";
    return "";
  }

  protected boolean isRouteDisplayed()
  {
    return false;
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903124);
    this.a = ((MapView)findViewById(2131165546));
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
    {
      this.e = ((Station)localBundle.getParcelable("station"));
      this.d = this.mPrefs.getString("fuelPreference", "");
      if (this.e.getCountry().equals("USA"))
      {
        this.b = VerifyFields.doubleToScale(this.e.getPrice(this, this.d), 2);
        this.f = GBApplication.getInstance().getLogo(this.e.getGasBrandId(), "/Android/data/gbis.gbandroid/cache/Brands");
      }
    }
    while (true)
    {
      this.c = PreferenceManager.getDefaultSharedPreferences(this);
      b();
      populateButtons();
      a();
      return;
      this.b = VerifyFields.doubleToScale(this.e.getPrice(this, this.d), 1);
      break;
      finish();
    }
  }

  public void populateButtons()
  {
    ((TextView)findViewById(2131165232)).setText(this.e.getStationName());
    if (this.f != null)
    {
      ((ImageView)findViewById(2131165231)).setImageBitmap(this.f);
      return;
    }
    ((ImageView)findViewById(2131165231)).setImageDrawable(this.mRes.getDrawable(2130837622));
  }

  protected String setAnalyticsPageName()
  {
    return getString(2131296846);
  }

  private final class a extends MyItemizedOverlay
  {
    public a(Drawable paramGoogleAnalyticsTracker, GoogleAnalyticsTracker arg3)
    {
      super(localGoogleAnalyticsTracker);
    }

    protected final boolean onTap(int paramInt)
    {
      CustomDialog.Builder localBuilder = new CustomDialog.Builder(MapStationDialog.this);
      View localView = MapStationDialog.a(MapStationDialog.this).inflate(2130903116, null);
      localBuilder.setContentView(localView);
      localBuilder.setTitle(MapStationDialog.b(MapStationDialog.this).getStationName());
      localBuilder.setStationLogo(MapStationDialog.b(MapStationDialog.this).getGasBrandId(), "/Android/data/gbis.gbandroid/cache/Brands");
      if (MapStationDialog.c(MapStationDialog.this).compareTo(BigDecimal.ZERO) > 0)
      {
        ((TextView)localView.findViewById(2131165463)).setText(MapStationDialog.d(MapStationDialog.this));
        ((TextView)localView.findViewById(2131165464)).setText(MapStationDialog.c(MapStationDialog.this));
        if (MapStationDialog.b(MapStationDialog.this).getCountry().equals("USA"))
        {
          ((TextView)localView.findViewById(2131165465)).setVisibility(0);
          ((TextView)localView.findViewById(2131165466)).setText("Updated: " + MapStationDialog.b(MapStationDialog.this).getTimeSpottedConverted(MapStationDialog.this, MapStationDialog.e(MapStationDialog.this)));
        }
      }
      while (true)
      {
        ((TextView)localView.findViewById(2131165459)).setText(MapStationDialog.b(MapStationDialog.this).getAddress());
        ((TextView)localView.findViewById(2131165460)).setText(MapStationDialog.b(MapStationDialog.this).getCity());
        CustomDialog localCustomDialog = localBuilder.create();
        localCustomDialog.getWindow().setFlags(4, 4);
        localCustomDialog.getWindow().addFlags(2);
        localCustomDialog.getWindow().getAttributes().dimAmount = 0.5F;
        localCustomDialog.show();
        return true;
        ((TextView)localView.findViewById(2131165465)).setVisibility(8);
        break;
        ((TextView)localView.findViewById(2131165463)).setVisibility(8);
        ((TextView)localView.findViewById(2131165464)).setVisibility(8);
        ((TextView)localView.findViewById(2131165466)).setVisibility(8);
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.map.MapStationDialog
 * JD-Core Version:    0.6.2
 */