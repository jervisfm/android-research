package gbis.gbandroid.activities.map;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.location.Location;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.preference.PreferenceManager;
import android.text.InputFilter;
import android.text.InputFilter.LengthFilter;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import gbis.gbandroid.activities.base.GBActivityMap;
import gbis.gbandroid.activities.base.GBActivityMap.MyOwnLocationOverlay;
import gbis.gbandroid.activities.list.ListStations;
import gbis.gbandroid.content.GBIntent;
import gbis.gbandroid.entities.ListMessage;
import gbis.gbandroid.entities.ListResults;
import gbis.gbandroid.entities.MapResults;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.queries.ListQuery;
import gbis.gbandroid.utils.CustomAsyncTask;
import gbis.gbandroid.utils.DBHelper;
import gbis.gbandroid.utils.DateUtils;
import gbis.gbandroid.views.CustomDialog.Builder;
import gbis.gbandroid.views.StationsMapView;
import java.lang.reflect.Type;
import java.util.List;

public class MapStations extends MapStationsBase
{
  public static final String COORDS = "coordinates";
  private MapController a;
  private d b;
  private RelativeLayout c;
  private b d;
  private boolean e;
  private boolean f;
  private PowerManager.WakeLock g;
  private DBHelper h;
  private CustomAsyncTask i;

  private void a()
  {
    this.c = ((RelativeLayout)findViewById(2131165455));
    g();
    ((RelativeLayout)findViewById(2131165457)).setOnClickListener(new a(this));
  }

  private void a(Bundle paramBundle)
  {
    int j = paramBundle.getInt("sm_id");
    String str = this.mPrefs.getString("fuelPreference", "");
    double d1;
    if (str.equals(this.mRes.getStringArray(2131099649)[0]))
      d1 = paramBundle.getDouble("fuel regular");
    while (true)
    {
      int k;
      if ((d1 > 0.0D) && (this.listMessages != null))
        k = this.listMessages.size();
      label228: for (int m = 0; ; m++)
      {
        if (m >= k);
        while (true)
        {
          locateOnMap();
          return;
          if (str.equals(this.mRes.getStringArray(2131099649)[1]))
          {
            d1 = paramBundle.getDouble("fuel midgrade");
            break;
          }
          if (str.equals(this.mRes.getStringArray(2131099649)[2]))
          {
            d1 = paramBundle.getDouble("fuel premium");
            break;
          }
          if (!str.equals(this.mRes.getStringArray(2131099649)[3]))
            break label234;
          d1 = paramBundle.getDouble("fuel diesel");
          break;
          ListMessage localListMessage = (ListMessage)this.listMessages.get(m);
          if (localListMessage.getStationId() != j)
            break label228;
          localListMessage.setPrice(d1);
          localListMessage.setTimeSpotted(paramBundle.getString("time spotted"));
          localListMessage.setTimeOffset(DateUtils.getNowOffset());
        }
      }
      label234: d1 = 0.0D;
    }
  }

  private void a(ListResults paramListResults)
  {
    GBIntent localGBIntent = new GBIntent(this, ListStations.class, getLocation());
    localGBIntent.putExtra("list", paramListResults);
    localGBIntent.putExtra("center latitude", this.center.getLatitudeE6());
    localGBIntent.putExtra("center longitude", this.center.getLongitudeE6());
    localGBIntent.putExtra("city", "");
    localGBIntent.putExtra("state", "");
    localGBIntent.putExtra("zip", "");
    unregisterPreferencesListener();
    startActivityForResult(localGBIntent, 1);
    setResult(-1);
    finish();
  }

  private void b()
  {
    this.d = new b(this);
    this.d.execute(new Object[0]);
    loadDialog(getString(2131296390), this.d);
  }

  private void c()
  {
    if (this.i != null)
      this.i.cancel(true);
    e();
    this.i = new c(this, (byte)0);
    this.i.execute(new Object[0]);
  }

  private void d()
  {
    this.center = this.mapView.getMapCenter();
    Type localType = new b(this).getType();
    this.mResponseObject = new ListQuery(this, this.mPrefs, localType, getLocation()).getResponseObject("", "", "", this.center);
  }

  private void e()
  {
    h();
  }

  private void f()
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this);
    View localView = this.mInflater.inflate(2130903057, null);
    TextView localTextView = (TextView)localView.findViewById(2131165246);
    EditText localEditText = (EditText)localView.findViewById(2131165247);
    InputFilter[] arrayOfInputFilter = new InputFilter[1];
    arrayOfInputFilter[0] = new InputFilter.LengthFilter(30);
    localEditText.setFilters(arrayOfInputFilter);
    localEditText.setImeOptions(6);
    localBuilder.setTitle(getString(2131296544));
    localBuilder.setContentView(localView);
    localTextView.setText(getString(2131296573));
    localBuilder.setPositiveButton(getString(2131296675), new c(this, localEditText));
    localBuilder.create().show();
  }

  private void g()
  {
    if (this.c != null)
      this.c.setVisibility(4);
  }

  private void h()
  {
    if (this.c != null)
      this.c.setVisibility(0);
  }

  private void i()
  {
    this.mapView.getOverlays().clear();
    loadMapPins();
  }

  private double j()
  {
    return 111.0D * Math.sqrt(Math.pow(Math.abs(this.center.getLatitudeE6()) - Math.abs(this.myLocationOverlay.getMyLocation().getLatitudeE6()), 2.0D) + Math.pow(Math.abs(this.center.getLongitudeE6()) - Math.abs(this.myLocationOverlay.getMyLocation().getLongitudeE6()), 2.0D)) / 0.001D / 1000000.0D;
  }

  private boolean k()
  {
    return j() > 400.0D;
  }

  protected boolean addPlace(String paramString)
  {
    return this.h.addSearchesRow(paramString, null, this.center.getLatitudeE6() / 1000000.0D, this.center.getLongitudeE6() / 1000000.0D);
  }

  public void closeDialog()
  {
    this.progress.dismiss();
  }

  public Location getLocation()
  {
    return super.getLocation();
  }

  public GeoPoint getMyLocation()
  {
    return this.myLocationOverlay.getMyLocation();
  }

  public void loadMapPins()
  {
    try
    {
      if (this.i != null)
        this.i.cancel(true);
      e();
      this.i = new c(this);
      this.i.execute(new Object[0]);
      return;
    }
    catch (Exception localException)
    {
      g();
    }
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    switch (paramInt1)
    {
    default:
    case 4:
    case 3:
    }
    do
    {
      do
      {
        do
          return;
        while (paramInt2 == 0);
        if (paramInt2 == -1)
        {
          setResult(-1);
          finish();
          return;
        }
        if (paramInt2 == 9)
        {
          showMessage(this.mRes.getString(2131296257));
          return;
        }
      }
      while (paramInt2 != 3);
      a(paramIntent.getExtras());
      return;
    }
    while ((paramInt2 == 0) || (paramInt2 != 3));
    a(paramIntent.getExtras());
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    loadMapPins();
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Object localObject = getLastNonConfigurationInstance();
    this.userMessage = "";
    this.e = false;
    this.f = true;
    this.b = new d((byte)0);
    this.mPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
    try
    {
      Bundle localBundle = getIntent().getExtras();
      if (localBundle != null)
      {
        setContentView(2130903114);
        this.mapView = ((StationsMapView)findViewById(2131165208));
        this.mapView.setControl(this);
        this.a = this.mapView.getController();
        if (localObject == null)
        {
          this.a.setZoom(14);
          this.mapResults = ((MapResults)localBundle.getParcelable("map"));
          this.center = new GeoPoint(localBundle.getInt("center latitude"), localBundle.getInt("center longitude"));
          if (this.mapResults != null)
          {
            this.listMessages = this.mapResults.getListMessage();
            showMoreStationsMessage(this.mapResults.getTotalStations());
          }
        }
        while (true)
        {
          this.myLocationOverlay = new a(this, this.mapView);
          this.mapOverlays = this.mapView.getOverlays();
          this.mapOverlays.add(this.myLocationOverlay);
          a();
          locateOnMap();
          this.a.animateTo(this.center);
          this.mapView.setBuiltInZoomControls(true);
          this.mapView.setSatellite(false);
          this.myLocationOverlay.enableMyLocation();
          this.myLocationOverlay.enableCompass();
          this.g = ((PowerManager)getSystemService("power")).newWakeLock(6, "WakeLock");
          return;
          this.a.setZoom(((Bundle)localObject).getInt("zoom"));
          this.center = new GeoPoint(((Bundle)localObject).getInt("center latitude"), ((Bundle)localObject).getInt("center longitude"));
          queryMapWebService();
        }
      }
    }
    catch (Exception localException)
    {
      finish();
    }
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    super.onCreateOptionsMenu(paramMenu);
    getMenuInflater().inflate(2131427332, paramMenu);
    return super.onCreateOptionsMenu(paramMenu);
  }

  public void onDestroy()
  {
    super.onDestroy();
    try
    {
      if (this.mapView != null)
      {
        this.mapView.removeAllViews();
        this.mapView = null;
      }
      this.mPrefs.unregisterOnSharedPreferenceChangeListener(this.b);
      return;
    }
    catch (Exception localException)
    {
    }
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    super.onOptionsItemSelected(paramMenuItem);
    switch (paramMenuItem.getItemId())
    {
    case 2131165583:
    case 2131165584:
    case 2131165585:
    default:
      return super.onOptionsItemSelected(paramMenuItem);
    case 2131165586:
      c();
      return true;
    case 2131165587:
      try
      {
        if (paramMenuItem.getTitle().equals(getString(2131296637)))
        {
          this.e = true;
          this.a.animateTo(this.myLocationOverlay.getMyLocation());
          this.myLocationOverlay.setFollowMe(this.e);
          this.g.acquire();
          return true;
        }
      }
      catch (Exception localException)
      {
        showMessage(getString(2131296287));
        return true;
      }
      this.e = false;
      this.g.release();
      this.myLocationOverlay.setFollowMe(this.e);
      return true;
    case 2131165588:
      b();
      return true;
    case 2131165591:
      showHome();
      return true;
    case 2131165590:
      launchSettings();
      return true;
    case 2131165589:
      showCityZipSearch();
      return true;
    case 2131165582:
    }
    this.h = new DBHelper(this);
    if (this.h.openDB())
    {
      f();
      return true;
    }
    showMessage(getString(2131296258));
    return true;
  }

  public void onPause()
  {
    super.onPause();
    try
    {
      this.myLocationOverlay.disableCompass();
      this.myLocationOverlay.disableMyLocation();
      this.myLocationOverlay.setFollowMe(false);
      this.g.release();
      return;
    }
    catch (Exception localException)
    {
    }
  }

  public boolean onPrepareOptionsMenu(Menu paramMenu)
  {
    if (!this.e)
    {
      paramMenu.getItem(1).setTitle(getString(2131296637));
      return true;
    }
    paramMenu.getItem(1).setTitle(getString(2131296638));
    return true;
  }

  protected void onResume()
  {
    super.onResume();
    if (this.listMessages == null)
    {
      Bundle localBundle = getIntent().getExtras();
      if (localBundle != null)
      {
        MapResults localMapResults = (MapResults)localBundle.getParcelable("map");
        if (localMapResults != null)
          this.listMessages = localMapResults.getListMessage();
      }
    }
    if (this.mapView == null)
    {
      this.mapView = ((StationsMapView)findViewById(2131165208));
      this.mapView.setControl(this);
      this.mapView.setBuiltInZoomControls(true);
      this.mapView.setSatellite(false);
    }
    if (this.mapOverlays == null)
      this.mapOverlays = this.mapView.getOverlays();
    if (this.myLocationOverlay == null)
    {
      this.myLocationOverlay = new a(this, this.mapView);
      this.mapOverlays.add(this.myLocationOverlay);
    }
    this.myLocationOverlay.enableMyLocation();
    this.myLocationOverlay.enableCompass();
    if (this.e)
    {
      if (this.a == null)
        this.a = this.mapView.getController();
      if (this.myLocationOverlay == null)
        this.myLocationOverlay = new a(this, this.mapView);
      this.a.animateTo(this.myLocationOverlay.getMyLocation());
      this.myLocationOverlay.setFollowMe(this.e);
      if (this.g == null)
        this.g = ((PowerManager)getSystemService("power")).newWakeLock(6, "WakeLock");
      this.g.acquire();
    }
  }

  public Object onRetainNonConfigurationInstance()
  {
    Bundle localBundle = new Bundle();
    localBundle.putInt("zoom", this.mapView.getZoomLevel());
    localBundle.putInt("center latitude", this.center.getLatitudeE6());
    localBundle.putInt("center longitude", this.center.getLongitudeE6());
    return localBundle;
  }

  public boolean onSearchRequested()
  {
    setAnalyticsTrackEvent(setAnalyticsPageName(), "PhoneButton", getString(2131296605), 0);
    showCityZipSearch();
    return true;
  }

  public void onStart()
  {
    super.onStart();
    this.mPrefs.registerOnSharedPreferenceChangeListener(this.b);
  }

  public void onStop()
  {
    super.onStop();
    if (this.h != null)
      this.h.closeDB();
  }

  protected String setAnalyticsPageName()
  {
    return getString(2131296850);
  }

  public void unregisterPreferencesListener()
  {
    this.mPrefs.unregisterOnSharedPreferenceChangeListener(this.b);
  }

  private final class a extends GBActivityMap.MyOwnLocationOverlay
  {
    public a(Context paramMapView, MapView arg3)
    {
      super(paramMapView, localMapView);
    }

    public final void onLocationChanged(Location paramLocation)
    {
      super.onLocationChanged(paramLocation);
      if (isFollowMe())
      {
        MapStations.b(MapStations.this).animateTo(MapStations.this.myLocationOverlay.getMyLocation());
        if ((MapStations.c(MapStations.this)) && (MapStations.d(MapStations.this)))
          MapStations.e(MapStations.this);
      }
    }
  }

  private final class b extends CustomAsyncTask
  {
    public b(GBActivityMap arg2)
    {
      super();
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      try
      {
        MapStations.h(MapStations.this).dismiss();
        label15: if (paramBoolean.booleanValue())
        {
          ListResults localListResults = (ListResults)MapStations.i(MapStations.this).getPayload();
          MapStations.a(MapStations.this, localListResults);
        }
        return;
      }
      catch (Exception localException)
      {
        break label15;
      }
    }

    protected final boolean queryWebService()
    {
      MapStations.j(MapStations.this);
      return true;
    }
  }

  private final class c extends CustomAsyncTask
  {
    private boolean b;

    public c(GBActivityMap arg2)
    {
      super();
    }

    public c(GBActivityMap paramByte, byte arg3)
    {
      super();
      this.b = true;
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      MapStations.f(MapStations.this);
      if (paramBoolean.booleanValue())
      {
        MapStations.this.locateOnMap();
        if (MapStations.this.zoomLevelFlag)
          break label67;
        ((TextView)MapStations.this.findViewById(2131165210)).setVisibility(4);
      }
      while (true)
      {
        if (this.b)
          MapStations.g(MapStations.this);
        return;
        label67: ((TextView)MapStations.this.findViewById(2131165210)).setVisibility(0);
      }
    }

    protected final boolean queryWebService()
    {
      if (!this.b);
      while (true)
      {
        try
        {
          MapStations.this.center = MapStations.this.mapView.getMapCenter();
          MapStations.this.queryMapWebService();
          return true;
        }
        catch (RuntimeException localRuntimeException)
        {
          if (localRuntimeException.getClass().toString().equals("class android.view.ViewRoot$CalledFromWrongThreadException"))
            continue;
          return false;
        }
        if ((MapStations.this.myLocationOverlay == null) || (MapStations.this.myLocationOverlay.getMyLocation() == null))
          break;
        MapStations.b(MapStations.this).animateTo(MapStations.this.myLocationOverlay.getMyLocation());
        MapStations.this.center = new GeoPoint(MapStations.this.myLocationOverlay.getMyLocation().getLatitudeE6(), MapStations.this.myLocationOverlay.getMyLocation().getLongitudeE6());
        MapStations.this.queryMapWebService();
      }
      MapStations.this.setMessage(MapStations.this.getString(2131296287));
      return false;
    }
  }

  private final class d
    implements SharedPreferences.OnSharedPreferenceChangeListener
  {
    private d()
    {
    }

    public final void onSharedPreferenceChanged(SharedPreferences paramSharedPreferences, String paramString)
    {
      if ((paramString.equals("fuelPreference")) || (paramString.equals("noPricesPreference")))
        MapStations.a(MapStations.this);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.map.MapStations
 * JD-Core Version:    0.6.2
 */