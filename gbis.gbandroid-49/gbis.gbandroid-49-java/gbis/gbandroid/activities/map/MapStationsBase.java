package gbis.gbandroid.activities.map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;
import gbis.gbandroid.GBApplication;
import gbis.gbandroid.activities.base.GBActivityMap;
import gbis.gbandroid.activities.base.GBActivityMap.MyOwnLocationOverlay;
import gbis.gbandroid.entities.ChoiceHotels;
import gbis.gbandroid.entities.ListMessage;
import gbis.gbandroid.entities.MapResults;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.queries.MapQuery;
import gbis.gbandroid.utils.ImageLoader;
import gbis.gbandroid.utils.VerifyFields;
import gbis.gbandroid.views.CustomMapPin;
import gbis.gbandroid.views.MapAdOverlay;
import gbis.gbandroid.views.MapStationOverlay;
import gbis.gbandroid.views.StationsMapView;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.WeakHashMap;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;

public abstract class MapStationsBase extends GBActivityMap
{
  private WeakHashMap<Integer, Bitmap> a = new WeakHashMap();
  private WeakHashMap<Integer, Bitmap> b = new WeakHashMap();
  protected GeoPoint center;
  protected MapStationOverlay itemizedoverlay;
  protected List<ListMessage> listMessages;
  protected Bitmap logoDefault;
  protected List<Overlay> mapOverlays;
  protected MapResults mapResults;
  protected StationsMapView mapView;
  protected Bitmap mappinDefault;
  protected Bitmap mappinFocus;
  protected Bitmap mappinPress;
  protected GBActivityMap.MyOwnLocationOverlay myLocationOverlay;
  protected boolean zoomLevelFlag;

  private Bitmap a(int paramInt)
  {
    if (this.a.containsKey(Integer.valueOf(paramInt)))
      return (Bitmap)this.a.get(Integer.valueOf(paramInt));
    Bitmap localBitmap = GBApplication.getInstance().getLogo(paramInt, "/Android/data/gbis.gbandroid/cache/Brands");
    if (localBitmap != null)
    {
      this.a.put(Integer.valueOf(paramInt), localBitmap);
      return localBitmap;
    }
    return this.logoDefault;
  }

  private void a()
  {
    Collections.sort(this.listMessages, new a());
    Collections.reverse(this.listMessages);
  }

  private void a(float paramFloat, boolean paramBoolean)
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.listMessages.iterator();
    int j;
    int n;
    if (!localIterator.hasNext())
    {
      j = 0;
      if (j >= this.mapOverlays.size())
      {
        int m = localArrayList.size();
        n = 0;
        label57: if (n < m)
          break label612;
      }
    }
    else
    {
      ListMessage localListMessage = (ListMessage)localIterator.next();
      StateListDrawable localStateListDrawable = new StateListDrawable();
      Bitmap localBitmap = a(localListMessage.getGasBrandId());
      if (localListMessage.getPrice() != 0.0D)
        if (localListMessage.getCountry().equals("USA"))
        {
          BigDecimal localBigDecimal2 = VerifyFields.doubleToScale(localListMessage.getPrice(), 2);
          if (paramBoolean)
          {
            localStateListDrawable.addState(new int[] { 16842908 }, new CustomMapPin(this.mappinFocus, localBitmap, localBigDecimal2.toString(), paramFloat));
            localStateListDrawable.addState(new int[] { 16842913 }, new CustomMapPin(this.mappinPress, localBitmap, localBigDecimal2.toString(), paramFloat));
          }
          localStateListDrawable.addState(new int[0], new CustomMapPin(this.mappinDefault, localBitmap, localBigDecimal2.toString(), paramFloat));
        }
      while (true)
      {
        int i = localStateListDrawable.getIntrinsicWidth();
        localStateListDrawable.setBounds(-13, 0 - localStateListDrawable.getIntrinsicHeight(), i - 13, 0);
        this.itemizedoverlay = new MapStationOverlay(localStateListDrawable, localListMessage, this, paramBoolean, this.mTracker);
        OverlayItem localOverlayItem = new OverlayItem(new GeoPoint((int)(1000000.0D * localListMessage.getLatitude()), (int)(1000000.0D * localListMessage.getLongitude())), localListMessage.getStationName(), Integer.toString(localListMessage.getStationId()));
        this.itemizedoverlay.addOverlay(localOverlayItem);
        localArrayList.add(this.itemizedoverlay);
        break;
        BigDecimal localBigDecimal1 = VerifyFields.doubleToScale(localListMessage.getPrice(), 1);
        if (paramBoolean)
        {
          localStateListDrawable.addState(new int[] { 16842908 }, new CustomMapPin(this.mappinFocus, localBitmap, localBigDecimal1.toString(), paramFloat));
          localStateListDrawable.addState(new int[] { 16842913 }, new CustomMapPin(this.mappinPress, localBitmap, localBigDecimal1.toString(), paramFloat));
        }
        localStateListDrawable.addState(new int[0], new CustomMapPin(this.mappinDefault, localBitmap, localBigDecimal1.toString(), paramFloat));
        continue;
        VerifyFields.doubleToScale(localListMessage.getPrice(), 2);
        localStateListDrawable.addState(new int[] { 16842908 }, new CustomMapPin(this.mappinFocus, localBitmap, "", paramFloat));
        localStateListDrawable.addState(new int[] { 16842913 }, new CustomMapPin(this.mappinPress, localBitmap, "", paramFloat));
        localStateListDrawable.addState(new int[0], new CustomMapPin(this.mappinDefault, localBitmap, "", paramFloat));
      }
    }
    Overlay localOverlay = (Overlay)this.mapOverlays.get(j);
    if (((localOverlay instanceof MapStationOverlay)) && (!localArrayList.contains(localOverlay)))
      this.mapOverlays.remove(j);
    for (int k = j - 1; ; k = j)
    {
      j = k + 1;
      break;
      label612: if (!this.mapOverlays.contains(localArrayList.get(n)))
        this.mapOverlays.add((Overlay)localArrayList.get(n));
      n++;
      break label57;
    }
  }

  private void a(int paramInt, float paramFloat, boolean paramBoolean)
  {
    new LogoDownloadAsyncTask(this, paramInt, paramFloat, paramBoolean).execute(new String[0]);
  }

  private Bitmap b(int paramInt)
  {
    if (this.b.containsKey(Integer.valueOf(paramInt)))
      return (Bitmap)this.b.get(Integer.valueOf(paramInt));
    Bitmap localBitmap = ImageLoader.getExistentImage(this, "/Android/data/gbis.gbandroid/cache/Ads", String.valueOf(paramInt), 0);
    if (localBitmap != null)
    {
      this.b.put(Integer.valueOf(paramInt), localBitmap);
      return localBitmap;
    }
    return this.logoDefault;
  }

  private void b(float paramFloat, boolean paramBoolean)
  {
    int i = 0;
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.mapResults.getChoiceHotels().iterator();
    int k;
    while (true)
    {
      if (!localIterator.hasNext())
      {
        k = 0;
        if (k < this.mapOverlays.size())
          break;
        int n = localArrayList.size();
        label61: if (i < n)
          break label395;
        return;
      }
      ChoiceHotels localChoiceHotels = (ChoiceHotels)localIterator.next();
      if (ImageLoader.isImageExistant(this, "/Android/data/gbis.gbandroid/cache/Ads", String.valueOf(localChoiceHotels.getAdId())))
      {
        StateListDrawable localStateListDrawable = new StateListDrawable();
        Bitmap localBitmap = b(localChoiceHotels.getAdId());
        localStateListDrawable.addState(new int[] { 16842908 }, new CustomMapPin(this.mappinFocus, localBitmap, "", paramFloat));
        localStateListDrawable.addState(new int[] { 16842913 }, new CustomMapPin(this.mappinPress, localBitmap, "", paramFloat));
        localStateListDrawable.addState(new int[0], new CustomMapPin(this.mappinDefault, localBitmap, "", paramFloat));
        int j = localStateListDrawable.getIntrinsicWidth();
        localStateListDrawable.setBounds(-13, 0 - localStateListDrawable.getIntrinsicHeight(), j - 13, 0);
        MapAdOverlay localMapAdOverlay = new MapAdOverlay(localStateListDrawable, localChoiceHotels, this, paramBoolean, this.mTracker);
        localMapAdOverlay.addOverlay(new OverlayItem(new GeoPoint((int)(1000000.0D * localChoiceHotels.getLatitude()), (int)(1000000.0D * localChoiceHotels.getLongitude())), localChoiceHotels.getName(), localChoiceHotels.getBrandId()));
        localArrayList.add(localMapAdOverlay);
      }
      else
      {
        a(localChoiceHotels.getAdId(), paramFloat, paramBoolean);
      }
    }
    Overlay localOverlay = (Overlay)this.mapOverlays.get(k);
    if (((localOverlay instanceof MapAdOverlay)) && ((!localArrayList.contains(localOverlay)) || (this.mapResults.isAdsOnTop())))
      this.mapOverlays.remove(k);
    for (int m = k - 1; ; m = k)
    {
      k = m + 1;
      break;
      label395: if (!this.mapOverlays.contains(localArrayList.get(i)))
        this.mapOverlays.add((Overlay)localArrayList.get(i));
      i++;
      break label61;
    }
  }

  public double getMapDistance()
  {
    if (this.mapView == null)
      this.mapView = ((StationsMapView)findViewById(2131165208));
    if (this.center == null)
      this.center = this.mapView.getMapCenter();
    return 111.0D * Math.sqrt(Math.pow(Math.abs(this.center.getLatitudeE6()) - Math.abs(this.mapView.getMapCenter().getLatitudeE6()), 2.0D) + Math.pow(Math.abs(this.center.getLongitudeE6()) - Math.abs(this.mapView.getMapCenter().getLongitudeE6()), 2.0D)) / 0.001D / 1000000.0D;
  }

  public abstract void loadMapPins();

  protected void locateOnMap()
  {
    locateOnMap(true);
  }

  protected void locateOnMap(boolean paramBoolean)
  {
    a();
    if (this.mapView == null)
      this.mapView = ((StationsMapView)findViewById(2131165208));
    this.mapOverlays = this.mapView.getOverlays();
    DisplayMetrics localDisplayMetrics = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
    float f = localDisplayMetrics.density;
    try
    {
      if (this.mapResults.isAdsOnTop())
      {
        a(f, paramBoolean);
        b(f, paramBoolean);
      }
      while (true)
      {
        this.mapOverlays.remove(this.myLocationOverlay);
        this.mapOverlays.add(this.myLocationOverlay);
        this.mapView.invalidate();
        return;
        b(f, paramBoolean);
        a(f, paramBoolean);
      }
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.mappinDefault = BitmapFactory.decodeResource(this.mRes, 2130837631);
    this.mappinPress = BitmapFactory.decodeResource(this.mRes, 2130837633);
    this.mappinFocus = BitmapFactory.decodeResource(this.mRes, 2130837632);
    this.logoDefault = BitmapFactory.decodeResource(this.mRes, 2130837622);
  }

  protected void onDestroy()
  {
    super.onDestroy();
    if (this.a != null)
      this.a.clear();
    if (this.b != null)
      this.b.clear();
  }

  protected void queryMapWebService()
  {
    Type localType = new d(this).getType();
    this.mResponseObject = new MapQuery(this, this.mPrefs, localType, getLocation()).getResponseObject(this.center, this.mapView.getLatitudeSpan(), this.mapView.getLongitudeSpan(), this.mapView.getHeight(), this.mapView.getWidth());
    this.mapResults = ((MapResults)this.mResponseObject.getPayload());
    this.listMessages = this.mapResults.getListMessage();
    showMoreStationsMessage(this.mapResults.getTotalStations());
  }

  protected void showMoreStationsMessage(int paramInt)
  {
    if (paramInt > this.listMessages.size())
    {
      this.zoomLevelFlag = true;
      return;
    }
    this.zoomLevelFlag = false;
  }

  public void showPinFront(ListMessage paramListMessage)
  {
    int i = this.mapOverlays.size();
    for (int j = 0; ; j++)
    {
      Object localObject;
      if (j >= i)
        localObject = null;
      while (true)
      {
        this.mapOverlays.add(localObject);
        ((MapStationOverlay)localObject).getItem(0).setMarker(((MapStationOverlay)localObject).getItem(0).getMarker(16842908));
        return;
        Overlay localOverlay = (Overlay)this.mapOverlays.get(j);
        if ((!(localOverlay instanceof MapStationOverlay)) || (!((MapStationOverlay)localOverlay).getItem(0).getSnippet().equals(Integer.toString(paramListMessage.getStationId()))))
          break;
        localObject = (MapStationOverlay)localOverlay;
        this.mapOverlays.remove(localOverlay);
      }
    }
  }

  public class LogoDownloadAsyncTask extends AsyncTask<String, Void, Bitmap>
  {
    private Context b;
    private int c;
    private float d;
    private boolean e;

    public LogoDownloadAsyncTask(Context paramInt, int paramFloat, float paramBoolean, boolean arg5)
    {
      this.b = paramInt;
      this.c = paramFloat;
      this.d = paramBoolean;
      boolean bool;
      this.e = bool;
    }

    protected Bitmap doInBackground(String[] paramArrayOfString)
    {
      File localFile = ImageLoader.getImageDirectory(this.b, "/Android/data/gbis.gbandroid/cache/Ads");
      return ImageLoader.getBitmap(GBApplication.getInstance().getImageAdUrl(this.c, MapStationsBase.a(MapStationsBase.this)), localFile, String.valueOf(this.c), 0);
    }

    protected void onPostExecute(Bitmap paramBitmap)
    {
      if (paramBitmap != null)
        MapStationsBase.a(MapStationsBase.this, this.d, this.e);
    }
  }

  public class MapOverlayItemMarkerAsyncTask extends AsyncTask<String, Void, Bitmap>
  {
    private OverlayItem b;
    private StationsMapView c;

    public MapOverlayItemMarkerAsyncTask(OverlayItem paramStationsMapView, StationsMapView arg3)
    {
      this.b = paramStationsMapView;
      Object localObject;
      this.c = localObject;
    }

    private static InputStream a(String paramString)
    {
      HttpGet localHttpGet = new HttpGet(URI.create(paramString));
      return new BufferedHttpEntity(new DefaultHttpClient().execute(localHttpGet).getEntity()).getContent();
    }

    protected Bitmap doInBackground(String[] paramArrayOfString)
    {
      try
      {
        InputStream localInputStream = a(paramArrayOfString[0]);
        Bitmap localBitmap = BitmapFactory.decodeStream(localInputStream, null, null);
        localInputStream.close();
        return localBitmap;
      }
      catch (IOException localIOException)
      {
      }
      return null;
    }

    protected void onPostExecute(Bitmap paramBitmap)
    {
      if (paramBitmap != null)
      {
        BitmapDrawable localBitmapDrawable = new BitmapDrawable(paramBitmap);
        localBitmapDrawable.setBounds(-localBitmapDrawable.getIntrinsicWidth() / 2, -localBitmapDrawable.getIntrinsicHeight(), localBitmapDrawable.getIntrinsicWidth() / 2, 0);
        this.b.setMarker(localBitmapDrawable);
        this.c.invalidate();
      }
    }
  }

  private final class a
    implements Comparator<ListMessage>
  {
    public a()
    {
    }

    private static int a(ListMessage paramListMessage1, ListMessage paramListMessage2)
    {
      if ((paramListMessage1.getPrice() == 0.0D) && (paramListMessage2.getPrice() != 0.0D))
        return 1;
      if ((paramListMessage2.getPrice() == 0.0D) && (paramListMessage1.getPrice() != 0.0D))
        return -1;
      return Double.compare(paramListMessage1.getPrice(), paramListMessage2.getPrice());
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.map.MapStationsBase
 * JD-Core Version:    0.6.2
 */