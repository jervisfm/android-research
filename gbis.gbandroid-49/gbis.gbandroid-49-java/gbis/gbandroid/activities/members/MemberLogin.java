package gbis.gbandroid.activities.members;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import gbis.gbandroid.activities.base.GBActivity;
import gbis.gbandroid.activities.base.GBActivityDialog;
import gbis.gbandroid.content.GBIntent;
import gbis.gbandroid.entities.LoginMessage;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.queries.MemberLoginQuery;
import gbis.gbandroid.utils.CustomAsyncTask;
import java.lang.reflect.Type;

public class MemberLogin extends GBActivityDialog
  implements View.OnClickListener
{
  private LoginMessage a;
  private String b;
  private String c;
  private EditText d;
  private EditText e;

  private void a()
  {
    this.d = ((EditText)findViewById(2131165264));
    this.e = ((EditText)findViewById(2131165265));
    addToEditTextList(this.d);
    addToEditTextList(this.e);
    this.d.setOnFocusChangeListener(new a(this));
    this.e.setOnFocusChangeListener(new b(this));
    this.e.setNextFocusDownId(findViewById(2131165237).getId());
    this.e.setOnEditorActionListener(new c(this));
  }

  private boolean a(String paramString1, String paramString2)
  {
    Type localType = new d(this).getType();
    this.mResponseObject = new MemberLoginQuery(this, this.mPrefs, localType, this.myLocation).getResponseObject(paramString1, paramString2);
    this.a = ((LoginMessage)this.mResponseObject.getPayload());
    return d();
  }

  private boolean b()
  {
    this.b = this.d.getText().toString();
    if (!this.b.equals(""))
    {
      this.c = this.e.getText().toString();
      if (!this.c.equals(""))
        return b(this.b, this.c);
      setMessage(getString(2131296382));
      return false;
    }
    setMessage(getString(2131296381));
    return false;
  }

  private boolean b(String paramString1, String paramString2)
  {
    if (a(paramString1, paramString2))
    {
      setMessage(getString(2131296379));
      return true;
    }
    return false;
  }

  private void c()
  {
    a locala = new a(this);
    locala.execute(new Object[0]);
    loadDialog(getString(2131296397), locala);
  }

  private boolean d()
  {
    return this.a.isSignedIn();
  }

  private void e()
  {
    SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(getBaseContext()).edit();
    localEditor.putString("car", this.a.getCar());
    localEditor.putInt("car_icon_id", this.a.getCarIconId());
    localEditor.putString("member_id", this.a.getMemberId());
    localEditor.putBoolean("is_member", true);
    localEditor.commit();
  }

  private void f()
  {
    startActivity(new GBIntent(this, ResetPassword.class, this.myLocation));
  }

  public static void logout(Context paramContext)
  {
    SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(paramContext).edit();
    localEditor.putString("car", "");
    localEditor.putInt("car_icon_id", 0);
    localEditor.putString("member_id", "");
    localEditor.putBoolean("awards_badge", false);
    localEditor.commit();
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    switch (paramInt1)
    {
    default:
    case 6:
    }
    do
      return;
    while (paramInt2 != -1);
    setResult(-1);
    finish();
  }

  public void onClick(View paramView)
  {
    if (paramView == getGBActivityDialogPositiveButton())
      c();
    do
    {
      return;
      if (paramView == getGBActivityDialogNegativeButton())
      {
        f();
        return;
      }
    }
    while (paramView != getGBActivityDialogNeutralButton());
    showRegistration();
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    setEditBoxHintConfiguration();
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    a();
  }

  protected void setAdsAfterGPSServiceConnected()
  {
  }

  protected String setAnalyticsPageName()
  {
    return getString(2131296823);
  }

  protected void setGBActivityDialogButtons()
  {
    setGBViewInContentLayout(2130903064, false);
    setGBActivityDialogTitle(getString(2131296523));
    setGBActivityDialogPositiveButton(2131296661, this);
    setGBActivityDialogNeutralButton(2131296663, this);
    setGBActivityDialogNegativeButton(2131296662, this);
  }

  private final class a extends CustomAsyncTask
  {
    public a(GBActivity arg2)
    {
      super();
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      try
      {
        MemberLogin.a(MemberLogin.this).dismiss();
        label15: if ((!isCancelled()) && (paramBoolean.booleanValue()))
        {
          MemberLogin localMemberLogin = MemberLogin.this;
          MemberLogin.b(MemberLogin.this);
          MemberLogin.c(MemberLogin.this);
          MemberLogin.d(localMemberLogin);
          MemberLogin.this.setResult(-1);
          MemberLogin.this.finish();
        }
        return;
      }
      catch (Exception localException)
      {
        break label15;
      }
    }

    protected final boolean queryWebService()
    {
      return MemberLogin.e(MemberLogin.this);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.members.MemberLogin
 * JD-Core Version:    0.6.2
 */