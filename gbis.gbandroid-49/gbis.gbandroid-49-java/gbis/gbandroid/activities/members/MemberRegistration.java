package gbis.gbandroid.activities.members;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import gbis.gbandroid.activities.base.GBActivity;
import gbis.gbandroid.activities.base.GBActivityDialog;
import gbis.gbandroid.entities.RegisterMessage;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.queries.MemberRegisterQuery;
import gbis.gbandroid.queries.MemberValidateQuery;
import gbis.gbandroid.utils.CustomAsyncTask;
import gbis.gbandroid.utils.VerifyFields;
import gbis.gbandroid.views.CustomDialog;
import gbis.gbandroid.views.CustomDialog.Builder;
import java.lang.reflect.Type;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MemberRegistration extends GBActivityDialog
  implements View.OnClickListener
{
  private RegisterMessage a;
  private EditText b;
  private EditText c;
  private EditText d;
  private EditText e;

  private void a()
  {
    this.b = ((EditText)findViewById(2131165292));
    this.c = ((EditText)findViewById(2131165296));
    this.d = ((EditText)findViewById(2131165300));
    this.e = ((EditText)findViewById(2131165298));
    addToEditTextList(this.b);
    addToEditTextList(this.c);
    addToEditTextList(this.d);
    addToEditTextList(this.e);
    b();
    this.b.requestFocus();
  }

  private void a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    Type localType = new n(this).getType();
    this.mResponseObject = new MemberRegisterQuery(this, this.mPrefs, localType, this.myLocation).getResponseObject(paramString1, paramString2, paramString3, paramString4);
    this.a = ((RegisterMessage)this.mResponseObject.getPayload());
  }

  private void a(boolean paramBoolean1, boolean paramBoolean2)
  {
    ImageView localImageView = (ImageView)findViewById(2131165295);
    if (paramBoolean1)
    {
      localImageView.setVisibility(0);
      if (paramBoolean2)
      {
        localImageView.setImageDrawable(this.mRes.getDrawable(2130837606));
        return;
      }
      localImageView.setImageDrawable(this.mRes.getDrawable(2130837607));
      return;
    }
    localImageView.setVisibility(4);
  }

  private void b()
  {
    this.b.setOnFocusChangeListener(new e(this));
    this.b.addTextChangedListener(new g(this));
    this.c.setOnFocusChangeListener(new h(this));
    this.d.setOnFocusChangeListener(new i(this));
    this.d.addTextChangedListener(new j(this));
    this.e.setOnFocusChangeListener(new k(this));
  }

  private void b(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    a locala = new a(this, paramString1, paramString2, paramString3, paramString4);
    locala.execute(new Object[0]);
    loadDialog(getString(2131296397), locala);
  }

  private void c()
  {
    String str1 = this.b.getText().toString();
    String str2 = this.c.getText().toString();
    String str3 = this.d.getText().toString();
    String str4 = this.e.getText().toString();
    if (!f(str1))
    {
      showMessage(getString(2131296420));
      return;
    }
    if (!e(str1))
    {
      showMessage(getString(2131296419));
      return;
    }
    if (!VerifyFields.checkEmail(str2))
    {
      showMessage(getString(2131296417));
      return;
    }
    if (!d(str3))
    {
      showMessage(getString(2131296422));
      return;
    }
    String str5 = VerifyFields.checkPostalCode(str4);
    if ((!VerifyFields.checkZip(str4)) && (str5.equals("")))
    {
      showMessage(getString(2131296423));
      return;
    }
    while (true)
    {
      try
      {
        if (!str5.equals(""))
        {
          b(str1, str2, str3, str5);
          return;
        }
      }
      catch (Exception localException)
      {
        return;
      }
      str5 = str4;
    }
  }

  private void d()
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this, this.mTracker);
    View localView = this.mInflater.inflate(2130903060, null);
    localBuilder.setTitle(getString(2131296546));
    localBuilder.setContentView(localView);
    ListView localListView = (ListView)localView.findViewById(2131165255);
    localListView.setChoiceMode(1);
    localListView.setAdapter(new ArrayAdapter(this, 2130903090, this.a.getMemberIdSuggestions()));
    localBuilder.setNegativeButton(2131296671, new l(this));
    CustomDialog localCustomDialog = localBuilder.create();
    localListView.setOnItemClickListener(new m(this, localListView, localCustomDialog));
    localCustomDialog.show();
  }

  private static boolean d(String paramString)
  {
    return (paramString.length() > 3) && (paramString.length() <= 15);
  }

  private boolean e()
  {
    Type localType = new f(this).getType();
    this.mResponseObject = new MemberValidateQuery(this, this.mPrefs, localType, this.myLocation).getResponseObject(this.b.getText().toString());
    return ((Boolean)this.mResponseObject.getPayload()).booleanValue();
  }

  private static boolean e(String paramString)
  {
    return Pattern.compile("[a-zA-Z_0-9]{3,}").matcher(paramString).matches();
  }

  private void f()
  {
    new b(this).execute(new Object[0]);
    ((ProgressBar)findViewById(2131165294)).setVisibility(0);
  }

  private static boolean f(String paramString)
  {
    return (paramString.length() > 3) && (paramString.length() <= 15);
  }

  private void g()
  {
    SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(getBaseContext()).edit();
    localEditor.putString("car", this.a.getCar());
    localEditor.putInt("car_icon_id", this.a.getCarIconId());
    localEditor.putString("member_id", this.a.getMemberId());
    localEditor.putBoolean("is_member", true);
    localEditor.commit();
  }

  public void onClick(View paramView)
  {
    if (paramView == getGBActivityDialogPositiveButton())
      c();
    while (paramView != getGBActivityDialogNegativeButton())
      return;
    finish();
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    setEditBoxHintConfiguration();
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    a();
  }

  protected void setAdsAfterGPSServiceConnected()
  {
  }

  protected String setAnalyticsPageName()
  {
    return getString(2131296824);
  }

  protected void setGBActivityDialogButtons()
  {
    setGBViewInContentLayout(2130903069, false);
    setGBActivityDialogTitle(getString(2131296524));
    setGBActivityDialogPositiveButton(2131296663, this);
    Bundle localBundle = getIntent().getExtras();
    if ((localBundle != null) && (localBundle.getBoolean("register show skip")))
      setGBActivityDialogNegativeButton(2131296670, this);
  }

  private final class a extends CustomAsyncTask
  {
    private String b;
    private String c;
    private String d;
    private String e;

    public a(GBActivity paramString1, String paramString2, String paramString3, String paramString4, String arg6)
    {
      super();
      this.b = paramString2;
      this.c = paramString3;
      this.d = paramString4;
      Object localObject;
      this.e = localObject;
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      try
      {
        MemberRegistration.a(MemberRegistration.this).dismiss();
        label15: if ((!isCancelled()) && (paramBoolean.booleanValue()))
        {
          if (!((RegisterMessage)MemberRegistration.b(MemberRegistration.this).getPayload()).isSignedIn())
            break label103;
          localMemberRegistration = MemberRegistration.this;
          MemberRegistration.c(localMemberRegistration);
          MemberRegistration.this.setResult(-1);
          setAnalyticsTrackEventScreenButton(MemberRegistration.d(MemberRegistration.this), MemberRegistration.this.getString(2131296858));
          MemberRegistration.this.finish();
        }
        label103: 
        while ((MemberRegistration.e(MemberRegistration.this).isSignedIn()) || (MemberRegistration.e(MemberRegistration.this).getMemberIdSuggestions().isEmpty()))
        {
          MemberRegistration localMemberRegistration;
          return;
        }
        MemberRegistration.f(MemberRegistration.this);
        return;
      }
      catch (Exception localException)
      {
        break label15;
      }
    }

    protected final boolean queryWebService()
    {
      MemberRegistration.a(MemberRegistration.this, this.b, this.c, this.d, this.e);
      return true;
    }
  }

  private final class b extends CustomAsyncTask
  {
    private boolean b;

    public b(GBActivity arg2)
    {
      super(false);
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      if (paramBoolean.booleanValue())
      {
        ((ProgressBar)MemberRegistration.this.findViewById(2131165294)).setVisibility(8);
        if (this.b)
          MemberRegistration.a(MemberRegistration.this, true, true);
      }
      else
      {
        return;
      }
      MemberRegistration.a(MemberRegistration.this, true, false);
    }

    protected final boolean queryWebService()
    {
      this.b = MemberRegistration.g(MemberRegistration.this);
      return true;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.members.MemberRegistration
 * JD-Core Version:    0.6.2
 */