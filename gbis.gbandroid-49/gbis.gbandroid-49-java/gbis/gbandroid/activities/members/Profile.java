package gbis.gbandroid.activities.members;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import gbis.gbandroid.activities.awards.Awards;
import gbis.gbandroid.activities.base.GBActivity;
import gbis.gbandroid.activities.base.GBActivityAds;
import gbis.gbandroid.content.GBIntent;
import gbis.gbandroid.entities.ProfileMessage;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.listeners.ScrollViewListener;
import gbis.gbandroid.queries.MemberProfileQuery;
import gbis.gbandroid.utils.CustomAsyncTask;
import gbis.gbandroid.utils.DateUtils;
import gbis.gbandroid.utils.ImageLoader;
import gbis.gbandroid.utils.ImageUtils;
import gbis.gbandroid.views.layouts.CustomScrollView;
import java.lang.reflect.Type;
import java.text.DecimalFormat;

public class Profile extends GBActivityAds
  implements View.OnClickListener, ScrollViewListener
{
  private String a;
  private String b;
  private ProfileMessage c;
  private View d;
  private View e;
  private View f;

  private static String a(int paramInt)
  {
    return new DecimalFormat("###,###,###").format(paramInt);
  }

  private void a()
  {
    this.d = findViewById(2131165503);
    this.d.setOnClickListener(this);
    this.e = findViewById(2131165499);
    this.e.setOnClickListener(this);
    this.f = findViewById(2131165501);
    this.f.setOnClickListener(this);
  }

  private void b()
  {
    TextView localTextView1 = (TextView)findViewById(2131165477);
    ImageView localImageView1 = (ImageView)findViewById(2131165476);
    ImageView localImageView2 = (ImageView)findViewById(2131165478);
    TextView localTextView2 = (TextView)findViewById(2131165480);
    TextView localTextView3 = (TextView)findViewById(2131165482);
    TextView localTextView4 = (TextView)findViewById(2131165484);
    TextView localTextView5 = (TextView)findViewById(2131165491);
    TextView localTextView6 = (TextView)findViewById(2131165493);
    TextView localTextView7 = (TextView)findViewById(2131165495);
    TextView localTextView8 = (TextView)findViewById(2131165497);
    TextView localTextView9 = (TextView)findViewById(2131165489);
    TextView localTextView10 = (TextView)findViewById(2131165487);
    localTextView1.setVisibility(0);
    localImageView1.setVisibility(0);
    localTextView2.setVisibility(0);
    localTextView3.setVisibility(0);
    localTextView4.setVisibility(0);
    localTextView1.setText(this.b);
    localTextView2.setText(this.c.getSiteName());
    try
    {
      localTextView3.setText(DateUtils.toString(DateUtils.toDateFormat(this.c.getJoinDate())));
      localTextView4.setText(DateUtils.getAge(DateUtils.toDateFormat(this.c.getJoinDate())));
      localTextView5.setText(a(this.c.getTotalPoints()));
      localTextView6.setText(a(this.c.getOverallRank()));
      localTextView7.setText(a(this.c.getOverallRank30Days()));
      localTextView8.setText(String.valueOf(this.c.getConsecutiveDays()));
      localTextView9.setText(a(this.c.getPointsToday()));
      localTextView10.setText(a(this.c.getPointBalance()));
      localImageView2.setVisibility(0);
      if (this.c.getPicturePath() != null)
        d();
      Drawable localDrawable = ImageUtils.getCarFromName(this.mRes, this.a);
      if (localDrawable != null)
        localImageView1.setImageDrawable(localDrawable);
      return;
    }
    catch (Exception localException)
    {
      while (true)
      {
        showMessage(getString(2131296455));
        finish();
      }
    }
  }

  private void c()
  {
    Type localType = new o(this).getType();
    this.mResponseObject = new MemberProfileQuery(this, this.mPrefs, localType, this.myLocation).getResponseObject();
    this.c = ((ProfileMessage)this.mResponseObject.getPayload());
  }

  private void d()
  {
    ImageView localImageView = (ImageView)findViewById(2131165478);
    localImageView.setTag(String.valueOf(this.c.getPicturePath().hashCode()));
    new ImageLoader(this, 2130837656).displayImage(this.c.getPicturePath(), this, localImageView, false);
  }

  private void e()
  {
    if (!this.a.equals(this.c.getCar()))
    {
      SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(getBaseContext()).edit();
      localEditor.putString("car", this.c.getCar());
      localEditor.putInt("car_icon_id", this.c.getCarIconId());
      localEditor.commit();
      this.a = this.c.getCar();
    }
  }

  private void f()
  {
    this.a = this.mPrefs.getString("car", "");
    this.b = this.mPrefs.getString("member_id", "");
  }

  private void g()
  {
    showMessage(getString(2131296383));
    MemberLogin.logout(this);
  }

  private void h()
  {
    GBIntent localGBIntent = new GBIntent(this, ProfilePhotoUploader.class, this.myLocation);
    localGBIntent.putExtra("title", getString(2131296442));
    startActivity(localGBIntent);
  }

  private void i()
  {
    startActivity(new GBIntent(this, Awards.class, this.myLocation));
  }

  public void onClick(View paramView)
  {
    String str = "";
    if (paramView == this.d)
    {
      str = getString(2131296652);
      g();
      finish();
    }
    while (true)
    {
      setAnalyticsTrackEventScreenButton(str);
      return;
      if (paramView == this.e)
      {
        str = getString(2131296646);
        h();
      }
      else if (paramView == this.f)
      {
        str = getString(2131296653);
        i();
      }
    }
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903119);
    f();
    try
    {
      a locala = new a(this);
      locala.execute(new Object[0]);
      loadDialog(getString(2131296397), locala);
      return;
    }
    catch (Exception localException)
    {
      setResult(9);
      finish();
    }
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    super.onCreateOptionsMenu(paramMenu);
    getMenuInflater().inflate(2131427334, paramMenu);
    return super.onCreateOptionsMenu(paramMenu);
  }

  protected void onDestroy()
  {
    super.onDestroy();
    cleanImageCache();
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    super.onOptionsItemSelected(paramMenuItem);
    switch (paramMenuItem.getItemId())
    {
    default:
      return super.onOptionsItemSelected(paramMenuItem);
    case 2131165597:
      g();
      finish();
      return true;
    case 2131165596:
    }
    h();
    return true;
  }

  public void onResume()
  {
    super.onResume();
  }

  public void onScrollChanged(CustomScrollView paramCustomScrollView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if (paramInt2 != paramInt4)
      hideAds();
  }

  protected void setAdsAfterGPSServiceConnected()
  {
    int i = this.mPrefs.getInt("adProfile", 0);
    setAdConfiguration(this.mPrefs.getString("adProfileKey", ""), this.mPrefs.getString("adProfileUnit", ""), i);
  }

  protected String setAnalyticsPageName()
  {
    return getString(2131296826);
  }

  private final class a extends CustomAsyncTask
  {
    public a(GBActivity arg2)
    {
      super();
    }

    protected final void onCancelled()
    {
      super.onCancelled();
      Profile.this.finish();
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      try
      {
        Profile.a(Profile.this).dismiss();
        label15: if (paramBoolean.booleanValue())
        {
          Profile.b(Profile.this);
          Profile.c(Profile.this);
          Profile.d(Profile.this);
          return;
        }
        Profile.this.finish();
        return;
      }
      catch (Exception localException)
      {
        break label15;
      }
    }

    protected final boolean queryWebService()
    {
      Profile.e(Profile.this);
      return true;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.members.Profile
 * JD-Core Version:    0.6.2
 */