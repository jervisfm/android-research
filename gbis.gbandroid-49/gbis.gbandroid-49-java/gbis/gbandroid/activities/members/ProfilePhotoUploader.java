package gbis.gbandroid.activities.members;

import android.content.Intent;
import android.os.Bundle;
import gbis.gbandroid.activities.base.PhotoDialog;
import gbis.gbandroid.queries.ProfilePhotoUploaderQuery;
import gbis.gbandroid.utils.Base64;
import java.lang.reflect.Type;

public class ProfilePhotoUploader extends PhotoDialog
{
  protected void getIntentExtras()
  {
    this.title = getIntent().getExtras().getString("title");
  }

  protected void postPhotoResized()
  {
    uploadPhoto();
  }

  protected void postPhotoTaken()
  {
  }

  protected void queryPhotoUploaderWebService(byte[] paramArrayOfByte)
  {
    Type localType = new p(this).getType();
    this.mResponseObject = new ProfilePhotoUploaderQuery(this, this.mPrefs, localType, this.myLocation).getResponseObject(Base64.encodeBytes(paramArrayOfByte));
  }

  protected void setAdsAfterGPSServiceConnected()
  {
  }

  protected String setAnalyticsPageName()
  {
    return getString(2131296827);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.members.ProfilePhotoUploader
 * JD-Core Version:    0.6.2
 */