package gbis.gbandroid.activities.members;

import android.app.Dialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import gbis.gbandroid.activities.base.GBActivity;
import gbis.gbandroid.activities.base.GBActivityDialog;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.queries.ResetPasswordQuery;
import gbis.gbandroid.utils.CustomAsyncTask;
import gbis.gbandroid.utils.VerifyFields;
import java.lang.reflect.Type;

public class ResetPassword extends GBActivityDialog
  implements View.OnClickListener
{
  private EditText a;

  private void a()
  {
    TextView localTextView1 = (TextView)findViewById(2131165246);
    this.a = ((EditText)findViewById(2131165247));
    this.a.setInputType(33);
    localTextView1.setText(getString(2131296564));
    TextView localTextView2 = new TextView(this);
    localTextView2.setText(getString(2131296591));
    localTextView2.setTextColor(this.mRes.getColor(2131230745));
    localTextView2.setGravity(17);
    localTextView2.setPadding(0, 0, 0, (int)(10.0F * getDensity()));
    ((LinearLayout)localTextView1.getParent()).addView(localTextView2, 0);
  }

  private void a(String paramString)
  {
    Type localType = new q(this).getType();
    this.mResponseObject = new ResetPasswordQuery(this, this.mPrefs, localType, this.myLocation).getResponseObject(paramString);
  }

  private void b(String paramString)
  {
    a locala = new a(this, paramString);
    locala.execute(new Object[0]);
    loadDialog(getString(2131296397), locala);
  }

  public void onClick(View paramView)
  {
    if (paramView == getGBActivityDialogPositiveButton())
    {
      String str = this.a.getText().toString();
      if (VerifyFields.checkEmail(str))
        b(str);
    }
    else
    {
      return;
    }
    showMessage(getString(2131296417));
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    a();
  }

  protected void setAdsAfterGPSServiceConnected()
  {
  }

  protected String setAnalyticsPageName()
  {
    return getString(2131296825);
  }

  protected void setGBActivityDialogButtons()
  {
    setGBViewInContentLayout(2130903057, false);
    setGBActivityDialogTitle(getString(2131296553));
    setGBActivityDialogPositiveButton(2131296634, this);
  }

  private final class a extends CustomAsyncTask
  {
    private String b;

    public a(GBActivity paramString, String arg3)
    {
      super();
      Object localObject;
      this.b = localObject;
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      try
      {
        ResetPassword.a(ResetPassword.this).dismiss();
        label15: if ((ResetPassword.b(ResetPassword.this) != null) && (((Boolean)ResetPassword.b(ResetPassword.this).getPayload()).booleanValue()))
          ResetPassword.this.finish();
        return;
      }
      catch (Exception localException)
      {
        break label15;
      }
    }

    protected final boolean queryWebService()
    {
      ResetPassword.a(ResetPassword.this, this.b);
      return true;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.members.ResetPassword
 * JD-Core Version:    0.6.2
 */