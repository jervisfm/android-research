package gbis.gbandroid.activities.members;

import android.text.Editable;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.widget.EditText;

final class e
  implements View.OnFocusChangeListener
{
  e(MemberRegistration paramMemberRegistration)
  {
  }

  public final void onFocusChange(View paramView, boolean paramBoolean)
  {
    if (paramBoolean)
      this.a.getWindow().setSoftInputMode(5);
    while (MemberRegistration.h(this.a).getText().toString().length() <= 0)
      return;
    if (!MemberRegistration.a(MemberRegistration.h(this.a).getText().toString()))
    {
      this.a.showMessage(this.a.getString(2131296420));
      MemberRegistration.a(this.a, true, false);
      return;
    }
    if (MemberRegistration.b(MemberRegistration.h(this.a).getText().toString()))
    {
      MemberRegistration.a(this.a, false, false);
      MemberRegistration.i(this.a);
      return;
    }
    this.a.showMessage(this.a.getString(2131296419));
    MemberRegistration.a(this.a, true, false);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.members.e
 * JD-Core Version:    0.6.2
 */