package gbis.gbandroid.activities.members;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

final class g
  implements TextWatcher
{
  g(MemberRegistration paramMemberRegistration)
  {
  }

  public final void afterTextChanged(Editable paramEditable)
  {
    if (MemberRegistration.h(this.a).getText().toString().length() == 15)
      MemberRegistration.j(this.a).requestFocus();
  }

  public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
  }

  public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.members.g
 * JD-Core Version:    0.6.2
 */