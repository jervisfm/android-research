package gbis.gbandroid.activities.members;

import android.content.res.Resources;
import android.text.Editable;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import gbis.gbandroid.utils.VerifyFields;

final class h
  implements View.OnFocusChangeListener
{
  h(MemberRegistration paramMemberRegistration)
  {
  }

  public final void onFocusChange(View paramView, boolean paramBoolean)
  {
    ImageView localImageView;
    if ((!paramBoolean) && (MemberRegistration.j(this.a).getText().toString().length() > 0))
    {
      localImageView = (ImageView)this.a.findViewById(2131165297);
      if (!VerifyFields.checkEmail(MemberRegistration.j(this.a).getText().toString()))
        break label81;
      localImageView.setImageDrawable(MemberRegistration.k(this.a).getDrawable(2130837606));
    }
    while (true)
    {
      localImageView.setVisibility(0);
      return;
      label81: localImageView.setImageDrawable(MemberRegistration.k(this.a).getDrawable(2130837607));
      this.a.showMessage(this.a.getString(2131296417));
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.members.h
 * JD-Core Version:    0.6.2
 */