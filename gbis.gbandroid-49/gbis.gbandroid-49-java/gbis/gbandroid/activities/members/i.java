package gbis.gbandroid.activities.members;

import android.content.res.Resources;
import android.text.Editable;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.ImageView;

final class i
  implements View.OnFocusChangeListener
{
  i(MemberRegistration paramMemberRegistration)
  {
  }

  public final void onFocusChange(View paramView, boolean paramBoolean)
  {
    ImageView localImageView;
    if ((!paramBoolean) && (MemberRegistration.l(this.a).getText().toString().length() > 0))
    {
      localImageView = (ImageView)this.a.findViewById(2131165301);
      if (!MemberRegistration.c(MemberRegistration.l(this.a).getText().toString()))
        break label86;
      localImageView.setImageDrawable(MemberRegistration.k(this.a).getDrawable(2130837606));
    }
    while (true)
    {
      localImageView.setVisibility(0);
      return;
      label86: localImageView.setImageDrawable(MemberRegistration.k(this.a).getDrawable(2130837607));
      this.a.showMessage(this.a.getString(2131296422));
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.members.i
 * JD-Core Version:    0.6.2
 */