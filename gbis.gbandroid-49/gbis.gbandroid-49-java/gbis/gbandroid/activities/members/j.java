package gbis.gbandroid.activities.members;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;

final class j
  implements TextWatcher
{
  j(MemberRegistration paramMemberRegistration)
  {
  }

  public final void afterTextChanged(Editable paramEditable)
  {
    if (MemberRegistration.l(this.a).getText().toString().length() == 15)
      MemberRegistration.m(this.a).requestFocusFromTouch();
  }

  public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
  }

  public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.members.j
 * JD-Core Version:    0.6.2
 */