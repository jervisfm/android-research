package gbis.gbandroid.activities.members;

import android.content.res.Resources;
import android.text.Editable;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import gbis.gbandroid.utils.VerifyFields;

final class k
  implements View.OnFocusChangeListener
{
  k(MemberRegistration paramMemberRegistration)
  {
  }

  public final void onFocusChange(View paramView, boolean paramBoolean)
  {
    ImageView localImageView;
    if ((!paramBoolean) && (MemberRegistration.n(this.a).getText().toString().length() > 0))
    {
      localImageView = (ImageView)this.a.findViewById(2131165299);
      String str = MemberRegistration.n(this.a).getText().toString();
      if ((VerifyFields.checkZip(str)) || (!VerifyFields.checkPostalCode(str).equals("")))
        break label114;
      localImageView.setImageDrawable(MemberRegistration.k(this.a).getDrawable(2130837607));
      this.a.showMessage(this.a.getString(2131296423));
    }
    while (true)
    {
      localImageView.setVisibility(0);
      return;
      label114: localImageView.setImageDrawable(MemberRegistration.k(this.a).getDrawable(2130837606));
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.members.k
 * JD-Core Version:    0.6.2
 */