package gbis.gbandroid.activities.members;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ListView;
import gbis.gbandroid.entities.RegisterMessage;
import gbis.gbandroid.views.CustomDialog;
import java.util.List;

final class m
  implements AdapterView.OnItemClickListener
{
  m(MemberRegistration paramMemberRegistration, ListView paramListView, CustomDialog paramCustomDialog)
  {
  }

  public final void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    ((CheckedTextView)paramView).setChecked(true);
    MemberRegistration.h(this.a).setText((CharSequence)MemberRegistration.e(this.a).getMemberIdSuggestions().get(this.b.getCheckedItemPosition()));
    MemberRegistration.h(this.a).requestFocus();
    MemberRegistration.a(this.a, true, true);
    this.c.dismiss();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.members.m
 * JD-Core Version:    0.6.2
 */