package gbis.gbandroid.activities.prizes;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import gbis.gbandroid.activities.base.GBActivity;
import gbis.gbandroid.content.GBIntent;
import gbis.gbandroid.entities.PrizeMemberMessage;
import gbis.gbandroid.entities.PrizeResults;
import gbis.gbandroid.entities.PrizeStringsMessage;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.TicketMessage;
import gbis.gbandroid.queries.PrizeQuery;
import gbis.gbandroid.queries.TicketsQuery;
import gbis.gbandroid.utils.CustomAsyncTask;
import gbis.gbandroid.utils.VerifyFields;
import gbis.gbandroid.views.CustomDialog.Builder;
import java.lang.reflect.Type;

public class Prize extends GBActivity
{
  private PrizeMemberMessage a;
  private PrizeStringsMessage b;
  private TicketMessage c;
  private int d;
  private EditText e;
  private EditText f;
  private EditText g;
  private EditText h;
  private EditText i;
  private EditText j;
  private EditText k;
  private Spinner l;
  private Dialog m;
  private Dialog n;
  private Dialog o;

  private String a(int paramInt)
  {
    return getResources().getStringArray(2131099666)[paramInt];
  }

  private void a()
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this);
    View localView = this.mInflater.inflate(2130903065, null);
    TextView localTextView1 = (TextView)localView.findViewById(2131165266);
    TextView localTextView2 = (TextView)localView.findViewById(2131165267);
    TextView localTextView3 = (TextView)localView.findViewById(2131165270);
    EditText localEditText = (EditText)localView.findViewById(2131165269);
    localBuilder.setTitle(getString(2131296535));
    localBuilder.setContentView(localView);
    localTextView1.setText(this.b.getTitle());
    localTextView2.setText(this.b.getDate());
    localTextView3.setText(this.b.getLimit());
    localBuilder.setPositiveButton(getString(2131296672), new a(this, localEditText));
    localBuilder.setNegativeButton(getString(2131296673), new l(this));
    this.n = localBuilder.create();
    this.n.setOnCancelListener(new q(this));
    this.n.setOnKeyListener(new r(this, localEditText));
    this.n.show();
    if (this.a.getTicketsAvailable() == 0)
    {
      localEditText.setVisibility(8);
      ((TextView)localView.findViewById(2131165268)).setVisibility(8);
      this.n.findViewById(2131165237).setVisibility(8);
    }
    localEditText.setOnEditorActionListener(new s(this));
    this.n.setOnCancelListener(new t(this));
    localEditText.addTextChangedListener(new u(this));
  }

  private void a(View paramView)
  {
    this.m.setOnKeyListener(new f(this));
    this.m.setOnCancelListener(new g(this));
    this.e.setOnFocusChangeListener(new h(this, paramView));
    this.f.setOnFocusChangeListener(new i(this, paramView));
    this.g.setOnFocusChangeListener(new j(this, paramView));
    this.i.setOnFocusChangeListener(new k(this, paramView));
    this.i.setNextFocusDownId(this.l.getId());
    this.l.setOnFocusChangeListener(new m(this, paramView));
    this.j.setOnFocusChangeListener(new n(this, paramView));
    this.k.setOnFocusChangeListener(new o(this, paramView));
    this.k.setNextFocusDownId(2131165237);
    this.k.setOnEditorActionListener(new p(this));
  }

  private void a(String paramString)
  {
    if ((!paramString.equals("")) && (Integer.parseInt(paramString) != 0))
    {
      if (Integer.parseInt(paramString) <= this.a.getTicketsAvailable())
      {
        this.d = Integer.parseInt(paramString);
        this.n.dismiss();
        h();
        return;
      }
      showMessage(getString(2131296384));
      return;
    }
    showMessage(getString(2131296386));
  }

  private void b()
  {
    Type localType = new v(this).getType();
    this.mResponseObject = new PrizeQuery(this, this.mPrefs, localType, this.myLocation).getResponseObject();
    PrizeResults localPrizeResults = (PrizeResults)this.mResponseObject.getPayload();
    this.a = localPrizeResults.getPrizeMemberMessage();
    this.b = localPrizeResults.getPrizeStringsMessage();
  }

  private void c()
  {
    Type localType = new w(this).getType();
    this.mResponseObject = new TicketsQuery(this, this.mPrefs, localType, this.myLocation).getResponseObject(this.d, d());
    this.c = ((TicketMessage)this.mResponseObject.getPayload());
  }

  private PrizeMemberMessage d()
  {
    PrizeMemberMessage localPrizeMemberMessage = new PrizeMemberMessage();
    localPrizeMemberMessage.setFirstName(this.e.getText().toString());
    localPrizeMemberMessage.setLastName(this.f.getText().toString());
    localPrizeMemberMessage.setAddress1(this.g.getText().toString());
    localPrizeMemberMessage.setAddress2(this.h.getText().toString());
    localPrizeMemberMessage.setCity(this.i.getText().toString());
    localPrizeMemberMessage.setState(a(this.l.getSelectedItemPosition()));
    localPrizeMemberMessage.setPostalCode(this.j.getText().toString());
    localPrizeMemberMessage.setEmail(this.k.getText().toString());
    return localPrizeMemberMessage;
  }

  private void e()
  {
    a locala = new a(this);
    locala.execute(new Object[0]);
    loadDialog(getString(2131296398), locala);
  }

  private void f()
  {
    new b(this).execute(new Object[0]);
    loadDialog(getString(2131296400));
  }

  private void g()
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this, this.mTracker);
    View localView = this.mInflater.inflate(2130903067, null);
    localBuilder.setTitle(getString(2131296536));
    localBuilder.setContentView(localView);
    TextView localTextView1 = (TextView)localView.findViewById(2131165287);
    TextView localTextView2 = (TextView)localView.findViewById(2131165289);
    localTextView1.setText(this.b.getTotalPoints());
    localTextView2.setText(this.b.getExplanation());
    localBuilder.setPositiveButton(getString(2131296672), new b(this));
    localBuilder.setNeutralButton(getString(2131296674), new c(this));
    this.o = localBuilder.create();
    this.o.setOnCancelListener(new d(this));
    this.o.show();
  }

  private void h()
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this, this.mTracker);
    View localView = this.mInflater.inflate(2130903066, null);
    localBuilder.setTitle(getString(2131296535));
    localBuilder.setContentView(localView);
    this.e = ((EditText)localView.findViewById(2131165272));
    this.f = ((EditText)localView.findViewById(2131165274));
    this.g = ((EditText)localView.findViewById(2131165276));
    this.h = ((EditText)localView.findViewById(2131165278));
    this.i = ((EditText)localView.findViewById(2131165279));
    this.j = ((EditText)localView.findViewById(2131165283));
    this.k = ((EditText)localView.findViewById(2131165285));
    this.l = ((Spinner)localView.findViewById(2131165281));
    addToEditTextList(this.e);
    addToEditTextList(this.f);
    addToEditTextList(this.g);
    addToEditTextList(this.h);
    addToEditTextList(this.i);
    addToEditTextList(this.k);
    addToEditTextList(this.j);
    TextView localTextView = (TextView)localView.findViewById(2131165271);
    String str = getString(2131296588);
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Integer.valueOf(this.d);
    localTextView.setText(String.format(str, arrayOfObject));
    ArrayAdapter localArrayAdapter = new ArrayAdapter(this, 2130903091, getResources().getStringArray(2131099665));
    localArrayAdapter.setDropDownViewResource(2130903090);
    this.l.setAdapter(localArrayAdapter);
    localBuilder.setPositiveButton(getString(2131296672), new e(this, localBuilder));
    this.m = localBuilder.create();
    a(localView);
    j();
    this.m.show();
  }

  private void i()
  {
    startActivity(new GBIntent(this, WinnersList.class, this.myLocation));
  }

  private void j()
  {
    if (!this.a.getFirstName().equals(""))
      this.e.setText(this.a.getFirstName());
    if (!this.a.getLastName().equals(""))
      this.f.setText(this.a.getLastName());
    if (!this.a.getAddress1().equals(""))
      this.g.setText(this.a.getAddress1());
    if (!this.a.getAddress2().equals(""))
      this.h.setText(this.a.getAddress2());
    if (!this.a.getCity().equals(""))
      this.i.setText(this.a.getCity());
    if (!this.a.getState().equals(""))
      this.l.setSelection(k());
    if (!this.a.getPostalCode().equals(""))
      this.j.setText(this.a.getPostalCode());
    if (!this.a.getEmail().equals(""))
      this.k.setText(this.a.getEmail());
  }

  private int k()
  {
    boolean bool = this.a.getState().equals("");
    int i1 = 0;
    String[] arrayOfString;
    int i2;
    int i3;
    if (!bool)
    {
      arrayOfString = getResources().getStringArray(2131099666);
      i2 = 0;
      i3 = 0;
    }
    while (true)
    {
      if ((i3 >= arrayOfString.length) || (i2 != 0))
      {
        i1 = 0;
        if (i2 != 0)
          i1 = i3;
        return i1;
      }
      if (arrayOfString[i3].equals(this.a.getState()))
        i2 = 1;
      else
        i3++;
    }
  }

  private boolean l()
  {
    String str1 = this.e.getText().toString();
    String str2 = this.f.getText().toString();
    String str3 = this.g.getText().toString();
    String str4 = this.i.getText().toString();
    String str5 = this.j.getText().toString();
    String str6 = this.k.getText().toString();
    String str7 = (String)this.l.getSelectedItem();
    if (str1.length() <= 0)
      showMessage(getString(2131296433));
    while (true)
    {
      return false;
      if (str2.length() <= 0)
      {
        showMessage(getString(2131296434));
      }
      else if (str3.length() <= 0)
      {
        showMessage(getString(2131296435));
      }
      else if (str4.length() <= 0)
      {
        showMessage(getString(2131296436));
      }
      else if (str7.equals(""))
      {
        showMessage(getString(2131296437));
      }
      else
      {
        String str8 = VerifyFields.checkPostalCode(str5);
        if ((!VerifyFields.checkZip(str5)) && (str8.equals("")))
        {
          showMessage(getString(2131296423));
        }
        else
        {
          if (VerifyFields.checkEmail(str6))
            break;
          showMessage(getString(2131296417));
        }
      }
    }
    f();
    return true;
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    setEditBoxHintConfiguration();
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Object[] arrayOfObject = (Object[])getLastNonConfigurationInstance();
    if (arrayOfObject != null)
    {
      if (((Integer)arrayOfObject[0]).intValue() == 0)
      {
        this.m = ((Dialog)arrayOfObject[1]);
        this.m.show();
      }
      do
      {
        return;
        if (((Integer)arrayOfObject[0]).intValue() == 1)
        {
          this.n = ((Dialog)arrayOfObject[1]);
          this.n.show();
          return;
        }
      }
      while (((Integer)arrayOfObject[0]).intValue() != 2);
      this.o = ((Dialog)arrayOfObject[1]);
      this.o.show();
      return;
    }
    e();
  }

  public Object onRetainNonConfigurationInstance()
  {
    Object[] arrayOfObject = new Object[2];
    if ((this.m != null) && (this.m.isShowing()))
    {
      arrayOfObject[0] = Integer.valueOf(0);
      arrayOfObject[1] = this.m;
      this.m.dismiss();
      return arrayOfObject;
    }
    if ((this.n != null) && (this.n.isShowing()))
    {
      arrayOfObject[0] = Integer.valueOf(1);
      arrayOfObject[1] = this.n;
      this.n.dismiss();
      return arrayOfObject;
    }
    if ((this.o != null) && (this.o.isShowing()))
    {
      arrayOfObject[0] = Integer.valueOf(2);
      arrayOfObject[1] = this.o;
      this.o.dismiss();
      return arrayOfObject;
    }
    return null;
  }

  protected void setAdsAfterGPSServiceConnected()
  {
  }

  protected String setAnalyticsPageName()
  {
    return getString(2131296830);
  }

  private final class a extends CustomAsyncTask
  {
    public a(GBActivity arg2)
    {
      super();
    }

    protected final void onCancelled()
    {
      super.onCancelled();
      Prize.this.finish();
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      try
      {
        Prize.a(Prize.this).dismiss();
        label15: if (paramBoolean.booleanValue())
        {
          Prize.b(Prize.this);
          return;
        }
        Prize.this.finish();
        return;
      }
      catch (Exception localException)
      {
        break label15;
      }
    }

    protected final boolean queryWebService()
    {
      Prize.c(Prize.this);
      return true;
    }
  }

  private final class b extends CustomAsyncTask
  {
    public b(GBActivity arg2)
    {
      super();
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      try
      {
        Prize.a(Prize.this).dismiss();
        label15: if ((paramBoolean.booleanValue()) && (Prize.d(Prize.this).getTotalTickets() != 0) && (Prize.d(Prize.this).getTickets() != 0))
          Prize.this.finish();
        return;
      }
      catch (Exception localException)
      {
        break label15;
      }
    }

    protected final boolean queryWebService()
    {
      Prize.e(Prize.this);
      return true;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.prizes.Prize
 * JD-Core Version:    0.6.2
 */