package gbis.gbandroid.activities.prizes;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import gbis.gbandroid.activities.base.GBActivity;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.WinnersMessage;
import gbis.gbandroid.queries.WinnersListQuery;
import gbis.gbandroid.utils.CustomAsyncTask;
import gbis.gbandroid.utils.DateUtils;
import gbis.gbandroid.utils.ImageLoader;
import gbis.gbandroid.utils.ImageUtils;
import java.lang.reflect.Type;
import java.util.List;

public class WinnersList extends GBActivity
{
  private List<WinnersMessage> a;

  private void a()
  {
    ((ListView)findViewById(2131165397)).setAdapter(new b(this, this.a));
  }

  private void b()
  {
    Type localType = new x(this).getType();
    this.mResponseObject = new WinnersListQuery(this, this.mPrefs, localType, this.myLocation).getResponseObject();
    this.a = ((List)this.mResponseObject.getPayload());
  }

  private void c()
  {
    c localc = new c(this);
    localc.execute(new Object[0]);
    loadDialog(getString(2131296399), localc);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903099);
    c();
    ((TextView)findViewById(2131165395)).setText(getString(2131296674));
  }

  protected void onDestroy()
  {
    super.onDestroy();
    cleanImageCache();
  }

  protected void setAdsAfterGPSServiceConnected()
  {
  }

  protected String setAnalyticsPageName()
  {
    return getString(2131296832);
  }

  static final class a
  {
    TextView a;
    TextView b;
    TextView c;
    TextView d;
    ImageView e;
    ImageView f;
  }

  private final class b extends ArrayAdapter<WinnersMessage>
  {
    private int b = 2130903109;
    private List<WinnersMessage> c;
    private ImageLoader d;

    public b(int arg2)
    {
      super(2130903109, localList);
      this.c = localList;
      this.d = new ImageLoader(WinnersList.this, 2130837610);
    }

    public final boolean areAllItemsEnabled()
    {
      return false;
    }

    public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      WinnersList.a locala1;
      if (paramView == null)
      {
        paramView = WinnersList.a(WinnersList.this).inflate(this.b, null);
        WinnersList.a locala2 = new WinnersList.a();
        locala2.a = ((TextView)paramView.findViewById(2131165431));
        locala2.b = ((TextView)paramView.findViewById(2131165428));
        locala2.c = ((TextView)paramView.findViewById(2131165430));
        locala2.d = ((TextView)paramView.findViewById(2131165432));
        locala2.f = ((ImageView)paramView.findViewById(2131165429));
        locala2.e = ((ImageView)paramView.findViewById(2131165427));
        paramView.setTag(locala2);
        locala1 = locala2;
      }
      while (true)
      {
        WinnersMessage localWinnersMessage = (WinnersMessage)this.c.get(paramInt);
        locala1.a.setText(DateUtils.getDateForWinners(DateUtils.toDateFormat(localWinnersMessage.getEndDate())));
        locala1.b.setText(localWinnersMessage.getMemberId());
        locala1.c.setText(localWinnersMessage.getCityTitle());
        locala1.d.setText(localWinnersMessage.getPrize());
        locala1.e.setTag(String.valueOf(localWinnersMessage.getImgUrl().hashCode()));
        Drawable localDrawable = ImageUtils.getCarFromName(WinnersList.b(WinnersList.this), localWinnersMessage.getCar());
        if (localDrawable != null)
          locala1.f.setImageDrawable(localDrawable);
        try
        {
          if (!localWinnersMessage.getImgUrl().equals(""))
          {
            this.d.displayImage(localWinnersMessage.getImgUrl(), WinnersList.this, locala1.e, false, 100);
            return paramView;
            locala1 = (WinnersList.a)paramView.getTag();
          }
          else
          {
            locala1.e.setImageResource(2130837610);
            return paramView;
          }
        }
        catch (Exception localException)
        {
          localException.printStackTrace();
        }
      }
      return paramView;
    }

    public final boolean isEnabled(int paramInt)
    {
      return false;
    }
  }

  private final class c extends CustomAsyncTask
  {
    public c(GBActivity arg2)
    {
      super();
    }

    protected final void onCancelled()
    {
      super.onCancelled();
      WinnersList.this.finish();
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      try
      {
        WinnersList.c(WinnersList.this).dismiss();
        label15: if (paramBoolean.booleanValue())
        {
          WinnersList.d(WinnersList.this);
          return;
        }
        WinnersList.this.finish();
        return;
      }
      catch (Exception localException)
      {
        break label15;
      }
    }

    protected final boolean queryWebService()
    {
      WinnersList.e(WinnersList.this);
      return true;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.prizes.WinnersList
 * JD-Core Version:    0.6.2
 */