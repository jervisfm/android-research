package gbis.gbandroid.activities.prizes;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.view.KeyEvent;
import android.widget.Button;

final class f
  implements DialogInterface.OnKeyListener
{
  f(Prize paramPrize)
  {
  }

  public final boolean onKey(DialogInterface paramDialogInterface, int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 66) && (paramKeyEvent.getAction() == 1))
    {
      Button localButton = (Button)Prize.k(this.a).findViewById(2131165237);
      if (localButton.hasFocus())
      {
        localButton.setEnabled(false);
        if (!Prize.j(this.a))
          localButton.setEnabled(true);
        return true;
      }
      return false;
    }
    return false;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.prizes.f
 * JD-Core Version:    0.6.2
 */