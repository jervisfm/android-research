package gbis.gbandroid.activities.prizes;

import android.content.res.Resources;
import android.text.Editable;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.ImageView;

final class i
  implements View.OnFocusChangeListener
{
  i(Prize paramPrize, View paramView)
  {
  }

  public final void onFocusChange(View paramView, boolean paramBoolean)
  {
    ImageView localImageView;
    if (!paramBoolean)
    {
      localImageView = (ImageView)this.b.findViewById(2131165275);
      if (Prize.m(this.a).getText().toString().length() <= 0)
        break label60;
      localImageView.setImageDrawable(this.a.getResources().getDrawable(2130837606));
    }
    while (true)
    {
      localImageView.setVisibility(0);
      return;
      label60: localImageView.setImageDrawable(this.a.getResources().getDrawable(2130837607));
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.prizes.i
 * JD-Core Version:    0.6.2
 */