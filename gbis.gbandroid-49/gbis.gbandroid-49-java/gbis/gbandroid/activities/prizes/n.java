package gbis.gbandroid.activities.prizes;

import android.content.res.Resources;
import android.text.Editable;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import gbis.gbandroid.utils.VerifyFields;

final class n
  implements View.OnFocusChangeListener
{
  n(Prize paramPrize, View paramView)
  {
  }

  public final void onFocusChange(View paramView, boolean paramBoolean)
  {
    ImageView localImageView;
    if (!paramBoolean)
    {
      localImageView = (ImageView)this.b.findViewById(2131165284);
      if (Prize.q(this.a).getText().toString().length() <= 0)
        break label133;
      String str = Prize.q(this.a).getText().toString();
      if ((VerifyFields.checkZip(str)) || (!VerifyFields.checkPostalCode(str).equals("")))
        break label114;
      localImageView.setImageDrawable(this.a.getResources().getDrawable(2130837607));
      this.a.showMessage(this.a.getString(2131296423));
    }
    while (true)
    {
      localImageView.setVisibility(0);
      return;
      label114: localImageView.setImageDrawable(this.a.getResources().getDrawable(2130837606));
    }
    label133: localImageView.setImageDrawable(this.a.getResources().getDrawable(2130837607));
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.prizes.n
 * JD-Core Version:    0.6.2
 */