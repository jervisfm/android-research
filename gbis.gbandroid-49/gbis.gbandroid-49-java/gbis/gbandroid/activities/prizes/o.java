package gbis.gbandroid.activities.prizes;

import android.content.res.Resources;
import android.text.Editable;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import gbis.gbandroid.utils.VerifyFields;

final class o
  implements View.OnFocusChangeListener
{
  o(Prize paramPrize, View paramView)
  {
  }

  public final void onFocusChange(View paramView, boolean paramBoolean)
  {
    ImageView localImageView;
    if (!paramBoolean)
    {
      localImageView = (ImageView)this.b.findViewById(2131165286);
      if (Prize.r(this.a).getText().toString().length() <= 0)
        break label116;
      if (!VerifyFields.checkEmail(Prize.r(this.a).getText().toString()))
        break label81;
      localImageView.setImageDrawable(this.a.getResources().getDrawable(2130837606));
    }
    while (true)
    {
      localImageView.setVisibility(0);
      return;
      label81: localImageView.setImageDrawable(this.a.getResources().getDrawable(2130837607));
      this.a.showMessage(this.a.getString(2131296417));
    }
    label116: localImageView.setImageDrawable(this.a.getResources().getDrawable(2130837607));
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.prizes.o
 * JD-Core Version:    0.6.2
 */