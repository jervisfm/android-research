package gbis.gbandroid.activities.prizes;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

final class r
  implements DialogInterface.OnKeyListener
{
  r(Prize paramPrize, EditText paramEditText)
  {
  }

  public final boolean onKey(DialogInterface paramDialogInterface, int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 66) && (paramKeyEvent.getAction() == 1))
    {
      if (Prize.g(this.a).findViewById(2131165237).hasFocus())
        Prize.a(this.a, this.b.getText().toString());
      while (!Prize.g(this.a).findViewById(2131165239).hasFocus())
        return false;
      Prize.f(this.a);
      Prize.g(this.a).dismiss();
      return true;
    }
    return false;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.prizes.r
 * JD-Core Version:    0.6.2
 */