package gbis.gbandroid.activities.prizes;

import android.text.Editable;
import android.text.TextWatcher;
import gbis.gbandroid.entities.PrizeMemberMessage;

final class u
  implements TextWatcher
{
  u(Prize paramPrize)
  {
  }

  public final void afterTextChanged(Editable paramEditable)
  {
    int i;
    if (paramEditable != null)
    {
      i = 1;
      if (!paramEditable.toString().equals(""))
        break label129;
    }
    label129: for (int j = 0; ; j = 1)
    {
      if (((i & j) != 0) && (Integer.parseInt(paramEditable.toString()) > Prize.h(this.a).getTicketsAvailable()))
      {
        paramEditable.delete(-1 + paramEditable.length(), paramEditable.length());
        Prize localPrize = this.a;
        String str = this.a.getString(2131296385);
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = Integer.valueOf(Prize.h(this.a).getTicketsAvailable());
        localPrize.showMessage(String.format(str, arrayOfObject));
      }
      return;
      i = 0;
      break;
    }
  }

  public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
  }

  public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.prizes.u
 * JD-Core Version:    0.6.2
 */