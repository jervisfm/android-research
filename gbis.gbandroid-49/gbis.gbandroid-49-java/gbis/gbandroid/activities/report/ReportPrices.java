package gbis.gbandroid.activities.report;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import gbis.gbandroid.activities.base.GBActivity;
import gbis.gbandroid.activities.base.GBActivityDialog;
import gbis.gbandroid.activities.report.pricekeyboard.PriceKeyboardDrawer;
import gbis.gbandroid.activities.report.pricekeyboard.ReportPriceKeyboardView;
import gbis.gbandroid.entities.FavReportMessage;
import gbis.gbandroid.entities.PriceValidation;
import gbis.gbandroid.entities.ReportMessage;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.SmartPrompt;
import gbis.gbandroid.entities.Station;
import gbis.gbandroid.entities.StationMessage;
import gbis.gbandroid.queries.ReportPricesFavoriteQuery;
import gbis.gbandroid.queries.ReportPricesQuery;
import gbis.gbandroid.queries.StationDetailsQuery;
import gbis.gbandroid.utils.CustomAsyncTask;
import gbis.gbandroid.utils.DateUtils;
import gbis.gbandroid.utils.VerifyFields;
import gbis.gbandroid.views.CustomDialog.Builder;
import gbis.gbandroid.views.CustomEditTextForPrices;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReportPrices extends GBActivityDialog
  implements View.OnFocusChangeListener
{
  public static final String BLANK_PRICE = "---";
  public static final Character DECIMAL;
  public static final String DECIMAL_STRING;
  private static boolean a;
  private static boolean b;
  private static final EnumMap<Currency, Integer> c;
  private LinearLayout A;
  private ViewGroup B;
  private boolean C = true;
  private EnumMap<FuelType, HashMap<String, Double>> D = null;
  private String d;
  private String e;
  private int f;
  private int g;
  private ReportMessage h;
  private FavReportMessage i;
  private Currency j;
  private String k;
  private StationMessage l;
  private Double m;
  private Double n;
  private Double o;
  private Double p;
  private int q;
  private CustomEditTextForPrices r;
  private CustomEditTextForPrices s;
  private CustomEditTextForPrices t;
  private CustomEditTextForPrices u;
  private EditText v;
  private Spinner w;
  private Integer x = Integer.valueOf(0);
  private PriceKeyboardDrawer y;
  private ReportPriceKeyboardView z;

  static
  {
    Character localCharacter = Character.valueOf('.');
    DECIMAL = localCharacter;
    DECIMAL_STRING = localCharacter.toString();
    a = true;
    b = false;
    EnumMap localEnumMap = new EnumMap(Currency.class);
    c = localEnumMap;
    localEnumMap.put(Currency.USA, Integer.valueOf(2));
    c.put(Currency.CAN, Integer.valueOf(1));
  }

  private Integer a(FuelType paramFuelType, Double paramDouble)
  {
    if ((this.D == null) || (this.D.size() == 0) || (paramDouble.doubleValue() == -1.0D) || (paramDouble == null) || (paramFuelType == null))
      return null;
    if (paramDouble.doubleValue() > ((Double)((HashMap)this.D.get(paramFuelType)).get("max")).doubleValue())
      return Integer.valueOf(1);
    if (paramDouble.doubleValue() < ((Double)((HashMap)this.D.get(paramFuelType)).get("min")).doubleValue())
      return Integer.valueOf(-1);
    return Integer.valueOf(0);
  }

  private Integer a(String paramString)
  {
    if (paramString.length() == 0);
    do
    {
      do
      {
        do
          return null;
        while (paramString.contains("---"));
        switch (q()[this.j.ordinal()])
        {
        default:
          return null;
        case 1:
        case 2:
        }
      }
      while (paramString.length() <= 0);
      return Integer.valueOf(1);
      if ((Integer.parseInt(paramString.substring(0, 1)) >= 8) && (paramString.length() >= 2))
        return Integer.valueOf(2);
    }
    while (paramString.length() < 3);
    return Integer.valueOf(3);
  }

  private String a(BigDecimal paramBigDecimal, int paramInt)
  {
    String str = "";
    if (paramInt > 0)
      str = paramBigDecimal + String.format(getString(2131296269), new Object[] { "high" }) + "\n";
    while (paramInt >= 0)
      return str;
    return paramBigDecimal + String.format(getString(2131296269), new Object[] { "low" }) + "\n";
  }

  private void a(double paramDouble1, String paramString1, double paramDouble2, String paramString2, double paramDouble3, String paramString3, double paramDouble4, String paramString4, int paramInt)
  {
    ViewGroup localViewGroup1 = (ViewGroup)findViewById(2131165305);
    ViewGroup localViewGroup2 = (ViewGroup)findViewById(2131165308);
    ViewGroup localViewGroup3 = (ViewGroup)findViewById(2131165311);
    ViewGroup localViewGroup4 = (ViewGroup)findViewById(2131165314);
    int i1;
    String str4;
    if (this.j == Currency.USA)
    {
      i1 = 2;
      if (paramDouble1 == 0.0D)
        break label380;
      if (paramString1.equals(""))
        break label338;
      str4 = DateUtils.compareDateToNow(DateUtils.toDateFormat(paramString1), paramInt);
      label83: a(localViewGroup1, paramDouble1, str4);
      localViewGroup1.setOnClickListener(new b(this, paramDouble1, i1));
    }
    label140: label199: label338: label352: label359: label366: label373: label380: for (int i2 = 1; ; i2 = 0)
    {
      String str3;
      String str2;
      String str1;
      if (paramDouble2 != 0.0D)
      {
        if (!paramString2.equals(""))
        {
          str3 = DateUtils.compareDateToNow(DateUtils.toDateFormat(paramString2), paramInt);
          a(localViewGroup2, paramDouble2, str3);
          localViewGroup2.setOnClickListener(new c(this, paramDouble2, i1));
          i2 = 1;
        }
      }
      else
      {
        if (paramDouble3 != 0.0D)
        {
          if (paramString3.equals(""))
            break label352;
          str2 = DateUtils.compareDateToNow(DateUtils.toDateFormat(paramString3), paramInt);
          a(localViewGroup3, paramDouble3, str2);
          localViewGroup3.setOnClickListener(new d(this, paramDouble3, i1));
          i2 = 1;
        }
        if (paramDouble4 == 0.0D)
          break label373;
        if (paramString4.equals(""))
          break label359;
        str1 = DateUtils.compareDateToNow(DateUtils.toDateFormat(paramString4), paramInt);
        a(localViewGroup4, paramDouble4, str1);
        localViewGroup4.setOnClickListener(new e(this, paramDouble4, i1));
      }
      for (int i3 = 1; ; i3 = i2)
      {
        TableRow localTableRow;
        if (this.mRes.getConfiguration().orientation == 2)
        {
          localTableRow = (TableRow)findViewById(2131165321);
          if (localTableRow != null)
          {
            if (i3 != 0)
              break label366;
            localTableRow.setVisibility(8);
          }
        }
        return;
        i1 = 1;
        break;
        str4 = "";
        break label83;
        str3 = "";
        break label140;
        str2 = "";
        break label199;
        str1 = "";
        break label258;
        localTableRow.setVisibility(0);
        return;
      }
    }
  }

  private void a(Intent paramIntent)
  {
    StringBuilder localStringBuilder;
    Iterator localIterator;
    String str2;
    Integer localInteger;
    if (b)
    {
      ArrayList localArrayList = paramIntent.getStringArrayListExtra("android.speech.extra.RESULTS");
      localStringBuilder = new StringBuilder();
      localIterator = localArrayList.iterator();
      if (!localIterator.hasNext())
        Toast.makeText(this, localStringBuilder.toString(), 1).show();
    }
    else
    {
      String str1 = (String)paramIntent.getStringArrayListExtra("android.speech.extra.RESULTS").get(0);
      Matcher localMatcher = Pattern.compile("([2-9])(0\\s)(\\d)").matcher(str1);
      if (localMatcher.find())
        str1 = new StringBuffer(str1).replace(localMatcher.start(), localMatcher.end(), "").insert(localMatcher.start(), localMatcher.group(1) + localMatcher.group(3)).toString();
      str2 = str1.replaceAll("\\D+", "");
      localInteger = a(str2);
      if ((localInteger == null) || (localInteger.intValue() == str2.length()))
        break label480;
      str3 = new StringBuilder(str2).insert(localInteger.intValue(), DECIMAL_STRING).toString();
      if (str3.substring(1 + localInteger.intValue()).length() <= ((Integer)c.get(this.j)).intValue());
    }
    label480: for (String str3 = str3.substring(0, 1 + localInteger.intValue() + ((Integer)c.get(this.j)).intValue()); ; str3 = str2)
    {
      if (str3.equals(""))
      {
        a(getString(2131296272), str3, false);
        return;
        localStringBuilder.append((String)localIterator.next()).append(", ");
        break;
      }
      if (str3.length() > 8)
      {
        a(getString(2131296272), str3, false);
        return;
      }
      if (d(str3.toString()).booleanValue())
      {
        a(getString(2131296273), str3, true);
        return;
      }
      Double localDouble = Double.valueOf(b(str3));
      if (localDouble.doubleValue() < 0.0D)
      {
        a(getString(2131296272), str3, false);
        return;
      }
      if (((this.z.getTarget() instanceof CustomEditTextForPrices)) && (a(((CustomEditTextForPrices)this.z.getTarget()).getFuelType(), localDouble).intValue() != 0))
      {
        a(getString(2131296273), str3, true);
        return;
      }
      this.z.getTarget().setText(str3);
      return;
    }
  }

  private void a(ViewGroup paramViewGroup, double paramDouble, String paramString)
  {
    if (this.j == Currency.USA)
      paramViewGroup.findViewById(2131165324).setVisibility(0);
    for (int i1 = 2; ; i1 = 1)
    {
      ((TextView)paramViewGroup.findViewById(2131165323)).setText(VerifyFields.doubleToScale(paramDouble, i1));
      if (paramString != "")
        ((TextView)paramViewGroup.findViewById(2131165325)).setText(paramString);
      paramViewGroup.setVisibility(0);
      if (this.mRes.getConfiguration().orientation == 2)
        paramViewGroup.findViewById(2131165326).setVisibility(4);
      return;
    }
  }

  private void a(PriceValidation paramPriceValidation)
  {
    this.D = new EnumMap(FuelType.class);
    double d1 = paramPriceValidation.getExpectedRegularPrice();
    double d2 = d1 + paramPriceValidation.getCommonIncreaseRegular();
    double d3 = d1 + paramPriceValidation.getCommonDecreaseRegular();
    HashMap localHashMap1 = new HashMap();
    localHashMap1.put("min", Double.valueOf(d3));
    localHashMap1.put("max", Double.valueOf(d2));
    localHashMap1.put("expected", Double.valueOf(d1));
    this.D.put(FuelType.Regular, localHashMap1);
    HashMap localHashMap2 = new HashMap();
    localHashMap2.put("min", Double.valueOf(d3 + paramPriceValidation.getOffsetMidgrade()));
    localHashMap2.put("max", Double.valueOf(d2 + paramPriceValidation.getOffsetMidgrade()));
    this.D.put(FuelType.Midgrade, localHashMap2);
    HashMap localHashMap3 = new HashMap();
    localHashMap3.put("min", Double.valueOf(d3 + paramPriceValidation.getOffsetPremium()));
    localHashMap3.put("max", Double.valueOf(d2 + paramPriceValidation.getOffsetPremium()));
    this.D.put(FuelType.Premium, localHashMap3);
    double d4 = paramPriceValidation.getExpectedDieselPrice();
    HashMap localHashMap4 = new HashMap();
    localHashMap4.put("min", Double.valueOf(d4 + paramPriceValidation.getCommonDecreaseDiesel()));
    localHashMap4.put("max", Double.valueOf(d4 + paramPriceValidation.getCommonIncreaseDiesel()));
    localHashMap4.put("expected", Double.valueOf(d4));
    this.D.put(FuelType.Diesel, localHashMap4);
  }

  private static void a(CustomEditTextForPrices paramCustomEditTextForPrices)
  {
    paramCustomEditTextForPrices.setNineCents(true);
  }

  private void a(String paramString1, String paramString2, boolean paramBoolean)
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this, this.mTracker);
    localBuilder.setPositiveButton(getString(2131296276), new n(this, paramString2));
    localBuilder.setNeutralButton(getString(2131296277), new o(this));
    if (paramBoolean)
    {
      localBuilder.setTitle(String.format(getString(2131296275), new Object[] { paramString2 }));
      localBuilder.setMessage(paramString1);
    }
    while (true)
    {
      localBuilder.create().show();
      return;
      localBuilder.setTitle(getString(2131296274));
      localBuilder.setMessage(paramString1);
      localBuilder.setPositiveButton(null, null);
    }
  }

  private static double b(String paramString)
  {
    double d1 = -1.0D;
    if ((!paramString.equals("---")) && (paramString.length() != 0));
    try
    {
      double d2 = Double.parseDouble(paramString);
      d1 = d2;
      return d1;
    }
    catch (Exception localException)
    {
    }
    return d1;
  }

  private void b()
  {
    this.B = ((ViewGroup)findViewById(2131165230));
    this.A = ((LinearLayout)findViewById(2131165302));
    this.r = ((CustomEditTextForPrices)findViewById(2131165304));
    this.s = ((CustomEditTextForPrices)findViewById(2131165307));
    this.t = ((CustomEditTextForPrices)findViewById(2131165310));
    this.u = ((CustomEditTextForPrices)findViewById(2131165313));
    this.v = ((EditText)findViewById(2131165318));
    p localp = new p(this);
    q localq = new q(this);
    r localr = new r(this);
    this.r.setOnTextChangedListener(localp);
    this.s.setOnTextChangedListener(localp);
    this.t.setOnTextChangedListener(localp);
    this.u.setOnTextChangedListener(localp);
    this.r.registerOnFocusChangeListener(this);
    this.s.registerOnFocusChangeListener(this);
    this.t.registerOnFocusChangeListener(this);
    this.u.registerOnFocusChangeListener(this);
    this.r.registerOnFocusChangeListener(localq);
    this.s.registerOnFocusChangeListener(localq);
    this.t.registerOnFocusChangeListener(localq);
    this.u.registerOnFocusChangeListener(localq);
    this.r.setOnClickListener(localr);
    this.s.setOnClickListener(localr);
    this.t.setOnClickListener(localr);
    this.u.setOnClickListener(localr);
    this.r.setNextFocusDownId(this.s.getId());
    this.s.setNextFocusDownId(this.t.getId());
    this.t.setNextFocusDownId(this.u.getId());
    this.u.setNextFocusDownId(this.v.getId());
    this.r.setFuelType(FuelType.Regular);
    this.s.setFuelType(FuelType.Midgrade);
    this.t.setFuelType(FuelType.Premium);
    this.u.setFuelType(FuelType.Diesel);
    if (this.j == Currency.USA)
    {
      a(this.r);
      a(this.s);
      a(this.t);
      a(this.u);
    }
    addToEditTextList(this.r);
    addToEditTextList(this.s);
    addToEditTextList(this.t);
    addToEditTextList(this.u);
    addToEditTextList(this.v);
    if (this.g != 0)
    {
      this.v.setVisibility(8);
      ((TextView)findViewById(2131165317)).setVisibility(8);
      this.v.setOnFocusChangeListener(new s(this));
      this.w = ((Spinner)findViewById(2131165316));
      List localList = d();
      ArrayAdapter localArrayAdapter = new ArrayAdapter(this, 2130903091, (List)localList.get(0));
      localArrayAdapter.setDropDownViewResource(2130903090);
      this.w.setAdapter(localArrayAdapter);
      this.w.setOnItemSelectedListener(new t(this, localList));
      setGBActivityDialogTitle(this.l.getStationName());
      setGBActivityDialogStationIcon(this.l);
      this.y = ((PriceKeyboardDrawer)findViewById(2131165320));
      this.z = this.y.getKeyboardView();
      this.y.setOnDrawerStateChangedListener(new u(this));
      if ((a) && (this.mPrefs.getBoolean("customKeyboardPreference", false)) && (this.mRes.getConfiguration().orientation != 2))
        break label729;
      this.C = false;
      this.r.usePriceKeyboard(false);
      this.s.usePriceKeyboard(false);
      this.t.usePriceKeyboard(false);
      this.u.usePriceKeyboard(false);
    }
    while (true)
    {
      ((Button)findViewById(2131165319)).setOnClickListener(new v(this));
      return;
      this.v.setVisibility(0);
      ((TextView)findViewById(2131165317)).setVisibility(0);
      break;
      label729: this.C = true;
      this.r.usePriceKeyboard(true);
      this.s.usePriceKeyboard(true);
      this.t.usePriceKeyboard(true);
      this.u.usePriceKeyboard(true);
    }
  }

  private void b(CustomEditTextForPrices paramCustomEditTextForPrices)
  {
    Integer localInteger = a(paramCustomEditTextForPrices.getFuelType(), Double.valueOf(b(paramCustomEditTextForPrices.getText().toString())));
    if (localInteger != null)
      paramCustomEditTextForPrices.setBackgroundHintFromValidStatus(localInteger.intValue());
  }

  private void c(String paramString)
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this, this.mTracker);
    localBuilder.setPositiveButton(getString(2131296666), new i(this));
    localBuilder.setNegativeButton(getString(2131296667), new j(this));
    localBuilder.setTitle(getString(2131296547));
    localBuilder.setMessage(paramString);
    localBuilder.create().show();
  }

  private boolean c()
  {
    String str1 = this.r.getText().toString();
    String str2 = this.s.getText().toString();
    String str3 = this.t.getText().toString();
    String str4 = this.u.getText().toString();
    if ((!str1.equals("---")) && (!str1.equals("")));
    while (((!str2.equals("---")) && (!str2.equals(""))) || ((!str3.equals("---")) && (!str3.equals(""))) || ((!str4.equals("---")) && (!str4.equals(""))))
      return true;
    return false;
  }

  private static Boolean d(String paramString)
  {
    int i1 = paramString.indexOf(DECIMAL.charValue());
    if ((paramString.length() > 5) || (paramString.length() < 4) || (i1 == -1) || (i1 + 1 == paramString.length()))
      return Boolean.valueOf(true);
    return Boolean.valueOf(false);
  }

  private static List<List<String>> d()
  {
    ArrayList localArrayList1 = new ArrayList(2);
    ArrayList localArrayList2 = new ArrayList();
    ArrayList localArrayList3 = new ArrayList();
    localArrayList2.add(DateUtils.getTimeNow());
    localArrayList3.add("now");
    Date[] arrayOfDate = DateUtils.getPrevHours();
    int i1 = arrayOfDate.length;
    for (int i2 = 0; ; i2++)
    {
      if ((i2 >= i1) || (arrayOfDate[i2] == null))
      {
        localArrayList1.add(localArrayList2);
        localArrayList1.add(localArrayList3);
        return localArrayList1;
      }
      Date localDate = arrayOfDate[i2];
      localArrayList2.add(DateUtils.getTimeToString(localDate));
      localArrayList3.add(DateUtils.getTimeToValue(localDate));
    }
  }

  private void e()
  {
    if (this.g == 0)
      if ((!this.e.equals("")) && (!this.e.equals(this.h.getCarIcon())))
      {
        localEditor2 = PreferenceManager.getDefaultSharedPreferences(getBaseContext()).edit();
        localEditor2.putString("car", this.h.getCarIcon());
        localEditor2.putInt("car_icon_id", this.h.getCarIconId());
        localEditor2.commit();
      }
    while (this.e.equals(this.i.getCarIcon()))
    {
      SharedPreferences.Editor localEditor2;
      return;
    }
    SharedPreferences.Editor localEditor1 = PreferenceManager.getDefaultSharedPreferences(getBaseContext()).edit();
    localEditor1.putString("car", this.i.getCarIcon());
    localEditor1.putInt("car_icon_id", this.i.getCarIconId());
    localEditor1.commit();
  }

  private void f()
  {
    this.e = this.mPrefs.getString("car", "");
    this.f = this.mPrefs.getInt("car_icon_id", 0);
    this.d = this.mPrefs.getString("member_id", "");
  }

  private void g()
  {
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
    {
      String str1 = "";
      String str2 = "";
      String str3 = "";
      String str4 = "";
      double d1 = localBundle.getDouble("fuel regular");
      if (d1 != 0.0D)
        str1 = localBundle.getString("time spotted regular");
      double d2 = localBundle.getDouble("fuel midgrade");
      if (d2 != 0.0D)
        str2 = localBundle.getString("time spotted midgrade");
      double d3 = localBundle.getDouble("fuel premium");
      if (d3 != 0.0D)
        str3 = localBundle.getString("time spotted premium");
      double d4 = localBundle.getDouble("fuel diesel");
      if (d4 != 0.0D)
        str4 = localBundle.getString("time spotted diesel");
      int i1 = localBundle.getInt("time offset");
      this.j.toString();
      a(d1, str1, d2, str2, d3, str3, d4, str4, i1);
    }
  }

  private void h()
  {
    new b(this).execute(new Object[0]);
  }

  private void i()
  {
    Type localType = new f(this).getType();
    this.l = ((StationMessage)new StationDetailsQuery(this, this.mPrefs, localType, this.myLocation).getResponseObject(this.l.getStationId()).getPayload());
  }

  private void j()
  {
    String str = p();
    if ((str == null) || (str.equals("")))
    {
      k();
      return;
    }
    c(str);
  }

  private void k()
  {
    a locala = new a(this);
    loadDialog(getString(2131296397), locala);
    locala.execute(new Object[0]);
    setAnalyticsTrackEventScreenButton(getString(2131296676));
  }

  private boolean l()
  {
    if (this.myLocation == null)
      setMyLocationToZeros();
    if (this.g == 0)
    {
      Type localType2 = new g(this).getType();
      this.mResponseObject = new ReportPricesQuery(this, this.mPrefs, localType2, this.myLocation).getResponseObject(this.l.getStationId(), this.m, this.n, this.o, this.p, this.e, this.f, this.k, this.v.getText().toString());
      this.h = ((ReportMessage)this.mResponseObject.getPayload());
      if ((this.h.getRegPriceId().doubleValue() != 0.0D) || (this.h.getMidPriceId().doubleValue() != 0.0D) || (this.h.getPremPriceId().doubleValue() != 0.0D) || (this.h.getDieselPriceId().doubleValue() != 0.0D))
      {
        this.q = this.h.getTotalPointsAwarded();
        e();
        return true;
      }
      setMessage(this.mRes.getString(2131296265));
      return false;
    }
    Type localType1 = new h(this).getType();
    this.mResponseObject = new ReportPricesFavoriteQuery(this, this.mPrefs, localType1, this.myLocation).getResponseObject(this.l.getStationId(), this.g, this.m, this.n, this.o, this.p, this.e, this.f, this.k);
    this.i = ((FavReportMessage)this.mResponseObject.getPayload());
    if ((this.i.getMinPriceId().doubleValue() == 0.0D) || (this.i.getMinPriceId().doubleValue() == 0.0D))
    {
      setMessage(this.mRes.getString(2131296265));
      return false;
    }
    this.q = this.i.getTotalPointsAwarded();
    e();
    return true;
  }

  private void m()
  {
    Intent localIntent = new Intent();
    localIntent.putExtra("sm_id", this.l.getStationId());
    localIntent.putExtra("fuel regular", this.m);
    localIntent.putExtra("fuel midgrade", this.n);
    localIntent.putExtra("fuel premium", this.o);
    localIntent.putExtra("fuel diesel", this.p);
    localIntent.putExtra("time spotted", DateUtils.changeToServerType(this.k));
    setResult(3, localIntent);
  }

  private void n()
  {
    SharedPreferences.Editor localEditor = this.mPrefs.edit();
    localEditor.putLong("lastReportedPriceDate", Calendar.getInstance().getTimeInMillis());
    localEditor.commit();
  }

  private void o()
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this);
    localBuilder.setPositiveButton(getString(2131296666), new k(this));
    localBuilder.setNegativeButton(getString(2131296667), new m(this));
    localBuilder.setTitle(getString(2131296534));
    localBuilder.setMessage(2131296268);
    localBuilder.create().show();
  }

  private String p()
  {
    String str1 = this.r.getText().toString();
    String str2 = this.s.getText().toString();
    String str3 = this.t.getText().toString();
    String str4 = this.u.getText().toString();
    this.m = Double.valueOf(b(str1));
    this.n = Double.valueOf(b(str2));
    this.o = Double.valueOf(b(str3));
    this.p = Double.valueOf(b(str4));
    if ((this.D == null) || (this.D.size() < 4))
    {
      localObject1 = "";
      return localObject1;
    }
    Object localObject1 = "";
    int i1;
    if (this.j == Currency.USA)
    {
      i1 = 2;
      if (((Double)((HashMap)this.D.get(FuelType.Regular)).get("expected")).doubleValue() != 0.0D)
        if (this.m.doubleValue() <= 0.0D)
          break label460;
    }
    label458: label460: for (Object localObject2 = localObject1 + a(VerifyFields.doubleToScale(this.m.doubleValue(), i1), a(FuelType.Regular, this.m).intValue()); ; localObject2 = localObject1)
    {
      if (this.n.doubleValue() > 0.0D)
        localObject2 = localObject2 + a(VerifyFields.doubleToScale(this.n.doubleValue(), i1), a(FuelType.Midgrade, this.n).intValue());
      if (this.o.doubleValue() > 0.0D);
      for (localObject1 = localObject2 + a(VerifyFields.doubleToScale(this.o.doubleValue(), i1), a(FuelType.Premium, this.o).intValue()); ; localObject1 = localObject2)
      {
        if ((this.p.doubleValue() <= 0.0D) || (((Double)((HashMap)this.D.get(FuelType.Diesel)).get("expected")).doubleValue() == 0.0D))
          break label458;
        return localObject1 + a(VerifyFields.doubleToScale(this.p.doubleValue(), i1), a(FuelType.Diesel, this.p).intValue());
        i1 = 1;
        break;
      }
      break;
    }
  }

  protected void OnSmartPromptPressedNo(DialogInterface paramDialogInterface, int paramInt, boolean paramBoolean)
  {
    super.OnSmartPromptPressedNo(paramDialogInterface, paramInt, paramBoolean);
    finish();
  }

  protected void OnSmartPromptPressedYes(DialogInterface paramDialogInterface, int paramInt, boolean paramBoolean)
  {
    super.OnSmartPromptPressedYes(paramDialogInterface, paramInt, paramBoolean);
    finish();
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    switch (paramInt1)
    {
    default:
    case 1234:
    }
    while (true)
    {
      super.onActivityResult(paramInt1, paramInt2, paramIntent);
      return;
      if (paramInt2 == -1)
        a(paramIntent);
    }
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Object[] arrayOfObject = (Object[])getLastNonConfigurationInstance();
    Bundle localBundle = getIntent().getExtras();
    f();
    if (localBundle != null)
    {
      Station localStation = (Station)localBundle.getParcelable("station");
      this.l = new StationMessage();
      this.l.setStationName(localStation.getStationName());
      this.l.setGasBrandId(localStation.getGasBrandId());
      this.l.setStationId(localStation.getStationId());
      this.l.setCountry(localStation.getCountry());
      this.g = localBundle.getInt("favourite id");
      this.j = Currency.valueOf(this.l.getCountry());
      b();
      if (arrayOfObject != null)
      {
        this.r.setText((String)arrayOfObject[0]);
        this.s.setText((String)arrayOfObject[1]);
        this.t.setText((String)arrayOfObject[2]);
        this.u.setText((String)arrayOfObject[3]);
        this.l = ((StationMessage)arrayOfObject[4]);
        if (this.l != null)
        {
          double d1 = this.l.getRegPrice();
          String str1 = this.l.getRegTimeSpotted();
          double d2 = this.l.getMidPrice();
          String str2 = this.l.getMidTimeSpotted();
          double d3 = this.l.getPremPrice();
          String str3 = this.l.getPremTimeSpotted();
          double d4 = this.l.getDieselPrice();
          String str4 = this.l.getDieselTimeSpotted();
          int i1 = this.l.getTimeOffset();
          this.l.getCountry();
          a(d1, str1, d2, str2, d3, str3, d4, str4, i1);
          a(this.l.getPriceValidation());
          b(this.r);
          b(this.s);
          b(this.t);
          b(this.u);
        }
      }
    }
    while (true)
    {
      this.A.getViewTreeObserver().addOnGlobalLayoutListener(new a(this));
      setEditBoxHintConfiguration();
      if ((!this.mPrefs.getBoolean("largeScreenBoolean", true)) && (this.mRes.getConfiguration().orientation != 2))
      {
        new Handler().postDelayed(new l(this), 300L);
        getWindow().setLayout(-1, -1);
        findViewById(2131165230).setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        ViewGroup.LayoutParams localLayoutParams = this.A.getLayoutParams();
        localLayoutParams.height = -1;
        this.A.setLayoutParams(localLayoutParams);
      }
      return;
      if (this.l.getStationId() != 0)
        h();
      g();
      continue;
      finish();
    }
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    super.onCreateOptionsMenu(paramMenu);
    getMenuInflater().inflate(2131427335, paramMenu);
    return super.onCreateOptionsMenu(paramMenu);
  }

  public void onFocusChange(View paramView, boolean paramBoolean)
  {
    if (!(paramView instanceof CustomEditTextForPrices));
    CustomEditTextForPrices localCustomEditTextForPrices;
    do
    {
      Integer localInteger;
      do
      {
        do
        {
          return;
          localCustomEditTextForPrices = (CustomEditTextForPrices)paramView;
        }
        while (localCustomEditTextForPrices.getFuelType() == null);
        Double localDouble = Double.valueOf(b(localCustomEditTextForPrices.getText().toString()));
        localInteger = a(localCustomEditTextForPrices.getFuelType(), localDouble);
        if (!paramBoolean)
          break;
      }
      while ((!localCustomEditTextForPrices.getText().toString().equals("---")) && (!localCustomEditTextForPrices.getText().toString().equals("")));
      localCustomEditTextForPrices.setText("");
      localCustomEditTextForPrices.setBackgroundHint(0);
      return;
      if (localCustomEditTextForPrices.getText().toString().equals(""))
      {
        localCustomEditTextForPrices.setText("---");
        localCustomEditTextForPrices.setBackgroundHint(0);
        return;
      }
      if (localInteger != null)
        localCustomEditTextForPrices.setBackgroundHintFromValidStatus(localInteger.intValue());
    }
    while (!d(localCustomEditTextForPrices.getText().toString()).booleanValue());
    localCustomEditTextForPrices.setBackgroundHint(1);
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    boolean bool = true;
    switch (paramInt)
    {
    default:
      bool = super.onKeyDown(paramInt, paramKeyEvent);
    case 4:
    case 84:
    case 82:
    }
    while (true)
    {
      return bool;
      if (this.y.isClosed())
        break;
      this.y.close();
      return bool;
      this.y.close();
      break;
      if (((this.z.getTarget() instanceof CustomEditTextForPrices)) && (this.C))
        try
        {
          if (Integer.parseInt(Build.VERSION.SDK) >= 4)
          {
            paramKeyEvent.getClass().getMethod("startTracking", new Class[0]).invoke(paramKeyEvent, new Object[0]);
            return bool;
          }
        }
        catch (Exception localException)
        {
          return bool;
        }
    }
    return false;
  }

  public boolean onKeyLongPress(int paramInt, KeyEvent paramKeyEvent)
  {
    switch (paramInt)
    {
    default:
      return super.onKeyDown(paramInt, paramKeyEvent);
    case 82:
    }
    if (((this.z.getTarget() instanceof CustomEditTextForPrices)) && (this.C))
    {
      this.y.toggle();
      return true;
    }
    return false;
  }

  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
  {
    switch (paramInt)
    {
    default:
    case 82:
    }
    while (true)
    {
      return super.onKeyUp(paramInt, paramKeyEvent);
      if (((this.z.getTarget() instanceof CustomEditTextForPrices)) && (this.C) && ((0x100 & paramKeyEvent.getFlags()) == 0))
        openOptionsMenu();
    }
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    super.onOptionsItemSelected(paramMenuItem);
    switch (paramMenuItem.getItemId())
    {
    default:
      return super.onOptionsItemSelected(paramMenuItem);
    case 2131165598:
    }
    launchSettings();
    return true;
  }

  public void onResume()
  {
    super.onResume();
    f();
    getWindow().setSoftInputMode(16);
  }

  public Object onRetainNonConfigurationInstance()
  {
    Object[] arrayOfObject = new Object[5];
    arrayOfObject[0] = this.r.getText().toString();
    arrayOfObject[1] = this.s.getText().toString();
    arrayOfObject[2] = this.t.getText().toString();
    arrayOfObject[3] = this.u.getText().toString();
    arrayOfObject[4] = this.l;
    return arrayOfObject;
  }

  protected void onSmartPromptCancelled(DialogInterface paramDialogInterface, int paramInt, boolean paramBoolean)
  {
    super.onSmartPromptCancelled(paramDialogInterface, paramInt, paramBoolean);
    finish();
  }

  protected void onSmartPromptPressedDontKnow(DialogInterface paramDialogInterface, int paramInt, boolean paramBoolean)
  {
    super.onSmartPromptPressedDontKnow(paramDialogInterface, paramInt, paramBoolean);
    finish();
  }

  protected void setAdsAfterGPSServiceConnected()
  {
  }

  protected String setAnalyticsPageName()
  {
    if (getIntent().getExtras().containsKey("favourite id"))
      return getString(2131296820);
    return getString(2131296819);
  }

  protected void setGBActivityDialogButtons()
  {
    setGBViewInContentLayout(2130903070, false);
  }

  public void submitPricesClick()
  {
    if (!c())
    {
      showMessage(this.mRes.getString(2131296267));
      return;
    }
    f();
    if (!this.d.equals(""))
    {
      j();
      return;
    }
    o();
  }

  public static enum Currency
  {
    static
    {
      CAN = new Currency("CAN", 1);
      Currency[] arrayOfCurrency = new Currency[2];
      arrayOfCurrency[0] = USA;
      arrayOfCurrency[1] = CAN;
    }
  }

  public static enum FuelType
  {
    static
    {
      Midgrade = new FuelType("Midgrade", 1);
      Premium = new FuelType("Premium", 2);
      Diesel = new FuelType("Diesel", 3);
      FuelType[] arrayOfFuelType = new FuelType[4];
      arrayOfFuelType[0] = Regular;
      arrayOfFuelType[1] = Midgrade;
      arrayOfFuelType[2] = Premium;
      arrayOfFuelType[3] = Diesel;
    }
  }

  private final class a extends CustomAsyncTask
  {
    public a(GBActivity arg2)
    {
      super();
    }

    private boolean a()
    {
      for (int i = 0; ; i++)
      {
        if (i >= ReportPrices.j(ReportPrices.this).getSmartPrompts().size())
          return false;
        if (((SmartPrompt)ReportPrices.j(ReportPrices.this).getSmartPrompts().get(i)).getPromptId() >= 0)
          return true;
      }
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      try
      {
        ReportPrices.c(ReportPrices.this).dismiss();
        label15: if (paramBoolean.booleanValue())
        {
          if (ReportPrices.d(ReportPrices.this) == 0)
            break label169;
          ReportPrices localReportPrices = ReportPrices.this;
          String str = ReportPrices.e(ReportPrices.this).getString(2131296262);
          Object[] arrayOfObject = new Object[1];
          arrayOfObject[0] = Integer.valueOf(ReportPrices.d(ReportPrices.this));
          localReportPrices.showMessage(String.format(str, arrayOfObject));
          ReportPrices.f(ReportPrices.this);
          ReportPrices.g(ReportPrices.this);
          if (ReportPrices.h(ReportPrices.this) != 0)
            break label188;
          setAnalyticsTrackEventScreenButton(ReportPrices.i(ReportPrices.this), ReportPrices.this.getString(2131296857));
        }
        while (true)
        {
          if ((ReportPrices.j(ReportPrices.this).getSmartPrompts().size() == 0) || (!a()) || (!ReportPrices.k(ReportPrices.this)))
            ReportPrices.this.finish();
          return;
          label169: ReportPrices.this.showMessage(ReportPrices.this.getString(2131296263));
          break;
          label188: setAnalyticsTrackEventScreenButton(ReportPrices.i(ReportPrices.this), ReportPrices.this.getString(2131296857), ReportPrices.this.getString(2131296856));
        }
      }
      catch (Exception localException)
      {
        break label15;
      }
    }

    protected final boolean queryWebService()
    {
      return ReportPrices.l(ReportPrices.this);
    }
  }

  private final class b extends CustomAsyncTask
  {
    public b(GBActivity arg2)
    {
      super();
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      if (paramBoolean.booleanValue())
      {
        ReportPrices.a(ReportPrices.this, ReportPrices.a(ReportPrices.this).getRegPrice(), ReportPrices.a(ReportPrices.this).getRegTimeSpotted(), ReportPrices.a(ReportPrices.this).getMidPrice(), ReportPrices.a(ReportPrices.this).getMidTimeSpotted(), ReportPrices.a(ReportPrices.this).getPremPrice(), ReportPrices.a(ReportPrices.this).getPremTimeSpotted(), ReportPrices.a(ReportPrices.this).getDieselPrice(), ReportPrices.a(ReportPrices.this).getDieselTimeSpotted(), ReportPrices.a(ReportPrices.this).getTimeOffset(), ReportPrices.a(ReportPrices.this).getCountry());
        ReportPrices.a(ReportPrices.this, ReportPrices.a(ReportPrices.this).getPriceValidation());
      }
    }

    protected final boolean queryWebService()
    {
      ReportPrices.b(ReportPrices.this);
      return true;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.report.ReportPrices
 * JD-Core Version:    0.6.2
 */