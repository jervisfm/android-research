package gbis.gbandroid.activities.report;

import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.LinearLayout;

final class a
  implements ViewTreeObserver.OnGlobalLayoutListener
{
  a(ReportPrices paramReportPrices)
  {
  }

  public final void onGlobalLayout()
  {
    int i = ReportPrices.m(this.a).getMeasuredHeight();
    if ((i != 0) && (i > ReportPrices.n(this.a).intValue()))
      ReportPrices.a(this.a, Integer.valueOf(i));
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.report.a
 * JD-Core Version:    0.6.2
 */