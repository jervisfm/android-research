package gbis.gbandroid.activities.report;

import android.view.View;
import android.view.View.OnClickListener;
import gbis.gbandroid.utils.VerifyFields;
import gbis.gbandroid.views.CustomEditTextForPrices;
import java.math.BigDecimal;

final class b
  implements View.OnClickListener
{
  b(ReportPrices paramReportPrices, double paramDouble, int paramInt)
  {
  }

  public final void onClick(View paramView)
  {
    ReportPrices.b(this.a, this.a.getString(2131296717));
    ReportPrices.t(this.a).setText(VerifyFields.doubleToScale(this.b, this.c).toString());
    ReportPrices.a(this.a, ReportPrices.t(this.a));
    this.a.findViewById(ReportPrices.t(this.a).getNextFocusDownId()).requestFocus();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.report.b
 * JD-Core Version:    0.6.2
 */