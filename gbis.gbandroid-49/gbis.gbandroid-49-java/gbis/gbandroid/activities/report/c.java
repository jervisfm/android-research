package gbis.gbandroid.activities.report;

import android.view.View;
import android.view.View.OnClickListener;
import gbis.gbandroid.utils.VerifyFields;
import gbis.gbandroid.views.CustomEditTextForPrices;
import java.math.BigDecimal;

final class c
  implements View.OnClickListener
{
  c(ReportPrices paramReportPrices, double paramDouble, int paramInt)
  {
  }

  public final void onClick(View paramView)
  {
    ReportPrices.b(this.a, this.a.getString(2131296718));
    ReportPrices.u(this.a).setText(VerifyFields.doubleToScale(this.b, this.c).toString());
    ReportPrices.a(this.a, ReportPrices.u(this.a));
    this.a.findViewById(ReportPrices.u(this.a).getNextFocusDownId()).requestFocus();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.report.c
 * JD-Core Version:    0.6.2
 */