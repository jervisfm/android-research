package gbis.gbandroid.activities.report;

import android.widget.EditText;
import gbis.gbandroid.activities.report.pricekeyboard.ReportPriceKeyboardView;
import gbis.gbandroid.activities.report.pricekeyboard.base.SafeSpannableStringBuilder;
import gbis.gbandroid.views.CustomEditTextForPrices;
import gbis.gbandroid.views.CustomEditTextForPrices.OnTextChangedListener;
import java.util.EnumMap;

final class p
  implements CustomEditTextForPrices.OnTextChangedListener
{
  p(ReportPrices paramReportPrices)
  {
  }

  public final void onTextChanged(CustomEditTextForPrices paramCustomEditTextForPrices, boolean paramBoolean, String paramString1, String paramString2, int paramInt)
  {
    paramCustomEditTextForPrices.haltTextWatcher();
    SafeSpannableStringBuilder localSafeSpannableStringBuilder = (SafeSpannableStringBuilder)paramCustomEditTextForPrices.getEditableText();
    Integer localInteger = ReportPrices.a(this.a, paramString1.replace(ReportPrices.DECIMAL_STRING, ""));
    boolean bool = localSafeSpannableStringBuilder.toString().contains(ReportPrices.DECIMAL_STRING);
    int i = 0;
    int j = 0;
    char[] arrayOfChar;
    int i2;
    if (bool)
    {
      arrayOfChar = paramString1.toCharArray();
      int i1 = arrayOfChar.length;
      i2 = 0;
      if (i2 < i1)
        break label352;
      j = 0;
      if (i > 1)
        j = 1;
      if ((localInteger == null) || (localSafeSpannableStringBuilder.toString().indexOf(ReportPrices.DECIMAL.charValue()) != localInteger.intValue()))
        j = 1;
    }
    if (j != 0)
    {
      localSafeSpannableStringBuilder.stripCharacter(ReportPrices.DECIMAL);
      i = 0;
    }
    if (!paramBoolean)
    {
      int n = paramString2.indexOf(ReportPrices.DECIMAL.charValue());
      if ((paramString1.equals(paramString2.replace(ReportPrices.DECIMAL_STRING, ""))) && (n >= 0))
      {
        localSafeSpannableStringBuilder.delete(n - 1, n);
        i = 0;
      }
    }
    if (localInteger != null)
    {
      if ((localSafeSpannableStringBuilder.length() > 4) && (i == 0))
        localSafeSpannableStringBuilder.delete(4, 5);
      if ((localInteger.intValue() <= localSafeSpannableStringBuilder.length()) && (i <= 0))
        localSafeSpannableStringBuilder.insert(localInteger.intValue(), ReportPrices.DECIMAL_STRING);
      int k = localSafeSpannableStringBuilder.subSequence(1 + localInteger.intValue(), localSafeSpannableStringBuilder.length()).length();
      int m = ((Integer)ReportPrices.a().get(ReportPrices.p(this.a))).intValue();
      if (k >= m)
      {
        if (k <= m)
          break label378;
        localSafeSpannableStringBuilder.overwrite(paramString2);
        if ((paramInt > 0) && (paramInt < localSafeSpannableStringBuilder.length()))
          ReportPrices.q(this.a).getTarget().setSelection(paramInt - 1);
      }
    }
    while (true)
    {
      paramCustomEditTextForPrices.enableTextWatcher();
      return;
      label352: if (Character.valueOf(arrayOfChar[i2]).equals(ReportPrices.DECIMAL))
        i++;
      i2++;
      break;
      label378: if (paramBoolean)
        ReportPrices.q(this.a).focusNext();
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.report.p
 * JD-Core Version:    0.6.2
 */