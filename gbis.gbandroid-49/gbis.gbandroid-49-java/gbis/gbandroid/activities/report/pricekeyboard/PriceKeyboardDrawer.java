package gbis.gbandroid.activities.report.pricekeyboard;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;

public class PriceKeyboardDrawer extends LinearLayout
{
  Animation a;
  Animation b;
  private Context c;
  private ReportPriceKeyboardView d;
  private boolean e = true;
  protected OnDrawerStateChangedListener onDrawerStateChangedListener;

  public PriceKeyboardDrawer(Context paramContext)
  {
    super(paramContext);
    this.c = paramContext;
    a();
  }

  public PriceKeyboardDrawer(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.c = paramContext;
    a();
  }

  private void a()
  {
    ((LayoutInflater)this.c.getSystemService("layout_inflater")).inflate(2130903118, this);
    this.d = ((ReportPriceKeyboardView)findViewById(2131165474));
    this.a = AnimationUtils.loadAnimation(this.c, 2130968579);
    this.b = AnimationUtils.loadAnimation(this.c, 2130968578);
    this.a.setAnimationListener(new a(this));
  }

  public void close()
  {
    if (this.e);
    do
    {
      return;
      setVisibility(4);
      startAnimation(this.b);
      this.e = true;
    }
    while (this.onDrawerStateChangedListener == null);
    this.onDrawerStateChangedListener.onClose();
  }

  public ReportPriceKeyboardView getKeyboardView()
  {
    return this.d;
  }

  public boolean isClosed()
  {
    return this.e;
  }

  public void open()
  {
    if (!this.e)
      return;
    ((InputMethodManager)this.c.getSystemService("input_method")).hideSoftInputFromWindow(getWindowToken(), 0);
    setVisibility(0);
    startAnimation(this.a);
    this.e = false;
  }

  public void setOnDrawerStateChangedListener(OnDrawerStateChangedListener paramOnDrawerStateChangedListener)
  {
    this.onDrawerStateChangedListener = paramOnDrawerStateChangedListener;
  }

  public void toggle()
  {
    if (this.e)
    {
      open();
      return;
    }
    close();
  }

  public static abstract interface OnDrawerStateChangedListener
  {
    public abstract void onClose();

    public abstract void onOpen();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.report.pricekeyboard.PriceKeyboardDrawer
 * JD-Core Version:    0.6.2
 */