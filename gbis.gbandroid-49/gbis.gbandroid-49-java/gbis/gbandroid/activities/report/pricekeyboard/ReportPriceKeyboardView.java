package gbis.gbandroid.activities.report.pricekeyboard;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.FocusFinder;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.EditText;
import gbis.gbandroid.activities.report.pricekeyboard.base.CustomKeyboard;
import gbis.gbandroid.activities.report.pricekeyboard.base.CustomKeyboard.Key;
import gbis.gbandroid.activities.report.pricekeyboard.base.CustomKeyboardView;
import gbis.gbandroid.activities.report.pricekeyboard.base.CustomKeyboardView.OnKeyboardActionListener;
import java.util.Iterator;
import java.util.List;

public class ReportPriceKeyboardView extends CustomKeyboardView
{
  public static final int KEYCODE_CLEAR = -3;
  public static final int KEYCODE_DISABLED = -999;
  public static final int KEYCODE_NEXT = -2;
  public static final int KEYCODE_VOICE = -1;
  public static final int VOICE_RECOGNITION_REQUEST_CODE = 1234;
  CustomKeyboardView.OnKeyboardActionListener a = new b(this);
  private CustomKeyboard c;
  private Context d;
  private EditText e = null;
  private boolean f = false;

  public ReportPriceKeyboardView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.d = paramContext;
    a();
  }

  public ReportPriceKeyboardView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    this.d = paramContext;
    a();
  }

  private void a()
  {
    this.c = new CustomKeyboard(this.d, 2131034113);
    setKeyboard(this.c);
    setOnKeyboardActionListener(this.a);
    setSpeechEnabled(true);
  }

  public void focusNext()
  {
    if (!(this.d instanceof Activity));
    label103: label122: 
    while (true)
    {
      return;
      if ((this.e != null) && (this.e.getVisibility() != 8))
      {
        Object localObject1 = this.e.getParent();
        Object localObject2 = localObject1;
        if ((localObject2 == null) || (!(localObject2 instanceof ViewGroup)))
          if (this.e.getNextFocusDownId() == -1)
            break label103;
        for (View localView = ((ViewGroup)localObject1).findViewById(this.e.getNextFocusDownId()); ; localView = FocusFinder.getInstance().findNextFocus((ViewGroup)localObject1, this.e, 130))
        {
          if (localView == null)
            break label122;
          localView.requestFocus();
          return;
          ViewParent localViewParent = localObject2.getParent();
          localObject1 = localObject2;
          localObject2 = localViewParent;
          break;
        }
      }
    }
  }

  public EditText getTarget()
  {
    return this.e;
  }

  public void requestSpeechInput(int paramInt)
  {
    requestSpeechInput(paramInt, this.d.getString(2131296271));
  }

  public void requestSpeechInput(int paramInt, String paramString)
  {
    if (!this.f)
      return;
    Intent localIntent = new Intent("android.speech.action.RECOGNIZE_SPEECH");
    localIntent.putExtra("android.speech.extra.LANGUAGE_MODEL", "free_form");
    localIntent.putExtra("android.speech.extra.PROMPT", paramString);
    ((Activity)this.d).startActivityForResult(localIntent, paramInt);
  }

  public void setSpeechEnabled(boolean paramBoolean)
  {
    if ((this.d.getPackageManager().queryIntentActivities(new Intent("android.speech.action.RECOGNIZE_SPEECH"), 0).size() != 0) && (paramBoolean))
      this.f = true;
    while (true)
    {
      return;
      this.f = false;
      Iterator localIterator = this.c.getKeys().iterator();
      while (localIterator.hasNext())
      {
        CustomKeyboard.Key localKey = (CustomKeyboard.Key)localIterator.next();
        if (localKey.codes[0] == -1)
        {
          localKey.codes[0] = -999;
          localKey.icon = null;
          localKey.background = this.d.getResources().getDrawable(2130837619);
        }
      }
    }
  }

  public void setTarget(EditText paramEditText)
  {
    this.e = paramEditText;
  }

  public void setTargetText(String paramString)
  {
    this.e.setText(paramString);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.report.pricekeyboard.ReportPriceKeyboardView
 * JD-Core Version:    0.6.2
 */