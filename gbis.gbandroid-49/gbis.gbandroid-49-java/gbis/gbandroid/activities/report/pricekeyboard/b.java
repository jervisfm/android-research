package gbis.gbandroid.activities.report.pricekeyboard;

import android.view.KeyEvent;
import android.widget.EditText;
import gbis.gbandroid.activities.report.pricekeyboard.base.CustomKeyboardView.OnKeyboardActionListener;

final class b
  implements CustomKeyboardView.OnKeyboardActionListener
{
  b(ReportPriceKeyboardView paramReportPriceKeyboardView)
  {
  }

  public final void onKey(int paramInt, int[] paramArrayOfInt)
  {
    if ((ReportPriceKeyboardView.a(this.a) == null) || (paramInt == -999))
      return;
    long l = System.currentTimeMillis();
    KeyEvent localKeyEvent = new KeyEvent(l, l, 0, paramInt, 0, 0, 0, 0, 6);
    switch (paramInt)
    {
    default:
      localKeyEvent.dispatch(ReportPriceKeyboardView.a(this.a));
      return;
    case -2:
      this.a.focusNext();
      return;
    case -1:
      this.a.requestSpeechInput(1234);
      return;
    case -3:
    }
    ReportPriceKeyboardView.a(this.a).setText("");
  }

  public final void onPress(int paramInt)
  {
    if (paramInt != -999)
      this.a.performHapticFeedback(3);
  }

  public final void onRelease(int paramInt)
  {
  }

  public final void onText(CharSequence paramCharSequence)
  {
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.report.pricekeyboard.b
 * JD-Core Version:    0.6.2
 */