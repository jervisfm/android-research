package gbis.gbandroid.activities.report.pricekeyboard.base;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.util.Xml;
import gbis.gbandroid.R.styleable;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class CustomKeyboard
{
  public static final int EDGE_BOTTOM = 8;
  public static final int EDGE_LEFT = 1;
  public static final int EDGE_RIGHT = 2;
  public static final int EDGE_TOP = 4;
  public static final int KEYCODE_ALT = -6;
  public static final int KEYCODE_CANCEL = -3;
  public static final int KEYCODE_DELETE = -5;
  public static final int KEYCODE_DONE = -4;
  public static final int KEYCODE_MODE_CHANGE = -2;
  public static final int KEYCODE_SHIFT = -1;
  private static float s = 1.8F;
  private int a;
  private int b;
  private int c;
  private int d;
  private boolean e;
  private Key f;
  private int g = -1;
  private int h;
  private int i;
  private List<Key> j;
  private List<Key> k;
  private int l;
  private int m;
  private int n;
  private int o;
  private int p;
  private int[][] q;
  private int r;

  public CustomKeyboard(Context paramContext, int paramInt)
  {
    this(paramContext, paramInt, 0);
  }

  public CustomKeyboard(Context paramContext, int paramInt1, int paramInt2)
  {
    DisplayMetrics localDisplayMetrics = paramContext.getResources().getDisplayMetrics();
    this.l = localDisplayMetrics.widthPixels;
    this.m = localDisplayMetrics.heightPixels;
    this.a = 0;
    this.b = (this.l / 4);
    this.d = 0;
    this.c = this.b;
    this.j = new ArrayList();
    this.k = new ArrayList();
    this.n = paramInt2;
    a(paramContext, paramContext.getResources().getXml(paramInt1));
  }

  static int a(TypedArray paramTypedArray, int paramInt1, int paramInt2, int paramInt3)
  {
    TypedValue localTypedValue = paramTypedArray.peekValue(paramInt1);
    if (localTypedValue == null);
    do
    {
      return paramInt3;
      if (localTypedValue.type == 5)
        return paramTypedArray.getDimensionPixelOffset(paramInt1, paramInt3);
    }
    while (localTypedValue.type != 6);
    return Math.round(paramTypedArray.getFraction(paramInt1, paramInt2, paramInt2, paramInt3));
  }

  private void a()
  {
    this.o = ((-1 + (10 + getMinWidth())) / 10);
    this.p = ((-1 + (5 + getHeight())) / 5);
    this.q = new int[50][];
    int[] arrayOfInt1 = new int[this.j.size()];
    int i1 = 10 * this.o;
    int i2 = 5 * this.p;
    int i3 = 0;
    int i4;
    while (true)
    {
      if (i3 >= i1)
        return;
      i4 = 0;
      if (i4 < i2)
        break;
      i3 += this.o;
    }
    int i5 = 0;
    int i6 = 0;
    while (true)
    {
      if (i5 >= this.j.size())
      {
        int[] arrayOfInt2 = new int[i6];
        System.arraycopy(arrayOfInt1, 0, arrayOfInt2, 0, i6);
        this.q[(10 * (i4 / this.p) + i3 / this.o)] = arrayOfInt2;
        i4 += this.p;
        break;
      }
      Key localKey = (Key)this.j.get(i5);
      if ((localKey.squaredDistanceFrom(i3, i4) < this.r) || (localKey.squaredDistanceFrom(-1 + (i3 + this.o), i4) < this.r) || (localKey.squaredDistanceFrom(-1 + (i3 + this.o), -1 + (i4 + this.p)) < this.r) || (localKey.squaredDistanceFrom(i3, -1 + (i4 + this.p)) < this.r))
      {
        int i7 = i6 + 1;
        arrayOfInt1[i6] = i5;
        i6 = i7;
      }
      i5++;
    }
  }

  private void a(Context paramContext, XmlResourceParser paramXmlResourceParser)
  {
    Row localRow = null;
    Resources localResources = paramContext.getResources();
    Object localObject = null;
    int i1 = 0;
    int i2 = 0;
    int i3 = 0;
    int i4 = 0;
    while (true)
    {
      int i5;
      int i7;
      Key localKey;
      try
      {
        i5 = paramXmlResourceParser.next();
        if (i5 == 1)
        {
          this.h = (i1 - this.d);
          return;
        }
        if (i5 == 2)
        {
          String str = paramXmlResourceParser.getName();
          if ("Row".equals(str))
          {
            localRow = createRowFromXml(localResources, paramXmlResourceParser);
            if ((localRow.mode == 0) || (localRow.mode == this.n))
              break label403;
            i7 = 1;
            if (i7 == 0)
              break label394;
            a(paramXmlResourceParser);
            i2 = 0;
            i3 = 0;
            continue;
          }
          if ("Key".equals(str))
          {
            localKey = createKeyFromXml(localResources, localRow, i2, i1, paramXmlResourceParser);
            this.j.add(localKey);
            if (localKey.codes[0] == -1)
            {
              this.f = localKey;
              this.g = (-1 + this.j.size());
              this.k.add(localKey);
              localObject = localKey;
              i4 = 1;
              continue;
            }
            if (localKey.codes[0] != -6)
              break label384;
            this.k.add(localKey);
            localObject = localKey;
            i4 = 1;
            continue;
          }
          if (!"Keyboard".equals(str))
            continue;
          a(localResources, paramXmlResourceParser);
          continue;
        }
      }
      catch (Exception localException)
      {
        Log.e("Keyboard", "Parse error:" + localException);
        localException.printStackTrace();
        continue;
      }
      if (i5 == 3)
        if (i4 != 0)
        {
          i2 += localObject.gap + localObject.width;
          if (i2 > this.i)
          {
            this.i = i2;
            i4 = 0;
          }
        }
        else if (i3 != 0)
        {
          i1 += localRow.verticalGap;
          int i6 = localRow.defaultHeight;
          i1 += i6;
          i3 = 0;
          continue;
          i4 = 0;
          continue;
          label384: localObject = localKey;
          i4 = 1;
          continue;
          label394: i3 = 1;
          i2 = 0;
          continue;
          label403: i7 = 0;
        }
    }
  }

  private void a(Resources paramResources, XmlResourceParser paramXmlResourceParser)
  {
    TypedArray localTypedArray = paramResources.obtainAttributes(Xml.asAttributeSet(paramXmlResourceParser), R.styleable.Keyboard);
    this.b = a(localTypedArray, 0, this.l, this.l / 4);
    this.c = a(localTypedArray, 1, this.m, 50);
    this.a = a(localTypedArray, 2, this.l, 0);
    this.d = a(localTypedArray, 3, this.m, 0);
    this.r = ((int)(this.b * s));
    this.r *= this.r;
    localTypedArray.recycle();
  }

  private static void a(XmlResourceParser paramXmlResourceParser)
  {
    while (true)
    {
      int i1 = paramXmlResourceParser.next();
      if (i1 == 1);
      do
      {
        return;
        if (i1 != 3)
          break;
      }
      while (paramXmlResourceParser.getName().equals("Row"));
    }
  }

  protected Key createKeyFromXml(Resources paramResources, Row paramRow, int paramInt1, int paramInt2, XmlResourceParser paramXmlResourceParser)
  {
    return new Key(paramResources, paramRow, paramInt1, paramInt2, paramXmlResourceParser);
  }

  protected Row createRowFromXml(Resources paramResources, XmlResourceParser paramXmlResourceParser)
  {
    return new Row(paramResources, this, paramXmlResourceParser);
  }

  public int getHeight()
  {
    return this.h;
  }

  protected int getHorizontalGap()
  {
    return this.a;
  }

  protected int getKeyHeight()
  {
    return this.c;
  }

  protected int getKeyWidth()
  {
    return this.b;
  }

  public List<Key> getKeys()
  {
    return this.j;
  }

  public int getMinWidth()
  {
    return this.i;
  }

  public List<Key> getModifierKeys()
  {
    return this.k;
  }

  public int[] getNearestKeys(int paramInt1, int paramInt2)
  {
    if (this.q == null)
      a();
    if ((paramInt1 >= 0) && (paramInt1 < getMinWidth()) && (paramInt2 >= 0) && (paramInt2 < getHeight()))
    {
      int i1 = 10 * (paramInt2 / this.p) + paramInt1 / this.o;
      if (i1 < 50)
        return this.q[i1];
    }
    return new int[0];
  }

  public int getShiftKeyIndex()
  {
    return this.g;
  }

  protected int getVerticalGap()
  {
    return this.d;
  }

  public boolean isShifted()
  {
    return this.e;
  }

  protected void setHorizontalGap(int paramInt)
  {
    this.a = paramInt;
  }

  protected void setKeyHeight(int paramInt)
  {
    this.c = paramInt;
  }

  protected void setKeyWidth(int paramInt)
  {
    this.b = paramInt;
  }

  public boolean setShifted(boolean paramBoolean)
  {
    if (this.f != null)
      this.f.on = paramBoolean;
    if (this.e != paramBoolean)
    {
      this.e = paramBoolean;
      return true;
    }
    return false;
  }

  protected void setVerticalGap(int paramInt)
  {
    this.d = paramInt;
  }

  public static class Key
  {
    private static final int[] b = { 16842911, 16842912 };
    private static final int[] c = { 16842919, 16842911, 16842912 };
    private static final int[] d = { 16842911 };
    private static final int[] e = { 16842919, 16842911 };
    private static final int[] f = new int[0];
    private static final int[] g = { 16842919 };
    private CustomKeyboard a;
    public Drawable background;
    public int[] codes;
    public int edgeFlags;
    public int gap;
    public int height;
    public Drawable icon;
    public Drawable iconPreview;
    public CharSequence label;
    public boolean modifier;
    public boolean on;
    public CharSequence popupCharacters;
    public int popupResId;
    public boolean pressed;
    public boolean repeatable;
    public boolean sticky;
    public CharSequence text;
    public int width;
    public int x;
    public int y;

    public Key(Resources paramResources, CustomKeyboard.Row paramRow, int paramInt1, int paramInt2, XmlResourceParser paramXmlResourceParser)
    {
      this(paramRow);
      this.x = paramInt1;
      this.y = paramInt2;
      Log.d("GasBuddy", "Keyboard width: " + CustomKeyboard.g(CustomKeyboard.Row.a(paramRow)));
      TypedArray localTypedArray1 = paramResources.obtainAttributes(Xml.asAttributeSet(paramXmlResourceParser), R.styleable.Keyboard);
      this.width = CustomKeyboard.a(localTypedArray1, 0, CustomKeyboard.a(this.a), paramRow.defaultWidth);
      this.height = CustomKeyboard.a(localTypedArray1, 1, CustomKeyboard.c(this.a), paramRow.defaultHeight);
      this.gap = CustomKeyboard.a(localTypedArray1, 2, CustomKeyboard.a(this.a), paramRow.defaultHorizontalGap);
      localTypedArray1.recycle();
      TypedArray localTypedArray2 = paramResources.obtainAttributes(Xml.asAttributeSet(paramXmlResourceParser), R.styleable.Keyboard_Key);
      this.x += this.gap;
      TypedValue localTypedValue = new TypedValue();
      localTypedArray2.getValue(1, localTypedValue);
      if ((localTypedValue.type == 16) || (localTypedValue.type == 17))
      {
        int[] arrayOfInt1 = new int[1];
        arrayOfInt1[0] = localTypedValue.data;
        this.codes = arrayOfInt1;
      }
      while (true)
      {
        this.iconPreview = localTypedArray2.getDrawable(6);
        if (this.iconPreview != null)
          this.iconPreview.setBounds(0, 0, this.iconPreview.getIntrinsicWidth(), this.iconPreview.getIntrinsicHeight());
        this.repeatable = localTypedArray2.getBoolean(5, false);
        this.modifier = localTypedArray2.getBoolean(3, false);
        this.sticky = localTypedArray2.getBoolean(4, false);
        this.edgeFlags = localTypedArray2.getInt(2, 0);
        this.edgeFlags |= paramRow.rowEdgeFlags;
        this.icon = localTypedArray2.getDrawable(9);
        if (this.icon != null)
          this.icon.setBounds(0, 0, this.icon.getIntrinsicWidth(), this.icon.getIntrinsicHeight());
        this.label = localTypedArray2.getText(8);
        this.text = localTypedArray2.getText(7);
        this.background = localTypedArray2.getDrawable(10);
        if ((this.codes == null) && (!TextUtils.isEmpty(this.label)))
        {
          int[] arrayOfInt2 = new int[1];
          arrayOfInt2[0] = this.label.charAt(0);
          this.codes = arrayOfInt2;
        }
        localTypedArray2.recycle();
        return;
        if (localTypedValue.type == 3)
          this.codes = a(localTypedValue.string.toString());
      }
    }

    public Key(CustomKeyboard.Row paramRow)
    {
      this.a = CustomKeyboard.Row.a(paramRow);
      this.height = paramRow.defaultHeight;
      this.width = paramRow.defaultWidth;
      this.gap = paramRow.defaultHorizontalGap;
      this.edgeFlags = paramRow.rowEdgeFlags;
    }

    private static int[] a(String paramString)
    {
      int i = 0;
      int j;
      if (paramString.length() > 0)
      {
        int m = 0;
        j = 0;
        do
        {
          j++;
          m = paramString.indexOf(",", m + 1);
        }
        while (m > 0);
      }
      while (true)
      {
        int[] arrayOfInt = new int[j];
        StringTokenizer localStringTokenizer = new StringTokenizer(paramString, ",");
        while (true)
        {
          if (!localStringTokenizer.hasMoreTokens())
            return arrayOfInt;
          int k = i + 1;
          try
          {
            arrayOfInt[i] = Integer.parseInt(localStringTokenizer.nextToken());
            i = k;
          }
          catch (NumberFormatException localNumberFormatException)
          {
            Log.e("Keyboard", "Error parsing keycodes " + paramString);
            i = k;
          }
        }
        j = 0;
      }
    }

    public int[] getCurrentDrawableState()
    {
      int[] arrayOfInt = f;
      if (this.on)
        if (this.pressed)
          arrayOfInt = c;
      do
      {
        return arrayOfInt;
        return b;
        if (this.sticky)
        {
          if (this.pressed)
            return e;
          return d;
        }
      }
      while (!this.pressed);
      return g;
    }

    public boolean isInside(int paramInt1, int paramInt2)
    {
      int i;
      int j;
      label23: int k;
      if ((0x1 & this.edgeFlags) > 0)
      {
        i = 1;
        if ((0x2 & this.edgeFlags) <= 0)
          break label158;
        j = 1;
        if ((0x4 & this.edgeFlags) <= 0)
          break label164;
        k = 1;
        label35: if ((0x8 & this.edgeFlags) <= 0)
          break label170;
      }
      label158: label164: label170: for (int m = 1; ; m = 0)
      {
        if (((paramInt1 < this.x) && ((i == 0) || (paramInt1 > this.x + this.width))) || ((paramInt1 >= this.x + this.width) && ((j == 0) || (paramInt1 < this.x))) || ((paramInt2 < this.y) && ((k == 0) || (paramInt2 > this.y + this.height))) || ((paramInt2 >= this.y + this.height) && ((m == 0) || (paramInt2 < this.y))))
          break label176;
        return true;
        i = 0;
        break;
        j = 0;
        break label23;
        k = 0;
        break label35;
      }
      label176: return false;
    }

    public void onPressed()
    {
      if (this.pressed);
      for (boolean bool = false; ; bool = true)
      {
        this.pressed = bool;
        return;
      }
    }

    public void onReleased(boolean paramBoolean)
    {
      boolean bool1;
      boolean bool3;
      if (this.pressed)
      {
        bool1 = false;
        this.pressed = bool1;
        if (this.sticky)
        {
          boolean bool2 = this.on;
          bool3 = false;
          if (!bool2)
            break label45;
        }
      }
      while (true)
      {
        this.on = bool3;
        return;
        bool1 = true;
        break;
        label45: bool3 = true;
      }
    }

    public int squaredDistanceFrom(int paramInt1, int paramInt2)
    {
      int i = this.x + this.width / 2 - paramInt1;
      int j = this.y + this.height / 2 - paramInt2;
      return i * i + j * j;
    }
  }

  public static class Row
  {
    private CustomKeyboard a;
    public int defaultHeight;
    public int defaultHorizontalGap;
    public int defaultWidth;
    public int mode;
    public int rowEdgeFlags;
    public int verticalGap;

    public Row(Resources paramResources, CustomKeyboard paramCustomKeyboard, XmlResourceParser paramXmlResourceParser)
    {
      this.a = paramCustomKeyboard;
      TypedArray localTypedArray1 = paramResources.obtainAttributes(Xml.asAttributeSet(paramXmlResourceParser), R.styleable.Keyboard);
      this.defaultWidth = CustomKeyboard.a(localTypedArray1, 0, CustomKeyboard.a(paramCustomKeyboard), CustomKeyboard.b(paramCustomKeyboard));
      this.defaultHeight = CustomKeyboard.a(localTypedArray1, 1, CustomKeyboard.c(paramCustomKeyboard), CustomKeyboard.d(paramCustomKeyboard));
      this.defaultHorizontalGap = CustomKeyboard.a(localTypedArray1, 2, CustomKeyboard.a(paramCustomKeyboard), CustomKeyboard.e(paramCustomKeyboard));
      this.verticalGap = CustomKeyboard.a(localTypedArray1, 3, CustomKeyboard.c(paramCustomKeyboard), CustomKeyboard.f(paramCustomKeyboard));
      localTypedArray1.recycle();
      TypedArray localTypedArray2 = paramResources.obtainAttributes(Xml.asAttributeSet(paramXmlResourceParser), R.styleable.Keyboard_Row);
      this.rowEdgeFlags = localTypedArray2.getInt(0, 0);
      this.mode = localTypedArray2.getResourceId(1, 0);
    }

    public Row(CustomKeyboard paramCustomKeyboard)
    {
      this.a = paramCustomKeyboard;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.report.pricekeyboard.base.CustomKeyboard
 * JD-Core Version:    0.6.2
 */