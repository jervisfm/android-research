package gbis.gbandroid.activities.report.pricekeyboard.base;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.Region.Op;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import gbis.gbandroid.R.styleable;
import java.util.Arrays;
import java.util.List;

public class CustomKeyboardView extends View
{
  private static int N = 12;
  private static final int[] a = { -5 };
  private int A;
  private int B;
  private int C;
  private int D = -1;
  private long E;
  private long F;
  private int[] G = new int[12];
  private GestureDetector H;
  private int I = -1;
  private boolean J;
  private CustomKeyboard.Key K;
  private Rect L = new Rect(0, 0, 0, 0);
  private Drawable M;
  private int[] O = new int[N];
  private int P;
  private int Q;
  private long R;
  private boolean S;
  private boolean T;
  private Rect U = new Rect();
  private Bitmap V;
  private Canvas W;
  Handler b = new a(this);
  private CustomKeyboard c;
  private int d = -1;
  private int e;
  private int f;
  private int g;
  private float h;
  private int i;
  private CustomKeyboard.Key[] j;
  private OnKeyboardActionListener k;
  private int l;
  private int m;
  private int n;
  private int o;
  private int p;
  private int q;
  private boolean r;
  private Paint s;
  private Rect t;
  private int u = 0;
  private int v = 0;
  private int w = 0;
  private int x = 2;
  private long y;
  private long z;

  public CustomKeyboardView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 2130771976);
  }

  public CustomKeyboardView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.KeyboardView);
    int i1 = localTypedArray.getIndexCount();
    int i2 = 0;
    if (i2 >= i1)
    {
      this.u = getPaddingLeft();
      this.v = getPaddingRight();
      this.w = getPaddingTop();
      this.x = getPaddingBottom();
      this.s = new Paint();
      this.s.setAntiAlias(true);
      this.s.setTextSize(0.0F);
      this.s.setTextAlign(Paint.Align.CENTER);
      this.t = new Rect(0, 0, 0, 0);
      this.M.getPadding(this.t);
      e();
      a();
      return;
    }
    int i3 = localTypedArray.getIndex(i2);
    if (i3 == 1)
      this.M = localTypedArray.getDrawable(i3);
    while (true)
    {
      i2++;
      break;
      if (i3 == 5)
        this.l = localTypedArray.getDimensionPixelOffset(i3, 0);
      else if (i3 == 2)
        this.f = localTypedArray.getDimensionPixelSize(i3, 18);
      else if (i3 == 4)
        this.g = localTypedArray.getColor(i3, -16777216);
      else if (i3 == 3)
        this.e = localTypedArray.getDimensionPixelSize(i3, 14);
      else if (i3 == 6)
        this.i = localTypedArray.getColor(i3, -16777216);
      else if (i3 == 7)
        this.h = localTypedArray.getFloat(i3, 10.0F);
    }
  }

  private int a(int paramInt1, int paramInt2, int[] paramArrayOfInt)
  {
    CustomKeyboard.Key[] arrayOfKey = this.j;
    int i1 = -1;
    int i2 = -1;
    int i3 = 1 + this.m;
    Arrays.fill(this.O, 2147483647);
    int[] arrayOfInt = this.c.getNearestKeys(paramInt1, paramInt2);
    int i4 = arrayOfInt.length;
    int i5 = 0;
    if (i5 >= i4)
    {
      if (i1 == -1)
        i1 = i2;
      return i1;
    }
    CustomKeyboard.Key localKey = arrayOfKey[arrayOfInt[i5]];
    boolean bool1 = localKey.isInside(paramInt1, paramInt2);
    boolean bool2 = this.r;
    int i6 = 0;
    if (bool2)
    {
      i6 = localKey.squaredDistanceFrom(paramInt1, paramInt2);
      if (i6 < this.m)
        break label124;
    }
    label124: int i9;
    int i10;
    label162: int i11;
    label169: int i7;
    if ((bool1) && (localKey.codes[0] > 32))
    {
      i9 = localKey.codes.length;
      if (i6 < i3)
      {
        i10 = arrayOfInt[i5];
        i3 = i6;
        if (paramArrayOfInt != null)
        {
          i11 = 0;
          if (i11 >= this.O.length)
          {
            i7 = i3;
            i2 = i10;
          }
        }
      }
    }
    while (true)
    {
      int i8;
      if (bool1)
        i8 = arrayOfInt[i5];
      while (true)
      {
        i5++;
        i1 = i8;
        i3 = i7;
        break;
        if (this.O[i11] > i6)
        {
          System.arraycopy(this.O, i11, this.O, i11 + i9, this.O.length - i11 - i9);
          System.arraycopy(paramArrayOfInt, i11, paramArrayOfInt, i11 + i9, paramArrayOfInt.length - i11 - i9);
          for (int i12 = 0; ; i12++)
          {
            if (i12 >= i9)
            {
              i7 = i3;
              i2 = i10;
              break;
            }
            paramArrayOfInt[(i11 + i12)] = localKey.codes[i12];
            this.O[(i11 + i12)] = i6;
          }
        }
        i11++;
        break label169;
        i8 = i1;
        continue;
        i7 = i3;
        i2 = i10;
        i8 = i1;
      }
      i10 = i2;
      break label162;
      i7 = i3;
    }
  }

  private CharSequence a(CharSequence paramCharSequence)
  {
    if ((this.c.isShifted()) && (paramCharSequence != null) && (paramCharSequence.length() < 3) && (Character.isLowerCase(paramCharSequence.charAt(0))))
      paramCharSequence = paramCharSequence.toString().toUpperCase();
    return paramCharSequence;
  }

  private void a()
  {
    this.H = new GestureDetector(getContext(), new b(this));
    this.H.setIsLongpressEnabled(false);
  }

  private void a(int paramInt)
  {
    int i1 = this.d;
    this.d = paramInt;
    CustomKeyboard.Key[] arrayOfKey = this.j;
    CustomKeyboard.Key localKey;
    if (i1 != this.d)
      if ((i1 != -1) && (arrayOfKey.length > i1))
      {
        localKey = arrayOfKey[i1];
        if (this.d != -1)
          break label97;
      }
    label97: for (boolean bool = true; ; bool = false)
    {
      localKey.onReleased(bool);
      b(i1);
      if ((this.d != -1) && (arrayOfKey.length > this.d))
      {
        arrayOfKey[this.d].onPressed();
        b(this.d);
      }
      return;
    }
  }

  private void a(int paramInt1, int paramInt2, long paramLong)
  {
    int i1 = this.D;
    CustomKeyboard.Key localKey;
    if ((i1 != -1) && (i1 < this.j.length))
    {
      localKey = this.j[i1];
      if (localKey.text != null)
      {
        this.k.onText(localKey.text);
        this.k.onRelease(-1);
        this.P = i1;
        this.R = paramLong;
      }
    }
    else
    {
      return;
    }
    int i2 = localKey.codes[0];
    int[] arrayOfInt = new int[N];
    Arrays.fill(arrayOfInt, -1);
    a(paramInt1, paramInt2, arrayOfInt);
    if (this.S)
    {
      if (this.Q == -1)
        break label174;
      this.k.onKey(-5, a);
    }
    while (true)
    {
      i2 = localKey.codes[this.Q];
      this.k.onKey(i2, arrayOfInt);
      this.k.onRelease(i2);
      break;
      label174: this.Q = 0;
    }
  }

  private void a(long paramLong, int paramInt)
  {
    if (paramInt == -1);
    do
    {
      return;
      CustomKeyboard.Key localKey = this.j[paramInt];
      if (localKey.codes.length > 1)
      {
        this.S = true;
        if ((paramLong < 800L + this.R) && (paramInt == this.P))
        {
          this.Q = ((1 + this.Q) % localKey.codes.length);
          return;
        }
        this.Q = -1;
        return;
      }
    }
    while ((paramLong <= 800L + this.R) && (paramInt == this.P));
    e();
  }

  private void a(CustomKeyboard paramCustomKeyboard)
  {
    int i1 = 0;
    if (paramCustomKeyboard == null);
    CustomKeyboard.Key[] arrayOfKey;
    do
    {
      return;
      arrayOfKey = this.j;
    }
    while (arrayOfKey == null);
    int i2 = arrayOfKey.length;
    int i3 = 0;
    while (true)
    {
      if (i1 >= i2)
      {
        if ((i3 < 0) || (i2 == 0))
          break;
        this.m = ((int)(1.4F * i3 / i2));
        this.m *= this.m;
        return;
      }
      CustomKeyboard.Key localKey = arrayOfKey[i1];
      i3 += Math.min(localKey.width, localKey.height) + localKey.gap;
      i1++;
    }
  }

  private void b()
  {
    if (this.V == null)
    {
      this.V = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
      this.W = new Canvas(this.V);
      c();
    }
    Canvas localCanvas = this.W;
    localCanvas.clipRect(this.U, Region.Op.REPLACE);
    if (this.c == null)
      return;
    Paint localPaint = this.s;
    Rect localRect1 = this.L;
    Rect localRect2 = this.t;
    int i1 = this.u;
    int i2 = this.w;
    CustomKeyboard.Key[] arrayOfKey = this.j;
    CustomKeyboard.Key localKey1 = this.K;
    localPaint.setAlpha(255);
    localPaint.setColor(this.g);
    int i3 = 0;
    if (localKey1 != null)
    {
      boolean bool = localCanvas.getClipBounds(localRect1);
      i3 = 0;
      if (bool)
      {
        int i8 = -1 + (i1 + localKey1.x);
        int i9 = localRect1.left;
        i3 = 0;
        if (i8 <= i9)
        {
          int i10 = -1 + (i2 + localKey1.y);
          int i11 = localRect1.top;
          i3 = 0;
          if (i10 <= i11)
          {
            int i12 = 1 + (i1 + (localKey1.x + localKey1.width));
            int i13 = localRect1.right;
            i3 = 0;
            if (i12 >= i13)
            {
              int i14 = 1 + (i2 + (localKey1.y + localKey1.height));
              int i15 = localRect1.bottom;
              i3 = 0;
              if (i14 >= i15)
                i3 = 1;
            }
          }
        }
      }
    }
    localCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
    int i4 = arrayOfKey.length;
    int i5 = 0;
    if (i5 >= i4)
    {
      this.K = null;
      this.T = false;
      this.U.setEmpty();
      return;
    }
    CustomKeyboard.Key localKey2 = arrayOfKey[i5];
    Drawable localDrawable;
    label397: String str;
    if ((i3 == 0) || (localKey1 == localKey2))
    {
      if (localKey2.x + localKey2.width >= getWidth() - this.v)
        localKey2.width = (getWidth() - localKey2.x - this.v);
      if (localKey2.background != null)
        break label662;
      localDrawable = this.M;
      localDrawable.setState(localKey2.getCurrentDrawableState());
      if (localKey2.label != null)
        break label672;
      str = null;
      label419: Rect localRect3 = localDrawable.getBounds();
      if ((localKey2.width != localRect3.right) || (localKey2.height != localRect3.bottom))
        localDrawable.setBounds(0, 0, localKey2.width, localKey2.height);
      localCanvas.translate(i1 + localKey2.x, i2 + localKey2.y);
      localDrawable.draw(localCanvas);
      if (str == null)
        break label711;
      if ((str.length() <= 1) || (localKey2.codes.length >= 2))
        break label691;
      localPaint.setTextSize(this.e);
      localPaint.setTypeface(Typeface.DEFAULT);
      label538: localPaint.setShadowLayer(this.h, 0.0F, 0.0F, this.i);
      localCanvas.drawText(str, (localKey2.width - localRect2.left - localRect2.right) / 2 + localRect2.left, (localKey2.height - localRect2.top - localRect2.bottom) / 2 + (localPaint.getTextSize() - localPaint.descent()) / 2.0F + localRect2.top, localPaint);
      localPaint.setShadowLayer(0.0F, 0.0F, 0.0F, 0);
    }
    while (true)
    {
      localCanvas.translate(-localKey2.x - i1, -localKey2.y - i2);
      i5++;
      break;
      label662: localDrawable = localKey2.background;
      break label397;
      label672: str = a(localKey2.label).toString();
      break label419;
      label691: localPaint.setTextSize(this.f);
      localPaint.setTypeface(Typeface.DEFAULT);
      break label538;
      label711: if (localKey2.icon != null)
      {
        int i6 = (localKey2.width - localRect2.left - localRect2.right - localKey2.icon.getIntrinsicWidth()) / 2 + localRect2.left;
        int i7 = (localKey2.height - localRect2.top - localRect2.bottom - localKey2.icon.getIntrinsicHeight()) / 2 + localRect2.top;
        localCanvas.translate(i6, i7);
        localKey2.icon.setBounds(0, 0, localKey2.icon.getIntrinsicWidth(), localKey2.icon.getIntrinsicHeight());
        localKey2.icon.draw(localCanvas);
        localCanvas.translate(-i6, -i7);
      }
    }
  }

  private void b(int paramInt)
  {
    if ((paramInt < 0) || (paramInt >= this.j.length))
      return;
    CustomKeyboard.Key localKey = this.j[paramInt];
    this.K = localKey;
    this.U.union(localKey.x + this.u, localKey.y + this.w, localKey.x + localKey.width + this.u, localKey.y + localKey.height + this.w);
    b();
    invalidate(localKey.x + this.u, localKey.y + this.w, localKey.x + localKey.width + this.u, localKey.y + localKey.height + this.w);
  }

  private void c()
  {
    this.U.union(0, 0, getWidth(), getHeight());
    this.T = true;
    invalidate();
  }

  private boolean d()
  {
    CustomKeyboard.Key localKey = this.j[this.I];
    a(localKey.x, localKey.y, this.R);
    return true;
  }

  private void e()
  {
    this.P = -1;
    this.Q = 0;
    this.R = -1L;
    this.S = false;
  }

  public void closing()
  {
    this.b.removeMessages(3);
    this.b.removeMessages(4);
    this.b.removeMessages(1);
    this.V = null;
    this.W = null;
  }

  public CustomKeyboard getKeyboard()
  {
    return this.c;
  }

  protected OnKeyboardActionListener getOnKeyboardActionListener()
  {
    return this.k;
  }

  public boolean isProximityCorrectionEnabled()
  {
    return this.r;
  }

  public boolean isShifted()
  {
    if (this.c != null)
      return this.c.isShifted();
    return false;
  }

  public void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    closing();
  }

  public void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    if ((this.T) || (this.V == null))
      b();
    paramCanvas.drawBitmap(this.V, 0.0F, 0.0F, null);
  }

  protected boolean onLongPress(CustomKeyboard.Key paramKey)
  {
    return false;
  }

  public void onMeasure(int paramInt1, int paramInt2)
  {
    if (this.c == null)
    {
      setMeasuredDimension(this.u + this.v, this.w + this.x);
      return;
    }
    int i1 = this.c.getMinWidth() + this.u + this.v;
    if (View.MeasureSpec.getSize(paramInt1) < i1 + 10)
      i1 = View.MeasureSpec.getSize(paramInt1);
    setMeasuredDimension(i1, this.c.getHeight() + this.w + this.x);
  }

  public void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    this.V = null;
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    int i1 = (int)paramMotionEvent.getX() - this.u;
    int i2 = (int)paramMotionEvent.getY() + this.l - this.w;
    int i3 = paramMotionEvent.getAction();
    long l1 = paramMotionEvent.getEventTime();
    int i4 = a(i1, i2, null);
    if (this.H.onTouchEvent(paramMotionEvent))
    {
      a(-1);
      this.b.removeMessages(3);
      this.b.removeMessages(4);
      return true;
    }
    switch (i3)
    {
    default:
      this.n = i1;
      this.o = i2;
      return true;
    case 0:
      this.J = false;
      this.p = i1;
      this.q = i2;
      this.B = i1;
      this.C = i2;
      this.E = 0L;
      this.F = 0L;
      this.A = -1;
      this.D = i4;
      this.y = paramMotionEvent.getEventTime();
      this.z = this.y;
      a(l1, i4);
      OnKeyboardActionListener localOnKeyboardActionListener = this.k;
      if (i4 != -1);
      for (int i8 = this.j[i4].codes[0]; ; i8 = 0)
      {
        localOnKeyboardActionListener.onPress(i8);
        if ((this.D >= 0) && (this.j[this.D].repeatable))
        {
          this.I = this.D;
          d();
          Message localMessage3 = this.b.obtainMessage(3);
          this.b.sendMessageDelayed(localMessage3, 400L);
        }
        if (this.D != -1)
        {
          Message localMessage2 = this.b.obtainMessage(4, paramMotionEvent);
          this.b.sendMessageDelayed(localMessage2, 800L);
        }
        a(i4);
        break;
      }
    case 2:
      int i7 = 0;
      if (i4 != -1)
      {
        if (this.D != -1)
          break label442;
        this.D = i4;
        this.F = (l1 - this.y);
      }
      while (true)
      {
        if (i4 != this.I)
        {
          this.b.removeMessages(3);
          this.I = -1;
        }
        if (i7 == 0)
        {
          this.b.removeMessages(4);
          if (i4 != -1)
          {
            Message localMessage1 = this.b.obtainMessage(4, paramMotionEvent);
            this.b.sendMessageDelayed(localMessage1, 800L);
          }
        }
        a(i4);
        break;
        label442: if (i4 == this.D)
        {
          this.F += l1 - this.z;
          i7 = 1;
        }
        else
        {
          e();
          this.A = this.D;
          this.B = this.n;
          this.C = this.o;
          this.E = (l1 + this.F - this.z);
          this.D = i4;
          this.F = 0L;
          i7 = 0;
        }
      }
    case 1:
    }
    this.b.removeMessages(1);
    this.b.removeMessages(3);
    this.b.removeMessages(4);
    label583: int i6;
    int i5;
    if (i4 == this.D)
    {
      this.F += l1 - this.z;
      if ((this.F >= this.E) || (this.A == -1))
        break label723;
      this.D = this.A;
      i6 = this.B;
      i5 = this.C;
    }
    while (true)
    {
      a(-1);
      Arrays.fill(this.G, -1);
      if ((this.I == -1) && (!this.J))
        a(i6, i5, l1);
      b(i4);
      this.I = -1;
      i1 = i6;
      i2 = i5;
      break;
      e();
      this.A = this.D;
      this.E = (l1 + this.F - this.z);
      this.D = i4;
      this.F = 0L;
      break label583;
      label723: i5 = i2;
      i6 = i1;
    }
  }

  public void setKeyboard(CustomKeyboard paramCustomKeyboard)
  {
    if (this.c != null)
      a(-1);
    this.c = paramCustomKeyboard;
    List localList = this.c.getKeys();
    this.j = ((CustomKeyboard.Key[])localList.toArray(new CustomKeyboard.Key[localList.size()]));
    requestLayout();
    this.V = null;
    c();
    a(paramCustomKeyboard);
  }

  public void setOnKeyboardActionListener(OnKeyboardActionListener paramOnKeyboardActionListener)
  {
    this.k = paramOnKeyboardActionListener;
  }

  public void setProximityCorrectionEnabled(boolean paramBoolean)
  {
    this.r = paramBoolean;
  }

  public boolean setShifted(boolean paramBoolean)
  {
    if ((this.c != null) && (this.c.setShifted(paramBoolean)))
    {
      c();
      return true;
    }
    return false;
  }

  public static abstract interface OnKeyboardActionListener
  {
    public abstract void onKey(int paramInt, int[] paramArrayOfInt);

    public abstract void onPress(int paramInt);

    public abstract void onRelease(int paramInt);

    public abstract void onText(CharSequence paramCharSequence);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.report.pricekeyboard.base.CustomKeyboardView
 * JD-Core Version:    0.6.2
 */