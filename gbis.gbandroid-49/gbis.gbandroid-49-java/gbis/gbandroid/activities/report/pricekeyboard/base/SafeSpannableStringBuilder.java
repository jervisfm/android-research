package gbis.gbandroid.activities.report.pricekeyboard.base;

import android.text.Editable;
import android.text.Editable.Factory;
import android.text.SpannableStringBuilder;

public class SafeSpannableStringBuilder extends SpannableStringBuilder
{
  public SafeSpannableStringBuilder()
  {
  }

  public SafeSpannableStringBuilder(CharSequence paramCharSequence)
  {
    super(paramCharSequence);
  }

  public SafeSpannableStringBuilder(CharSequence paramCharSequence, int paramInt1, int paramInt2)
  {
    super(paramCharSequence, paramInt1, paramInt2);
  }

  public SafeSpannableStringBuilder delete(int paramInt1, int paramInt2)
  {
    return (SafeSpannableStringBuilder)super.delete(paramInt1, paramInt2);
  }

  public SafeSpannableStringBuilder insert(int paramInt, CharSequence paramCharSequence)
  {
    return replace(paramInt, paramInt, paramCharSequence, 0, paramCharSequence.length());
  }

  public SafeSpannableStringBuilder insert(int paramInt1, CharSequence paramCharSequence, int paramInt2, int paramInt3)
  {
    return replace(paramInt1, paramInt1, paramCharSequence, paramInt2, paramInt3);
  }

  public SafeSpannableStringBuilder overwrite(CharSequence paramCharSequence)
  {
    int i = length();
    CharSequence localCharSequence = null;
    if (i == 0);
    while (true)
    {
      if (localCharSequence != null)
        replace(0, length(), localCharSequence);
      if (paramCharSequence != null)
        append(paramCharSequence);
      return this;
      if (paramCharSequence.length() <= length())
      {
        localCharSequence = paramCharSequence;
        paramCharSequence = null;
      }
      else
      {
        localCharSequence = paramCharSequence.subSequence(0, 1 + length());
        paramCharSequence = paramCharSequence.subSequence(1 + length(), paramCharSequence.length());
      }
    }
  }

  public SafeSpannableStringBuilder replace(int paramInt1, int paramInt2, CharSequence paramCharSequence)
  {
    return replace(paramInt1, paramInt2, paramCharSequence, 0, paramCharSequence.length());
  }

  public SafeSpannableStringBuilder replace(int paramInt1, int paramInt2, CharSequence paramCharSequence, int paramInt3, int paramInt4)
  {
    if (paramInt2 > length());
    for (int i = length(); ; i = paramInt2)
    {
      if (paramInt4 > paramCharSequence.length());
      for (int j = paramCharSequence.length(); ; j = paramInt4)
      {
        if (paramInt1 < 0);
        for (int k = 0; ; k = paramInt1)
        {
          if (k > i)
            k = i;
          int m = 0;
          if (paramInt3 < 0);
          while (true)
          {
            if (m > j)
              m = j;
            return (SafeSpannableStringBuilder)super.replace(k, i, paramCharSequence, m, j);
            m = paramInt3;
          }
        }
      }
    }
  }

  public SafeSpannableStringBuilder replaceAllCharacters(Character paramCharacter1, Character paramCharacter2)
  {
    int i = length();
    for (int j = 0; ; j++)
    {
      if (j >= i)
        return this;
      if (paramCharacter1.equals(Character.valueOf(charAt(j))))
        replace(j, j + 1, paramCharacter2.toString());
    }
  }

  public SafeSpannableStringBuilder stripCharacter(Character paramCharacter)
  {
    int i = length();
    int j = 0;
    while (true)
    {
      if (j >= i)
        return this;
      if (paramCharacter.equals(Character.valueOf(charAt(j))))
      {
        delete(j, j + 1);
        if (j > 0)
          j--;
        i--;
      }
      else
      {
        j++;
      }
    }
  }

  public CharSequence subSequence(int paramInt1, int paramInt2)
  {
    if (paramInt2 > length());
    for (int i = length(); ; i = paramInt2)
    {
      if (i < 0)
        i = 0;
      if (paramInt1 > length());
      for (int j = length(); ; j = paramInt1)
      {
        int k = 0;
        if (j < 0);
        while (true)
        {
          if (k > i)
            k = i;
          return super.subSequence(k, i);
          k = j;
        }
      }
    }
  }

  public static class Factory extends Editable.Factory
  {
    public Editable newEditable(CharSequence paramCharSequence)
    {
      return new SafeSpannableStringBuilder(paramCharSequence);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.report.pricekeyboard.base.SafeSpannableStringBuilder
 * JD-Core Version:    0.6.2
 */