package gbis.gbandroid.activities.report;

import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import gbis.gbandroid.activities.report.pricekeyboard.PriceKeyboardDrawer;
import gbis.gbandroid.activities.report.pricekeyboard.ReportPriceKeyboardView;

final class q
  implements View.OnFocusChangeListener
{
  q(ReportPrices paramReportPrices)
  {
  }

  public final void onFocusChange(View paramView, boolean paramBoolean)
  {
    if (ReportPrices.q(this.a) == null);
    EditText localEditText;
    do
    {
      do
        return;
      while (!(paramView instanceof EditText));
      localEditText = (EditText)paramView;
      if (!paramBoolean)
        break;
      ReportPrices.q(this.a).setTarget(localEditText);
    }
    while (!ReportPrices.r(this.a));
    localEditText.setSelection(localEditText.length());
    ReportPrices.o(this.a).open();
    return;
    ReportPrices.q(this.a).setTarget(null);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.report.q
 * JD-Core Version:    0.6.2
 */