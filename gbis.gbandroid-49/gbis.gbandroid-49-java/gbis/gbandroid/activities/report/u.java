package gbis.gbandroid.activities.report;

import android.util.Log;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import gbis.gbandroid.activities.report.pricekeyboard.PriceKeyboardDrawer.OnDrawerStateChangedListener;
import gbis.gbandroid.activities.report.pricekeyboard.ReportPriceKeyboardView;

final class u
  implements PriceKeyboardDrawer.OnDrawerStateChangedListener
{
  u(ReportPrices paramReportPrices)
  {
  }

  public final void onClose()
  {
    Log.d("GasBuddy", "Closing keyboard");
    ViewGroup.LayoutParams localLayoutParams = ReportPrices.m(this.a).getLayoutParams();
    localLayoutParams.height = -1;
    ReportPrices.m(this.a).setLayoutParams(localLayoutParams);
    ReportPrices.s(this.a).invalidate();
  }

  public final void onOpen()
  {
    Log.d("GasBuddy", "Opening keyboard, fulllayoutheight=" + ReportPrices.n(this.a) + ", priceKeyboardViewH=" + ReportPrices.q(this.a).getMeasuredHeight());
    ViewGroup.LayoutParams localLayoutParams = ReportPrices.m(this.a).getLayoutParams();
    localLayoutParams.height = (ReportPrices.n(this.a).intValue() - ReportPrices.q(this.a).getMeasuredHeight());
    ReportPrices.m(this.a).setLayoutParams(localLayoutParams);
    ReportPrices.s(this.a).invalidate();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.report.u
 * JD-Core Version:    0.6.2
 */