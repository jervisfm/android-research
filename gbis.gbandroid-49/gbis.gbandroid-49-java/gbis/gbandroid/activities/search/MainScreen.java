package gbis.gbandroid.activities.search;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import gbis.gbandroid.activities.base.GBActivitySearch;
import gbis.gbandroid.activities.initial.InitialScreen;
import gbis.gbandroid.activities.members.Profile;
import gbis.gbandroid.activities.prizes.Prize;
import gbis.gbandroid.activities.settings.FeedBack;
import gbis.gbandroid.content.GBIntent;
import gbis.gbandroid.views.CustomDialog;
import gbis.gbandroid.views.CustomDialog.Builder;
import gbis.gbandroid.views.MainButton;
import gbis.gbandroid.views.layouts.DnDGridView;
import gbis.gbandroid.views.layouts.DnDGridView.DragListener;
import gbis.gbandroid.views.layouts.DnDGridView.DropListner;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;

public class MainScreen extends GBActivitySearch
  implements View.OnClickListener, View.OnLongClickListener
{
  public static final int LIKE_THIS_APP_TIMES = 4;
  private int b;
  private long c;
  private c d;
  private a e;
  private List<MainButton> f;
  private DnDGridView g;
  private int h;
  private DnDGridView.DropListner i = new a(this);
  private DnDGridView.DragListener j = new d(this);

  private MainButton a(int paramInt)
  {
    int k = this.f.size();
    for (int m = 0; ; m++)
    {
      MainButton localMainButton;
      if (m >= k)
        localMainButton = null;
      do
      {
        return localMainButton;
        localMainButton = (MainButton)this.f.get(m);
      }
      while (localMainButton.getButtonId() == paramInt);
    }
  }

  private void a()
  {
    autoComplete();
    if (!this.memberId.equals(""))
      c();
    long l = this.mPrefs.getLong("initial_screen_date", 0L);
    if ((!this.mPrefs.getBoolean("is_member", false)) && (Calendar.getInstance().getTimeInMillis() - l >= 2592000000L))
    {
      if (!this.memberId.equals(""))
        break label93;
      r();
    }
    while (true)
    {
      ((ImageView)findViewById(2131165242)).setFocusableInTouchMode(true);
      return;
      label93: SharedPreferences.Editor localEditor = this.mPrefs.edit();
      localEditor.putBoolean("is_member", true);
      localEditor.commit();
    }
  }

  private void b()
  {
    this.e.remove(a(5));
    this.e.remove(a(6));
    this.e.remove(a(4));
    MainButton localMainButton = new MainButton(this.mRes.getDrawable(2130837538), getString(2131296621), 3);
    this.f.add(localMainButton);
    h();
  }

  private void c()
  {
    this.e.remove(a(3));
    MainButton localMainButton1 = new MainButton(this.mRes.getDrawable(2130837538), getString(2131296620), 4);
    this.f.add(localMainButton1);
    MainButton localMainButton2 = new MainButton(this.mRes.getDrawable(2130837539), getString(2131296619), 5);
    this.f.add(localMainButton2);
    MainButton localMainButton3 = new MainButton(this.mRes.getDrawable(2130837547), getString(2131296622), 6);
    this.f.add(localMainButton3);
    h();
  }

  private void d()
  {
    this.g = ((DnDGridView)findViewById(2131165449));
    this.g.setDropListener(this.i);
    this.g.setDragListener(this.j);
    this.f = new ArrayList();
    this.e = new a(this, this.f);
    this.g.setAdapter(this.e);
    this.buttonNearMe = findViewById(2131165450);
    TextView localTextView = (TextView)this.buttonNearMe.findViewById(2131165441);
    ((ImageView)this.buttonNearMe.findViewById(2131165440)).setImageResource(2130837531);
    localTextView.setText(getString(2131296617));
    localTextView.setTextSize(14.0F);
    this.buttonNearMe.setBackgroundDrawable(null);
    MainButton localMainButton1 = new MainButton(this.mRes.getDrawable(2130837550), getString(2131296618), 2);
    this.f.add(localMainButton1);
    MainButton localMainButton2 = new MainButton(this.mRes.getDrawable(2130837538), getString(2131296621), 3);
    this.f.add(localMainButton2);
    h();
    resizeElements(this.mRes.getConfiguration());
  }

  private void e()
  {
    if (this.mRes.getConfiguration().orientation != 2)
    {
      if (this.memberId.equals(""))
      {
        this.g.setNumColumns(2);
        return;
      }
      this.g.setNumColumns(4);
      return;
    }
    this.g.setNumColumns(1);
    this.g.setStretchMode(2);
  }

  private int[] f()
  {
    int k = 0;
    String str;
    int[] arrayOfInt;
    int m;
    if (this.memberId.equals(""))
    {
      str = this.mPrefs.getString("buttonOrderLoggedOut", "1|2|3");
      arrayOfInt = new int[(1 + str.length()) / 2];
      m = str.length();
    }
    for (int n = 0; ; n++)
    {
      if (n >= m)
      {
        return arrayOfInt;
        str = this.mPrefs.getString("buttonOrderLoggedIn", "1|2|4|5|6|7");
        break;
      }
      if (str.charAt(n) != '|')
      {
        arrayOfInt[k] = Integer.parseInt(String.valueOf(str.charAt(n)));
        k++;
      }
    }
  }

  private void g()
  {
    SharedPreferences.Editor localEditor = this.mPrefs.edit();
    String str1;
    int k;
    String str2;
    if (this.memberId.equals(""))
    {
      str1 = "buttonOrderLoggedOut";
      k = -1 + this.e.getCount();
      str2 = "";
    }
    for (int m = 0; ; m++)
    {
      if (m >= k)
      {
        localEditor.putString(str1, str2.concat(((MainButton)this.e.getItem(m)).getButtonId()));
        localEditor.commit();
        return;
        str1 = "buttonOrderLoggedIn";
        break;
      }
      str2 = str2.concat(((MainButton)this.e.getItem(m)).getButtonId() + "|");
    }
  }

  private void h()
  {
    b localb = new b(f());
    this.e.sort(localb);
  }

  private void i()
  {
    this.b = this.mPrefs.getInt("like_app", 0);
    this.c = this.mPrefs.getLong("like_app_date", 0L);
    if (this.c < Calendar.getInstance().getTimeInMillis() - 86400000L)
    {
      this.b = (1 + this.b);
      SharedPreferences.Editor localEditor = this.mPrefs.edit();
      localEditor.putInt("like_app", this.b);
      localEditor.putLong("like_app_date", Calendar.getInstance().getTimeInMillis());
      localEditor.commit();
      if (this.b == 4)
        n();
    }
  }

  private void j()
  {
    int k = this.mPrefs.getInt("share_app", 0);
    if (this.mPrefs.getLong("share_app_date", 0L) < Calendar.getInstance().getTimeInMillis() - 86400000L)
    {
      int m = k + 1;
      if ((m == 1) && (this.b > 4))
        m = 6;
      if (m == 7)
        p();
      SharedPreferences.Editor localEditor = this.mPrefs.edit();
      localEditor.putInt("share_app", m);
      localEditor.putLong("share_app_date", Calendar.getInstance().getTimeInMillis());
      localEditor.commit();
    }
  }

  private void k()
  {
    for (int k = 0; ; k++)
    {
      if (k >= this.f.size())
        return;
      MainButton localMainButton = (MainButton)this.f.get(k);
      if (localMainButton.getButtonId() == 4)
      {
        localMainButton.setBadgeDisplayed(this.mPrefs.getBoolean("awards_badge", false));
        this.g.invalidateViews();
        return;
      }
    }
  }

  private void l()
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this, this.mTracker);
    localBuilder.setPositiveButton(getString(2131296666), new e(this));
    localBuilder.setNegativeButton(getString(2131296667), new f(this));
    localBuilder.setTitle(getString(2131296542));
    localBuilder.setMessage(2131296557);
    CustomDialog localCustomDialog = localBuilder.create();
    ((TextView)localCustomDialog.findViewById(2131165234)).setTextSize(20.0F);
    localCustomDialog.show();
  }

  private void m()
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this, this.mTracker);
    localBuilder.setPositiveButton(getString(2131296666), new g(this));
    localBuilder.setNegativeButton(getString(2131296667), new h(this));
    localBuilder.setTitle(getString(2131296543));
    localBuilder.setMessage(2131296558);
    CustomDialog localCustomDialog = localBuilder.create();
    ((TextView)localCustomDialog.findViewById(2131165234)).setTextSize(20.0F);
    localCustomDialog.show();
  }

  private void n()
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this, this.mTracker);
    localBuilder.setPositiveButton(getString(2131296666), new i(this));
    localBuilder.setNegativeButton(getString(2131296667), new j(this));
    localBuilder.setTitle(getString(2131296541));
    localBuilder.setMessage(2131296555);
    CustomDialog localCustomDialog = localBuilder.create();
    ((TextView)localCustomDialog.findViewById(2131165234)).setTextSize(20.0F);
    localCustomDialog.show();
  }

  private void o()
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this, this.mTracker);
    localBuilder.setNegativeButton(getString(2131296664), new k(this));
    localBuilder.setTitle(getString(2131296533));
    localBuilder.setMessage(2131296406);
    localBuilder.create().show();
  }

  private void p()
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this, this.mTracker);
    localBuilder.setPositiveButton(getString(2131296666), new b(this));
    localBuilder.setNegativeButton(getString(2131296667), new c(this));
    localBuilder.setTitle(getString(2131296552));
    localBuilder.setMessage(2131296556);
    CustomDialog localCustomDialog = localBuilder.create();
    ((TextView)localCustomDialog.findViewById(2131165234)).setTextSize(20.0F);
    localCustomDialog.show();
  }

  private void q()
  {
    try
    {
      startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=gbis.gbandroid")));
      return;
    }
    catch (NullPointerException localNullPointerException)
    {
      localNullPointerException.printStackTrace();
    }
  }

  private void r()
  {
    startActivity(new GBIntent(this, InitialScreen.class, this.myLocation));
  }

  private void s()
  {
    startActivityForResult(new GBIntent(this, Profile.class, this.myLocation), 1);
  }

  private void t()
  {
    startActivityForResult(new GBIntent(this, Prize.class, this.myLocation), 1);
  }

  private void u()
  {
    startActivity(new GBIntent(this, FeedBack.class, this.myLocation));
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    switch (paramInt1)
    {
    default:
    case 1:
    }
    do
      return;
    while (paramInt2 != 9);
    showMessage(getString(2131296257));
  }

  public void onClick(View paramView)
  {
    resetFocus();
    String str = "";
    switch (paramView.getId())
    {
    default:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    }
    while (true)
    {
      setAnalyticsTrackEventScreenButton(str);
      return;
      str = getString(2131296617);
      startNearMeThread();
      continue;
      str = getString(2131296618);
      launchSettings();
      continue;
      str = getString(2131296621);
      showLogin();
      continue;
      str = getString(2131296620);
      s();
      continue;
      str = getString(2131296619);
      showFavourites();
      continue;
      str = getString(2131296622);
      t();
    }
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    ArrayAdapter localArrayAdapter = (ArrayAdapter)this.searchCityZip.getAdapter();
    String str = this.searchCityZip.getText().toString();
    boolean bool = this.searchCityZip.hasFocus();
    if (bool)
      resetFocus();
    ((RelativeLayout)findViewById(2131165350)).removeAllViews();
    setContentView(2130903113);
    d();
    populateButtons();
    a();
    setAutocompleteWithQuickLinks();
    e();
    this.searchCityZip.setAdapter(localArrayAdapter);
    if (bool)
    {
      this.searchCityZip.setText(str);
      this.searchCityZip.requestFocus();
      ((InputMethodManager)getSystemService("input_method")).showSoftInput(this.searchCityZip, 0);
    }
    if (this.adBanner != null)
      ((RelativeLayout)findViewById(2131165350)).addView(this.adBanner);
    k();
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.d = new c((byte)0);
    getPreferences();
    setContentView(2130903113);
    d();
    populateButtons();
    if (!checkConnection())
    {
      o();
      return;
    }
    a();
    i();
    j();
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    super.onCreateOptionsMenu(paramMenu);
    getMenuInflater().inflate(2131427331, paramMenu);
    return true;
  }

  public void onDestroy()
  {
    super.onDestroy();
    try
    {
      this.mPrefs.unregisterOnSharedPreferenceChangeListener(this.d);
      return;
    }
    catch (Exception localException)
    {
    }
  }

  public boolean onLongClick(View paramView)
  {
    this.g.startDrag(paramView);
    return true;
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    super.onOptionsItemSelected(paramMenuItem);
    setAnalyticsTrackEvent("Click", "MenuButton", paramMenuItem.getTitle().toString(), 0);
    switch (paramMenuItem.getItemId())
    {
    default:
      return true;
    case 2131165585:
    }
    showShare();
    return true;
  }

  public void onResume()
  {
    super.onResume();
    if (this.screenPreference.equals("home"))
      getPreferences();
    e();
    k();
  }

  public boolean onSearchRequested()
  {
    return true;
  }

  public void onStart()
  {
    super.onStart();
    this.mPrefs.registerOnSharedPreferenceChangeListener(this.d);
  }

  protected void populateButtons()
  {
    super.populateButtons();
    if (this.mRes.getConfiguration().orientation == 2)
    {
      this.searchCityZip.setHint(getString(2131296427).replace(":", ""));
      DisplayMetrics localDisplayMetrics = new DisplayMetrics();
      getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
      int k = localDisplayMetrics.widthPixels;
      if (k < 480)
        findViewById(2131165452).getLayoutParams().width = (k - 120);
      return;
    }
    this.searchCityZip.setHint(getString(2131296721));
  }

  public void resizeElements(Configuration paramConfiguration)
  {
    if (paramConfiguration.orientation == 1)
    {
      RelativeLayout.LayoutParams localLayoutParams = (RelativeLayout.LayoutParams)((ImageView)findViewById(2131165242)).getLayoutParams();
      localLayoutParams.height = this.mRes.getDrawable(2130837585).getIntrinsicHeight();
      localLayoutParams.width = -2;
      ((ImageView)findViewById(2131165242)).setLayoutParams(localLayoutParams);
      ((RelativeLayout)findViewById(2131165241)).setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
    }
  }

  protected void setAdsAfterGPSServiceConnected()
  {
    int k = this.mPrefs.getInt("adMain", 0);
    setAdConfiguration(this.mPrefs.getString("adMainKey", ""), this.mPrefs.getString("adMainUnit", ""), k);
  }

  protected String setAnalyticsPageName()
  {
    return getString(2131296813);
  }

  private final class a extends ArrayAdapter<MainButton>
  {
    private List<MainButton> b;
    private int c;
    private double d;

    public a(int arg2)
    {
      super(2130903112, localList);
      this.b = localList;
      this.c = 2130903112;
      this.d = MainScreen.d(MainScreen.this);
    }

    public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      MainScreen.d locald1;
      MainButton localMainButton;
      if (paramView == null)
      {
        paramView = MainScreen.e(MainScreen.this).inflate(this.c, null);
        MainScreen.d locald2 = new MainScreen.d();
        locald2.a = ((TextView)paramView.findViewById(2131165441));
        locald2.b = ((ImageView)paramView.findViewById(2131165440));
        locald2.c = ((ImageView)paramView.findViewById(2131165442));
        if (MainScreen.f(MainScreen.this).getConfiguration().orientation == 2)
        {
          paramView.setBackgroundResource(2130837627);
          paramView.setLayoutParams(new AbsListView.LayoutParams(-1, (int)(46.0D * this.d)));
          RelativeLayout.LayoutParams localLayoutParams1 = (RelativeLayout.LayoutParams)locald2.a.getLayoutParams();
          RelativeLayout.LayoutParams localLayoutParams2 = (RelativeLayout.LayoutParams)locald2.b.getLayoutParams();
          localLayoutParams1.addRule(3, 0);
          localLayoutParams1.addRule(14, 0);
          localLayoutParams1.addRule(1, 2131165440);
          localLayoutParams1.addRule(15, -1);
          localLayoutParams2.height = ((int)(35.0D * this.d));
          localLayoutParams2.addRule(14, 0);
          localLayoutParams2.addRule(15, -1);
          localLayoutParams2.addRule(9, -1);
          locald2.a.setTextSize(20.0F);
          paramView.setPadding(0, (int)(5.0D * this.d), 0, (int)(5.0D * this.d));
        }
        paramView.setTag(locald2);
        locald1 = locald2;
        localMainButton = (MainButton)this.b.get(paramInt);
        if (localMainButton != null)
        {
          locald1.a.setText(localMainButton.getLabel());
          locald1.b.setImageDrawable(localMainButton.getIcon());
          if (!localMainButton.isBadgeDisplayed())
            break label414;
          if (localMainButton.getBadge() == null)
            break label401;
          locald1.c.setImageDrawable(localMainButton.getBadge());
          label329: locald1.c.setVisibility(0);
        }
      }
      while (true)
      {
        paramView.setId(localMainButton.getButtonId());
        paramView.setOnClickListener(MainScreen.this);
        paramView.setOnLongClickListener(MainScreen.this);
        if (localMainButton.getButtonId() != MainScreen.g(MainScreen.this))
          break label427;
        paramView.clearAnimation();
        paramView.setVisibility(4);
        return paramView;
        locald1 = (MainScreen.d)paramView.getTag();
        break;
        label401: locald1.c.setImageResource(2130837603);
        break label329;
        label414: locald1.c.setVisibility(8);
      }
      label427: if (MainScreen.g(MainScreen.this) != 0)
        paramView.startAnimation(AnimationUtils.loadAnimation(MainScreen.this, 2130968577));
      paramView.setVisibility(0);
      return paramView;
    }
  }

  private final class b
    implements Comparator<MainButton>
  {
    private int[] b;

    public b(int[] arg2)
    {
      Object localObject;
      this.b = localObject;
    }

    private int a(int paramInt)
    {
      int i = this.b.length;
      for (int j = 0; ; j++)
      {
        if (j >= i)
          j = 0;
        while (this.b[j] == paramInt)
          return j;
      }
    }

    private int a(MainButton paramMainButton1, MainButton paramMainButton2)
    {
      int i = a(paramMainButton1.getButtonId());
      int j = a(paramMainButton2.getButtonId());
      return Integer.valueOf(i).compareTo(Integer.valueOf(j));
    }
  }

  private final class c
    implements SharedPreferences.OnSharedPreferenceChangeListener
  {
    private c()
    {
    }

    public final void onSharedPreferenceChanged(SharedPreferences paramSharedPreferences, String paramString)
    {
      if (paramString.equals("member_id"))
      {
        MainScreen.h(MainScreen.this);
        MainScreen.i(MainScreen.this);
        MainScreen.j(MainScreen.this);
        if (!MainScreen.k(MainScreen.this).equals(""))
          MainScreen.l(MainScreen.this);
      }
      else
      {
        return;
      }
      MainScreen.m(MainScreen.this);
    }
  }

  static final class d
  {
    TextView a;
    ImageView b;
    ImageView c;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.search.MainScreen
 * JD-Core Version:    0.6.2
 */