package gbis.gbandroid.activities.search;

import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import gbis.gbandroid.activities.base.GBActivitySearch;
import gbis.gbandroid.views.CustomToast;

public class SearchBar extends GBActivitySearch
{
  private void a()
  {
    this.buttonSearch = ((Button)findViewById(2131165446));
    ((RelativeLayout)findViewById(2131165512)).setOnClickListener(new l(this));
  }

  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
  {
    if (paramMotionEvent.getAction() == 4)
    {
      setResult(0);
      finish();
      return true;
    }
    return super.dispatchTouchEvent(paramMotionEvent);
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    WindowManager.LayoutParams localLayoutParams = getWindow().getAttributes();
    localLayoutParams.windowAnimations = 17432577;
    localLayoutParams.dimAmount = 0.5F;
    getWindow().addFlags(262144);
    getWindow().addFlags(32);
    getWindow().addFlags(2);
    getWindow().setAttributes(localLayoutParams);
    DisplayMetrics localDisplayMetrics = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
    setContentView(new a(this));
    a();
    populateButtons();
    autoComplete();
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    switch (paramInt)
    {
    default:
      return super.onKeyDown(paramInt, paramKeyEvent);
    case 4:
    }
    setResult(0);
    finish();
    return false;
  }

  public boolean onSearchRequested()
  {
    setResult(0);
    finish();
    return true;
  }

  protected void setAdsAfterGPSServiceConnected()
  {
  }

  protected String setAnalyticsPageName()
  {
    return getString(2131296814);
  }

  public void showMessage()
  {
    if ((this.userMessage != null) && (!this.userMessage.equals("")))
    {
      CustomToast localCustomToast = new CustomToast(this, this.userMessage, 1);
      localCustomToast.setGravity(49, 0, ((RelativeLayout.LayoutParams)((RelativeLayout)findViewById(2131165513)).getLayoutParams()).height);
      localCustomToast.show();
      setMessage("");
    }
  }

  private final class a extends RelativeLayout
  {
    public a(Context arg2)
    {
      super();
      LayoutInflater.from(localContext).inflate(2130903121, this, true);
    }

    protected final void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
      super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
      InputMethodManager localInputMethodManager = (InputMethodManager)SearchBar.this.getSystemService("input_method");
      if ((paramInt4 != 0) && (paramInt4 < paramInt2) && (paramInt2 - paramInt4 > 100) && (!localInputMethodManager.isFullscreenMode()) && (!SearchBar.a(SearchBar.this)))
      {
        SearchBar.this.setResult(0);
        SearchBar.this.finish();
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.search.SearchBar
 * JD-Core Version:    0.6.2
 */