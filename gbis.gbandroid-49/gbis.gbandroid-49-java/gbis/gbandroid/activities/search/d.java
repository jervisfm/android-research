package gbis.gbandroid.activities.search;

import gbis.gbandroid.views.MainButton;
import gbis.gbandroid.views.layouts.DnDGridView.DragListener;

final class d
  implements DnDGridView.DragListener
{
  d(MainScreen paramMainScreen)
  {
  }

  public final void drag(int paramInt1, int paramInt2)
  {
    if ((paramInt2 < MainScreen.a(this.a).getCount()) && (paramInt1 < MainScreen.a(this.a).getCount()) && (paramInt2 >= 0) && (paramInt1 >= 0))
    {
      MainButton localMainButton = (MainButton)MainScreen.a(this.a).getItem(paramInt1);
      MainScreen.a(this.a).remove(localMainButton);
      MainScreen.a(this.a).insert(localMainButton, paramInt2);
      MainScreen.a(this.a, localMainButton.getButtonId());
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.search.d
 * JD-Core Version:    0.6.2
 */