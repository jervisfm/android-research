package gbis.gbandroid.activities.settings;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import gbis.gbandroid.activities.base.GBActivity;
import gbis.gbandroid.activities.base.GBActivityDialog;
import gbis.gbandroid.queries.FeedBackQuery;
import gbis.gbandroid.utils.Base64;
import gbis.gbandroid.utils.CustomAsyncTask;
import gbis.gbandroid.utils.VerifyFields;
import java.lang.reflect.Type;

public class FeedBack extends GBActivityDialog
  implements View.OnClickListener
{
  private String a;
  private EditText b;
  private EditText c;

  private void a()
  {
    this.b = ((EditText)findViewById(2131165249));
    this.c = ((EditText)findViewById(2131165250));
    if (this.a.equals(""))
      ((TextView)findViewById(2131165248)).setText(getString(2131296570));
    while (true)
    {
      this.b.setOnFocusChangeListener(new a(this));
      if (!this.a.equals(""))
        this.c.requestFocus();
      return;
      this.b.setText(this.a);
      this.b.setEnabled(false);
    }
  }

  private void a(String paramString1, String paramString2)
  {
    a locala = new a(this, paramString1, paramString2);
    locala.execute(new Object[0]);
    loadDialog(getString(2131296397), locala);
  }

  private void b()
  {
    this.a = this.mPrefs.getString("member_id", "");
  }

  private void b(String paramString1, String paramString2)
  {
    Type localType = new b(this).getType();
    this.mResponseObject = new FeedBackQuery(this, this.mPrefs, localType, this.myLocation).getResponseObject(paramString1, Base64.encodeBytes(paramString2.getBytes()));
  }

  public void onClick(View paramView)
  {
    if (paramView == getGBActivityDialogPositiveButton())
    {
      if ((this.a.equals("")) && (!VerifyFields.checkEmail(this.b.getText().toString())))
        showMessage(getString(2131296417));
    }
    else
      return;
    if (this.c.getText().length() > 1)
    {
      String str = this.b.getText().toString();
      if (str.equals(this.a))
        str = "";
      a(str, this.c.getText().toString());
      return;
    }
    showMessage(getString(2131296362));
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    b();
    a();
  }

  protected void setAdsAfterGPSServiceConnected()
  {
  }

  protected String setAnalyticsPageName()
  {
    return getString(2131296821);
  }

  protected void setGBActivityDialogButtons()
  {
    setGBViewInContentLayout(2130903058, false);
    setGBActivityDialogTitle(getString(2131296532));
    setGBActivityDialogPositiveButton(2131296634, this);
  }

  private final class a extends CustomAsyncTask
  {
    private String b;
    private String c;

    public a(GBActivity paramString1, String paramString2, String arg4)
    {
      super();
      this.b = paramString2;
      Object localObject;
      this.c = localObject;
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      try
      {
        FeedBack.a(FeedBack.this).dismiss();
        label15: FeedBack.this.finish();
        return;
      }
      catch (Exception localException)
      {
        break label15;
      }
    }

    protected final boolean queryWebService()
    {
      FeedBack.a(FeedBack.this, this.b, this.c);
      return true;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.settings.FeedBack
 * JD-Core Version:    0.6.2
 */