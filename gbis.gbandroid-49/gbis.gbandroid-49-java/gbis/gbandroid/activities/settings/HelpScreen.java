package gbis.gbandroid.activities.settings;

import gbis.gbandroid.activities.base.GBWebViewScreen;

public class HelpScreen extends GBWebViewScreen
{
  protected String getUrl()
  {
    return getString(2131296779);
  }

  protected void setAdsAfterGPSServiceConnected()
  {
  }

  protected String setAnalyticsPageName()
  {
    return getString(2131296817);
  }

  protected void setGBActivityDialogButtons()
  {
    setGBViewInContentLayout(2130903076, false);
    setGBActivityDialogTitle(getString(2131296520));
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.settings.HelpScreen
 * JD-Core Version:    0.6.2
 */