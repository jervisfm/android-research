package gbis.gbandroid.activities.settings;

import gbis.gbandroid.activities.base.GBWebViewScreen;

public class PrivacyPolicy extends GBWebViewScreen
{
  protected String getUrl()
  {
    return getString(2131296780);
  }

  protected void setAdsAfterGPSServiceConnected()
  {
  }

  protected String setAnalyticsPageName()
  {
    return getString(2131296818);
  }

  protected void setGBActivityDialogButtons()
  {
    setGBViewInContentLayout(2130903076, false);
    setGBActivityDialogTitle(getString(2131296521));
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.settings.PrivacyPolicy
 * JD-Core Version:    0.6.2
 */