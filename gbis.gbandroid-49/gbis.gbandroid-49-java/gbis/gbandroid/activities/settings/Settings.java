package gbis.gbandroid.activities.settings;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceGroup;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import gbis.gbandroid.activities.base.GBActivity;
import gbis.gbandroid.activities.base.GBPreferenceActivity;
import gbis.gbandroid.activities.members.MemberLogin;
import gbis.gbandroid.content.GBIntent;
import gbis.gbandroid.entities.requests.RequestMemberPreference;
import gbis.gbandroid.queries.MemberPreferenceQuery;
import gbis.gbandroid.utils.CustomAsyncTask;
import gbis.gbandroid.views.CustomDialog.Builder;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class Settings extends GBPreferenceActivity
  implements SharedPreferences.OnSharedPreferenceChangeListener, Preference.OnPreferenceChangeListener, Preference.OnPreferenceClickListener
{
  public static final String AWARDS_BADGE = "awards_badge";
  public static final String AWARDS_CACHED_IMAGES_TIME = "awards_cached_images_time";
  public static final String SOCIAL_MEDIA_TOKEN_EXISTS_FACEBOOK = "social_media_token_exists_facebook";
  public static final String SOCIAL_MEDIA_TOKEN_EXISTS_TWITTER = "social_media_token_exists_twitter";
  private boolean a;
  private Dialog b;
  private String c;
  private Preference d;
  private CheckBoxPreference e;
  private Preference f;
  private Preference g;
  private Preference h;
  private Preference i;
  private Preference j;
  private Preference k;
  private Preference l;
  private Preference m;
  private CheckBoxPreference n;

  private void a()
  {
    this.d = findPreference("loginButton");
    this.e = ((CheckBoxPreference)findPreference("noPricesPreference"));
    this.f = findPreference("fuelPreference");
    this.g = findPreference("distancePreference");
    this.h = findPreference("screenPreference");
    this.i = findPreference("aboutButton");
    this.j = findPreference("supportButton");
    this.k = findPreference("helpButton");
    this.l = findPreference("shareButton");
    this.m = findPreference("rateButton");
    this.n = ((CheckBoxPreference)findPreference("smartPromptPreference"));
  }

  private void b()
  {
    this.f.setOnPreferenceClickListener(this);
    this.g.setOnPreferenceClickListener(this);
    this.d.setOnPreferenceClickListener(this);
    this.h.setOnPreferenceClickListener(this);
    this.i.setOnPreferenceClickListener(this);
    this.j.setOnPreferenceClickListener(this);
    this.k.setOnPreferenceClickListener(this);
    this.l.setOnPreferenceClickListener(this);
    this.m.setOnPreferenceClickListener(this);
    this.e.setOnPreferenceChangeListener(this);
    this.n.setOnPreferenceChangeListener(this);
  }

  private void c()
  {
    MemberLogin.logout(this);
  }

  private void d()
  {
    this.d.setSummary(getString(2131296477));
    this.d.setTitle(getString(2131296476));
    this.a = true;
  }

  private void e()
  {
    Preference localPreference = this.d;
    String str = this.res.getString(2131296479);
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = this.c;
    localPreference.setSummary(String.format(str, arrayOfObject));
    this.d.setTitle(getString(2131296478));
    this.a = false;
  }

  private void f()
  {
    startActivity(new GBIntent(this, MemberLogin.class, this.myLocation));
  }

  private void g()
  {
    startActivity(new GBIntent(this, FeedBack.class, this.myLocation));
  }

  private void h()
  {
    startActivity(new GBIntent(this, ShareApp.class, this.myLocation));
  }

  private void i()
  {
    startActivity(new GBIntent(this, PrivacyPolicy.class, this.myLocation));
  }

  private void j()
  {
    ((PreferenceGroup)findPreference("GbAppCategory")).removePreference(findPreference("customKeyboardPreference"));
  }

  private void k()
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this);
    View localView = this.mInflater.inflate(2130903056, null);
    Button localButton1 = (Button)localView.findViewById(2131165245);
    Button localButton2 = (Button)localView.findViewById(2131165244);
    TextView localTextView = (TextView)localView.findViewById(2131165243);
    localButton1.setOnClickListener(new c(this));
    localButton2.setOnClickListener(new e(this));
    String str = getString(2131296559);
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = GBActivity.getVersion(this);
    localTextView.setText(String.format(str, arrayOfObject));
    localBuilder.setTitle(getString(2131296522));
    localBuilder.setContentView(localView);
    localBuilder.setPositiveButton(getString(2131296664), new f(this));
    this.b = localBuilder.create();
    this.b.show();
  }

  private void l()
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this);
    localBuilder.setNegativeButton(getString(2131296667), new g(this));
    localBuilder.setPositiveButton(getString(2131296664), new h(this));
    localBuilder.setTitle(getString(2131296547));
    localBuilder.setMessage(2131296407);
    this.b = localBuilder.create();
    this.b.setOnCancelListener(new i(this));
    this.b.show();
  }

  private void m()
  {
    startActivity(new GBIntent(this, HelpScreen.class, this.myLocation));
  }

  private void n()
  {
    try
    {
      if (this.mPrefs.getInt("like_app", 0) < 4)
      {
        SharedPreferences.Editor localEditor = this.mPrefs.edit();
        localEditor.putInt("like_app", 4);
        localEditor.putLong("like_app_date", Calendar.getInstance().getTimeInMillis());
        localEditor.commit();
      }
      startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=gbis.gbandroid")));
      return;
    }
    catch (NullPointerException localNullPointerException)
    {
      localNullPointerException.printStackTrace();
    }
  }

  private void o()
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this);
    localBuilder.setNegativeButton(getString(2131296667), new j(this));
    localBuilder.setPositiveButton(getString(2131296666), new k(this));
    localBuilder.setTitle(getString(2131296548));
    localBuilder.setMessage(2131296408);
    this.b = localBuilder.create();
    this.b.setOnCancelListener(new l(this));
    this.b.show();
  }

  private void p()
  {
    this.c = this.mPrefs.getString("member_id", "");
  }

  private void q()
  {
    new a(this).execute(new Object[0]);
  }

  private void r()
  {
    Type localType = new d(this).getType();
    this.mResponseObject = new MemberPreferenceQuery(this, this.mPrefs, localType, this.myLocation).getResponseObject(s());
  }

  private List<RequestMemberPreference> s()
  {
    ArrayList localArrayList = new ArrayList();
    RequestMemberPreference localRequestMemberPreference = new RequestMemberPreference();
    localRequestMemberPreference.setName("SmartPrompt");
    localRequestMemberPreference.setValue(this.mPrefs.getBoolean("smartPromptPreference", true));
    localArrayList.add(localRequestMemberPreference);
    return localArrayList;
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    addPreferencesFromResource(2131034114);
    this.a = true;
    a();
    p();
    if (!this.c.equals(""))
      e();
    b();
    Dialog localDialog = (Dialog)getLastNonConfigurationInstance();
    if (localDialog != null)
    {
      this.b = localDialog;
      this.b.show();
    }
    if (this.mPrefs.getBoolean("largeScreenBoolean", true))
      j();
  }

  public void onDestroy()
  {
    super.onDestroy();
    this.mPrefs.unregisterOnSharedPreferenceChangeListener(this);
  }

  public boolean onPreferenceChange(Preference paramPreference, Object paramObject)
  {
    setAnalyticsTrackEventScreenButton(paramPreference.getTitle().toString());
    if (paramPreference == this.e)
    {
      if (this.mPrefs.getBoolean("noPricesPreference", true))
        l();
      return true;
    }
    if (paramPreference == this.n)
    {
      if (this.mPrefs.getBoolean("smartPromptPreference", true))
      {
        o();
        return true;
      }
      q();
      return true;
    }
    return false;
  }

  public boolean onPreferenceClick(Preference paramPreference)
  {
    setAnalyticsTrackEventScreenButton(paramPreference.getTitle().toString());
    if (paramPreference == this.f)
    {
      showListDialog(this.res.getStringArray(2131099648), this.res.getStringArray(2131099649), "fuelPreference", getString(2131296526));
      return true;
    }
    if (paramPreference == this.g)
    {
      showListDialog(this.res.getStringArray(2131099650), this.res.getStringArray(2131099651), "distancePreference", getString(2131296527));
      return true;
    }
    if (paramPreference == this.d)
    {
      if (this.a)
        f();
      while (true)
      {
        return true;
        c();
      }
    }
    if (paramPreference == this.h)
    {
      String[] arrayOfString1 = this.res.getStringArray(2131099656);
      String[] arrayOfString2 = this.res.getStringArray(2131099657);
      if (this.c.equals(""))
      {
        ArrayList localArrayList1 = new ArrayList(Arrays.asList(arrayOfString1));
        int i1 = localArrayList1.indexOf(getString(2131296486));
        localArrayList1.remove(i1);
        arrayOfString1 = (String[])localArrayList1.toArray(new String[0]);
        ArrayList localArrayList2 = new ArrayList(Arrays.asList(arrayOfString2));
        localArrayList2.remove(i1);
        arrayOfString2 = (String[])localArrayList2.toArray(new String[0]);
      }
      showListDialog(arrayOfString1, arrayOfString2, "screenPreference", getString(2131296529));
      return true;
    }
    if (paramPreference == this.i)
    {
      k();
      return true;
    }
    if (paramPreference == this.j)
    {
      g();
      return true;
    }
    if (paramPreference == this.k)
    {
      m();
      return true;
    }
    if (paramPreference == this.l)
      h();
    while (true)
    {
      return false;
      if (paramPreference == this.m)
        n();
    }
  }

  public void onResume()
  {
    super.onResume();
    this.mPrefs.registerOnSharedPreferenceChangeListener(this);
  }

  public Object onRetainNonConfigurationInstance()
  {
    Dialog localDialog1 = this.b;
    Dialog localDialog2 = null;
    if (localDialog1 != null)
    {
      boolean bool = this.b.isShowing();
      localDialog2 = null;
      if (bool)
      {
        localDialog2 = this.b;
        this.b.dismiss();
      }
    }
    return localDialog2;
  }

  public void onSharedPreferenceChanged(SharedPreferences paramSharedPreferences, String paramString)
  {
    if (paramString.equals("member_id"))
    {
      p();
      if (!this.c.equals(""))
        e();
    }
    else
    {
      return;
    }
    d();
  }

  protected String setAnalyticsPageName()
  {
    return getString(2131296815);
  }

  private final class a extends CustomAsyncTask
  {
    public a(GBPreferenceActivity arg2)
    {
      super();
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      paramBoolean.booleanValue();
    }

    protected final boolean queryWebService()
    {
      Settings.a(Settings.this);
      return true;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.settings.Settings
 * JD-Core Version:    0.6.2
 */