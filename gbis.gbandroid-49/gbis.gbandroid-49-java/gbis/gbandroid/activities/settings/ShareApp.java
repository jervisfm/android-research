package gbis.gbandroid.activities.settings;

import android.app.Dialog;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import gbis.gbandroid.activities.base.GBActivity;
import gbis.gbandroid.activities.base.GBActivityDialog;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.ShareMessage;
import gbis.gbandroid.queries.ShareAppQuery;
import gbis.gbandroid.utils.CustomAsyncTask;
import gbis.gbandroid.utils.FeaturesUtils;
import java.lang.reflect.Type;

public class ShareApp extends GBActivityDialog
  implements View.OnClickListener
{
  private ShareMessage a;
  private Button b;
  private Button c;

  private void a()
  {
    this.b = ((Button)findViewById(2131165327));
    if (!b())
      this.b.setVisibility(8);
    while (true)
    {
      this.c = ((Button)findViewById(2131165328));
      this.c.setOnClickListener(this);
      return;
      this.b.setOnClickListener(this);
    }
  }

  private void a(int paramInt)
  {
    Type localType = new m(this).getType();
    this.mResponseObject = new ShareAppQuery(this, this.mPrefs, localType, this.myLocation).getResponseObject(paramInt);
    this.a = ((ShareMessage)this.mResponseObject.getPayload());
  }

  private void b(int paramInt)
  {
    a locala = new a(this, paramInt);
    locala.execute(new Object[0]);
    loadDialog(getString(2131296397), locala);
  }

  private boolean b()
  {
    if ((TelephonyManager)getSystemService("phone") == null)
      return false;
    if (Integer.parseInt(Build.VERSION.SDK) < 7)
      return true;
    return FeaturesUtils.isSMSAvailable(this);
  }

  private void c()
  {
    Intent localIntent = new Intent("android.intent.action.VIEW");
    localIntent.setType("vnd.android-dir/mms-sms");
    localIntent.putExtra("sms_body", this.a.getMessage());
    startActivity(localIntent);
  }

  private void c(int paramInt)
  {
    Intent localIntent = new Intent(this, SocialMedia.class);
    localIntent.putExtra(SocialMedia.SOCIAL_NETOWRK, paramInt);
    startActivityForResult(localIntent, 1);
  }

  private void d()
  {
    Intent localIntent = new Intent("android.intent.action.SEND");
    localIntent.setType("plain/text");
    localIntent.putExtra("android.intent.extra.SUBJECT", this.a.getSubject());
    localIntent.putExtra("android.intent.extra.TEXT", this.a.getMessage());
    startActivity(Intent.createChooser(localIntent, "Send mail..."));
  }

  public void onClick(View paramView)
  {
    if (paramView == this.b)
      sendSMS(paramView);
    while (true)
    {
      setAnalyticsTrackEventScreenButton(((Button)paramView).getText().toString());
      return;
      if (paramView == this.c)
        sendEmail(paramView);
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    a();
  }

  public void sendEmail(View paramView)
  {
    b(2);
  }

  public void sendSMS(View paramView)
  {
    b(1);
  }

  protected void setAdsAfterGPSServiceConnected()
  {
  }

  protected String setAnalyticsPageName()
  {
    return getString(2131296822);
  }

  protected void setGBActivityDialogButtons()
  {
    setGBViewInContentLayout(2130903072, false);
    setGBActivityDialogTitle(getString(2131296552));
  }

  public void shareFacebook(View paramView)
  {
    c(5);
  }

  public void shareTwitter(View paramView)
  {
    c(4);
  }

  private final class a extends CustomAsyncTask
  {
    private int b;

    public a(GBActivity paramInt, int arg3)
    {
      super();
      int i;
      this.b = i;
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      try
      {
        ShareApp.a(ShareApp.this).dismiss();
        label15: if (!paramBoolean.booleanValue())
        {
          ShareApp.a(ShareApp.this, new ShareMessage());
          ShareApp.b(ShareApp.this).setSubject(ShareApp.this.getString(2131296460));
          ShareApp.b(ShareApp.this).setMessage(ShareApp.this.getString(2131296461));
        }
        switch (this.b)
        {
        default:
          return;
        case 1:
          ShareApp.c(ShareApp.this);
          return;
        case 2:
        }
        ShareApp.d(ShareApp.this);
        return;
      }
      catch (Exception localException)
      {
        break label15;
      }
    }

    protected final boolean queryWebService()
    {
      ShareApp.a(ShareApp.this, this.b);
      return true;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.settings.ShareApp
 * JD-Core Version:    0.6.2
 */