package gbis.gbandroid.activities.settings;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import gbis.gbandroid.activities.base.GBActivity;
import gbis.gbandroid.activities.base.GBActivityDialog;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.queries.SocialMediaActionQuery;
import gbis.gbandroid.queries.SocialMediaQuery;
import gbis.gbandroid.utils.CustomAsyncTask;
import java.lang.reflect.Type;

public class SocialMedia extends GBActivityDialog
{
  public static final int FACEBOOK = 5;
  public static String SOCIAL_NETOWRK = "socialNetwork";
  public static final int TWITTER = 4;
  public static final int TYPE_EMAIL = 2;
  public static final int TYPE_SMS = 1;
  private WebView a;
  private int b;
  private int c;
  private int d;
  private String e;
  private String f;

  private void a()
  {
    this.a = ((WebView)findViewById(2131165334));
    this.a.getSettings().setJavaScriptEnabled(true);
    this.a.setWebViewClient(new a((byte)0));
    this.a.getSettings().setJavaScriptEnabled(true);
  }

  private void b()
  {
    SocialMediaQuery localSocialMediaQuery = new SocialMediaQuery(this, this.mPrefs, null, this.myLocation);
    this.a.loadUrl(localSocialMediaQuery.formLoginURL(this.c));
  }

  private void c()
  {
    if (d())
    {
      e();
      return;
    }
    b();
  }

  private boolean d()
  {
    boolean bool = true;
    switch (this.c)
    {
    default:
      bool = false;
    case 4:
    case 5:
    }
    do
    {
      do
        return bool;
      while (this.mPrefs.getBoolean("social_media_token_exists_twitter", false));
      return false;
    }
    while (this.mPrefs.getBoolean("social_media_token_exists_facebook", false));
    return false;
  }

  private void e()
  {
    b localb = new b(this);
    localb.execute(new Object[0]);
    loadDialog(getString(2131296397), localb);
  }

  private void f()
  {
    Type localType = new n(this).getType();
    this.mResponseObject = new SocialMediaActionQuery(this, this.mPrefs, localType, this.myLocation).getResponseObject(this.b, this.c, this.d, this.e, this.f);
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    switch (paramInt1)
    {
    default:
      return;
    case 6:
    }
    if (paramInt2 == -1)
    {
      c();
      return;
    }
    finish();
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
    {
      this.c = localBundle.getInt(SOCIAL_NETOWRK);
      this.b = 0;
      this.d = 1;
      this.e = "This is the test message";
      this.f = "This is the user message";
      a();
      if (this.mPrefs.getString("member_id", "").equals(""))
      {
        showMessage(getString(2131296463));
        showLogin();
        return;
      }
      c();
      return;
    }
    finish();
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 4)
    {
      if (this.a.canGoBack())
        this.a.goBack();
      while (true)
      {
        return true;
        finish();
      }
    }
    return false;
  }

  protected void setAdsAfterGPSServiceConnected()
  {
  }

  protected String setAnalyticsPageName()
  {
    return "";
  }

  protected void setGBActivityDialogButtons()
  {
    setGBViewInContentLayout(2130903076, false);
    setGBActivityDialogTitle(getString(2131296552));
  }

  private final class a extends WebViewClient
  {
    private a()
    {
    }

    public final void onPageFinished(WebView paramWebView, String paramString)
    {
      super.onPageFinished(paramWebView, paramString);
      try
      {
        SocialMedia.a(SocialMedia.this).dismiss();
        label16: if (paramString.equalsIgnoreCase(SocialMedia.this.getString(2131296783)))
        {
          SocialMedia.b(SocialMedia.this);
          SocialMedia.this.finish();
        }
        while (!paramString.equalsIgnoreCase(SocialMedia.this.getString(2131296784)))
          return;
        SocialMedia.this.finish();
        return;
      }
      catch (Exception localException)
      {
        break label16;
      }
    }

    public final void onPageStarted(WebView paramWebView, String paramString, Bitmap paramBitmap)
    {
      super.onPageStarted(paramWebView, paramString, paramBitmap);
      SocialMedia.a(SocialMedia.this, SocialMedia.this.getString(2131296397));
    }

    public final boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
    {
      paramWebView.loadUrl(paramString);
      return true;
    }
  }

  private final class b extends CustomAsyncTask
  {
    public b(GBActivity arg2)
    {
      super();
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      try
      {
        SocialMedia.a(SocialMedia.this).dismiss();
        label15: if (((Integer)SocialMedia.c(SocialMedia.this).getPayload()).intValue() == 1)
        {
          SocialMedia.this.showMessage(SocialMedia.this.getString(2131296462));
          SocialMedia.this.finish();
          return;
        }
        SocialMedia.d(SocialMedia.this);
        return;
      }
      catch (Exception localException)
      {
        break label15;
      }
    }

    protected final boolean queryWebService()
    {
      SocialMedia.e(SocialMedia.this);
      return true;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.settings.SocialMedia
 * JD-Core Version:    0.6.2
 */