package gbis.gbandroid.activities.station;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapController;
import gbis.gbandroid.GBApplication;
import gbis.gbandroid.activities.base.GBActivityMap;
import gbis.gbandroid.activities.base.GBActivityMap.MyOwnLocationOverlay;
import gbis.gbandroid.activities.map.MapStationsBase;
import gbis.gbandroid.content.GBIntent;
import gbis.gbandroid.entities.AutoCompMessage;
import gbis.gbandroid.entities.BrandAutoCompMessage;
import gbis.gbandroid.entities.GeocodeMessage;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.StationMessage;
import gbis.gbandroid.queries.AddStationQuery;
import gbis.gbandroid.queries.AutoCompleteBrandQuery;
import gbis.gbandroid.queries.AutoCompleteCityQuery;
import gbis.gbandroid.queries.GeocoderQuery;
import gbis.gbandroid.utils.CustomAsyncTask;
import gbis.gbandroid.views.CustomDialog;
import gbis.gbandroid.views.CustomDialog.Builder;
import gbis.gbandroid.views.StationsMapView;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class AddStationMap extends MapStationsBase
  implements DialogInterface.OnClickListener, DialogInterface.OnKeyListener
{
  public static final int SEARCH_TYPE_CITY = 2;
  public static final int SEARCH_TYPE_NEARME = 1;
  public static final int SEARCH_TYPE_ZIP = 3;
  private TextView A;
  private View a;
  private Dialog b;
  private Dialog c;
  private Dialog d;
  private Location e;
  private AutoCompleteTextView f;
  private EditText g;
  private TableLayout h;
  private List<CheckedTextView> i;
  private List<StationMessage> j;
  private String k = "";
  private String l = "";
  private String m = "";
  private String n = "";
  private String o = "";
  private String p = "";
  private CustomAsyncTask q;
  private CustomAsyncTask r;
  private EditText s;
  private EditText t;
  private AutoCompleteTextView u;
  private Spinner v;
  private String w;
  private int x;
  private boolean y;
  private AsyncTask<Object, Object, Boolean> z;

  private String a(int paramInt)
  {
    return getResources().getStringArray(2131099666)[paramInt];
  }

  private void a()
  {
    CustomDialog.Builder localBuilder;
    ImageView localImageView;
    if (this.b == null)
    {
      localBuilder = new CustomDialog.Builder(this);
      this.a = this.mInflater.inflate(2130903045, null);
      localImageView = (ImageView)this.a.findViewById(2131165209);
      this.mapView = ((StationsMapView)this.a.findViewById(2131165208));
      this.mapView.setControl(this);
      this.A = ((TextView)this.a.findViewById(2131165210));
      localBuilder.setTitle(getString(2131296549));
      localBuilder.setContentView(this.a);
      localBuilder.setNegativeButton(getString(2131296642), this);
      localBuilder.setPositiveButton(getString(2131296643), this);
      if (Integer.parseInt(Build.VERSION.SDK) <= 4)
        break label314;
    }
    label314: for (Object localObject = new a(this, this.mRes, this.mappinPress); ; localObject = new h(this, this.mappinPress))
    {
      localImageView.setImageDrawable((Drawable)localObject);
      localImageView.setPadding(0, 0, 0, this.mappinPress.getHeight());
      this.b = localBuilder.create(true);
      if (this.d == null)
        this.b.findViewById(2131165237).setVisibility(4);
      this.b.setOnCancelListener(new i(this));
      this.b.setOnKeyListener(this);
      this.myLocationOverlay = new GBActivityMap.MyOwnLocationOverlay(this, this, this.mapView);
      this.mapOverlays = this.mapView.getOverlays();
      this.mapOverlays.add(this.myLocationOverlay);
      this.mapView.getController().setZoom(17);
      this.mapView.setBuiltInZoomControls(true);
      this.mapView.setSatellite(true);
      d();
      this.b.show();
      loadMapPins();
      return;
    }
  }

  private void a(AutoCompleteTextView paramAutoCompleteTextView)
  {
    ArrayList localArrayList = new ArrayList();
    paramAutoCompleteTextView.setAdapter(new ArrayAdapter(getApplicationContext(), 2130903122, localArrayList));
  }

  private void a(StationMessage paramStationMessage)
  {
    GBIntent localGBIntent = new GBIntent(this, StationDetails.class, getLocation());
    localGBIntent.putExtra("station", paramStationMessage);
    startActivityForResult(localGBIntent, 4);
  }

  private void a(boolean paramBoolean)
  {
    if ((this.myLocationOverlay != null) && (this.myLocationOverlay.getLastFix() != null))
    {
      this.e = this.myLocationOverlay.getLastFix();
      if (!this.g.getText().toString().equals(""))
        break label207;
    }
    label207: for (int i1 = 0; ; i1 = Integer.parseInt(this.g.getText().toString()))
    {
      Type localType = new o(this).getType();
      this.mResponseObject = new AddStationQuery(this, this.mPrefs, localType, getLocation()).getResponseObject(this.f.getText().toString(), this.k, this.l, this.m, this.n, this.o, this.p, n(), i1, this.mapView.getMapCenter().getLatitudeE6() / 1000000.0D, this.mapView.getMapCenter().getLongitudeE6() / 1000000.0D, this.w, this.x, paramBoolean);
      this.j = ((List)this.mResponseObject.getPayload());
      return;
      if (!this.y)
        break;
      this.e.setLatitude(0.0D);
      this.e.setLongitude(0.0D);
      break;
    }
  }

  private static String b(String paramString)
  {
    return paramString.substring(2 + paramString.indexOf(","), paramString.length());
  }

  private void b()
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this);
    this.a = this.mInflater.inflate(2130903044, null);
    this.f = ((AutoCompleteTextView)this.a.findViewById(2131165203));
    this.g = ((EditText)this.a.findViewById(2131165206));
    this.h = ((TableLayout)this.a.findViewById(2131165207));
    localBuilder.setTitle(getString(2131296551));
    localBuilder.setContentView(this.a);
    localBuilder.setNegativeButton(getString(2131296642), this);
    localBuilder.setPositiveButton(getString(2131296643), this);
    this.c = localBuilder.create();
    this.c.setOnCancelListener(new j(this));
    this.c.setOnKeyListener(this);
    f();
    j();
    this.c.show();
  }

  private void b(boolean paramBoolean)
  {
    a locala = new a(this, paramBoolean);
    locala.execute(new Object[0]);
    loadDialog(getString(2131296397), locala);
  }

  private int c(String paramString)
  {
    String[] arrayOfString = getResources().getStringArray(2131099666);
    int i1 = 0;
    int i2 = 0;
    while (true)
    {
      if ((i2 >= arrayOfString.length) || (i1 != 0))
      {
        if (i1 == 0)
          break;
        return i2;
      }
      if (arrayOfString[i2].equals(paramString))
        i1 = 1;
      else
        i2++;
    }
    return 0;
  }

  private void c()
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this, this.mTracker);
    this.a = this.mInflater.inflate(2130903043, null);
    this.s = ((EditText)this.a.findViewById(2131165195));
    this.t = ((EditText)this.a.findViewById(2131165197));
    this.u = ((AutoCompleteTextView)this.a.findViewById(2131165199));
    this.v = ((Spinner)this.a.findViewById(2131165201));
    localBuilder.setTitle(getString(2131296550));
    localBuilder.setContentView(this.a);
    localBuilder.setNegativeButton(getString(2131296642), this);
    localBuilder.setPositiveButton(getString(2131296643), this);
    ArrayAdapter localArrayAdapter = new ArrayAdapter(this, 2130903091, getResources().getStringArray(2131099665));
    localArrayAdapter.setDropDownViewResource(2130903090);
    this.v.setAdapter(localArrayAdapter);
    this.d = localBuilder.create();
    this.d.findViewById(2131165237).setVisibility(4);
    this.d.setOnCancelListener(new k(this));
    e();
    this.d.show();
  }

  private void d()
  {
    this.myLocationOverlay.enableMyLocation();
    if ((this.myLocationOverlay != null) && (this.myLocationOverlay.getMyLocation() != null))
    {
      this.mapView.getController().animateTo(this.myLocationOverlay.getMyLocation());
      return;
    }
    this.mapView.getController().animateTo(new GeoPoint((int)(1000000.0D * this.e.getLatitude()), (int)(1000000.0D * this.e.getLongitude())));
  }

  private static String e(Address paramAddress)
  {
    if (paramAddress.getAddressLine(0) != null)
      return paramAddress.getAddressLine(0);
    return "";
  }

  private void e()
  {
    this.u.setThreshold(2);
    this.u.addTextChangedListener(new l(this));
    this.u.setOnItemClickListener(new m(this));
    a(this.u);
  }

  private static String f(Address paramAddress)
  {
    try
    {
      if (paramAddress.getAddressLine(1) != null)
      {
        String str1 = paramAddress.getAddressLine(1);
        String str2 = str1.substring(0, str1.indexOf(","));
        return str2;
      }
      return "";
    }
    catch (Exception localException)
    {
    }
    return "";
  }

  private void f()
  {
    this.f.setThreshold(1);
    this.f.addTextChangedListener(new n(this));
    a(this.f);
  }

  private static String g(Address paramAddress)
  {
    try
    {
      String str1 = paramAddress.getAddressLine(1);
      String str2 = str1.substring(2 + str1.indexOf(","), str1.length());
      String str3 = str2.substring(0, str2.indexOf(" "));
      return str3;
    }
    catch (Exception localException)
    {
    }
    return "";
  }

  private void g()
  {
    new d(this).execute(new Object[0]);
  }

  private static String h(Address paramAddress)
  {
    if (paramAddress.getPostalCode() != null)
      return paramAddress.getPostalCode();
    return "";
  }

  private void h()
  {
    if (this.q != null)
      this.q.cancel(true);
    this.q = new b(this);
    this.q.execute(new Object[0]);
  }

  private void i()
  {
    if (this.r != null)
      this.r.cancel(true);
    this.r = new f(this);
    this.r.execute(new Object[0]);
    loadDialog(getString(2131296397), this.r);
  }

  private void j()
  {
    int i1 = 3;
    String[] arrayOfString = new String[17];
    arrayOfString[0] = getString(2131296325);
    arrayOfString[1] = getString(2131296326);
    arrayOfString[2] = getString(2131296327);
    arrayOfString[i1] = getString(2131296328);
    arrayOfString[4] = getString(2131296329);
    arrayOfString[5] = getString(2131296330);
    arrayOfString[6] = getString(2131296331);
    arrayOfString[7] = getString(2131296340);
    arrayOfString[8] = getString(2131296336);
    arrayOfString[9] = getString(2131296334);
    arrayOfString[10] = getString(2131296333);
    arrayOfString[11] = getString(2131296337);
    arrayOfString[12] = getString(2131296338);
    arrayOfString[13] = getString(2131296341);
    arrayOfString[14] = getString(2131296339);
    arrayOfString[15] = getString(2131296335);
    arrayOfString[16] = getString(2131296332);
    e locale = new e((byte)0);
    int i4;
    if (this.i == null)
    {
      this.i = new ArrayList();
      i4 = arrayOfString.length;
    }
    TableRow.LayoutParams localLayoutParams;
    int i3;
    for (int i5 = 0; ; i5++)
    {
      if (i5 >= i4)
      {
        if (this.mRes.getConfiguration().orientation == 1)
          i1 = 2;
        localLayoutParams = new TableRow.LayoutParams(-1, -1, 1.0F);
        int i2 = this.i.size();
        i3 = 1;
        if (i3 <= i2)
          break;
        return;
      }
      String str = arrayOfString[i5];
      CheckedTextView localCheckedTextView = (CheckedTextView)this.mInflater.inflate(2130903051, null);
      localCheckedTextView.setOnClickListener(locale);
      localCheckedTextView.setText(str);
      localCheckedTextView.setFocusable(true);
      this.i.add(localCheckedTextView);
    }
    if ((i3 == 1) || (i3 % i1 == 1))
    {
      TableRow localTableRow1 = new TableRow(this);
      this.h.addView(localTableRow1);
      localTableRow1.setGravity(17);
      ((CheckedTextView)this.i.get(i3 - 1)).setLayoutParams(localLayoutParams);
      localTableRow1.addView((View)this.i.get(i3 - 1));
    }
    while (true)
    {
      i3++;
      break;
      TableRow localTableRow2 = (TableRow)this.h.getChildAt(-1 + this.h.getChildCount());
      ((CheckedTextView)this.i.get(i3 - 1)).setLayoutParams(localLayoutParams);
      localTableRow2.addView((View)this.i.get(i3 - 1));
    }
  }

  private List<AutoCompMessage> k()
  {
    Type localType = new b(this).getType();
    this.mResponseObject = new AutoCompleteCityQuery(this, this.mPrefs, localType, this.e).getResponseObject(this.u.getText().toString());
    return (List)this.mResponseObject.getPayload();
  }

  private GeocodeMessage l()
  {
    Type localType = new c(this).getType();
    this.mResponseObject = new GeocoderQuery(this, this.mPrefs, localType, this.e).getResponseObject(this.k, this.l, this.m, this.n, "");
    return (GeocodeMessage)this.mResponseObject.getPayload();
  }

  private List<BrandAutoCompMessage> m()
  {
    Type localType = new d(this).getType();
    return (List)new AutoCompleteBrandQuery(this, this.mPrefs, localType, this.e).getResponseObject(this.f.getText().toString()).getPayload();
  }

  private boolean[] n()
  {
    int i1 = this.i.size();
    boolean[] arrayOfBoolean = new boolean[i1];
    for (int i2 = 0; ; i2++)
    {
      if (i2 >= i1)
        return arrayOfBoolean;
      arrayOfBoolean[i2] = ((CheckedTextView)this.i.get(i2)).isChecked();
    }
  }

  private boolean o()
  {
    if (!this.f.getText().toString().equals(""))
      return true;
    showMessage(getString(2131296351));
    return false;
  }

  private boolean p()
  {
    if (!this.s.getText().toString().equals(""))
      if (!this.u.getText().toString().equals(""))
      {
        if (this.v.getSelectedItemPosition() != 0)
          return true;
        showMessage(getString(2131296354));
      }
    while (true)
    {
      return false;
      showMessage(getString(2131296353));
      continue;
      showMessage(getString(2131296352));
    }
  }

  private void q()
  {
    this.k = this.s.getText().toString();
    this.l = this.t.getText().toString();
    this.m = this.u.getText().toString();
    this.n = a(this.v.getSelectedItemPosition());
  }

  public void loadMapPins()
  {
    this.center = this.mapView.getMapCenter();
    if (this.z != null)
      this.z.cancel(true);
    this.z = new g(this);
    this.z.execute(new Object[0]);
  }

  public void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    if (paramDialogInterface == this.d)
      if (paramInt == -2)
      {
        if (!p())
          break label30;
        q();
        i();
      }
    label30: 
    do
    {
      do
      {
        do
        {
          do
          {
            return;
            showMessage();
            return;
            if (paramDialogInterface != this.b)
              break;
            if (paramInt == -2)
            {
              h();
              if (this.c == null)
              {
                b();
                return;
              }
              this.c.show();
              return;
            }
          }
          while (paramInt != -1);
          if (this.d == null)
          {
            finish();
            return;
          }
          this.d.show();
          return;
        }
        while (paramDialogInterface != this.c);
        if (paramInt != -2)
          break;
      }
      while (!o());
      b(false);
      return;
    }
    while (paramInt != -1);
    this.c.dismiss();
    if (this.b == null)
    {
      a();
      return;
    }
    this.b.show();
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    int i1;
    if ((this.c != null) && (this.c.isShowing()))
      i1 = this.h.getChildCount();
    for (int i2 = 0; ; i2++)
    {
      if (i2 >= i1)
      {
        this.h.removeAllViews();
        j();
        return;
      }
      ((ViewGroup)this.h.getChildAt(i2)).removeAllViews();
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
    {
      this.e = ((Location)localBundle.getParcelable("my location"));
      if ((this.e != null) && (this.e.getLatitude() != 0.0D) && (this.e.getLongitude() != 0.0D))
        a();
      while (true)
      {
        this.w = localBundle.getString("search terms");
        this.x = localBundle.getInt("search type");
        return;
        this.y = true;
        this.e = new Location("reverseGeocoded");
        c();
      }
    }
    c();
  }

  public boolean onKey(DialogInterface paramDialogInterface, int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramKeyEvent.getAction() == 0) && (paramInt == 4))
    {
      if (paramDialogInterface == this.c)
      {
        this.b.show();
        this.c.dismiss();
        return true;
      }
      if (paramDialogInterface == this.b)
      {
        this.b.dismiss();
        if (this.d != null)
        {
          this.d.show();
          return true;
        }
        finish();
        return true;
      }
    }
    return false;
  }

  public void onPause()
  {
    super.onPause();
    if (this.myLocationOverlay != null)
      this.myLocationOverlay.disableMyLocation();
  }

  protected void onResume()
  {
    super.onResume();
    if ((this.b != null) && (this.b.isShowing()))
    {
      if (this.myLocationOverlay == null)
      {
        this.myLocationOverlay = new GBActivityMap.MyOwnLocationOverlay(this, this, this.mapView);
        this.mapView.getOverlays().add(this.myLocationOverlay);
      }
      this.myLocationOverlay.enableMyLocation();
    }
  }

  protected String setAnalyticsPageName()
  {
    return getString(2131296840);
  }

  public void showMatchStationDialog()
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this, this.mTracker);
    View localView = this.mInflater.inflate(2130903062, null);
    localBuilder.setTitle(getString(2131296358));
    localBuilder.setContentView(localView);
    ((TextView)localView.findViewById(2131165261)).setText(getString(2131296359));
    ListView localListView = (ListView)localView.findViewById(2131165255);
    localListView.setChoiceMode(1);
    localListView.setAdapter(new h(this, this.j));
    localBuilder.setPositiveButton(2131296666, new e(this, localListView));
    localBuilder.setNegativeButton(2131296667, new f(this));
    CustomDialog localCustomDialog = localBuilder.create();
    localListView.setOnItemClickListener(new g(this));
    localCustomDialog.show();
  }

  private final class a extends CustomAsyncTask
  {
    private boolean b;

    public a(GBActivityMap paramBoolean, boolean arg3)
    {
      super();
      boolean bool;
      this.b = bool;
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      try
      {
        AddStationMap.g(AddStationMap.this).dismiss();
        label15: if (paramBoolean.booleanValue())
        {
          if (AddStationMap.n(AddStationMap.this).size() > 0)
            AddStationMap.this.showMatchStationDialog();
        }
        else
          return;
        AddStationMap.this.finish();
        return;
      }
      catch (Exception localException)
      {
        break label15;
      }
    }

    protected final boolean queryWebService()
    {
      AddStationMap.a(AddStationMap.this, this.b);
      return true;
    }
  }

  private final class b extends CustomAsyncTask
  {
    private Address b;

    public b(GBActivityMap arg2)
    {
      super();
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      try
      {
        AddStationMap.g(AddStationMap.this).dismiss();
        label15: if ((paramBoolean.booleanValue()) && (!isCancelled()))
        {
          AddStationMap localAddStationMap1 = AddStationMap.this;
          AddStationMap.a(localAddStationMap1, AddStationMap.a(this.b));
          AddStationMap localAddStationMap2 = AddStationMap.this;
          AddStationMap.b(localAddStationMap2, AddStationMap.b(this.b));
          AddStationMap localAddStationMap3 = AddStationMap.this;
          AddStationMap.c(localAddStationMap3, AddStationMap.c(this.b));
          AddStationMap localAddStationMap4 = AddStationMap.this;
          AddStationMap.d(localAddStationMap4, AddStationMap.d(this.b));
        }
        return;
      }
      catch (Exception localException)
      {
        break label15;
      }
    }

    protected final boolean queryWebService()
    {
      Geocoder localGeocoder = new Geocoder(AddStationMap.this.getBaseContext());
      try
      {
        List localList = localGeocoder.getFromLocation(AddStationMap.o(AddStationMap.this).getMapCenter().getLatitudeE6() / 1000000.0D, AddStationMap.o(AddStationMap.this).getMapCenter().getLongitudeE6() / 1000000.0D, 1);
        if (localList.size() > 0)
        {
          this.b = ((Address)localList.get(0));
          return true;
        }
        AddStationMap.this.setMessage(AddStationMap.this.getString(2131296356));
        return false;
      }
      catch (IOException localIOException)
      {
        AddStationMap.this.setMessage(AddStationMap.this.getString(2131296357));
      }
      return false;
    }
  }

  private final class c extends CustomAsyncTask
  {
    private List<AutoCompMessage> b;

    public c(GBActivityMap arg2)
    {
      super(false);
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      List localList;
      ArrayList localArrayList;
      int i;
      int j;
      if (paramBoolean.booleanValue())
      {
        localList = this.b;
        localArrayList = new ArrayList();
        i = localList.size();
        j = 0;
      }
      while (true)
      {
        if (j >= i)
        {
          ArrayAdapter localArrayAdapter = new ArrayAdapter(AddStationMap.this.getApplicationContext(), 2130903122, localArrayList);
          AddStationMap.e(AddStationMap.this).setAdapter(localArrayAdapter);
        }
        try
        {
          AddStationMap.e(AddStationMap.this).showDropDown();
          return;
          AutoCompMessage localAutoCompMessage = (AutoCompMessage)localList.get(j);
          String str = localAutoCompMessage.getCity() + ", " + localAutoCompMessage.getState();
          if (!localArrayList.contains(str))
            localArrayList.add(str);
          j++;
        }
        catch (Exception localException)
        {
        }
      }
    }

    protected final boolean queryWebService()
    {
      this.b = AddStationMap.f(AddStationMap.this);
      return true;
    }
  }

  private final class d extends CustomAsyncTask
  {
    private List<BrandAutoCompMessage> b;

    public d(GBActivityMap arg2)
    {
      super(false);
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      List localList;
      ArrayList localArrayList;
      int i;
      int j;
      if (paramBoolean.booleanValue())
      {
        localList = this.b;
        localArrayList = new ArrayList();
        i = localList.size();
        j = 0;
      }
      while (true)
      {
        if (j >= i)
        {
          ArrayAdapter localArrayAdapter = new ArrayAdapter(AddStationMap.this.getApplicationContext(), 2130903122, localArrayList);
          AddStationMap.c(AddStationMap.this).setAdapter(localArrayAdapter);
        }
        try
        {
          AddStationMap.c(AddStationMap.this).showDropDown();
          return;
          localArrayList.add(((BrandAutoCompMessage)localList.get(j)).getBrand());
          j++;
        }
        catch (Exception localException)
        {
        }
      }
    }

    protected final boolean queryWebService()
    {
      this.b = AddStationMap.d(AddStationMap.this);
      return true;
    }
  }

  private final class e
    implements View.OnClickListener
  {
    private e()
    {
    }

    public final void onClick(View paramView)
    {
      CheckedTextView localCheckedTextView = (CheckedTextView)paramView;
      if (localCheckedTextView.isChecked());
      for (boolean bool = false; ; bool = true)
      {
        localCheckedTextView.setChecked(bool);
        return;
      }
    }
  }

  private final class f extends CustomAsyncTask
  {
    private GeocodeMessage b;

    public f(GBActivityMap arg2)
    {
      super();
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      try
      {
        AddStationMap.g(AddStationMap.this).dismiss();
        label15: if (paramBoolean.booleanValue())
        {
          AddStationMap.h(AddStationMap.this).setLatitude(this.b.getLatitude());
          AddStationMap.h(AddStationMap.this).setLongitude(this.b.getLongitude());
          AddStationMap.i(AddStationMap.this).dismiss();
          if (AddStationMap.j(AddStationMap.this) == null)
          {
            AddStationMap.k(AddStationMap.this);
            return;
          }
          AddStationMap.j(AddStationMap.this).show();
          AddStationMap.l(AddStationMap.this);
          return;
        }
        AddStationMap.this.showMessage(AddStationMap.this.getString(2131296357));
        return;
      }
      catch (Exception localException)
      {
        break label15;
      }
    }

    protected final boolean queryWebService()
    {
      this.b = AddStationMap.m(AddStationMap.this);
      return (this.b != null) && (this.b.getLatitude() != 0.0D);
    }
  }

  private final class g extends CustomAsyncTask
  {
    public g(GBActivityMap arg2)
    {
      super();
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      if (paramBoolean.booleanValue())
      {
        AddStationMap.p(AddStationMap.this);
        if (!AddStationMap.q(AddStationMap.this))
          AddStationMap.r(AddStationMap.this).setVisibility(4);
      }
      else
      {
        return;
      }
      AddStationMap.r(AddStationMap.this).setVisibility(0);
    }

    protected final boolean queryWebService()
    {
      try
      {
        AddStationMap.s(AddStationMap.this);
        return true;
      }
      catch (RuntimeException localRuntimeException)
      {
        while (localRuntimeException.getClass().toString().equals("class android.view.ViewRoot$CalledFromWrongThreadException"));
      }
      return false;
    }
  }

  private final class h extends ArrayAdapter<StationMessage>
  {
    private List<StationMessage> b;
    private int c;

    public h(int arg2)
    {
      super(2130903104, localList);
      this.b = localList;
      this.c = 2130903104;
    }

    public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      AddStationMap.i locali;
      if (paramView == null)
      {
        paramView = AddStationMap.a(AddStationMap.this).inflate(this.c, null);
        locali = new AddStationMap.i();
        locali.a = ((TextView)paramView.findViewById(2131165419));
        locali.b = ((TextView)paramView.findViewById(2131165420));
        locali.c = ((TextView)paramView.findViewById(2131165421));
        locali.d = ((ImageView)paramView.findViewById(2131165418));
        paramView.setTag(locali);
      }
      while (true)
      {
        StationMessage localStationMessage = (StationMessage)this.b.get(paramInt);
        if (localStationMessage != null)
        {
          locali.a.setText(localStationMessage.getStationName());
          locali.b.setText(localStationMessage.getAddress());
          locali.c.setText(localStationMessage.getCity());
          Bitmap localBitmap = GBApplication.getInstance().getLogo(localStationMessage.getGasBrandId(), "/Android/data/gbis.gbandroid/cache/Brands");
          if (localBitmap == null)
            break;
          locali.d.setImageBitmap(localBitmap);
        }
        return paramView;
        locali = (AddStationMap.i)paramView.getTag();
      }
      locali.d.setImageDrawable(AddStationMap.b(AddStationMap.this).getDrawable(2130837622));
      return paramView;
    }
  }

  static final class i
  {
    TextView a;
    TextView b;
    TextView c;
    ImageView d;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.station.AddStationMap
 * JD-Core Version:    0.6.2
 */