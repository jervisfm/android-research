package gbis.gbandroid.activities.station;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputFilter.LengthFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import gbis.gbandroid.activities.base.GBActivity;
import gbis.gbandroid.activities.base.GBActivityDialog;
import gbis.gbandroid.entities.FavListMessage;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.Station;
import gbis.gbandroid.queries.FavAddStationQuery;
import gbis.gbandroid.queries.FavListQuery;
import gbis.gbandroid.utils.CustomAsyncTask;
import java.lang.reflect.Type;
import java.util.List;

public class FavAddStation extends GBActivityDialog
  implements View.OnClickListener
{
  private String a;
  private List<FavListMessage> b;
  private int c;
  private Station d;
  private ListView e;
  private EditText f;

  private void a()
  {
    this.e = ((ListView)findViewById(2131165255));
    TextView localTextView1 = (TextView)findViewById(2131165261);
    View localView = this.mInflater.inflate(2130903061, null);
    this.f = ((EditText)localView.findViewById(2131165259));
    CheckBox localCheckBox = (CheckBox)localView.findViewById(2131165260);
    TextView localTextView2 = (TextView)localView.findViewById(2131165258);
    setGBActivityDialogTitle(this.d.getStationName());
    setGBActivityDialogStationIcon(this.d);
    localTextView1.setText(getString(2131296537));
    localTextView2.setText(getString(2131296320));
    this.e.setChoiceMode(1);
    ArrayAdapter localArrayAdapter = new ArrayAdapter(this, 2130903090, d());
    this.f.setOnFocusChangeListener(new p(this));
    EditText localEditText = this.f;
    InputFilter[] arrayOfInputFilter = new InputFilter[1];
    arrayOfInputFilter[0] = new InputFilter.LengthFilter(30);
    localEditText.setFilters(arrayOfInputFilter);
    this.f.setImeOptions(6);
    localView.setOnTouchListener(new q(this, localCheckBox, localTextView1));
    this.e.addHeaderView(localView, null, true);
    this.e.setAdapter(localArrayAdapter);
    this.e.setOnItemClickListener(new r(this, localTextView1, localCheckBox));
  }

  private void b()
  {
    new a(this).execute(new Object[0]);
    loadDialog(getString(2131296401));
  }

  private void c()
  {
    b localb = new b(this);
    localb.execute(new Object[0]);
    loadDialog(getString(2131296402), localb);
  }

  private String[] d()
  {
    String[] arrayOfString = new String[this.b.size()];
    int i = this.b.size();
    for (int j = 0; ; j++)
    {
      if (j >= i)
        return arrayOfString;
      arrayOfString[j] = ((FavListMessage)this.b.get(j)).getListName();
    }
  }

  private void e()
  {
    Type localType = new s(this).getType();
    this.mResponseObject = new FavListQuery(this, this.mPrefs, localType, this.myLocation).getResponseObject();
    this.b = ((List)this.mResponseObject.getPayload());
  }

  private boolean f()
  {
    Type localType = new t(this).getType();
    this.mResponseObject = new FavAddStationQuery(this, this.mPrefs, localType, this.myLocation).getResponseObject(this.c, this.d.getStationId(), this.a);
    return this.mResponseObject != null;
  }

  public void onClick(View paramView)
  {
    if (paramView == getGBActivityDialogPositiveButton())
    {
      if (this.e.getCheckedItemPosition() != -1)
        break label87;
      this.a = this.f.getText().toString();
      if ((this.a.equals("")) || (this.a.replaceAll(" ", "").equals("")))
        showMessage(getString(2131296368));
    }
    else
    {
      return;
    }
    this.c = 0;
    label87: b();
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    try
    {
      this.d = ((Station)getIntent().getExtras().getParcelable("station"));
      List localList = (List)getLastNonConfigurationInstance();
      if (localList != null)
      {
        this.b = localList;
        a();
        return;
      }
      c();
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
      setResult(9);
      finish();
    }
  }

  public Object onRetainNonConfigurationInstance()
  {
    return this.b;
  }

  protected void setAdsAfterGPSServiceConnected()
  {
  }

  protected String setAnalyticsPageName()
  {
    return getString(2131296838);
  }

  protected void setGBActivityDialogButtons()
  {
    setGBViewInContentLayout(2130903062, false);
    setGBActivityDialogTitle(getString(2131296537));
    setGBActivityDialogPositiveButton(2131296629, this);
  }

  private final class a extends CustomAsyncTask
  {
    public a(GBActivity arg2)
    {
      super();
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      try
      {
        FavAddStation.a(FavAddStation.this).dismiss();
        label15: if (paramBoolean.booleanValue())
        {
          if (((Boolean)FavAddStation.b(FavAddStation.this).getPayload()).booleanValue())
          {
            FavAddStation.this.showMessage(FavAddStation.this.getString(2131296319));
            FavAddStation.this.setResult(-1);
            FavAddStation.this.finish();
          }
        }
        else
          return;
        FavAddStation.this.showMessage(FavAddStation.this.getString(2131296321));
        return;
      }
      catch (Exception localException)
      {
        break label15;
      }
    }

    protected final boolean queryWebService()
    {
      return FavAddStation.c(FavAddStation.this);
    }
  }

  private final class b extends CustomAsyncTask
  {
    public b(GBActivity arg2)
    {
      super();
    }

    protected final void onCancelled()
    {
      super.onCancelled();
      FavAddStation.this.finish();
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      try
      {
        FavAddStation.a(FavAddStation.this).dismiss();
        label15: if (paramBoolean.booleanValue())
        {
          FavAddStation.d(FavAddStation.this);
          return;
        }
        FavAddStation.this.finish();
        return;
      }
      catch (Exception localException)
      {
        break label15;
      }
    }

    protected final boolean queryWebService()
    {
      FavAddStation.e(FavAddStation.this);
      return true;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.station.FavAddStation
 * JD-Core Version:    0.6.2
 */