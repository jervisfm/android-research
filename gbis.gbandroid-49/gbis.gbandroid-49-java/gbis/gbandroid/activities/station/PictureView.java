package gbis.gbandroid.activities.station;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import gbis.gbandroid.activities.base.GBActivity;
import gbis.gbandroid.entities.StationMessage;
import gbis.gbandroid.entities.StationPhotos;
import gbis.gbandroid.utils.ImageLoader;
import gbis.gbandroid.views.CustomDialog.Builder;
import java.util.List;

public class PictureView extends GBActivity
  implements DialogInterface.OnClickListener, View.OnClickListener
{
  private StationMessage a;
  private int b;
  private View c;
  private Dialog d;
  private ImageLoader e;
  private TextView f;
  private TextView g;
  private ImageView h;
  private View i;
  private View j;
  private GestureDetector k;

  private void a()
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this);
    this.c = this.mInflater.inflate(2130903117, null);
    localBuilder.setTitle(this.a.getStationName());
    localBuilder.setStationLogo(this.a.getGasBrandId(), "/Android/data/gbis.gbandroid/cache/Brands");
    localBuilder.setContentView(this.c);
    localBuilder.setNegativeButton(getString(2131296642), this);
    localBuilder.setPositiveButton(getString(2131296643), this);
    try
    {
      int m = Integer.parseInt(Build.VERSION.SDK);
      bool = false;
      if (m < 8)
        bool = true;
      this.d = localBuilder.create(bool);
      this.d.setOnCancelListener(new u(this));
      this.d.setOnKeyListener(new v(this));
      b();
      this.d.show();
      c();
      return;
    }
    catch (Exception localException)
    {
      while (true)
        boolean bool = false;
    }
  }

  private void b()
  {
    this.f = ((TextView)this.c.findViewById(2131165469));
    this.g = ((TextView)this.c.findViewById(2131165471));
    this.h = ((ImageView)this.c.findViewById(2131165467));
    this.i = this.d.findViewById(2131165237);
    this.j = this.d.findViewById(2131165239);
    if (this.mRes.getConfiguration().orientation != 1)
    {
      this.i.setVisibility(8);
      this.j.setVisibility(8);
      this.i = this.c.findViewById(2131165472);
      this.j = this.c.findViewById(2131165473);
      this.i.setOnClickListener(this);
      this.j.setOnClickListener(this);
    }
    this.k = new GestureDetector(new w(this));
    this.h.setOnTouchListener(new x(this));
  }

  private void c()
  {
    while (true)
    {
      try
      {
        if (this.b == 0)
        {
          this.i.setVisibility(4);
          if (this.b == -1 + this.a.getStationPhotos().size())
          {
            this.j.setVisibility(4);
            this.f.setText(((StationPhotos)this.a.getStationPhotos().get(this.b)).getTitle());
            this.g.setText(((StationPhotos)this.a.getStationPhotos().get(this.b)).getCaption());
            this.h.setTag(String.valueOf(((StationPhotos)this.a.getStationPhotos().get(this.b)).getMediumPath().hashCode()));
            this.e.displayImage(((StationPhotos)this.a.getStationPhotos().get(this.b)).getMediumPath(), this, this.h, false);
          }
        }
        else
        {
          this.i.setVisibility(0);
          continue;
        }
      }
      catch (Throwable localThrowable)
      {
        showMessage(this.mRes.getString(2131296257));
        return;
      }
      this.j.setVisibility(0);
    }
  }

  private void d()
  {
    this.b = (1 + this.b);
    c();
  }

  private void e()
  {
    this.b = (-1 + this.b);
    c();
  }

  public void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    if (paramInt == -2)
      d();
    while (paramInt != -1)
      return;
    e();
  }

  public void onClick(View paramView)
  {
    if (paramView == this.j)
      d();
    while (paramView != this.i)
      return;
    e();
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    this.d.dismiss();
    a();
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Bundle localBundle = getIntent().getExtras();
    this.e = new ImageLoader(this, 2130837658);
    if (localBundle != null)
    {
      DisplayMetrics localDisplayMetrics = new DisplayMetrics();
      getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
      Integer localInteger = (Integer)getLastNonConfigurationInstance();
      if (localInteger != null);
      for (this.b = localInteger.intValue(); ; this.b = localBundle.getInt("station photos position"))
      {
        this.a = ((StationMessage)localBundle.getParcelable("station"));
        a();
        return;
      }
    }
    finish();
  }

  protected void onDestroy()
  {
    super.onDestroy();
    cleanImageCache();
  }

  public Object onRetainNonConfigurationInstance()
  {
    return Integer.valueOf(this.b);
  }

  protected void setAdsAfterGPSServiceConnected()
  {
  }

  protected String setAnalyticsPageName()
  {
    return getString(2131296845);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.station.PictureView
 * JD-Core Version:    0.6.2
 */