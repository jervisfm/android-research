package gbis.gbandroid.activities.station;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.Gallery.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import gbis.gbandroid.GBApplication;
import gbis.gbandroid.R.styleable;
import gbis.gbandroid.activities.base.GBActivity;
import gbis.gbandroid.activities.base.GBActivityAds;
import gbis.gbandroid.activities.report.ReportPrices;
import gbis.gbandroid.content.GBIntent;
import gbis.gbandroid.entities.AdInLine;
import gbis.gbandroid.entities.ListMessage;
import gbis.gbandroid.entities.ResponseMessage;
import gbis.gbandroid.entities.Station;
import gbis.gbandroid.entities.StationMessage;
import gbis.gbandroid.entities.StationPhotos;
import gbis.gbandroid.listeners.ImageLazyLoaderListener;
import gbis.gbandroid.listeners.ScrollViewListener;
import gbis.gbandroid.queries.StationDetailsQuery;
import gbis.gbandroid.utils.CustomAsyncTask;
import gbis.gbandroid.utils.ImageLoader;
import gbis.gbandroid.utils.VerifyFields;
import gbis.gbandroid.views.CustomTextViewAutoResize;
import gbis.gbandroid.views.layouts.CustomScrollView;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class StationDetails extends GBActivityAds
  implements ImageLazyLoaderListener, ScrollViewListener
{
  BroadcastReceiver a = new y(this);
  private int b;
  private StationMessage c;
  private String d;
  private String e;
  private a f;
  private int g;
  private int h;
  private TableLayout i;
  private TableRow j;
  private ViewGroup k;
  private ViewGroup l;
  private ViewGroup m;
  private ViewGroup n;
  private TextView o;
  private SensorManager p;
  private Sensor q;
  private b r;
  private float s;

  private int a(int paramInt, LinearLayout paramLinearLayout, ViewGroup[] paramArrayOfViewGroup)
  {
    int i1;
    switch (paramInt)
    {
    default:
      paramArrayOfViewGroup[0] = null;
      i1 = paramLinearLayout.getChildCount();
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    }
    for (int i2 = 0; ; i2++)
    {
      if (i2 >= i1)
      {
        return 0;
        paramArrayOfViewGroup[0] = this.k;
        return 3;
        paramArrayOfViewGroup[0] = this.k;
        break;
        paramArrayOfViewGroup[0] = this.l;
        break;
        paramArrayOfViewGroup[0] = this.m;
        break;
        paramArrayOfViewGroup[0] = this.n;
        break;
      }
      if (paramArrayOfViewGroup[0] == paramLinearLayout.getChildAt(i2))
        return i2 + 1;
    }
  }

  private static String a(List<String> paramList)
  {
    Object localObject = (String)paramList.get(0);
    int i1 = paramList.size();
    int i2 = 1;
    while (true)
    {
      if (i2 >= i1)
        return localObject + ": ";
      String str = localObject + ", " + (String)paramList.get(i2);
      i2++;
      localObject = str;
    }
  }

  private void a(double paramDouble, String paramString1, String paramString2, String paramString3)
  {
    if (this.e.equals(this.mRes.getStringArray(2131099649)[0]))
      a(this.k, paramDouble, paramString2, paramString1, paramString3, getString(2131296592));
    do
    {
      return;
      if (this.e.equals(this.mRes.getStringArray(2131099649)[1]))
      {
        a(this.l, paramDouble, paramString2, paramString1, paramString3, getString(2131296593));
        return;
      }
      if (this.e.equals(this.mRes.getStringArray(2131099649)[2]))
      {
        a(this.m, paramDouble, paramString2, paramString1, paramString3, getString(2131296594));
        return;
      }
    }
    while (!this.e.equals(this.mRes.getStringArray(2131099649)[3]));
    a(this.n, paramDouble, paramString2, paramString1, paramString3, getString(2131296595));
  }

  private void a(int paramInt)
  {
    GBIntent localGBIntent = new GBIntent(this, PictureView.class, this.myLocation);
    localGBIntent.putExtra("station", this.c);
    localGBIntent.putExtra("station photos position", paramInt);
    startActivity(localGBIntent);
  }

  private void a(ViewGroup paramViewGroup, double paramDouble, String paramString1, String paramString2, String paramString3, String paramString4)
  {
    TextView localTextView1 = (TextView)paramViewGroup.findViewById(2131165550);
    TextView localTextView2 = (TextView)paramViewGroup.findViewById(2131165551);
    TextView localTextView3 = (TextView)paramViewGroup.findViewById(2131165553);
    TextView localTextView4 = (TextView)paramViewGroup.findViewById(2131165554);
    TextView localTextView5 = (TextView)paramViewGroup.findViewById(2131165555);
    TextView localTextView6 = (TextView)paramViewGroup.findViewById(2131165547);
    if (paramViewGroup.getId() == 2131165529)
    {
      localTextView6.setTypeface(Typeface.DEFAULT, 1);
      localTextView1.setTypeface(Typeface.DEFAULT, 1);
      localTextView2.setTypeface(Typeface.DEFAULT, 1);
    }
    localTextView6.setText(paramString4);
    if (paramDouble > 0.0D)
    {
      if (paramString1.equals("USA"))
      {
        localTextView1.setText(VerifyFields.doubleToScale(paramDouble, 2));
        localTextView2.setVisibility(0);
      }
      while (true)
      {
        if ((paramString2 != null) && (!paramString2.equals("")))
          localTextView3.setText(paramString2);
        localTextView4.setText(paramString3);
        localTextView4.setVisibility(0);
        localTextView5.setText(getString(2131296302));
        return;
        localTextView1.setText(VerifyFields.doubleToScale(paramDouble, 1));
        localTextView2.setVisibility(8);
      }
    }
    localTextView1.setText("---");
    localTextView3.setText("---");
    localTextView4.setVisibility(8);
    localTextView2.setVisibility(8);
    localTextView5.setText(getString(2131296303));
  }

  private void a(Station paramStation)
  {
    this.k = ((ViewGroup)findViewById(2131165529));
    this.l = ((ViewGroup)findViewById(2131165530));
    this.m = ((ViewGroup)findViewById(2131165531));
    this.n = ((ViewGroup)findViewById(2131165532));
    CustomTextViewAutoResize localCustomTextViewAutoResize = (CustomTextViewAutoResize)findViewById(2131165519);
    ((CustomScrollView)findViewById(2131165515)).setOnScrollViewListener(this);
    localCustomTextViewAutoResize.setMinTextSize(20.0F);
    localCustomTextViewAutoResize.setText(paramStation.getStationName());
    ((TextView)findViewById(2131165523)).setText(paramStation.getAddress());
    ((TextView)findViewById(2131165524)).setText(paramStation.getCity());
    this.o = ((TextView)findViewById(2131165527));
    if (paramStation.getPostalCode() != null)
    {
      TextView localTextView = (TextView)findViewById(2131165525);
      localTextView.setText(paramStation.getPostalCode());
      localTextView.setVisibility(0);
    }
    while (true)
    {
      b(paramStation);
      double d1 = paramStation.getPrice(this, this.e);
      if (d1 != 0.0D)
        a(d1, paramStation.getTimeSpottedConverted(this, this.e), paramStation.getCountry(), "");
      if ((paramStation instanceof ListMessage))
        a(paramStation, ((ListMessage)paramStation).getDistance());
      return;
      ((TextView)findViewById(2131165525)).setVisibility(8);
    }
  }

  private void a(Station paramStation, double paramDouble)
  {
    String str;
    if ((paramDouble != 0.0D) && (this.myLocation == null) && (this.myLocation.getLatitude() == 0.0D) && (this.myLocation.getLongitude() == 0.0D))
      str = this.mPrefs.getString("distancePreference", "");
    try
    {
      if (!str.equals(""))
      {
        if (str.equals("miles"))
        {
          this.o.setText(VerifyFields.roundTwoDecimals(paramDouble) + " mi");
          return;
        }
        this.o.setText(VerifyFields.roundTwoDecimals(paramDouble * 1.609344D) + " km");
        return;
      }
      if (paramStation.getCountry().equals("USA"))
      {
        this.o.setText(VerifyFields.roundTwoDecimals(paramDouble) + " mi");
        return;
      }
      this.o.setText(VerifyFields.roundTwoDecimals(paramDouble * 1.609344D) + " km");
      return;
    }
    catch (Exception localException)
    {
    }
  }

  private void a(String paramString)
  {
    startActivity(new Intent("android.intent.action.VIEW", Uri.parse(paramString)));
  }

  private void a(String paramString, int paramInt)
  {
    this.h = (1 + this.h);
    if (this.h == 1)
    {
      this.j = new TableRow(this);
      this.i.addView(this.j);
    }
    RelativeLayout localRelativeLayout = (RelativeLayout)this.mInflater.inflate(2130903046, null);
    ((ImageView)localRelativeLayout.findViewById(2131165212)).setImageResource(paramInt);
    ((TextView)localRelativeLayout.findViewById(2131165213)).setText(paramString);
    this.j.addView(localRelativeLayout);
    if (this.h >= this.mRes.getDisplayMetrics().widthPixels / (100.0F * getDensity()))
      this.h = 0;
  }

  private static void a(Map<String, List<String>> paramMap, String paramString1, String paramString2)
  {
    if ((paramString1 != null) && (!paramString1.equals("")))
    {
      Object localObject = (List)paramMap.get(paramString1);
      if (localObject == null)
      {
        localObject = new ArrayList();
        paramMap.put(paramString1, localObject);
      }
      ((List)localObject).add(paramString2);
    }
  }

  private void b()
  {
    if (this.p != null)
    {
      if (this.r != null)
      {
        this.p.unregisterListener(this.r);
        this.r = null;
      }
      this.q = null;
      this.p = null;
    }
  }

  private void b(Station paramStation)
  {
    ImageView localImageView = (ImageView)findViewById(2131165517);
    ImageLoader localImageLoader = new ImageLoader(this, 2130837622, "/Android/data/gbis.gbandroid/cache/Brands");
    String str = GBApplication.getInstance().getImageUrl(paramStation.getGasBrandId(), getDensity());
    localImageView.setTag(String.valueOf(paramStation.getGasBrandId()));
    try
    {
      if (paramStation.getGasBrandId() > 0)
      {
        Cursor localCursor = getBrand(paramStation.getGasBrandId());
        if (localCursor.getCount() == 0)
        {
          addBrand(paramStation.getGasBrandId(), paramStation.getGasBrandVersion());
          localCursor = getBrand(paramStation.getGasBrandId());
        }
        localCursor.moveToFirst();
        int i1 = localCursor.getInt(localCursor.getColumnIndex("version"));
        int i2 = paramStation.getGasBrandVersion();
        boolean bool = false;
        if (i2 > i1)
        {
          bool = true;
          updateBrandVersion(paramStation.getGasBrandId(), paramStation.getGasBrandVersion());
        }
        localImageLoader.displayImage(str, this, localImageView, bool, 0, String.valueOf(paramStation.getGasBrandId()));
        return;
      }
      localImageView.setImageDrawable(this.mRes.getDrawable(2130837622));
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }

  private void b(String paramString)
  {
    Intent localIntent = new Intent("android.intent.action.DIAL");
    localIntent.setData(Uri.parse("tel:" + paramString));
    startActivity(localIntent);
  }

  private void c()
  {
    RelativeLayout localRelativeLayout1 = (RelativeLayout)findViewById(2131165542);
    RelativeLayout localRelativeLayout2 = (RelativeLayout)findViewById(2131165540);
    RelativeLayout localRelativeLayout3 = (RelativeLayout)findViewById(2131165544);
    Button localButton = (Button)findViewById(2131165528);
    localButton.setOnClickListener(new ae(this, localButton));
    localRelativeLayout1.setOnClickListener(new af(this));
    localRelativeLayout3.setOnClickListener(new ag(this));
    localRelativeLayout2.setOnClickListener(new ah(this));
    c localc = new c((byte)0);
    this.k.setOnClickListener(localc);
    this.l.setOnClickListener(localc);
    this.m.setOnClickListener(localc);
    this.n.setOnClickListener(localc);
  }

  private void d()
  {
    new d(this).execute(new Object[0]);
  }

  private void e()
  {
    ImageView localImageView = (ImageView)findViewById(2131165521);
    String str = ((StationPhotos)this.c.getStationPhotos().get(0)).getMediumPath();
    localImageView.setTag(String.valueOf(str.hashCode()));
    ImageLoader localImageLoader = new ImageLoader(this, 2130837658);
    localImageLoader.setOnLazyLoaderFinished(this);
    localImageLoader.displayImage(str, this, localImageView, false);
  }

  private void f()
  {
    Gallery localGallery = (Gallery)findViewById(2131165539);
    ((TextView)findViewById(2131165538)).setVisibility(8);
    localGallery.setAdapter(new ImageAdapter(this, g()));
    localGallery.setVisibility(0);
    localGallery.setOnItemClickListener(new ai(this));
  }

  private String[] g()
  {
    String[] arrayOfString = new String[this.c.getStationPhotos().size()];
    int i1 = this.c.getStationPhotos().size();
    for (int i2 = 0; ; i2++)
    {
      if (i2 >= i1)
        return arrayOfString;
      arrayOfString[i2] = ((StationPhotos)this.c.getStationPhotos().get(i2)).getMediumPath();
    }
  }

  private void h()
  {
    Type localType = new aj(this).getType();
    this.mResponseObject = new StationDetailsQuery(this, this.mPrefs, localType, this.myLocation).getResponseObject(this.b);
    this.c = ((StationMessage)this.mResponseObject.getPayload());
  }

  private void i()
  {
    TextView localTextView1 = (TextView)findViewById(2131165523);
    TextView localTextView2 = (TextView)findViewById(2131165524);
    TextView localTextView3 = (TextView)findViewById(2131165525);
    TextView localTextView4 = (TextView)findViewById(2131165526);
    ImageView localImageView = (ImageView)findViewById(2131165518);
    String str;
    if (this.c.isNotIntersection())
    {
      str = "near";
      localTextView1.setText(this.c.getAddress() + " " + str + " " + this.c.getCrossStreet());
      localTextView2.setText(this.c.getCity() + ", " + this.c.getState());
      localTextView3.setText(this.c.getPostalCode());
      localTextView3.setVisibility(0);
      if (!this.c.getPhone().equals(""))
      {
        localTextView4.setVisibility(0);
        localTextView4.setText(this.c.getPhone());
        localTextView4.setOnClickListener(new ak(this));
      }
      if (this.c.isIsFav() != 1)
        break label321;
      localImageView.setVisibility(0);
      label240: j();
      k();
      l();
      if (this.c.getStationPhotos().size() <= 0)
        break label331;
      ((TextView)findViewById(2131165538)).setVisibility(0);
      ((TextView)findViewById(2131165537)).setVisibility(0);
      e();
      f();
    }
    while (true)
    {
      o();
      return;
      str = "&";
      break;
      label321: localImageView.setVisibility(8);
      break label240;
      label331: n();
    }
  }

  private void j()
  {
    a(this.k, this.c.getRegPrice(), this.c.getCountry(), this.c.getTimeSpottedConverted(this, getString(2131296713)), this.c.getRegMemberId(), getString(2131296592));
    a(this.l, this.c.getMidPrice(), this.c.getCountry(), this.c.getTimeSpottedConverted(this, getString(2131296714)), this.c.getMidMemberId(), getString(2131296593));
    a(this.m, this.c.getPremPrice(), this.c.getCountry(), this.c.getTimeSpottedConverted(this, getString(2131296715)), this.c.getPremMemberId(), getString(2131296594));
    a(this.n, this.c.getDieselPrice(), this.c.getCountry(), this.c.getTimeSpottedConverted(this, getString(2131296716)), this.c.getDieselMemberId(), getString(2131296595));
  }

  private void k()
  {
    this.h = 0;
    this.i = ((TableLayout)findViewById(2131165536));
    this.i.removeAllViews();
    this.i.setStretchAllColumns(true);
    this.j = new TableRow(this);
    if (this.c.hasRegularGas())
      a(getString(2131296325), 2130837699);
    if (this.c.hasMidgradeGas())
      a(getString(2131296326), 2130837655);
    if (this.c.hasPremiumGas())
      a(getString(2131296327), 2130837666);
    if (this.c.hasDiesel())
      a(getString(2131296328), 2130837582);
    if (this.c.hasE85())
      a(getString(2131296329), 2130837583);
    if (this.c.hasPropane())
      a(getString(2131296330), 2130837673);
    if (this.c.hasCStore())
      a(getString(2131296331), 2130837570);
    if (this.c.hasServiceStation())
      a(getString(2131296340), 2130837706);
    if (this.c.hasPayAtPump())
      a(getString(2131296336), 2130837664);
    if (this.c.hasRestaurant())
      a(getString(2131296334), 2130837700);
    if (this.c.hasRestrooms())
      a(getString(2131296333), 2130837701);
    if (this.c.hasAir())
      a(getString(2131296337), 2130837505);
    if (this.c.hasPayphone())
      a(getString(2131296338), 2130837665);
    if (this.c.hasAtm())
      a(getString(2131296341), 2130837507);
    if (this.c.hasOpen247())
      a(getString(2131296339), 2130837660);
    if (this.c.hasTruckStop())
      a(getString(2131296335), 2130837717);
    if (this.c.hasCarWash())
      a(getString(2131296332), 2130837571);
    if (this.i.getChildCount() == 0)
    {
      TextView localTextView = new TextView(this);
      localTextView.setText(this.mRes.getString(2131296699));
      localTextView.setTextColor(this.mRes.getColor(2131230724));
      localTextView.setTextSize(16.0F);
      localTextView.setGravity(1);
      this.i.addView(localTextView);
    }
  }

  private void l()
  {
    TextView localTextView = (TextView)findViewById(2131165533);
    LinkedHashMap localLinkedHashMap = new LinkedHashMap();
    a(localLinkedHashMap, this.c.getRegComments(), getString(2131296592));
    a(localLinkedHashMap, this.c.getMidComments(), getString(2131296593));
    a(localLinkedHashMap, this.c.getPremComments(), getString(2131296594));
    a(localLinkedHashMap, this.c.getDieselComments(), getString(2131296595));
    if (!localLinkedHashMap.isEmpty())
    {
      Iterator localIterator = localLinkedHashMap.keySet().iterator();
      if (localLinkedHashMap.size() == 1)
        localTextView.setText((String)localIterator.next());
      String str1;
      while (true)
      {
        localTextView.setVisibility(0);
        return;
        str1 = "";
        if (localIterator.hasNext())
          break;
        localTextView.setText(str1);
      }
      String str2 = (String)localIterator.next();
      String str3 = a((List)localLinkedHashMap.get(str2)) + str2;
      StringBuilder localStringBuilder = new StringBuilder(String.valueOf(str1)).append(str3);
      if (localIterator.hasNext());
      for (String str4 = "\n"; ; str4 = "")
      {
        str1 = str4;
        break;
      }
    }
    localTextView.setVisibility(8);
  }

  private void m()
  {
    p();
    if (this.d.equals(""))
    {
      showMessage(getString(2131296317));
      showLogin();
      return;
    }
    showCameraDialog(this.c);
  }

  private void n()
  {
    ImageView localImageView = (ImageView)findViewById(2131165521);
    localImageView.setImageDrawable(this.mRes.getDrawable(2130837658));
    ((TextView)findViewById(2131165522)).setVisibility(0);
    localImageView.setVisibility(0);
    localImageView.setOnClickListener(new al(this));
  }

  private void o()
  {
    LinearLayout localLinearLayout1 = (LinearLayout)findViewById(2131165516);
    ViewGroup[] arrayOfViewGroup = new ViewGroup[1];
    float f1 = getDensity();
    List localList = this.c.getAdsInLine();
    int i1 = localList.size();
    int i2 = 0;
    if (i2 >= i1)
      return;
    AdInLine localAdInLine = (AdInLine)localList.get(i2);
    LinearLayout localLinearLayout2;
    LinearLayout.LayoutParams localLayoutParams1;
    LinearLayout.LayoutParams localLayoutParams2;
    if ((localAdInLine.getPosition() > 0) && (localAdInLine.getPosition() < 6))
    {
      localLinearLayout2 = new LinearLayout(this);
      localLinearLayout1.addView(localLinearLayout2, a(localAdInLine.getPosition(), localLinearLayout1, arrayOfViewGroup));
      localLayoutParams1 = (LinearLayout.LayoutParams)localLinearLayout2.getLayoutParams();
      localLayoutParams2 = (LinearLayout.LayoutParams)arrayOfViewGroup[0].getLayoutParams();
      if (localAdInLine.getHeight() <= 0)
        break label328;
    }
    label328: for (localLayoutParams1.height = ((int)(localAdInLine.getHeight() * getDensity())); ; localLayoutParams1.height = arrayOfViewGroup[0].getHeight())
    {
      localLayoutParams1.setMargins(localLayoutParams2.leftMargin, localLayoutParams2.topMargin, localLayoutParams2.rightMargin, localLayoutParams2.bottomMargin);
      WebView localWebView = new WebView(this);
      localWebView.loadData(localAdInLine.getHtml(), "text/html", "utf-8");
      localWebView.setBackgroundColor(0);
      localLinearLayout2.setPadding((int)(2.0F * f1), (int)(2.0F * f1), (int)(2.0F * f1), (int)(2.0F * f1));
      localLinearLayout2.addView(localWebView, new LinearLayout.LayoutParams(-1, -1));
      localLinearLayout2.setBackgroundDrawable(new ShapeDrawable(new z(this, localAdInLine, localLinearLayout2)));
      if (!localAdInLine.getUrl().equals(""))
      {
        localLinearLayout2.setFocusable(true);
        localLinearLayout2.setOnClickListener(new aa(this, localAdInLine));
        localWebView.setOnTouchListener(new ab(this, localAdInLine));
      }
      i2++;
      break;
    }
  }

  private void p()
  {
    this.d = this.mPrefs.getString("member_id", "");
    this.e = this.mPrefs.getString("fuelPreference", "");
  }

  private void q()
  {
    GBIntent localGBIntent = new GBIntent(this, FavAddStation.class, this.myLocation);
    localGBIntent.putExtra("station", this.c);
    startActivityForResult(localGBIntent, 5);
  }

  private void r()
  {
    GBIntent localGBIntent = new GBIntent(this, ReportPrices.class, this.myLocation);
    localGBIntent.putExtra("station", this.c);
    localGBIntent.putExtra("time offset", this.c.getTimeOffset());
    localGBIntent.putExtra("fuel regular", this.c.getRegPrice());
    localGBIntent.putExtra("time spotted regular", this.c.getRegTimeSpotted());
    localGBIntent.putExtra("fuel midgrade", this.c.getMidPrice());
    localGBIntent.putExtra("time spotted midgrade", this.c.getMidTimeSpotted());
    localGBIntent.putExtra("fuel premium", this.c.getPremPrice());
    localGBIntent.putExtra("time spotted premium", this.c.getPremTimeSpotted());
    localGBIntent.putExtra("fuel diesel", this.c.getDieselPrice());
    localGBIntent.putExtra("time spotted diesel", this.c.getDieselTimeSpotted());
    localGBIntent.putExtra("favourite id", this.g);
    startActivityForResult(localGBIntent, 3);
  }

  private void s()
  {
    Location localLocation = getLastKnownLocation();
    if (localLocation != null)
    {
      showDirections(this.c.getLatitude(), this.c.getLongitude(), localLocation);
      return;
    }
    showMessage(getString(2131296322));
    showStationOnMap(this.c);
  }

  private void t()
  {
    if ((this.c != null) && (this.myLocation != null))
    {
      Location localLocation = new Location("Station");
      localLocation.setLatitude(this.c.getLatitude());
      localLocation.setLongitude(this.c.getLongitude());
      double d1 = this.myLocation.distanceTo(localLocation) / 1000.0F / 1.609344D;
      a(this.c, d1);
    }
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    switch (paramInt1)
    {
    default:
    case 4:
    case 3:
    case 5:
    }
    do
    {
      do
      {
        do
          return;
        while ((paramInt2 == 0) || (paramInt2 != -1));
        setResult(-1);
        finish();
        return;
      }
      while ((paramInt2 == 0) || (paramInt2 != 3));
      setResult(3, paramIntent);
      finish();
      return;
    }
    while ((paramInt2 == 0) || (paramInt2 != -1));
    ((ImageView)findViewById(2131165518)).setVisibility(0);
    this.c.setIsFav(1);
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    if (this.c != null)
      k();
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    IntentFilter localIntentFilter = new IntentFilter();
    localIntentFilter.addAction("android.intent.action.SCREEN_OFF");
    registerReceiver(this.a, localIntentFilter);
    this.f = new a((byte)0);
    p();
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
    {
      openDB();
      setContentView(2130903123);
      Station localStation = (Station)localBundle.getParcelable("station");
      this.b = localStation.getStationId();
      this.g = localBundle.getInt("favourite id");
      a(localStation);
      d();
      return;
    }
    finish();
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    super.onCreateOptionsMenu(paramMenu);
    getMenuInflater().inflate(2131427337, paramMenu);
    return true;
  }

  public void onDestroy()
  {
    super.onDestroy();
    cleanImageCache();
    unregisterReceiver(this.a);
    try
    {
      this.mPrefs.unregisterOnSharedPreferenceChangeListener(this.f);
      return;
    }
    catch (Exception localException)
    {
    }
  }

  public void onImageFailed(ImageView paramImageView)
  {
    ((TextView)findViewById(2131165522)).setVisibility(0);
    paramImageView.setOnClickListener(new ad(this));
  }

  public void onImageLoaded(ImageView paramImageView)
  {
    paramImageView.setOnClickListener(new ac(this));
  }

  public void onMyLocationChanged(Location paramLocation)
  {
    super.onMyLocationChanged(paramLocation);
    t();
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    super.onOptionsItemSelected(paramMenuItem);
    switch (paramMenuItem.getItemId())
    {
    default:
      return false;
    case 2131165604:
      r();
      return true;
    case 2131165606:
      s();
      return true;
    case 2131165605:
      m();
      return true;
    case 2131165608:
      launchSettings();
      return true;
    case 2131165609:
      showCityZipSearch();
      return true;
    case 2131165607:
    }
    if (paramMenuItem.getTitle().equals(getString(2131296629)))
    {
      p();
      if (this.d.equals(""))
      {
        showMessage(getString(2131296318));
        showLogin();
        return true;
      }
      q();
      return true;
    }
    showFavourites();
    return true;
  }

  public void onPause()
  {
    super.onPause();
    b();
  }

  public boolean onPrepareOptionsMenu(Menu paramMenu)
  {
    if (this.c == null)
      return false;
    paramMenu.getItem(3).setVisible(true);
    paramMenu.getItem(3).setEnabled(true);
    if (this.c.isIsFav() == 0)
    {
      paramMenu.getItem(3).setTitle(getString(2131296629));
      paramMenu.getItem(3).setIcon(2130837634);
      return true;
    }
    paramMenu.getItem(3).setTitle(getString(2131296632));
    paramMenu.getItem(3).setIcon(2130837647);
    return true;
  }

  protected void onResume()
  {
    super.onResume();
    openDB();
    p();
  }

  public void onScrollChanged(CustomScrollView paramCustomScrollView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if (paramInt2 != paramInt4)
      hideAds();
  }

  public void onStart()
  {
    super.onStart();
    this.mPrefs.registerOnSharedPreferenceChangeListener(this.f);
  }

  protected void setAdsAfterGPSServiceConnected()
  {
    int i1 = this.mPrefs.getInt("adStation", 0);
    setAdConfiguration(this.mPrefs.getString("adFavoritesKey", ""), this.mPrefs.getString("adFavoritesUnit", ""), i1);
    registerMyLocationChangedListener(this);
  }

  protected String setAnalyticsPageName()
  {
    return getString(2131296844);
  }

  public class ImageAdapter extends BaseAdapter
    implements ImageLazyLoaderListener
  {
    int a;
    private Context c;
    private ImageLoader d;
    private String[] e;

    public ImageAdapter(Context paramArrayOfString, String[] arg3)
    {
      this.c = paramArrayOfString;
      TypedArray localTypedArray = StationDetails.this.obtainStyledAttributes(R.styleable.ShowStation);
      this.a = localTypedArray.getResourceId(0, 0);
      localTypedArray.recycle();
      this.d = new ImageLoader(StationDetails.this, 2130837658);
      this.d.setOnLazyLoaderFinished(this);
      Object localObject;
      this.e = localObject;
    }

    public int getCount()
    {
      return this.e.length;
    }

    public Object getItem(int paramInt)
    {
      return Integer.valueOf(paramInt);
    }

    public long getItemId(int paramInt)
    {
      return paramInt;
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      ImageView localImageView = new ImageView(this.c);
      localImageView.setTag(String.valueOf(this.e[paramInt].hashCode()));
      this.d.displayImage(this.e[paramInt], StationDetails.this, localImageView, false);
      localImageView.setBackgroundResource(this.a);
      return localImageView;
    }

    public void onImageFailed(ImageView paramImageView)
    {
    }

    public void onImageLoaded(ImageView paramImageView)
    {
      int i = paramImageView.getDrawable().getIntrinsicWidth();
      int j = paramImageView.getDrawable().getIntrinsicHeight();
      paramImageView.setLayoutParams(new Gallery.LayoutParams(i, j));
      ViewGroup.LayoutParams localLayoutParams = ((Gallery)StationDetails.this.findViewById(2131165539)).getLayoutParams();
      if (j > localLayoutParams.height)
        localLayoutParams.height = j;
    }
  }

  private final class a
    implements SharedPreferences.OnSharedPreferenceChangeListener
  {
    private a()
    {
    }

    public final void onSharedPreferenceChanged(SharedPreferences paramSharedPreferences, String paramString)
    {
      if (paramString.equals("member_id"))
        StationDetails.a(StationDetails.this);
    }
  }

  private final class b
    implements SensorEventListener
  {
    public final void onAccuracyChanged(Sensor paramSensor, int paramInt)
    {
    }

    public final void onSensorChanged(SensorEvent paramSensorEvent)
    {
      if (paramSensorEvent.sensor == StationDetails.g(this.a))
      {
        float f = StationDetails.h(this.a);
        StationDetails.a(this.a, paramSensorEvent.values[0]);
        if ((Math.abs(f - StationDetails.h(this.a)) > 2.0F) && (StationDetails.i(this.a) != null))
        {
          StationDetails.i(this.a);
          StationDetails.a();
        }
      }
    }
  }

  private final class c
    implements View.OnClickListener
  {
    private c()
    {
    }

    public final void onClick(View paramView)
    {
      StationDetails.a(StationDetails.this, StationDetails.this.getString(2131296647) + " Row");
      StationDetails.f(StationDetails.this);
    }
  }

  private final class d extends CustomAsyncTask
  {
    public d(GBActivity arg2)
    {
      super();
    }

    protected final void onPostExecute(Boolean paramBoolean)
    {
      super.onPostExecute(paramBoolean);
      if (paramBoolean.booleanValue())
      {
        ((TextView)StationDetails.this.findViewById(2131165535)).setVisibility(8);
        StationDetails.b(StationDetails.this);
        StationDetails.c(StationDetails.this);
        return;
      }
      ((TextView)StationDetails.this.findViewById(2131165535)).setText(StationDetails.this.getString(2131296311));
      StationDetails.d(StationDetails.this);
    }

    protected final boolean queryWebService()
    {
      StationDetails.e(StationDetails.this);
      return true;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.station.StationDetails
 * JD-Core Version:    0.6.2
 */