package gbis.gbandroid.activities.station;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.location.Location;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputFilter.LengthFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import gbis.gbandroid.activities.base.PhotoDialog;
import gbis.gbandroid.entities.Station;
import gbis.gbandroid.queries.StationPhotoUploaderQuery;
import gbis.gbandroid.utils.Base64;
import gbis.gbandroid.views.CustomDialog;
import gbis.gbandroid.views.CustomDialog.Builder;
import java.lang.reflect.Type;

public class StationPhotoUploader extends PhotoDialog
{
  private String a;

  private void a()
  {
    CustomDialog.Builder localBuilder = new CustomDialog.Builder(this);
    View localView1 = this.mInflater.inflate(2130903062, null);
    ListView localListView = (ListView)localView1.findViewById(2131165255);
    TextView localTextView1 = (TextView)localView1.findViewById(2131165261);
    View localView2 = this.mInflater.inflate(2130903061, null);
    EditText localEditText = (EditText)localView2.findViewById(2131165259);
    CheckBox localCheckBox = (CheckBox)localView2.findViewById(2131165260);
    TextView localTextView2 = (TextView)localView2.findViewById(2131165258);
    String[] arrayOfString = getResources().getStringArray(2131099664);
    localBuilder.setTitle(this.station.getStationName());
    localListView.setChoiceMode(1);
    localBuilder.setStationLogo(this.station.getGasBrandId(), "/Android/data/gbis.gbandroid/cache/Brands");
    localTextView1.setText(getString(2131296684));
    localTextView2.setText(getString(2131296693));
    localBuilder.setContentView(localView1);
    localBuilder.setPositiveButton(getString(2131296646), new an(this, localBuilder, localListView, localEditText));
    localEditText.setOnFocusChangeListener(new ao(this, localListView));
    InputFilter[] arrayOfInputFilter = new InputFilter[1];
    arrayOfInputFilter[0] = new InputFilter.LengthFilter(50);
    localEditText.setFilters(arrayOfInputFilter);
    localEditText.setImeOptions(6);
    localView2.setOnTouchListener(new ap(this, localView2, localListView, localEditText, localCheckBox, localTextView1));
    localListView.addFooterView(localView2, null, true);
    localListView.setAdapter(new ArrayAdapter(this, 2130903090, 2131165228, arrayOfString));
    CustomDialog localCustomDialog = localBuilder.create();
    localListView.setOnItemClickListener(new aq(this, localListView, arrayOfString, localEditText, localTextView1, localCheckBox));
    localCustomDialog.setOnCancelListener(new ar(this));
    localCustomDialog.show();
  }

  protected void getIntentExtras()
  {
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
    {
      this.station = ((Station)localBundle.getParcelable("station"));
      return;
    }
    finish();
  }

  protected void postPhotoResized()
  {
    a();
  }

  protected void postPhotoTaken()
  {
    Location localLocation = getLastKnownLocation();
    if (localLocation != null)
    {
      this.latitude = localLocation.getLatitude();
      this.longitude = localLocation.getLongitude();
      return;
    }
    this.latitude = 0.0D;
    this.longitude = 0.0D;
  }

  protected void queryPhotoUploaderWebService(byte[] paramArrayOfByte)
  {
    Type localType = new am(this).getType();
    this.mResponseObject = new StationPhotoUploaderQuery(this, this.mPrefs, localType, this.myLocation).getResponseObject(this.station.getStationId(), this.a, Base64.encodeBytes(paramArrayOfByte));
  }

  protected void setAdsAfterGPSServiceConnected()
  {
  }

  protected String setAnalyticsPageName()
  {
    return getString(2131296847);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.station.StationPhotoUploader
 * JD-Core Version:    0.6.2
 */