package gbis.gbandroid.activities.station;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;

final class a extends BitmapDrawable
{
  a(AddStationMap paramAddStationMap, Resources paramResources, Bitmap paramBitmap)
  {
    super(paramResources, paramBitmap);
  }

  public final void draw(Canvas paramCanvas)
  {
    super.draw(paramCanvas);
    paramCanvas.drawBitmap(AddStationMap.t(this.a), AddStationMap.u(this.a).getWidth() / 2 - AddStationMap.t(this.a).getWidth() / 2, 3.0F, null);
    invalidateSelf();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.station.a
 * JD-Core Version:    0.6.2
 */