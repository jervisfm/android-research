package gbis.gbandroid.activities.station;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.text.Editable;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import gbis.gbandroid.views.CustomDialog.Builder;

final class an
  implements DialogInterface.OnClickListener
{
  an(StationPhotoUploader paramStationPhotoUploader, CustomDialog.Builder paramBuilder, ListView paramListView, EditText paramEditText)
  {
  }

  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    this.b.getPositiveButton().setEnabled(false);
    if (this.c.getCheckedItemPosition() == -1)
      StationPhotoUploader.a(this.a, this.d.getText().toString());
    StationPhotoUploader.a(this.a);
    paramDialogInterface.dismiss();
    this.a.finish();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.station.an
 * JD-Core Version:    0.6.2
 */