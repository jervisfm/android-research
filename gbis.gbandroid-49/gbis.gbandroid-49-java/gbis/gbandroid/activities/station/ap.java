package gbis.gbandroid.activities.station;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

final class ap
  implements View.OnTouchListener
{
  ap(StationPhotoUploader paramStationPhotoUploader, View paramView, ListView paramListView, EditText paramEditText, CheckBox paramCheckBox, TextView paramTextView)
  {
  }

  public final boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    if (paramMotionEvent.getAction() == 1)
    {
      this.b.requestFocusFromTouch();
      this.b.clearFocus();
      this.c.setSelection(this.c.getLastVisiblePosition());
      this.d.setVisibility(0);
      this.e.setChecked(true);
      this.f.setVisibility(8);
      this.c.setDescendantFocusability(262144);
      this.d.requestFocus();
      ((InputMethodManager)this.a.getSystemService("input_method")).showSoftInput(this.d, 0);
    }
    return true;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.station.ap
 * JD-Core Version:    0.6.2
 */