package gbis.gbandroid.activities.station;

import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

final class aq
  implements AdapterView.OnItemClickListener
{
  aq(StationPhotoUploader paramStationPhotoUploader, ListView paramListView, String[] paramArrayOfString, EditText paramEditText, TextView paramTextView, CheckBox paramCheckBox)
  {
  }

  public final void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    this.b.setDescendantFocusability(131072);
    StationPhotoUploader.a(this.a, this.c[paramInt]);
    ((CheckedTextView)paramView).setChecked(true);
    ((InputMethodManager)this.a.getSystemService("input_method")).hideSoftInputFromWindow(this.d.getWindowToken(), 0);
    this.d.setVisibility(8);
    this.e.setVisibility(0);
    this.f.setChecked(false);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.station.aq
 * JD-Core Version:    0.6.2
 */