package gbis.gbandroid.activities.station;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.widget.ListView;
import gbis.gbandroid.entities.StationMessage;
import java.util.List;

final class e
  implements DialogInterface.OnClickListener
{
  e(AddStationMap paramAddStationMap, ListView paramListView)
  {
  }

  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    if (this.b.getCheckedItemPosition() != -1)
    {
      StationMessage localStationMessage = (StationMessage)AddStationMap.n(this.a).get(this.b.getCheckedItemPosition());
      AddStationMap.a(this.a, localStationMessage);
      this.a.finish();
      return;
    }
    this.a.showMessage(this.a.getString(2131296355));
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.station.e
 * JD-Core Version:    0.6.2
 */