package gbis.gbandroid.activities.station;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.AutoCompleteTextView;
import java.util.regex.Pattern;

final class l
  implements TextWatcher
{
  l(AddStationMap paramAddStationMap)
  {
  }

  public final void afterTextChanged(Editable paramEditable)
  {
  }

  public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
  }

  public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
    if (!Pattern.matches(".*\\p{Digit}.*", paramCharSequence))
    {
      if (paramCharSequence.length() != 2)
        break label46;
      new AddStationMap.c(this.a, AddStationMap.v(this.a)).execute(new Object[0]);
    }
    label46: 
    while (paramCharSequence.length() > 0)
      return;
    AddStationMap.e(this.a).dismissDropDown();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.station.l
 * JD-Core Version:    0.6.2
 */