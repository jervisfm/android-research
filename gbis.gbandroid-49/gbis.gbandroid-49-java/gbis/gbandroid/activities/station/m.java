package gbis.gbandroid.activities.station;

import android.text.Editable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.ListAdapter;
import android.widget.Spinner;

final class m
  implements AdapterView.OnItemClickListener
{
  m(AddStationMap paramAddStationMap)
  {
  }

  public final void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    String str1 = AddStationMap.e(this.a).getAdapter().getItem(paramInt).toString();
    String str2 = AddStationMap.a(str1);
    AddStationMap.w(this.a).setSelection(AddStationMap.e(this.a, str2));
    Editable localEditable = AddStationMap.e(this.a).getText();
    localEditable.delete(localEditable.toString().indexOf(","), localEditable.length());
    AddStationMap.a(this.a, AddStationMap.e(this.a));
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.station.m
 * JD-Core Version:    0.6.2
 */