package gbis.gbandroid.activities.station;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

final class q
  implements View.OnTouchListener
{
  q(FavAddStation paramFavAddStation, CheckBox paramCheckBox, TextView paramTextView)
  {
  }

  public final boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    if (paramMotionEvent.getAction() == 1)
    {
      FavAddStation.g(this.a).setVisibility(0);
      this.b.setChecked(true);
      this.c.setVisibility(8);
      FavAddStation.g(this.a).requestFocus();
      ((InputMethodManager)this.a.getSystemService("input_method")).showSoftInput(FavAddStation.g(this.a), 0);
    }
    return true;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.station.q
 * JD-Core Version:    0.6.2
 */