package gbis.gbandroid.activities.station;

import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import gbis.gbandroid.entities.FavListMessage;
import java.util.List;

final class r
  implements AdapterView.OnItemClickListener
{
  r(FavAddStation paramFavAddStation, TextView paramTextView, CheckBox paramCheckBox)
  {
  }

  public final void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    FavAddStation.a(this.a, (String)FavAddStation.f(this.a).getItemAtPosition(paramInt));
    FavAddStation.a(this.a, ((FavListMessage)FavAddStation.h(this.a).get(paramInt - 1)).getListId());
    ((CheckedTextView)paramView).setChecked(true);
    ((InputMethodManager)this.a.getSystemService("input_method")).hideSoftInputFromWindow(FavAddStation.g(this.a).getWindowToken(), 0);
    FavAddStation.g(this.a).setVisibility(8);
    this.b.setVisibility(0);
    this.c.setChecked(false);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.station.r
 * JD-Core Version:    0.6.2
 */