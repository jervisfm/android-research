package gbis.gbandroid.activities.station;

import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import gbis.gbandroid.entities.StationMessage;
import java.util.List;

final class w extends GestureDetector.SimpleOnGestureListener
{
  w(PictureView paramPictureView)
  {
  }

  public final boolean onFling(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2)
  {
    if ((paramMotionEvent1.getX() < paramMotionEvent2.getX()) && (PictureView.a(this.a) > 0))
      PictureView.b(this.a);
    while (true)
    {
      return true;
      if ((paramMotionEvent1.getX() > paramMotionEvent2.getX()) && (PictureView.a(this.a) < -1 + PictureView.c(this.a).getStationPhotos().size()))
        PictureView.d(this.a);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.station.w
 * JD-Core Version:    0.6.2
 */