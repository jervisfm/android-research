package gbis.gbandroid.activities.station;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.graphics.drawable.shapes.Shape;
import android.view.ViewGroup;
import gbis.gbandroid.entities.AdInLine;

final class z extends Shape
{
  z(StationDetails paramStationDetails, AdInLine paramAdInLine, ViewGroup paramViewGroup)
  {
  }

  public final void draw(Canvas paramCanvas, Paint paramPaint)
  {
    paramPaint.setColor(Color.parseColor(this.b.getStrokeColor()));
    paramPaint.setStyle(Paint.Style.STROKE);
    paramPaint.setStrokeWidth(2.0F);
    paramPaint.setAntiAlias(true);
    paramCanvas.drawRoundRect(new RectF(2.0F, 2.0F, -2 + this.c.getMeasuredWidth(), -2 + this.c.getMeasuredHeight()), 4.0F, 4.0F, paramPaint);
    paramPaint.setColor(Color.parseColor(this.b.getBackgroundColor()));
    paramPaint.setStyle(Paint.Style.FILL_AND_STROKE);
    paramCanvas.drawRoundRect(new RectF(4.0F, 4.0F, -4 + this.c.getMeasuredWidth(), -4 + this.c.getMeasuredHeight()), 2.0F, 2.0F, paramPaint);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.activities.station.z
 * JD-Core Version:    0.6.2
 */