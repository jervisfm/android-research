package gbis.gbandroid.animations;

import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;

public class MyScaleAnimation extends ScaleAnimation
{
  private View a;
  private ViewGroup.LayoutParams b;
  private boolean c = false;
  private int d;
  private boolean e;

  public MyScaleAnimation(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, int paramInt, View paramView, boolean paramBoolean)
  {
    super(paramFloat1, paramFloat2, paramFloat3, paramFloat4);
    setDuration(paramInt);
    this.a = paramView;
    this.c = paramBoolean;
    this.b = paramView.getLayoutParams();
    if (this.b.height < 0);
    for (int i = this.a.getHeight(); ; i = this.b.height)
    {
      this.d = i;
      if (paramFloat3 <= paramFloat4)
        break;
      this.e = true;
      return;
    }
    this.b.height = 1;
  }

  protected void applyTransformation(float paramFloat, Transformation paramTransformation)
  {
    super.applyTransformation(paramFloat, paramTransformation);
    if (paramFloat < 1.0F)
      if (this.e)
      {
        this.b.height = ((int)(this.d * (1.0F - paramFloat)));
        if ((this.a != null) && (this.a.getParent() != null))
          this.a.getParent().requestLayout();
      }
    while ((!this.c) || (this.a == null))
      while (true)
      {
        return;
        this.b.height = ((int)(paramFloat * this.d));
      }
    this.a.setVisibility(8);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.animations.MyScaleAnimation
 * JD-Core Version:    0.6.2
 */