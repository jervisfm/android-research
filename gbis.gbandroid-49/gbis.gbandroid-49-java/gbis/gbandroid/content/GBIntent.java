package gbis.gbandroid.content;

import android.content.Context;
import android.content.Intent;
import android.location.Location;

public class GBIntent extends Intent
{
  public GBIntent(Context paramContext, Class<?> paramClass, Location paramLocation)
  {
    super(paramContext, paramClass);
    putExtra("my location", paramLocation);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.content.GBIntent
 * JD-Core Version:    0.6.2
 */