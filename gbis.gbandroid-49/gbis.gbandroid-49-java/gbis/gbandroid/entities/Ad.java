package gbis.gbandroid.entities;

public class Ad
{
  private int networkId;
  private String networkKey;
  private String unitId;

  public int getNetworkId()
  {
    return this.networkId;
  }

  public String getNetworkKey()
  {
    return this.networkKey;
  }

  public String getUnitId()
  {
    return this.unitId;
  }

  public void setNetworkId(int paramInt)
  {
    this.networkId = paramInt;
  }

  public void setNetworkKey(String paramString)
  {
    this.networkKey = paramString;
  }

  public void setUnitId(String paramString)
  {
    this.unitId = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.Ad
 * JD-Core Version:    0.6.2
 */