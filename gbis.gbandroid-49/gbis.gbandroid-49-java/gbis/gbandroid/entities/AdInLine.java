package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gbson.annotations.SerializedName;

public class AdInLine
  implements Parcelable
{
  public static final Parcelable.Creator<AdInLine> CREATOR = new a();
  private String backgroundColor;
  private int height;
  private String html;
  private int position;

  @SerializedName("BorderColor")
  private String strokeColor;
  private String url;

  public AdInLine()
  {
  }

  private AdInLine(Parcel paramParcel)
  {
    readFromParcel(paramParcel);
  }

  private void readFromParcel(Parcel paramParcel)
  {
    this.backgroundColor = paramParcel.readString();
    this.strokeColor = paramParcel.readString();
    this.url = paramParcel.readString();
    this.html = paramParcel.readString();
    this.position = paramParcel.readInt();
    this.height = paramParcel.readInt();
  }

  public AdInLine copy()
  {
    AdInLine localAdInLine = new AdInLine();
    localAdInLine.backgroundColor = this.backgroundColor;
    localAdInLine.height = this.height;
    localAdInLine.html = this.html;
    localAdInLine.position = this.position;
    localAdInLine.strokeColor = this.strokeColor;
    localAdInLine.url = this.url;
    return localAdInLine;
  }

  public int describeContents()
  {
    return 0;
  }

  public String getBackgroundColor()
  {
    return this.backgroundColor;
  }

  public int getHeight()
  {
    return this.height;
  }

  public String getHtml()
  {
    return this.html;
  }

  public int getPosition()
  {
    return this.position;
  }

  public String getStrokeColor()
  {
    return this.strokeColor;
  }

  public String getUrl()
  {
    return this.url;
  }

  public void setBackgroundColor(String paramString)
  {
    this.backgroundColor = paramString;
  }

  public void setHeight(int paramInt)
  {
    this.height = paramInt;
  }

  public void setHtml(String paramString)
  {
    this.html = paramString;
  }

  public void setPosition(int paramInt)
  {
    this.position = paramInt;
  }

  public void setStrokeColor(String paramString)
  {
    this.strokeColor = paramString;
  }

  public void setUrl(String paramString)
  {
    this.url = paramString;
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("HTML: ");
    localStringBuilder.append(this.html);
    localStringBuilder.append('\n');
    localStringBuilder.append("Bck Color: ");
    localStringBuilder.append(this.backgroundColor);
    localStringBuilder.append('\n');
    localStringBuilder.append("Stroke Color: ");
    localStringBuilder.append(this.strokeColor);
    localStringBuilder.append('\n');
    localStringBuilder.append("URL: ");
    localStringBuilder.append(this.url);
    localStringBuilder.append('\n');
    localStringBuilder.append("Height: ");
    localStringBuilder.append(this.height);
    localStringBuilder.append('\n');
    localStringBuilder.append("Position: ");
    localStringBuilder.append(this.position);
    localStringBuilder.append('\n');
    return localStringBuilder.toString();
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeString(this.backgroundColor);
    paramParcel.writeString(this.strokeColor);
    paramParcel.writeString(this.url);
    paramParcel.writeString(this.html);
    paramParcel.writeInt(this.position);
    paramParcel.writeInt(this.height);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.AdInLine
 * JD-Core Version:    0.6.2
 */