package gbis.gbandroid.entities;

public class Ads
{
  private Ad details;
  private Ad favorites;
  private Ad home;
  private Ad init;
  private int initTime;
  private Ad list;
  private Ad listBottom;
  private int listScrollTime;
  private Ad listTop;
  private int listWaitTime;
  private Ad profile;

  public Ad getDetails()
  {
    return this.details;
  }

  public Ad getFavorites()
  {
    return this.favorites;
  }

  public Ad getHome()
  {
    return this.home;
  }

  public Ad getInit()
  {
    return this.init;
  }

  public int getInitTime()
  {
    return this.initTime;
  }

  public Ad getList()
  {
    return this.list;
  }

  public Ad getListBottom()
  {
    return this.listBottom;
  }

  public int getListScrollTime()
  {
    return this.listScrollTime;
  }

  public Ad getListTop()
  {
    return this.listTop;
  }

  public int getListWaitTime()
  {
    return this.listWaitTime;
  }

  public Ad getProfile()
  {
    return this.profile;
  }

  public void setDetails(Ad paramAd)
  {
    this.details = paramAd;
  }

  public void setFavorites(Ad paramAd)
  {
    this.favorites = paramAd;
  }

  public void setHome(Ad paramAd)
  {
    this.home = paramAd;
  }

  public void setInit(Ad paramAd)
  {
    this.init = paramAd;
  }

  public void setInitTime(int paramInt)
  {
    this.initTime = paramInt;
  }

  public void setList(Ad paramAd)
  {
    this.list = paramAd;
  }

  public void setListBottom(Ad paramAd)
  {
    this.listBottom = paramAd;
  }

  public void setListScrollTime(int paramInt)
  {
    this.listScrollTime = paramInt;
  }

  public void setListTop(Ad paramAd)
  {
    this.listTop = paramAd;
  }

  public void setListWaitTime(int paramInt)
  {
    this.listWaitTime = paramInt;
  }

  public void setProfile(Ad paramAd)
  {
    this.profile = paramAd;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.Ads
 * JD-Core Version:    0.6.2
 */