package gbis.gbandroid.entities;

public class AdsGSA
{
  private String anchorTextColor;
  private String backgroundColor;
  private BackgroundGradient backgroundGradient;
  private String borderColor;
  private int borderThickness;
  private int borderType;
  private String descriptionTextColor;
  private String fontFace;
  private String headerTextColor;
  private int headerTextSize;
  private String keywords;

  public String getAnchorTextColor()
  {
    return this.anchorTextColor;
  }

  public String getBackgroundColor()
  {
    return this.backgroundColor;
  }

  public BackgroundGradient getBackgroundGradient()
  {
    return this.backgroundGradient;
  }

  public String getBorderColor()
  {
    return this.borderColor;
  }

  public int getBorderThickness()
  {
    return this.borderThickness;
  }

  public int getBorderType()
  {
    return this.borderType;
  }

  public String getDescriptionTextColor()
  {
    return this.descriptionTextColor;
  }

  public String getFontFace()
  {
    return this.fontFace;
  }

  public String getHeaderTextColor()
  {
    return this.headerTextColor;
  }

  public int getHeaderTextSize()
  {
    return this.headerTextSize;
  }

  public String getKeywords()
  {
    return this.keywords;
  }

  public void setAnchorTextColor(String paramString)
  {
    this.anchorTextColor = paramString;
  }

  public void setBackgroundColor(String paramString)
  {
    this.backgroundColor = paramString;
  }

  public void setBackgroundGradient(BackgroundGradient paramBackgroundGradient)
  {
    this.backgroundGradient = paramBackgroundGradient;
  }

  public void setBorderColor(String paramString)
  {
    this.borderColor = paramString;
  }

  public void setBorderThickness(int paramInt)
  {
    this.borderThickness = paramInt;
  }

  public void setBorderType(int paramInt)
  {
    this.borderType = paramInt;
  }

  public void setDescriptionTextColor(String paramString)
  {
    this.descriptionTextColor = paramString;
  }

  public void setFontFace(String paramString)
  {
    this.fontFace = paramString;
  }

  public void setHeaderTextColor(String paramString)
  {
    this.headerTextColor = paramString;
  }

  public void setHeaderTextSize(int paramInt)
  {
    this.headerTextSize = paramInt;
  }

  public void setKeywords(String paramString)
  {
    this.keywords = paramString;
  }

  public class BackgroundGradient
  {
    private String fromColor;
    private String toColor;

    public BackgroundGradient()
    {
    }

    public String getFromColor()
    {
      return this.fromColor;
    }

    public String getToColor()
    {
      return this.toColor;
    }

    public void setFromColor(String paramString)
    {
      this.fromColor = paramString;
    }

    public void setToColor(String paramString)
    {
      this.toColor = paramString;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.AdsGSA
 * JD-Core Version:    0.6.2
 */