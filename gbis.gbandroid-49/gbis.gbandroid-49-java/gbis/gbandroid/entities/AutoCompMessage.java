package gbis.gbandroid.entities;

import com.google.gbson.annotations.SerializedName;

public class AutoCompMessage
  implements Comparable<AutoCompMessage>
{

  @SerializedName("c")
  private String city;

  @SerializedName("s")
  private String state;

  public int compareTo(AutoCompMessage paramAutoCompMessage)
  {
    return 1;
  }

  public AutoCompMessage copy()
  {
    AutoCompMessage localAutoCompMessage = new AutoCompMessage();
    localAutoCompMessage.city = this.city;
    localAutoCompMessage.state = this.state;
    return localAutoCompMessage;
  }

  public String getCity()
  {
    return this.city;
  }

  public String getState()
  {
    return this.state;
  }

  public void setCity(String paramString)
  {
    this.city = paramString;
  }

  public void setState(String paramString)
  {
    this.state = paramString;
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.city);
    localStringBuilder.append(", ");
    localStringBuilder.append(this.state);
    return localStringBuilder.toString();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.AutoCompMessage
 * JD-Core Version:    0.6.2
 */