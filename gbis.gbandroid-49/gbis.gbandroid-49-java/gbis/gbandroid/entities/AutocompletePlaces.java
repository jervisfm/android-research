package gbis.gbandroid.entities;

import android.graphics.drawable.Drawable;

public class AutocompletePlaces
{
  private Drawable icon;
  private boolean isPlace;
  private String text;

  public AutocompletePlaces(String paramString, Drawable paramDrawable, boolean paramBoolean)
  {
    this.text = paramString;
    this.isPlace = paramBoolean;
    setIcon(paramDrawable);
  }

  public AutocompletePlaces(String paramString, boolean paramBoolean)
  {
    setText(paramString);
    setPlace(paramBoolean);
  }

  public Drawable getIcon()
  {
    return this.icon;
  }

  public String getText()
  {
    return this.text;
  }

  public boolean isPlace()
  {
    return this.isPlace;
  }

  public void setIcon(Drawable paramDrawable)
  {
    this.icon = paramDrawable;
  }

  public void setPlace(boolean paramBoolean)
  {
    this.isPlace = paramBoolean;
  }

  public void setText(String paramString)
  {
    this.text = paramString;
  }

  public String toString()
  {
    return this.text;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.AutocompletePlaces
 * JD-Core Version:    0.6.2
 */