package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class AwardsMessage
  implements Parcelable, Comparable<AwardsMessage>
{
  public static final Parcelable.Creator<AwardsMessage> CREATOR = new b();
  private int actionCount;
  private String addDate;
  private int awardId;
  private int awarded;
  private String color;
  private String completedMessage;
  private String description;
  private int difference;
  private int level;
  private String message;
  private int milestoneId;
  private String title;
  private int totalTimes;

  public AwardsMessage()
  {
  }

  private AwardsMessage(Parcel paramParcel)
  {
    readFromParcel(paramParcel);
  }

  public int compareTo(AwardsMessage paramAwardsMessage)
  {
    return 1;
  }

  public AwardsMessage copy()
  {
    AwardsMessage localAwardsMessage = new AwardsMessage();
    localAwardsMessage.actionCount = this.actionCount;
    localAwardsMessage.addDate = this.addDate;
    localAwardsMessage.awarded = this.awarded;
    localAwardsMessage.awardId = this.awardId;
    localAwardsMessage.color = this.color;
    localAwardsMessage.completedMessage = this.completedMessage;
    localAwardsMessage.description = this.description;
    localAwardsMessage.difference = this.difference;
    localAwardsMessage.level = this.level;
    localAwardsMessage.milestoneId = this.milestoneId;
    localAwardsMessage.message = this.message;
    localAwardsMessage.totalTimes = this.totalTimes;
    localAwardsMessage.title = this.title;
    return localAwardsMessage;
  }

  public int describeContents()
  {
    return 0;
  }

  public int getActionCount()
  {
    return this.actionCount;
  }

  public String getAddDate()
  {
    return this.addDate;
  }

  public int getAwardId()
  {
    return this.awardId;
  }

  public int getAwarded()
  {
    return this.awarded;
  }

  public String getColor()
  {
    return this.color;
  }

  public String getCompletedMessage()
  {
    return this.completedMessage;
  }

  public String getDescription()
  {
    return this.description;
  }

  public int getDifference()
  {
    return this.difference;
  }

  public int getLevel()
  {
    return this.level;
  }

  public String getMessage()
  {
    return this.message;
  }

  public int getMilestoneId()
  {
    return this.milestoneId;
  }

  public String getTitle()
  {
    return this.title;
  }

  public int getTotalTimes()
  {
    return this.totalTimes;
  }

  protected void readFromParcel(Parcel paramParcel)
  {
    this.title = paramParcel.readString();
    this.level = paramParcel.readInt();
    this.color = paramParcel.readString();
    this.awardId = paramParcel.readInt();
    this.milestoneId = paramParcel.readInt();
    this.description = paramParcel.readString();
    this.actionCount = paramParcel.readInt();
    this.totalTimes = paramParcel.readInt();
    this.addDate = paramParcel.readString();
    this.message = paramParcel.readString();
    this.awarded = paramParcel.readInt();
    this.completedMessage = paramParcel.readString();
    this.difference = paramParcel.readInt();
  }

  public void setActionCount(int paramInt)
  {
    this.actionCount = paramInt;
  }

  public void setAddDate(String paramString)
  {
    this.addDate = paramString;
  }

  public void setAwardId(int paramInt)
  {
    this.awardId = paramInt;
  }

  public void setAwarded(int paramInt)
  {
    this.awarded = paramInt;
  }

  public void setColor(String paramString)
  {
    this.color = paramString;
  }

  public void setCompletedMessage(String paramString)
  {
    this.completedMessage = paramString;
  }

  public void setDescription(String paramString)
  {
    this.description = paramString;
  }

  public void setDifference(int paramInt)
  {
    this.difference = paramInt;
  }

  public void setLevel(int paramInt)
  {
    this.level = paramInt;
  }

  public void setMessage(String paramString)
  {
    this.message = paramString;
  }

  public void setMilestoneId(int paramInt)
  {
    this.milestoneId = paramInt;
  }

  public void setTitle(String paramString)
  {
    this.title = paramString;
  }

  public void setTotalTimes(int paramInt)
  {
    this.totalTimes = paramInt;
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Title: ");
    localStringBuilder.append(this.title);
    localStringBuilder.append('\n');
    localStringBuilder.append("Level: ");
    localStringBuilder.append(this.level);
    localStringBuilder.append('\n');
    localStringBuilder.append("Award Id: ");
    localStringBuilder.append(this.awardId);
    localStringBuilder.append('\n');
    localStringBuilder.append("Milestone Id: ");
    localStringBuilder.append(this.milestoneId);
    localStringBuilder.append('\n');
    localStringBuilder.append("Description: ");
    localStringBuilder.append(this.description);
    localStringBuilder.append('\n');
    localStringBuilder.append("Message: ");
    localStringBuilder.append(this.message);
    localStringBuilder.append('\n');
    localStringBuilder.append("Add Date: ");
    localStringBuilder.append(this.addDate);
    localStringBuilder.append('\n');
    return localStringBuilder.toString();
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeString(this.title);
    paramParcel.writeInt(this.level);
    paramParcel.writeString(this.color);
    paramParcel.writeInt(this.awardId);
    paramParcel.writeInt(this.milestoneId);
    paramParcel.writeString(this.description);
    paramParcel.writeInt(this.actionCount);
    paramParcel.writeInt(this.totalTimes);
    paramParcel.writeString(this.addDate);
    paramParcel.writeString(this.message);
    paramParcel.writeInt(this.awarded);
    paramParcel.writeString(this.completedMessage);
    paramParcel.writeInt(this.difference);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.AwardsMessage
 * JD-Core Version:    0.6.2
 */