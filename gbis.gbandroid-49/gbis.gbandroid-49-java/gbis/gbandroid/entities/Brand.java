package gbis.gbandroid.entities;

public class Brand
{
  private int gasBrandId;
  private int gasBrandVersion;

  public int getGasBrandId()
  {
    return this.gasBrandId;
  }

  public int getGasBrandVersion()
  {
    return this.gasBrandVersion;
  }

  public void setGasBrandId(int paramInt)
  {
    this.gasBrandId = paramInt;
  }

  public void setGasBrandVersion(int paramInt)
  {
    this.gasBrandVersion = paramInt;
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.Brand
 * JD-Core Version:    0.6.2
 */