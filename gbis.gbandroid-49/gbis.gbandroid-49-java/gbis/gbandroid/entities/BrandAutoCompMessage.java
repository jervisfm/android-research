package gbis.gbandroid.entities;

import com.google.gbson.annotations.SerializedName;

public class BrandAutoCompMessage
  implements Comparable<BrandAutoCompMessage>
{

  @SerializedName("n")
  private String brand;

  @SerializedName("c")
  private int count;

  public int compareTo(BrandAutoCompMessage paramBrandAutoCompMessage)
  {
    return 1;
  }

  public BrandAutoCompMessage copy()
  {
    BrandAutoCompMessage localBrandAutoCompMessage = new BrandAutoCompMessage();
    localBrandAutoCompMessage.brand = this.brand;
    localBrandAutoCompMessage.count = this.count;
    return localBrandAutoCompMessage;
  }

  public String getBrand()
  {
    return this.brand;
  }

  public int getCount()
  {
    return this.count;
  }

  public void setBrand(String paramString)
  {
    this.brand = paramString;
  }

  public void setCount(int paramInt)
  {
    this.count = paramInt;
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Brand: ");
    localStringBuilder.append(this.brand);
    localStringBuilder.append('\n');
    localStringBuilder.append("Count: ");
    localStringBuilder.append(this.count);
    localStringBuilder.append('\n');
    return localStringBuilder.toString();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.BrandAutoCompMessage
 * JD-Core Version:    0.6.2
 */