package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class Center
  implements Parcelable
{
  public static final Parcelable.Creator<Center> CREATOR = new c();
  private double latitude;
  private double longitude;

  public Center()
  {
  }

  private Center(Parcel paramParcel)
  {
    readFromParcel(paramParcel);
  }

  private void readFromParcel(Parcel paramParcel)
  {
    this.latitude = paramParcel.readDouble();
    this.longitude = paramParcel.readDouble();
  }

  public int describeContents()
  {
    return 0;
  }

  public double getLatitude()
  {
    return this.latitude;
  }

  public double getLongitude()
  {
    return this.longitude;
  }

  public void setLatitude(double paramDouble)
  {
    this.latitude = paramDouble;
  }

  public void setLongitude(double paramDouble)
  {
    this.longitude = paramDouble;
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Latitude: ");
    localStringBuilder.append(this.latitude);
    localStringBuilder.append('\n');
    localStringBuilder.append("Longitude: ");
    localStringBuilder.append(this.longitude);
    localStringBuilder.append('\n');
    return localStringBuilder.toString();
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeDouble(this.latitude);
    paramParcel.writeDouble(this.longitude);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.Center
 * JD-Core Version:    0.6.2
 */