package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class ChoiceHotels
  implements Parcelable
{
  public static final Parcelable.Creator<ChoiceHotels> CREATOR = new d();
  private int adId;
  private String address;
  private String address2;
  private String brand;
  private String brandId;
  private String city;
  private String clickText;
  private String country;
  private double latitude;
  private double longitude;
  private String name;
  private String phone;
  private String postalCode;
  private String state;
  private String url;

  public ChoiceHotels()
  {
  }

  private ChoiceHotels(Parcel paramParcel)
  {
    try
    {
      readFromParcel(paramParcel);
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }

  public int describeContents()
  {
    return 0;
  }

  public int getAdId()
  {
    return this.adId;
  }

  public String getAddress()
  {
    return this.address;
  }

  public String getAddress2()
  {
    return this.address2;
  }

  public String getBrand()
  {
    return this.brand;
  }

  public String getBrandId()
  {
    return this.brandId;
  }

  public String getCity()
  {
    return this.city;
  }

  public String getClickText()
  {
    return this.clickText;
  }

  public String getCountry()
  {
    return this.country;
  }

  public double getLatitude()
  {
    return this.latitude;
  }

  public double getLongitude()
  {
    return this.longitude;
  }

  public String getName()
  {
    return this.name;
  }

  public String getPhone()
  {
    return this.phone;
  }

  public String getPostalCode()
  {
    return this.postalCode;
  }

  public String getState()
  {
    return this.state;
  }

  public String getUrl()
  {
    return this.url;
  }

  protected void readFromParcel(Parcel paramParcel)
  {
    this.adId = paramParcel.readInt();
    this.brand = paramParcel.readString();
    this.brandId = paramParcel.readString();
    this.name = paramParcel.readString();
    this.address = paramParcel.readString();
    this.address2 = paramParcel.readString();
    this.city = paramParcel.readString();
    this.state = paramParcel.readString();
    this.postalCode = paramParcel.readString();
    this.country = paramParcel.readString();
    this.phone = paramParcel.readString();
    this.latitude = paramParcel.readDouble();
    this.longitude = paramParcel.readDouble();
    this.url = paramParcel.readString();
    this.clickText = paramParcel.readString();
  }

  public void setAdId(int paramInt)
  {
    this.adId = paramInt;
  }

  public void setAddress(String paramString)
  {
    this.address = paramString;
  }

  public void setAddress2(String paramString)
  {
    this.address2 = paramString;
  }

  public void setBrand(String paramString)
  {
    this.brand = paramString;
  }

  public void setBrandId(String paramString)
  {
    this.brandId = paramString;
  }

  public void setCity(String paramString)
  {
    this.city = paramString;
  }

  public void setClickText(String paramString)
  {
    this.clickText = paramString;
  }

  public void setCountry(String paramString)
  {
    this.country = paramString;
  }

  public void setLatitude(double paramDouble)
  {
    this.latitude = paramDouble;
  }

  public void setLongitude(double paramDouble)
  {
    this.longitude = paramDouble;
  }

  public void setName(String paramString)
  {
    this.name = paramString;
  }

  public void setPhone(String paramString)
  {
    this.phone = paramString;
  }

  public void setPostalCode(String paramString)
  {
    this.postalCode = paramString;
  }

  public void setState(String paramString)
  {
    this.state = paramString;
  }

  public void setUrl(String paramString)
  {
    this.url = paramString;
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeInt(this.adId);
    paramParcel.writeString(this.brand);
    paramParcel.writeString(this.brandId);
    paramParcel.writeString(this.name);
    paramParcel.writeString(this.address);
    paramParcel.writeString(this.address2);
    paramParcel.writeString(this.city);
    paramParcel.writeString(this.state);
    paramParcel.writeString(this.postalCode);
    paramParcel.writeString(this.country);
    paramParcel.writeString(this.phone);
    paramParcel.writeDouble(this.latitude);
    paramParcel.writeDouble(this.longitude);
    paramParcel.writeString(this.url);
    paramParcel.writeString(this.clickText);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.ChoiceHotels
 * JD-Core Version:    0.6.2
 */