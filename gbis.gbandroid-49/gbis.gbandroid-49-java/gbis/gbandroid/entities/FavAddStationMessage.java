package gbis.gbandroid.entities;

public class FavAddStationMessage
{
  private int listId;

  public FavAddStationMessage copy()
  {
    FavAddStationMessage localFavAddStationMessage = new FavAddStationMessage();
    localFavAddStationMessage.listId = this.listId;
    return localFavAddStationMessage;
  }

  public int getLisId()
  {
    return this.listId;
  }

  public void setListId(String paramString)
  {
    this.listId = new Integer(paramString).intValue();
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("List Id: ");
    localStringBuilder.append(this.listId);
    localStringBuilder.append('\n');
    return localStringBuilder.toString();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.FavAddStationMessage
 * JD-Core Version:    0.6.2
 */