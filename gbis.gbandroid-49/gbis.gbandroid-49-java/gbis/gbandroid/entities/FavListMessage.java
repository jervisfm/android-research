package gbis.gbandroid.entities;

public class FavListMessage
  implements Comparable<FavListMessage>
{
  private boolean defaultYn;
  private int listId;
  private String listName;

  public int compareTo(FavListMessage paramFavListMessage)
  {
    return 1;
  }

  public FavListMessage copy()
  {
    FavListMessage localFavListMessage = new FavListMessage();
    localFavListMessage.defaultYn = this.defaultYn;
    localFavListMessage.listId = this.listId;
    localFavListMessage.listName = this.listName;
    return localFavListMessage;
  }

  public int getListId()
  {
    return this.listId;
  }

  public String getListName()
  {
    return this.listName;
  }

  public boolean isDefaultYn()
  {
    return this.defaultYn;
  }

  public void setDefaultYn(boolean paramBoolean)
  {
    this.defaultYn = paramBoolean;
  }

  public void setListId(int paramInt)
  {
    this.listId = paramInt;
  }

  public void setListName(String paramString)
  {
    this.listName = paramString;
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("List Id: ");
    localStringBuilder.append(this.listId);
    localStringBuilder.append('\n');
    localStringBuilder.append("List Name: ");
    localStringBuilder.append(this.listName);
    localStringBuilder.append('\n');
    localStringBuilder.append("Default Yn: ");
    localStringBuilder.append(this.defaultYn);
    localStringBuilder.append('\n');
    return localStringBuilder.toString();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.FavListMessage
 * JD-Core Version:    0.6.2
 */