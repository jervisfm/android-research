package gbis.gbandroid.entities;

import java.math.BigInteger;

public class FavReportMessage
{
  private String carIcon;
  private int carIconId;
  private BigInteger maxPriceId;
  private BigInteger minPriceId;
  private int pointBalance;
  private int totalPointsAwarded;

  public FavReportMessage copy()
  {
    FavReportMessage localFavReportMessage = new FavReportMessage();
    localFavReportMessage.carIcon = this.carIcon;
    localFavReportMessage.carIconId = this.carIconId;
    localFavReportMessage.minPriceId = this.minPriceId;
    localFavReportMessage.maxPriceId = this.maxPriceId;
    localFavReportMessage.totalPointsAwarded = this.totalPointsAwarded;
    localFavReportMessage.pointBalance = this.pointBalance;
    return localFavReportMessage;
  }

  public String getCarIcon()
  {
    return this.carIcon;
  }

  public int getCarIconId()
  {
    return this.carIconId;
  }

  public BigInteger getMaxPriceId()
  {
    return this.maxPriceId;
  }

  public BigInteger getMinPriceId()
  {
    return this.minPriceId;
  }

  public int getPointBalance()
  {
    return this.pointBalance;
  }

  public int getTotalPointsAwarded()
  {
    return this.totalPointsAwarded;
  }

  public void setCarIcon(String paramString)
  {
    this.carIcon = paramString;
  }

  public void setCarIconId(String paramString)
  {
    this.carIconId = Integer.parseInt(paramString);
  }

  public void setMaxPriceId(String paramString)
  {
    this.maxPriceId = new BigInteger(paramString);
  }

  public void setMinPriceId(String paramString)
  {
    this.minPriceId = new BigInteger(paramString);
  }

  public void setPointBalance(String paramString)
  {
    this.pointBalance = Integer.parseInt(paramString);
  }

  public void setTotalPointsAwarded(String paramString)
  {
    this.totalPointsAwarded = Integer.parseInt(paramString);
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Min Price: ");
    localStringBuilder.append(this.minPriceId);
    localStringBuilder.append('\n');
    localStringBuilder.append("Midgrade Price: ");
    localStringBuilder.append(this.maxPriceId);
    localStringBuilder.append('\n');
    localStringBuilder.append("Total Points Awarded: ");
    localStringBuilder.append(this.totalPointsAwarded);
    localStringBuilder.append('\n');
    localStringBuilder.append("Point Balance: ");
    localStringBuilder.append(this.pointBalance);
    localStringBuilder.append('\n');
    localStringBuilder.append("Car Icon: ");
    localStringBuilder.append(this.carIcon);
    localStringBuilder.append('\n');
    localStringBuilder.append("Car Icon Id: ");
    localStringBuilder.append(this.carIconId);
    localStringBuilder.append('\n');
    return localStringBuilder.toString();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.FavReportMessage
 * JD-Core Version:    0.6.2
 */