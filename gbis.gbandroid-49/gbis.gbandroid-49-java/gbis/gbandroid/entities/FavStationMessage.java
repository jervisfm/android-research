package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.gbson.annotations.SerializedName;

public class FavStationMessage extends StationDetailed
  implements Comparable<FavStationMessage>
{
  public static final Parcelable.Creator<FavStationMessage> CREATOR = new e();
  private String comments;
  private double diffFromAvg;
  private int listId;
  private int memberFavId;

  @SerializedName("MSLMatchStatus")
  private int mslMatchStatus;
  private double siteAvg;

  public FavStationMessage()
  {
  }

  private FavStationMessage(Parcel paramParcel)
  {
    readFromParcel(paramParcel);
  }

  public int compareTo(FavStationMessage paramFavStationMessage)
  {
    return 1;
  }

  public FavStationMessage copy()
  {
    FavStationMessage localFavStationMessage = new FavStationMessage();
    localFavStationMessage.address = this.address;
    localFavStationMessage.city = this.city;
    localFavStationMessage.comments = this.comments;
    localFavStationMessage.country = this.country;
    localFavStationMessage.dieselPrice = this.dieselPrice;
    localFavStationMessage.dieselTimeSpotted = this.dieselTimeSpotted;
    localFavStationMessage.dieselMemberId = this.dieselMemberId;
    localFavStationMessage.dieselCar = this.dieselCar;
    localFavStationMessage.diffFromAvg = this.diffFromAvg;
    localFavStationMessage.gasBrandId = this.gasBrandId;
    localFavStationMessage.gasBrandVersion = this.gasBrandVersion;
    localFavStationMessage.latitude = this.latitude;
    localFavStationMessage.listId = this.listId;
    localFavStationMessage.longitude = this.longitude;
    localFavStationMessage.memberFavId = this.memberFavId;
    localFavStationMessage.midPrice = this.midPrice;
    localFavStationMessage.midTimeSpotted = this.midTimeSpotted;
    localFavStationMessage.midMemberId = this.midMemberId;
    localFavStationMessage.midCar = this.midCar;
    localFavStationMessage.mslMatchStatus = this.mslMatchStatus;
    localFavStationMessage.premPrice = this.premPrice;
    localFavStationMessage.premTimeSpotted = this.premTimeSpotted;
    localFavStationMessage.premMemberId = this.premMemberId;
    localFavStationMessage.premCar = this.premCar;
    localFavStationMessage.regPrice = this.regPrice;
    localFavStationMessage.regTimeSpotted = this.regTimeSpotted;
    localFavStationMessage.regMemberId = this.regMemberId;
    localFavStationMessage.regCar = this.regCar;
    localFavStationMessage.siteAvg = this.siteAvg;
    localFavStationMessage.state = this.state;
    localFavStationMessage.stationId = this.stationId;
    localFavStationMessage.stationName = this.stationName;
    localFavStationMessage.timeOffset = this.timeOffset;
    return localFavStationMessage;
  }

  public int describeContents()
  {
    return 0;
  }

  public String getComments()
  {
    return this.comments;
  }

  public double getDiffFromAvg()
  {
    return this.diffFromAvg;
  }

  public int getListId()
  {
    return this.listId;
  }

  public int getMemberFavId()
  {
    return this.memberFavId;
  }

  public int getMslMatchStatus()
  {
    return this.mslMatchStatus;
  }

  public double getSiteAvg()
  {
    return this.siteAvg;
  }

  protected void readFromParcel(Parcel paramParcel)
  {
    super.readFromParcel(paramParcel);
    this.memberFavId = paramParcel.readInt();
    this.mslMatchStatus = paramParcel.readInt();
    this.comments = paramParcel.readString();
    this.listId = paramParcel.readInt();
    this.diffFromAvg = paramParcel.readDouble();
    this.siteAvg = paramParcel.readDouble();
  }

  public void setComments(String paramString)
  {
    this.comments = paramString;
  }

  public void setDiffFromAvg(double paramDouble)
  {
    this.diffFromAvg = paramDouble;
  }

  public void setListId(int paramInt)
  {
    this.listId = paramInt;
  }

  public void setMemberFavId(int paramInt)
  {
    this.memberFavId = paramInt;
  }

  public void setMslMatchStatus(int paramInt)
  {
    this.mslMatchStatus = paramInt;
  }

  public void setSiteAvg(double paramDouble)
  {
    this.siteAvg = paramDouble;
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(super.toString());
    localStringBuilder.append("Member Favourite Id: ");
    localStringBuilder.append(this.memberFavId);
    localStringBuilder.append('\n');
    localStringBuilder.append("MSL Match Status: ");
    localStringBuilder.append(this.mslMatchStatus);
    localStringBuilder.append('\n');
    localStringBuilder.append("Station Id: ");
    localStringBuilder.append(this.stationId);
    localStringBuilder.append('\n');
    localStringBuilder.append("Comments: ");
    localStringBuilder.append(this.comments);
    localStringBuilder.append('\n');
    localStringBuilder.append("List Id: ");
    localStringBuilder.append(this.listId);
    localStringBuilder.append('\n');
    localStringBuilder.append("Diff From Avg: ");
    localStringBuilder.append(this.diffFromAvg);
    localStringBuilder.append('\n');
    localStringBuilder.append("Site Avg: ");
    localStringBuilder.append(this.siteAvg);
    localStringBuilder.append('\n');
    localStringBuilder.append("Time Offset: ");
    localStringBuilder.append(this.timeOffset);
    localStringBuilder.append('\n');
    return localStringBuilder.toString();
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    paramParcel.writeInt(this.memberFavId);
    paramParcel.writeInt(this.mslMatchStatus);
    paramParcel.writeString(this.comments);
    paramParcel.writeInt(this.listId);
    paramParcel.writeDouble(this.diffFromAvg);
    paramParcel.writeDouble(this.siteAvg);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.FavStationMessage
 * JD-Core Version:    0.6.2
 */