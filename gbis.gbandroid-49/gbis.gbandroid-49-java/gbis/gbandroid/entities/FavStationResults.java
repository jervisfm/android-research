package gbis.gbandroid.entities;

import com.google.gbson.annotations.SerializedName;
import java.util.List;

public class FavStationResults
{

  @SerializedName("MemberFavoriteStationCollection")
  private List<FavStationMessage> favStationMessage;
  private int totalStations;

  public FavStationResults copy()
  {
    FavStationResults localFavStationResults = new FavStationResults();
    localFavStationResults.favStationMessage = this.favStationMessage;
    localFavStationResults.totalStations = this.totalStations;
    return localFavStationResults;
  }

  public List<FavStationMessage> getFavStationMessage()
  {
    return this.favStationMessage;
  }

  public int getTotalStations()
  {
    return this.totalStations;
  }

  public void setFavStationMessage(List<FavStationMessage> paramList)
  {
    this.favStationMessage = paramList;
  }

  public void setTotalStations(int paramInt)
  {
    this.totalStations = paramInt;
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Fav Stations: ");
    localStringBuilder.append(this.favStationMessage.toString());
    localStringBuilder.append('\n');
    localStringBuilder.append("Total Stations: ");
    localStringBuilder.append(this.totalStations);
    localStringBuilder.append('\n');
    return localStringBuilder.toString();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.FavStationResults
 * JD-Core Version:    0.6.2
 */