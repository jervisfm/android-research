package gbis.gbandroid.entities;

public class GeocodeMessage
{
  private String address;
  private String city;
  private double latitude;
  private double longitude;
  private String postalCode;
  private String state;

  public GeocodeMessage copy()
  {
    GeocodeMessage localGeocodeMessage = new GeocodeMessage();
    localGeocodeMessage.address = this.address;
    localGeocodeMessage.city = this.city;
    localGeocodeMessage.latitude = this.latitude;
    localGeocodeMessage.longitude = this.longitude;
    localGeocodeMessage.state = this.state;
    localGeocodeMessage.postalCode = this.postalCode;
    return localGeocodeMessage;
  }

  public String getAddress()
  {
    return this.address;
  }

  public String getCity()
  {
    return this.city;
  }

  public double getLatitude()
  {
    return this.latitude;
  }

  public double getLongitude()
  {
    return this.longitude;
  }

  public String getPostalCode()
  {
    return this.postalCode;
  }

  public String getState()
  {
    return this.state;
  }

  public void setAddress(String paramString)
  {
    this.address = paramString;
  }

  public void setCity(String paramString)
  {
    this.city = paramString;
  }

  public void setLatitude(double paramDouble)
  {
    this.latitude = paramDouble;
  }

  public void setLongitude(double paramDouble)
  {
    this.longitude = paramDouble;
  }

  public void setPostalCode(String paramString)
  {
    this.postalCode = paramString;
  }

  public void setState(String paramString)
  {
    this.state = paramString;
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Address: ");
    localStringBuilder.append(this.address);
    localStringBuilder.append('\n');
    localStringBuilder.append("City: ");
    localStringBuilder.append(this.city);
    localStringBuilder.append('\n');
    localStringBuilder.append("State: ");
    localStringBuilder.append(this.state);
    localStringBuilder.append('\n');
    localStringBuilder.append("Latitde: ");
    localStringBuilder.append(this.latitude);
    localStringBuilder.append('\n');
    localStringBuilder.append("Longitude: ");
    localStringBuilder.append(this.longitude);
    localStringBuilder.append('\n');
    localStringBuilder.append("Postal Code: ");
    localStringBuilder.append(this.postalCode);
    localStringBuilder.append('\n');
    return localStringBuilder.toString();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.GeocodeMessage
 * JD-Core Version:    0.6.2
 */