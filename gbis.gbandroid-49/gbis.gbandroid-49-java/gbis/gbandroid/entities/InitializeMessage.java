package gbis.gbandroid.entities;

import com.google.gbson.annotations.SerializedName;

public class InitializeMessage
{
  private int action;
  private Ads ads;
  private String authId;
  private boolean debug;

  @SerializedName("GSA")
  private AdsGSA gsa;
  private String host;

  public InitializeMessage copy()
  {
    InitializeMessage localInitializeMessage = new InitializeMessage();
    localInitializeMessage.action = this.action;
    localInitializeMessage.ads = this.ads;
    localInitializeMessage.authId = this.authId;
    localInitializeMessage.debug = this.debug;
    localInitializeMessage.gsa = this.gsa;
    localInitializeMessage.host = this.host;
    return localInitializeMessage;
  }

  public int getAction()
  {
    return this.action;
  }

  public Ads getAds()
  {
    return this.ads;
  }

  public String getAuthId()
  {
    return this.authId;
  }

  public AdsGSA getGSA()
  {
    return this.gsa;
  }

  public String getHost()
  {
    return this.host;
  }

  public boolean isDebug()
  {
    return this.debug;
  }

  public void setAction(int paramInt)
  {
    this.action = paramInt;
  }

  public void setAds(Ads paramAds)
  {
    this.ads = paramAds;
  }

  public void setAuthId(String paramString)
  {
    this.authId = paramString;
  }

  public void setDebug(boolean paramBoolean)
  {
    this.debug = paramBoolean;
  }

  public void setGSA(AdsGSA paramAdsGSA)
  {
    this.gsa = paramAdsGSA;
  }

  public void setHost(String paramString)
  {
    this.host = paramString;
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Action: ");
    localStringBuilder.append(this.action);
    localStringBuilder.append('\n');
    localStringBuilder.append("Auth Id: ");
    localStringBuilder.append(this.authId);
    localStringBuilder.append('\n');
    localStringBuilder.append("Host: ");
    localStringBuilder.append(this.host);
    localStringBuilder.append('\n');
    localStringBuilder.append("Debug: ");
    localStringBuilder.append(this.debug);
    localStringBuilder.append('\n');
    return localStringBuilder.toString();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.InitializeMessage
 * JD-Core Version:    0.6.2
 */