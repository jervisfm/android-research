package gbis.gbandroid.entities;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import gbis.gbandroid.utils.DateUtils;
import java.math.BigInteger;

public class ListMessage extends Station
  implements Comparable<ListMessage>
{
  public static final Parcelable.Creator<ListMessage> CREATOR = new f();
  private String car;
  private String carName;
  private String comments;
  private double distance;
  private boolean isNear;
  private String memberId;
  private double price;
  private BigInteger priceId;
  private int sortOrder;
  private String timeSpotted;

  public ListMessage()
  {
  }

  private ListMessage(Parcel paramParcel)
  {
    readFromParcel(paramParcel);
  }

  public int compareTo(ListMessage paramListMessage)
  {
    return 1;
  }

  public ListMessage copy()
  {
    ListMessage localListMessage = new ListMessage();
    localListMessage.address = this.address;
    localListMessage.car = this.car;
    localListMessage.carName = this.carName;
    localListMessage.city = this.city;
    localListMessage.comments = this.comments;
    localListMessage.country = this.country;
    localListMessage.crossStreet = this.crossStreet;
    localListMessage.distance = this.distance;
    localListMessage.gasBrandId = this.gasBrandId;
    localListMessage.gasBrandVersion = this.gasBrandVersion;
    localListMessage.isNear = this.isNear;
    localListMessage.latitude = this.latitude;
    localListMessage.longitude = this.longitude;
    localListMessage.memberId = this.memberId;
    localListMessage.notIntersection = this.notIntersection;
    localListMessage.postalCode = this.postalCode;
    localListMessage.price = this.price;
    localListMessage.priceId = this.priceId;
    localListMessage.state = this.state;
    localListMessage.stationId = this.stationId;
    localListMessage.stationName = this.stationName;
    localListMessage.timeSpotted = this.timeSpotted;
    localListMessage.timeOffset = this.timeOffset;
    localListMessage.sortOrder = this.sortOrder;
    return localListMessage;
  }

  public int describeContents()
  {
    return 0;
  }

  public String getCar()
  {
    return this.car;
  }

  public String getCarName()
  {
    return this.carName;
  }

  public String getComments()
  {
    return this.comments;
  }

  public double getDistance()
  {
    return this.distance;
  }

  public String getMemberId()
  {
    return this.memberId;
  }

  public double getPrice()
  {
    return this.price;
  }

  public double getPrice(Context paramContext, String paramString)
  {
    return this.price;
  }

  public BigInteger getPriceId()
  {
    return this.priceId;
  }

  public int getSortOrder()
  {
    return this.sortOrder;
  }

  public String getTimeSpotted()
  {
    return this.timeSpotted;
  }

  public String getTimeSpottedConverted(Context paramContext, String paramString)
  {
    return DateUtils.compareDateToNow(DateUtils.toDateFormat(this.timeSpotted), this.timeOffset);
  }

  public boolean isNear()
  {
    return this.isNear;
  }

  protected void readFromParcel(Parcel paramParcel)
  {
    super.readFromParcel(paramParcel);
    this.priceId = new BigInteger(paramParcel.readString());
    this.price = paramParcel.readDouble();
    this.timeSpotted = paramParcel.readString();
    this.distance = paramParcel.readDouble();
    this.memberId = paramParcel.readString();
    this.car = paramParcel.readString();
    this.carName = paramParcel.readString();
    this.comments = paramParcel.readString();
    boolean[] arrayOfBoolean = new boolean[1];
    paramParcel.readBooleanArray(arrayOfBoolean);
    this.isNear = arrayOfBoolean[0];
    this.sortOrder = paramParcel.readInt();
  }

  public void setCar(String paramString)
  {
    this.car = paramString;
  }

  public void setCarName(String paramString)
  {
    this.carName = paramString;
  }

  public void setComments(String paramString)
  {
    this.comments = paramString;
  }

  public void setDistance(double paramDouble)
  {
    this.distance = paramDouble;
  }

  public void setMemberId(String paramString)
  {
    this.memberId = paramString;
  }

  public void setNear(boolean paramBoolean)
  {
    this.isNear = paramBoolean;
  }

  public void setPrice(double paramDouble)
  {
    this.price = paramDouble;
  }

  public void setPriceId(BigInteger paramBigInteger)
  {
    this.priceId = paramBigInteger;
  }

  public void setSortOrder(int paramInt)
  {
    this.sortOrder = paramInt;
  }

  public void setTimeSpotted(String paramString)
  {
    this.timeSpotted = paramString;
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(super.toString());
    localStringBuilder.append("Distance: ");
    localStringBuilder.append(this.distance);
    localStringBuilder.append('\n');
    localStringBuilder.append("Price Id: ");
    localStringBuilder.append(this.priceId);
    localStringBuilder.append('\n');
    localStringBuilder.append("Price: ");
    localStringBuilder.append(this.price);
    localStringBuilder.append('\n');
    localStringBuilder.append("Time Spotted: ");
    localStringBuilder.append(this.timeSpotted);
    localStringBuilder.append('\n');
    localStringBuilder.append("Time Offset: ");
    localStringBuilder.append(this.timeOffset);
    localStringBuilder.append('\n');
    localStringBuilder.append("Member Id: ");
    localStringBuilder.append(this.memberId);
    localStringBuilder.append('\n');
    localStringBuilder.append("Car: ");
    localStringBuilder.append(this.car);
    localStringBuilder.append('\n');
    localStringBuilder.append("Car Name: ");
    localStringBuilder.append(this.carName);
    localStringBuilder.append('\n');
    localStringBuilder.append("Comments: ");
    localStringBuilder.append(this.comments);
    localStringBuilder.append('\n');
    localStringBuilder.append("Is Near: ");
    localStringBuilder.append(this.isNear);
    localStringBuilder.append('\n');
    return localStringBuilder.toString();
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    if (this.priceId != null);
    for (String str = this.priceId.toString(); ; str = "0")
    {
      paramParcel.writeString(str);
      paramParcel.writeDouble(this.price);
      paramParcel.writeString(this.timeSpotted);
      paramParcel.writeDouble(this.distance);
      paramParcel.writeString(this.memberId);
      paramParcel.writeString(this.car);
      paramParcel.writeString(this.carName);
      paramParcel.writeString(this.comments);
      boolean[] arrayOfBoolean = new boolean[1];
      arrayOfBoolean[0] = this.isNear;
      paramParcel.writeBooleanArray(arrayOfBoolean);
      paramParcel.writeInt(this.sortOrder);
      return;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.ListMessage
 * JD-Core Version:    0.6.2
 */