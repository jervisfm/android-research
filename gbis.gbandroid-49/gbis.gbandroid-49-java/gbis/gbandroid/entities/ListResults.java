package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gbson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

public class ListResults
  implements Parcelable
{
  public static final Parcelable.Creator<ListResults> CREATOR = new g();
  private Center center;

  @SerializedName("PriceCollection")
  private List<ListMessage> listMessages;
  private String searchTerms;
  private int totalNear;

  @SerializedName("Total")
  private int totalStations;

  public ListResults()
  {
  }

  private ListResults(Parcel paramParcel)
  {
    readFromParcel(paramParcel);
  }

  public ListResults copy()
  {
    ListResults localListResults = new ListResults();
    localListResults.center = this.center;
    localListResults.listMessages = this.listMessages;
    localListResults.searchTerms = this.searchTerms;
    localListResults.totalNear = this.totalNear;
    localListResults.totalStations = this.totalStations;
    return localListResults;
  }

  public int describeContents()
  {
    return 0;
  }

  public Center getCenter()
  {
    return this.center;
  }

  public List<ListMessage> getListMessages()
  {
    return this.listMessages;
  }

  public String getSearchTerms()
  {
    return this.searchTerms;
  }

  public int getTotalNear()
  {
    return this.totalNear;
  }

  public int getTotalStations()
  {
    return this.totalStations;
  }

  protected void readFromParcel(Parcel paramParcel)
  {
    this.listMessages = new ArrayList();
    paramParcel.readList(this.listMessages, ListMessage.class.getClassLoader());
    this.totalNear = paramParcel.readInt();
    this.totalStations = paramParcel.readInt();
    this.searchTerms = paramParcel.readString();
    this.center = ((Center)paramParcel.readParcelable(Center.class.getClassLoader()));
  }

  public void setCenter(Center paramCenter)
  {
    this.center = paramCenter;
  }

  public void setListMessages(List<ListMessage> paramList)
  {
    this.listMessages = paramList;
  }

  public void setSearchTerms(String paramString)
  {
    this.searchTerms = paramString;
  }

  public void setTotalNear(int paramInt)
  {
    this.totalNear = paramInt;
  }

  public void setTotalStations(int paramInt)
  {
    this.totalStations = paramInt;
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Station Collection: ");
    localStringBuilder.append(this.listMessages.toString());
    localStringBuilder.append('\n');
    localStringBuilder.append("Search Terms: ");
    localStringBuilder.append(this.searchTerms);
    localStringBuilder.append('\n');
    localStringBuilder.append("Center: ");
    localStringBuilder.append(this.center);
    localStringBuilder.append('\n');
    localStringBuilder.append("Search Terms: ");
    localStringBuilder.append(this.searchTerms);
    localStringBuilder.append('\n');
    localStringBuilder.append("Total Near: ");
    localStringBuilder.append(this.totalNear);
    localStringBuilder.append('\n');
    localStringBuilder.append("Total Stations: ");
    localStringBuilder.append(this.totalStations);
    localStringBuilder.append('\n');
    return localStringBuilder.toString();
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeList(this.listMessages);
    paramParcel.writeInt(this.totalNear);
    paramParcel.writeInt(this.totalStations);
    paramParcel.writeString(this.searchTerms);
    paramParcel.writeParcelable(this.center, paramInt);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.ListResults
 * JD-Core Version:    0.6.2
 */