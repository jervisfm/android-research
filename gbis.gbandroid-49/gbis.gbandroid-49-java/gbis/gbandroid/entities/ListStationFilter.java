package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.ArrayList;
import java.util.List;

public class ListStationFilter
  implements Parcelable
{
  public static final Parcelable.Creator<ListStationFilter> CREATOR = new h();
  private double distance;
  private boolean hasPrices;
  private double price;
  private List<StationNameFilter> stationNames;

  public ListStationFilter()
  {
    this.stationNames = new ArrayList();
  }

  public ListStationFilter(double paramDouble1, double paramDouble2, boolean paramBoolean, List<StationNameFilter> paramList)
  {
    this.price = paramDouble1;
    this.distance = paramDouble2;
    this.hasPrices = paramBoolean;
    this.stationNames = paramList;
  }

  private ListStationFilter(Parcel paramParcel)
  {
    readFromParcel(paramParcel);
  }

  public int describeContents()
  {
    return 0;
  }

  public double getDistance()
  {
    return this.distance;
  }

  public double getPrice()
  {
    return this.price;
  }

  public List<StationNameFilter> getStationNames()
  {
    return this.stationNames;
  }

  public boolean hasPrices()
  {
    return this.hasPrices;
  }

  protected void readFromParcel(Parcel paramParcel)
  {
    this.price = paramParcel.readDouble();
    this.distance = paramParcel.readDouble();
    boolean[] arrayOfBoolean = new boolean[1];
    paramParcel.readBooleanArray(arrayOfBoolean);
    this.hasPrices = arrayOfBoolean[0];
    this.stationNames = new ArrayList();
    paramParcel.readList(this.stationNames, StationNameFilter.class.getClassLoader());
  }

  public void setDistance(double paramDouble)
  {
    this.distance = paramDouble;
  }

  public void setHasPrices(boolean paramBoolean)
  {
    this.hasPrices = paramBoolean;
  }

  public void setPrice(double paramDouble)
  {
    this.price = paramDouble;
  }

  public void setStationNames(List<StationNameFilter> paramList)
  {
    this.stationNames = paramList;
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeDouble(this.price);
    paramParcel.writeDouble(this.distance);
    boolean[] arrayOfBoolean = new boolean[1];
    arrayOfBoolean[0] = this.hasPrices;
    paramParcel.writeBooleanArray(arrayOfBoolean);
    paramParcel.writeList(this.stationNames);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.ListStationFilter
 * JD-Core Version:    0.6.2
 */