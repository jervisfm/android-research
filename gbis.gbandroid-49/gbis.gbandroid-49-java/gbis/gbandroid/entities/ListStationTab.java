package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.ArrayList;
import java.util.List;

public class ListStationTab
  implements Parcelable
{
  public static final Parcelable.Creator<ListStationTab> CREATOR = new i();
  private String fuelType;
  private List<ListMessage> listStations;
  private List<ListMessage> listStationsFiltered;
  private int page;
  private int sortOrder;
  private ListStationFilter stationFilter;
  private int totalNear;
  private int totalStations;

  public ListStationTab()
  {
  }

  private ListStationTab(Parcel paramParcel)
  {
    readFromParcel(paramParcel);
  }

  public int describeContents()
  {
    return 0;
  }

  public String getFuelType()
  {
    return this.fuelType;
  }

  public List<ListMessage> getListStations()
  {
    return this.listStations;
  }

  public List<ListMessage> getListStationsFiltered()
  {
    return this.listStationsFiltered;
  }

  public int getPage()
  {
    return this.page;
  }

  public int getSortOrder()
  {
    return this.sortOrder;
  }

  public ListStationFilter getStationFilter()
  {
    return this.stationFilter;
  }

  public int getTotalNear()
  {
    return this.totalNear;
  }

  public int getTotalStations()
  {
    return this.totalStations;
  }

  protected void readFromParcel(Parcel paramParcel)
  {
    this.listStations = new ArrayList();
    paramParcel.readList(this.listStations, ListMessage.class.getClassLoader());
    this.listStationsFiltered = new ArrayList();
    paramParcel.readList(this.listStationsFiltered, ListMessage.class.getClassLoader());
    this.sortOrder = paramParcel.readInt();
    this.page = paramParcel.readInt();
    this.totalStations = paramParcel.readInt();
    this.totalNear = paramParcel.readInt();
    this.fuelType = paramParcel.readString();
    this.stationFilter = ((ListStationFilter)paramParcel.readParcelable(ListStationFilter.class.getClassLoader()));
  }

  public void setFuelType(String paramString)
  {
    this.fuelType = paramString;
  }

  public void setListStations(List<ListMessage> paramList)
  {
    this.listStations = paramList;
  }

  public void setListStationsFiltered(List<ListMessage> paramList)
  {
    this.listStationsFiltered = paramList;
  }

  public void setPage(int paramInt)
  {
    this.page = paramInt;
  }

  public void setSortOrder(int paramInt)
  {
    this.sortOrder = paramInt;
  }

  public void setStationFilter(ListStationFilter paramListStationFilter)
  {
    this.stationFilter = paramListStationFilter;
  }

  public void setTotalNear(int paramInt)
  {
    this.totalNear = paramInt;
  }

  public void setTotalStations(int paramInt)
  {
    this.totalStations = paramInt;
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeList(this.listStations);
    paramParcel.writeList(this.listStationsFiltered);
    paramParcel.writeInt(this.sortOrder);
    paramParcel.writeInt(this.page);
    paramParcel.writeInt(this.totalStations);
    paramParcel.writeInt(this.totalNear);
    paramParcel.writeString(this.fuelType);
    paramParcel.writeParcelable(this.stationFilter, paramInt);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.ListStationTab
 * JD-Core Version:    0.6.2
 */