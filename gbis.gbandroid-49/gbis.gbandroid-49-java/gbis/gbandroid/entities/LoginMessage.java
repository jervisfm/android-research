package gbis.gbandroid.entities;

public class LoginMessage
{
  private String car;
  private int carIconId;
  private String memberId;
  private boolean signedIn;

  public LoginMessage copy()
  {
    LoginMessage localLoginMessage = new LoginMessage();
    localLoginMessage.signedIn = this.signedIn;
    localLoginMessage.car = this.car;
    localLoginMessage.carIconId = this.carIconId;
    localLoginMessage.memberId = this.memberId;
    return localLoginMessage;
  }

  public String getCar()
  {
    return this.car;
  }

  public int getCarIconId()
  {
    return this.carIconId;
  }

  public String getMemberId()
  {
    return this.memberId;
  }

  public boolean isSignedIn()
  {
    return this.signedIn;
  }

  public void setCar(String paramString)
  {
    this.car = paramString;
  }

  public void setCarIconId(String paramString)
  {
    this.carIconId = Integer.parseInt(paramString);
  }

  public void setMemberId(String paramString)
  {
    this.memberId = paramString;
  }

  public void setSignedIn(boolean paramBoolean)
  {
    this.signedIn = paramBoolean;
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Signed In: ");
    localStringBuilder.append(this.signedIn);
    localStringBuilder.append('\n');
    localStringBuilder.append("Car: ");
    localStringBuilder.append(this.car);
    localStringBuilder.append('\n');
    localStringBuilder.append("Car Icon Id: ");
    localStringBuilder.append(this.carIconId);
    localStringBuilder.append('\n');
    localStringBuilder.append("Member ID: ");
    localStringBuilder.append(this.memberId);
    localStringBuilder.append('\n');
    return localStringBuilder.toString();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.LoginMessage
 * JD-Core Version:    0.6.2
 */