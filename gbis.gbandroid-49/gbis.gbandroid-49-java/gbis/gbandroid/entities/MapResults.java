package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gbson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

public class MapResults
  implements Parcelable
{
  public static final Parcelable.Creator<MapResults> CREATOR = new j();
  private boolean adsOnTop;

  @SerializedName("Ads")
  private List<ChoiceHotels> choiceHotels;

  @SerializedName("Stations")
  private List<ListMessage> listMessages;
  private int totalStations;

  public MapResults()
  {
  }

  private MapResults(Parcel paramParcel)
  {
    readFromParcel(paramParcel);
  }

  public MapResults copy()
  {
    MapResults localMapResults = new MapResults();
    localMapResults.listMessages = this.listMessages;
    localMapResults.totalStations = this.totalStations;
    localMapResults.choiceHotels = this.choiceHotels;
    localMapResults.adsOnTop = this.adsOnTop;
    return localMapResults;
  }

  public int describeContents()
  {
    return 0;
  }

  public List<ChoiceHotels> getChoiceHotels()
  {
    return this.choiceHotels;
  }

  public List<ListMessage> getListMessage()
  {
    return this.listMessages;
  }

  public int getTotalStations()
  {
    return this.totalStations;
  }

  public boolean isAdsOnTop()
  {
    return this.adsOnTop;
  }

  protected void readFromParcel(Parcel paramParcel)
  {
    this.listMessages = new ArrayList();
    paramParcel.readList(this.listMessages, ListMessage.class.getClassLoader());
    this.totalStations = paramParcel.readInt();
    this.choiceHotels = new ArrayList();
    paramParcel.readList(this.choiceHotels, ChoiceHotels.class.getClassLoader());
    boolean[] arrayOfBoolean = new boolean[1];
    paramParcel.readBooleanArray(arrayOfBoolean);
    this.adsOnTop = arrayOfBoolean[0];
  }

  public void setAdsOnTop(boolean paramBoolean)
  {
    this.adsOnTop = paramBoolean;
  }

  public void setChoiceHotels(List<ChoiceHotels> paramList)
  {
    this.choiceHotels = paramList;
  }

  public void setListMessage(List<ListMessage> paramList)
  {
    this.listMessages = paramList;
  }

  public void setTotalStations(int paramInt)
  {
    this.totalStations = paramInt;
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("List Stations: ");
    localStringBuilder.append(this.listMessages.toString());
    localStringBuilder.append('\n');
    localStringBuilder.append("Total Stations: ");
    localStringBuilder.append(this.totalStations);
    localStringBuilder.append('\n');
    return localStringBuilder.toString();
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeList(this.listMessages);
    paramParcel.writeInt(this.totalStations);
    paramParcel.writeList(this.choiceHotels);
    boolean[] arrayOfBoolean = new boolean[1];
    arrayOfBoolean[0] = this.adsOnTop;
    paramParcel.writeBooleanArray(arrayOfBoolean);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.MapResults
 * JD-Core Version:    0.6.2
 */