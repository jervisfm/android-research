package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class PriceShareMessages
  implements Parcelable
{
  public static final Parcelable.Creator<PriceShareMessages> CREATOR = new k();
  private String allFuelTypesMessage;
  private String dieselMessage;
  private String midgradeMessage;
  private String premiumMessage;
  private String regularMessage;

  public PriceShareMessages()
  {
  }

  private PriceShareMessages(Parcel paramParcel)
  {
    readFromParcel(paramParcel);
  }

  private void readFromParcel(Parcel paramParcel)
  {
    this.regularMessage = paramParcel.readString();
    this.midgradeMessage = paramParcel.readString();
    this.premiumMessage = paramParcel.readString();
    this.dieselMessage = paramParcel.readString();
    this.allFuelTypesMessage = paramParcel.readString();
  }

  public int describeContents()
  {
    return 0;
  }

  public String getAllFuelTypesMessage()
  {
    return this.allFuelTypesMessage;
  }

  public String getDieselMessage()
  {
    return this.dieselMessage;
  }

  public String getMidgradeMessage()
  {
    return this.midgradeMessage;
  }

  public String getPremiumMessage()
  {
    return this.premiumMessage;
  }

  public String getRegularMessage()
  {
    return this.regularMessage;
  }

  public void setAllFuelTypesMessage(String paramString)
  {
    this.allFuelTypesMessage = paramString;
  }

  public void setDieselMessage(String paramString)
  {
    this.dieselMessage = paramString;
  }

  public void setMidgradeMessage(String paramString)
  {
    this.midgradeMessage = paramString;
  }

  public void setPremiumMessage(String paramString)
  {
    this.premiumMessage = paramString;
  }

  public void setRegularMessage(String paramString)
  {
    this.regularMessage = paramString;
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Regular Message: ");
    localStringBuilder.append(this.regularMessage);
    localStringBuilder.append('\n');
    localStringBuilder.append("Midgrade Message: ");
    localStringBuilder.append(this.midgradeMessage);
    localStringBuilder.append('\n');
    localStringBuilder.append("Premium Message: ");
    localStringBuilder.append(this.premiumMessage);
    localStringBuilder.append('\n');
    localStringBuilder.append("Diesel Message: ");
    localStringBuilder.append(this.dieselMessage);
    localStringBuilder.append('\n');
    localStringBuilder.append("All Fuel Types Message: ");
    localStringBuilder.append(this.allFuelTypesMessage);
    localStringBuilder.append('\n');
    return localStringBuilder.toString();
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeString(this.regularMessage);
    paramParcel.writeString(this.midgradeMessage);
    paramParcel.writeString(this.premiumMessage);
    paramParcel.writeString(this.dieselMessage);
    paramParcel.writeString(this.allFuelTypesMessage);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.PriceShareMessages
 * JD-Core Version:    0.6.2
 */