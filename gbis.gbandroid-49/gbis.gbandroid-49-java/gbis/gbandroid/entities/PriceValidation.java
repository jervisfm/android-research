package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class PriceValidation
  implements Parcelable
{
  public static final Parcelable.Creator<PriceValidation> CREATOR = new l();
  private double commonDecreaseDiesel;
  private double commonDecreaseRegular;
  private double commonIncreaseDiesel;
  private double commonIncreaseRegular;
  private double expectedDieselPrice;
  private double expectedRegularPrice;
  private double offsetMidgrade;
  private double offsetPremium;
  private double radius;

  public PriceValidation()
  {
  }

  private PriceValidation(Parcel paramParcel)
  {
    readFromParcel(paramParcel);
  }

  private void readFromParcel(Parcel paramParcel)
  {
    this.expectedRegularPrice = paramParcel.readDouble();
    this.commonIncreaseRegular = paramParcel.readDouble();
    this.commonDecreaseRegular = paramParcel.readDouble();
    this.offsetMidgrade = paramParcel.readDouble();
    this.offsetPremium = paramParcel.readDouble();
    this.expectedDieselPrice = paramParcel.readDouble();
    this.commonIncreaseDiesel = paramParcel.readDouble();
    this.commonDecreaseDiesel = paramParcel.readDouble();
    this.radius = paramParcel.readDouble();
  }

  public int describeContents()
  {
    return 0;
  }

  public double getCommonDecreaseDiesel()
  {
    return this.commonDecreaseDiesel;
  }

  public double getCommonDecreaseRegular()
  {
    return this.commonDecreaseRegular;
  }

  public double getCommonIncreaseDiesel()
  {
    return this.commonIncreaseDiesel;
  }

  public double getCommonIncreaseRegular()
  {
    return this.commonIncreaseRegular;
  }

  public double getExpectedDieselPrice()
  {
    return this.expectedDieselPrice;
  }

  public double getExpectedRegularPrice()
  {
    return this.expectedRegularPrice;
  }

  public double getOffsetMidgrade()
  {
    return this.offsetMidgrade;
  }

  public double getOffsetPremium()
  {
    return this.offsetPremium;
  }

  public double getRadius()
  {
    return this.radius;
  }

  public void setCommonDecreaseDiesel(double paramDouble)
  {
    this.commonDecreaseDiesel = paramDouble;
  }

  public void setCommonDecreaseRegular(double paramDouble)
  {
    this.commonDecreaseRegular = paramDouble;
  }

  public void setCommonIncreaseDiesel(double paramDouble)
  {
    this.commonIncreaseDiesel = paramDouble;
  }

  public void setCommonIncreaseRegular(double paramDouble)
  {
    this.commonIncreaseRegular = paramDouble;
  }

  public void setExpectedDieselPrice(double paramDouble)
  {
    this.expectedDieselPrice = paramDouble;
  }

  public void setExpectedRegularPrice(double paramDouble)
  {
    this.expectedRegularPrice = paramDouble;
  }

  public void setOffsetMidgrade(double paramDouble)
  {
    this.offsetMidgrade = paramDouble;
  }

  public void setOffsetPremium(double paramDouble)
  {
    this.offsetPremium = paramDouble;
  }

  public void setRadius(double paramDouble)
  {
    this.radius = paramDouble;
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Expected Regular Price: ");
    localStringBuilder.append(this.expectedRegularPrice);
    localStringBuilder.append('\n');
    localStringBuilder.append("Common Increase Regular: ");
    localStringBuilder.append(this.commonIncreaseRegular);
    localStringBuilder.append('\n');
    localStringBuilder.append("Common Decrease Regular: ");
    localStringBuilder.append(this.commonDecreaseRegular);
    localStringBuilder.append('\n');
    localStringBuilder.append("Offset Midgrade: ");
    localStringBuilder.append(this.offsetMidgrade);
    localStringBuilder.append('\n');
    localStringBuilder.append("Offset Premium: ");
    localStringBuilder.append(this.offsetMidgrade);
    localStringBuilder.append('\n');
    localStringBuilder.append("Expected Diesel Price: ");
    localStringBuilder.append(this.expectedDieselPrice);
    localStringBuilder.append('\n');
    localStringBuilder.append("Common Increase Diesel: ");
    localStringBuilder.append(this.commonIncreaseDiesel);
    localStringBuilder.append('\n');
    localStringBuilder.append("Common Decrease Diesel: ");
    localStringBuilder.append(this.commonDecreaseDiesel);
    localStringBuilder.append('\n');
    localStringBuilder.append("Radius: ");
    localStringBuilder.append(this.radius);
    localStringBuilder.append('\n');
    return localStringBuilder.toString();
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeDouble(this.expectedRegularPrice);
    paramParcel.writeDouble(this.commonIncreaseRegular);
    paramParcel.writeDouble(this.commonDecreaseRegular);
    paramParcel.writeDouble(this.offsetMidgrade);
    paramParcel.writeDouble(this.offsetPremium);
    paramParcel.writeDouble(this.expectedDieselPrice);
    paramParcel.writeDouble(this.commonIncreaseDiesel);
    paramParcel.writeDouble(this.commonDecreaseDiesel);
    paramParcel.writeDouble(this.radius);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.PriceValidation
 * JD-Core Version:    0.6.2
 */