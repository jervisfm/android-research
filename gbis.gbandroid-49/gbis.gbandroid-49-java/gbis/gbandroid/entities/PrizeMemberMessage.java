package gbis.gbandroid.entities;

public class PrizeMemberMessage
{
  private String address1;
  private String address2;
  private String city;
  private String country;
  private String email;
  private String firstName;
  private String lastName;
  private int pointBalance;
  private String postalCode;
  private String state;
  private int ticketsAvailable;
  private int ticketsPurchased;
  private int totalPoints;

  public PrizeMemberMessage copy()
  {
    PrizeMemberMessage localPrizeMemberMessage = new PrizeMemberMessage();
    localPrizeMemberMessage.address1 = this.address1;
    localPrizeMemberMessage.address2 = this.address2;
    localPrizeMemberMessage.city = this.city;
    localPrizeMemberMessage.country = this.country;
    localPrizeMemberMessage.email = this.email;
    localPrizeMemberMessage.firstName = this.firstName;
    localPrizeMemberMessage.lastName = this.lastName;
    localPrizeMemberMessage.pointBalance = this.pointBalance;
    localPrizeMemberMessage.postalCode = this.postalCode;
    localPrizeMemberMessage.state = this.state;
    localPrizeMemberMessage.ticketsAvailable = this.ticketsAvailable;
    localPrizeMemberMessage.ticketsPurchased = this.ticketsPurchased;
    localPrizeMemberMessage.totalPoints = this.totalPoints;
    return localPrizeMemberMessage;
  }

  public String getAddress1()
  {
    return this.address1;
  }

  public String getAddress2()
  {
    return this.address2;
  }

  public String getCity()
  {
    return this.city;
  }

  public String getCountry()
  {
    return this.country;
  }

  public String getEmail()
  {
    return this.email;
  }

  public String getFirstName()
  {
    return this.firstName;
  }

  public String getLastName()
  {
    return this.lastName;
  }

  public int getPointBalance()
  {
    return this.pointBalance;
  }

  public String getPostalCode()
  {
    return this.postalCode;
  }

  public String getState()
  {
    return this.state;
  }

  public int getTicketsAvailable()
  {
    return this.ticketsAvailable;
  }

  public int getTicketsPurchased()
  {
    return this.ticketsPurchased;
  }

  public int getTotalPoints()
  {
    return this.totalPoints;
  }

  public void setAddress1(String paramString)
  {
    this.address1 = paramString;
  }

  public void setAddress2(String paramString)
  {
    this.address2 = paramString;
  }

  public void setCity(String paramString)
  {
    this.city = paramString;
  }

  public void setCountry(String paramString)
  {
    this.country = paramString;
  }

  public void setEmail(String paramString)
  {
    this.email = paramString;
  }

  public void setFirstName(String paramString)
  {
    this.firstName = paramString;
  }

  public void setLastName(String paramString)
  {
    this.lastName = paramString;
  }

  public void setPointBalance(String paramString)
  {
    this.pointBalance = new Integer(paramString).intValue();
  }

  public void setPostalCode(String paramString)
  {
    this.postalCode = paramString;
  }

  public void setState(String paramString)
  {
    this.state = paramString;
  }

  public void setTicketsAvailable(String paramString)
  {
    this.ticketsAvailable = new Integer(paramString).intValue();
  }

  public void setTicketsPurchased(String paramString)
  {
    this.ticketsPurchased = new Integer(paramString).intValue();
  }

  public void setTotalPoints(String paramString)
  {
    this.totalPoints = new Integer(paramString).intValue();
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Email: ");
    localStringBuilder.append(this.email);
    localStringBuilder.append('\n');
    localStringBuilder.append("First Name: ");
    localStringBuilder.append(this.firstName);
    localStringBuilder.append('\n');
    localStringBuilder.append("Last Name: ");
    localStringBuilder.append(this.lastName);
    localStringBuilder.append('\n');
    localStringBuilder.append("Address1: ");
    localStringBuilder.append(this.address1);
    localStringBuilder.append('\n');
    localStringBuilder.append("Address2: ");
    localStringBuilder.append(this.address2);
    localStringBuilder.append('\n');
    localStringBuilder.append("City: ");
    localStringBuilder.append(this.city);
    localStringBuilder.append('\n');
    localStringBuilder.append("State: ");
    localStringBuilder.append(this.state);
    localStringBuilder.append('\n');
    localStringBuilder.append("Country: ");
    localStringBuilder.append(this.country);
    localStringBuilder.append('\n');
    localStringBuilder.append("Postal Code: ");
    localStringBuilder.append(this.postalCode);
    localStringBuilder.append('\n');
    localStringBuilder.append("Total Points: ");
    localStringBuilder.append(this.totalPoints);
    localStringBuilder.append('\n');
    localStringBuilder.append("Point Balance: ");
    localStringBuilder.append(this.pointBalance);
    localStringBuilder.append('\n');
    localStringBuilder.append("Tickets Purchased: ");
    localStringBuilder.append(this.ticketsPurchased);
    localStringBuilder.append('\n');
    localStringBuilder.append("Tickets Available: ");
    localStringBuilder.append(this.ticketsAvailable);
    localStringBuilder.append('\n');
    return localStringBuilder.toString();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.PrizeMemberMessage
 * JD-Core Version:    0.6.2
 */