package gbis.gbandroid.entities;

import com.google.gbson.annotations.SerializedName;
import java.util.List;

public class PrizeResults
{

  @SerializedName("MemberPrizeDraw")
  private PrizeMemberMessage prizeMemberMessage;
  private PrizeStringsMessage prizeStringsMessage;

  @SerializedName("Strings")
  private List<String> strings;

  private PrizeStringsMessage setMessagesStrings()
  {
    PrizeStringsMessage localPrizeStringsMessage = new PrizeStringsMessage();
    localPrizeStringsMessage.setExplanation((String)this.strings.get(0));
    localPrizeStringsMessage.setTitle((String)this.strings.get(1));
    localPrizeStringsMessage.setDate((String)this.strings.get(2));
    localPrizeStringsMessage.setTotalPoints((String)this.strings.get(3));
    localPrizeStringsMessage.setLimit((String)this.strings.get(4));
    return localPrizeStringsMessage;
  }

  public PrizeResults copy()
  {
    PrizeResults localPrizeResults = new PrizeResults();
    localPrizeResults.prizeMemberMessage = this.prizeMemberMessage;
    localPrizeResults.prizeStringsMessage = this.prizeStringsMessage;
    return localPrizeResults;
  }

  public PrizeMemberMessage getPrizeMemberMessage()
  {
    return this.prizeMemberMessage;
  }

  public PrizeStringsMessage getPrizeStringsMessage()
  {
    if (this.prizeStringsMessage == null)
      this.prizeStringsMessage = setMessagesStrings();
    return this.prizeStringsMessage;
  }

  public List<String> getStrings()
  {
    return this.strings;
  }

  public void setPrizeMemberMessage(PrizeMemberMessage paramPrizeMemberMessage)
  {
    this.prizeMemberMessage = paramPrizeMemberMessage;
  }

  public void setPrizeStringsMessage(PrizeStringsMessage paramPrizeStringsMessage)
  {
    this.prizeStringsMessage = paramPrizeStringsMessage;
  }

  public void setStrings(List<String> paramList)
  {
    this.strings = paramList;
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Member Info: ");
    localStringBuilder.append(this.prizeMemberMessage.toString());
    localStringBuilder.append('\n');
    localStringBuilder.append("Strings: ");
    localStringBuilder.append(this.prizeStringsMessage.toString());
    localStringBuilder.append('\n');
    return localStringBuilder.toString();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.PrizeResults
 * JD-Core Version:    0.6.2
 */