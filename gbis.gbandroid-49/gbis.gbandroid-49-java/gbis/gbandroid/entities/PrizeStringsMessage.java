package gbis.gbandroid.entities;

public class PrizeStringsMessage
{
  private String date;
  private String explanation;
  private String limit;
  private String title;
  private String totalPoints;

  public PrizeStringsMessage copy()
  {
    PrizeStringsMessage localPrizeStringsMessage = new PrizeStringsMessage();
    localPrizeStringsMessage.date = this.date;
    localPrizeStringsMessage.explanation = this.explanation;
    localPrizeStringsMessage.limit = this.limit;
    localPrizeStringsMessage.title = this.title;
    localPrizeStringsMessage.totalPoints = this.totalPoints;
    return localPrizeStringsMessage;
  }

  public String getDate()
  {
    return this.date;
  }

  public String getExplanation()
  {
    return this.explanation;
  }

  public String getLimit()
  {
    return this.limit;
  }

  public String getTitle()
  {
    return this.title;
  }

  public String getTotalPoints()
  {
    return this.totalPoints;
  }

  public void setDate(String paramString)
  {
    this.date = paramString;
  }

  public void setExplanation(String paramString)
  {
    this.explanation = paramString;
  }

  public void setLimit(String paramString)
  {
    this.limit = paramString;
  }

  public void setTitle(String paramString)
  {
    this.title = paramString;
  }

  public void setTotalPoints(String paramString)
  {
    this.totalPoints = paramString;
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Title: ");
    localStringBuilder.append(this.title);
    localStringBuilder.append('\n');
    localStringBuilder.append("Date: ");
    localStringBuilder.append(this.date);
    localStringBuilder.append('\n');
    localStringBuilder.append("Explanation: ");
    localStringBuilder.append(this.explanation);
    localStringBuilder.append('\n');
    localStringBuilder.append("Limit: ");
    localStringBuilder.append(this.limit);
    localStringBuilder.append('\n');
    localStringBuilder.append("Total Points: ");
    localStringBuilder.append(this.totalPoints);
    localStringBuilder.append('\n');
    return localStringBuilder.toString();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.PrizeStringsMessage
 * JD-Core Version:    0.6.2
 */