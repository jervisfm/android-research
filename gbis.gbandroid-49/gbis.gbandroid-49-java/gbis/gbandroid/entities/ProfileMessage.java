package gbis.gbandroid.entities;

public class ProfileMessage
{
  private String car;
  private int carIconId;
  private int consecutiveDays;
  private int forumPosts;
  private String forumTitle;
  private String joinDate;
  private int overallRank;
  private int overallRank30Days;
  private String picturePath;
  private int pointBalance;
  private int pointsToday;
  private String site;
  private String siteName;
  private int totalPoints;

  public ProfileMessage copy()
  {
    ProfileMessage localProfileMessage = new ProfileMessage();
    localProfileMessage.car = this.car;
    localProfileMessage.carIconId = this.carIconId;
    localProfileMessage.consecutiveDays = this.consecutiveDays;
    localProfileMessage.forumPosts = this.forumPosts;
    localProfileMessage.forumTitle = this.forumTitle;
    localProfileMessage.joinDate = this.joinDate;
    localProfileMessage.overallRank = this.overallRank;
    localProfileMessage.overallRank30Days = this.overallRank30Days;
    localProfileMessage.picturePath = this.picturePath;
    localProfileMessage.pointBalance = this.pointBalance;
    localProfileMessage.pointsToday = this.pointsToday;
    localProfileMessage.site = this.site;
    localProfileMessage.siteName = this.siteName;
    localProfileMessage.totalPoints = this.totalPoints;
    return localProfileMessage;
  }

  public String getCar()
  {
    return this.car;
  }

  public int getCarIconId()
  {
    return this.carIconId;
  }

  public int getConsecutiveDays()
  {
    return this.consecutiveDays;
  }

  public int getForumPosts()
  {
    return this.forumPosts;
  }

  public String getForumTitle()
  {
    return this.forumTitle;
  }

  public String getJoinDate()
  {
    return this.joinDate;
  }

  public int getOverallRank()
  {
    return this.overallRank;
  }

  public int getOverallRank30Days()
  {
    return this.overallRank30Days;
  }

  public String getPicturePath()
  {
    return this.picturePath;
  }

  public int getPointBalance()
  {
    return this.pointBalance;
  }

  public int getPointsToday()
  {
    return this.pointsToday;
  }

  public String getSite()
  {
    return this.site;
  }

  public String getSiteName()
  {
    return this.siteName;
  }

  public int getTotalPoints()
  {
    return this.totalPoints;
  }

  public void setCar(String paramString)
  {
    this.car = paramString;
  }

  public void setCarIconId(String paramString)
  {
    this.carIconId = Integer.parseInt(paramString);
  }

  public void setConsecutiveDays(String paramString)
  {
    this.consecutiveDays = Integer.parseInt(paramString);
  }

  public void setForumPosts(String paramString)
  {
    this.forumPosts = Integer.parseInt(paramString);
  }

  public void setForumTitle(String paramString)
  {
    this.forumTitle = paramString;
  }

  public void setJoinDate(String paramString)
  {
    this.joinDate = paramString;
  }

  public void setOverallRank(String paramString)
  {
    this.overallRank = Integer.parseInt(paramString);
  }

  public void setOverallRank30Days(String paramString)
  {
    this.overallRank30Days = Integer.parseInt(paramString);
  }

  public void setPicturePath(String paramString)
  {
    this.picturePath = paramString;
  }

  public void setPointBalance(String paramString)
  {
    this.pointBalance = Integer.parseInt(paramString);
  }

  public void setPointsToday(String paramString)
  {
    this.pointsToday = Integer.parseInt(paramString);
  }

  public void setSite(String paramString)
  {
    this.site = paramString;
  }

  public void setSiteName(String paramString)
  {
    this.siteName = paramString;
  }

  public void setTotalPoints(String paramString)
  {
    this.totalPoints = Integer.parseInt(paramString);
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Site: ");
    localStringBuilder.append(this.site);
    localStringBuilder.append('\n');
    localStringBuilder.append("Site Name: ");
    localStringBuilder.append(this.siteName);
    localStringBuilder.append('\n');
    localStringBuilder.append("Join Date: ");
    localStringBuilder.append(this.joinDate);
    localStringBuilder.append('\n');
    localStringBuilder.append("Forum Title: ");
    localStringBuilder.append(this.forumTitle);
    localStringBuilder.append('\n');
    localStringBuilder.append("Total Points: ");
    localStringBuilder.append(this.totalPoints);
    localStringBuilder.append('\n');
    localStringBuilder.append("Picture Path: ");
    localStringBuilder.append(this.picturePath);
    localStringBuilder.append('\n');
    localStringBuilder.append("Overall Rank: ");
    localStringBuilder.append(this.overallRank);
    localStringBuilder.append('\n');
    localStringBuilder.append("Overall Rank 30 Days: ");
    localStringBuilder.append(this.overallRank30Days);
    localStringBuilder.append('\n');
    localStringBuilder.append("Consecutive Days: ");
    localStringBuilder.append(this.consecutiveDays);
    localStringBuilder.append('\n');
    localStringBuilder.append("Forum Posts: ");
    localStringBuilder.append(this.forumPosts);
    localStringBuilder.append('\n');
    localStringBuilder.append("Points Today: ");
    localStringBuilder.append(this.pointsToday);
    localStringBuilder.append('\n');
    localStringBuilder.append("Point Balance: ");
    localStringBuilder.append(this.pointBalance);
    localStringBuilder.append('\n');
    localStringBuilder.append("Car: ");
    localStringBuilder.append(this.car);
    localStringBuilder.append('\n');
    localStringBuilder.append("Car Icon Id: ");
    localStringBuilder.append(this.carIconId);
    localStringBuilder.append('\n');
    return localStringBuilder.toString();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.ProfileMessage
 * JD-Core Version:    0.6.2
 */