package gbis.gbandroid.entities;

import java.util.ArrayList;
import java.util.List;

public class RegisterMessage
{
  private String car;
  private int carIconId;
  private String memberId;
  private List<String> memberIdSuggestions = new ArrayList();
  private boolean signedIn;

  public void addMemberIdSuggestions(String paramString)
  {
    this.memberIdSuggestions.add(paramString);
  }

  public RegisterMessage copy()
  {
    RegisterMessage localRegisterMessage = new RegisterMessage();
    localRegisterMessage.signedIn = this.signedIn;
    localRegisterMessage.car = this.car;
    localRegisterMessage.carIconId = this.carIconId;
    localRegisterMessage.memberId = this.memberId;
    localRegisterMessage.memberIdSuggestions = this.memberIdSuggestions;
    return localRegisterMessage;
  }

  public String getCar()
  {
    return this.car;
  }

  public int getCarIconId()
  {
    return this.carIconId;
  }

  public String getMemberId()
  {
    return this.memberId;
  }

  public List<String> getMemberIdSuggestions()
  {
    return this.memberIdSuggestions;
  }

  public boolean isSignedIn()
  {
    return this.signedIn;
  }

  public void setCar(String paramString)
  {
    this.car = paramString;
  }

  public void setCarIconId(String paramString)
  {
    this.carIconId = Integer.parseInt(paramString);
  }

  public void setMemberId(String paramString)
  {
    this.memberId = paramString;
  }

  public void setSignedIn(String paramString)
  {
    this.signedIn = new Boolean(paramString).booleanValue();
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Signed In: ");
    localStringBuilder.append(this.signedIn);
    localStringBuilder.append('\n');
    localStringBuilder.append("Car: ");
    localStringBuilder.append(this.car);
    localStringBuilder.append('\n');
    localStringBuilder.append("Car Icon Id: ");
    localStringBuilder.append(this.carIconId);
    localStringBuilder.append('\n');
    localStringBuilder.append("Member ID: ");
    localStringBuilder.append(this.memberId);
    localStringBuilder.append('\n');
    return localStringBuilder.toString();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.RegisterMessage
 * JD-Core Version:    0.6.2
 */