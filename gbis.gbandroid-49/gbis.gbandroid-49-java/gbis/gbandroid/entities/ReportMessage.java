package gbis.gbandroid.entities;

import java.math.BigInteger;

public class ReportMessage
{
  private String carIcon;
  private int carIconId;
  private double dieselPrice;
  private BigInteger dieselPriceId;
  private double midPrice;
  private BigInteger midPriceId;
  private int pointBalance;
  private double premPrice;
  private BigInteger premPriceId;
  private double regPrice;
  private BigInteger regPriceId;
  private int totalPointsAwarded;

  public ReportMessage copy()
  {
    ReportMessage localReportMessage = new ReportMessage();
    localReportMessage.carIcon = this.carIcon;
    localReportMessage.carIconId = this.carIconId;
    localReportMessage.dieselPriceId = this.dieselPriceId;
    localReportMessage.midPriceId = this.midPriceId;
    localReportMessage.premPriceId = this.premPriceId;
    localReportMessage.regPriceId = this.regPriceId;
    localReportMessage.totalPointsAwarded = this.totalPointsAwarded;
    localReportMessage.pointBalance = this.pointBalance;
    return localReportMessage;
  }

  public String getCarIcon()
  {
    return this.carIcon;
  }

  public int getCarIconId()
  {
    return this.carIconId;
  }

  public double getDieselPrice()
  {
    return this.dieselPrice;
  }

  public BigInteger getDieselPriceId()
  {
    return this.dieselPriceId;
  }

  public double getMidPrice()
  {
    return this.midPrice;
  }

  public BigInteger getMidPriceId()
  {
    return this.midPriceId;
  }

  public int getPointBalance()
  {
    return this.pointBalance;
  }

  public double getPremPrice()
  {
    return this.premPrice;
  }

  public BigInteger getPremPriceId()
  {
    return this.premPriceId;
  }

  public double getRegPrice()
  {
    return this.regPrice;
  }

  public BigInteger getRegPriceId()
  {
    return this.regPriceId;
  }

  public int getTotalPointsAwarded()
  {
    return this.totalPointsAwarded;
  }

  public void setCarIcon(String paramString)
  {
    this.carIcon = paramString;
  }

  public void setCarIconId(int paramInt)
  {
    this.carIconId = paramInt;
  }

  public void setDieselPrice(double paramDouble)
  {
    this.dieselPrice = paramDouble;
  }

  public void setDieselPriceId(BigInteger paramBigInteger)
  {
    this.dieselPriceId = paramBigInteger;
  }

  public void setMidPrice(double paramDouble)
  {
    this.midPrice = paramDouble;
  }

  public void setMidPriceId(BigInteger paramBigInteger)
  {
    this.midPriceId = paramBigInteger;
  }

  public void setPointBalance(int paramInt)
  {
    this.pointBalance = paramInt;
  }

  public void setPremPrice(double paramDouble)
  {
    this.premPrice = paramDouble;
  }

  public void setPremPriceId(BigInteger paramBigInteger)
  {
    this.premPriceId = paramBigInteger;
  }

  public void setRegPrice(double paramDouble)
  {
    this.regPrice = paramDouble;
  }

  public void setRegPriceId(BigInteger paramBigInteger)
  {
    this.regPriceId = paramBigInteger;
  }

  public void setTotalPointsAwarded(int paramInt)
  {
    this.totalPointsAwarded = paramInt;
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Regular price: ");
    localStringBuilder.append(this.regPriceId);
    localStringBuilder.append('\n');
    localStringBuilder.append("Midgrade price: ");
    localStringBuilder.append(this.midPriceId);
    localStringBuilder.append('\n');
    localStringBuilder.append("Premium price: ");
    localStringBuilder.append(this.premPriceId);
    localStringBuilder.append('\n');
    localStringBuilder.append("Diesel price: ");
    localStringBuilder.append(this.dieselPriceId);
    localStringBuilder.append('\n');
    localStringBuilder.append("Total Points Awarded: ");
    localStringBuilder.append(this.totalPointsAwarded);
    localStringBuilder.append('\n');
    localStringBuilder.append("Point Balance: ");
    localStringBuilder.append(this.pointBalance);
    localStringBuilder.append('\n');
    localStringBuilder.append("Car Icon: ");
    localStringBuilder.append(this.carIcon);
    localStringBuilder.append('\n');
    localStringBuilder.append("Car Icon Id: ");
    localStringBuilder.append(this.carIconId);
    localStringBuilder.append('\n');
    return localStringBuilder.toString();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.ReportMessage
 * JD-Core Version:    0.6.2
 */