package gbis.gbandroid.entities;

import com.google.gbson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

public class ResponseMessage<T>
{

  @SerializedName("AwardCollection")
  private List<AwardsMessage> awards;

  @SerializedName("Payload")
  private T payload;

  @SerializedName("ResponseCode")
  private int responseCode;

  @SerializedName("ResponseMessage")
  private String responseMessage;

  @SerializedName("SmartPromptCollection")
  private List<SmartPrompt> smartPrompts;

  public ResponseMessage()
  {
    setAwards(new ArrayList());
  }

  public void addAwards(AwardsMessage paramAwardsMessage)
  {
    this.awards.add(paramAwardsMessage);
  }

  public ResponseMessage<T> copy()
  {
    ResponseMessage localResponseMessage = new ResponseMessage();
    localResponseMessage.awards = this.awards;
    localResponseMessage.payload = this.payload;
    localResponseMessage.responseCode = this.responseCode;
    localResponseMessage.responseMessage = this.responseMessage;
    localResponseMessage.smartPrompts = this.smartPrompts;
    return localResponseMessage;
  }

  public List<AwardsMessage> getAwards()
  {
    return this.awards;
  }

  public T getPayload()
  {
    return this.payload;
  }

  public int getResponseCode()
  {
    return this.responseCode;
  }

  public String getResponseMessage()
  {
    return this.responseMessage;
  }

  public List<SmartPrompt> getSmartPrompts()
  {
    return this.smartPrompts;
  }

  public void setAwards(List<AwardsMessage> paramList)
  {
    this.awards = paramList;
  }

  public void setPayload(T paramT)
  {
    this.payload = paramT;
  }

  public void setResponseCode(int paramInt)
  {
    this.responseCode = paramInt;
  }

  public void setResponseMessage(String paramString)
  {
    this.responseMessage = paramString;
  }

  public void setSmartPrompts(List<SmartPrompt> paramList)
  {
    this.smartPrompts = paramList;
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Response Code: ");
    localStringBuilder.append(this.responseCode);
    localStringBuilder.append('\n');
    localStringBuilder.append("Response Message: ");
    localStringBuilder.append(this.responseMessage);
    localStringBuilder.append('\n');
    localStringBuilder.append("Payload: ");
    localStringBuilder.append(this.payload);
    localStringBuilder.append('\n');
    localStringBuilder.append("Awards: ");
    localStringBuilder.append(this.awards);
    localStringBuilder.append('\n');
    return localStringBuilder.toString();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.ResponseMessage
 * JD-Core Version:    0.6.2
 */