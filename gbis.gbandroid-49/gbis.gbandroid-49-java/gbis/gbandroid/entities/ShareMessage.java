package gbis.gbandroid.entities;

public class ShareMessage
{
  private String message;
  private String subject;

  public ShareMessage copy()
  {
    ShareMessage localShareMessage = new ShareMessage();
    localShareMessage.message = this.message;
    localShareMessage.subject = this.subject;
    return localShareMessage;
  }

  public String getMessage()
  {
    return this.message;
  }

  public String getSubject()
  {
    return this.subject;
  }

  public void setMessage(String paramString)
  {
    this.message = paramString;
  }

  public void setSubject(String paramString)
  {
    this.subject = paramString;
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Subject: ");
    localStringBuilder.append(this.subject);
    localStringBuilder.append('\n');
    localStringBuilder.append("Message: ");
    localStringBuilder.append(this.message);
    localStringBuilder.append('\n');
    return localStringBuilder.toString();
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.ShareMessage
 * JD-Core Version:    0.6.2
 */