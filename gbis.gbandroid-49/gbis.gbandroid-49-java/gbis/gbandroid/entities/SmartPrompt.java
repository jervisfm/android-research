package gbis.gbandroid.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gbson.annotations.SerializedName;

public class SmartPrompt
  implements Parcelable
{
  public static final Parcelable.Creator<SmartPrompt> CREATOR = new m();
  private String prompt;

  @SerializedName("PromptID")
  private int promptId;

  public SmartPrompt()
  {
  }

  private SmartPrompt(Parcel paramParcel)
  {
    readFromParcel(paramParcel);
  }

  public SmartPrompt copy()
  {
    SmartPrompt localSmartPrompt = new SmartPrompt();
    localSmartPrompt.prompt = this.prompt;
    localSmartPrompt.promptId = this.promptId;
    return localSmartPrompt;
  }

  public int describeContents()
  {
    return 0;
  }

  public String getPrompt()
  {
    return this.prompt;
  }

  public int getPromptId()
  {
    return this.promptId;
  }

  protected void readFromParcel(Parcel paramParcel)
  {
    this.prompt = paramParcel.readString();
    this.promptId = paramParcel.readInt();
  }

  public void setPrompt(String paramString)
  {
    this.prompt = paramString;
  }

  public void setPromptId(int paramInt)
  {
    this.promptId = paramInt;
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeString(this.prompt);
    paramParcel.writeInt(this.promptId);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.SmartPrompt
 * JD-Core Version:    0.6.2
 */