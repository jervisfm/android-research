package gbis.gbandroid.entities;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

public abstract class Station
  implements Parcelable
{
  protected String address;
  protected String city;
  protected String country;
  protected String crossStreet;
  protected int gasBrandId;
  protected int gasBrandVersion;
  protected double latitude;
  protected double longitude;
  protected boolean notIntersection;
  protected String postalCode;
  protected String state;
  protected int stationId;
  protected String stationName;
  protected int timeOffset;

  public String getAddress()
  {
    return this.address;
  }

  public String getCity()
  {
    return this.city;
  }

  public String getCountry()
  {
    return this.country;
  }

  public String getCrossStreet()
  {
    return this.crossStreet;
  }

  public int getGasBrandId()
  {
    return this.gasBrandId;
  }

  public int getGasBrandVersion()
  {
    return this.gasBrandVersion;
  }

  public double getLatitude()
  {
    return this.latitude;
  }

  public double getLongitude()
  {
    return this.longitude;
  }

  public String getPostalCode()
  {
    return this.postalCode;
  }

  public abstract double getPrice(Context paramContext, String paramString);

  public String getState()
  {
    return this.state;
  }

  public int getStationId()
  {
    return this.stationId;
  }

  public String getStationName()
  {
    return this.stationName;
  }

  public int getTimeOffset()
  {
    return this.timeOffset;
  }

  public abstract String getTimeSpottedConverted(Context paramContext, String paramString);

  public boolean isNotIntersection()
  {
    return this.notIntersection;
  }

  protected void readFromParcel(Parcel paramParcel)
  {
    this.stationId = paramParcel.readInt();
    this.address = paramParcel.readString();
    this.crossStreet = paramParcel.readString();
    this.stationName = paramParcel.readString();
    this.city = paramParcel.readString();
    this.state = paramParcel.readString();
    this.country = paramParcel.readString();
    this.postalCode = paramParcel.readString();
    this.latitude = paramParcel.readDouble();
    this.longitude = paramParcel.readDouble();
    this.timeOffset = paramParcel.readInt();
    this.gasBrandId = paramParcel.readInt();
    this.gasBrandVersion = paramParcel.readInt();
  }

  public void setAddress(String paramString)
  {
    this.address = paramString;
  }

  public void setCity(String paramString)
  {
    this.city = paramString;
  }

  public void setCountry(String paramString)
  {
    this.country = paramString;
  }

  public void setCrossStreet(String paramString)
  {
    this.crossStreet = paramString;
  }

  public void setGasBrandId(int paramInt)
  {
    this.gasBrandId = paramInt;
  }

  public void setGasBrandVersion(int paramInt)
  {
    this.gasBrandVersion = paramInt;
  }

  public void setLatitude(double paramDouble)
  {
    this.latitude = paramDouble;
  }

  public void setLongitude(double paramDouble)
  {
    this.longitude = paramDouble;
  }

  public void setNotIntersection(boolean paramBoolean)
  {
    this.notIntersection = paramBoolean;
  }

  public void setPostalCode(String paramString)
  {
    this.postalCode = paramString;
  }

  public void setState(String paramString)
  {
    this.state = paramString;
  }

  public void setStationId(int paramInt)
  {
    this.stationId = paramInt;
  }

  public void setStationName(String paramString)
  {
    this.stationName = paramString;
  }

  public void setTimeOffset(int paramInt)
  {
    this.timeOffset = paramInt;
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Station Id: ");
    localStringBuilder.append(this.stationId);
    localStringBuilder.append('\n');
    localStringBuilder.append("Adress: ");
    localStringBuilder.append(this.address);
    localStringBuilder.append('\n');
    localStringBuilder.append("Cross Street: ");
    localStringBuilder.append(this.crossStreet);
    localStringBuilder.append('\n');
    localStringBuilder.append("Not Intersection: ");
    localStringBuilder.append(this.notIntersection);
    localStringBuilder.append('\n');
    localStringBuilder.append("Station Name: ");
    localStringBuilder.append(this.stationName);
    localStringBuilder.append('\n');
    localStringBuilder.append("City: ");
    localStringBuilder.append(this.city);
    localStringBuilder.append('\n');
    localStringBuilder.append("State: ");
    localStringBuilder.append(this.state);
    localStringBuilder.append('\n');
    localStringBuilder.append("Country: ");
    localStringBuilder.append(this.country);
    localStringBuilder.append('\n');
    localStringBuilder.append("Postal Code: ");
    localStringBuilder.append(this.postalCode);
    localStringBuilder.append('\n');
    localStringBuilder.append("Latitude: ");
    localStringBuilder.append(this.latitude);
    localStringBuilder.append('\n');
    localStringBuilder.append("Longitude: ");
    localStringBuilder.append(this.longitude);
    localStringBuilder.append('\n');
    return localStringBuilder.toString();
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeInt(this.stationId);
    paramParcel.writeString(this.address);
    paramParcel.writeString(this.crossStreet);
    paramParcel.writeString(this.stationName);
    paramParcel.writeString(this.city);
    paramParcel.writeString(this.state);
    paramParcel.writeString(this.country);
    paramParcel.writeString(this.postalCode);
    paramParcel.writeDouble(this.latitude);
    paramParcel.writeDouble(this.longitude);
    paramParcel.writeInt(this.timeOffset);
    paramParcel.writeInt(this.gasBrandId);
    paramParcel.writeInt(this.gasBrandVersion);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.Station
 * JD-Core Version:    0.6.2
 */