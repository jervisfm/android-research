package gbis.gbandroid.entities;

import android.content.Context;
import android.os.Parcel;
import gbis.gbandroid.utils.DateUtils;

public abstract class StationDetailed extends Station
{
  protected String dieselCar;
  protected String dieselMemberId;
  protected double dieselPrice;
  protected String dieselTimeSpotted;
  protected String midCar;
  protected String midMemberId;
  protected double midPrice;
  protected String midTimeSpotted;
  protected String premCar;
  protected String premMemberId;
  protected double premPrice;
  protected String premTimeSpotted;
  protected String regCar;
  protected String regMemberId;
  protected double regPrice;
  protected String regTimeSpotted;

  public String getDieselCar()
  {
    return this.dieselCar;
  }

  public String getDieselMemberId()
  {
    return this.dieselMemberId;
  }

  public double getDieselPrice()
  {
    return this.dieselPrice;
  }

  public String getDieselTimeSpotted()
  {
    return this.dieselTimeSpotted;
  }

  public String getMidCar()
  {
    return this.midCar;
  }

  public String getMidMemberId()
  {
    return this.midMemberId;
  }

  public double getMidPrice()
  {
    return this.midPrice;
  }

  public String getMidTimeSpotted()
  {
    return this.midTimeSpotted;
  }

  public String getPremCar()
  {
    return this.premCar;
  }

  public String getPremMemberId()
  {
    return this.premMemberId;
  }

  public double getPremPrice()
  {
    return this.premPrice;
  }

  public String getPremTimeSpotted()
  {
    return this.premTimeSpotted;
  }

  public double getPrice(Context paramContext, String paramString)
  {
    if (paramString.equals(paramContext.getString(2131296713)))
      return this.regPrice;
    if (paramString.equals(paramContext.getString(2131296714)))
      return this.midPrice;
    if (paramString.equals(paramContext.getString(2131296715)))
      return this.premPrice;
    if (paramString.equals(paramContext.getString(2131296716)))
      return this.dieselPrice;
    return 0.0D;
  }

  public String getRegCar()
  {
    return this.regCar;
  }

  public String getRegMemberId()
  {
    return this.regMemberId;
  }

  public double getRegPrice()
  {
    return this.regPrice;
  }

  public String getRegTimeSpotted()
  {
    return this.regTimeSpotted;
  }

  public String getTimeSpottedConverted(Context paramContext, String paramString)
  {
    if (paramString.equals(paramContext.getString(2131296713)))
      return DateUtils.compareDateToNow(DateUtils.toDateFormat(this.regTimeSpotted), this.timeOffset);
    if (paramString.equals(paramContext.getString(2131296714)))
      return DateUtils.compareDateToNow(DateUtils.toDateFormat(this.midTimeSpotted), this.timeOffset);
    if (paramString.equals(paramContext.getString(2131296715)))
      return DateUtils.compareDateToNow(DateUtils.toDateFormat(this.premTimeSpotted), this.timeOffset);
    if (paramString.equals(paramContext.getString(2131296716)))
      return DateUtils.compareDateToNow(DateUtils.toDateFormat(this.dieselTimeSpotted), this.timeOffset);
    return "";
  }

  protected void readFromParcel(Parcel paramParcel)
  {
    super.readFromParcel(paramParcel);
    this.regPrice = paramParcel.readDouble();
    this.midPrice = paramParcel.readDouble();
    this.premPrice = paramParcel.readDouble();
    this.dieselPrice = paramParcel.readDouble();
    this.regTimeSpotted = paramParcel.readString();
    this.regMemberId = paramParcel.readString();
    this.regCar = paramParcel.readString();
    this.midTimeSpotted = paramParcel.readString();
    this.midMemberId = paramParcel.readString();
    this.midCar = paramParcel.readString();
    this.premTimeSpotted = paramParcel.readString();
    this.premMemberId = paramParcel.readString();
    this.premCar = paramParcel.readString();
    this.dieselTimeSpotted = paramParcel.readString();
    this.dieselMemberId = paramParcel.readString();
    this.dieselCar = paramParcel.readString();
  }

  public void setDieselCar(String paramString)
  {
    this.dieselCar = paramString;
  }

  public void setDieselMemberId(String paramString)
  {
    this.dieselMemberId = paramString;
  }

  public void setDieselPrice(double paramDouble)
  {
    this.dieselPrice = paramDouble;
  }

  public void setDieselTimeSpotted(String paramString)
  {
    this.dieselTimeSpotted = paramString;
  }

  public void setMidCar(String paramString)
  {
    this.midCar = paramString;
  }

  public void setMidMemberId(String paramString)
  {
    this.midMemberId = paramString;
  }

  public void setMidPrice(double paramDouble)
  {
    this.midPrice = paramDouble;
  }

  public void setMidTimeSpotted(String paramString)
  {
    this.midTimeSpotted = paramString;
  }

  public void setPremCar(String paramString)
  {
    this.premCar = paramString;
  }

  public void setPremMemberId(String paramString)
  {
    this.premMemberId = paramString;
  }

  public void setPremPrice(double paramDouble)
  {
    this.premPrice = paramDouble;
  }

  public void setPremTimeSpotted(String paramString)
  {
    this.premTimeSpotted = paramString;
  }

  public void setRegCar(String paramString)
  {
    this.regCar = paramString;
  }

  public void setRegMemberId(String paramString)
  {
    this.regMemberId = paramString;
  }

  public void setRegPrice(double paramDouble)
  {
    this.regPrice = paramDouble;
  }

  public void setRegTimeSpotted(String paramString)
  {
    this.regTimeSpotted = paramString;
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(super.toString());
    localStringBuilder.append("Regular price: ");
    localStringBuilder.append(this.regPrice);
    localStringBuilder.append('\n');
    localStringBuilder.append("Regular time: ");
    localStringBuilder.append(this.regTimeSpotted);
    localStringBuilder.append('\n');
    localStringBuilder.append("Regular Member Id: ");
    localStringBuilder.append(this.regMemberId);
    localStringBuilder.append('\n');
    localStringBuilder.append("Regular Car: ");
    localStringBuilder.append(this.regCar);
    localStringBuilder.append('\n');
    localStringBuilder.append("Midgrade price: ");
    localStringBuilder.append(this.midPrice);
    localStringBuilder.append('\n');
    localStringBuilder.append("Midgrade time: ");
    localStringBuilder.append(this.midTimeSpotted);
    localStringBuilder.append('\n');
    localStringBuilder.append("Midgrade Member Id: ");
    localStringBuilder.append(this.midMemberId);
    localStringBuilder.append('\n');
    localStringBuilder.append("Midgrade Car: ");
    localStringBuilder.append(this.midCar);
    localStringBuilder.append('\n');
    localStringBuilder.append("Premium price: ");
    localStringBuilder.append(this.premPrice);
    localStringBuilder.append('\n');
    localStringBuilder.append("Premium time: ");
    localStringBuilder.append(this.premTimeSpotted);
    localStringBuilder.append('\n');
    localStringBuilder.append("Premium Member Id: ");
    localStringBuilder.append(this.premMemberId);
    localStringBuilder.append('\n');
    localStringBuilder.append("Premium Car: ");
    localStringBuilder.append(this.premCar);
    localStringBuilder.append('\n');
    localStringBuilder.append("Diesel price: ");
    localStringBuilder.append(this.dieselPrice);
    localStringBuilder.append('\n');
    localStringBuilder.append("Diesel time: ");
    localStringBuilder.append(this.dieselTimeSpotted);
    localStringBuilder.append('\n');
    localStringBuilder.append("Diesel Member Id: ");
    localStringBuilder.append(this.dieselMemberId);
    localStringBuilder.append('\n');
    localStringBuilder.append("Diesel Car: ");
    localStringBuilder.append(this.dieselCar);
    localStringBuilder.append('\n');
    localStringBuilder.append(super.toString());
    return localStringBuilder.toString();
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    paramParcel.writeDouble(this.regPrice);
    paramParcel.writeDouble(this.midPrice);
    paramParcel.writeDouble(this.premPrice);
    paramParcel.writeDouble(this.dieselPrice);
    paramParcel.writeString(this.regTimeSpotted);
    paramParcel.writeString(this.regMemberId);
    paramParcel.writeString(this.regCar);
    paramParcel.writeString(this.midTimeSpotted);
    paramParcel.writeString(this.midMemberId);
    paramParcel.writeString(this.midCar);
    paramParcel.writeString(this.premTimeSpotted);
    paramParcel.writeString(this.premMemberId);
    paramParcel.writeString(this.premCar);
    paramParcel.writeString(this.dieselTimeSpotted);
    paramParcel.writeString(this.dieselMemberId);
    paramParcel.writeString(this.dieselCar);
  }
}

/* Location:           D:\code\Research\Android\apks\gbis.gbandroid-49\gbis.gbandroid-49_dex2jar.jar
 * Qualified Name:     gbis.gbandroid.entities.StationDetailed
 * JD-Core Version:    0.6.2
 */